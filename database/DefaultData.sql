﻿-- Demo Data --
-- Default Data to be loaded for Rudhira Billing System --
-- Select database --
USE rudhira;
-- Insert a default Blood Bank --
INSERT INTO BloodBank (id, name, registrationNo, logo, address, phone1, phone2, email, website, tagline) VALUES (1, "Kovid Blood Bank", "E 1226", "jbb.jpg", "S. No. 24/3, Shirdharnagar, Dhankawadi, Pune", "(020) 24444502", "(020) 24229527", "kovidbioanalytics@gmail.com", "www.kovidbioanalytics.com", "Blood donation is the best donation");
-- Insert a default admin user --
INSERT INTO AppUser (id, firstName, middleName, lastName, phone, address, city, state, country, email, username, password, pin, role, activityStatus, bloodBankID) VALUES (1, "Mandar", "S", "Sahasrabuddhe", "9823014386", "Dhankawadi", "Pune", "Maharashtra", "India", "mandar@kovidbioanalytics.com", "admin", "hTp5/BwP6VgfI1UijRbuXw==", "5sjGPLK1b2c=", "administrator", "Active", "1");
-- Insert Permissible Values for Concession --
INSERT INTO PVConcession (id, concessionType, concessionAmt, bloodBankID) VALUES (1, "IPF IP", "100", 1);
INSERT INTO PVConcession (id, concessionType, concessionAmt, bloodBankID) VALUES (2, "IPF WSP", "100", 1);
INSERT INTO PVConcession (id, concessionType, concessionAmt, bloodBankID) VALUES (3, "Cancer", "200", 1);
INSERT INTO PVConcession (id, concessionType, concessionAmt, bloodBankID) VALUES (4, "Dialysis", "300", 1);
INSERT INTO PVConcession (id, concessionType, concessionAmt, bloodBankID) VALUES (5, "Poor & Needy", "200", 1);
INSERT INTO PVConcession (id, concessionType, concessionAmt, bloodBankID) VALUES (6, "Rare Blood Disease Disorder", "100", 1);
INSERT INTO PVConcession (id, concessionType, concessionAmt, bloodBankID) VALUES (7, "Thalassemia", "100", 1);
INSERT INTO PVConcession (id, concessionType, concessionAmt, bloodBankID) VALUES (8, "Donor Coupon", "100", 1);
INSERT INTO PVConcession (id, concessionType, concessionAmt, bloodBankID) VALUES (9, "BSC", "100", 1);
INSERT INTO PVConcession (id, concessionType, concessionAmt, bloodBankID) VALUES (10, "MOU", "100", 1);
INSERT INTO PVConcession (id, concessionType, concessionAmt, bloodBankID) VALUES (11, "Hospital Discount", "100", 1);
-- Insert Permissible Values for Other Charges --
INSERT INTO PVOtherCharges (id, chargeType, charges, bloodBankID) VALUES (1, "BT Set", "100", 1);
INSERT INTO PVOtherCharges (id, chargeType, charges, bloodBankID) VALUES (2, "Irradiation", "100", 1);
INSERT INTO PVOtherCharges (id, chargeType, charges, bloodBankID) VALUES (3, "Courier", "100", 1);
INSERT INTO PVOtherCharges (id, chargeType, charges, bloodBankID) VALUES (4, "Gr Cross Match", "200", 1);
INSERT INTO PVOtherCharges (id, chargeType, charges, bloodBankID) VALUES (5, "ABD Screening", "250", 1);
-- Insert Permissible Values for Components --
-- Component WB --
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (1, "WB", "General", "250", 1);
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (2, "WB", "Thalassemia General", "250", 1);
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (3, "WB", "Paediatric below 100 ml General", "280", 1);
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (4, "WB", "NAT", "280", 1);
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (5, "WB", "Thalassemia NAT", "280", 1);
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (6, "WB", "Paediatric below 100 ml NAT", "280", 1);

-- Component PC --
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (7, "WB", "General", "250", 1);
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (8, "WB", "Thalassemia General", "250", 1);
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (9, "WB", "Paediatric below 100 ml General", "280", 1);
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (10, "WB", "NAT", "250", 1);
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (11, "WB", "Thalassemia NAT", "250", 1);
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (12, "WB", "Paediatric below 100 ml NAT", "250", 1);
-- Component LR RCC: No values given --
-- Component LD RCC: No values given --
-- Component SW RCC: No values given --
-- Component IR RCC: No values given --
-- Component PEDIA --
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (13, "PEDIA", "General", "280", 1);
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (14, "PEDIA", "Thalassemia General", "280", 1);
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (15, "PEDIA", "Paediatric below 100 ml General", "280", 1);
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (16, "PEDIA", "NAT", "280", 1);
-- Component FFP --
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (17, "FFP", "General", "200", 1);
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (18, "FFP", "Thalassemia General", "200", 1);
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (19, "FFP", "Paediatric below 100 ml General", "200", 1);
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (20, "FFP", "NAT", "200", 1);
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (21, "FFP", "Paediatric below 100 ml NAT", "200", 1);
-- Component Platelet --
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (22, "Platelet", "General", "200", 1);
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (23, "Platelet", "Thalassemia General", "200", 1);
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (24, "Platelet", "Paediatric below 100 ml General", "200", 1);
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (25, "Platelet", "NAT", "200", 1);
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (26, "Platelet", "Paediatric below 100 ml NAT", "200", 1);
-- Component LP PC: No values given --
-- Component LD PC: No values given --
-- Component Cryo --
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (27, "Platelet", "General", "200", 1);
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (28, "Platelet", "Thalassemia General", "200", 1);
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (29, "Platelet", "Paediatric below 100 ml General", "200", 1);
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (30, "Platelet", "NAT", "200", 1);
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (31, "Platelet", "Paediatric below 100 ml NAT", "200", 1);
-- Component CPP: No values given --
-- Component SDP --
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (32, "SDP", "General", "200", 1);
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (33, "SDP", "NAT", "200", 1);
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (34, "Half SDP", "General", "200", 1);
INSERT INTO PVComponents (id, component, category, price, bloodBankID) VALUES (35, "Half SDP", "NAT", "200", 1);
-- Instructions --
