<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Login | RudhiraKosh</title>

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="https://colorlib.com/polygon/gentelella/css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="build/css/custom.min.css" rel="stylesheet">
    
    <!-- For Back button -->
    <script type="text/javascript">
	jQuery(document).ready(function($) {

		if (window.history && window.history.pushState) {

			window.history.pushState('forward', null, './');

			$(window).on('popstate', function() {
				alert('Please login again');
			});

		}
	});
	</script>
	
	<!-- Ends -->

	<!-- For success and error messages (to remove . before messages) -->
	<style type="text/css">

		ul.actionMessage{
			list-style:none;
			font-family: "Helvetica Neue", Roboto, Arial, "Droid Sans", sans-serif;
		}
		
		ul.errorMessage{
			list-style:none;
			font-family: "Helvetica Neue", Roboto, Arial, "Droid Sans", sans-serif;
		} 
		
		#loginMsgID{
			font-family: "Helvetica Neue", Roboto, Arial, "Droid Sans", sans-serif;
		}

	</style>
	
	<%
		String loginMessage = (String)request.getAttribute("loginMessage");
		if(loginMessage == "" || loginMessage == null){
			loginMessage = "";
		}
	
	%>
    
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
         <div align="center">
        	<img alt="RudhiraKosh Logo" src="images/rudhiraKoshLogo.png" style="height: 150px;" class="img-responsive">
         </div>	
          <section class="login_content" style="padding-top:0px;margin-top:-15px;">
          	
            <form action="LoginAction" method="POST">
              <h1>Login </h1>
              
              <div>
              	<s:if test="hasActionErrors()">
					<p class="bg-danger">
					<font style="color: red;font-size:16px;"><s:actionerror /></font>
					</p>
				</s:if><s:else>
					<center>
					<font style="color: green;font-size:16px;"><s:actionmessage /></font>
					</center>
				</s:else>
				<center>
					<font id="loginMsgID" style="color: red;font-size:16px;"><%=loginMessage %></font>
				</center>
              </div>
              
              <div style="margin-top:10px;">
                <input type="text" class="form-control" name="username" placeholder="Username" required="required" />
              </div>
              <div>
                <input type="password" class="form-control" name="password" placeholder="Password" required="required" />
              </div>
              <div>
              	<button type="submit" class="btn btn-default submit">Log in</button>
                <a class="reset_pass" href="#">Forget password?</a>
              </div>

              <div class="clearfix"></div>
              
              <div class="separator">
                
              </div>
              
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>
</html>