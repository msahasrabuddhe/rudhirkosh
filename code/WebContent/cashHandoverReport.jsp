<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="rudhira.DAO.BillingDAOImpl"%>
<%@page import="rudhira.DAO.BillingDAOInf"%>
<%@page import="rudhira.DAO.UserDAOImpl"%>
<%@page import="rudhira.DAO.UserDAOInf"%>
<%@page import="rudhira.form.UserForm"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>    
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Cash hand-over Report | RudhiraKosh</title>

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
    
    <!-- Datatables -->
    <link href="vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    
    <!-- Custom Theme Style -->
    <link href="build/css/custom.min.css" rel="stylesheet">
    
    <script type="text/javascript">
      function windowOpen(){
        document.location="dashboard.jsp";
      }
    </script>
    
    
    <!-- Refresh page on close register cancel button -->
    
    <script type="text/javascript">
    	
    	function refreshPage(){
    		location.reload();
    	}
    
    </script>
    
    <!-- Ends -->
	
	
	<!-- Open close register alert modal -->
    
    <script type="text/javascript">
    	function registerCloseAlert(){
    		$('#logoutModal').modal('hide');
    		$('#closeRegisterAlert').modal('show');
    	}
    	
    	function registerCloseAlertLockScreen(){
    		$('#lockModal').modal('hide');
    		$('#closeRegisterAlertLockScreen').modal('show');
    	}
    </script>
    
    <!-- Ends -->
    
    
    
    <!-- Disable User start -->
    
    <script type="text/javascript">
    	function disableUser(url){
			if(confirm("Disabled users cannot access application. Are you sure you want to disable this user?")){
				document.location = url;
			}
    	}
    </script>
    
    <!-- Ends -->
    
    
    <!-- On Search select check -->
    
    <script type="text/javascript">
    
    	function onSearchCheck(){
    		var value = $('#patientReportCriteriaID').val();
    		
    		if(value == "-1"){
    			
    			$('#onSearchSelectID').modal("show");
    			return false;
    			
    		}else{
    			
    			return true;
    			
    		}
    	}
    
    </script>
    
    <!-- Ends -->
    
    <style type="text/css">

		ul.actionMessage{
			list-style:none;
			font-family: "Helvetica Neue", Roboto, Arial, "Droid Sans", sans-serif;
		}
		
		ul.errorMessage{
			list-style:none;
			font-family: "Helvetica Neue", Roboto, Arial, "Droid Sans", sans-serif;
		}

	</style>
    
    <!-- For login message -->
    <%
    UserForm form = (UserForm) session.getAttribute("USER");
		if(session.getAttribute("USER") == "" || session.getAttribute("USER") == null){
			form.setFullname("");
			String loginMessage = "Plase login using valid credentials";
			request.setAttribute("loginMessage",loginMessage);
			RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
			dispatcher.forward(request,response);
		}
		
		int userID = form.getUserID();
		
	UserDAOInf userDAOInf = new UserDAOImpl();
	
	BillingDAOInf billingDAOInf = new BillingDAOImpl();
	
	%>
    <!-- Ends -->
    
    <!-- Sumbit Open Register, Close register form -->
    
    <script type="text/javascript">
    
    	function submitOpenReg(){
    		document.forms["openRegForm"].action = "OpenRegister";
    		document.forms["openRegForm"].submit();
    	}
    	
    	function submitCloseReg(){
    		
    		if($('#cashHandoverToID').val() == "000"){
    			
    			alert('Please select cash handover to.');
    			
    		}else{
    			
    			document.forms["closeRegForm"].action = "CloseRegister";
        		document.forms["closeRegForm"].submit();
    			
    		}
    	}
    	
    	function submitEditReg(){
    		document.forms["editRegForm"].action = "EditRegister";
    		document.forms["editRegForm"].submit();
    	}
    
    </script>
    
    <!-- Ends -->
    
    <!-- Retrieve Cash deposited  -->
    
    <script type="text/javascript" charset="UTF-8">
	var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function getRegisterDetails() {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);

				for ( var i = 0; i < array.Release.length; i++) {
					
					$('#cashDepositedID').val(array.Release[i].cashDeposited);
					
				}
				
				$('#editRegister').modal('show');
			}
		};
		xmlhttp.open("GET", "GetRegisterDetails", true);
		xmlhttp.send();
	}
	</script>
    
    <!-- Ends -->
    
    
    <!-- Open register check -->
    
    <%
    	int check = userDAOInf.verifyOpenRegister();
    %>
    
    <!-- Ends -->
    
    <!-- Retrieving shift name list -->
    
    <%
    	//List<String> shiftList = billingDAOInf.retrieveShiftName(form.getBloodBankID());
    %>
    
    <!-- Ends -->
    
    <!-- Retrieving receiptionist user List -->
    
    <%
    	HashMap<Integer, String> receptionistList = userDAOInf.retrieveReceiptionistUser(userID);
    
    %>
    
    <!-- Ends -->
    
    <!-- Retrieve Shift time -->
    
    <script type="text/javascript" charset="UTF-8">
	var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function getShiftTime(shiftName) {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);

				for ( var i = 0; i < array.Release.length; i++) {
					
					var array1 = array.Release[i].starTime.split(" ");
					var startAMPM = array1[1];
					var startArray = array1[0].split(":");

					var startMM = "";
					var startHH = "";
					
					if(startArray[1].length == 1){
						startMM = "0"+startArray[1];
					}else{
						startMM = startArray[1];
					}
					
					if(startArray[0].length == 1){
						startHH = "0"+startArray[0];
					}else{
						startHH = startArray[0];
					}
					
					var finalStartTime = startHH+":"+startMM+" "+startAMPM;
					
					var array2 = array.Release[i].endTime.split(" ");
					var endAMPM = array2[1];
					var endArray = array2[0].split(":");
					
					var endMM = "";
					var endHH = "";
					
					if(endArray[1].length == 1){
						endMM = "0"+endArray[1];
					}else{
						endMM = endArray[1];
					}
					
					if(endArray[0].length == 1){
						endHH = "0"+endArray[0];
					}else{
						endHH = endArray[0];
					}
					
					var finalEndTime = endHH+":"+endMM+" "+endAMPM;
					
					$('#startFontID').html(finalStartTime);
					$('#startTimeID').val(array.Release[i].starTime);
					
					$('#endFontID').html(finalEndTime);
					$('#endTimeID').val(array.Release[i].endTime);
					
					$('#timeID').show();
					
				}
			}
		};
		xmlhttp.open("GET", "RetrieveShiftTime?shiftName="
				+ shiftName , true);
		xmlhttp.send();
	}
	</script>
    
    <!-- Ends -->
    
    <!-- Retrieve Register balance  -->
    
    <script type="text/javascript" charset="UTF-8">
	var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function getRegisterBalance() {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);

				for ( var i = 0; i < array.Release.length; i++) {
					
					$('#registerBalanceID').val(array.Release[i].registerBalance);
					$('#totalCashID').val(array.Release[i].shiftCash);
					
				}
				
				$('#closeRegister').modal('show');
			}
		};
		xmlhttp.open("GET", "GetRegisterBalance", true);
		xmlhttp.send();
	}
	</script>
    
    <!-- Ends -->
    
    <!-- Retrieve balance  -->
    
    <script type="text/javascript">
    
    	function openViewBalance(){
    		
			$('#viewBalanceDivID').html("");
			
			$('#noRecFontID').html("");
			
			$('#single_cal3').val("");
			$('#single_cal31').val("");
			
			$('#viewBalance').modal('show');
    		
    	}
    
    </script>
    
    
    <script type="text/javascript">
    	
    function getBalanceDetails1(startDate, endDate){
    	
    	if(startDate == "" && endDate == ""){
			alert('Please select from and to date.');
		}else if(startDate == ""){
			alert('Please select from date.');
		}else if(endDate == ""){
			alert('Please select to date.');
		}else{
			
			getBalanceDetails(startDate, endDate);
		}
    	
    }
    
    </script>
    
    
    <script type="text/javascript" charset="UTF-8">
	var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function getBalanceDetails(startDate, endDate) {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);
				
				var tag = "<table border='1' style='text-align:center; width:100%'>";
				
				tag += "<tr style='font-size:14px;'>"+
				"<td style='padding:5px;'>Date</td>"+
	    		"<td style='padding:5px;'>Shift</td>"+
	    		"<td style='padding:5px;'>Cash Balance</td>"+
	    		"<td style='padding:5px;'>Cash Deposited in bank</td>"+
	    		"</tr>";
	    		
	    		var valCheck = 0;
	    		
	    			for ( var i = 0; i < array.Release.length; i++) {
						
						tag += "<tr style='font-size:14px;'>"+
						"<td style='padding:5px;'>"+array.Release[i].registerDate+"</td>"+
			    		"<td style='padding:5px;'>"+array.Release[i].shift+"</td>"+
			    		"<td style='padding:5px;'>"+array.Release[i].registerBalance+"</td>"+
			    		"<td style='padding:5px;'>"+array.Release[i].cashDeposited+"</td>"+
			    		"</tr>";
			    		
			    		valCheck = array.Release[i].check;
						
					}
	    			
	    			if(valCheck == 0){
	    				$('#noRecFontID').html("No record(s) found.");
	    			}else{
	    				tag += "</table>";
						
						$('#viewBalanceDivID').html(tag);
	    			}

			}
		};
		xmlhttp.open("GET", "GetBalanceDetails?startDate="+startDate+
				"&endDate="+endDate, true);
		xmlhttp.send();
	}
	</script>
    
    <!-- Ends -->
    
    <!-- For session timeout -->
    <%
    
    int sessionTimeout = 30;
    	
    int maxSessionTimeout = sessionTimeout * 60 * 1000;

    
    %>
    <!-- Ends -->
    
    <!-- Lock screen pop up calling after time interval -->
    
    <script type="text/javascript">
    	document.addEventListener('DOMContentLoaded', function() {
        	var interval = <%=maxSessionTimeout%>;
    		setInterval(function(){

    			//Setting blank values to PIN fields
            	$('#lockPIN').val("");

            	//opening security credentials modal
        		$("#lockModal").modal("show");	
				
        	}, interval);
        });
    </script>
    
    <!-- Ends -->
    
    <%
    	String userListEnable = (String) request.getAttribute("searchEnable");
    %>
    
    
     <!-- Shortcut menu Full screen function -->
    
    <script type="text/javascript">
    window.onload = maxWindow;

    function widnwoFullScreen() {
        
    	var docElm = document.documentElement;
		if (docElm.requestFullscreen) {
		    docElm.requestFullscreen();
		}
		else if (docElm.mozRequestFullScreen) {
		    docElm.mozRequestFullScreen();
		}
		else if (docElm.webkitRequestFullScreen) {
		    docElm.webkitRequestFullScreen();
		}
		
	}

	</script> 
    
    <!-- Ends -->
    
    <!-- Show modals function -->
    
    <script type="text/javascript">
    function showLockModal(){
    	$(document).ready(function(){
    		//Setting blank values to PIN fields
        	$('#lockPIN').val("");

        	//opening security credentials modal
    		$("#lockModal").modal("show");
    	});
    }

    function showLogoutModal(){
    	$(document).ready(function(){
    		$("#logoutModal").modal("show");
    	});
    }

    function showSecurityModal(){
    	$(document).ready(function(){
        	//Setting blank values and disabling new Pass and confirm new Pass fields
    		$('#oldPassword').val("");
    		$('#newPassID').val("");
    		$('#confirmNewPassID').val("");
        	$('#newPassID').prop('disabled', true);
        	$('#confirmNewPassID').prop('disabled', true);

        	//Setting blank values to new PIN and confirm new PIN fields
        	$('#newPINID').val("");
    		$('#confirmNewPINID').val("");

    		//removing images by setting innerHTML to blank("")
    		$('#oldPassID').html("");
    		$('#newPassCheckID').html("");
    		$('#confirmPassID').html("");    		
    		$('#confirmPINID').html(""); 

    		//opening security credentials modal
    		$("#securityModal").modal("show");
    	});
    }
    </script>
    
    <!-- ENds -->
    
    <!-- Lock PIN check -->
    
    <script type="text/javascript" charset="UTF-8">
	var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function checkUnlockPIN(lockPIN) {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);

				for ( var i = 0; i < array.Release.length; i++) {
					var check = array.Release[i].PINCheck;
					var errMsg = array.Release[i].ErrMsg;

					if(check == "check"){

						document.getElementById("lockImgID").innerHTML = "";

						unlockModal();
						
					}else{

						document.getElementById("lockImgID").innerHTML = "<img src='images/wrong.png' alt='Incorrect PIN' title='Incorrect PIN' style='height:24px;margin-top: 5px;'>";
						
					}
				}
			}
		};
		xmlhttp.open("GET", "VerifyUnlockPIN?lockPIN="
				+ lockPIN, true);
		xmlhttp.send();
	}
	</script>
    
    <!-- Ends -->
    
    
    <!-- Old Password check -->
    
    <script type="text/javascript" charset="UTF-8">
	var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function verifyOldPass(oldPass) {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);

				for ( var i = 0; i < array.Release.length; i++) {
					var check = array.Release[i].PassCheck;
					var errMsg = array.Release[i].ErrMsg;

					if(check == "check"){

						document.getElementById("oldPassID").innerHTML = "<img src='images/correct.png' alt='Correct Old Password' title='Correct Old Password' style='height:24px;margin-top: 5px;'>";

						//Enabling New password and confirm password input fields
						document.getElementById("newPassID").disabled = false;
						
					}else{

						document.getElementById("oldPassID").innerHTML = "<img src='images/wrong.png' alt='Incorrect Old Password' title='Incorrect Old Password' style='height:24px;margin-top: 5px;'>";
						
					}
				}
			}
		};
		xmlhttp.open("GET", "VerifyOldPass?oldPass="
				+ oldPass, true);
		xmlhttp.send();
	}
	</script>
    
    <!-- Ends -->
    
     <!-- New Password check -->
    
    <script type="text/javascript" charset="UTF-8">
	var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function newPassCheck(newPass) {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);

				for ( var i = 0; i < array.Release.length; i++) {
					var check = array.Release[i].newPassCheck;
					var errMsg = array.Release[i].ErrMsg;

					if(check == "true"){

						document.getElementById("newPassCheckID").innerHTML = "<img src='images/correct.png' alt='Valid new password' title='New password is valid' style='height:24px;margin-top: 5px;'>";
						document.getElementById("confirmNewPassID").disabled = false;
						
					}else{

						document.getElementById("newPassCheckID").innerHTML = "<img src='images/wrong.png' alt='Invalid new password' title='Incorrect Old Password' style='height:24px;margin-top: 5px;'>";
						
					}
				}
			}
		};
		xmlhttp.open("GET", "NewPassCheck?newPass="
				+ newPass, true);
		xmlhttp.send();
	}
	</script>
    
    <!-- Ends -->
    
    
    <!-- Submit Security Credentials -->
    
    <script type="text/javascript" charset="UTF-8">
	var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function updateSecurityCredentials(newPass, newPIN) {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);

				for ( var i = 0; i < array.Release.length; i++) {
					var check = array.Release[i].PassCheck;
					var errMsg = array.Release[i].ErrMsg;

					if(check == "check"){

						hideSecCredModal();
						
					}else{

						document.getElementById("oldPassID").innerHTML = "<img src='images/wrong.png' alt='Incorrect Old Password' title='Incorrect Old Password' style='height:24px;margin-top: 5px;'>";
						
					}
				}
			}
		};
		xmlhttp.open("GET", "UpdateSecurityCredentials?newPass="
				+ newPass + "&newPIN=" + newPIN , true);
		xmlhttp.send();
	}
	</script>
    
    <!-- Ends -->
    
    
    <!-- Confirm new pass and Confirm new PIN check -->
    
    <script type="text/javascript">
    	function checkConfirmNewPass(confirmNewPassID,newPassID){

			if(confirmNewPassID == newPassID){

				document.getElementById("confirmPassID").innerHTML = "<img src='images/correct.png' alt='Confirm New Password matched' title='Confirm New Password matched' style='height:24px;margin-top: 5px;'>";

				$('#securityBtnID').prop('disabled', false);
				
			}else{

				$('#securityBtnID').prop('disabled', true);

				document.getElementById("confirmPassID").innerHTML = "<img src='images/wrong.png' alt='Confirm Password didn't match' title='Confirm New Password must be same as New Password' style='height:24px;margin-top: 5px;'>";

			}
    	}

    	function checkConfirmNewPIN(confirmNewPINID,newPINID){

			if(confirmNewPINID == newPINID){

				document.getElementById("confirmPINID").innerHTML = "<img src='images/correct.png' alt='Confirm New PIN matched' title='Confirm New PIN matched' style='height:24px;margin-top: 5px;'>";

				$('#securityBtnID').prop('disabled', false);
				
			}else{

				$('#securityBtnID').prop('disabled', true);

				document.getElementById("confirmPINID").innerHTML = "<img src='images/wrong.png' alt='Confirm PIN didn't match' title='Confirm New PIN must be same as New PIN' style='height:24px;margin-top: 5px;'>";

			}
    	}
    </script>
    
    <!-- Ends -->
    
    
    <!-- Hide Lock Modal and Security Credentials modal -->
    
    <script type="text/javascript">
    	function unlockModal(){
			$(document).ready(function(){
				
				$('#lockButtonID').click();
			});
    	}

    	function hideSecCredModal(){
			$(document).ready(function(){
				
				$('#secCredCancelID').click();
			});
    	}
    </script>
    
    <!-- Ends -->
    
    
    <!-- Register details while closing it -->
    
    <script type="text/javascript">
    
    var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	function retrieveCloseRegisterDetails() {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);

				for ( var i = 0; i < array.Release.length; i++) {
					
					$("#expectedEndCashID").val(array.Release[i].expectedCash);
					$("#bloodBankEndBalanceID").val(array.Release[i].bloodBankBalance);
					$("#otherProductEndBalanceID").val(array.Release[i].otherProductBalance);
					$("#bankDepositEndCashID").val(array.Release[i].totalCashDeposited);
					$("#cashAdjustedEndCashID").val(array.Release[i].totalCashAdjusted);
					$("#cashHandoverEndCashID").val(array.Release[i].cashHandover);
					$("#registerID").val(array.Release[i].registerID);
					
					$("#closeRegisterModal").modal("show");
					
				}
				
			}
		};
		xmlhttp.open("GET", "RetrieveCloseRegisterDetails?", true);
		xmlhttp.send();
	}
    
    </script>
    
    <!-- Ends -->
	
	<!-- Show cash adjustment modal -->
	
	<script type="text/javascript">
	
		function getCashAdjustmentModal(){
			
			$("#headingThree").addClass("in");
			
			getCashAdjustmentDetails();
		}
	
	</script>
	
	<!-- Ends -->
	
	<!-- Cash matching error calculation function -->
    
    <script type="text/javascript">
    
    	function calculateCashMatchingError(expectedCash, actualCash){
    		
    		if(expectedCash == ""){
    			expectedCash = 0;
    		}
    		
    		if(actualCash == ""){
    			actualCash = 0;
    		}
    		
    		var cashMatchingErrAmt = parseFloat(parseFloat(expectedCash) - parseFloat(actualCash));
    		
    		$("#cashMatchingEndErrorID").val(cashMatchingErrAmt);
    		
    		$("#cashMatchingEndErrorID").attr("readonly", true);
    		
			$("#cashMatchingErrorID").val(cashMatchingErrAmt);
    		
    		$("#cashMatchingErrorID").attr("readonly", true);
    		
    	}
    
    </script>
    
    <!-- Ends -->
    
    <!-- Retrieve Cash deposited  -->
    
    <script type="text/javascript" charset="UTF-8">
	var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function getBankDepositeDetails() {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);
				
				var check = 0;
				
				var tableTag = "<table border='1' style='width: 100%;'><thead><tr>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Sr. No.</th>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Cash Deposited</th>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Comment</th>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Regiser ID</th>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Action</th>"
							  +"</tr></thead><tbody>";

				for ( var i = 0; i < array.Release.length; i++) {
					
					check = array.Release[i].check;
					
					tableTag += "<tr>"
							   +"<td style='text-align: center; padding: 5px;'>"+check+"</td>"
							   +"<td style='text-align: center; padding: 5px;'>"+array.Release[i].cashDeposited+"</td>"
							   +"<td style='text-align: center; padding: 5px;'>"+array.Release[i].comment+"</td>"
							   +"<td style='text-align: center; padding: 5px;'>"+array.Release[i].registerID+"</td>"
							   +"<td style='text-align: center; padding: 5px;'><img src='images/editBill.png' title='Edit' style='height:20px;cursor: pointer;' alt='Edit' onclick='getEditBankDeposit(\""+array.Release[i].bankDepID+"\");'>"
							   +"&nbsp;<img src='images/delete_icon_1.png' alt='Delete' title='Edit' style='height:20px;cursor: pointer;' onclick='deleteBankDeposit(\""+array.Release[i].bankDepID+"\");'></td>"
							   +"</tr>";
					
				}
				
				tableTag += "</tbody></table>";
				
				if(check == 0){
					$("#depositFontID").html("No bank deposit details found. Please add new bank deposit details.");
					$("#depositDivID").html("");
				}else{
					$("#depositDivID").html(tableTag);
					$("#depositFontID").html("");
				}
				
				var btnTag = "<input type='button' class='btn btn-success' onclick='addCashDeposit(cashDepositedID.value, commentID.value);' value='Add'>";
				$("#cashDepositBtnID").html(btnTag);
				$("#cashDepositedID").val("");
				$("#commentID").val("");
				
				getCashAdjustmentDetails();

			}
		};
		xmlhttp.open("GET", "GetBankDepositeDetails", true);
		xmlhttp.send();
	}
	
	//delete cash deposit details function
	function deleteBankDeposit(cashDepID){
		
		if(confirm("Are you cure you want to delete this value?")){
			
			confirmDeleteBankDeposit(cashDepID);
			
		}
		
	}
	</script>
	
	
	<script type="text/javascript" charset="UTF-8">
	var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function getBankDepositeDetails1() {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);
				
				var check = 0;
				
				var tableTag = "<table border='1' style='width: 100%;'><thead><tr>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Sr. No.</th>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Cash Deposited</th>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Comment</th>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Regiser ID</th>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Action</th>"
							  +"</tr></thead><tbody>";

				for ( var i = 0; i < array.Release.length; i++) {
					
					check = array.Release[i].check;
					
					tableTag += "<tr>"
							   +"<td style='text-align: center; padding: 5px;'>"+check+"</td>"
							   +"<td style='text-align: center; padding: 5px;'>"+array.Release[i].cashDeposited+"</td>"
							   +"<td style='text-align: center; padding: 5px;'>"+array.Release[i].comment+"</td>"
							   +"<td style='text-align: center; padding: 5px;'>"+array.Release[i].registerID+"</td>"
							   +"<td style='text-align: center; padding: 5px;'><img src='images/editBill.png' title='Edit' style='height:20px;cursor: pointer;' alt='Edit' onclick='getEditBankDeposit(\""+array.Release[i].bankDepID+"\");'>"
							   +"&nbsp;<img src='images/delete_icon_1.png' alt='Delete' title='Delete' style='height:20px;cursor: pointer;' onclick='deleteBankDeposit(\""+array.Release[i].bankDepID+"\");'></td>"
							   +"</tr>";
					
				}
				
				tableTag += "</tbody></table>";
				
				if(check == 0){
					$("#depositFontID").html("No bank deposit details found. Please add new bank deposit details.");
					$("#depositDivID").html("");
				}else{
					$("#depositDivID").html(tableTag);
					$("#depositFontID").html("");
				}
				
				var btnTag = "<input type='button' class='btn btn-success' onclick='addCashDeposit(cashDepositedID.value, commentID.value);' value='Add'>";
				$("#cashDepositBtnID").html(btnTag);
				$("#cashDepositedID").val("");
				$("#commentID").val("");

			}
		};
		xmlhttp.open("GET", "GetBankDepositeDetails", true);
		xmlhttp.send();
	}

	</script>
    
    <!-- Ends -->
    
    
    
    <!-- Add cash deposit details -->
    
    <script type="text/javascript">
    
    var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function addCashDeposit(cashDeposited, comment) {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);
				
				var check = "error";

				for ( var i = 0; i < array.Release.length; i++) {
					
					check = array.Release[i].msg;
					
				}
				
				if(check == "success"){
					
					getBankDepositeDetails1();
					
					alert("Added successfully.");
					
				}else{
					
					alert("Failed to add cash deposit deails. Please check server logs for more details.");
					
					getBankDepositeDetails1();
					
				}

			}
		};
		xmlhttp.open("GET", "AddCashDeposit?cashDeposited="+cashDeposited+"&comment="+comment, true);
		xmlhttp.send();
	}
    
    </script>
    
    <!-- Ends -->
    
    
    
    <!-- Edit cash deposit details -->
    
    <script type="text/javascript">
    
    var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function editCashDeposit(cashDepID, cashDeposited, comment) {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);
				
				var check = "error";

				for ( var i = 0; i < array.Release.length; i++) {
					
					check = array.Release[i].msg;
					
				}
				
				if(check == "success"){
					
					getBankDepositeDetails1();
					
					alert("Updated successfully.");
					
				}else{
					
					alert("Failed to update cash deposit deails. Please check server logs for more details.");
					
					getBankDepositeDetails1();
					
				}

			}
		};
		xmlhttp.open("GET", "EditCashDeposit?cashDeposited="+cashDeposited+"&comment="+comment+"&cashDepID="+cashDepID, true);
		xmlhttp.send();
	}
    
    </script>
    
    <!-- Ends -->
    
    
    
    <!-- Delete cash deposit details -->
    
    <script type="text/javascript">
    
    var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function confirmDeleteBankDeposit(cashDepID) {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);
				
				var check = "error";

				for ( var i = 0; i < array.Release.length; i++) {
					
					check = array.Release[i].msg;
					
				}
				
				if(check == "success"){
					
					getBankDepositeDetails1();
					
					alert("Deleted successfully.");
					
				}else{
					
					alert("Failed to delete cash deposit deails. Please check server logs for more details.");
					
					getBankDepositeDetails1();
					
				}

			}
		};
		xmlhttp.open("GET", "ConfirmDeleteBankDeposit?cashDepID="+cashDepID, true);
		xmlhttp.send();
	}
    
    </script>
    
    <!-- Ends -->
    
    
    
    <!-- Render Edit cash deposit details -->
    
    <script type="text/javascript">
    
    var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function getEditBankDeposit(cashDepID) {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);
				
				var id = 0;
				var cashDep = 0;
				var comment = "";

				for ( var i = 0; i < array.Release.length; i++) {
					
					id = array.Release[i].cashDepID;
					cashDep = array.Release[i].cashDeposited;
					comment = array.Release[i].comment;
					
				}
				
				$("#cashDespIDID").val(id);
				$("#cashDepositedID").val(cashDep);
				$("#commentID").val(comment);
				
				var btnTag = "<input type='button' class='btn btn-success' onclick='editCashDeposit(\""+id+"\",cashDepositedID.value, commentID.value);' value='Update'>";
				$("#cashDepositBtnID").html(btnTag);

			}
		};
		xmlhttp.open("GET", "GetEditBankDeposit?cashDepID="+cashDepID, true);
		xmlhttp.send();
	}
    
    </script>
    
    <!-- Ends -->
    
    
    <!-- Retrieve Cash adjustment details  -->
    
    <script type="text/javascript" charset="UTF-8">
	var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function getCashAdjustmentDetails() {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);
				
				var check = 0;
				
				var tableTag = "<table border='1' style='width: 100%;'><thead><tr>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Sr. No.</th>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Cash Adjusted</th>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Reason</th>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Regiser ID</th>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Action</th>"
							  +"</tr></thead><tbody>";

				for ( var i = 0; i < array.Release.length; i++) {
					
					check = array.Release[i].check;
					
					if(check == 0){
						
						tableTag = "";
						
					}else{
						
						tableTag += "<tr>"
							   +"<td style='text-align: center; padding: 5px;'>"+check+"</td>"
							   +"<td style='text-align: center; padding: 5px;'>"+array.Release[i].cashAdjusted+"</td>"
							   +"<td style='text-align: center; padding: 5px;'>"+array.Release[i].reason+"</td>"
							   +"<td style='text-align: center; padding: 5px;'>"+array.Release[i].resgiterID+"</td>"
							   +"<td style='text-align: center; padding: 5px;'><img src='images/editBill.png' title='Edit' style='height:20px;cursor: pointer;' alt='Edit' onclick='getEditCashAdjustment(\""+array.Release[i].cashAdjID+"\");'>"
							   +"&nbsp;<img src='images/delete_icon_1.png' alt='Delete'title='Delete'  style='height:20px;cursor: pointer;' onclick='deleteCashAdjustment(\""+array.Release[i].cashAdjID+"\");'></td>"
							   +"</tr>";
						
					}
					
				}
				
				tableTag += "</tbody></table>";
				
				if(check == 0){
					$("#adjustmentFontID").html("No cash adjustment details found. Please add new cash adjustment details.");
					$("#adjustmentDivID").html("");
				}else{
					$("#adjustmentDivID").html(tableTag);
					$("#adjustmentFontID").html("");
				}
				
				var btnTag = "<input type='button' class='btn btn-success' onclick='addCashAdjustment(cashAdjustedID.value, reasonID.value);' value='Add'>";
				$("#cashAdjustmentBtnID").html(btnTag);
				
				$("#cashAdjustedID").val("");
				$("#reasonID").val("");
				
				$("#updateRegister").modal('show');

			}
		};
		xmlhttp.open("GET", "GetCashAdjustmentDetails", true);
		xmlhttp.send();
	}
	
	
	//delete cash adjustment details function
	function deleteCashAdjustment(cashAdjID){
		
		if(confirm("Are you cure you want to delete this value?")){
			
			confirmDeleteCashAdjustment(cashAdjID);
			
		}
		
	}
	
	</script>
	
	
	<script type="text/javascript" charset="UTF-8">
	var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function getCashAdjustmentDetails1() {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);
				
				var check = 0;
				
				var tableTag = "<table border='1' style='width: 100%;'><thead><tr>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Sr. No.</th>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Cash Adjusted</th>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Reason</th>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Regiser ID</th>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Action</th>"
							  +"</tr></thead><tbody>";

				for ( var i = 0; i < array.Release.length; i++) {
					
					check = array.Release[i].check;
					
					if(check == 0){
						
						tableTag = "";
						
					}else{
						
						tableTag += "<tr>"
							   +"<td style='text-align: center; padding: 5px;'>"+check+"</td>"
							   +"<td style='text-align: center; padding: 5px;'>"+array.Release[i].cashAdjusted+"</td>"
							   +"<td style='text-align: center; padding: 5px;'>"+array.Release[i].reason+"</td>"
							   +"<td style='text-align: center; padding: 5px;'>"+array.Release[i].resgiterID+"</td>"
							   +"<td style='text-align: center; padding: 5px;'><img src='images/editBill.png' title='Edit' style='height:20px;cursor: pointer;' alt='Edit' onclick='getEditCashAdjustment(\""+array.Release[i].cashAdjID+"\");'>"
							   +"&nbsp;<img src='images/delete_icon_1.png' alt='Delete' title='Delete' style='height:20px;cursor: pointer;' onclick='deleteCashAdjustment(\""+array.Release[i].cashAdjID+"\");'></td>"
							   +"</tr>";
						
					}
					
				}
				
				tableTag += "</tbody></table>";
				
				if(check == 0){
					$("#adjustmentFontID").html("No cash adjustment details found. Please add new cash adjustment details.");
					$("#adjustmentDivID").html("");
				}else{
					$("#adjustmentDivID").html(tableTag);
					$("#adjustmentFontID").html("");
				}
				
				var btnTag = "<input type='button' class='btn btn-success' onclick='addCashAdjustment(cashAdjustedID.value, reasonID.value);' value='Add'>";
				$("#cashAdjustmentBtnID").html(btnTag);
				
				$("#cashAdjustedID").val("");
				$("#reasonID").val("");

			}
		};
		xmlhttp.open("GET", "GetCashAdjustmentDetails", true);
		xmlhttp.send();
	}
	
	</script>
	
    
    <!-- Ends -->
    
    
    <!-- Add cash Adjusted details -->
    
    <script type="text/javascript">
    
    var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function addCashAdjustment(cashAdjusted, reason) {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);
				
				var check = "error";

				for ( var i = 0; i < array.Release.length; i++) {
					
					check = array.Release[i].msg;
					
				}
				
				if(check == "success"){
					
					getCashAdjustmentDetails1();
					
					alert("Added successfully.");
					
				}else{
					
					alert("Failed to add cash adjustment deails. Please check server logs for more details.");
					
					getCashAdjustmentDetails1();
					
				}

			}
		};
		xmlhttp.open("GET", "AddCashAdjustment?cashAdjusted="+cashAdjusted+"&reason="+reason, true);
		xmlhttp.send();
	}
    
    </script>
    
    <!-- Ends -->
    
    
    <!-- Edit cash Adjusted details -->
    
    <script type="text/javascript">
    
    var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function editCashAdjustment(cashAdjID, cashAdjusted, reason) {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);
				
				var check = "error";

				for ( var i = 0; i < array.Release.length; i++) {
					
					check = array.Release[i].msg;
					
				}
				
				if(check == "success"){
					
					getCashAdjustmentDetails1();
					
					alert("Updated successfully.");
					
				}else{
					
					alert("Failed to update cash adjustment deails. Please check server logs for more details.");
					
					getCashAdjustmentDetails1();
					
				}

			}
		};
		xmlhttp.open("GET", "EditCashAdjustment?cashAdjusted="+cashAdjusted+"&reason="+reason+"&cashAdjID="+cashAdjID, true);
		xmlhttp.send();
	}
    
    </script>
    
    <!-- Ends -->
    
    
    
    <!-- Delete cash Adjusted details -->
    
    <script type="text/javascript">
    
    var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function confirmDeleteCashAdjustment(cashAdjID) {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);
				
				var check = "error";

				for ( var i = 0; i < array.Release.length; i++) {
					
					check = array.Release[i].msg;
					
				}
				
				if(check == "success"){
					
					getCashAdjustmentDetails1();
					
					alert("Deleted successfully.");
					
				}else{
					
					alert("Failed to delete cash adjustment deails. Please check server logs for more details.");
					
					getCashAdjustmentDetails1();
					
				}

			}
		};
		xmlhttp.open("GET", "ConfirmDeleteCashAdjustment?cashAdjID="+cashAdjID, true);
		xmlhttp.send();
	}
    
    </script>
    
    <!-- Ends -->
    
    
    <!-- Render Edit cash adjsutment details -->
    
    <script type="text/javascript">
    
    var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function getEditCashAdjustment(cashAdjID) {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);
				
				var id = 0;
				var cashAdj = 0;
				var reason = "";

				for ( var i = 0; i < array.Release.length; i++) {
					
					id = array.Release[i].cashAdjID;
					cashAdj = array.Release[i].cashAdjusted;
					reason = array.Release[i].reason;
					
				}
				
				$("#cashAdjIDID").val(id);
				$("#cashAdjustedID").val(cashAdj);
				$("#reasonID").val(reason);
				
				var btnTag = "<input type='button' class='btn btn-success' onclick='editCashAdjustment(\""+id+"\",cashAdjustedID.value, reasonID.value);' value='Update'>";
				$("#cashAdjustmentBtnID").html(btnTag);

			}
		};
		xmlhttp.open("GET", "GetEditCashAdjustment?cashAdjID="+cashAdjID, true);
		xmlhttp.send();
	}
    
    </script>
    
    <!-- Ends -->
    
    
    <%
    
    	Date date = new Date();
    
    	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
    
    %>
    
    
    
  </head>

  <body class="nav-md">
  
  
  <!-- Open register Alter modal -->
  
  <div id="openRegisterAlterID" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
      
        <div class="modal-body">
          
          <center>
          	<h4 style="color:black;">Register is closed. Please ask receptionist to start the register.</h4>
          </center>

        </div>
        <div class="modal-footer">
        <center>
          <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
        </center>
        </div>

      </div>
    </div>
  </div>
  
  <div id="openRegisterAlterID1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
      
        <div class="modal-body">
          
          <center>
          	<h4 style="color:black;">Register is closed. Please ask receptionist to start the register.</h4>
          </center>

        </div>
        <div class="modal-footer">
        <center>
          <a href="LogoutUser?userID=<%= userID %>"><button type="button" class="btn btn-success">OK</button></a>
        </center>
        </div>

      </div>
    </div>
  </div>
  
  <!-- Ends -->
  
  
  <!-- close register modal -->
  
  <div id="closeRegisterModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
      
      <div class="modal-header" align="center" style="color:black;">
          <h4 class="modal-title" id="myModalLabel">CLOSE REGISTER</h4>
      </div>
      
        <div class="modal-body">
          
          <form action="CloseRegister" method="POST" id="shiftCloseForm" name="shiftCloseForm">
          
          <input type="hidden" name="cashHandover" id="cashHandoverEndCashID">
          <input type="hidden" name="cashAdjusted" id="cashAdjustedEndCashID">
          <input type="hidden" name="cashDeposited" id="bankDepositEndCashID">
          <input type="hidden" name="registerID" id="registerID">
          <input type="hidden" name="userID" value="<%=userID%>">
          
          <div class="row" style="margin-top:15px;">
          
          	  <div class="col-md-6">
	          	<font style="font-size: 14px;">Date & End time:</font>
			  </div>
          
	          <div class="col-md-6">
	          	<input type="text" name="registerDate" readonly="readonly" class="form-control" value="<%=dateFormat.format(date)%>" id="dateEndTimeID">
			  </div>
          </div>
          
          <div class="row" style="margin-top:15px;">
          	  <div class="col-md-6">
	          	<font style="font-size: 14px;">Expected Cash Balance:</font>
			  </div>
          
	          <div class="col-md-6">
	          	<input type="number" class="form-control" readonly="readonly" id="expectedEndCashID" name="expectedCash">
			  </div>
			  
			  <div class="row" style="margin-left: 15px;">
			  
			  	  <div class="col-md-6" style="margin-top:15px;">
		          	<font style="font-size: 14px;">Blood Bank Balance:</font>
				  </div>
	          
		          <div class="col-md-6" style="margin-top:15px;">
		          	<input type="number" class="form-control" style="width: 96%;" readonly="readonly" id="bloodBankEndBalanceID" name="bloodBankBalance">
				  </div>
			  
			  </div>
			  
			  <div class="row" style="margin-left: 15px;">
			  
			  	  <div class="col-md-6" style="margin-top:15px;">
		          	<font style="font-size: 14px;">Other Product Balance:</font>
				  </div>
	          
		          <div class="col-md-6" style="margin-top:15px;">
		          	<input type="number" class="form-control" style="width: 96%;" readonly="readonly" id="otherProductEndBalanceID" name="otherProductBalance">
				  </div>
			  
			  </div>
			  
          </div>
          
          <div class="row" style="margin-top:15px;">
          
          	  <div class="col-md-6">
	          	<font style="font-size: 14px;">Actual Cash Balance:</font>
			  </div>
          
	          <div class="col-md-6">
	          	<input type="number" class="form-control" id="actualCashEndBalanceID" onkeyup="calculateCashMatchingError(expectedEndCashID.value, actualCashEndBalanceID.value);" name="actualCash">
			  </div>
          </div>
          
          <div class="row" style="margin-top:15px;">
          
          	  <div class="col-md-6">
	          	<font style="font-size: 14px;">Cash matching error:</font>
			  </div>
          
	          <div class="col-md-3">
	          	<input type="number" class="form-control" readonly="readonly" id="cashMatchingEndErrorID" name="cashMatchingError">
			  </div>
			  <div class="col-md-3">
	          	<button class="btn btn-primary" type="button" onclick="getCashAdjustmentModal();">Cash Adjustment</button>
			  </div>
			  
          </div>

        </div>
        <div class="modal-footer">
        <center>
          <button type="submit" class="btn btn-success">End Register</button>
        </center>
        </form>
        </div>

      </div>
    </div>
  </div>
  
  <!-- Ends -->
  
  
  <!-- Edit Register modal -->
  
  <div id="updateRegister" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      <div class="modal-header" align="center" style="color:black;">
          <h4 class="modal-title" id="myModalLabel">UPDATE REGISTER</h4>
      </div>
      
        <div class="modal-body">
          
          <!-- start accordion -->
                    <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                      <div class="panel" id="cashDepositCollapseID">
                        <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                          <h4 class="panel-title">Cash Deposit</h4>
                        </a>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                          <div class="panel-body">
                           	
                           	<div class="row" style="margin-top: 15px;">
                           		
                           		<div class="col-md-6 col-sm-12 col-xs-12" id="" align="center" style="padding-top: 35px;">
                           			<font id="depositFontID" style="color: red; font-size: 14px;"></font>
                           			<div id="depositDivID"></div>
                           		</div>
                           		
                           		<div class="col-md-6 col-sm-12 col-xs-12">
                           			
                           			<center>
                           			<font style="font-size: 14px;font-weight: bold;">Add Cash Deposit</font>
                           			</center>
                           			
                           			<div class="row" style="margin-top:15px;">
                           			
                           				<input type="hidden" id="cashDespIDID" value="">
                           			
                           				<div class="col-md-4 col-sm-6 col-xs-6">
                           					<font style="font-size: 14px;">Cash Deposited</font>
                           				</div>
                           				
                           				<div class="col-md-8 col-sm-6 col-xs-6">
                           					<input type="number" name="" id="cashDepositedID" class="form-control">
                           				</div>
                           			
                           			</div>
                           			
                           			<div class="row" style="margin-top:15px;">
                           			
                           				<div class="col-md-4 col-sm-6 col-xs-6">
                           					<font style="font-size: 14px;">Comment</font>
                           				</div>
                           				
                           				<div class="col-md-8 col-sm-6 col-xs-6">
                           					<input type="text" name="" id="commentID" class="form-control">
                           				</div>
                           			
                           			</div>
                           			
                           			<div class="row" style="margin-top:15px;">
                           			
                           				<div class="col-md-3 col-sm-3 col-xs-6"></div>
                           				<div class="col-md-3 col-sm-3 col-xs-6"></div>
                           			
                           				<div class="col-md-3 col-sm-3 col-xs-6" align="right">
                           					<input type="reset" class="btn btn-default" value="Reset">
                           				</div>
                           				
                           				<div class="col-md-3 col-sm-3 col-xs-6" id="cashDepositBtnID">
                           					<input type="button" class="btn btn-success" value="Add" onclick="addCashDeposit(cashDepositedID.value, commentID.value);">
                           				</div> 
                           			
                           			</div>
                           			
                           		</div>
                           		
                           	</div>
                           	
                          </div>
                        </div>
                      </div>
                      <div class="panel">
                        <a class="panel-heading collapsed" role="tab" id="headingThree" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                          <h4 class="panel-title">Cash Adjustment</h4>
                        </a>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                          <div class="panel-body">
                           	<div class="row" style="margin-top: 15px;">
                           		
                           		<div class="col-md-6 col-sm-12 col-xs-12"  align="center"  style="padding-top: 35px;">
                           			<font id="adjustmentFontID" style="color: red; font-size: 14px;"></font>
                           			<div id="adjustmentDivID"></div>
                           		</div>
                           		
                           		<div class="col-md-6 col-sm-12 col-xs-12">
                           			
                           			<center>
                           			<font style="font-size: 14px;font-weight: bold;">Add Cash Adjustment</font>
                           			</center>
                           			
                           			<div class="row" style="margin-top:15px;">
                           			
                           			<input type="hidden" id="cashAdjIDID" value="">
                           			
                           				<div class="col-md-4 col-sm-6 col-xs-6">
                           					<font style="font-size: 14px;">Cash Adjusted</font>
                           				</div>
                           				
                           				<div class="col-md-8 col-sm-6 col-xs-6">
                           					<input type="number" name="" id="cashAdjustedID" class="form-control">
                           				</div>
                           			
                           			</div>
                           			
                           			<div class="row" style="margin-top:15px;">
                           			
                           				<div class="col-md-4 col-sm-6 col-xs-6">
                           					<font style="font-size: 14px;">Reason</font>
                           				</div>
                           				
                           				<div class="col-md-8 col-sm-6 col-xs-6">
                           					<input type="text" name="" id="reasonID" class="form-control">
                           				</div>
                           			
                           			</div>
                           			
                           			<div class="row" style="margin-top:15px;">
                           			
                           				<div class="col-md-3 col-sm-3 col-xs-6"></div>
                           				<div class="col-md-3 col-sm-3 col-xs-6"></div>
                           			
                           				<div class="col-md-3 col-sm-3 col-xs-6" align="right">
                           					<input type="reset" class="btn btn-default" value="Reset">
                           				</div>
                           				
                           				<div class="col-md-3 col-sm-3 col-xs-6" id="cashAdjustmentBtnID">
                           					<input type="button" class="btn btn-success" value="Add"  onclick="addCashAdjustment(cashAdjustedID.value, reasonID.value);">
                           				</div> 
                           			
                           			</div>
                           			
                           		</div>
                           		
                           	</div>
                          </div>
                        </div>
                      </div>
                    </div>
		<!-- end of accordion -->

        </div>
        <div class="modal-footer">
	        <center>
	          <button type="button" data-dismiss="modal" class="btn btn-primary">Close</button>
	        </center>
        </div>

      </div>
    </div>
  </div>
  
  <!-- Ends -->
  
  
  <!-- View Balance modal -->
  
  <div id="viewBalance" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
       <div class="modal-header" align="center" style="color:black;">
          <h4 class="modal-title" id="myModalLabel">VIEW BALANCE</h4>
      </div>
      
        <div class="modal-body">
          
          <div class="row" style="margin-top:15px;">
	          <div class="col-md-4">
				<input type="text" class="form-control" name="" id="single_cal3" placeholder="From date" aria-describedby="inputSuccess2Status3">
			  </div>
			  <div class="col-md-4">
				<input type="text" class="form-control" name="" id="single_cal31" placeholder="To date" aria-describedby="inputSuccess2Status3">
			  </div>
			  <div class="col-md-4">
				<button type="button" class="btn btn-success" onclick="getBalanceDetails1(single_cal3.value, single_cal31.value);">View Balance</button>
			  </div>
          </div>
          
          <center>
	          <font style="color:red; font-size: 16px;" id="noRecFontID"></font>
	        </center>
          
          <div class="row" id="viewBalanceDivID" style="padding:5px;margin-top:15px;">
         	
         </div>

        </div>
        <div class="modal-footer">
        	<center>
	          <button type="button" data-dismiss="modal" class="btn btn-primary">Cancel</button>
	        </center>
        </div>

      </div>
    </div>
  </div>
  
  <!-- Ends -->
  
  <!-- On Search Select modal -->
  
  <div id="onSearchSelectID" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
      
        <div class="modal-body">
          
          <center>
          	<h4 style="color:black;">Please select 'Search On' criteria.</h4>
          </center>

        </div>
        <div class="modal-footer">
        <center>
          <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
        </center>
        </div>

      </div>
    </div>
  </div>
  
  <!-- Ends -->
  
  <!-- Logout modal -->
  
  <div id="logoutModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
      <div class="modal-header" align="center" style="color:black;">
          <h4 class="modal-title" id="myModalLabel">LOGOUT</h4>
      </div>
      
        <div class="modal-body">
          
          <center>
          	<h4 style="color:black;">You will lose any unsaved data. Are you sure you want to log out?</h4>
          </center>

        </div>
        <div class="modal-footer">
        <center>
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
          
          	<%
          	if(form.getUserRole().equals("administrator") || form.getUserRole().equals("courier")){
          %>
            
            <a href="LogoutUser?userID=<%= userID %>"><button type="button" class="btn btn-success">OK</button></a>
            
          <%
          	}else{
          %>
          
            <a href="javascript:retrieveCloseRegisterDetails();"><button type="submit" class="btn btn-success">OK</button></a>
            
          <%
          	}
          %>
        </center>
        </div>

      </div>
    </div>
  </div>
  
  <!-- Ends -->
  
  <!-- Lock modal -->
  
  <div id="lockModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
       <div class="modal-header" align="center" style="color:black;">
          <h4 class="modal-title" id="myModalLabel">SCREEN LOCKED</h4>
      </div>
      
        <div class="modal-body">
          
          <center style="margin-bottom: 20px;">
          	<font style="color:black;font-size: 16px;">Enter your PIN to onlock the screen</font>
          </center>
          
          <div class="row">
	          <div class="col-md-10 col-sm-10 col-xs-8">
	          	<input type="password" name="lockPIN" id="lockPIN" onkeyup="checkUnlockPIN(lockPIN.value);" class="form-control" placeholder="Enter your PIN here">
	          </div>
	          <div class="col-md-2 col-sm-2 col-xs-4" id="lockImgID" style="margin-left:-15px;">
	          	
	          </div>
          </div>

        </div>
        <div class="modal-footer">
        <center>
        
        <%
          	if(form.getUserRole().equals("administrator") || form.getUserRole().equals("courier")){
          %>
            
            <a href="LogoutUser?userID=<%= userID %>"><button type="button" class="btn btn-primary">Logout</button></a>
            
          <%
          	}else{
          %>
          
            <a href="javascript:retrieveCloseRegisterDetails();"><button type="submit" class="btn btn-primary">Logout</button></a>
            
          <%
          	}
          %>
          <button type="button" id="lockButtonID" class="btn btn-primary" data-dismiss="modal" style="display: none;"></button>
        </center>
        </div>

      </div>
    </div>
  </div>
  
  <!-- Ends -->
  
  <!-- Security Credential modal -->
  
  <div id="securityModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
      <div class="modal-header" align="center" style="color:black;">
          <h4 class="modal-title" id="myModalLabel">CHANGE SECURITY CREDENTIALS</h4>
      </div>
      
        <div class="modal-body">
          
          <!-- start accordion -->
                    <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                      <div class="panel">
                        <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                          <h4 class="panel-title">Change Password</h4>
                        </a>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                          <div class="panel-body">
                           <div class="row" style="margin-bottom:15px;">
                           		<div class="col-md-4">
                           			Old Password
                           		</div>
                           		<div class="col-md-7">
                           			<input type="password" name="oldPassword" id="oldPassword" class="form-control" onkeyup="verifyOldPass(oldPassword.value);">
                           		</div>
                           		<div class="col-md-1" id="oldPassID">
                           			
                           		</div>
                           	</div>
                           		
                           	<div class="row" style="margin-bottom:15px;">
                           		<div class="col-md-4">
                           			New Password
                           		</div>
                           		<div class="col-md-7">
                           			<input type="password" name="newPassword" id="newPassID" disabled="disabled" onkeyup="newPassCheck(newPassID.value);" class="form-control">
                           		</div>
                           		<div class="col-md-1" id="newPassCheckID">
                           			
                           		</div>
                           	</div>
                           		
                           	<div class="row" style="margin-bottom:15px;">
                           		<div class="col-md-4">
                           			Confirm New Password
                           		</div>
                           		<div class="col-md-7">
                           			<input type="password" name="confirmNewPassword" id="confirmNewPassID" disabled="disabled" onkeyup="checkConfirmNewPass(confirmNewPassID.value,newPassID.value);" class="form-control">
                           		</div>
                           		<div class="col-md-1" id="confirmPassID">
                           			
                           		</div>
                           </div>
                          </div>
                        </div>
                      </div>
                      <div class="panel">
                        <a class="panel-heading collapsed" role="tab" id="headingThree" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                          <h4 class="panel-title">Change PIN</h4>
                        </a>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                          <div class="panel-body">
                           	<div class="row" style="margin-bottom:15px;">
                           		
                           		<div class="col-md-4">
                           			New PIN
                           		</div>
                           		<div class="col-md-7">
                           			<input type="password" name="newPIN" id="newPINID" class="form-control">
                           		</div>
                           		
                           	</div>
                           	
                           	<div class="row" style="margin-bottom:15px;">
                           		
                           		<div class="col-md-4">
                           			Confirm New PIN
                           		</div>
                           		<div class="col-md-7">
                           			<input type="password" name="confirmNewPIN" id="confirmNewPINID" onkeyup="checkConfirmNewPIN(confirmNewPINID.value,newPINID.value);" class="form-control">
                           		</div>
                           		<div class="col-md-1" id="confirmPINID">
                           			
                           		</div>
                           		
                           </div>
                          </div>
                        </div>
                      </div>
                    </div>
		<!-- end of accordion -->

        </div>
        <div class="modal-footer">
	        <center>
	          <button type="button" data-dismiss="modal" id="secCredCancelID" class="btn btn-primary">Cancel</button>
	          <button type="button" style="width:25%" id="securityBtnID" onclick="updateSecurityCredentials(newPassID.value, newPINID.value);" class="btn btn-success" disabled="disabled" data-dismiss="modal">Update</button>
	        </center>
        </div>

      </div>
    </div>
  </div>
  
  <!-- Ends -->
  
 
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.jsp" class="site_title"><i class="fa fa-paw"></i> <span>RudhiraKosh!</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile">
              <div class="profile_pic">
                 <img src="<%= userDAOInf.retrieveBloodBankLogo(form.getBloodBankID()) %>" alt="Blood Bank Logo" style="height:53px;" class="img-circle profile_img" >
              </div>
              <div class="profile_info" style="margin-bottom:10px;">
                <h2 style="font-size:16px;font-weight:bold;"><%= userDAOInf.retrieveBloodBankName(form.getBloodBankID()) %></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3 style="color:#2A3F54; text-shadow: none;">a</h3>
                <ul class="nav side-menu">
                  <li><a onclick="windowOpen();"><i class="fa fa-tachometer"></i> Dashboard <span class="fa"></span></a>
                  </li>
                  <%
                  	if(form.getUserRole().equals("administrator") || form.getUserRole().equals("receptionist")){
                  		
                  		if((form.getUserRole().equals("administrator") && check == 0)){
                  %>
                  
                  <li><a><i class="fa fa-desktop"></i> Accounts <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="#openRegisterAlterID" data-toggle="modal">New Bill</a></li>
                      <li><a href="#openRegisterAlterID" data-toggle="modal">View Bills</a></li>
                      <li><a href="#openRegisterAlterID" data-toggle="modal">Other product</a></li>
                      <li><a href="#openRegisterAlterID" data-toggle="modal">View Other product</a></li>
                    </ul>
                  </li>
                  
                  <%
                  		}else{
                  %>
                  <li><a><i class="fa fa-desktop"></i> Accounts <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="RenderNewBill">New Bill</a></li>
                      <li><a href="viewBillList.jsp">View Bills</a></li>
                      <li><a href="otherProduct.jsp">Other product</a></li>
                      <li><a href="viewOtherProducts.jsp">View Other product</a></li>
                    </ul>
                  </li>
                  <%
                  		}
                  	}
                  	
                  	if(form.getUserRole().equals("administrator") || form.getUserRole().equals("courier")){
                  		
                  		if((form.getUserRole().equals("administrator") && check == 0)){
                  %>
                  
                  <li><a><i class="fa fa-truck"></i> Courier <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="#openRegisterAlterID" data-toggle="modal">New Bill</a></li>
                      <li><a href="#openRegisterAlterID" data-toggle="modal">View Bills</a></li>
                    </ul>
                  </li>
                  
                  <%
                  		}else{
                  %>
                  <li><a><i class="fa fa-truck"></i> Courier <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="RenderNewBillCourier">New Bill</a></li>
                      <li><a href="RenderViewBill">View Bills</a></li>
                    </ul>
                  </li>
                  <%
                  		}
                  	}
                  %>
                  <li><a><i class="fa fa-file-text-o"></i> Reports <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="patientReport.jsp">Patient-based Reports</a></li>
                      <li><a href="RenderExportStorageCenter">Storage Center Reports</a></li>
                      <li><a href="RenderComponentReport">Component-based Reports</a></li>
                      <li><a href="RenderConcessionReport">Concession-based Reports</a></li>
                      <li><a href="registerReport.jsp">Register Reports</a></li>
                      <li><a href="exportForAccountsReport.jsp">Export for Accounts</a></li>
                      <li><a href="cardChequeReport.jsp">Card/Cheque Report</a></li>
                    </ul>
                  </li>
                  <%
                  	if(form.getUserRole().equals("administrator")){
                  %>
                  <li><a><i class="fa fa-cogs"></i>Administration<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a>Users<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li class="sub_menu"><a href="RenderAddUser">Add Users</a>
                            </li>
                            <li class="sub_menu"><a href="editUserList.jsp">Edit Users</a>
                            </li>
                           </ul>
                        </li>
                        <li><a>Blood Bank<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li class="sub_menu"><a href="addBloodBank.jsp">Add Blood Bank</a>
                            </li>
                            <li class="sub_menu"><a href="editBloodBankList.jsp">Edit Blood Bank</a>
                            </li>
                           </ul>
                        </li>
                        <li><a>Configurations<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li class="sub_menu"><a href="configureComponents.jsp">Components</a>
                            </li>
                            <li class="sub_menu"><a href="configureConcession.jsp">Concessions</a>
                            </li>
                            <!-- <li class="sub_menu"><a href="configureInstructions.jsp">Instructions</a>
                            </li> -->
                            <li class="sub_menu"><a href="configureOtherCharges.jsp">Other Charges</a>
                            </li>
                            <!-- <li class="sub_menu"><a href="configureShifts.jsp">Shifts</a>
                            </li> -->
                          </ul>
                        </li>
					</ul>
                  </li>   
                  <%
                  	}
                  %>     
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->
            
            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" style="color:#73879C;" data-placement="top" title="Security Credentials" onclick="showSecurityModal();">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" style="color:#73879C;" data-placement="top" title="FullScreen" onclick="widnwoFullScreen();">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" style="color:#73879C;" data-placement="top" title="Lock" onclick="showLockModal();">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" style="color:#73879C;" data-placement="top" title="Logout" onclick="showLogoutModal();">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
            
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
              
              	  <%
	            		if(check == 0){
	              %>
	              
	              <!-- <div class="col-md-1" style="margin-top:10px;">
	              	<img src="images/edit.png" alt="Update Register" onclick="getBankDepositeDetails();" style="height: 38px;cursor: pointer;" data-toggle="tooltip" data-placement="bottom" title="Update Register">
	              </div> -->
	              <!-- <div class="col-md-1" style="margin-top:10px;">
	              	<img src="images/close.png" alt="Close Register" onclick="getRegisterBalance();" style="height: 38px;cursor: pointer;" data-toggle="tooltip" data-placement="bottom" title="Close Register">
	              </div>
	               
	               <div class="col-md-1" style="margin-top:10px;">
	              	<img src="images/cashBalance.png" alt="View Balance" onclick="openViewBalance();" style="height: 38px;cursor: pointer;" data-toggle="tooltip" data-placement="bottom" title="View Balance">
	              </div>-->
	              <div class="col-md-3" style="margin-top:18px;padding-left:0px;">
	              	<font style=" font-size: 16px;">Register Status: </font><font style="font-weight: bold; font-size: 16px;">Closed</font>
	              </div>
	              
	              <%
	            		}else{
	              %>
	              
	              <div class="col-md-1" style="margin-top:10px;">
	              	<img src="images/edit.png" alt="Update Register" onclick="getBankDepositeDetails();" style="height: 38px;cursor: pointer;" data-toggle="tooltip" data-placement="bottom" title="Update Register">
	              </div>
	              <!-- <div class="col-md-1" style="margin-top:10px;">
	              	<img src="images/close.png" alt="Close Register" onclick="getRegisterBalance();" style="height: 38px;cursor: pointer;" data-toggle="tooltip" data-placement="bottom" title="Close Register">
	              </div>
	               
	               <div class="col-md-1" style="margin-top:10px;">
	              	<img src="images/cashBalance.png" alt="View Balance" onclick="openViewBalance();" style="height: 38px;cursor: pointer;" data-toggle="tooltip" data-placement="bottom" title="View Balance">
	              </div>-->
	              <div class="col-md-3" style="margin-top:18px;padding-left:0px;">
	              	<font style=" font-size: 16px;">Register Status: </font><font style="font-weight: bold; font-size: 16px;">Opened</font>
	              </div>
	              
	              <%
	            		}
	              %>

              <ul class="nav navbar-nav navbar-right" style="width: 30%">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  	<%
              		if(userDAOInf.retrieveProfilePicName(form.getUserID()) == null || userDAOInf.retrieveProfilePicName(form.getUserID()) == ""){
	              	%>
	              
	                 <img src="images/user.png" alt="Profile Pic"><%= form.getFullname() %>
	                
	                <%
	              		}else{
	                %>
	                
	                 <img src="<%= userDAOInf.retrieveProfilePicName(form.getUserID()) %>" alt="Profile Pic"><%= form.getFullname() %>
	                
	                <%
	              		}
	                %>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="RenderEditProfile"> Profile</a></li>
                    <li><a href="DownloadManual"> Help</a></li>
                    <%
			          	if(form.getUserRole().equals("administrator") || form.getUserRole().equals("courier")){
			          %>
			          
			            <li><a href="LogoutUser?userID=<%= userID %>"><i class="fa fa-sign-out pull-right"></i>Log Out</a></li>
			            
			          <%
			          	}else{
			          %>
			          
			            <li><a href="javascript:retrieveCloseRegisterDetails();"><i class="fa fa-sign-out pull-right"></i>Log Out</a></li>
			            
			          <%
			          	}
			          %>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                <div class="page-title">
                      <div class="title_left">
                        <h3>CASH HAND-OVER REPORT</h3>
                      </div>
                    </div>

                    <div class="ln_solid" style="margin-top:0px;"></div>
                    
                    <div class="row">
                     <form action="SearchCashHandoverReport" method="POST">
                       <div class="col-md-3 col-sm-4 col-xs-6">
                         <input type="text" class="form-control" required="required" name="startDate" id="single_cal32" placeholder="From date*" aria-describedby="inputSuccess2Status3">
                       </div>
                       <div class="col-md-3 col-sm-3 col-xs-6">
                         <input type="text" class="form-control" required="required" name="endDate" id="single_cal33" placeholder="To date*" aria-describedby="inputSuccess2Status3">
                       </div>
                        <div class="col-md-2 col-sm-3 col-xs-6">
                          <button type="submit" class="btn btn-success" onclick="return onSearchCheck();">Search Cash hand-over Report</button>
                        </div>
                      </form>
                     </div>
                     
                     <div class="ln_solid"></div>
                     
                  <div class="x_content">
                  
                  
                  <%
                  	if(userListEnable == null || userListEnable == ""){
                  %>
                  
                  	<div>
		              	<s:if test="hasActionErrors()">
							<center>
							<font style="color: red; font-size:16px;"><s:actionerror /></font>
							</center>
						</s:if><s:else>
							<center>
							<font style="color: green;font-size:16px;"><s:actionmessage /></font>
							</center>
						</s:else>
		              </div>
		             
		            <%
                  	}else{
		            %> 
		            
		              <div>
		              	<s:if test="hasActionErrors()">
							<center>
							<font style="color: red; font-size:16px;"><s:actionerror /></font>
							</center>
						</s:if><s:else>
							<center>
							<font style="color: green;font-size:16px;"><s:actionmessage /></font>
							</center>
						</s:else>
		              </div>
		              
		              <form action="CashHandoverReport" name="reportForm" id="reportForm" method="POST">
		              
		              <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Register Date</th>
                          <th>Shift</th>
                          <th>Cash Handover</th>
                          <th>Cash Deposited</th>
                          <th>Shift Cash</th>
                          <th>Other Product</th>
                          <th>Balance</th>
                        </tr>
                      </thead>
                      <tbody>
                        <s:iterator value="cashHandoverReportList" var="UserForm">
                        	<tr>
                        			<td><s:property value="registerDate" />
                        			<input type="hidden" name="startDate" value="<s:property value="startDate" />">
                        			<input type="hidden" name="endDate" value="<s:property value="endDate" />">
                        			</td>
						
									<td><s:property value="shift" /></td>
										
									<td><s:property value="cashHandover" /></td>
									
									<td><s:property value="cashDeposited" /></td>
									
									<td><s:property value="totalCash" /></td>
									
									<td><s:property value="otherProductAmount" /></td>
									
									<td><s:property value="registerBalance" /></td>
										 
                        	</tr>
                        </s:iterator>
                      </tbody>
                    </table>
                    
                   <%
                  	} if(userListEnable == null || userListEnable == ""){
                   %>
                   
                   <%
                  	}else{
                   %>
                   
	                   <div class="row" style="margin-top:15px;">
	                   			<div class="col-md-4" id="reportDivID"></div>
	                   			<div class="col-md-4"></div>
	                   			<div class="col-md-4">
	                   				<div class="col-md-6" align="right">
	                   					<button type="submit" name="submitButton" value="PDF" class="btn btn-primary" id="pdfID">Export to PDF</button>
	                   				</div>
	                   				<div class="col-md-6" align="right">
	                   					<button type="submit" value="EXCEL" name="submitButton" class="btn btn-success" id="excelID">Export to Excel</button>
	                   				</div>
	                   			</div>
	                   </div>
	                   
	                <%
                  	}
	                %>
                   
                   </form>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="vendors/nprogress/nprogress.js"></script>
    <!-- validator -->
    <script src="vendors/validator/validator.js"></script>
    
    <!-- Datatables -->
    <script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="vendors/jszip/dist/jszip.min.js"></script>
    <script src="vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="build/js/custom.min.js"></script>
    
    <!-- bootstrap-daterangepicker -->
    <script src="js/moment/moment.min.js"></script>
    <script src="js/datepicker/daterangepicker.js"></script>
    
    <!-- bootstrap-daterangepicker -->

    <script>
   
    document.addEventListener('DOMContentLoaded', function() {

        $('#single_cal3').daterangepicker({
          singleDatePicker: true,
          calender_style: "picker_3"
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#single_cal31').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_3"
          }, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
          });
        
        $('#single_cal32').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_3"
          }, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
          });

          $('#single_cal33').daterangepicker({
              singleDatePicker: true,
              calender_style: "picker_3"
            }, function(start, end, label) {
              console.log(start.toISOString(), end.toISOString(), label);
            });
        
      });
    
    </script>

    <!-- /bootstrap-daterangepicker -->
    
    <!-- Datatables -->
    <script>
      $(document).ready(function() {

        $('#datatable-responsive').DataTable();

      });
    </script>
    <!-- /Datatables -->
    
  </body>
</html>