<%@page import="java.util.HashMap"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="rudhira.DAO.BillingDAOImpl"%>
<%@page import="rudhira.DAO.BillingDAOInf"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="rudhira.DAO.UserDAOImpl"%>
<%@page import="rudhira.DAO.UserDAOInf"%>
<%@page import="rudhira.form.UserForm"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>    
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>New Bill | RudhiraKosh</title>

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
    
    <!-- Datatables -->
    <link href="vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    
    
    <!-- Custom Theme Style -->
    <link href="build/css/custom.min.css" rel="stylesheet">
    
    <script type="text/javascript">
      function windowOpen(){
        document.location="dashboard.jsp";
      }
    </script>
    
    <!-- Enabling submit button  -->
    
    <script type="text/javascript">
    
    	function enableSubmit(bbStorage){
    		
    		if(bbStorage == "000"){
    			
				$('#send').attr('disabled',true);
        		
        		$('#firstNameID').attr('required','required');
    			$('#lastNameID').attr('required','required');
    			$('#patientID').attr('required','required');
    			
    			$('#fetchRefReceiptBtnID').attr('disabled', true);
    			
    		}else{
    			
    			$('#send').attr('disabled',false);
        		
        		$('#firstNameID').removeAttr('required');
    			$('#lastNameID').removeAttr('required');
    			$('#patientID').removeAttr('required');
    			
    			$('#fetchRefReceiptBtnID').attr('disabled', false);
    			
    		}
    		
    	}
    
    </script>
    
    <!-- Ends -->
    
    
    <!-- Refresh page on close register cancel button -->
    
    <script type="text/javascript">
    	
    	function refreshPage(){
    		location.reload();
    	}
    
    </script>
    
    <!-- Ends -->
	
	
	<!-- Open close register alert modal -->
    
    <script type="text/javascript">
    	function registerCloseAlert(){
    		$('#logoutModal').modal('hide');
    		$('#closeRegisterAlert').modal('show');
    	}
    	
    	function registerCloseAlertLockScreen(){
    		$('#lockModal').modal('hide');
    		$('#closeRegisterAlertLockScreen').modal('show');
    	}
    </script>
    
    <!-- Ends -->
    
    
    <!-- Retrieve Register balance  -->
    
    <script type="text/javascript" charset="UTF-8">
	var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function getRegisterBalance() {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);

				for ( var i = 0; i < array.Release.length; i++) {
					
					$('#registerBalanceID').val(array.Release[i].registerBalance);
					$('#totalCashID').val(array.Release[i].shiftCash);
					
				}
				
				$('#closeRegister').modal('show');
			}
		};
		xmlhttp.open("GET", "GetRegisterBalance", true);
		xmlhttp.send();
	}
	</script>
    
    <!-- Ends -->
    
    <!-- Sumbit Open Register, Close register form -->
    
    <script type="text/javascript">
    	
    	function submitCloseReg(){
    		
    		if($('#cashHandoverToID').val() == "000"){
    			
    			alert('Please select cash handover to.');
    			
    		}else{
    			
    			document.forms["closeRegForm"].action = "CloseRegister";
        		document.forms["closeRegForm"].submit();
    			
    		}
    	}
    	
    </script>
    
    <!-- Ends -->
    
    
    <!-- Scanning bag no and retrieving blood group from Jankalyan DB -->
    
    <script type="text/javascript">
    
    document.addEventListener('DOMContentLoaded', function() {
    		
    	$("#BagNumberID").keypress(function(e){
		    
		     $("#BagNumberID").focus();
		    	 
		      var bagNo = $("#BagNumberID").val();

			  getBloodGroup(bagNo);
			  
		   });
    	
    });
    
    </script>
    
    <!-- Ends-->
    
    <!-- Retrieve blood group based on bag no from Jankalyan DB -->
    
    <script type="text/javascript">
    	var xmlhttp;
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	
		function getBloodGroup(bagNo) {
			
			var check = 0;
		
			xmlhttp.onreadystatechange = function() {
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	
					var array = JSON.parse(xmlhttp.responseText);
	
					for ( var i = 0; i < array.Release.length; i++) {
						
						document.getElementById("bloodGroupID").value = array.Release[i].bloodGroup;
						check = array.Release[i].check;
						
					}
					
					if(check == 0){
						document.getElementById("bloodGroupID").value = "";
					}
				}
			};
			xmlhttp.open("GET", "GetBloodGroup?bloodBagNo="
					+ bagNo, true);
			xmlhttp.setRequestHeader("Content-type", "charset=UTF-8");
			xmlhttp.send();
		}
    </script>
    
    <!-- Ends -->
    
    <!-- Scanning bag no and retrieving blood group from Jankalyan DB for cross matching -->
    
    <script type="text/javascript">
    
    document.addEventListener('DOMContentLoaded', function() {
    		
    	$("#BagNumberID1").keypress(function(e){
		    
		     $("#BagNumberID1").focus();
		    	 
		      var bagNo = $("#BagNumberID1").val();

			  getBloodGroupCRM(bagNo);
			  
		   });
    	
    });
    
    </script>
    
    <!-- Ends-->
    
    <!-- Retrieve blood group based on bag no from Jankalyan DB for cross matching -->
    
    <script type="text/javascript">
    	var xmlhttp;
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	
		function getBloodGroupCRM(bagNo) {
			
			var check = 0;
		
			xmlhttp.onreadystatechange = function() {
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	
					var array = JSON.parse(xmlhttp.responseText);
	
					for ( var i = 0; i < array.Release.length; i++) {
						
						document.getElementById("bloodGroupID1").value = array.Release[i].bloodGroup;
						check = array.Release[i].check;
						
					}
					
					if(check == 0){
						document.getElementById("bloodGroupID1").value = "";
					}
				}
			};
			xmlhttp.open("GET", "GetBloodGroup?bloodBagNo="
					+ bagNo, true);
			xmlhttp.setRequestHeader("Content-type", "charset=UTF-8");
			xmlhttp.send();
		}
    </script>
    
    <!-- Ends -->
    
    
    <!-- Scanning bag no and retrieving blood group from Jankalyan DB for ABD screening -->
    
    <script type="text/javascript">
    
    document.addEventListener('DOMContentLoaded', function() {
    		
    	$("#BagNumberID2").keypress(function(e){
		    
		     $("#BagNumberID2").focus();
		    	 
		      var bagNo = $("#BagNumberID2").val();

			  getBloodGroupABD(bagNo);
			  
		   });
    	
    });
    
    </script>
    
    <!-- Ends-->
    
    <!-- Retrieve blood group based on bag no from Jankalyan DB for ABD screening -->
    
    <script type="text/javascript">
    	var xmlhttp;
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	
		function getBloodGroupABD(bagNo) {
			
			var check = 0;
		
			xmlhttp.onreadystatechange = function() {
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	
					var array = JSON.parse(xmlhttp.responseText);
	
					for ( var i = 0; i < array.Release.length; i++) {
						
						document.getElementById("bloodGroupID2").value = array.Release[i].bloodGroup;
						check = array.Release[i].check;
						
					}
					
					if(check == 0){
						document.getElementById("bloodGroupID2").value = "";
					}
				}
			};
			xmlhttp.open("GET", "GetBloodGroup?bloodBagNo="
					+ bagNo, true);
			xmlhttp.setRequestHeader("Content-type", "charset=UTF-8");
			xmlhttp.send();
		}
    </script>
    
    <!-- Ends -->
    
    
    <!-- Function to open add bags modal -->
    
    <script type="text/javascript">
    
    var abdCheck = 1;
    var crmCheck = 1;
    	
    document.addEventListener('DOMContentLoaded', function() {
		
		$('#addBagID').on('shown.bs.modal', function () {
			$('#BagNumberID').focus();
			
			/*
			* Clearing all TRs except first when adding next component
			*/
			if(popUpCheck == "close"){
				$('#bagNoTblID tr:gt(0)').remove();
				bagBGCheckCounter = 1;
				//counter = 3;
			}
		})
		
		$('#addCRMBagID').on('shown.bs.modal', function () {
			$('#BagNumberID1').focus();
		})
		
		$('#addABDBagID').on('shown.bs.modal', function () {
			$('#BagNumberID2').focus();
		})
	});
    
    </script>
    
    <!-- Ends -->
    
    
    <!-- Function to open view bag modal -->
	
	<script type="text/javascript">
	
		function viewBags(bagNo){
			
			if(bagNo.startsWith(",")){
				bagNo = bagNo.substr(1);
			}
			
			if(bagNo.includes(",")){
				
				var bagNoArray = bagNo.split(",");
				
				var tag = "<table border='1' style='text-align:center; width:100%'>";
				var srNo = 1;
				
				tag += "<tr style='font-size:14px;'>"+
				"<td style='padding:5px;width:10%;'>Sr.No.</td>"+
	    		"<td style='padding:5px;width:45%;'>Bag No.</td>"+
	    		//"<td style='padding:5px;width:45%;'>Blood Group</td>"+
	    		"</tr>";
				
				for(var i = 0; i < bagNoArray.length; i++){
					
					var bagNoBldGrpArray = bagNoArray[i].split("$");
					
					tag += "<tr style='font-size:14px;'>"+
					"<td style='padding:5px;width:10%;'>"+srNo+"</td>"+
		    		"<td style='padding:5px;width:45%;'>"+bagNoBldGrpArray[0]+"</td>"+
		    		//"<td style='padding:5px;width:45%;'>"+bagNoBldGrpArray[1]+"</td>"+
		    		"</tr>";
		    		
					srNo++;
					
				}
				
				tag += "</table>";
				
				$('#bagDetailID').html(tag);
				$('#viewBagID').modal('show');
				
			}else{
				
				var tag = "<table border='1' style='text-align:center; width:100%'>";
				var srNo = 1;
				
				var bagNoBldGrpArray = bagNo.split("$");
				
				tag += "<tr style='font-size:14px;'>"+
				"<td style='padding:5px;width:10%;'>Sr.No.</td>"+
	    		"<td style='padding:5px;width:45%;'>Bag No.</td>"+
	    		//"<td style='padding:5px;width:45%;'>Blood Group</td>"+
	    		"</tr>";
				
				tag += "<tr style='font-size:14px;'>"+
				"<td style='padding:5px;width:10%;'>"+srNo+"</td>"+
	    		"<td style='padding:5px;width:45%;'>"+bagNoBldGrpArray[0]+"</td>"+
	    		//"<td style='padding:5px;width:45%;'>"+bagNoBldGrpArray[1]+"</td>"+
	    		"</tr>";
	    		
				tag += "</table>";
				
				$('#bagDetailID').html(tag);
				$('#viewBagID').modal('show');
				
			}
			
		}
	
	</script>
	
	<!-- Ends -->
    
    
   	<!-- Barcode scanning -->
    
    <script type="text/javascript">
	
	    document.addEventListener('DOMContentLoaded', function() {
	    	
	    		$("#bagNoID").keypress(function(e){
	    		    
	    		     $("#bagNoID").focus();
	    		     //Setting timeout of 1 sec in order to append comma(,)
	    		     //after each scanned barcode
	    		     setTimeout(function() {
	    		    	 
	    		      var a = $("#bagNoID").val();
	    		      var b = a.length;

	    		      setBarcode(b,a);
	    		      
	    		}, 1000);
	    		   });
	    			
	    	//For abd screening bag no
	    	$("#babcScreeningBagNoID").keypress(function(e){
    		    
   		     $("#babcScreeningBagNoID").focus();
   		     
   		     //Setting timeout of 1 sec in order to append comma(,)
   		     //after each scanned barcode
   		     setTimeout(function() {
   		    	 
   		      var c = $("#babcScreeningBagNoID").val();
   		      var d = c.length;

   		      setBarcode1(d,c);
   		      
   		}, 1000);
   		   });
	    		
	    	//For cross matching bag no
	    		$("#crossMatchBagNoID").keypress(function(e){
	    		    
	      		     $("#crossMatchBagNoID").focus();
	      		     
	      		     //Setting timeout of 1 sec in order to append comma(,)
	      		     //after each scanned barcode
	      		     setTimeout(function() {
	      		    	 
	      		      var c = $("#crossMatchBagNoID").val();
	      		      var d = c.length;

	      		      setBarcode2(d,c);
	      		      
	      		}, 1000);
	      		   });
	    	     
		});
	    
	    var barcodeCounter = 0;
	    var barcodeCounter1 = 0;
	    var barcodeCounter2 = 0;
	    
	    function setBarcode(b,a){
	    	barcodeCounter++;
	    	if(barcodeCounter>b){
	    		var c = $("#bagNoID").val();
	    		$("#bagNoID").val(c+",");
	    		barcodeCounter=b+1;
	    	}

	    }
	    
	    function setBarcode1(b,a){
	    	barcodeCounter1++;
	    	if(barcodeCounter1>b){
	    		var c = $("#babcScreeningBagNoID").val();
	    		$("#babcScreeningBagNoID").val(c+",");
	    		barcodeCounter1=b+1;
	    	}

	    }
	    
	    function setBarcode2(b,a){
	    	barcodeCounter2++;
	    	if(barcodeCounter2>b){
	    		var c = $("#crossMatchBagNoID").val();
	    		$("#crossMatchBagNoID").val(c+",");
	    		barcodeCounter2=b+1;
	    	}

	    }
	  
	</script>
    
    <!-- Ends -->
    
    
    <!-- Display and hide cheque details div -->
    
    <script type="text/javascript">
    
    	var paymentCounter = 1;
    
    	function displayChequeDetailsDiv(){

        	$('#chequeDetailID').show(1000);
        	$('#cardDetailID').hide(1000);

    	}

    	function hideChequeDetailsDiv(){

    		$('#chequeDetailID').hide(1000);
    		$('#cardDetailID').hide(1000);
    		
			if(paymentCounter > 1){
        		
        		var oustandingAmt = $("#oustandingAmtID").val();
            	
            	$("#oustandingAmtID").val(0);
            	$("#actualPaymentID").val(oustandingAmt);
        		
        	}

    	}
    	
    	function displayCardDiv(){
    		
    		$('#chequeDetailID').hide(1000);
    		$('#cardDetailID').show(1000);
    		
    	}
    	
    	function displayChequeDetailsDivCreditNote(){

    		$('#chequeDetailID').hide(1000);
    		$('#cardDetailID').hide(1000);
        	
        	var actualPayment = $("#actualPaymentID").val();
        	
        	$("#actualPaymentID").val(0);
        	$("#oustandingAmtID").val(actualPayment);
        	
        	paymentCounter++;

    	}
    	
    </script>
    
    <!-- Ends -->
    
    
    <!-- Disable User start -->
    
    <script type="text/javascript">
    	function disableUser(url){
			if(confirm("Disabled users cannot access application. Are you sure you want to disable this user?")){
				document.location = url;
			}
    	}
    </script>
    
    <!-- Ends -->
    
    <!-- PDF Print window -->
    
    <%
		String pdfOutFIleName = (String) request.getAttribute("PDFOutFileName");
		System.out.println("OPD PDF out file path ::: "+pdfOutFIleName);
		
		if(pdfOutFIleName == null || pdfOutFIleName == ""){
			
			pdfOutFIleName = "dummy";
			
		}else{
			
			if(pdfOutFIleName.contains("\\")){
				
				pdfOutFIleName = pdfOutFIleName.replaceAll("\\\\", "/");
				
			}
		}
	%>
	
	<%
		if(pdfOutFIleName!="dummy"){
	%>
	<script type="text/javascript">
	
	    document.addEventListener('DOMContentLoaded', function() {
			  PdfAction();
		});
	  
	</script>
	<% 
		}
	%>
	
	<script type="text/javascript">
	
		var popup;
		function PdfAction(){
			popup = window.open('PDFDownload?pdfOutPath=<%=pdfOutFIleName%>',"Popup", "width=700,height=700");
			popup.focus();
			popup.print();
		}
	  
	</script>
    
    <!-- Ends -->
    
    <style type="text/css">

		ul.actionMessage{
			list-style:none;
			font-family: "Helvetica Neue", Roboto, Arial, "Droid Sans", sans-serif;
		}
		
		ul.errorMessage{
			list-style:none;
			font-family: "Helvetica Neue", Roboto, Arial, "Droid Sans", sans-serif;
		}

	</style>
	
	
	<!-- Get CUrrent date and time -->
	
	<%
		Date date = new Date();
	
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
	%>
	
	<!-- Ends -->
    
    <!-- For login message -->
    <%
    UserForm form = (UserForm) session.getAttribute("USER");
		if(session.getAttribute("USER") == "" || session.getAttribute("USER") == null){
			form.setFullname("");
			String loginMessage = "Plase login using valid credentials";
			request.setAttribute("loginMessage",loginMessage);
			RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
			dispatcher.forward(request,response);
		}
		
		int userID = form.getUserID();
		
	UserDAOInf userDAOInf = new UserDAOImpl();
	BillingDAOInf billingDAOInf = new BillingDAOImpl();
	
	%>
    <!-- Ends -->
    
    
    <!-- Open register check -->
    
    <%
    	int check = userDAOInf.verifyOpenRegister();
    %>
    
    <!-- Ends -->
    
    <!-- Retrieving receiptionist user List -->
    
    <%
    	HashMap<Integer, String> receptionistList = userDAOInf.retrieveReceiptionistUser(userID);
    
    %>
    
    
     <!-- Retrieving last receipt no in order to set new receipt no -->
    
    <%
    	String receiptNo = billingDAOInf.retrieveReceiptNoForCourier("CR", form.getBloodBankID());
    %>
    
    <!-- Ends -->
    
    
    <!-- Retrieve Component, COncession and Other CHarges list -->
    
    <%
    	List<String> componentList = billingDAOInf.retrieveComponentList(form.getBloodBankID());
    
	    List<String> concessionList = billingDAOInf.retrieveConcessionList(form.getBloodBankID());
	    
	    List<String> otherChargesList = billingDAOInf.retrieveOtherChargesList(form.getBloodBankID());
    %>
    
    <!-- Ends -->
    
    <!-- For session timeout -->
    <%
    
    int sessionTimeout = 30;
    	
    int maxSessionTimeout = sessionTimeout * 60 * 1000;
	//session.setMaxInactiveInterval(maxSessionTimeout);
	
	//redirecting to login page on session timeout.
	//int timeout = session.getMaxInactiveInterval();
	//response.setHeader("Refresh", timeout + "; URL = index.jsp?message=Your session has been expired. Please login again.");
    
    %>
    <!-- Ends -->
    
    <!-- Lock screen pop up calling after time interval -->
    
    <script type="text/javascript">
    	document.addEventListener('DOMContentLoaded', function() {
        	var interval = <%=maxSessionTimeout%>;
    		setInterval(function(){

    			//Setting blank values to PIN fields
            	$('#lockPIN').val("");

            	//opening security credentials modal
        		$("#lockModal").modal("show");	
				
        	}, interval);
        });
    </script>
    
    <!-- Ends -->
    
    <%
    	String userListEnable = (String) request.getAttribute("userListEnable");
    %>
    
    
     <!-- Shortcut menu Full screen function -->
    
    <script type="text/javascript">
    window.onload = maxWindow;

    function widnwoFullScreen() {
        
    	var docElm = document.documentElement;
		if (docElm.requestFullscreen) {
		    docElm.requestFullscreen();
		}
		else if (docElm.mozRequestFullScreen) {
		    docElm.mozRequestFullScreen();
		}
		else if (docElm.webkitRequestFullScreen) {
		    docElm.webkitRequestFullScreen();
		}
		
	}

	</script> 
    
    <!-- Ends -->
    
    <!-- Show modals function -->
    
    <script type="text/javascript">
    function showLockModal(){
    		//Setting blank values to PIN fields
        	$('#lockPIN').val("");

        	//opening security credentials modal
    		$("#lockModal").modal("show");
    }

    function showLogoutModal(){
    		$("#logoutModal").modal("show");
    }

    function showSecurityModal(){
        	//Setting blank values and disabling new Pass and confirm new Pass fields
    		$('#oldPassword').val("");
    		$('#newPassID').val("");
    		$('#confirmNewPassID').val("");
        	$('#newPassID').prop('disabled', true);
        	$('#confirmNewPassID').prop('disabled', true);

        	//Setting blank values to new PIN and confirm new PIN fields
        	$('#newPINID').val("");
    		$('#confirmNewPINID').val("");

    		//removing images by setting innerHTML to blank("")
    		$('#oldPassID').html("");
    		$('#newPassCheckID').html("");
    		$('#confirmPassID').html("");    		
    		$('#confirmPINID').html(""); 

    		//opening security credentials modal
    		$("#securityModal").modal("show");
    }
    </script>
    
    <!-- ENds -->
    
    
     <!-- BDR No. based patient details -->

	<script type="text/javascript" charset="UTF-8">
		var xmlhttp;
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	
		function getPatientDetail(bdrNo) {
		
			xmlhttp.onreadystatechange = function() {
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	
					var array = JSON.parse(xmlhttp.responseText);
					var check = 0;
					var patientIDNo;
					var firstName;
					var middleName;
					var lastName;
					var hospital;
					var mobile;

					if(bdrNo == "" || bdrNo == null){

						check = 0;

					}else{
	
						for ( var i = 0; i < array.Release.length; i++) {
	
							console.log(array.Release.length);
							
							patientIDNo = array.Release[i].patientCode;
							firstName = array.Release[i].firstName;
							middleName = array.Release[i].middleName;
							lastName = array.Release[i].lastName;
							address = array.Release[i].address;
							hospital = array.Release[i].hospital;
							mobile = array.Release[i].mobile;
							check = array.Release[i].check;
	
							console.log('Value for check :: '+check);
	
							if(patientIDNo == "undefined"){
								patientIDNo = "";
							}
							if(firstName == "undefined"){
								firstName = "";
							}
							if(middleName == "undefined"){
								middleName = "";
							}
							if(lastName == "undefined"){
								lastName = "";
							}
							if(address == "undefined"){
								address = "";
							}
							if(hospital == "undefined"){
								hospital = "";
							}
							if(mobile == "undefined"){
								mobile = "";
							}
							if(check == "undefined"){
								check = 0;
							}
							
						}

					}

					if(check == 1){

						$('#bdrCheckID').html("<img src='images/correct.png' alt='Valid BDR No' title='BDR No is valid' style='height:24px;margin-top: 5px;'>");
						
						$('#patientID').val(patientIDNo);
						$('#firstNameID').val(firstName);
						$('#middleNameID').val(middleName);
						$('#lastNameID').val(lastName);
						$('#addressID').val(address);
						$('#hospitalID').val(hospital);
						$("#mobileID").val(mobile);

						$('#send').attr('disabled', false);
						
						$('#fetchRefReceiptBtnID').attr('disabled', false);
						
						$('#bbStorageCenterID').removeAttr('required');
						
						generateBarcode(patientIDNo);
						
					}else{

						$('#bdrCheckID').html("<img src='images/wrong.png' alt='Invalid BDR No' title='BDR No is invalid' style='height:24px;margin-top: 5px;'>");

						$('#patientID').val("");
						$('#firstNameID').val("");
						$('#middleNameID').val("");
						$('#lastNameID').val("");
						$('#addressID').val("");
						$('#hospitalID').val("");
						$("#mobileID").val("");

						$('#send').attr('disabled', true);
						
						$('#fetchRefReceiptBtnID').attr('disabled', true);

					}
					
				}
			};
			xmlhttp.open("GET", "GetPatientDetail?bdrNo="
					+ bdrNo, true);
			xmlhttp.setRequestHeader("Content-type", "charset=UTF-8");
			xmlhttp.send();
		}
		
		function generateBarcode(patientIDNo){
    		
    		var value = patientIDNo;
    		
    		var settings = {
    			output:'css',
    		    bgColor: '#FFFFFF',
    		    color: '#000000',
    		    barWidth: '1',
    		    barHeight: '20',
    	        moduleSize: '5',
    	        posX: '10',
    	        posY: '20',
    	        addQuietZone: '1'
    		};
    		
    		$('#generateID').html('').show().barcode(value, 'code39', settings);
    		
    	}
	</script>
    
    <!-- Ends -->
    
    
    <!-- Rate based on component -->

	<script type="text/javascript" charset="UTF-8">
		var xmlhttp;
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	
		function getComponentRate(component) {
		
			xmlhttp.onreadystatechange = function() {
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	
					var array = JSON.parse(xmlhttp.responseText);

					var rate;
	
					for ( var i = 0; i < array.Release.length; i++) {
						document.getElementById("rateID").value = array.Release[i].componentRate;

						
						rate = array.Release[i].componentRate;
					}

					var quantity = document.getElementById("quantityID").value;

					var componentAmt = parseFloat(parseFloat(quantity) * parseFloat(rate));

					document.getElementById("amountID").value = parseFloat(componentAmt);

					var dummyTotl = document.getElementById("dummyTotalAmt").value;

					if(dummyTotl == ""){
						dummyTotl = 0;
	        		}

					var total = parseFloat(parseFloat(componentAmt) + parseFloat(dummyTotl));

					var crsMtchAmt = $('#crossMatchAmountID').val();

	        		var abdScrAmt = $('#abcScreeningAmountID').val();

	        		var concessionAmt = $('#concessionAmountID').val();

	        		var otherChargeAtm = $('#otherChargeID').val();

	        		var totalAmt;

	        		if(concessionAmt == ""){
	        			concessionAmt = 0;
	        		}

	        		if(otherChargeAtm == ""){
	        			otherChargeAtm = 0;
	        		}

	        		if(crsMtchAmt == "" && abdScrAmt == ""){

	        			crsMtchAmt = 0;

	        			abdScrAmt = 0;

	        			totalAmt = parseFloat(parseFloat(crsMtchAmt) + parseFloat(abdScrAmt) + parseFloat(total) + parseFloat(otherChargeAtm) + parseFloat(concessionAmt));
		        		
	        		}else if(crsMtchAmt == ""){
		        		
	        			crsMtchAmt = 0;

	        			totalAmt = parseFloat(parseFloat(crsMtchAmt) + parseFloat(abdScrAmt) + parseFloat(total) + parseFloat(otherChargeAtm) + parseFloat(concessionAmt));
	        			
	        			
	        		}else if(abdScrAmt == ""){
		        		
	        			abdScrAmt = 0;

	        			totalAmt = parseFloat(parseFloat(crsMtchAmt) + parseFloat(abdScrAmt) + parseFloat(total) + parseFloat(otherChargeAtm) + parseFloat(concessionAmt));
	        			
	        		}else{

	        			totalAmt = parseFloat(parseFloat(crsMtchAmt) + parseFloat(abdScrAmt) + parseFloat(total) + parseFloat(otherChargeAtm) + parseFloat(concessionAmt));

	        		}

					
				}
			};
			xmlhttp.open("GET", "GetComponentRate?component="
					+ component, true);
			xmlhttp.setRequestHeader("Content-type", "charset=UTF-8");
			xmlhttp.send();
		}
	</script>
    
    <!-- Ends -->
    
    <!-- Retrieve Receipt No based on receipt Type -->
    
    <script type="text/javascript">
	
		function retrieveReceiptNo(receiptType) {
			
			var date = new Date();
		    var currentYear = date.getFullYear();
		    var month = date.getMonth();
		    
		    if(month >= 3){
		    	document.getElementById("receiptNoID").value = receiptType + "-" + currentYear + "-";
		    }else{
		    	document.getElementById("receiptNoID").value = receiptType + "-" + (parseInt(currentYear)-1) + "-";
		    }
			
			if(receiptType == 'CBSC'){
				
				$('#patientDetailID').hide(1000);
				$('#BBStorageID').show(1000);
				
			}else{
				
				$('#BBStorageID').hide(1000);
				$('#patientDetailID').show(1000);
				
			}
			
		}
    </script>
    
    <!-- Ends -->
    
    
    <!-- Rate based on other charges -->

	<script type="text/javascript" charset="UTF-8">
		var xmlhttp;
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	
		function getOtherChargesRate(chargeType) {
		
			xmlhttp.onreadystatechange = function() {
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	
					var array = JSON.parse(xmlhttp.responseText);
					var charge;
	
					for ( var i = 0; i < array.Release.length; i++) {
						document.getElementById("otherChargeID").value = array.Release[i].chargeRate;

						charge = array.Release[i].chargeRate;
						
					}

					/** 
					var totalAmt = document.getElementById("dummyTotalAmt").value;
					var concession = document.getElementById("concessionAmountID").value;

					if(concession == ""){
						concession = 0;
					}

					var finalTotalAmt = parseFloat(parseFloat(totalAmt) + parseFloat(charge) + parseFloat(concession));

					document.getElementById("totalAmountID").value = parseFloat(finalTotalAmt);
					document.getElementById("netReceivableAmtID").value = parseFloat(finalTotalAmt);
					document.getElementById("oustandingAmtID").value = parseFloat(finalTotalAmt);
					**/
				}
			};
			xmlhttp.open("GET", "GetOtherChargesRate?chargeType="
					+ chargeType, true);
			xmlhttp.setRequestHeader("Content-type", "charset=UTF-8");
			xmlhttp.send();
		}
	</script>
    
    <!-- Ends -->
    
    
    <!-- Rate based on concession -->

	<script type="text/javascript" charset="UTF-8">
		var xmlhttp;
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		var concessionCheck = 1;
	
		function getConcessionRate(concession) {
		
			xmlhttp.onreadystatechange = function() {
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	
					var array = JSON.parse(xmlhttp.responseText);
					var concession;
					
					var temp = document.getElementById("concessionID").value;
					
					var netReceivableAmtID = $('#netReceivableAmtID').val();
					
					var lastConcessionAmt = $('#lastConcessionAmtID').val();

					if(netReceivableAmtID == ""){
						netReceivableAmtID = 0;
					}
					
					if(lastConcessionAmt == ""){
						lastConcessionAmt = 0;
					}
					
					if(temp == "000"){
						
						var finalTotalAmt = parseFloat(parseFloat(netReceivableAmtID) + parseFloat(lastConcessionAmt));
						
						document.getElementById("netReceivableAmtID").value = parseFloat(finalTotalAmt);
						//document.getElementById("lastNetReceivableAmtID").value = parseFloat(finalTotalAmt);
						document.getElementById("oustandingAmtID").value = parseFloat(0);
						document.getElementById("actualPaymentID").value = parseFloat(finalTotalAmt);
						document.getElementById("lastConcessionAmtID").value = "";
						
						document.getElementById("concessionAmountID").value = "";
						
					}
	
					for ( var i = 0; i < array.Release.length; i++) {
						document.getElementById("concessionAmountID").value = array.Release[i].concessionRate;

						concession = array.Release[i].concessionRate;
						
					}
					
					if(concessionCheck == 1){
						
						var finalTotalAmt = parseFloat(parseFloat(netReceivableAmtID) - parseFloat(concession));

						//document.getElementById("totalAmountID").value = parseFloat(finalTotalAmt);
						document.getElementById("netReceivableAmtID").value = parseFloat(finalTotalAmt);
						//document.getElementById("lastNetReceivableAmtID").value = parseFloat(finalTotalAmt);
						document.getElementById("oustandingAmtID").value = parseFloat(0);
						document.getElementById("actualPaymentID").value = parseFloat(finalTotalAmt);
						document.getElementById("lastConcessionAmtID").value = parseFloat(concession);
						
						concessionCheck++;
						
					}else{
						
						var finalTotalAmt = parseFloat(parseFloat(netReceivableAmtID) + parseFloat(lastConcessionAmt) - parseFloat(concession));
						
						//document.getElementById("totalAmountID").value = parseFloat(finalTotalAmt);
						document.getElementById("netReceivableAmtID").value = parseFloat(finalTotalAmt);
						//document.getElementById("lastNetReceivableAmtID").value = parseFloat(finalTotalAmt);
						document.getElementById("actualPaymentID").value = parseFloat(finalTotalAmt);
						document.getElementById("oustandingAmtID").value = parseFloat(0);
						document.getElementById("lastConcessionAmtID").value = parseFloat(concession);
						
					}
					
				}
			};
			xmlhttp.open("GET", "GetConcessionRate?concession="
					+ concession, true);
			xmlhttp.setRequestHeader("Content-type", "charset=UTF-8");
			xmlhttp.send();
		}
	</script>
    
    <!-- Ends -->
    
    
    <!-- Lock PIN check -->
    
    <script type="text/javascript" charset="UTF-8">
	var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function checkUnlockPIN(lockPIN) {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);

				for ( var i = 0; i < array.Release.length; i++) {
					var check = array.Release[i].PINCheck;
					var errMsg = array.Release[i].ErrMsg;

					if(check == "check"){

						document.getElementById("lockImgID").innerHTML = "";

						unlockModal();
						
					}else{

						document.getElementById("lockImgID").innerHTML = "<img src='images/wrong.png' alt='Incorrect PIN' title='Incorrect PIN' style='height:24px;margin-top: 5px;'>";
						
					}
				}
			}
		};
		xmlhttp.open("GET", "VerifyUnlockPIN?lockPIN="
				+ lockPIN, true);
		xmlhttp.send();
	}
	</script>
    
    <!-- Ends -->
    
    
    <!-- Old Password check -->
    
    <script type="text/javascript" charset="UTF-8">
	var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function verifyOldPass(oldPass) {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);

				for ( var i = 0; i < array.Release.length; i++) {
					var check = array.Release[i].PassCheck;
					var errMsg = array.Release[i].ErrMsg;

					if(check == "check"){

						document.getElementById("oldPassID").innerHTML = "<img src='images/correct.png' alt='Correct Old Password' title='Correct Old Password' style='height:24px;margin-top: 5px;'>";

						//Enabling New password and confirm password input fields
						document.getElementById("newPassID").disabled = false;
						
					}else{

						document.getElementById("oldPassID").innerHTML = "<img src='images/wrong.png' alt='Incorrect Old Password' title='Incorrect Old Password' style='height:24px;margin-top: 5px;'>";
						
					}
				}
			}
		};
		xmlhttp.open("GET", "VerifyOldPass?oldPass="
				+ oldPass, true);
		xmlhttp.send();
	}
	</script>
    
    <!-- Ends -->
    
     <!-- New Password check -->
    
    <script type="text/javascript" charset="UTF-8">
	var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function newPassCheck(newPass) {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);

				for ( var i = 0; i < array.Release.length; i++) {
					var check = array.Release[i].newPassCheck;
					var errMsg = array.Release[i].ErrMsg;

					if(check == "true"){

						document.getElementById("newPassCheckID").innerHTML = "<img src='images/correct.png' alt='Valid new password' title='New password is valid' style='height:24px;margin-top: 5px;'>";
						document.getElementById("confirmNewPassID").disabled = false;
						
					}else{

						document.getElementById("newPassCheckID").innerHTML = "<img src='images/wrong.png' alt='Invalid new password' title='Incorrect Old Password' style='height:24px;margin-top: 5px;'>";
						
					}
				}
			}
		};
		xmlhttp.open("GET", "NewPassCheck?newPass="
				+ newPass, true);
		xmlhttp.send();
	}
	</script>
    
    <!-- Ends -->
    
    
    <!-- Submit Security Credentials -->
    
    <script type="text/javascript" charset="UTF-8">
	var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function updateSecurityCredentials(newPass, newPIN) {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);

				for ( var i = 0; i < array.Release.length; i++) {
					var check = array.Release[i].PassCheck;
					var errMsg = array.Release[i].ErrMsg;

					if(check == "check"){

						hideSecCredModal();
						
					}else{

						document.getElementById("oldPassID").innerHTML = "<img src='images/wrong.png' alt='Incorrect Old Password' title='Incorrect Old Password' style='height:24px;margin-top: 5px;'>";
						
					}
				}
			}
		};
		xmlhttp.open("GET", "UpdateSecurityCredentials?newPass="
				+ newPass + "&newPIN=" + newPIN , true);
		xmlhttp.send();
	}
	</script>
    
    <!-- Ends -->
    
    
    <!-- Confirm new pass and Confirm new PIN check -->
    
    <script type="text/javascript">
    	function checkConfirmNewPass(confirmNewPassID,newPassID){

			if(confirmNewPassID == newPassID){

				document.getElementById("confirmPassID").innerHTML = "<img src='images/correct.png' alt='Confirm New Password matched' title='Confirm New Password matched' style='height:24px;margin-top: 5px;'>";

				$('#securityBtnID').prop('disabled', false);
				
			}else{

				$('#securityBtnID').prop('disabled', true);

				document.getElementById("confirmPassID").innerHTML = "<img src='images/wrong.png' alt='Confirm Password didn't match' title='Confirm New Password must be same as New Password' style='height:24px;margin-top: 5px;'>";

			}
    	}

    	function checkConfirmNewPIN(confirmNewPINID,newPINID){

			if(confirmNewPINID == newPINID){

				document.getElementById("confirmPINID").innerHTML = "<img src='images/correct.png' alt='Confirm New PIN matched' title='Confirm New PIN matched' style='height:24px;margin-top: 5px;'>";

				$('#securityBtnID').prop('disabled', false);
				
			}else{

				$('#securityBtnID').prop('disabled', true);

				document.getElementById("confirmPINID").innerHTML = "<img src='images/wrong.png' alt='Confirm PIN didn't match' title='Confirm New PIN must be same as New PIN' style='height:24px;margin-top: 5px;'>";

			}
    	}
    </script>
    
    <!-- Ends -->
    
    
    <!-- Hide Lock Modal and Security Credentials modal -->
    
    <script type="text/javascript">
    	function unlockModal(){
				
				$('#lockButtonID').click();
			
    	}

    	function hideSecCredModal(){
				
				$('#secCredCancelID').click();

    	}
    </script>
    
    <!-- Ends -->
  
    
    <!-- ABD Screening And Cross matching check -->
    
    <script type="text/javascript">
        var checkCounter = 1;
        var checkCounter1 = 1;
        var checkCounter2 = 1;
		var verifyComp = "dummy";
        
	    function check(){
			
	    	if($("#crossMatchingCheck").is(':checked') &&
    				$("#abcScreeningCheck").is(':checked')){
	    		
	    		//Removing required attribute of field ref receipt no
		    	 $("#refReceiptNoID").removeAttr("required");
		    	 $("#refRecNoSpanID").html("");

	    	 var abdScrRate = $('#abcScreeningRateID').val();
	    	 var abdScrQty = $('#abcScreeningQuantityID').val();
	    	 var crsMtchRate = $('#crossMatchRateID').val();
    		 var crsMtchQty = $('#crossMatchQuantityID').val();
    		 var compAmt = $('#dummyTotalAmt').val();
    		 var crsMtchBagNo = $('#crossMatchBagNoID').val();
    		 var abdScrBagNo = $('#babcScreeningBagNoID').val();

    		 var concessionAmt = $('#concessionAmountID').val();
			 var otherChargeAtm = $('#otherChargeID').val();
			 var netReceivableAmtID = $('#netReceivableAmtID').val();

    		 if(compAmt == ""){
    			 compAmt = 0;
    		 }
    		 if(crsMtchQty == ""){
    			 crsMtchQty = 0;
    		 }
    		 if(crsMtchRate == ""){
    			 crsMtchRate = 0;
    		 }
    		 if(abdScrRate == ""){
    			 abdScrRate = 0;
    		 }
    		 if(abdScrQty == ""){
    			 abdScrQty = 0;
    		 }
    		 if(concessionAmt == ""){
    			 concessionAmt = 0;
    		 }
    		 if(otherChargeAtm == ""){
    			 otherChargeAtm = 0;
    		 }
    		 if(crsMtchBagNo == ""){
    			 crsMtchBagNo = "";
    		 }
    		 if(abdScrBagNo == ""){
    			 abdScrBagNo = "";
    		 }
    		 if(netReceivableAmtID == "" || netReceivableAmtID == 0){
    			 netReceivableAmtID = 0;
    		 }

    		 var csrMtchAmt = parseFloat(parseFloat(crsMtchQty) * parseFloat(crsMtchRate));

    		 var abdScrAmt = parseFloat(parseFloat(abdScrRate) * parseFloat(abdScrQty));

    		 var totalAmt;

    		 var finalAmt;

    		 if(compAmt == csrMtchAmt || compAmt == abdScrAmt){

    			 totalAmt = parseFloat(parseFloat(csrMtchAmt) + parseFloat(abdScrAmt));

    			 finalAmt = parseFloat(parseFloat(csrMtchAmt) + parseFloat(abdScrAmt));

    		 }else{

        		 if(checkCounter2 == 1){

        			 totalAmt = parseFloat(parseFloat(csrMtchAmt) + parseFloat(abdScrAmt) + parseFloat(compAmt));

        			 finalAmt = parseFloat(parseFloat(netReceivableAmtID) + parseFloat(csrMtchAmt) + parseFloat(abdScrAmt));

        		 }else{

            		 if(verifyComp == "CR"){

            			 totalAmt = parseFloat(parseFloat(abdScrAmt) + parseFloat(compAmt));

            			 finalAmt = parseFloat(parseFloat(netReceivableAmtID) + parseFloat(abdScrAmt));
            			 
            		 }else{

            			 totalAmt = parseFloat(parseFloat(csrMtchAmt) + parseFloat(compAmt));

            			 finalAmt = parseFloat(parseFloat(netReceivableAmtID) + parseFloat(csrMtchAmt));
            			 
            		 }

        			 checkCounter1 = 2;

            		 checkCounter2 = 2;

        		 }            		 

    		 }
    		 checkCounter2++;
    		 
    		 $('#hiddenID').html("");

    		 //Creating input hidden element for cross matching and abd screening
    		 var hiddenTag = "<input type='hidden' name='component' value='Gr Cross Match'>"+
    		 "<input type='hidden' name='component' value='ABD Screening'>"+
    		 //"<input type='hidden' name='bagNo' value='"+crsMtchBagNo+"'>"+
    		 //"<input type='hidden' name='bagNo' value='"+abdScrBagNo+"'>"+
    		 "<input type='hidden' name='bagNo' value=''>"+
    		 "<input type='hidden' name='bagNo' value=''>"+
    		 "<input type='hidden' name='quantity' value='"+crsMtchQty+"'>"+
    		 "<input type='hidden' name='quantity' value='"+abdScrQty+"'>"+
    		 "<input type='hidden' name='rate' value='"+crsMtchRate+"'>"+
    		 "<input type='hidden' name='rate' value='"+abdScrRate+"'>"+
    		 "<input type='hidden' name='amount' value='"+csrMtchAmt+"'>"+
    		 "<input type='hidden' name='amount' value='"+abdScrAmt+"'>";

    		   $('#hiddenID').html(hiddenTag);	
    		   $('#crossMatchAmountID').val(csrMtchAmt);
    		   $('#abcScreeningAmountID').val(abdScrAmt);
	    	   $('#totalAmountID').val(parseFloat(totalAmt));
	    	   $('#dummyTotalAmt').val(parseFloat(totalAmt));
	    	   $('#netReceivableAmtID').val(finalAmt);
	    	   $('#lastNetReceivableAmtID').val(finalAmt);
	    	   $('#actualPaymentID').val(parseFloat(finalAmt));
	    	   $('#oustandingAmtID').val(parseFloat(0));
	    	   
	    	   var CRMBagTag = "<a href='javascript:viewBags(\""+crsMtchBagNo+"\");'>View bags</a>";
	    	   
	    	   var ABDBagTag = "<a href='javascript:viewBags(\""+abdScrBagNo+"\");'>View bags</a>";
	    	   
	    	   //$('#crBagBldGpID').text("");
	    	   //$('#crBagBldGpID').html(CRMBagTag);
	    	   
	    	   //$('#abdBldGpID').text("");
	    	   //$('#abdBldGpID').html(ABDBagTag);
	    	   
	    	   crmCheck++;
	    	   abdCheck++;
	    	   
	    	   ABDBagQuantityCounter = 1;
	       	   CRMBagQuantityCounter = 1;
	    	   
	    	   bagBGCheckCounterCRM = 1;
	    	   bagBGCheckCounterABD = 1;

	    	   //Disabling fields
	    	   $('#abcScreeningRateID').attr('disabled',true);
	    	   $('#abcScreeningQuantityID').attr('disabled',true);
	    	   $('#crossMatchRateID').attr('disabled',true);
	    	   $('#crossMatchQuantityID').attr('disabled',true);
	    	   $('#crossMatchBagNoID').attr('disabled',true);
	    	   $('#babcScreeningBagNoID').attr('disabled',true);
	    	   
	    	
    		}else if($("#crossMatchingCheck").is(':checked')){
    			
    			//Removing required attribute of field ref receipt no
	   	    	 $("#refReceiptNoID").removeAttr("required");
	   	    	 $("#refRecNoSpanID").html("");
		    	
	    		 var crsMtchRate = $('#crossMatchRateID').val();
	    		 var crsMtchQty = $('#crossMatchQuantityID').val();
	    		 var crsMtchBagNo = $('#crossMatchBagNoID').val();

	    		 var abdScrRate = $('#abcScreeningRateID').val();
		    	 var abdScrQty = $('#abcScreeningQuantityID').val();

	    		 var concessionAmt = $('#concessionAmountID').val();
				 var otherChargeAtm = $('#otherChargeID').val();

				 var netReceivableAmtID = $('#netReceivableAmtID').val();
	    		 
	    		 var compAmt = $('#dummyTotalAmt').val();

	    		 if(netReceivableAmtID == "" || netReceivableAmtID == 0){
	    			 netReceivableAmtID = 0;
	    		 }

	    		 if(compAmt == ""){
	    			 compAmt = 0;
	    		 }
	    		 if(crsMtchQty == ""){
	    			 crsMtchQty = 0;
	    		 }
	    		 if(compAmt == ""){
	    			 compAmt = 0;
	    		 }
	    		 if(concessionAmt == ""){
	    			 concessionAmt = 0;
	    		 }
	    		 if(otherChargeAtm == ""){
	    			 otherChargeAtm = 0;
	    		 }
	    		 if(crsMtchBagNo == ""){
	    			 crsMtchBagNo = "";
	    		 }

	    		 var totalAmt;

	    		 var finalAmt;

	    		 var csrMtchAmt = parseFloat(parseFloat(crsMtchQty) * parseFloat(crsMtchRate));

	    		 var abdScrAmt = parseFloat(parseFloat(abdScrRate) * parseFloat(abdScrQty));

	    		 if(counter == 3){

	    			 totalAmt = parseFloat(parseFloat(csrMtchAmt));

	    			 finalAmt = parseFloat(parseFloat(totalAmt));
						
	    		 }else{

		    		 if(checkCounter == 1){

		    			 totalAmt = parseFloat(parseFloat(csrMtchAmt) + parseFloat(compAmt));

		    			 finalAmt = parseFloat(parseFloat(csrMtchAmt) + parseFloat(netReceivableAmtID));

		    			 checkCounter1 = 2;

			    		 checkCounter2 = 2;

		    		 }else{

		    			 totalAmt = parseFloat(parseFloat(compAmt)) - parseInt(abdScrAmt);

		    			 finalAmt = parseFloat(parseFloat(netReceivableAmtID) - parseInt(abdScrAmt));

		    			 checkCounter1 = 2;

			    		 checkCounter2 = 2;

		    		 }

	    		 }

	    		 verifyComp = "CR";

	    		 checkCounter ++;

	    		 $('#hiddenID').html("");

	    		//Creating input hidden element for cross matching
	    		 var hiddenTag = "<input type='hidden' name='component' value='Gr Cross Match'>"+
	    		 //"<input type='hidden' name='bagNo' value='"+crsMtchBagNo+"'>"+
	    		 "<input type='hidden' name='bagNo' value=''>"+
	    		 "<input type='hidden' name='quantity' value='"+crsMtchQty+"'>"+
	    		 "<input type='hidden' name='rate' value='"+crsMtchRate+"'>"+
	    		 "<input type='hidden' name='amount' value='"+csrMtchAmt+"'>";

	    		   $('#hiddenID').html(hiddenTag);

	    		   $('#crossMatchAmountID').val(csrMtchAmt);
	    		   $('#abcScreeningAmountID').val(0);
		    	   $('#totalAmountID').val(totalAmt);
		    	   $('#dummyTotalAmt').val(totalAmt);
		    	   $('#netReceivableAmtID').val(finalAmt);
		    	   $('#lastNetReceivableAmtID').val(finalAmt);
		    	   $('#actualPaymentID').val(parseFloat(finalAmt));
		    	   $('#oustandingAmtID').val(parseFloat(0));
		    	   
		    	   var CRMBagTag = "<a href='javascript:viewBags(\""+crsMtchBagNo+"\");'>View bags</a>";
		    	   
		    	   //$('#crBagBldGpID').text("");
		    	   //$('#crBagBldGpID').html(CRMBagTag);
		    	   
		    	   var ABDBagTag = "<input  class='form-control' name='babcScreeningBagNo' readonly='readonly' data-toggle='modal' data-target='#addABDBagID' id='babcScreeningBagNoID1' placeholder='Click here to add bags' type='text'>";
		    	   
		    	   //$('#abdBldGpID').text("");
		    	   //$('#abdBldGpID').html(ABDBagTag);
		    	   
		    	   $('#babcScreeningBagNoID').val("");
		    	   
		    	   crmCheck++;
		    	   
		    	   bagBGCheckCounterCRM = 1;
		       	   CRMBagQuantityCounter = 1;

		    	 	//Disabling fields
		    	   $('#crossMatchRateID').attr('disabled',true);
		    	   $('#crossMatchQuantityID').attr('disabled',true);
		    	   $('#crossMatchBagNoID').attr('disabled',true);
		    	   
	    	    
	    	}else if($("#abcScreeningCheck").is(':checked')){
	    		
	    		//Removing required attribute of field ref receipt no
		    	 $("#refReceiptNoID").removeAttr("required");
		    	 $("#refRecNoSpanID").html("");
		    	
	    		   var abdScrRate = $('#abcScreeningRateID').val();
	    		   var abdScrQty = $('#abcScreeningQuantityID').val();
	    		   var abdScrBagNo = $('#babcScreeningBagNoID').val();

	    		   var crsMtchRate = $('#crossMatchRateID').val();
		    	   var crsMtchQty = $('#crossMatchQuantityID').val();

	    		   var concessionAmt = $('#concessionAmountID').val();
				   var otherChargeAtm = $('#otherChargeID').val();

				   var netReceivableAmtID = $('#netReceivableAmtID').val();

				   if(netReceivableAmtID == "" || netReceivableAmtID == 0){
		    			 netReceivableAmtID = 0;
		    		 }
	    		   
		    	   var compAmt = $('#dummyTotalAmt').val();

		    		 if(compAmt == ""){
		    			 compAmt = 0;
		    		 }
		    		 if(abdScrRate == ""){
		    			 abdScrRate = 0;
		    		 }
		    		 if(abdScrQty == ""){
		    			 abdScrQty = 0;
		    		 }
		    		 if(concessionAmt == ""){
		    			 concessionAmt = 0;
		    		 }
		    		 if(otherChargeAtm == ""){
		    			 otherChargeAtm = 0;
		    		 }
		    		 if(abdScrBagNo == ""){
		    			 abdScrBagNo = "";
		    		 }

		    		 var abdScrAmt = parseFloat(parseFloat(abdScrRate) * parseFloat(abdScrQty));

		    		 var csrMtchAmt = parseFloat(parseFloat(crsMtchQty) * parseFloat(crsMtchRate));

		    		 var totalAmt;

		    		 var finalAmt;

		    		 if(counter == 3){

		    			 totalAmt = parseFloat(parseFloat(abdScrAmt));

		    			 finalAmt = parseFloat(parseFloat(totalAmt));

		    		 }else{

		    			 if(checkCounter1 == 1){
			    			 
		    				 totalAmt = parseFloat(parseFloat(abdScrAmt) + parseFloat(compAmt));

		    				 finalAmt = parseFloat(parseFloat(abdScrAmt) + parseFloat(netReceivableAmtID));

		    				 checkCounter = 2;

				    		 checkCounter2 = 2;

		    			 }else{
			    			 
		    				 totalAmt = parseFloat(parseFloat(compAmt)) - parseInt(csrMtchAmt);

		    				 finalAmt = parseFloat(parseFloat(netReceivableAmtID) - parseInt(csrMtchAmt));

		    				 checkCounter = 2;

				    		 checkCounter2 = 2;

		    			 }

		    		 }

		    		 checkCounter1++;

		    		 verifyComp = "ABD";

		    		 $('#hiddenID').html("");

		    		 //Creating input hidden element for abd screening
		    		 var hiddenTag = "<input type='hidden' name='component' value='ABD Screening'>"+
		    		 //"<input type='hidden' name='bagNo' value='"+abdScrBagNo+"'>"+
		    		 "<input type='hidden' name='bagNo' value=''>"+
		    		 "<input type='hidden' name='quantity' value='"+abdScrQty+"'>"+
		    		 "<input type='hidden' name='rate' value='"+abdScrRate+"'>"+
		    		 "<input type='hidden' name='amount' value='"+abdScrAmt+"'>";

		    		   $('#hiddenID').html(hiddenTag);

		    	   $('#abcScreeningAmountID').val(abdScrAmt);
		    	   $('#crossMatchAmountID').val(0);
		    	   $('#totalAmountID').val(totalAmt);
		    	   $('#dummyTotalAmt').val(totalAmt);
		    	   $('#netReceivableAmtID').val(finalAmt);
		    	   $('#lastNetReceivableAmtID').val(finalAmt);
		    	   $('#actualPaymentID').val(parseFloat(finalAmt));
		    	   $('#oustandingAmtID').val(parseFloat(0));
		    	   
		    	   var ABDBagTag = "<a href='javascript:viewBags(\""+abdScrBagNo+"\");'>View bags</a>";
		    	   
		    	   //$('#abdBldGpID').text("");
		    	   //$('#abdBldGpID').html(ABDBagTag);
		    	   
				   var CRMBagTag = "<input  class='form-control' name='crossMatchBagNo' readonly='readonly' data-toggle='modal' data-target='#addCRMBagID' id='crossMatchBagNoID1' placeholder='Click here to add bags' type='text'>";
		    	   
		    	   //$('#crBagBldGpID').text("");
		    	   //$('#crBagBldGpID').html(CRMBagTag);

		    	   $('#crossMatchBagNoID').val("");
		    	   
		    	   abdCheck++;
		    	   bagBGCheckCounterABD = 1;
		    	   ABDBagQuantityCounter = 1;

		    	 	//Disabling fields
		    	   $('#abcScreeningRateID').attr('disabled',true);
		    	   $('#abcScreeningQuantityID').attr('disabled',true);
		    	   $('#babcScreeningBagNoID').attr('disabled',true);
		    	    
		    }else{
		    	
		    	//Add required attribute of field ref receipt no
		    	$("#refReceiptNoID").attr("required","required");
		    	$("#refRecNoSpanID").html("*");

		    	var abdScrRate = $('#abcScreeningRateID').val();
		    	var abdScrQty = $('#abcScreeningQuantityID').val();
		    	var crsMtchRate = $('#crossMatchRateID').val();
	    		var crsMtchQty = $('#crossMatchQuantityID').val();

	    		var dummyTotal = $('#dummyTotalAmt').val();

		    	var compAmt = $('#amountID').val();

		    	var concessionAmt = $('#concessionAmountID').val();
				var otherChargeAtm = $('#otherChargeID').val();

				 var netReceivableAmtID = $('#netReceivableAmtID').val();

				   if(netReceivableAmtID == "" || netReceivableAmtID == 0){
		    			 netReceivableAmtID = 0;
		    		 }

	    		 if(compAmt == ""){
	    			 compAmt = 0;
	    		 }
	    		 if(concessionAmt == ""){
	    			 concessionAmt = 0;
	    		 }
	    		 if(otherChargeAtm == ""){
	    			 otherChargeAtm = 0;
	    		 }
	    		 if(crsMtchQty == ""){
	    			 crsMtchQty = 0;
	    		 }
	    		 if(crsMtchRate == ""){
	    			 crsMtchRate = 0;
	    		 }
	    		 if(abdScrRate == ""){
	    			 abdScrRate = 0;
	    		 }
	    		 if(abdScrQty == ""){
	    			 abdScrQty = 0;
	    		 }

	    		 var csrMtchAmt = parseFloat(parseFloat(crsMtchQty) * parseFloat(crsMtchRate));

	    		 var abdScrAmt = parseFloat(parseFloat(abdScrRate) * parseFloat(abdScrQty));

	    		 var dummyAmt;

	    		 var finalAmt;

	    		 if(verifyComp == "CR"){

	    			 dummyAmt = parseFloat(parseFloat(dummyTotal) - (parseFloat(csrMtchAmt)));

	    			 finalAmt = parseFloat(parseFloat(netReceivableAmtID) - (parseFloat(csrMtchAmt)));

	    			 checkCounter1 = 1;

		    		 checkCounter = 1;

		    		 checkCounter2 = 1;

	    		 } 

	    		 if(verifyComp == "ABD"){

	    			 dummyAmt = parseFloat(parseFloat(dummyTotal) - (parseFloat(abdScrAmt)));

	    			 finalAmt = parseFloat(parseFloat(netReceivableAmtID) - (parseFloat(abdScrAmt)));

	    			 checkCounter1 = 1;

		    		 checkCounter = 1;

		    		 checkCounter2 = 1;

	    		 }

	    		 checkCounter1 = 1;

	    		 checkCounter = 1;

	    		 checkCounter2 = 1;

	    		 $('#hiddenID').html("");
	    		 $('#crossMatchAmountID').val(0);
	    		 $('#abcScreeningAmountID').val(0);
	    		 $('#totalAmountID').val(dummyAmt);
		    	 $('#dummyTotalAmt').val(dummyAmt);
		    	 $('#netReceivableAmtID').val(finalAmt);
		    	 $('#lastNetReceivableAmtID').val(finalAmt);
		    	 $('#actualPaymentID').val(parseFloat(finalAmt));
		    	 $('#oustandingAmtID').val(parseFloat(0));
		    	 
		    	 var abdScrBagNo = $('#babcScreeningBagNoID').val();
		    	 var crsMtchBagNo = $('#crossMatchBagNoID').val();
		    	 
		    	 
		    	 var CRMBagTag = "<input  class='form-control' name='crossMatchBagNo' readonly='readonly' data-toggle='modal' data-target='#addCRMBagID' id='crossMatchBagNoID1' placeholder='Click here to add bags' type='text'>";
		    	   
		    	   var ABDBagTag = "<input  class='form-control' name='babcScreeningBagNo' readonly='readonly' data-toggle='modal' data-target='#addABDBagID' id='babcScreeningBagNoID1' placeholder='Click here to add bags' type='text'>";
		    	   
		    	   //$('#crBagBldGpID').text("");
		    	   //$('#crBagBldGpID').html(CRMBagTag);
		    	   
		    	   //$('#abdBldGpID').text("");
		    	   //$('#abdBldGpID').html(ABDBagTag);
		    	   
		    	   $('#babcScreeningBagNoID').val("");
		    	   $('#crossMatchBagNoID').val("");
		    	   
		    	   crmCheck++;
		    	   abdCheck++;
		    	   bagBGCheckCounterCRM = 1;
		    	   bagBGCheckCounterABD = 1;
		    	   ABDBagQuantityCounter = 1;
		       	   CRMBagQuantityCounter = 1;

		    	//Disabling fields
		    	   $('#abcScreeningRateID').attr('disabled',false);
		    	   $('#abcScreeningQuantityID').attr('disabled',false);
		    	   $('#crossMatchRateID').attr('disabled',false);
		    	   $('#crossMatchQuantityID').attr('disabled',false);
		    	   $('#crossMatchBagNoID').attr('disabled',false);
		    	   $('#babcScreeningBagNoID').attr('disabled',false);

	    	}
		}
    </script>
    
    <!-- Ends -->
    
    
    
    <!-- Cross Match, ABD Screening and Component Quantity change function -->
    
    <script type="text/javascript">
	    function crsMtchQtyCheck(crsMtchQuantity, crsMtchRate){
	
			//var amountID = $('#amountID').val();
			
			var crsMtchAmt = $('#crossMatchAmountID').val();
	
			var abdScrAmt = $('#abcScreeningAmountID').val();
	
	    	var dummyTotal = $('#dummyTotalAmt').val();
	
			var netReceivableAmtID = $("#netReceivableAmtID").val();
	
	   		if(netReceivableAmtID == ""){
	   			netReceivableAmtID = 0;
	   		 }
	
			if($("#crossMatchingCheck").is(':checked')){
	
				if(crsMtchQuantity == "" || crsMtchRate == ""){
	
					var totalAmt = parseFloat(parseFloat(dummyTotal) - parseFloat(crsMtchAmt));
					
					var finalAmt = parseFloat(parseFloat(netReceivableAmtID) - parseFloat(crsMtchAmt));
	
					document.getElementById("crossMatchAmountID").value = 0;
	
					document.getElementById("totalAmountID").value = totalAmt;
	
	    			document.getElementById("dummyTotalAmt").value = totalAmt;
	
	    			document.getElementById("netReceivableAmtID").value = finalAmt;
	    			
	    			document.getElementById("lastNetReceivableAmtID").value = finalAmt;
	
					document.getElementById("oustandingAmtID").value = parseFloat(0);
        			
        			document.getElementById("actualPaymentID").value = parseFloat(finalAmt);
	
				}else{
	
					crsMtchAmt = parseFloat(crsMtchQuantity) * parseFloat(crsMtchRate);
	
					var totalAmt = parseFloat(parseFloat(dummyTotal) + parseFloat(crsMtchAmt));
					
					var finalAmt = parseFloat(parseFloat(netReceivableAmtID) + parseFloat(crsMtchAmt));
	
					document.getElementById("crossMatchAmountID").value = crsMtchAmt;
					
					document.getElementById("totalAmountID").value = totalAmt;
	
	    			document.getElementById("dummyTotalAmt").value = totalAmt;
	
	    			document.getElementById("netReceivableAmtID").value = finalAmt;
	    			
	    			document.getElementById("lastNetReceivableAmtID").value = finalAmt;
	
					document.getElementById("oustandingAmtID").value = parseFloat(0);
        			
        			document.getElementById("actualPaymentID").value = parseFloat(finalAmt);
	
				}
	
			}
			
		}
	
		function abdScrnQtyCheck(abdScrnQuantity, abdScrnRate){
	
			var crsMtchAmt = $('#crossMatchAmountID').val();
	
			var abdScrAmt = $('#abcScreeningAmountID').val();
	
			var dummyTotal = $('#dummyTotalAmt').val();
	
			var netReceivableAmtID = $("#netReceivableAmtID").val();
	
	   		if(netReceivableAmtID == ""){
	   			netReceivableAmtID = 0;
	   		 }
	
	   		if($("#abcScreeningCheck").is(':checked')){
	
				if(abdScrnQuantity == "" || abdScrnRate == ""){
	
					var totalAmt = parseFloat(parseFloat(dummyTotal) - parseFloat(abdScrAmt));
					
					var finalAmt = parseFloat(parseFloat(netReceivableAmtID) - parseFloat(abdScrAmt));
	
					document.getElementById("abcScreeningAmountID").value = 0;
	
					document.getElementById("totalAmountID").value = totalAmt;
	
	    			document.getElementById("dummyTotalAmt").value = totalAmt;
	
	    			document.getElementById("netReceivableAmtID").value = finalAmt;
	    			
	    			document.getElementById("lastNetReceivableAmtID").value = finalAmt;
	
					document.getElementById("oustandingAmtID").value = parseFloat(0);
        			
        			document.getElementById("actualPaymentID").value = parseFloat(finalAmt);
	
				}else{
	
					abdScrAmt = parseFloat(abdScrnQuantity) * parseFloat(abdScrnRate);
	
					var totalAmt = parseFloat(parseFloat(dummyTotal) + parseFloat(abdScrAmt));
					
					var finalAmt = parseFloat(parseFloat(netReceivableAmtID) + parseFloat(abdScrAmt));
	
					document.getElementById("abcScreeningAmountID").value = abdScrAmt;
					
					document.getElementById("totalAmountID").value = totalAmt;
	
	    			document.getElementById("dummyTotalAmt").value = totalAmt;
	
	    			document.getElementById("netReceivableAmtID").value = finalAmt;
	    			
	    			document.getElementById("lastNetReceivableAmtID").value = finalAmt;
	
					document.getElementById("oustandingAmtID").value = parseFloat(0);
        			
        			document.getElementById("actualPaymentID").value = parseFloat(finalAmt);
	
				}
	
			}
					
		}
		
		function compQtyCheck(compQuantity, compRate){
	
			var concessionAmt = $('#concessionAmountID').val();
			var otherChargeAtm = $('#otherChargeID').val();
	
			if(concessionAmt == ""){
	   			 concessionAmt = 0;
	   		 }
	   		 if(otherChargeAtm == ""){
	   			 otherChargeAtm = 0;
	   		 }
	
			if(compQuantity == ""){
				compQuantity = 0;
	
				document.getElementById("amountID").value = 0;
	    	}
	
			if(compRate == ""){
				compRate = 0;
	
				document.getElementById("amountID").value = 0;
	
	    		//var dummyTotal = $('#dummyTotalAmt').val();
	
	    	}
	
			var compAmt = parseFloat(parseFloat(compQuantity) * parseFloat(compRate));
	
			document.getElementById("amountID").value = parseFloat(compAmt);
			
		}

		function concessionAmtChange(concessionAMt,event){
			
			var key = event.which;

			if(concessionAMt == ""){
				concessionAMt = 0;
        	}

        	var netReceivableAmtID = $('#netReceivableAmtID').val();
        	
        	var lastNetReceivableAmt = $('#lastNetReceivableAmtID').val();

        	if(netReceivableAmtID == ""){
        		netReceivableAmtID = 0;
        	}
        	
        	if(lastNetReceivableAmt == ""){
        		lastNetReceivableAmt = 0;
        	}
        	
        	var finalTotalAmt = 0;
        	
        	/*
        	 *	Checking whether backspace has entered or not, if yes 
        	 *  then adding concession amount to last net receivable amount else 
             *  substracting from it.
        	 */
        	 if(key == 8){
        		 
        		 finalTotalAmt = parseFloat(parseFloat(lastNetReceivableAmt) + parseFloat(concessionAMt));
        		 
        	 }else{
        		
        		 finalTotalAmt = parseFloat(parseFloat(lastNetReceivableAmt) - parseFloat(concessionAMt));
        		 
        	 }


			//document.getElementById("totalAmountID").value = parseFloat(finalTotalAmt);
			document.getElementById("netReceivableAmtID").value = parseFloat(finalTotalAmt);
			document.getElementById("lastConcessionAmtID").value = parseFloat(concessionAMt);
			document.getElementById("oustandingAmtID").value = parseFloat(0);
			document.getElementById("actualPaymentID").value = parseFloat(finalTotalAmt);
					
		}
		
		function actualPaymentAmtChange(actualPayment, netRecvPayment){

			if(actualPayment == ""){
				actualPayment = 0;
        	}

			if(actualPayment == ""){
				actualPayment = 0;
        	}

			var outstndngAmt = parseFloat(parseFloat(netRecvPayment) - parseFloat(actualPayment));

			document.getElementById("oustandingAmtID").value = parseFloat(outstndngAmt);
			
		}

		function outstandingPaidAmtChange(outstandingAmt, outstandingPaidAmt){
			
			var actualPayment = $('#actualPaymentID').val();
			
			var netReceivableAmt = $('#netReceivableAmtID').val();

			if(outstandingPaidAmt == ""){
				outstandingPaidAmt = 0;
        	}
			
			if(netReceivableAmt == ""){
				netReceivableAmt = 0;
        	}
			
			if(actualPayment == ""){
				actualPayment = 0;
        	}
			
			var amount = parseFloat(parseFloat(netReceivableAmt) - parseFloat(actualPayment));

			if(outstandingAmt == "" || outstandingAmt == 0){
				
				$('#outstandingAmtModal').modal("show");
				
				$('#outstandingPaidAmtID').val("");
				
        	}else{

        		var outstndngAmt = parseFloat(parseFloat(amount) - parseFloat(outstandingPaidAmt));

        		document.getElementById("oustandingAmtID").value = parseFloat(outstndngAmt);

        	}
			
		}
    </script>
    
    <!-- Ends -->
    
    
    <!-- Add Bags with blood group -->
    
    <script type="text/javascript">
    
    	var bagBGCheckCounter = 1;
    	
    	var bagQuantityCounter = 1;
    	
    	var popUpCheck = "open";
    
		function addBagWithBldGrp(bagNo, bloodGroup){
    		
    		if(bloodGroup == ""){
    			bloodGroup = " ";
    		}
    		
    		var valueToAppend = "";
    		
    		if(bagBGCheckCounter != 1){
    			
    			valueToAppend =  "," + bagNo + "$" + bloodGroup;
    			
    			bagBGCheckCounter++;
    			
    		}else{
    			
    			valueToAppend = bagNo + "$" + bloodGroup;
    			
    			bagBGCheckCounter++;
    		}
    		
    		//appending values to bagNo text box
    		$('#bagNoID').val($('#bagNoID').val()+valueToAppend);
    		
    		var trID = "bgTRID"+bagBGCheckCounter;
    		
    		var nextTRTag = "<tr id='"+trID+"' style='font-size:14px;'>"+
    		"<td style='padding:5px;width:45%;'><input type='text' value='"+bagNo+"' name='' readonly='readonly' class='form-control'></td>"+
    		//"<td style='padding:5px;width:45%;'><input type='text' value='"+bloodGroup+"' name='' readonly='readonly' class='form-control'></td>"+
    		"<td><img src='images/delete.png' style='height:24px;cursor: pointer;' alt='Remove row' title='Remove row' onclick='removeBagNoBldGrpRow(\"" + valueToAppend +"\",\""+trID+"\");'/></td>"+
    		"</tr>";
    		
    		$(nextTRTag).insertAfter($('#bagWithBGID'));
    		$('#bagNoID11').attr("placeholder", "Bags added");
    		
    		//setting component quantity as bag no added
    		$('#quantityID').val(bagQuantityCounter);
    		
    		var productRate = $('#rateID').val();
    		
    		if(productRate == ""){
    			productRate = 0;
    		}
    		
    		var productAmount = parseInt(bagQuantityCounter) * parseFloat(productRate);
    		
    		//Updating product amount as per quantity
    		$('#amountID').val(productAmount);
    		
    		bagQuantityCounter++;
    		
    		$('#BagNumberID').val("");
    		$('#bloodGroupID').val("");
    		$('#BagNumberID').focus();
    		
    		popUpCheck = "open";
    		
    	}
    	
    	//function to remove row
    	function removeBagNoBldGrpRow(valueToBeRemoved, trID){
    		
    		var bagNoVal = $('#bagNoID').val();
    		var newValue = bagNoVal.replace(valueToBeRemoved,'');
    		
    		//updating value of bagNo field
    		$('#bagNoID').val(newValue);
    		
    		//removing TR
    		$("#"+trID+"").remove();
    		
    		//updating quantity
    		var qnty = $('#quantityID').val();
    		
    		var newQty = parseInt(qnty)-1;
    		
    		$('#quantityID').val(newQty);
    		
			var productRate = $('#rateID').val();
    		
    		if(productRate == ""){
    			productRate = 0;
    		}
    		
    		var productAmount = parseInt(newQty) * parseFloat(productRate);
    		
    		//Updating product amount as per quantity
    		$('#amountID').val(productAmount);
    		
    		bagQuantityCounter--;
    		
    	}
    
    </script>
    
    <!-- Ends -->
    
    
    <!-- Add Bags with blood group For cross matching -->
    
    <script type="text/javascript">
    
    	var bagBGCheckCounterCRM = 1;
    	
    	var CRMBagQuantityCounter = 1;
    
    	function addBagWithBldGrpCRM(bagNo, bloodGroup){
    		
    		if(bloodGroup == ""){
    			bloodGroup = " ";
    		}
    		
    		var valueToAppend = "";
    		
    		if(bagBGCheckCounterCRM != 1){
    			
    			valueToAppend =  "," + bagNo + "$" + bloodGroup;
    			
    			bagBGCheckCounterCRM++;
    			
    		}else{
    			
    			valueToAppend = bagNo + "$" + bloodGroup;
    			
    			bagBGCheckCounterCRM++;
    		}
    		
    		//appending values to bagNo text box
    		$('#crossMatchBagNoID').val($('#crossMatchBagNoID').val()+valueToAppend);
    		
    		var trID = "bgCRMTRID"+bagBGCheckCounterCRM;
    		
    		var nextTRTag = "<tr id='"+trID+"' style='font-size:14px;'>"+
    		"<td style='padding:5px;width:45%;'><input type='text' value='"+bagNo+"' name='' readonly='readonly' class='form-control'></td>"+
    		"<td style='padding:5px;width:45%;'><input type='text' value='"+bloodGroup+"' name='' readonly='readonly' class='form-control'></td>"+
    		"<td><img src='images/delete.png' style='height:24px;cursor: pointer;' alt='Remove row' title='Remove row' onclick='removeBagNoBldGrpCRMRow(\"" + valueToAppend +"\",\""+trID+"\");'/></td>"+
    		"</tr>";
    		
    		$(nextTRTag).insertAfter($('#bagWithBGIDCRM'));
    		$('#crossMatchBagNoID1').attr("placeholder", "Bags added");
    		
    		//setting component quantity as bag no added
    		$('#crossMatchQuantityID').val(CRMBagQuantityCounter);
    		
    		CRMBagQuantityCounter++;
    		
    		$('#BagNumberID1').val("");
    		$('#bloodGroupID1').val("");
    		$('#BagNumberID1').focus();
    	}
    	
    	//function to remove row
    	function removeBagNoBldGrpCRMRow(valueToBeRemoved, trID){
    		
    		var bagNoVal = $('#crossMatchBagNoID').val();
    		var newValue = bagNoVal.replace(valueToBeRemoved,'');
    		
    		//updating value of bagNo field
    		$('#crossMatchBagNoID').val(newValue);
    		
    		//removing TR
    		$("#"+trID+"").remove();
    		
    		//updating quantity
    		var qnty = $('#crossMatchQuantityID').val();
    		
    		var newQty = parseInt(qnty)-1;
    		
    		$('#crossMatchQuantityID').val(newQty);
    		
    		CRMBagQuantityCounter--;
    	}
    
    </script>
    
    <!-- Ends -->
    
    
    
    <!-- Add Bags with blood group for abd screening -->
    
    <script type="text/javascript">
    
    	var bagBGCheckCounterABD = 1;
    	
    	var ABDBagQuantityCounter = 1;
    
    	function addBagWithBldGrpABD(bagNo, bloodGroup){
    		
    		if(bloodGroup == ""){
    			bloodGroup = " ";
    		}
    		
    		var valueToAppend = "";
    		
    		if(bagBGCheckCounterABD != 1){
    			
    			valueToAppend =  "," + bagNo + "$" + bloodGroup;
    			
    			bagBGCheckCounterABD++;
    			
    		}else{
    			
    			valueToAppend = bagNo + "$" + bloodGroup;
    			
    			bagBGCheckCounterABD++;
    		}
    		
    		//appending values to bagNo text box
    		$('#babcScreeningBagNoID').val($('#babcScreeningBagNoID').val()+valueToAppend);
    		
    		var trID = "bgABDTRID"+bagBGCheckCounterABD;
    		
    		var nextTRTag = "<tr id='"+trID+"' style='font-size:14px;'>"+
    		"<td style='padding:5px;width:45%;'><input type='text' value='"+bagNo+"' name='' readonly='readonly' class='form-control'></td>"+
    		"<td style='padding:5px;width:45%;'><input type='text' value='"+bloodGroup+"' name='' readonly='readonly' class='form-control'></td>"+
    		"<td><img src='images/delete.png' style='height:24px;cursor: pointer;' alt='Remove row' title='Remove row' onclick='removeBagNoBldGrpABDRow(\"" + valueToAppend +"\",\""+trID+"\");'/></td>"+
    		"</tr>";
    		
    		$(nextTRTag).insertAfter($('#bagWithBGIDABD'));
    		$('#babcScreeningBagNoID1').attr("placeholder", "Bags added");
    		
    		//setting component quantity as bag no added
    		$('#abcScreeningQuantityID').val(ABDBagQuantityCounter);
    		
    		ABDBagQuantityCounter++;
    		
    		$('#BagNumberID2').val("");
    		$('#bloodGroupID2').val("");
    		$('#BagNumberID2').focus();
    	}
    	
    	//function to remove row
    	function removeBagNoBldGrpABDRow(valueToBeRemoved, trID){
    		
    		var bagNoVal = $('#babcScreeningBagNoID').val();
    		var newValue = bagNoVal.replace(valueToBeRemoved,'');
    		
    		//updating value of bagNo field
    		$('#babcScreeningBagNoID').val(newValue);
    		
    		//removing TR
    		$("#"+trID+"").remove();
    		
    		//updating quantity
    		var qnty = $('#abcScreeningQuantityID').val();
    		
    		var newQty = parseInt(qnty)-1;
    		
    		$('#abcScreeningQuantityID').val(newQty);
    		
    		ABDBagQuantityCounter--;
    	}
    
    </script>
    
    <!-- Ends -->
    
    
   <!-- Add New Row function for Component -->
    
    <script type="text/javascript">
    	var counter = 3;
    	
    	function addNewRow(dummyTotalAmt, component, bagNo, qnty, rate, amount){
        	
    			var bgNo = $('#bagNoID').val();
            	var trID = "trID"+counter;
            	var tdID = "tdID"+counter;
            	var delImgTDID = "delImgTDID"+counter;
            	var amtTDIT = "amtTDID"+counter;
				var trTag = "<tr id="+trID+" style='font-size:14px;'>"+
				"<td id="+tdID+">"+counter+"</td>"+
				"<td>"+component+"<input type='hidden' name='component' value='"+component+"'></td>";
				if(bgNo == ""){
					
					trTag += "<td style='text-align:center;'><a href='javascript:viewBags(\""+bgNo+"\");'>View bags</a><input type='hidden' name='bagNo' value=''></td>";
					
				}else{
					
					trTag += "<td style='text-align:center;'><a href='javascript:viewBags(\""+bgNo+"\");'>View bags</a><input type='hidden' name='bagNo' value='"+bgNo+"'></td>";
					
				}
				trTag += "<td>"+qnty+"<input type='hidden' name='quantity' value='"+qnty+"'></td>"+
				"<td>"+rate+"<input type='hidden' name='rate' value='"+rate+"'></td>"+
				"<td id="+amtTDIT+">"+amount+"<input type='hidden' name='amount' value='"+amount+"'></td>"+
				"<td id="+delImgTDID+"><img src='images/delete.png' style='height:24px;cursor: pointer;' alt='Remove row' title='Remove row' onclick='removeTR(\"" + trID +"\",\""+counter+"\",\""+tdID+"\",\""+dummyTotalAmt+"\",\""+amount+"\");'/></td>"+
				"</tr>";

				$(trTag).insertBefore($('#compTRID'));

				if(counter == 3){

					if($("#crossMatchingCheck").is(':checked') &&
		    				$("#abcScreeningCheck").is(':checked')){

						var concessionAmt = $('#concessionAmountID').val();
		   				var otherChargeAtm = $('#newOtherChargeAmt').val();

			       		 if(concessionAmt == ""){
			       			 concessionAmt = 0;
			       		 }
			       		 if(otherChargeAtm == ""){
			       			 otherChargeAtm = 0;
			       		 }

						var total = parseFloat(parseFloat(dummyTotalAmt) + parseFloat(amount));
						
						var finalTotal = parseFloat(parseFloat(total) - parseFloat(concessionAmt) + parseFloat(otherChargeAtm));

						document.getElementById("totalAmountID").value = parseFloat(total);

						document.getElementById("dummyTotalAmt").value = parseFloat(total);

						document.getElementById("netReceivableAmtID").value = parseFloat(finalTotal);
						
						document.getElementById("lastNetReceivableAmtID").value = parseFloat(finalTotal);

						document.getElementById("oustandingAmtID").value = parseFloat(0);
						
						document.getElementById("actualPaymentID").value = parseFloat(finalTotal);

					}else if($("#crossMatchingCheck").is(':checked')){

						var concessionAmt = $('#concessionAmountID').val();
		   				var otherChargeAtm = $('#newOtherChargeAmt').val();

			       		 if(concessionAmt == ""){
			       			 concessionAmt = 0;
			       		 }
			       		 if(otherChargeAtm == ""){
			       			 otherChargeAtm = 0;
			       		 }

						var total = parseFloat(parseFloat(dummyTotalAmt) + parseFloat(amount));
						
						var finalTotal = parseFloat(parseFloat(total) - parseFloat(concessionAmt) + parseFloat(otherChargeAtm));

						document.getElementById("totalAmountID").value = parseFloat(total);

						document.getElementById("dummyTotalAmt").value = parseFloat(total);

						document.getElementById("netReceivableAmtID").value = parseFloat(finalTotal);
						
						document.getElementById("lastNetReceivableAmtID").value = parseFloat(finalTotal);

						document.getElementById("oustandingAmtID").value = parseFloat(0);
						
						document.getElementById("actualPaymentID").value = parseFloat(finalTotal);

					}else if($("#abcScreeningCheck").is(':checked')){

						var concessionAmt = $('#concessionAmountID').val();
		   				var otherChargeAtm = $('#newOtherChargeAmt').val();

			       		 if(concessionAmt == ""){
			       			 concessionAmt = 0;
			       		 }
			       		 if(otherChargeAtm == ""){
			       			 otherChargeAtm = 0;
			       		 }

						var total = parseFloat(parseFloat(dummyTotalAmt) + parseFloat(amount));
						
						var finalTotal = parseFloat(parseFloat(total) - parseFloat(concessionAmt) + parseFloat(otherChargeAtm));

						document.getElementById("totalAmountID").value = parseFloat(total);

						document.getElementById("dummyTotalAmt").value = parseFloat(total);

						document.getElementById("netReceivableAmtID").value = parseFloat(finalTotal);
						
						document.getElementById("lastNetReceivableAmtID").value = parseFloat(finalTotal);

						document.getElementById("oustandingAmtID").value = parseFloat(0);
						
						document.getElementById("actualPaymentID").value = parseFloat(finalTotal);

					}else{

						if(dummyTotalAmt == ""){
			       			dummyTotalAmt = 0;
			       		 }
			       		 
						var total = parseFloat(parseFloat(dummyTotalAmt) + parseFloat(amount));
						
						var finalTotal = parseFloat(parseFloat(total) - parseFloat(concessionAmt) + parseFloat(otherChargeAtm));

						document.getElementById("totalAmountID").value = parseFloat(total);

						document.getElementById("dummyTotalAmt").value = parseFloat(total);

						document.getElementById("netReceivableAmtID").value = parseFloat(finalTotal);
						
						document.getElementById("lastNetReceivableAmtID").value = parseFloat(finalTotal);

						document.getElementById("oustandingAmtID").value = parseFloat(0);
						
						document.getElementById("actualPaymentID").value = parseFloat(finalTotal);

					}
					
				}else{

					var concessionAmt = $('#concessionAmountID').val();
	   				var otherChargeAtm = $('#newOtherChargeAmt').val();

		       		 if(concessionAmt == ""){
		       			 concessionAmt = 0;
		       		 }
		       		 if(otherChargeAtm == ""){
		       			 otherChargeAtm = 0;
		       		 }

		       		var total = parseFloat(parseFloat(dummyTotalAmt) + parseFloat(amount));
					
					var finalTotal = parseFloat(parseFloat(total) - parseFloat(concessionAmt) + parseFloat(otherChargeAtm));

					document.getElementById("totalAmountID").value = parseFloat(total);

					document.getElementById("dummyTotalAmt").value = parseFloat(total);

					document.getElementById("netReceivableAmtID").value = parseFloat(finalTotal);
					
					document.getElementById("lastNetReceivableAmtID").value = parseFloat(finalTotal);

					document.getElementById("oustandingAmtID").value = parseFloat(0);
					
					document.getElementById("actualPaymentID").value = parseFloat(finalTotal);

				}

				counter++;
				
				bagQuantityCounter = 1;
				
				bagBGCheckCounter = 1;
				
				popUpCheck = "close";

				//changing sr no of next row
				$("#compomentTDID").text(counter);
				$("#rateID").val("");
				$("#bagNoID").val("");
				$("#quantityID").val(1);
				$("#componentID").val("000");
				$("#amountID").val("");
				$('#bagNoID11').attr("placeholder", "Click here to add bags");
           
			
    	}

    	//Function to remove TR
    	function removeTR(trID, count, tdID, dummyTotalAmt, amount){
        	if(confirm("Are you sure you want to remove this row?")){

            	var compSrNo = $("#compomentTDID").text();

            	var array = trID.split('ID');

            	count = array[1];

            	dummyTotalAmt = $('#dummyTotalAmt').val();
            	
            	var temp = parseInt(compSrNo) - parseInt(count);

            	var temp1 = count;

            	$("#"+trID+"").remove();

            	var concessionAmt = $('#concessionAmountID').val();
   				var otherChargeAtm = $('#otherChargeID').val();

	       		 if(concessionAmt == ""){
	       			 concessionAmt = 0;
	       		 }
	       		 if(otherChargeAtm == ""){
	       			 otherChargeAtm = 0;
	       		 }

	       		var netReceivableAmtID = $('#netReceivableAmtID').val();

            	var finalAmt = parseFloat(dummyTotalAmt) - parseFloat(amount);

            	var finalAmt1 = parseFloat(netReceivableAmtID) - parseFloat(amount);

            	document.getElementById("totalAmountID").value = parseFloat(finalAmt);

    			document.getElementById("dummyTotalAmt").value = parseFloat(finalAmt);

    			document.getElementById("netReceivableAmtID").value = parseFloat(finalAmt1);
    			
    			document.getElementById("lastNetReceivableAmtID").value = parseFloat(finalAmt1);

				document.getElementById("oustandingAmtID").value = parseFloat(0);
    			
    			document.getElementById("actualPaymentID").value = parseFloat(finalAmt1);

            	if(temp == 1){
                	
                	counter = count;
                	$("#compomentTDID").text(counter);
                	
            	}else{
            		
            		for(var i = 1; i < parseInt(temp); i++){

                		var a = parseInt(count) + parseInt(i);

                		var b = parseInt(a) - 1;

                		var tempTRID = "tdID"+a;

                		var oldTrID = "trID"+a;

                		var oldTDID = "tdID"+a;

                		var newTrID = "trID" + b;

                		var newTDID = "tdID" + b;

                		var oldImgID = "delImgTDID" + a;

                		var newImgID = "delImgTDID" + b;

                		var oldAmtTDID = "amtTDID" + a;

                		var newAmtTDID = "amtTDID" + b;

                		//Fetching old amount
                		var oldAmt = $("#"+oldAmtTDID+"").text();

                		$("#"+tempTRID+"").text(temp1);

                		//changing id attributes of tr and td
                		$("#"+oldTrID+"").attr('id',newTrID);
                		$("#"+oldTDID+"").attr('id',newTDID);

                		var delImgID = "<img src='images/delete.png' style='height:24px;cursor: pointer;' alt='Remove row' title='Remove row' onclick='removeTR(\"" + newTrID +"\",\""+temp1+"\",\""+newTDID+"\",\""+dummyTotalAmt+"\",\""+oldAmt+"\");'/>";

                		//setting new image with new attributes for tr and td id while calling remoceTR method
						$("#"+oldImgID+"").html(delImgID);
						
                		//changing id attributes of delete image TD id
                		$("#"+oldImgID+"").attr('id',newImgID);

                		//changing id attributes of amount td
                		$("#"+oldAmtTDID+"").attr('id',newAmtTDID);

                		temp1++;

                		counter = temp1;
            		}
            		
            		$("#compomentTDID").text(counter);
            	}
        	}
    	}
    </script>
    
    <!-- Ends -->
    
    
    <!-- Add New Row function for Other Charges -->
    
    <script type="text/javascript">
    	var counterNew = 1;
    	function addNewRowOtherCharge(totalAmt, chargeType, chargeAmt){
    		
    			if(chargeType == "000"){
    				
    				alert('Please select charge type');
    				
    				//changing sr no of next row
					$("#otherChargeID").val("");
					$("#chargeTypeID").val("000");
    				
    			}else{
        	
	            	var trID = "otherChrgTRID"+counterNew;
	            	var tdID = "otherChrgTDID"+counterNew;
					var trTag = "<tr id="+trID+" style='font-size:14px;'>"+
					"<td colspan='4' align='right' >Other Charges</td>"+
					"<td>"+chargeType+"<input type='hidden' name='chargeType' value='"+chargeType+"'></td>"+
					"<td>"+chargeAmt+"<input type='hidden' name='otherCharge' value='"+chargeAmt+"'></td>"+
					"<td><img src='images/delete.png' style='height:24px;cursor: pointer;' alt='Remove row' title='Remove row' onclick='removeOtherChargeTR(\"" + trID +"\",\""+counterNew+"\",\""+tdID+"\",\""+totalAmt+"\",\""+chargeAmt+"\");'/></td>"+
					"</tr>";
	
					$(trTag).insertBefore($('#otherChargeTRID'));
	
					var concession = document.getElementById("concessionAmountID").value;
	
					if(concession == ""){
						concession = 0;
					}
					
					//adding chargeAmt to newChargeAmt
					var newChargeAmt =  $('#newOtherChargeAmt').val();
					
					if(newChargeAmt == ""){
						newChargeAmt = 0;
					}
					
					newChargeAmt = parseFloat(parseFloat(newChargeAmt) + parseFloat(chargeAmt));
					
					$('#newOtherChargeAmt').val(newChargeAmt);
	
					var netReceivableAmtID = $("#netReceivableAmtID").val();
					
					if(netReceivableAmtID == ""){
						netReceivableAmtID = 0;
					}
	
					var finalTotalAmt = parseFloat(parseFloat(netReceivableAmtID) + parseFloat(chargeAmt));
					
					//document.getElementById("totalAmountID").value = parseFloat(finalTotalAmt);
					document.getElementById("netReceivableAmtID").value = parseFloat(finalTotalAmt);
					document.getElementById("lastNetReceivableAmtID").value = parseFloat(finalTotalAmt);
					document.getElementById("actualPaymentID").value = parseFloat(finalTotalAmt);
					document.getElementById("oustandingAmtID").value = parseFloat(0);
					$('#newOtherChargeAmt').val(finalTotalAmt);
	
					counterNew++;
	
					//changing sr no of next row
					$("#otherChargeID").val("");
					$("#chargeTypeID").val("000");
				
    			}
           
			
    	}

    	//Function to remove TR
    	function removeOtherChargeTR(trID, count, tdID, totalAmt, chargeAmt){
        	
        	if(confirm("Are you sure you want to remove this row?")){

            	$("#"+trID+"").remove();
            	
            	var newValue = $('#newOtherChargeAmt').val();
            	
            	if(newValue == ""){
            		newValue = 0;
	       		 }

            	var concessionAmt = $('#concessionAmountID').val();
            	totalAmt = $('#totalAmountID').val();

	       		 if(concessionAmt == ""){
	       			 concessionAmt = 0;
	       		 }
	       		 
	       		 //updating newOtherChargeAmt
	       		 $('#newOtherChargeAmt').val(parseFloat(newValue) - parseFloat(chargeAmt));

	       		var netReceivableAmtID = $('#netReceivableAmtID').val();

            	//var finalAmt = parseFloat(totalAmt) - parseFloat(chargeAmt);

            	var finalAmt1 = parseFloat(netReceivableAmtID) - parseFloat(chargeAmt);

            	//document.getElementById("totalAmountID").value = parseFloat(finalAmt);

    			//document.getElementById("dummyTotalAmt").value = parseFloat(finalAmt);

    			document.getElementById("netReceivableAmtID").value = parseFloat(finalAmt1);
    			
    			document.getElementById("lastNetReceivableAmtID").value = parseFloat(finalAmt1);

    			document.getElementById("actualPaymentID").value = parseFloat(finalAmt1);
    			
    			document.getElementById("oustandingAmtID").value = parseFloat(0);


            	}
        	
        }
    </script>
    
    <!-- Ends -->
    
    
    <!-- receipt details based on patient ID -->

	<script type="text/javascript" charset="UTF-8">
		var xmlhttp;
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	
		function retrieveRefReceiptNo() {
			
			var patientID = $('#patientID').val();
		
			xmlhttp.onreadystatechange = function() {
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	
					var array = JSON.parse(xmlhttp.responseText);
					var check = 0;
					
					var tableTag = "<table border='1' style='width: 100%;'><thead><tr>"
								+ "<th style='text-align:center;padding: 5px;font-weight: bold;'>Sr. No.</th>"
								+ "<th style='text-align:center;padding: 5px;font-weight: bold;'>Receipt No.</th>"
								+ "<th style='text-align:center;padding: 5px;font-weight: bold;'>Receipt Date</th>"
								+ "<th style='text-align:center;padding: 5px;font-weight: bold;'>Net Amount</th>"
								+ "<th style='text-align:center;padding: 5px;font-weight: bold;'>Outstanding Amount</th>"
								+ "<th style='text-align:center;padding: 5px;font-weight: bold;'>ABD Screening?</th>"
								+ "<th style='text-align:center;padding: 5px;font-weight: bold;'>Cross Matching?</th>"
								+ "<th style='text-align:center;padding: 5px;font-weight: bold;'>Select</th>"
								+ "</tr></thead></tbody>";
	
						for ( var i = 0; i < array.Release.length; i++) {
							
							check = array.Release[i].check;
							
							var receiptNo = array.Release[i].receiptNo;
							
							tableTag += "<tr>"
									  + "<td style='text-align:center;padding: 5px;'>"+check+"</td>"
									  + "<td style='text-align:center;padding: 5px;'>"+receiptNo+"</td>"
									  + "<td style='text-align:center;padding: 5px;'>"+array.Release[i].receiptDate+"</td>"
									  + "<td style='text-align:center;padding: 5px;'>"+array.Release[i].netAmt+"</td>"
									  + "<td style='text-align:center;padding: 5px;'>"+array.Release[i].outstadingAmt+"</td>"
									  + "<td style='text-align:center;padding: 5px;'>"+array.Release[i].ABD+"</td>"
									  + "<td style='text-align:center;padding: 5px;'>"+array.Release[i].CR+"</td>"
									  + "<td style='text-align:center;padding: 5px;'><input type='radio' name='refRecptCheck' onclick='setRefReceiptNo(\""+receiptNo+"\");'</td>"
									  + "</tr>";
							
						}
								
								tableTag += "</tbody><table>";


					if(check == 0){
						
						$("#noABDCRErrModal").modal('show');
						
					}else{
						
						$("#receiptTblID").html(tableTag);

						$("#ABDCRDetailsModal").modal('show');

					}
					
				}
			};
			xmlhttp.open("GET", "RetrieveRefReceiptNo?patientID="
					+ patientID, true);
			xmlhttp.setRequestHeader("Content-type", "charset=UTF-8");
			xmlhttp.send();
		}
		
		
		//Function to set ref receipt no into field
		function setRefReceiptNo(receiptNo){
			
			$('#refReceiptNoID').val(receiptNo);
			
		}
		
	</script>
    
    <!-- Ends -->
    
    
    <!-- Register details while closing it -->
    
    <script type="text/javascript">
    
    var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	function retrieveCloseRegisterDetails() {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);

				for ( var i = 0; i < array.Release.length; i++) {
					
					$("#expectedEndCashID").val(array.Release[i].expectedCash);
					$("#bloodBankEndBalanceID").val(array.Release[i].bloodBankBalance);
					$("#otherProductEndBalanceID").val(array.Release[i].otherProductBalance);
					$("#bankDepositEndCashID").val(array.Release[i].totalCashDeposited);
					$("#cashAdjustedEndCashID").val(array.Release[i].totalCashAdjusted);
					$("#cashHandoverEndCashID").val(array.Release[i].cashHandover);
					$("#registerID").val(array.Release[i].registerID);
					
					$("#closeRegisterModal").modal("show");
					
				}
				
			}
		};
		xmlhttp.open("GET", "RetrieveCloseRegisterDetails?", true);
		xmlhttp.send();
	}
    
    </script>
    
    <!-- Ends -->
	
	<!-- Show cash adjustment modal -->
	
	<script type="text/javascript">
	
		function getCashAdjustmentModal(){
			
			$("#headingThree").addClass("in");
			
			getCashAdjustmentDetails();
		}
	
	</script>
	
	<!-- Ends -->
	
	
	<!-- Cash matching error calculation function -->
    
    <script type="text/javascript">
    
    	function calculateCashMatchingError(expectedCash, actualCash){
    		
    		if(expectedCash == ""){
    			expectedCash = 0;
    		}
    		
    		if(actualCash == ""){
    			actualCash = 0;
    		}
    		
    		var cashMatchingErrAmt = parseFloat(parseFloat(expectedCash) - parseFloat(actualCash));
    		
    		$("#cashMatchingEndErrorID").val(cashMatchingErrAmt);
    		
    		$("#cashMatchingEndErrorID").attr("readonly", true);
    		
			$("#cashMatchingErrorID").val(cashMatchingErrAmt);
    		
    		$("#cashMatchingErrorID").attr("readonly", true);
    		
    	}
    
    </script>
    
    <!-- Ends -->
    
    <!-- Retrieve Cash deposited  -->
    
    <script type="text/javascript" charset="UTF-8">
	var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function getBankDepositeDetails() {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);
				
				var check = 0;
				
				var tableTag = "<table border='1' style='width: 100%;'><thead><tr>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Sr. No.</th>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Cash Deposited</th>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Comment</th>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Regiser ID</th>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Action</th>"
							  +"</tr></thead><tbody>";

				for ( var i = 0; i < array.Release.length; i++) {
					
					check = array.Release[i].check;
					
					tableTag += "<tr>"
							   +"<td style='text-align: center; padding: 5px;'>"+check+"</td>"
							   +"<td style='text-align: center; padding: 5px;'>"+array.Release[i].cashDeposited+"</td>"
							   +"<td style='text-align: center; padding: 5px;'>"+array.Release[i].comment+"</td>"
							   +"<td style='text-align: center; padding: 5px;'>"+array.Release[i].registerID+"</td>"
							   +"<td style='text-align: center; padding: 5px;'><img src='images/editBill.png' title='Edit' style='height:20px;cursor: pointer;' alt='Edit' onclick='getEditBankDeposit(\""+array.Release[i].bankDepID+"\");'>"
							   +"&nbsp;<img src='images/delete_icon_1.png' alt='Delete' title='Edit' style='height:20px;cursor: pointer;' onclick='deleteBankDeposit(\""+array.Release[i].bankDepID+"\");'></td>"
							   +"</tr>";
					
				}
				
				tableTag += "</tbody></table>";
				
				if(check == 0){
					$("#depositFontID").html("No bank deposit details found. Please add new bank deposit details.");
					$("#depositDivID").html("");
				}else{
					$("#depositDivID").html(tableTag);
					$("#depositFontID").html("");
				}
				
				var btnTag = "<input type='button' class='btn btn-success' onclick='addCashDeposit(cashDepositedID.value, commentID.value);' value='Add'>";
				$("#cashDepositBtnID").html(btnTag);
				$("#cashDepositedID").val("");
				$("#commentID").val("");
				
				getCashAdjustmentDetails();

			}
		};
		xmlhttp.open("GET", "GetBankDepositeDetails", true);
		xmlhttp.send();
	}
	
	//delete cash deposit details function
	function deleteBankDeposit(cashDepID){
		
		if(confirm("Are you cure you want to delete this value?")){
			
			confirmDeleteBankDeposit(cashDepID);
			
		}
		
	}
	</script>
	
	
	<script type="text/javascript" charset="UTF-8">
	var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function getBankDepositeDetails1() {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);
				
				var check = 0;
				
				var tableTag = "<table border='1' style='width: 100%;'><thead><tr>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Sr. No.</th>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Cash Deposited</th>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Comment</th>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Regiser ID</th>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Action</th>"
							  +"</tr></thead><tbody>";

				for ( var i = 0; i < array.Release.length; i++) {
					
					check = array.Release[i].check;
					
					tableTag += "<tr>"
							   +"<td style='text-align: center; padding: 5px;'>"+check+"</td>"
							   +"<td style='text-align: center; padding: 5px;'>"+array.Release[i].cashDeposited+"</td>"
							   +"<td style='text-align: center; padding: 5px;'>"+array.Release[i].comment+"</td>"
							   +"<td style='text-align: center; padding: 5px;'>"+array.Release[i].registerID+"</td>"
							   +"<td style='text-align: center; padding: 5px;'><img src='images/editBill.png' title='Edit' style='height:20px;cursor: pointer;' alt='Edit' onclick='getEditBankDeposit(\""+array.Release[i].bankDepID+"\");'>"
							   +"&nbsp;<img src='images/delete_icon_1.png' alt='Delete' title='Delete' style='height:20px;cursor: pointer;' onclick='deleteBankDeposit(\""+array.Release[i].bankDepID+"\");'></td>"
							   +"</tr>";
					
				}
				
				tableTag += "</tbody></table>";
				
				if(check == 0){
					$("#depositFontID").html("No bank deposit details found. Please add new bank deposit details.");
					$("#depositDivID").html("");
				}else{
					$("#depositDivID").html(tableTag);
					$("#depositFontID").html("");
				}
				
				var btnTag = "<input type='button' class='btn btn-success' onclick='addCashDeposit(cashDepositedID.value, commentID.value);' value='Add'>";
				$("#cashDepositBtnID").html(btnTag);
				$("#cashDepositedID").val("");
				$("#commentID").val("");

			}
		};
		xmlhttp.open("GET", "GetBankDepositeDetails", true);
		xmlhttp.send();
	}

	</script>
    
    <!-- Ends -->
    
    
    
    <!-- Add cash deposit details -->
    
    <script type="text/javascript">
    
    var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function addCashDeposit(cashDeposited, comment) {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);
				
				var check = "error";

				for ( var i = 0; i < array.Release.length; i++) {
					
					check = array.Release[i].msg;
					
				}
				
				if(check == "success"){
					
					getBankDepositeDetails1();
					
					alert("Added successfully.");
					
				}else{
					
					alert("Failed to add cash deposit deails. Please check server logs for more details.");
					
					getBankDepositeDetails1();
					
				}

			}
		};
		xmlhttp.open("GET", "AddCashDeposit?cashDeposited="+cashDeposited+"&comment="+comment, true);
		xmlhttp.send();
	}
    
    </script>
    
    <!-- Ends -->
    
    
    
    <!-- Edit cash deposit details -->
    
    <script type="text/javascript">
    
    var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function editCashDeposit(cashDepID, cashDeposited, comment) {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);
				
				var check = "error";

				for ( var i = 0; i < array.Release.length; i++) {
					
					check = array.Release[i].msg;
					
				}
				
				if(check == "success"){
					
					getBankDepositeDetails1();
					
					alert("Updated successfully.");
					
				}else{
					
					alert("Failed to update cash deposit deails. Please check server logs for more details.");
					
					getBankDepositeDetails1();
					
				}

			}
		};
		xmlhttp.open("GET", "EditCashDeposit?cashDeposited="+cashDeposited+"&comment="+comment+"&cashDepID="+cashDepID, true);
		xmlhttp.send();
	}
    
    </script>
    
    <!-- Ends -->
    
    
    
    <!-- Delete cash deposit details -->
    
    <script type="text/javascript">
    
    var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function confirmDeleteBankDeposit(cashDepID) {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);
				
				var check = "error";

				for ( var i = 0; i < array.Release.length; i++) {
					
					check = array.Release[i].msg;
					
				}
				
				if(check == "success"){
					
					getBankDepositeDetails1();
					
					alert("Deleted successfully.");
					
				}else{
					
					alert("Failed to delete cash deposit deails. Please check server logs for more details.");
					
					getBankDepositeDetails1();
					
				}

			}
		};
		xmlhttp.open("GET", "ConfirmDeleteBankDeposit?cashDepID="+cashDepID, true);
		xmlhttp.send();
	}
    
    </script>
    
    <!-- Ends -->
    
    
    
    <!-- Render Edit cash deposit details -->
    
    <script type="text/javascript">
    
    var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function getEditBankDeposit(cashDepID) {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);
				
				var id = 0;
				var cashDep = 0;
				var comment = "";

				for ( var i = 0; i < array.Release.length; i++) {
					
					id = array.Release[i].cashDepID;
					cashDep = array.Release[i].cashDeposited;
					comment = array.Release[i].comment;
					
				}
				
				$("#cashDespIDID").val(id);
				$("#cashDepositedID").val(cashDep);
				$("#commentID").val(comment);
				
				var btnTag = "<input type='button' class='btn btn-success' onclick='editCashDeposit(\""+id+"\",cashDepositedID.value, commentID.value);' value='Update'>";
				$("#cashDepositBtnID").html(btnTag);

			}
		};
		xmlhttp.open("GET", "GetEditBankDeposit?cashDepID="+cashDepID, true);
		xmlhttp.send();
	}
    
    </script>
    
    <!-- Ends -->
    
    
    <!-- Retrieve Cash adjustment details  -->
    
    <script type="text/javascript" charset="UTF-8">
	var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function getCashAdjustmentDetails() {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);
				
				var check = 0;
				
				var tableTag = "<table border='1' style='width: 100%;'><thead><tr>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Sr. No.</th>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Cash Adjusted</th>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Reason</th>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Regiser ID</th>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Action</th>"
							  +"</tr></thead><tbody>";

				for ( var i = 0; i < array.Release.length; i++) {
					
					check = array.Release[i].check;
					
					if(check == 0){
						
						tableTag = "";
						
					}else{
						
						tableTag += "<tr>"
							   +"<td style='text-align: center; padding: 5px;'>"+check+"</td>"
							   +"<td style='text-align: center; padding: 5px;'>"+array.Release[i].cashAdjusted+"</td>"
							   +"<td style='text-align: center; padding: 5px;'>"+array.Release[i].reason+"</td>"
							   +"<td style='text-align: center; padding: 5px;'>"+array.Release[i].resgiterID+"</td>"
							   +"<td style='text-align: center; padding: 5px;'><img src='images/editBill.png' title='Edit' style='height:20px;cursor: pointer;' alt='Edit' onclick='getEditCashAdjustment(\""+array.Release[i].cashAdjID+"\");'>"
							   +"&nbsp;<img src='images/delete_icon_1.png' alt='Delete'title='Delete'  style='height:20px;cursor: pointer;' onclick='deleteCashAdjustment(\""+array.Release[i].cashAdjID+"\");'></td>"
							   +"</tr>";
						
					}
					
				}
				
				tableTag += "</tbody></table>";
				
				if(check == 0){
					$("#adjustmentFontID").html("No cash adjustment details found. Please add new cash adjustment details.");
					$("#adjustmentDivID").html("");
				}else{
					$("#adjustmentDivID").html(tableTag);
					$("#adjustmentFontID").html("");
				}
				
				var btnTag = "<input type='button' class='btn btn-success' onclick='addCashAdjustment(cashAdjustedID.value, reasonID.value);' value='Add'>";
				$("#cashAdjustmentBtnID").html(btnTag);
				
				$("#cashAdjustedID").val("");
				$("#reasonID").val("");
				
				$("#updateRegister").modal('show');

			}
		};
		xmlhttp.open("GET", "GetCashAdjustmentDetails", true);
		xmlhttp.send();
	}
	
	
	//delete cash adjustment details function
	function deleteCashAdjustment(cashAdjID){
		
		if(confirm("Are you cure you want to delete this value?")){
			
			confirmDeleteCashAdjustment(cashAdjID);
			
		}
		
	}
	
	</script>
	
	
	<script type="text/javascript" charset="UTF-8">
	var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function getCashAdjustmentDetails1() {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);
				
				var check = 0;
				
				var tableTag = "<table border='1' style='width: 100%;'><thead><tr>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Sr. No.</th>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Cash Adjusted</th>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Reason</th>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Regiser ID</th>"
							  +"<th style='text-align: center; padding: 5px; font-weight:bold;'>Action</th>"
							  +"</tr></thead><tbody>";

				for ( var i = 0; i < array.Release.length; i++) {
					
					check = array.Release[i].check;
					
					if(check == 0){
						
						tableTag = "";
						
					}else{
						
						tableTag += "<tr>"
							   +"<td style='text-align: center; padding: 5px;'>"+check+"</td>"
							   +"<td style='text-align: center; padding: 5px;'>"+array.Release[i].cashAdjusted+"</td>"
							   +"<td style='text-align: center; padding: 5px;'>"+array.Release[i].reason+"</td>"
							   +"<td style='text-align: center; padding: 5px;'>"+array.Release[i].resgiterID+"</td>"
							   +"<td style='text-align: center; padding: 5px;'><img src='images/editBill.png' title='Edit' style='height:20px;cursor: pointer;' alt='Edit' onclick='getEditCashAdjustment(\""+array.Release[i].cashAdjID+"\");'>"
							   +"&nbsp;<img src='images/delete_icon_1.png' alt='Delete' title='Delete' style='height:20px;cursor: pointer;' onclick='deleteCashAdjustment(\""+array.Release[i].cashAdjID+"\");'></td>"
							   +"</tr>";
						
					}
					
				}
				
				tableTag += "</tbody></table>";
				
				if(check == 0){
					$("#adjustmentFontID").html("No cash adjustment details found. Please add new cash adjustment details.");
					$("#adjustmentDivID").html("");
				}else{
					$("#adjustmentDivID").html(tableTag);
					$("#adjustmentFontID").html("");
				}
				
				var btnTag = "<input type='button' class='btn btn-success' onclick='addCashAdjustment(cashAdjustedID.value, reasonID.value);' value='Add'>";
				$("#cashAdjustmentBtnID").html(btnTag);
				
				$("#cashAdjustedID").val("");
				$("#reasonID").val("");

			}
		};
		xmlhttp.open("GET", "GetCashAdjustmentDetails", true);
		xmlhttp.send();
	}
	
	</script>
	
    
    <!-- Ends -->
    
    
    <!-- Add cash Adjusted details -->
    
    <script type="text/javascript">
    
    var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function addCashAdjustment(cashAdjusted, reason) {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);
				
				var check = "error";

				for ( var i = 0; i < array.Release.length; i++) {
					
					check = array.Release[i].msg;
					
				}
				
				if(check == "success"){
					
					getCashAdjustmentDetails1();
					
					alert("Added successfully.");
					
				}else{
					
					alert("Failed to add cash adjustment deails. Please check server logs for more details.");
					
					getCashAdjustmentDetails1();
					
				}

			}
		};
		xmlhttp.open("GET", "AddCashAdjustment?cashAdjusted="+cashAdjusted+"&reason="+reason, true);
		xmlhttp.send();
	}
    
    </script>
    
    <!-- Ends -->
    
    
    <!-- Edit cash Adjusted details -->
    
    <script type="text/javascript">
    
    var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function editCashAdjustment(cashAdjID, cashAdjusted, reason) {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);
				
				var check = "error";

				for ( var i = 0; i < array.Release.length; i++) {
					
					check = array.Release[i].msg;
					
				}
				
				if(check == "success"){
					
					getCashAdjustmentDetails1();
					
					alert("Updated successfully.");
					
				}else{
					
					alert("Failed to update cash adjustment deails. Please check server logs for more details.");
					
					getCashAdjustmentDetails1();
					
				}

			}
		};
		xmlhttp.open("GET", "EditCashAdjustment?cashAdjusted="+cashAdjusted+"&reason="+reason+"&cashAdjID="+cashAdjID, true);
		xmlhttp.send();
	}
    
    </script>
    
    <!-- Ends -->
    
    
    
    <!-- Delete cash Adjusted details -->
    
    <script type="text/javascript">
    
    var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function confirmDeleteCashAdjustment(cashAdjID) {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);
				
				var check = "error";

				for ( var i = 0; i < array.Release.length; i++) {
					
					check = array.Release[i].msg;
					
				}
				
				if(check == "success"){
					
					getCashAdjustmentDetails1();
					
					alert("Deleted successfully.");
					
				}else{
					
					alert("Failed to delete cash adjustment deails. Please check server logs for more details.");
					
					getCashAdjustmentDetails1();
					
				}

			}
		};
		xmlhttp.open("GET", "ConfirmDeleteCashAdjustment?cashAdjID="+cashAdjID, true);
		xmlhttp.send();
	}
    
    </script>
    
    <!-- Ends -->
    
    
    <!-- Render Edit cash adjsutment details -->
    
    <script type="text/javascript">
    
    var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function getEditCashAdjustment(cashAdjID) {
	
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

				var array = JSON.parse(xmlhttp.responseText);
				
				var id = 0;
				var cashAdj = 0;
				var reason = "";

				for ( var i = 0; i < array.Release.length; i++) {
					
					id = array.Release[i].cashAdjID;
					cashAdj = array.Release[i].cashAdjusted;
					reason = array.Release[i].reason;
					
				}
				
				$("#cashAdjIDID").val(id);
				$("#cashAdjustedID").val(cashAdj);
				$("#reasonID").val(reason);
				
				var btnTag = "<input type='button' class='btn btn-success' onclick='editCashAdjustment(\""+id+"\",cashAdjustedID.value, reasonID.value);' value='Update'>";
				$("#cashAdjustmentBtnID").html(btnTag);

			}
		};
		xmlhttp.open("GET", "GetEditCashAdjustment?cashAdjID="+cashAdjID, true);
		xmlhttp.send();
	}
    
    </script>
    
    <!-- Ends -->
    
    
    <!-- submit bill function -->
    
    <script type="text/javascript">
    
    	function submitBill(){
    		
    		if($("#refReceiptNoID").val() == "" && $("#refReceiptNoID").prop("required")){
    			$("#refRecNoErrModal").modal("show");
    			$("#refReceiptNoID").focus();
    			return false;
    		}else{
    			return true;
    		}
    		
    	}
    	
    	function closeModal(){
    		$("#refRecNoErrModal").modal("hide");
    		$("#refReceiptNoID").focus();
    	}
    
    </script>
    
    <!-- Ends -->
    
    <!-- Retrieving blood storage center name list -->
    
    <%
    	List<String> bloodStorageCenterList = billingDAOInf.retrieveBloodStorageNameList();
    %>
    
    <!-- End -->
    
  </head>

  <body class="nav-md" onload="myFun();">
  
  
  <!-- add new BSC modal -->
  
  	<div id="addNewBSCModal" class="modal fade" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
      <div class="modal-header" align="center" style="color:black;">
          <h4 class="modal-title" id="myModalLabel">ADD NEW BLOOD STORAGE CENTER</h4>
      </div>
      <form action="AddNewCourierBloodStorageCenter" method="POST">
        <div class="modal-body">
          
         <div class="row" style="padding:15px;" id="">
         	
         	<input type="text" class="form-control" name="bbStorageCenter" placeholder="Enter name here">
         	
         </div>

        </div>
        <div class="modal-footer">
        <center>
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success">Add New</button>
        </center>
        </div>
	</form>
      </div>
    </div>
  </div>
  
  <!-- Ends -->
  
  <!-- Ref. receipt no error msg modal -->
  
  <div id="refRecNoErrModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
      
        <div class="modal-body">
          
          <center>
          	<h4 style="color:black;">Please provide Ref. Receipt Number.</h4>
          </center>

        </div>
        <div class="modal-footer">
        <center>
          <button type="button" class="btn btn-success" onclick="closeModal();">OK</button>
        </center>
        </div>

      </div>
    </div>
  </div>
  
  <!-- Ends -->
  
  
  <!-- Open register Alter modal -->
  
  <div id="openRegisterAlterID" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
      
        <div class="modal-body">
          
          <center>
          	<h4 style="color:black;">Register is closed. Please ask receptionist to start the register.</h4>
          </center>

        </div>
        <div class="modal-footer">
        <center>
          <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
        </center>
        </div>

      </div>
    </div>
  </div>
  
  <div id="openRegisterAlterID1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
      
        <div class="modal-body">
          
          <center>
          	<h4 style="color:black;">Register is closed. Please ask receptionist to start the register.</h4>
          </center>

        </div>
        <div class="modal-footer">
        <center>
          <a href="LogoutUser?userID=<%= userID %>"><button type="button" class="btn btn-success">OK</button></a>
        </center>
        </div>

      </div>
    </div>
  </div>
  
  <!-- Ends -->
  
  
  <!-- close register modal -->
  
  <div id="closeRegisterModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
      
      <div class="modal-header" align="center" style="color:black;">
          <h4 class="modal-title" id="myModalLabel">CLOSE REGISTER</h4>
      </div>
      
        <div class="modal-body">
          
          <form action="CloseRegister" method="POST" id="shiftCloseForm" name="shiftCloseForm">
          
          <input type="hidden" name="cashHandover" id="cashHandoverEndCashID">
          <input type="hidden" name="cashAdjusted" id="cashAdjustedEndCashID">
          <input type="hidden" name="cashDeposited" id="bankDepositEndCashID">
          <input type="hidden" name="registerID" id="registerID">
          <input type="hidden" name="userID" value="<%=userID%>">
          
          <div class="row" style="margin-top:15px;">
          
          	  <div class="col-md-6">
	          	<font style="font-size: 14px;">Date & End time:</font>
			  </div>
          
	          <div class="col-md-6">
	          	<input type="text" name="registerDate" readonly="readonly" class="form-control" value="<%=dateFormat.format(date)%>" id="dateEndTimeID">
			  </div>
          </div>
          
          <div class="row" style="margin-top:15px;">
          	  <div class="col-md-6">
	          	<font style="font-size: 14px;">Expected Cash Balance:</font>
			  </div>
          
	          <div class="col-md-6">
	          	<input type="number" class="form-control" readonly="readonly" id="expectedEndCashID" name="expectedCash">
			  </div>
			  
			  <div class="row" style="margin-left: 15px;">
			  
			  	  <div class="col-md-6" style="margin-top:15px;">
		          	<font style="font-size: 14px;">Blood Bank Balance:</font>
				  </div>
	          
		          <div class="col-md-6" style="margin-top:15px;">
		          	<input type="number" class="form-control" style="width: 96%;" readonly="readonly" id="bloodBankEndBalanceID" name="bloodBankBalance">
				  </div>
			  
			  </div>
			  
			  <div class="row" style="margin-left: 15px;">
			  
			  	  <div class="col-md-6" style="margin-top:15px;">
		          	<font style="font-size: 14px;">Other Product Balance:</font>
				  </div>
	          
		          <div class="col-md-6" style="margin-top:15px;">
		          	<input type="number" class="form-control" style="width: 96%;" readonly="readonly" id="otherProductEndBalanceID" name="otherProductBalance">
				  </div>
			  
			  </div>
			  
          </div>
          
          <div class="row" style="margin-top:15px;">
          
          	  <div class="col-md-6">
	          	<font style="font-size: 14px;">Actual Cash Balance:</font>
			  </div>
          
	          <div class="col-md-6">
	          	<input type="number" class="form-control" id="actualCashEndBalanceID" onkeyup="calculateCashMatchingError(expectedEndCashID.value, actualCashEndBalanceID.value);" name="actualCash">
			  </div>
          </div>
          
          <div class="row" style="margin-top:15px;">
          
          	  <div class="col-md-6">
	          	<font style="font-size: 14px;">Cash matching error:</font>
			  </div>
          
	          <div class="col-md-3">
	          	<input type="number" class="form-control" readonly="readonly" id="cashMatchingEndErrorID" name="cashMatchingError">
			  </div>
			  <div class="col-md-3">
	          	<button class="btn btn-primary" type="button" onclick="getCashAdjustmentModal();">Cash Adjustment</button>
			  </div>
			  
          </div>

        </div>
        <div class="modal-footer">
        <center>
          <button type="submit" class="btn btn-success">End Register</button>
        </center>
        </form>
        </div>

      </div>
    </div>
  </div>
  
  <!-- Ends -->
  
  
  <!-- Edit Register modal -->
  
  <div id="updateRegister" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      <div class="modal-header" align="center" style="color:black;">
          <h4 class="modal-title" id="myModalLabel">UPDATE REGISTER</h4>
      </div>
      
        <div class="modal-body">
          
          <!-- start accordion -->
                    <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                      <div class="panel" id="cashDepositCollapseID">
                        <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                          <h4 class="panel-title">Cash Deposit</h4>
                        </a>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                          <div class="panel-body">
                           	
                           	<div class="row" style="margin-top: 15px;">
                           		
                           		<div class="col-md-6 col-sm-12 col-xs-12" id="" align="center" style="padding-top: 35px;">
                           			<font id="depositFontID" style="color: red; font-size: 14px;"></font>
                           			<div id="depositDivID"></div>
                           		</div>
                           		
                           		<div class="col-md-6 col-sm-12 col-xs-12">
                           			
                           			<center>
                           			<font style="font-size: 14px;font-weight: bold;">Add Cash Deposit</font>
                           			</center>
                           			
                           			<div class="row" style="margin-top:15px;">
                           			
                           				<input type="hidden" id="cashDespIDID" value="">
                           			
                           				<div class="col-md-4 col-sm-6 col-xs-6">
                           					<font style="font-size: 14px;">Cash Deposited</font>
                           				</div>
                           				
                           				<div class="col-md-8 col-sm-6 col-xs-6">
                           					<input type="number" name="" id="cashDepositedID" class="form-control">
                           				</div>
                           			
                           			</div>
                           			
                           			<div class="row" style="margin-top:15px;">
                           			
                           				<div class="col-md-4 col-sm-6 col-xs-6">
                           					<font style="font-size: 14px;">Comment</font>
                           				</div>
                           				
                           				<div class="col-md-8 col-sm-6 col-xs-6">
                           					<input type="text" name="" id="commentID" class="form-control">
                           				</div>
                           			
                           			</div>
                           			
                           			<div class="row" style="margin-top:15px;">
                           			
                           				<div class="col-md-3 col-sm-3 col-xs-6"></div>
                           				<div class="col-md-3 col-sm-3 col-xs-6"></div>
                           			
                           				<div class="col-md-3 col-sm-3 col-xs-6" align="right">
                           					<input type="reset" class="btn btn-default" value="Reset">
                           				</div>
                           				
                           				<div class="col-md-3 col-sm-3 col-xs-6" id="cashDepositBtnID">
                           					<input type="button" class="btn btn-success" value="Add" onclick="addCashDeposit(cashDepositedID.value, commentID.value);">
                           				</div> 
                           			
                           			</div>
                           			
                           		</div>
                           		
                           	</div>
                           	
                          </div>
                        </div>
                      </div>
                      <div class="panel">
                        <a class="panel-heading collapsed" role="tab" id="headingThree" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                          <h4 class="panel-title">Cash Adjustment</h4>
                        </a>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                          <div class="panel-body">
                           	<div class="row" style="margin-top: 15px;">
                           		
                           		<div class="col-md-6 col-sm-12 col-xs-12"  align="center"  style="padding-top: 35px;">
                           			<font id="adjustmentFontID" style="color: red; font-size: 14px;"></font>
                           			<div id="adjustmentDivID"></div>
                           		</div>
                           		
                           		<div class="col-md-6 col-sm-12 col-xs-12">
                           			
                           			<center>
                           			<font style="font-size: 14px;font-weight: bold;">Add Cash Adjustment</font>
                           			</center>
                           			
                           			<div class="row" style="margin-top:15px;">
                           			
                           			<input type="hidden" id="cashAdjIDID" value="">
                           			
                           				<div class="col-md-4 col-sm-6 col-xs-6">
                           					<font style="font-size: 14px;">Cash Adjusted</font>
                           				</div>
                           				
                           				<div class="col-md-8 col-sm-6 col-xs-6">
                           					<input type="number" name="" id="cashAdjustedID" class="form-control">
                           				</div>
                           			
                           			</div>
                           			
                           			<div class="row" style="margin-top:15px;">
                           			
                           				<div class="col-md-4 col-sm-6 col-xs-6">
                           					<font style="font-size: 14px;">Reason</font>
                           				</div>
                           				
                           				<div class="col-md-8 col-sm-6 col-xs-6">
                           					<input type="text" name="" id="reasonID" class="form-control">
                           				</div>
                           			
                           			</div>
                           			
                           			<div class="row" style="margin-top:15px;">
                           			
                           				<div class="col-md-3 col-sm-3 col-xs-6"></div>
                           				<div class="col-md-3 col-sm-3 col-xs-6"></div>
                           			
                           				<div class="col-md-3 col-sm-3 col-xs-6" align="right">
                           					<input type="reset" class="btn btn-default" value="Reset">
                           				</div>
                           				
                           				<div class="col-md-3 col-sm-3 col-xs-6" id="cashAdjustmentBtnID">
                           					<input type="button" class="btn btn-success" value="Add"  onclick="addCashAdjustment(cashAdjustedID.value, reasonID.value);">
                           				</div> 
                           			
                           			</div>
                           			
                           		</div>
                           		
                           	</div>
                          </div>
                        </div>
                      </div>
                    </div>
		<!-- end of accordion -->

        </div>
        <div class="modal-footer">
	        <center>
	          <button type="button" data-dismiss="modal" class="btn btn-primary">Close</button>
	        </center>
        </div>

      </div>
    </div>
  </div>
  
  <!-- Ends -->
  
  
  
  <!-- No ABD/CR matching found error msg modal -->
  
  <div id="noABDCRErrModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
      
        <div class="modal-body">
          
          <center>
          	<h4 style="color:black;">No records found for selected patient.</h4>
          </center>

        </div>
        <div class="modal-footer">
        <center>
          <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
        </center>
        </div>

      </div>
    </div>
  </div>
  
  <!-- Ends -->
  
  
  <!-- ABD/CR matching details modal -->
  
  	<div id="ABDCRDetailsModal" class="modal fade" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      <div class="modal-header" align="center" style="color:black;">
          <h4 class="modal-title" id="myModalLabel">RECEIPT DEAILS</h4>
      </div>
        <div class="modal-body">
          
         <div class="row" style="padding:5px;" id="receiptTblID">
         	
         </div>

        </div>
        <div class="modal-footer">
        <center>
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        </center>
        </div>

      </div>
    </div>
  </div>
  
  <!-- Ends -->
  
  
  <!-- Close register alert modal -->
  
  <div id="closeRegisterAlert" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
      
        <div class="modal-body">
          
          <center>
          	<h4 style="color:black;">The register is still open. Do you want to close the register?</h4>
          </center>

        </div>
        <div class="modal-footer">
        <center>
          <a href="LogoutUser"><button type="button" class="btn btn-primary">No</button></a>
          <button type="button" class="btn btn-success" onclick="getRegisterBalance();" data-dismiss="modal">Yes</button>
        </center>
        </div>

      </div>
    </div>
  </div>
  
  <!-- Ends -->
  
  
  <!-- Close Register modal -->
  
  <div id="closeRegister" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
       <div class="modal-header" align="center" style="color:black;">
          <h4 class="modal-title" id="myModalLabel">CLOSE REGISTER</h4>
      </div>
      
        <div class="modal-body">
        
        <form action="" method="POST" name="closeRegForm" id="closeRegForm">
          
           <!-- <center>
          	<h4 style="color:black;">Do you really want to close register?</h4>
          </center> -->
          
          <div class="row" style="margin-top:15px;">
          	  <div class="col-md-6">
				<font style="color:black; font-size:14px;">Cash balance: </font >
			  </div>
			  
	          <div class="col-md-6"> 
				<input type="number" name="registerBalance" readonly="readonly" id="registerBalanceID" class="form-control">
			  </div>
          </div>
          
          <div class="row" style="margin-top:15px;">
              <div class="col-md-6">
				<font style="color:black; font-size:14px;">Cash handover to *: </font >
			  </div>
			  
	          <div class="col-md-6">
	          
	          	<select name="cashHandoverToID" id="cashHandoverToID" class="form-control">
	          		<option value="000">Select cash handover to </option>
	          
	          	<%
	          		for(Integer key: receptionistList.keySet()){
	          	%>
	          
					<option value="<%=key%>"><%=receptionistList.get(key)%></option>
				
				<%
	          		}
				%>
				
				</select>
				
			  </div>
			  <input type="hidden" name="totalCash" id="totalCashID">
          </div>
          
        </form>

        </div>
        <div class="modal-footer">
        	<center>
	          <button type="button" data-dismiss="modal" class="btn btn-primary" onclick="refreshPage();">Cancel</button>
	          <button type="button" onclick="submitCloseReg();" class="btn btn-success">Close Register</button>
	        </center>
        </div>

      </div>
    </div>
  </div>
  
  <!-- Ends -->
  
  
  <!-- Close register alert modal for lock screen-->
  
  <div id="closeRegisterAlertLockScreen" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
      
        <div class="modal-body">
          
          <center>
          	<h4 style="color:black;">The register is still open. Do you want to close the register?</h4>
          </center>

        </div>
        <div class="modal-footer">
        <center>
          <a href="LogoutUser?userID=<%= userID %>"><button type="button" class="btn btn-primary">No</button></a>
          <button type="button" class="btn btn-success" onclick="getRegisterBalance();" data-dismiss="modal">Yes</button>
        </center>
        </div>

      </div>
    </div>
  </div>
  
  <!-- Ends -->
  
  
  
  <!-- Outstanding amount error modal -->
  
  <div id="outstandingAmtModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
      
        <div class="modal-body">
          
          <center>
          	<h4 style="color:black;">Not allowed to enter value.</h4>
          </center>

        </div>
        <div class="modal-footer">
        <center>
          <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
        </center>
        </div>

      </div>
    </div>
  </div>
  
  <!-- Ends -->
  
  <!-- Add Bag & Bloog Group modal -->
  
  <div id="addBagID" class="modal fade" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
      <div class="modal-header" align="center" style="color:black;">
          <h4 class="modal-title" id="myModalLabel">ADD BAGS</h4>
      </div>
        <div class="modal-body">
          
         <div class="row" style="padding:5px;">
         	<table id="bagNoTblID">
         		<tr id="bagWithBGID">
         			<td style="padding:5px;width:45%;"><input type="text" placeholder="Scan Bag No." name="bagNo2" id="BagNumberID" class="form-control" autofocus> 
         			<input type="hidden" name="bloodGroup1" id="bloodGroupID" placeholder="Blood Group" class="form-control" readonly="readonly"></td>
         			<!-- <td style="padding:5px;width:45%;">
         			
         			</td>  -->
         			<td style="width:10%;"><img src="images/addBill.png" style="height:24px;cursor: pointer;" onclick="addBagWithBldGrp(BagNumberID.value, bloodGroupID.value);" 
						onmouseover="this.src='images/addBill1.png'" onmouseout="this.src='images/addBill.png'" alt="Add Bag"
						title="Add Bag" /></td>
         		</tr>
         	</table>
         </div>

        </div>
        <div class="modal-footer">
        <center>
          <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
        </center>
        </div>

      </div>
    </div>
  </div>
  
  <!-- Ends -->
  
  <!-- Add Bag & Bloog Group modal for Cross matching -->
  
  <div id="addCRMBagID" class="modal fade" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
      <div class="modal-header" align="center" style="color:black;">
          <h4 class="modal-title" id="myModalLabel">ADD BAGS</h4>
      </div>
        <div class="modal-body">
          
         <div class="row" style="padding:5px;">
         	<table id="crmBagTRID">
         		<tr id="bagWithBGIDCRM">
         			<td style="padding:5px;width:45%;"><input type="text" placeholder="Scan Bag No." name="bagNo2" id="BagNumberID1" class="form-control" autofocus> </td>
         			<td style="padding:5px;width:45%;">
         			<input type="text" name="bloodGroup1" id="bloodGroupID1" placeholder="Blood Group" class="form-control" readonly="readonly"></td>
         			<td style="width:10%;"><img src="images/addBill.png" style="height:24px;cursor: pointer;" onclick="addBagWithBldGrpCRM(BagNumberID1.value, bloodGroupID1.value);" 
						onmouseover="this.src='images/addBill1.png'" onmouseout="this.src='images/addBill.png'" alt="Add Bag"
						title="Add Bag" /></td>
         		</tr>
         	</table>
         </div>

        </div>
        <div class="modal-footer">
        <center>
          <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
        </center>
        </div>

      </div>
    </div>
  </div>
  
  <!-- Ends -->
  
  <!-- Add Bag & Bloog Group modal for ABD screening -->
  
  <div id="addABDBagID" class="modal fade" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
      <div class="modal-header" align="center" style="color:black;">
          <h4 class="modal-title" id="myModalLabel">ADD BAGS</h4>
      </div>
        <div class="modal-body">
          
         <div class="row" style="padding:5px;">
         	<table id="abdBagTRID">
         		<tr id="bagWithBGIDABD">
         			<td style="padding:5px;width:45%;"><input type="text" placeholder="Scan Bag No." name="bagNo2" id="BagNumberID2" class="form-control" autofocus> </td>
         			<td style="padding:5px;width:45%;">
         			<input type="text" name="bloodGroup1" id="bloodGroupID2" placeholder="Blood Group" class="form-control" readonly="readonly"></td>
         			<td style="width:10%;"><img src="images/addBill.png" style="height:24px;cursor: pointer;" onclick="addBagWithBldGrpABD(BagNumberID2.value, bloodGroupID2.value);" 
						onmouseover="this.src='images/addBill1.png'" onmouseout="this.src='images/addBill.png'" alt="Add Bag"
						title="Add Bag" /></td>
         		</tr>
         	</table>
         </div>

        </div>
        <div class="modal-footer">
        <center>
          <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
        </center>
        </div>

      </div>
    </div>
  </div>
  
  <!-- Ends -->
  
  
  <!-- View Bag & Bloog Group modal -->
  
  <div id="viewBagID" class="modal fade" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
      <div class="modal-header" align="center" style="color:black;">
          <h4 class="modal-title" id="myModalLabel">BAG DETAILS</h4>
      </div>
        <div class="modal-body">
          
         <div class="row" id="bagDetailID" style="padding:5px;">
         	
         </div>

        </div>
        <div class="modal-footer">
        <center>
          <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
        </center>
        </div>

      </div>
    </div>
  </div>
  
  <!-- Ends -->
  
  
  <!-- Logout modal -->
  
  <div id="logoutModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
      <div class="modal-header" align="center" style="color:black;">
          <h4 class="modal-title" id="myModalLabel">LOGOUT</h4>
      </div>
      
        <div class="modal-body">
          
          <center>
          	<h4 style="color:black;">You will lose any unsaved data. Are you sure you want to log out?</h4>
          </center>

        </div>
        <div class="modal-footer">
        <center>
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
          
          	<%
          	if(form.getUserRole().equals("administrator") || form.getUserRole().equals("courier")){
          %>
            
            <a href="LogoutUser?userID=<%= userID %>"><button type="button" class="btn btn-success">OK</button></a>
            
          <%
          	}else{
          %>
          
            <a href="javascript:retrieveCloseRegisterDetails();"><button type="submit" class="btn btn-success">OK</button></a>
            
          <%
          	}
          %>
        </center>
        </div>

      </div>
    </div>
  </div>
  
  <!-- Ends -->
  
  <!-- Lock modal -->
  
  <div id="lockModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
       <div class="modal-header" align="center" style="color:black;">
          <h4 class="modal-title" id="myModalLabel">SCREEN LOCKED</h4>
      </div>
      
        <div class="modal-body">
          
          <center style="margin-bottom: 20px;">
          	<font style="color:black;font-size: 16px;">Enter your PIN to onlock the screen</font>
          </center>
          
          <div class="row">
	          <div class="col-md-10 col-sm-10 col-xs-8">
	          	<input type="password" name="lockPIN" id="lockPIN" onkeyup="checkUnlockPIN(lockPIN.value);" class="form-control" placeholder="Enter your PIN here">
	          </div>
	          <div class="col-md-2 col-sm-2 col-xs-4" id="lockImgID" style="margin-left:-15px;">
	          	
	          </div>
          </div>

        </div>
        <div class="modal-footer">
        <center>
        
        <%
          	if(form.getUserRole().equals("administrator") || form.getUserRole().equals("courier")){
          %>
            
            <a href="LogoutUser?userID=<%= userID %>"><button type="button" class="btn btn-primary">Logout</button></a>
            
          <%
          	}else{
          %>
          
            <a href="javascript:retrieveCloseRegisterDetails();"><button type="submit" class="btn btn-primary">Logout</button></a>
            
          <%
          	}
          %>
          <button type="button" id="lockButtonID" class="btn btn-primary" data-dismiss="modal" style="display: none;"></button>
        </center>
        </div>

      </div>
    </div>
  </div>
  
  <!-- Ends -->
  
  <!-- Security Credential modal -->
  
  <div id="securityModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
      <div class="modal-header" align="center" style="color:black;">
          <h4 class="modal-title" id="myModalLabel">CHANGE SECURITY CREDENTIALS</h4>
      </div>
      
        <div class="modal-body">
          
          <!-- start accordion -->
                    <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                      <div class="panel">
                        <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                          <h4 class="panel-title">Change Password</h4>
                        </a>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                          <div class="panel-body">
                           <div class="row" style="margin-bottom:15px;">
                           		<div class="col-md-4">
                           			Old Password
                           		</div>
                           		<div class="col-md-7">
                           			<input type="password" name="oldPassword" id="oldPassword" class="form-control" onkeyup="verifyOldPass(oldPassword.value);">
                           		</div>
                           		<div class="col-md-1" id="oldPassID">
                           			
                           		</div>
                           	</div>
                           		
                           	<div class="row" style="margin-bottom:15px;">
                           		<div class="col-md-4">
                           			New Password
                           		</div>
                           		<div class="col-md-7">
                           			<input type="password" name="newPassword" id="newPassID" disabled="disabled" onkeyup="newPassCheck(newPassID.value);" class="form-control">
                           		</div>
                           		<div class="col-md-1" id="newPassCheckID">
                           			
                           		</div>
                           	</div>
                           		
                           	<div class="row" style="margin-bottom:15px;">
                           		<div class="col-md-4">
                           			Confirm New Password
                           		</div>
                           		<div class="col-md-7">
                           			<input type="password" name="confirmNewPassword" id="confirmNewPassID" disabled="disabled" onkeyup="checkConfirmNewPass(confirmNewPassID.value,newPassID.value);" class="form-control">
                           		</div>
                           		<div class="col-md-1" id="confirmPassID">
                           			
                           		</div>
                           </div>
                          </div>
                        </div>
                      </div>
                      <div class="panel">
                        <a class="panel-heading collapsed" role="tab" id="headingThree" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                          <h4 class="panel-title">Change PIN</h4>
                        </a>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                          <div class="panel-body">
                           	<div class="row" style="margin-bottom:15px;">
                           		
                           		<div class="col-md-4">
                           			New PIN
                           		</div>
                           		<div class="col-md-7">
                           			<input type="password" name="newPIN" id="newPINID" class="form-control">
                           		</div>
                           		
                           	</div>
                           	
                           	<div class="row" style="margin-bottom:15px;">
                           		
                           		<div class="col-md-4">
                           			Confirm New PIN
                           		</div>
                           		<div class="col-md-7">
                           			<input type="password" name="confirmNewPIN" id="confirmNewPINID" onkeyup="checkConfirmNewPIN(confirmNewPINID.value,newPINID.value);" class="form-control">
                           		</div>
                           		<div class="col-md-1" id="confirmPINID">
                           			
                           		</div>
                           		
                           </div>
                          </div>
                        </div>
                      </div>
                    </div>
		<!-- end of accordion -->

        </div>
        <div class="modal-footer">
	        <center>
	          <button type="button" data-dismiss="modal" id="secCredCancelID" class="btn btn-primary">Cancel</button>
	          <button type="button" style="width:25%" id="securityBtnID" onclick="updateSecurityCredentials(newPassID.value, newPINID.value);" class="btn btn-success" disabled="disabled" data-dismiss="modal">Update</button>
	        </center>
        </div>

      </div>
    </div>
  </div>
  
  <!-- Ends -->
  
 
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.jsp" class="site_title"> <span>RudhiraKosh!</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3 style="color:#2A3F54; text-shadow: none;">a</h3>
                <ul class="nav side-menu">
                  <li><a onclick="windowOpen();"><i class="fa fa-tachometer"></i> Dashboard <span class="fa"></span></a>
                  </li>
                  <%
                  	if(form.getUserRole().equals("administrator") || form.getUserRole().equals("receptionist")){
                  		
                  		if((form.getUserRole().equals("administrator") && check == 0)){
                  %>
                  
                  <li><a><i class="fa fa-desktop"></i> Accounts <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="#openRegisterAlterID" data-toggle="modal">New Bill</a></li>
                      <li><a href="#openRegisterAlterID" data-toggle="modal">View Bills</a></li>
                      <li><a href="#openRegisterAlterID" data-toggle="modal">Other product</a></li>
                      <li><a href="#openRegisterAlterID" data-toggle="modal">View Other product</a></li>
                    </ul>
                  </li>
                  
                  <%
                  		}else{
                  %>
                  <li><a><i class="fa fa-desktop"></i> Accounts <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="RenderNewBill">New Bill</a></li>
                      <li><a href="viewBillList.jsp">View Bills</a></li>
                      <li><a href="otherProduct.jsp">Other product</a></li>
                      <li><a href="viewOtherProducts.jsp">View Other product</a></li>
                    </ul>
                  </li>
                  <%
                  		}
                  	}
                  	
                  	if(form.getUserRole().equals("administrator") || form.getUserRole().equals("courier")){
                  		
                  		if((form.getUserRole().equals("administrator") && check == 0)){
                  %>
                  
                  <li><a><i class="fa fa-truck"></i> Courier <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="#openRegisterAlterID" data-toggle="modal">New Bill</a></li>
                      <li><a href="#openRegisterAlterID" data-toggle="modal">View Bills</a></li>
                    </ul>
                  </li>
                  
                  <%
                  		}else{
                  %>
                  <li><a><i class="fa fa-truck"></i> Courier <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="RenderNewBillCourier">New Bill</a></li>
                      <li><a href="RenderViewBill">View Bills</a></li>
                    </ul>
                  </li>
                  <%
                  		}
                  	}
                  %>
                  <li><a><i class="fa fa-file-text-o"></i> Reports <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="patientReport.jsp">Patient-based Reports</a></li>
                      <li><a href="RenderExportStorageCenter">Storage Center Reports</a></li>
                      <li><a href="RenderComponentReport">Component-based Reports</a></li>
                      <li><a href="RenderConcessionReport">Concession-based Reports</a></li>
                      <li><a href="registerReport.jsp">Register Reports</a></li>
                      <li><a href="exportForAccountsReport.jsp">Export for Accounts</a></li>
                      <li><a href="cardChequeReport.jsp">Card/Cheque Report</a></li>
                    </ul>
                  </li>
                  <%
                  	if(form.getUserRole().equals("administrator")){
                  %>
                  <li><a><i class="fa fa-cogs"></i>Administration<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a>Users<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li class="sub_menu"><a href="RenderAddUser">Add Users</a>
                            </li>
                            <li class="sub_menu"><a href="editUserList.jsp">Edit Users</a>
                            </li>
                           </ul>
                        </li>
                        <li><a>Blood Bank<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li class="sub_menu"><a href="addBloodBank.jsp">Add Blood Bank</a>
                            </li>
                            <li class="sub_menu"><a href="editBloodBankList.jsp">Edit Blood Bank</a>
                            </li>
                           </ul>
                        </li>
                        <li><a>Configurations<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li class="sub_menu"><a href="configureComponents.jsp">Components</a>
                            </li>
                            <li class="sub_menu"><a href="configureConcession.jsp">Concessions</a>
                            </li>
                            <!-- <li class="sub_menu"><a href="configureInstructions.jsp">Instructions</a>
                            </li> -->
                            <li class="sub_menu"><a href="configureOtherCharges.jsp">Other Charges</a>
                            </li>
                            <!-- <li class="sub_menu"><a href="configureShifts.jsp">Shifts</a>
                            </li> -->
                          </ul>
                        </li>
					</ul>
                  </li>   
                  <%
                  	}
                  %>     
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->
            
            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" style="color:#73879C;" data-placement="top" title="Security Credentials" onclick="showSecurityModal();">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" style="color:#73879C;" data-placement="top" title="FullScreen" onclick="widnwoFullScreen();">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" style="color:#73879C;" data-placement="top" title="Lock" onclick="showLockModal();">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" style="color:#73879C;" data-placement="top" title="Logout" onclick="showLogoutModal();">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
            
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  	<%
              		if(userDAOInf.retrieveProfilePicName(form.getUserID()) == null || userDAOInf.retrieveProfilePicName(form.getUserID()) == ""){
	              	%>
	              
	                 <img src="images/user.png" alt="Profile Pic"><%= form.getFullname() %>
	                
	                <%
	              		}else{
	                %>
	                
	                 <img src="<%= userDAOInf.retrieveProfilePicName(form.getUserID()) %>" alt="Profile Pic"><%= form.getFullname() %>
	                
	                <%
	              		}
	                %>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="RenderEditProfile"> Profile</a></li>
                    <li><a href="DownloadManual"> Help</a></li>
                    <%
			          	if(form.getUserRole().equals("administrator") || form.getUserRole().equals("courier")){
			          %>
			          
			            <li><a href="LogoutUser?userID=<%= userID %>"><i class="fa fa-sign-out pull-right"></i>Log Out</a></li>
			            
			          <%
			          	}else{
			          %>
			          
			            <li><a href="javascript:retrieveCloseRegisterDetails();"><i class="fa fa-sign-out pull-right"></i>Log Out</a></li>
			            
			          <%
			          	}
			          %>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                <div class="page-title">
                      <div class="title_left">
                        <h3>NEW BILL</h3>
                      </div>
                    </div>

                    <div class="ln_solid" style="margin-top:0px;"></div>
                    
                    <form action="AddNewCourierBill" class="form-horizontal" method="POST">
                    
                    <div>
		              	<s:if test="hasActionErrors()">
							<center>
							<font style="color: red; font-size:16px;"><s:actionerror /></font>
							</center>
						</s:if><s:else>
							<center>
							<font style="color: green;font-size:16px;"><s:actionmessage /></font>
							</center>
						</s:else>
		              </div>
                    
                    <div class="row">
                    	<div class="col-md-2 col-sm-4 col-xs-6" align="left">
                    		<font style="font-size: 16px;font-weight:bold;" >Receipt Type: </font>
                    	</div>
                    	<!-- <div class="col-md-2 col-sm-4 col-xs-6" align="left">
                    		<font style="font-size: 15px;" >Courier </font>
                    		<input type="hidden" name="receiptType" value="CR">
                    	</div>-->
                    </div>
                    
                    <div class="row" style="margin-top:15px;">
                    	<div class="col-md-2 col-sm-4 col-xs-6">
                    		<input type="radio" name="receiptType" required="required" style="height: 20px;box-shadow: none;" id="receiptTypeID" onclick="retrieveReceiptNo('CPT');" value="CPT" class="form-control">
                    		<center style="margin-top:5px;"><font style="font-size: 16px;" >Courier-Patient</font></center>
                    	</div>
                    	<div class="col-md-2 col-sm-4 col-xs-6">
                    		<input type="radio" name="receiptType" required="required" style="height: 20px;box-shadow: none;" id="receiptTypeID1" onclick="retrieveReceiptNo('CBSC');" value="CBSC" class="form-control">
                    		<center style="margin-top:5px;"><font style="font-size: 16px;" >&nbsp;Courier-Blood Storage Center</font></center>
                    	</div>
                    </div>
                    
                    <div class="ln_solid"></div>
                     
                  <div class="x_content">
                    
                    <div class="row">
                     	<div class="col-md-3">
                     		<font style="font-size: 14px;"><b>Non Refundable</b></font>
                     	</div>
                     	<div class="col-md-6" align="center">
                     		<h3 style="margin-top:0px;"><%=userDAOInf.retrieveBloodBankName(form.getBloodBankID()) %></h3>
                     	</div>
                     	<div class="col-md-3"></div>
                     </div>
                     
                     <div class="row" style="margin-top:10px;">
                     	<div class="col-md-3">
                     		<font style="font-size: 16px;"><b>Reg No <%=userDAOInf.retrieveBloodBankRegNo(form.getBloodBankID()) %></b></font>
                     	</div>
                     	<div class="col-md-5">
                     		<font style="font-size: 16px;"><b><%=userDAOInf.retrieveBloodBankAddress(form.getBloodBankID()) %></b></font>
                     	</div>
                     	<div class="col-md-4">
                     		<font style="font-size: 16px;"><b>Phone: <%=userDAOInf.retrieveBloodBankPhone(form.getBloodBankID()) %></b></font>
                     	</div>
                     </div>
                     
                     <div class="ln_solid"></div>
                     
                      <div class="row">
                     	<div class="col-md-4">
                     		<font style="font-size: 16px;">Receipt Date & Time <b><%=dateFormat.format(date) %></b></font>
                     	</div>
                     	<div class="col-md-2">
                     		<font style="font-size: 16px;">Receipt No*</font>
                     	</div>
                     	<div class="col-md-2">
                     		<input  class="form-control" name="receiptNo" value="" required="required" placeholder="Receipt No" id="receiptNoID" type="text">
                     	</div>
                     	<div class="col-md-2">
                     		<font style="font-size: 16px;">Manual Receipt No</font>
                     	</div>
                     	<div class="col-md-2">
                     		<div id=""><input type="text" name="manualReceiptNo" class="form-control"> </div>
                     	</div>
                     </div>
                     
                     <div id="patientDetailID">
                     
                     <div class="row" style="margin-top:15px;">
                     	<div class="col-md-2">
                     		<font style="font-size: 16px;">Patient Name</font>
                     	</div>
                     	<div class="col-md-3">
                     		<input  class="form-control" name="firstName" readonly="readonly" id="firstNameID" required="required" placeholder="First Name" type="text">
                     	</div>
                     	<div class="col-md-3">
                     		<input  class="form-control" name="middleName" readonly="readonly" id="middleNameID" placeholder="Middle Name" type="text">
                     	</div>
                     	<div class="col-md-3">
                     		<input  class="form-control" name="lastName" readonly="readonly" id="lastNameID" required="required" placeholder="Last Name" type="text">
                     	</div>
                     </div>
                     
                     <div class="row" style="margin-top:15px;">
                     	<div class="col-md-2">
                     		<font style="font-size: 16px;">Patient ID*</font>
                     	</div>
                     	<div class="col-md-4">
                     		<input  class="form-control" name="patientIDNo" readonly="readonly" id="patientID" required="required" placeholder="Patient ID" type="text">
                     	</div>
                     	<div class="col-md-2">
                     		<font style="font-size: 16px;">Hospital</font>
                     	</div>
                     	<div class="col-md-4">
                     		<input  class="form-control" name="hospital" readonly="readonly" id="hospitalID" placeholder="Hospital" type="text">
                     	</div>
                     </div>
                     
                     <div class="row" style="margin-top:15px;">
                     	<div class="col-md-2">
                     		<font style="font-size: 16px;">Address</font>
                     	</div>
                     	<div class="col-md-4">
                     	    <textarea rows="3" class="form-control" name="address"  class="form-control" readonly="readonly" id="addressID" placeholder="Address"></textarea>
                     	</div>
                     	<div class="col-md-2">
                     		<font style="font-size: 16px;">BDR No</font>
                     	</div>
                     	<div class="col-md-3">
                     		<input  class="form-control" name="BBRNo" placeholder="BDR No" id="bdrNoID" onkeyup="getPatientDetail(bdrNoID.value);" type="text">
                     	</div>
                     	<div class="col-md-1" id="bdrCheckID">
                     		
                     	</div>
                     </div>
                     
                     <div class="row" style="margin-top:15px;">
                     	<div class="col-md-2">
                     		<font style="font-size: 16px;">Mobile No.</font>
                     	</div>
                     	<div class="col-md-4">
                     	    <input  class="form-control" name="mobile" id="mobileID" placeholder="Patient Mobile No." type="text">
                     	</div>
                     	<div class="col-md-2">
                     		<font style="font-size: 16px;">Charity Case No.</font>
                     	</div>
                     	<div class="col-md-4">
                     	    <input  class="form-control" name="charityCaseNo" placeholder="Charity Case No." type="text">
                     	</div>
                     </div>
                     
                     </div>
                     
                     <div id="BBStorageID" style="display:none;">
                     
                     	<div class="row" style="margin-top:15px;">
                     		<div class="col-md-3">
                     			<font style="font-size: 16px;">Blood Bank Storage Center*</font>
                     		</div>
                     		
                     		<div class="col-md-3">
                     			<!-- <input  class="form-control" name="bbStorageCenter" required="required" onkeyup="enableSubmit(bbStorageCenterID.value);" placeholder="Blood Bank Storage Center" id="bbStorageCenterID" type="text"> -->
                     			<select name="bbStorageCenter" class="form-control" onchange="enableSubmit(bbStorageCenterID.value);" id="bbStorageCenterID">
                     				<option value="000">Select Blood Storage Center</option>
                     				<%
                     					for(String bscName : bloodStorageCenterList){
                     				%>
                     				<option value="<%=bscName%>"><%=bscName%></option>
                     				<%
                     					}
                     				%>
                     			</select>
                     		</div>
                     		
                     		<div class="col-md-3">
	                     		<button class="btn btn-default" data-target="#addNewBSCModal" data-toggle="modal">Add New Storage Center</button>
	                     	</div>
                     		
                     	</div>
                     	
                     	<div class="row" style="margin-top:15px;">
                     	
                     		<div class="col-md-3">
	                     		<font style="font-size: 16px;">BDR No</font>
	                     	</div>
	                     	
	                     	<div class="col-md-3">
	                     		<input  class="form-control" name="storageBBRNo" placeholder="BDR No" id="bdrNoID1" type="text">
	                     	</div>
	                     	<div class="col-md-1" id="bdrCheckID">
	                     	</div>
                     	
                     	</div>
                     
                     </div>
                     
                      <div class="ln_solid"></div>
                      <div id="hiddenID"></div>
                      <div id="hiddenOtherChargeID"></div>

		              
		              <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Sr.No</th>
                          <th>Product Req.</th>
                          <th>Bag No</th>
                          <th>Qty</th>
                          <th>Rate</th>
                          <th>Amt</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                      	
                      	
                      	   <tr>
                         			<td style="font-size:14px;">1</td>
						
									<td><input  class="form-control" name="crossMatch" id="crossMatchID" value="Gr Cross Match" readonly="readonly" type="text"></td>
										
									<td id="crBagBldGpID" style="font-size:14px;text-align:center;"><!-- <input  class="form-control" name="crossMatchBagNo" readonly="readonly" data-toggle="modal" data-target="#addCRMBagID" id="crossMatchBagNoID1" placeholder="Click here to add bags" type="text"> --> </td>
									
									<td><input  class="form-control" name="crossMatchQuantity" value="1" onblur="crsMtchQtyCheck(crossMatchQuantityID.value, crossMatchRateID.value);" onfocus="this.value='0'" onblur="this.value" id="crossMatchQuantityID" placeholder="Quantity" type="number">
									<input type="hidden" id="crossMatchBagNoID"></td>
									
									<td><input  class="form-control" name="crossMatchRate" id="crossMatchRateID" placeholder="Rate" onblur="crsMtchQtyCheck(crossMatchQuantityID.value, crossMatchRateID.value);" value="<%=billingDAOInf.retrieveCrossMatchingRate() %>" type="number"></td>
									
									<td><input  class="form-control" name="crossMatchAmount" id="crossMatchAmountID" placeholder="Amount" readonly="readonly" type="text"></td>
									
									<td><input type="checkbox" id="crossMatchingCheck" class="form-control" style="margin-top:0px;" onclick="check();"></td>
									
                        	</tr>
                        	
                        	<tr >
                         			<td style="font-size:14px;">2</td>
						
									<td><input  class="form-control" name="abcScreening" id="abcScreeningID" value="ABD Screening" readonly="readonly" type="text"></td>
										
									<td id="abdBldGpID" style="font-size:14px;text-align:center;"><!--  <input  class="form-control" name="babcScreeningBagNo" readonly="readonly" data-toggle="modal" data-target="#addABDBagID" id="babcScreeningBagNoID1" placeholder="Click here to add bags" type="text"> --></td>
									
									<td><input  class="form-control" name="abcScreeningQuantity" id="abcScreeningQuantityID" onblur="abdScrnQtyCheck(abcScreeningQuantityID.value, abcScreeningRateID.value);" placeholder="Quantity" value="1" onfocus="this.value='0'" onblur="this.value" type="number">
									<input type="hidden" id="babcScreeningBagNoID"></td>
									
									<td><input  class="form-control" name="abcScreeningRate" id="abcScreeningRateID" onblur="abdScrnQtyCheck(abcScreeningQuantityID.value, abcScreeningRateID.value);" placeholder="Rate" value="<%=billingDAOInf.retrieveABDScreeningRate() %>" type="number"></td>
									
									<td><input  class="form-control" name="abcScreeningAmount" id="abcScreeningAmountID" placeholder="Amount" readonly="readonly" type="text"></td>
									
									<td><input type="checkbox" id="abcScreeningCheck" class="form-control" style="margin-top:0px;" onclick="check();"></td>
									
                        	</tr>
                        	
                        	
                        	<tr id="compTRID">
                         			<td id="compomentTDID" style="font-size:14px;">3</td>
						
									<td><select class="form-control" name="component1" id="componentID" onchange="getComponentRate(componentID.value);">
									<option value="000">Select Component</option>
									<%
										Iterator<String> iterator = componentList.iterator();
									
										while(iterator.hasNext()){
											
											String value = iterator.next();
									%>
									
										<option value="<%=value %>"><%=value %></option>
										
									<%
									}
									%>
									</select></td>
										
									<td><input  class="form-control" name="bagNoID" id="bagNoID11" style="cursor: pointer;" readonly="readonly" placeholder="Click here to add bags" data-toggle="modal" data-target="#addBagID" type="text">
									<input type="hidden" id="bagNoID"> </td>
									
									<td><input  class="form-control" name="quantity1" id="quantityID" placeholder="Quantity" onkeyup="compQtyCheck(quantityID.value, rateID.value);" value="1" onfocus="this.value='0'" onblur="this.value" type="number"></td>
									
									<td><input  class="form-control" name="rate1" id="rateID" placeholder="Rate" onkeyup="compQtyCheck(quantityID.value, rateID.value);" type="number"></td>
									
									<td><input  class="form-control" name="amount1" id="amountID" placeholder="Amount" readonly="readonly" type="number"></td>
									
									<td> <img src="images/addBill.png" style="height:24px;margin-top:5px;cursor: pointer;" onclick="addNewRow(dummyTotalAmt.value, componentID.value, bagNoID.value, quantityID.value, rateID.value, amountID.value);" 
											onmouseover="this.src='images/addBill1.png'" onmouseout="this.src='images/addBill.png'" alt="Add Component"
											title="Add Component" /></td>
									
                        	</tr>
                       
                        	
                        	<tr>
                        		
                        		<td colspan="5" align="right" style="font-size: 14px;padding-top:15px;">Total</td>
                        		
                        		<td colspan="2">
                        			<input  class="form-control" name="totalAmount" id="totalAmountID" placeholder="Total Amount" readonly="readonly" type="number">
                        			<input type="hidden" id="dummyTotalAmt">
                        		</td>
                        	
                        	</tr>
                        	
                        	<tr id="otherChargeTRID">
                        		
                        		<td colspan="4" align="right" style="font-size: 14px;padding-top:15px;">Other Charges</td>
                        		
                        		<td><select class="form-control" name="chargeType1" id="chargeTypeID" onchange="getOtherChargesRate(chargeTypeID.value);">
									<option value="000">Select Charge Type</option>
									<%
										Iterator<String> iterator1 = otherChargesList.iterator();
									
										while(iterator1.hasNext()){
											
											String value = iterator1.next();
									%>
									
										<option value="<%=value %>"><%=value %></option>
										
									<%
									}
									%>
								</select>
								<input type="hidden" id="newOtherChargeAmt">
								</td>
                        		
                        		<td>
                        			<input  class="form-control" name="otherCharge1" id="otherChargeID" placeholder="Other Charge" type="number">
                        		</td>
                        		
                        		<td><img src="images/addBill.png" style="height:24px;margin-top:5px;cursor: pointer;" onclick="addNewRowOtherCharge(totalAmountID.value, chargeTypeID.value, otherChargeID.value);" 
											onmouseover="this.src='images/addBill1.png'" onmouseout="this.src='images/addBill.png'" alt="Add Other Charges"
											title="Add Other Charges" /></td>
                        	
                        	</tr>
                        	
                        	<tr>
                        		
                        		<td colspan="4" align="right" style="font-size: 14px;padding-top:15px;">Concession</td>
                        		
                        		<td><select class="form-control" name="concessionType" id="concessionID" onchange="getConcessionRate(concessionID.value);">
									<option value="000">Select Concession</option>
									<%
										Iterator<String> iterator2 = concessionList.iterator();
									
										while(iterator2.hasNext()){
											
											String value = iterator2.next();
									%>
									
										<option value="<%=value %>"><%=value %></option>
										
									<%
									}
									%>
								</select>
								<input type="hidden" id="lastConcessionAmtID">
								</td>
                        		
                        		<td colspan="2">
                        			<input  class="form-control" name="concessionAmount" id="concessionAmountID" onkeyup="concessionAmtChange(concessionAmountID.value, event);" placeholder="Concession Amt." type="number">
                        		</td>
                        	
                        	</tr>
                        	
                        	<tr>
                        		
                        		<td colspan="5" align="right" style="font-size: 14px;padding-top:15px;">Net Receivable Amt.</td>
                        		
                        		<td colspan="2">
                        			<input  class="form-control" name="netReceivableAmt" id="netReceivableAmtID" readonly="readonly" placeholder="Net Receivable Amt." type="number">
                        			<input type="hidden" id="lastNetReceivableAmtID">
                        		</td>
                        	
                        	</tr>
                        	
                        	<tr>
                        		
                        		<td colspan="5" align="right" style="font-size: 14px;padding-top:15px;">Actual Payment</td>
                        		
                        		<td colspan="2">
                        			<input  class="form-control" name="actualPayment" id="actualPaymentID" placeholder="Actual Payment" onkeyup="actualPaymentAmtChange(actualPaymentID.value, netReceivableAmtID.value);" type="number">
                        		</td>
                        	
                        	</tr>
                        	
                        	<tr>
                        		
                        		<td colspan="5" align="right" style="font-size: 14px;padding-top:15px;">Outstanding Amt.</td>
                        		
                        		<td colspan="2">
                        			<input  class="form-control" name="oustandingAmt" id="oustandingAmtID" readonly="readonly" placeholder="Outstanding Amt." type="number">
                        		</td>
                        	
                        	</tr>
                        	
                      </tbody>
                    </table>
                    
                    <div class="row" style="margin-top:20px;">
                    	<div class="col-md-2 col-sm-3 col-md-4" style="padding-top:5px;">
                    		<font style="font-size: 16px;">Payment Type</font>
                    	</div>
                    	
                    	<div class="col-md-2 col-sm-3 col-md-4">
                    		<div class="col-md-4 col-sm-4 col-xs-6">
                    			<input type="radio" name="paymentType" checked="checked" onclick="hideChequeDetailsDiv();" style="height: 25px;box-shadow: none;" id="paymentTypeCashID" value="Cash" class="form-control">
                    		</div>
                    		<div class="col-md-8 col-sm-8 col-xs-6" style="padding-top:5px;">
                    			<font style="font-size: 15px;">Cash</font>
                    		</div>
                    	</div>
                    	
                    	<div class="col-md-2 col-sm-3 col-md-4">
                    		<div class="col-md-4 col-sm-4 col-xs-6">
                    			<input type="radio" name="paymentType" onclick="displayChequeDetailsDiv();" style="height: 25px;box-shadow: none;" id="paymentTypeChequeID" value="Cheque" class="form-control">
                    		</div>
                    		<div class="col-md-4 col-sm-4 col-xs-6" style="padding-top:5px;">
                    			<font style="font-size: 15px;">Cheque</font>
                    		</div>
                    	</div>
                    	
                    	<div class="col-md-3 col-sm-3 col-md-4">
                    		<div class="col-md-4 col-sm-4 col-xs-6">
                    			<input type="radio" name="paymentType" onclick="displayCardDiv();" style="height: 25px;box-shadow: none;" id="paymentTypeCardID" value="Credit/Debit Card" class="form-control">
                    		</div>
                    		<div class="col-md-8 col-sm-4 col-xs-6" style="padding-top:5px;">
                    			<font style="font-size: 15px;">Credit/Debit Card</font>
                    		</div>
                    	</div>
                    	
                    	<div class="col-md-2 col-sm-3 col-md-4">
                    		<div class="col-md-4 col-sm-4 col-xs-6">
                    			<input type="radio" name="paymentType" onclick="displayChequeDetailsDivCreditNote();" style="height: 25px;box-shadow: none;" id="paymentTypeCreditNoteID" value="Credit Note" class="form-control">
                    		</div>
                    		<div class="col-md-8 col-sm-4 col-xs-6" style="padding-top:5px;">
                    			<font style="font-size: 15px;">Credit</font>
                    		</div>
                    	</div>
                    </div>
                    
                    <div id="chequeDetailID" style="margin-top:15px; display: none;">
                    
                    <div class="ln_solid"></div>
                    
	                    <div class="row">
	                    	<div class="col-md-3">
	                    		<font style="font-size: 16px;">Cheque Issued By</font>
	                    	</div>
	                    	<div class="col-md-3">
		                        <input type="text" class="form-control" name="chequeIssuedBy" placeholder="Cheque Issued By">
	                    	</div>
	                    	<div class="col-md-3" align="right">
	                    		<font style="font-size: 16px;">Cheque No.</font>
	                    	</div>
	                    	<div class="col-md-3">
	                    		<input  class="form-control" name="chequeNo"  placeholder="Cheque No." type="text">
	                    	</div>
	                    </div>
	                    
	                    <div class="row" style="margin-top:15px;">
	                    	<div class="col-md-3">
	                    		<font style="font-size: 16px;">Bank Name</font>
	                    	</div>
	                    	<div class="col-md-3">
		                        <input type="text" class="form-control" name="chequeBankName" placeholder="Bank Name">
	                    	</div>
	                    	<div class="col-md-3" align="right">
	                    		<font style="font-size: 16px;">Branch</font>
	                    	</div>
	                    	<div class="col-md-3">
	                    		<input  class="form-control" name="chequeBankBranch"  placeholder="Branch" type="text">
	                    	</div>
	                    </div>
	                    
	                    <div class="row" style="margin-top:15px;">
	                    	<div class="col-md-3">
	                    		<font style="font-size: 16px;">Date</font>
	                    	</div>
	                    	<div class="col-md-3">
		                        <input type="text" class="form-control" name="chequeDate" id="single_cal31" placeholder="Date">
	                    	</div>
	                    	<div class="col-md-3" align="right">
	                    		<font style="font-size: 16px;">Amount</font>
	                    	</div>
	                    	<div class="col-md-3">
	                    		<input  class="form-control" name="chequeAmt"  placeholder="Amount" type="number">
	                    	</div>
	                    </div>
	                    
	                <div class="ln_solid"></div>
                    
                    </div>
                    
                    <div id="cardDetailID" style="margin-top:15px; display: none;">
                    
                    <div class="ln_solid"></div>
                    
	                    <div class="row">
	                    	<div class="col-md-3">
	                    		<font style="font-size: 16px;">Mobile No</font>
	                    	</div>
	                    	<div class="col-md-3">
		                        <input type="number" class="form-control" name="cardMobileNo" placeholder="Mobile No">
	                    	</div>
	                    </div>
	                    
	                <div class="ln_solid"></div>
                    
                    </div>
                    
                    <div class="row" style="margin-top:20px;">
                    	<div class="col-md-2 col-sm-2 col-md-6" style="padding-top:5px;">
                    		<font style="font-size: 16px;">TDS Amt.</font>
                    	</div>
                    	
                    	<div class="col-md-2 col-sm-2 col-md-6">
                    		<input type="number" name="tdsAmt" id="TDSAmtID" class="form-control">
                    	</div>
                    	
                    	<div class="col-md-3 col-sm-3 col-md-6" align="right" style="padding-top:5px;">
                    		<font style="font-size: 16px;">Ref. Receipt Number<span id="refRecNoSpanID">*</span></font>
                    	</div>
                    	
                    	<div class="col-md-2 col-sm-2 col-md-6">
                    		<input  class="form-control" name="refReceiptNo" id="refReceiptNoID" required="required" placeholder="Ref. Receipt Number" type="text">
                    	</div>
                    	
                    	<div class="col-md-2 col-sm-2 col-md-6">
                    		<input type="button" class="btn btn-warning" onclick="retrieveRefReceiptNo();" disabled="disabled" id="fetchRefReceiptBtnID" value="Fetch Ref. Receipt No.">
                    	</div>
                    </div>
                    
                    <div class="row" style="margin-top:15px;">
                    	<div class="col-md-3">
                    		<font style="font-size: 16px;">Outstanding Paid Date</font>
                    	</div>
                    	<div class="col-md-3">
	                        <input type="text" class="form-control" name="outstandingPaidDate" id="single_cal3" placeholder="Outstanding Paid Date" aria-describedby="inputSuccess2Status3">
                    	</div>
                    	<div class="col-md-3" align="right">
                    		<font style="font-size: 16px;">Outstanding Paid Amount</font>
                    	</div>
                    	<div class="col-md-3">
                    		<input  class="form-control" name="outstandingPaidAmt" id="outstandingPaidAmtID" onkeyup="outstandingPaidAmtChange(oustandingAmtID.value, outstandingPaidAmtID.value);" placeholder="Outstanding Paid Amount" type="number">
                    	</div>
                    </div>
                    
                    <div class="row" style="margin-top:15px;">
                    	<div class="col-md-3">
                    		<font style="font-size: 16px;">Receipt Give By</font>
                    	</div>
                    	<div class="col-md-3">
                    		<font style="font-size: 16px;"><b><%=form.getFullname() %></b></font>
                    	</div>
                    	<div class="col-md-3" align="right">
                    		<font style="font-size: 16px;">Receivers Signature</font>
                    	</div>
                    	<div class="col-md-3">
                    		
                    	</div>
                    </div>
                    
                     <div class="ln_solid"></div>
                     
                     <div class="row">
                     	<div class="col-md-3">
                    		
                    	</div>
                    	<div class="col-md-3">
                    		<button type="button" style="width:100%;" onclick="windowOpen();" class="btn btn-primary">Cancel</button>
                    	</div>
                    	<div class="col-md-3">
                    		<button id="send" style="width:100%;" type="submit" onclick="return submitBill();" class="btn btn-success " disabled="disabled">Add New Bill</button>
                    	</div>
                    	<div class="col-md-3">
                    		
                    	</div>
                     </div>
                    
                    </form>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="vendors/nprogress/nprogress.js"></script>
    <!-- validator -->
    <script src="vendors/validator/validator.js"></script>
    
    <!-- Datatables -->
    <script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    
    <!-- bootstrap-daterangepicker -->
    <script src="js/moment/moment.min.js"></script>
    <script src="js/datepicker/daterangepicker.js"></script>
    
    <!-- Barcode generator -->
    <script src="build/js/jquery-barcode.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="build/js/custom.min.js"></script>
    
    <!-- /Datatables -->
    
    <!-- bootstrap-daterangepicker -->

    <script>
    function myFun(){
      $(document).ready(function() {

        $('#single_cal3').daterangepicker({
          singleDatePicker: true,
          calender_style: "picker_3"
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#single_cal31').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_3"
          }, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
          });

        $('#datatable-responsive').DataTable();
        
      });
    }
    </script>

    <!-- /bootstrap-daterangepicker -->
    
  </body>
</html>