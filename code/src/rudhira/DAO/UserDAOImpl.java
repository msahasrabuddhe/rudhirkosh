/**
 * 
 */
package rudhira.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import rudhira.form.UserForm;
import rudhira.util.ActivityStatus;
import rudhira.util.ConfigXMLUtil;
import rudhira.util.DBConnection;
import rudhira.util.EncDescUtil;
import rudhira.util.QueryMaker;

/**
 * @author Kovid Bioanalytics
 * 
 */
public class UserDAOImpl extends DBConnection implements UserDAOInf {

	ConfigXMLUtil xmlUtil = null;

	Connection connection = null;
	PreparedStatement preparedStatement = null;
	ResultSet resultSet = null;

	String message = "error";

	static int counter = 1;

	static boolean check = false;

	static int extraMinutes = 0;

	public String validateUserCredentials(UserForm loginForm) {
		try {

			if (retrieveLockedUser(loginForm.getUsername()).equals(ActivityStatus.LOCKED)) {
				message = "locked";
				return message;
			} else {
				ResultSet resultSet1 = null;

				connection = getConnection();

				String verifyUserCredentialsQuery = QueryMaker.VERIFY_USER_CREDENTIALS;

				preparedStatement = connection.prepareStatement(verifyUserCredentialsQuery);
				preparedStatement.setString(1, ActivityStatus.ACTIVE);

				resultSet1 = preparedStatement.executeQuery();

				while (resultSet1.next()) {

					/*
					 * decrypt the password from database and store it in one
					 * string variable
					 */
					String decryptedPassword = null;
					decryptedPassword = EncDescUtil.DecryptText(resultSet1.getString("password").trim());

					System.out.println("Decrypted password is::::::" + decryptedPassword);

					System.out.println(
							"User credentials are:::::" + loginForm.getUsername() + " " + loginForm.getPassword());

					if ((loginForm.getUsername().equals("") || loginForm.getUsername().equals(null))
							&& (loginForm.getPassword().equals("") || loginForm.getPassword().equals(null))) {
						message = "input";
						return message;
					} else if (((resultSet1.getString("username")).equals(loginForm.getUsername()))
							&& (decryptedPassword.equals(loginForm.getPassword()))) {
						System.out.println("Credetials Matched.....");
						message = "success";
						return message;
					}

					if (((resultSet1.getString("username")).equals(loginForm.getUsername()))
							&& (decryptedPassword != (loginForm.getPassword()))) {
						/*
						 * Retrieving userID from User table
						 */
						int userId = retrieveUserIDByUsername(loginForm.getUsername());

						// Setting user id into session
						loginForm.setUserID(resultSet1.getInt("id"));

						if (userId != 0) {
							System.out.println("User id from User table for login attempt ::: " + userId);

							/*
							 * Verifying whether username that entered by user
							 * and username from loginAttempt table matches
							 */
							int useridForLoginAttempt = retrieveUserIDFromLoginAttempt(userId);

							System.out.println("UserID from LoginAttempt is ::" + useridForLoginAttempt);
							System.out.println("True or False:::" + (userId == useridForLoginAttempt));

							if (userId == useridForLoginAttempt) {
								System.out.println("updating LoginAttempt table");

								/*
								 * Retrieving date from LoginAttempt. If current
								 * date is equal to loginAttempt date then
								 * update the record in loginAttempt table with
								 * incremented attemptCounter value . If date
								 * value from login attempt table is not equal
								 * to current date or time then insert new value
								 * into Login attempt table with same user ID.
								 * And If login attempt date value matches
								 * current date and time value, then after 10
								 * min of time interval update the
								 * counterAttempt value of that userID to 1
								 */
								String loginAttemptDate = retrieveLoginAttemptTimestamp(userId);

								String[] timeStampArray = loginAttemptDate.split(" ");

								String date = timeStampArray[0];

								String time = timeStampArray[1];

								// Splitting time in order to get hour and
								// minutes
								String[] timeArray = time.split(":");

								String hour = timeArray[0];

								String minute = timeArray[1];

								// Fetching current date and time
								Date currentDate = new Date();

								SimpleDateFormat currentDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

								String currentDateTime = currentDateFormat.format(currentDate);

								// Splitting Current date and time value
								String currentTimeArray[] = currentDateTime.split(" ");

								String currDate = currentTimeArray[0];

								String currTimeArray[] = currentTimeArray[1].split(":");

								String currHour = currTimeArray[0];

								String currMin = currTimeArray[1];

								// Comparing current date time with login
								// attempt date time
								int timeCheck = date.compareTo(currDate);

								System.out.println("date compare check is :: " + timeCheck);

								// adding 10 to current minute in order to
								// get interval of 10 minute

								if (timeCheck == 0 && hour.equals(currHour)
										&& Integer.parseInt(currMin) >= Integer.parseInt(minute)) {

									extraMinutes = 0;
									check = false;

									int tempMinute = Integer.parseInt(minute) + 10;

									System.out.println("Temp minute with 10 minute interval is :::: " + tempMinute);

									if (tempMinute > Integer.parseInt(currMin)) {

										if (tempMinute >= 60) {

											System.out.println("temp minute is greater than 60");

											check = true;

											// finding extra minutes by
											// substracting 60 from tempMinute
											extraMinutes = tempMinute - 60;
										}

										/*
										 * updating attempt counter value of
										 * user by incrementing it by 1
										 */

										// Retrieving max login attempt value
										// from
										// ClinicConfiguration table based on
										// user name

										int MAX_LOGIN_ATTEMPT = 5;

										System.out.println("MAX_LOGIN_ATTEMPT ::: " + MAX_LOGIN_ATTEMPT);

										if (MAX_LOGIN_ATTEMPT > counter) {
											// incrementing counter by 1
											counter++;
											System.out.println("counter value in if::: " + counter);

											/*
											 * updating attemptCounter from
											 * LoginAttempt table
											 */
											updateLoginAttempt(userId, counter, loginAttemptDate);

											message = "input";

										} else {
											System.out.println("Login attempt exceeded the limit.");
											System.out.println("counter value in else::: " + counter);

											counter = 1;

											/*
											 * Updating activity status of
											 * LoginAttempt to Disable for that
											 * particular userID
											 */
											updateLoginAttemptStatus(userId);

											/*
											 * Updating activityStatus of User
											 * From AppUser table to Locked
											 */
											updateUserStatus(userId);
											System.out.println(
													"User is locked by updating his/her acticityStatus to Locked");

											message = "login";

										}

									} else {

										/*
										 * Setting static variables extraMinutes
										 * and check to its default value
										 */
										extraMinutes = 0;
										check = false;

										System.out.println("inserting new value with counter as 1");

										/*
										 * Updating activity status of
										 * LoginAttempt to Disable for that
										 * particular userID
										 */
										updateLoginAttemptStatus(userId);

										counter = 1;

										/*
										 * Inserting new record into
										 * LoginAttempt with counter value as 1
										 */
										insertLoginAttempt(userId, counter, loginForm.getUsername().trim());

										message = "input";

									}

								} else if (check) {

									System.out.println("Inside check trueee");
									/*
									 * It means if current time is 12:55, so
									 * after adding 10 minute to it, the time
									 * becomes 13.05. If this is the case, then
									 * update login attempt till the converted
									 * time exceeds
									 */
									if ((Integer.parseInt(currMin) > extraMinutes)) {

										int MAX_LOGIN_ATTEMPT = 5;

										System.out.println("MAX_LOGIN_ATTEMPT ::: " + MAX_LOGIN_ATTEMPT);

										if (MAX_LOGIN_ATTEMPT > counter) {
											// incrementing counter by 1
											counter++;
											System.out.println("counter value in if::: " + counter);

											/*
											 * updating attemptCounter from
											 * LoginAttempt table
											 */
											updateLoginAttempt(userId, counter, loginAttemptDate);

											message = "input";

										} else {
											System.out.println("Login attempt exceeded the limit.");
											System.out.println("counter value in else::: " + counter);

											counter = 1;

											/*
											 * Updating activity status of
											 * LoginAttempt to Disable for that
											 * particular userID
											 */
											updateLoginAttemptStatus(userId);

											/*
											 * Updating activityStatus of User
											 * From AppUser table to Locked
											 */
											updateUserStatus(userId);
											System.out.println(
													"User is locked by updating his/her acticityStatus to Locked");

											message = "login";

										}

									} else if ((Integer.parseInt(currMin) <= extraMinutes)) {

										if ((Integer.parseInt(currMin) == extraMinutes)) {
											check = false;
										}

										int MAX_LOGIN_ATTEMPT = 5;

										System.out.println("MAX_LOGIN_ATTEMPT ::: " + MAX_LOGIN_ATTEMPT);

										if (MAX_LOGIN_ATTEMPT > counter) {
											// incrementing counter by 1
											counter++;
											System.out.println("counter value in if::: " + counter);

											/*
											 * updating attemptCounter from
											 * LoginAttempt table
											 */
											updateLoginAttempt(userId, counter, loginAttemptDate);

											message = "input";

										} else {
											System.out.println("Login attempt exceeded the limit.");
											System.out.println("counter value in else::: " + counter);

											counter = 1;

											/*
											 * Updating activity status of
											 * LoginAttempt to Disable for that
											 * particular userID
											 */
											updateLoginAttemptStatus(userId);

											/*
											 * Updating activityStatus of User
											 * From AppUser table to Locked
											 */
											updateUserStatus(userId);
											System.out.println(
													"User is locked by updating his/her acticityStatus to Locked");

											message = "login";

										}

									} else {

										/*
										 * Setting static variables extraMinutes
										 * and check to its default value
										 */
										extraMinutes = 0;
										check = false;

										/*
										 * Updating activity status of
										 * LoginAttempt to Disable for that
										 * particular userID
										 */
										updateLoginAttemptStatus(userId);

										counter = 1;

										// inserting new value with same userID
										insertLoginAttempt(userId, counter, loginForm.getUsername().trim());

										message = "input";

									}

								} else {

									/*
									 * Setting static variables extraMinutes and
									 * check to its default value
									 */
									extraMinutes = 0;
									check = false;

									/*
									 * Updating activity status of LoginAttempt
									 * to Disable for that particular userID
									 */
									updateLoginAttemptStatus(userId);

									counter = 1;

									// inserting new value with same userID
									insertLoginAttempt(userId, counter, loginForm.getUsername().trim());

									message = "input";
								}

							} else {

								/*
								 * Setting static variables extraMinutes and
								 * check to its default value
								 */
								extraMinutes = 0;
								check = false;

								System.out.println("inserting into LoginAttempt table");

								counter = 1;

								/*
								 * Updating activity status of LoginAttempt to
								 * Disable for that particular userID
								 */
								updateLoginAttemptStatus(userId);

								/*
								 * Inserting values into LoginAttempt table
								 */
								insertLoginAttempt(userId, counter, loginForm.getUsername().trim());

								message = "input";
							}

						} else {
							message = "input";
						}

					} else {
						message = "input";
					}
				}

				resultSet.close();
				preparedStatement.close();
				connection.close();

			}
		} catch (Exception exception) {
			exception.printStackTrace();
			System.out.println(
					"Exception occured while checking user credentials while login due to:::" + exception.getMessage());
			message = "exception";
		}
		return message;
	}

	/**
	 * 
	 * @param username
	 * @return
	 */
	public String retrieveLockedUser(String username) {
		String userStatus = "dummy";
		try {
			connection = getConnection();

			String retrieveLockedUserQuery = QueryMaker.RETRIVE_LOCKED_USER_STATUS;

			preparedStatement = connection.prepareStatement(retrieveLockedUserQuery);
			preparedStatement.setString(1, username);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				userStatus = resultSet.getString("activityStatus");
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return userStatus;
	}

	/**
	 * 
	 * @param userID
	 * @return username
	 */
	public int retrieveUserIDFromLoginAttempt(int userID) {
		int loginAttempUserID = 0;
		try {
			connection = getConnection();

			String retrieveUserIDFromLoginAttemptQuery = QueryMaker.RETRIEVE_USER_ID_FROM_LOGIN_ATTEMPT;

			preparedStatement = connection.prepareStatement(retrieveUserIDFromLoginAttemptQuery);

			preparedStatement.setInt(1, userID);
			preparedStatement.setString(2, ActivityStatus.ENABLE);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				loginAttempUserID = resultSet.getInt("userID");
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return loginAttempUserID;
	}

	/**
	 * 
	 * @param userID
	 * @param counter
	 * @return result
	 */
	public String insertLoginAttempt(int userID, int counter, String username) {
		String result = "error";

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

		Date today = new Date();

		try {
			connection = getConnection();

			String insertLoginAttemptQuery = QueryMaker.INSERT_INTO_LOGIN_ATTEMPT;

			preparedStatement = connection.prepareStatement(insertLoginAttemptQuery);
			preparedStatement.setInt(1, counter);
			preparedStatement.setInt(2, userID);
			preparedStatement.setString(3, sdf.format(today));
			preparedStatement.setString(4, ActivityStatus.ENABLE);

			preparedStatement.execute();

			result = "success";

		} catch (Exception exception) {
			exception.printStackTrace();

			result = "error";
		}
		return result;
	}

	/**
	 * 
	 * @param userID
	 * @param counter
	 * @return result
	 */
	public String updateLoginAttempt(int userID, int counter, String loginAttemptDate) {
		String result = "error";
		try {
			connection = getConnection();

			String insertLoginAttemptQuery = QueryMaker.UPDATE_LOGIN_ATTEMPT;

			System.out
					.println("UPDATE LoginAttempt SET attemptCounter = " + counter + " WHERE userID = " + userID + "");

			preparedStatement = connection.prepareStatement(insertLoginAttemptQuery);
			preparedStatement.setInt(1, counter);
			preparedStatement.setInt(2, userID);
			preparedStatement.setString(3, loginAttemptDate);

			preparedStatement.executeUpdate();

			result = "success";

		} catch (Exception exception) {
			exception.printStackTrace();

			result = "error";
		}
		return result;
	}

	/**
	 * 
	 * @param userId
	 * @return result
	 */
	public String updateUserStatus(int userId) {
		String result = "error";
		try {
			connection = getConnection();

			String updateUserStatusQuery = QueryMaker.UPDATE_USER_STATUS;

			preparedStatement = connection.prepareStatement(updateUserStatusQuery);
			preparedStatement.setString(1, ActivityStatus.LOCKED);
			preparedStatement.setInt(2, userId);

			preparedStatement.executeUpdate();

			result = "success";

		} catch (Exception exception) {
			exception.printStackTrace();

			result = "error";
		}
		return result;
	}

	/**
	 * 
	 * @param username
	 * @return
	 */
	public int retrieveUserIDByUsername(String username) {
		int userID = 0;

		try {
			connection = getConnection();

			String retrieveUserIDByUsernameQuery = QueryMaker.RETRIEVE_USER_ID_BY_USERNAME;

			preparedStatement = connection.prepareStatement(retrieveUserIDByUsernameQuery);
			preparedStatement.setInt(1, userID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				userID = resultSet.getInt("id");
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return userID;
	}

	public UserForm getUserDetails(String userName) {
		UserForm loginForm = new UserForm();

		try {

			connection = getConnection();

			String getUserDetailsQuery = QueryMaker.RETRIEVE_USER_DETAILS;

			preparedStatement = connection.prepareStatement(getUserDetailsQuery);

			preparedStatement.setString(1, userName);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				if (resultSet.getString("middleName") == null || resultSet.getString("middleName") == "") {

					String fullName = resultSet.getString("firstName") + " " + resultSet.getString("lastName");

					loginForm.setFullname(fullName);

				} else {

					String fullName = resultSet.getString("firstName") + " " + resultSet.getString("middleName") + " "
							+ resultSet.getString("lastName");

					loginForm.setFullname(fullName);

				}

				loginForm.setFirstName(resultSet.getString("firstName"));
				loginForm.setMiddleName(resultSet.getString("middleName"));
				loginForm.setLastName(resultSet.getString("lastName"));
				loginForm.setUserID(resultSet.getInt("id"));
				loginForm.setEmail(resultSet.getString("email"));
				loginForm.setUsername(resultSet.getString("username"));
				loginForm.setUserRole(resultSet.getString("role"));
				loginForm.setActivityStatus(resultSet.getString("activityStatus"));
				loginForm.setBloodBankID(resultSet.getInt("bloodBankID"));
				loginForm.setProfilePicDBName(resultSet.getString("profilePic"));

			}

			System.out.println("Successfully retrieved user details");

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return loginForm;
	}

	/**
	 * 
	 * @param userID
	 * @return
	 */
	public String retrieveLoginAttemptTimestamp(int userID) {
		String loginAttemptDate = null;

		try {

			connection = getConnection();

			String retrieveLoginAttemptTimestampQuery = QueryMaker.RETRIEVE_LOGIN_ATTEMPT_DATE;

			preparedStatement = connection.prepareStatement(retrieveLoginAttemptTimestampQuery);

			preparedStatement.setInt(1, userID);
			preparedStatement.setString(2, ActivityStatus.ENABLE);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				loginAttemptDate = resultSet.getString("dateAndTime");
			}

			System.out
					.println("Date and time from Login Attempt for userID : " + userID + " is :: " + loginAttemptDate);

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return loginAttemptDate;
	}

	/**
	 * 
	 * @param userId
	 * @return
	 */
	public String updateLoginAttemptStatus(int userId) {
		String result = "error";
		try {
			connection = getConnection();

			String updateLoginAttemptStatusQuery = QueryMaker.UPDATE_LOGIN_ATTEMPT_STATUS;

			preparedStatement = connection.prepareStatement(updateLoginAttemptStatusQuery);

			preparedStatement.setString(1, ActivityStatus.DISABLE);
			preparedStatement.setInt(2, userId);

			preparedStatement.executeUpdate();

			result = "success";

		} catch (Exception exception) {
			exception.printStackTrace();

			result = "error";
		}
		return result;
	}

	public HashMap<Integer, String> retrieveBloodBankList() {

		HashMap<Integer, String> bloodBankMap = new HashMap<Integer, String>();

		try {

			connection = getConnection();

			String retrieveBloodBankListQuery = QueryMaker.RETRIEVE_BLOOD_BANK_LIST;

			preparedStatement = connection.prepareStatement(retrieveBloodBankListQuery);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				bloodBankMap.put(resultSet.getInt("id"), resultSet.getString("name"));

			}

			System.out.println("Blood bank list retrieve successfully...");

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return bloodBankMap;
	}

	public int verifyUsernameExist(String username) {

		int check = 0;

		try {
			connection = getConnection();

			String verifyUsernameExistQuery = QueryMaker.VERIFY_USERNAME_EXIST;

			preparedStatement = connection.prepareStatement(verifyUsernameExistQuery);

			preparedStatement.setString(1, username);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				check = 1;
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return check;
	}

	public String inserUserDetails(UserForm userForm) {

		// Encrypting password and PIN
		String encryptedPass = EncDescUtil.EncryptText(userForm.getPassword());

		String encryptedPIN = EncDescUtil.EncryptText(userForm.getPIN());

		System.out.println("Encrypted password and PIN is :: " + encryptedPass + " & " + encryptedPIN);

		try {

			connection = getConnection();

			String inserUserDetailsQuery = QueryMaker.INSERT_USER_DETAILS;

			preparedStatement = connection.prepareStatement(inserUserDetailsQuery);

			preparedStatement.setString(1, userForm.getFirstName());
			preparedStatement.setString(2, userForm.getMiddleName());
			preparedStatement.setString(3, userForm.getLastName());
			preparedStatement.setString(4, userForm.getPhone());
			preparedStatement.setString(5, userForm.getAddress());
			preparedStatement.setString(6, userForm.getCity());
			preparedStatement.setString(7, userForm.getState());
			preparedStatement.setString(8, userForm.getCountry());
			preparedStatement.setString(9, userForm.getEmail());
			preparedStatement.setString(10, userForm.getUsername());
			preparedStatement.setString(11, encryptedPass);
			preparedStatement.setString(12, userForm.getUserRole());
			preparedStatement.setString(13, ActivityStatus.ACTIVE);
			preparedStatement.setInt(14, userForm.getBloodBankID());
			preparedStatement.setString(15, userForm.getProfilePicDBName());
			preparedStatement.setString(16, encryptedPIN);

			preparedStatement.execute();

			message = "success";

			System.out.println("Successfully added user into AppUSer table");

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}
		return message;
	}

	public List<UserForm> retrieveUserList() {

		List<UserForm> userList = new ArrayList<UserForm>();

		UserForm userForm = null;

		try {

			connection = getConnection();

			String retrieveUserListQuery = QueryMaker.RETRIEVE_EDIT_USER_DETAILS;

			preparedStatement = connection.prepareStatement(retrieveUserListQuery);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				userForm = new UserForm();

				String fullName = null;

				if (resultSet.getString("middleName") == null || resultSet.getString("middleName") == "") {

					fullName = resultSet.getString("firstName") + " " + resultSet.getString("lastName");

				} else {

					fullName = resultSet.getString("firstName") + " " + resultSet.getString("middleName") + " "
							+ resultSet.getString("lastName");

				}

				userForm.setUserID(resultSet.getInt("id"));
				userForm.setFullname(fullName);
				userForm.setUsername(resultSet.getString("username"));
				userForm.setActivityStatus(resultSet.getString("activityStatus"));
				userForm.setUserRole(resultSet.getString("role"));

				userList.add(userForm);
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return userList;
	}

	public List<UserForm> retrieveEditUserListByUserID(int userID) {

		List<UserForm> userList = new ArrayList<UserForm>();

		UserForm userForm = new UserForm();

		try {

			connection = getConnection();

			String retrieveEditUserListByUserIDQuery = QueryMaker.RETRIEVE_EDIT_USER_DETAILS_BY_USER_ID;

			preparedStatement = connection.prepareStatement(retrieveEditUserListByUserIDQuery);

			preparedStatement.setInt(1, userID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				userForm.setFirstName(resultSet.getString("firstName"));
				userForm.setMiddleName(resultSet.getString("middleName"));
				userForm.setLastName(resultSet.getString("lastName"));
				userForm.setPhone(resultSet.getString("phone"));
				userForm.setAddress(resultSet.getString("address"));
				userForm.setCity(resultSet.getString("city"));
				userForm.setState(resultSet.getString("state"));
				userForm.setCountry(resultSet.getString("country"));
				userForm.setBloodBankID(resultSet.getInt("bloodBankID"));
				userForm.setUserID(resultSet.getInt("id"));
				userForm.setEmail(resultSet.getString("email"));
				userForm.setActivityStatus(resultSet.getString("activityStatus"));
				userForm.setUserRole(resultSet.getString("role"));
				userForm.setUsername(resultSet.getString("username"));

				// Decrypting password
				String decryptedPass = EncDescUtil.DecryptText(resultSet.getString("password"));

				System.out.println("Decrypted password is ::: " + decryptedPass);

				// Decrypting PIN
				String decryptedPIN = EncDescUtil.DecryptText(resultSet.getString("pin"));

				System.out.println("Decrypted PIN is ::: " + decryptedPIN);

				userForm.setPassword(decryptedPass);
				userForm.setPIN(decryptedPIN);
				userForm.setUserID(resultSet.getInt("id"));

				if (resultSet.getString("profilePic") == null || resultSet.getString("profilePic") == "") {

					userForm.setProfilePicDBName(resultSet.getString("profilePic"));

				} else if (resultSet.getString("profilePic").isEmpty()) {

					userForm.setProfilePicDBName(resultSet.getString("profilePic"));

				} else {

					String[] array = resultSet.getString("profilePic").split("/");

					userForm.setProfilePicDBName(array[2]);

				}

			}

			userList.add(userForm);

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return userList;
	}

	public String updateUserDetails(UserForm userForm) {

		// Encrypting password
		String encryptedPass = EncDescUtil.EncryptText(userForm.getPassword());

		String encryptedPIN = EncDescUtil.EncryptText(userForm.getPIN());

		System.out.println("Encrypted password and PIN is :: " + encryptedPass + " & " + encryptedPIN);

		try {

			connection = getConnection();

			String updateUserDetailsQuery = QueryMaker.UPDATE_USER_DETAILS;

			preparedStatement = connection.prepareStatement(updateUserDetailsQuery);

			preparedStatement.setString(1, userForm.getFirstName());
			preparedStatement.setString(2, userForm.getMiddleName());
			preparedStatement.setString(3, userForm.getLastName());
			preparedStatement.setString(4, userForm.getPhone());
			preparedStatement.setString(5, userForm.getAddress());
			preparedStatement.setString(6, userForm.getCity());
			preparedStatement.setString(7, userForm.getState());
			preparedStatement.setString(8, userForm.getCountry());
			preparedStatement.setString(9, userForm.getEmail());
			preparedStatement.setString(10, userForm.getUsername());
			preparedStatement.setString(11, encryptedPass);
			preparedStatement.setString(12, userForm.getUserRole());
			preparedStatement.setInt(13, userForm.getBloodBankID());
			preparedStatement.setString(14, userForm.getActivityStatus());
			preparedStatement.setString(15, userForm.getProfilePicDBName());
			preparedStatement.setString(16, encryptedPIN);
			preparedStatement.setInt(17, userForm.getUserID());

			preparedStatement.execute();

			message = "success";

			System.out.println("Successfully udpated user into AppUSer table");

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}
		return message;
	}

	public List<UserForm> searchUser(String username) {

		List<UserForm> list = new ArrayList<UserForm>();

		UserForm userForm = null;

		try {

			connection = getConnection();

			String searchUserQuery = QueryMaker.SEARCH_USER_BY_USER_NAME;

			preparedStatement = connection.prepareStatement(searchUserQuery);

			if (username.contains(" ")) {
				username = username.replace(" ", "%");
			}

			preparedStatement.setString(1, "%" + username + "%");

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				userForm = new UserForm();

				String fullName = null;

				if (resultSet.getString("middleName") == null || resultSet.getString("middleName") == "") {

					fullName = resultSet.getString("firstName") + " " + resultSet.getString("lastName");

				} else {

					fullName = resultSet.getString("firstName") + " " + resultSet.getString("middleName") + " "
							+ resultSet.getString("lastName");

				}

				userForm.setUserID(resultSet.getInt("id"));
				userForm.setFullname(fullName);
				userForm.setUsername(resultSet.getString("username"));
				userForm.setActivityStatus(resultSet.getString("activityStatus"));
				userForm.setUserRole(resultSet.getString("role"));
				userForm.setSearchUserName(username);

				list.add(userForm);

			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return list;
	}

	public String disableUser(int userID) {

		try {

			connection = getConnection();

			String disableUserQuery = QueryMaker.DISABLE_USER_BY_USER_ID;

			preparedStatement = connection.prepareStatement(disableUserQuery);

			preparedStatement.setString(1, ActivityStatus.DISABLE);
			preparedStatement.setInt(2, userID);

			preparedStatement.executeUpdate();

			System.out.println("Successfully disabled user by updating activity status to Disabled.");

			message = "success";

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}
		return message;
	}

	public String insertPasswordHistory(int userID, String password) {

		/*
		 * Encrypting password
		 */
		String encryptedPass = EncDescUtil.EncryptText(password);
		try {

			connection = getConnection();

			String insertPasswordHistoryQuery = QueryMaker.INSERT_PASSWORD_HISTORY;

			preparedStatement = connection.prepareStatement(insertPasswordHistoryQuery);

			preparedStatement.setString(1, encryptedPass);
			preparedStatement.setInt(2, userID);

			preparedStatement.execute();

			System.out.println("Successfully insertd password into PasswordHistory table");

			message = "success";

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}
		return message;
	}

	public boolean verifyPassword(int userID, String password) {

		boolean result = false;
		// Encrypting password
		String encryptedPass = EncDescUtil.EncryptText(password);

		try {

			connection = getConnection();

			String verifyPasswordQuery = QueryMaker.VERIFY_PASSWORD;

			preparedStatement = connection.prepareStatement(verifyPasswordQuery);

			preparedStatement.setInt(1, userID);
			preparedStatement.setString(2, encryptedPass);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				result = true;
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			result = false;
		}
		return result;
	}

	public boolean verifyPasswordHistory(int userID, String password) {

		// Encrypting password
		String encryptedPass = EncDescUtil.EncryptText(password);

		boolean result = false;

		try {

			connection = getConnection();

			String verifyPasswordHistoryQuery = QueryMaker.VERIFY_PASSWORD_HISTORY;

			preparedStatement = connection.prepareStatement(verifyPasswordHistoryQuery);

			preparedStatement.setInt(1, userID);
			preparedStatement.setString(2, encryptedPass);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				result = true;
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			result = false;
		}
		return result;
	}

	public String retrieveBloodBankName(int bloodBankID) {

		String bloodBankName = null;
		try {

			connection = getConnection();

			String retrieveBloodBankNameQuery = QueryMaker.RETRIEVE_BLOOD_BANK_NAME;

			preparedStatement = connection.prepareStatement(retrieveBloodBankNameQuery);

			preparedStatement.setInt(1, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				bloodBankName = resultSet.getString("name");

			}

			System.out.println("Blood bank name is ::: " + bloodBankName);

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return bloodBankName;
	}

	public String insertBloodBank(UserForm userForm) {

		try {

			connection = getConnection();

			String insertBloodBankQuery = QueryMaker.INSERT_BLOOD_BANK_DETALS;

			preparedStatement = connection.prepareStatement(insertBloodBankQuery);

			preparedStatement.setString(1, userForm.getBloodBankName());
			preparedStatement.setString(2, userForm.getBloodBankRegNo());
			preparedStatement.setString(3, userForm.getBloodBankLogoFileName());
			preparedStatement.setString(4, userForm.getBloodBankAddress());
			preparedStatement.setString(5, userForm.getBloodBankPhone1());
			preparedStatement.setString(6, userForm.getBloodBankPhone2());
			preparedStatement.setString(7, userForm.getBloodBankEmail());
			preparedStatement.setString(8, userForm.getBloodBankWebsite());
			preparedStatement.setString(9, userForm.getBloodBankTagline());

			preparedStatement.execute();

			System.out.println("Successfully inserted blood bank details into table");

			message = "success";

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}
		return message;
	}

	public String updateBloodBank(UserForm userForm) {

		try {

			connection = getConnection();

			String updateBloodBankQuery = QueryMaker.UPDATE_BLOOD_BANK_DETALS;

			preparedStatement = connection.prepareStatement(updateBloodBankQuery);

			preparedStatement.setString(1, userForm.getBloodBankName());
			preparedStatement.setString(2, userForm.getBloodBankRegNo());
			preparedStatement.setString(3, userForm.getBloodBankLogoFileName());
			preparedStatement.setString(4, userForm.getBloodBankAddress());
			preparedStatement.setString(5, userForm.getBloodBankPhone1());
			preparedStatement.setString(6, userForm.getBloodBankPhone2());
			preparedStatement.setString(7, userForm.getBloodBankEmail());
			preparedStatement.setString(8, userForm.getBloodBankWebsite());
			preparedStatement.setString(9, userForm.getBloodBankTagline());
			preparedStatement.setInt(10, userForm.getBloodBankID());

			preparedStatement.executeUpdate();

			System.out.println("Successfully updated blood bank details into table");

			message = "success";

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}
		return message;
	}

	public int verifyBloodBankNameExists(String bloodBankName) {

		int check = 0;

		try {
			connection = getConnection();

			String verifyBloodBankNameExistsQuery = QueryMaker.VERIFY_BLOOD_BANK_NAME_EXIST;

			preparedStatement = connection.prepareStatement(verifyBloodBankNameExistsQuery);

			preparedStatement.setString(1, bloodBankName);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				check = 1;
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return check;
	}

	public List<UserForm> searchBloodBank(String bloodBankName) {

		List<UserForm> list = new ArrayList<UserForm>();

		UserForm userForm = null;

		try {

			connection = getConnection();

			String searchBloodBankQuery = QueryMaker.SEARCH_BLOOD_BANK_BY_NAME;

			preparedStatement = connection.prepareStatement(searchBloodBankQuery);

			if (bloodBankName.contains(" ")) {
				bloodBankName = bloodBankName.replace(" ", "%");
			}

			preparedStatement.setString(1, "%" + bloodBankName + "%");

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				userForm = new UserForm();

				userForm.setBloodBankID(resultSet.getInt("id"));
				userForm.setBloodBankName(resultSet.getString("name"));
				userForm.setBloodBankRegNo(resultSet.getString("registrationNo"));
				userForm.setBloodBankEmail(resultSet.getString("email"));
				userForm.setSearchBloodBankName(bloodBankName);

				list.add(userForm);

			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return list;
	}

	public List<UserForm> retrieveEditBloodBankList() {

		List<UserForm> bloodBankList = new ArrayList<UserForm>();

		UserForm userForm = null;

		try {

			connection = getConnection();

			String retrieveEditBloodBankListQuery = QueryMaker.RETRIEVE_EDIT_BLOOD_BANK_LIST;

			preparedStatement = connection.prepareStatement(retrieveEditBloodBankListQuery);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				userForm = new UserForm();

				userForm.setBloodBankID(resultSet.getInt("id"));
				userForm.setBloodBankName(resultSet.getString("name"));
				userForm.setBloodBankRegNo(resultSet.getString("registrationNo"));
				userForm.setBloodBankEmail(resultSet.getString("email"));

				bloodBankList.add(userForm);
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return bloodBankList;
	}

	public List<UserForm> retrieveEditBloodBankListByID(int bloodBankID) {

		List<UserForm> bloodBankList = new ArrayList<UserForm>();

		UserForm userForm = new UserForm();

		try {

			connection = getConnection();

			String retrieveEditBloodBankListByIDQuery = QueryMaker.RETRIEVE_EDIT_BLOOD_BANK_LIST_BY_ID;

			preparedStatement = connection.prepareStatement(retrieveEditBloodBankListByIDQuery);

			preparedStatement.setInt(1, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				userForm.setBloodBankID(resultSet.getInt("id"));
				userForm.setBloodBankName(resultSet.getString("name"));
				userForm.setBloodBankRegNo(resultSet.getString("registrationNo"));
				userForm.setBloodBankEmail(resultSet.getString("email"));
				userForm.setBloodBankAddress(resultSet.getString("address"));
				userForm.setBloodBankPhone1(resultSet.getString("phone1"));
				userForm.setBloodBankPhone2(resultSet.getString("phone2"));
				userForm.setBloodBankWebsite(resultSet.getString("website"));
				userForm.setBloodBankTagline(resultSet.getString("tagline"));

				bloodBankList.add(userForm);
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return bloodBankList;
	}

	public int verifyBloodBankNameExists(String bloodBankName, int bloodBankID) {

		int check = 0;

		try {
			connection = getConnection();

			String verifyBloodBankNameExistsQuery = QueryMaker.VERIFY_BLOOD_BANK_NAME_EXIST1;

			preparedStatement = connection.prepareStatement(verifyBloodBankNameExistsQuery);

			preparedStatement.setString(1, bloodBankName);
			preparedStatement.setInt(2, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				check = 1;
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return check;
	}

	public String retrieveBloodBankLogo(int bloodBankID) {

		String bloodBankLogo = null;
		try {

			connection = getConnection();

			String retrieveBloodBankLogoQuery = QueryMaker.RETRIEVE_BLOOD_BANK_LOGO;

			preparedStatement = connection.prepareStatement(retrieveBloodBankLogoQuery);

			preparedStatement.setInt(1, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				bloodBankLogo = resultSet.getString("logo");

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return bloodBankLogo;
	}

	public JSONObject verifyUserPIN(String PIN, int userID) {

		JSONObject values = null;

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		// Encrypting PIN in order to check into DB
		String encryptedPIN = EncDescUtil.EncryptText(PIN);

		boolean result = false;

		try {
			connection = getConnection();

			String verifyUserPINQuery = QueryMaker.VERIFY_USER_PIN;

			preparedStatement = connection.prepareStatement(verifyUserPINQuery);

			preparedStatement.setString(1, encryptedPIN);
			preparedStatement.setInt(2, userID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				result = true;

			}

			/*
			 * Checking whether user PIN is correct or not and setting variable
			 * according, so as to unlock screen
			 */
			if (result) {

				object.put("PINCheck", "check");

				array.add(object);

				values.put("Release", array);

				preparedStatement.close();

				return values;

			} else {

				object.put("PINCheck", "uncheck");

				array.add(object);

				values.put("Release", array);

				preparedStatement.close();

				return values;

			}

		} catch (Exception exception) {
			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while verifying credentials.");

			array.add(object);

			values.put("Release", array);

			return values;
		}
	}

	public JSONObject verifyUserOldPassword(String oldPassword, int userID) {

		JSONObject values = null;

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		// Encrypting Password in order to check into DB
		String encryptedPass = EncDescUtil.EncryptText(oldPassword);

		boolean result = false;

		try {
			connection = getConnection();

			String verifyUserOldPasswordQuery = QueryMaker.VERIFY_USER_OLD_PASSWORD;

			preparedStatement = connection.prepareStatement(verifyUserOldPasswordQuery);

			preparedStatement.setString(1, encryptedPass);
			preparedStatement.setInt(2, userID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				result = true;

			}

			/*
			 * Checking whether user OLD Password is correct or not and setting
			 * variable according, so as to proceed further for change password
			 */
			if (result) {

				object.put("PassCheck", "check");

				array.add(object);

				values.put("Release", array);

				preparedStatement.close();

				return values;

			} else {

				object.put("PassCheck", "uncheck");

				array.add(object);

				values.put("Release", array);

				preparedStatement.close();

				return values;

			}

		} catch (Exception exception) {
			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while verifying old password.");

			array.add(object);

			values.put("Release", array);

			return values;
		}
	}

	public JSONObject updateUserSecurityCredentials(String newPass, String newPIN, int userID) {

		JSONObject values = null;

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		try {
			connection = getConnection();

			/*
			 * If newPass value is null, update PIN into AppUser table
			 */
			if (newPass == null || newPass == "") {

				// Encrypting PIN in order to update into AppUser
				String encryptedPIN = EncDescUtil.EncryptText(newPIN);

				String updateUserSecurityCredentialsQuery = QueryMaker.UPDATE_PIN;

				preparedStatement = connection.prepareStatement(updateUserSecurityCredentialsQuery);

				preparedStatement.setString(1, encryptedPIN);
				preparedStatement.setInt(2, userID);

				preparedStatement.executeUpdate();

				object.put("securityCheck", "check");

				array.add(object);

				values.put("Release", array);

				preparedStatement.close();

				return values;

				/*
				 * If newPIN value is null, update password into AppUser table
				 */
			} else if (newPIN == null || newPIN == "") {

				// Encrypting Password in order to update into AppUser
				String encryptedPass = EncDescUtil.EncryptText(newPass);

				String updateUserSecurityCredentialsQuery = QueryMaker.UPDATE_PASSWORD;

				preparedStatement = connection.prepareStatement(updateUserSecurityCredentialsQuery);

				preparedStatement.setString(1, encryptedPass);
				preparedStatement.setInt(2, userID);

				preparedStatement.executeUpdate();

				object.put("securityCheck", "check");

				array.add(object);

				values.put("Release", array);

				preparedStatement.close();

				return values;

				/*
				 * If newPass and newPIN both are not null, update both into
				 * AppUser table
				 */
			} else {

				// Encrypting Password and PIN in order to update into AppUser
				String encryptedPass = EncDescUtil.EncryptText(newPass);
				String encryptedPIN = EncDescUtil.EncryptText(newPIN);

				String updateUserSecurityCredentialsQuery = QueryMaker.UPDATE_PASSWORD_AND_PIN;

				preparedStatement = connection.prepareStatement(updateUserSecurityCredentialsQuery);

				preparedStatement.setString(1, encryptedPIN);
				preparedStatement.setString(2, encryptedPass);
				preparedStatement.setInt(3, userID);

				preparedStatement.executeUpdate();

				object.put("securityCheck", "check");

				array.add(object);

				values.put("Release", array);

				preparedStatement.close();

				return values;

			}

		} catch (Exception exception) {
			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while updating security credentials.");

			array.add(object);

			values.put("Release", array);

			return values;
		}
	}

	public String insertAuditDetails(int userID, String IPAddress, String action) {

		try {

			connection = getConnection();

			String insertAuditDetailsQuery = QueryMaker.INSERT_AUDIT_DETAILS;

			preparedStatement = connection.prepareStatement(insertAuditDetailsQuery);

			preparedStatement.setInt(1, userID);
			preparedStatement.setString(2, IPAddress);
			preparedStatement.setString(3, action);

			preparedStatement.execute();

			System.out.println("Audit details inserted successfully.");

			message = "success";

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			System.out.println("Exception occurred while inserting audit details.");

			message = "error";
		}
		return message;
	}

	public List<UserForm> searchConcession(String concessionType, int bloodBankID) {

		List<UserForm> list = new ArrayList<UserForm>();

		UserForm userForm = null;

		try {

			connection = getConnection();

			String searchConcessionQuery = QueryMaker.SEARCH_CONCESSION_BY_TYPE;

			preparedStatement = connection.prepareStatement(searchConcessionQuery);

			if (concessionType.contains(" ")) {
				concessionType = concessionType.replace(" ", "%");
			} else if (concessionType == null || concessionType == "") {
				concessionType = "";
			}

			preparedStatement.setString(1, "%" + concessionType + "%");
			preparedStatement.setInt(2, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				userForm = new UserForm();

				userForm.setConcessionID(resultSet.getInt("id"));
				userForm.setConcessionType(resultSet.getString("concessionType"));
				userForm.setConcessionAmt(resultSet.getDouble("concessionAmt"));
				userForm.setSearchConcessionName(concessionType);

				list.add(userForm);

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return list;
	}

	public String insertConcession(UserForm userForm, int bloodBankID) {

		try {

			connection = getConnection();

			String addConcessionQuery = QueryMaker.INSERT_CONCESSION_DETAILS;

			preparedStatement = connection.prepareStatement(addConcessionQuery);

			preparedStatement.setString(1, userForm.getConcessionType());
			preparedStatement.setDouble(2, userForm.getConcessionAmt());
			preparedStatement.setInt(3, bloodBankID);

			preparedStatement.execute();

			System.out.println("Concession inserted successfully into PVConcession table");

			message = "success";

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}
		return message;
	}

	public List<UserForm> retrieveEditConcessionList(int concessionID, String searchConcessionName) {

		List<UserForm> list = new ArrayList<UserForm>();

		UserForm userForm = new UserForm();

		try {

			connection = getConnection();

			String retrieveEditConcessionListQuery = QueryMaker.RETRIEVE_CONCESSION_LIST_BY_ID;

			preparedStatement = connection.prepareStatement(retrieveEditConcessionListQuery);

			preparedStatement.setInt(1, concessionID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				userForm.setConcessionID(resultSet.getInt("id"));
				userForm.setConcessionType(resultSet.getString("concessionType"));
				userForm.setConcessionAmt(resultSet.getDouble("concessionAmt"));
				userForm.setSearchConcessionName(searchConcessionName);

			}

			list.add(userForm);

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return list;
	}

	public String updateConcession(UserForm userForm, int bloodBankID) {

		try {

			connection = getConnection();

			String updateConcessionQuery = QueryMaker.UPDATE_CONCESSION_DETAILS;

			preparedStatement = connection.prepareStatement(updateConcessionQuery);

			preparedStatement.setString(1, userForm.getConcessionType());
			preparedStatement.setDouble(2, userForm.getConcessionAmt());
			preparedStatement.setInt(3, bloodBankID);
			preparedStatement.setInt(4, userForm.getConcessionID());

			preparedStatement.execute();

			System.out.println("Concession updated successfully into PVConcession table");

			message = "success";

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}
		return message;
	}

	public String deleteConcession(int concessionID) {

		try {

			connection = getConnection();

			String deleteConcessionQuery = QueryMaker.DELETE_CONCESSION;

			preparedStatement = connection.prepareStatement(deleteConcessionQuery);

			preparedStatement.setInt(1, concessionID);

			preparedStatement.execute();

			System.out.println("Concession deleted successfully from PVConcession table");

			message = "success";

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}
		return message;
	}

	public List<UserForm> searchChargeType(String chargeType, int bloodBankID) {

		List<UserForm> list = new ArrayList<UserForm>();

		UserForm userForm = null;

		try {

			connection = getConnection();

			String searchChargeTypeQuery = QueryMaker.SEARCH_CHARGE_BY_TYPE;

			preparedStatement = connection.prepareStatement(searchChargeTypeQuery);

			if (chargeType.contains(" ")) {
				chargeType = chargeType.replace(" ", "%");
			}

			preparedStatement.setString(1, "%" + chargeType + "%");
			preparedStatement.setInt(2, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				userForm = new UserForm();

				userForm.setChargeID(resultSet.getInt("id"));
				userForm.setChargeType(resultSet.getString("chargeType"));
				userForm.setCharge(resultSet.getDouble("charges"));
				userForm.setSearchChargeName(chargeType);

				list.add(userForm);

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return list;
	}

	public String insertChargeType(UserForm userForm, int bloodBankID) {

		try {

			connection = getConnection();

			String insertChargeTypeQuery = QueryMaker.INSERT_CHARGE_TYPE_DETAILS;

			preparedStatement = connection.prepareStatement(insertChargeTypeQuery);

			preparedStatement.setString(1, userForm.getChargeType());
			preparedStatement.setDouble(2, userForm.getCharge());
			preparedStatement.setInt(3, bloodBankID);

			preparedStatement.execute();

			System.out.println("Charget Type inserted successfully into PVOtherCharges table");

			message = "success";

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}
		return message;
	}

	public List<UserForm> retrieveEditChargeTypeList(int chargeTypeID, String searchChargeTypeName) {

		List<UserForm> list = new ArrayList<UserForm>();

		UserForm userForm = new UserForm();

		try {

			connection = getConnection();

			String retrieveEditChargeTypeListQuery = QueryMaker.RETRIEVE_CHARGE_TYPE_LIST_BY_ID;

			preparedStatement = connection.prepareStatement(retrieveEditChargeTypeListQuery);

			preparedStatement.setInt(1, chargeTypeID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				userForm.setChargeID(resultSet.getInt("id"));
				userForm.setChargeType(resultSet.getString("chargeType"));
				userForm.setCharge(resultSet.getDouble("charges"));
				userForm.setSearchChargeName(searchChargeTypeName);

				list.add(userForm);

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return list;
	}

	public String updateChargeType(UserForm userForm, int bloodBankID) {

		try {

			connection = getConnection();

			String updateChargeTypeQuery = QueryMaker.UPDATE_CHARGE_TYPE_DETAILS;

			preparedStatement = connection.prepareStatement(updateChargeTypeQuery);

			preparedStatement.setString(1, userForm.getChargeType());
			preparedStatement.setDouble(2, userForm.getCharge());
			preparedStatement.setInt(3, bloodBankID);
			preparedStatement.setInt(4, userForm.getChargeID());

			preparedStatement.execute();

			System.out.println("Charget Type updated successfully into PVOtherCharges table");

			message = "success";

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}
		return message;
	}

	public String deleteChargeTypeID(int chargeTypeID) {

		try {

			connection = getConnection();

			String deleteChargeTypeIDQuery = QueryMaker.DELETE_CHARGE_TYPE;

			preparedStatement = connection.prepareStatement(deleteChargeTypeIDQuery);

			preparedStatement.setInt(1, chargeTypeID);

			preparedStatement.execute();

			System.out.println("Charge type deleted successfully from PVOtherCharges table");

			message = "success";

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}
		return message;
	}

	public List<UserForm> searchComponents(String components, int bloodBankID) {

		List<UserForm> list = new ArrayList<UserForm>();

		UserForm userForm = null;

		try {

			connection = getConnection();

			String searchComponentsQuery = QueryMaker.SEARCH_COMPONENTS;

			preparedStatement = connection.prepareStatement(searchComponentsQuery);

			if (components.contains(" ")) {
				components = components.replace(" ", "%");
			}

			preparedStatement.setString(1, "%" + components + "%");
			preparedStatement.setInt(2, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				userForm = new UserForm();

				userForm.setComponentID(resultSet.getInt("id"));
				userForm.setComponent(resultSet.getString("component"));
				userForm.setComponentCategory(resultSet.getString("category"));
				userForm.setComponentPrice(resultSet.getDouble("price"));
				userForm.setSearchComponentName(components);

				list.add(userForm);

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return list;
	}

	public String insertComponents(UserForm userForm, int bloodBankID) {

		try {

			connection = getConnection();

			String insertComponentsQuery = QueryMaker.INSERT_COMPONENTS_DETAILS;

			preparedStatement = connection.prepareStatement(insertComponentsQuery);

			preparedStatement.setString(1, userForm.getComponent());
			preparedStatement.setString(2, userForm.getComponentCategory());
			preparedStatement.setDouble(3, userForm.getComponentPrice());
			preparedStatement.setInt(4, bloodBankID);

			preparedStatement.execute();

			System.out.println("Component inserted successfully into PVComponents table");
			message = "success";

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}
		return message;
	}

	public List<UserForm> retrieveEditComponentsList(int componentsID, String searchComponentName) {

		List<UserForm> list = new ArrayList<UserForm>();

		UserForm userForm = new UserForm();

		try {

			connection = getConnection();

			String retrieveEditComponentsListQuery = QueryMaker.RETRIEVE_COMPONENTS_LIST_BY_ID;

			preparedStatement = connection.prepareStatement(retrieveEditComponentsListQuery);

			preparedStatement.setInt(1, componentsID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				userForm.setComponentID(resultSet.getInt("id"));
				userForm.setComponent(resultSet.getString("component"));
				userForm.setComponentCategory(resultSet.getString("category"));
				userForm.setComponentPrice(resultSet.getDouble("price"));
				userForm.setSearchComponentName(searchComponentName);

				list.add(userForm);

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return list;
	}

	public String updateComponents(UserForm userForm, int bloodBankID) {

		try {

			connection = getConnection();

			String updateComponentsQuery = QueryMaker.UPDATE_COMPONENTS_DETAILS;

			preparedStatement = connection.prepareStatement(updateComponentsQuery);

			preparedStatement.setString(1, userForm.getComponent());
			preparedStatement.setString(2, userForm.getComponentCategory());
			preparedStatement.setDouble(3, userForm.getComponentPrice());
			preparedStatement.setInt(4, bloodBankID);
			preparedStatement.setInt(5, userForm.getComponentID());

			preparedStatement.execute();

			System.out.println("Component updated successfully into PVComponents table");

			message = "success";

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}
		return message;
	}

	public String deleteComponents(int chargeTypeID) {

		try {

			connection = getConnection();

			String deleteComponentsQuery = QueryMaker.DELETE_COMPONENTS;

			preparedStatement = connection.prepareStatement(deleteComponentsQuery);

			preparedStatement.setInt(1, chargeTypeID);

			preparedStatement.execute();

			System.out.println("Component deleted successfully from PVComponents table");

			message = "success";

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}
		return message;
	}

	public List<UserForm> searchInstructions(String instructions, int bloodBankID) {

		List<UserForm> list = new ArrayList<UserForm>();

		UserForm userForm = null;

		try {

			connection = getConnection();

			String searchInstructionsQuery = QueryMaker.SEARCH_INSTRUCTION;

			preparedStatement = connection.prepareStatement(searchInstructionsQuery);

			if (instructions.contains(" ")) {
				instructions = instructions.replace(" ", "%");
			}

			preparedStatement.setString(1, "%" + instructions + "%");
			preparedStatement.setInt(2, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				userForm = new UserForm();

				userForm.setInstructionID(resultSet.getInt("id"));
				userForm.setInstruction(resultSet.getString("instructions"));
				userForm.setSearchInstruction(instructions);

				list.add(userForm);

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return list;
	}

	public String insertInstructions(UserForm userForm, int bloodBankID) {

		try {

			connection = getConnection();

			String insertInstructionsQuery = QueryMaker.INSERT_INSTRUCTION_DETAILS;

			preparedStatement = connection.prepareStatement(insertInstructionsQuery);

			preparedStatement.setString(1, userForm.getInstruction());
			preparedStatement.setInt(2, bloodBankID);

			preparedStatement.execute();

			System.out.println("Instruction inserted successfully into Instructions table");
			message = "success";

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}
		return message;
	}

	public List<UserForm> retrieveEditInstructionsList(int instructionID, String searchInstructionName) {

		List<UserForm> list = new ArrayList<UserForm>();

		UserForm userForm = new UserForm();

		try {

			connection = getConnection();

			String retrieveEditInstructionsListQuery = QueryMaker.RETRIEVE_INSTRUCTION_LIST_BY_ID;

			preparedStatement = connection.prepareStatement(retrieveEditInstructionsListQuery);

			preparedStatement.setInt(1, instructionID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				userForm.setInstructionID(resultSet.getInt("id"));
				userForm.setInstruction(resultSet.getString("instructions"));
				userForm.setSearchInstruction(searchInstructionName);

				list.add(userForm);

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return list;
	}

	public String updateInstructions(UserForm userForm, int bloodBankID) {

		try {

			connection = getConnection();

			String updateInstructionsQuery = QueryMaker.UPDATE_INSTRUCTION_DETAILS;

			preparedStatement = connection.prepareStatement(updateInstructionsQuery);

			preparedStatement.setString(1, userForm.getInstruction());
			preparedStatement.setInt(2, userForm.getInstructionID());

			preparedStatement.execute();

			System.out.println("Instruction updated successfully into Instructions table");
			message = "success";

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}
		return message;
	}

	public String deleteInstructions(int instructionID) {

		try {

			connection = getConnection();

			String deleteInstructionsQuery = QueryMaker.DELETE_INSTRUCTION;

			preparedStatement = connection.prepareStatement(deleteInstructionsQuery);

			preparedStatement.setInt(1, instructionID);

			preparedStatement.execute();

			System.out.println("Instruction deleted successfully from Instructions table");

			message = "success";

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}
		return message;
	}

	public String retrieveBloodBankRegNo(int bloodBankID) {

		String bloodBankName = null;
		try {

			connection = getConnection();

			String retrieveBloodBankRegNoQuery = QueryMaker.RETRIEVE_BLOOD_BANK_REG_NO;

			preparedStatement = connection.prepareStatement(retrieveBloodBankRegNoQuery);

			preparedStatement.setInt(1, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				bloodBankName = resultSet.getString("registrationNo");

			}

			System.out.println("Blood bank reg no is ::: " + bloodBankName);

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return bloodBankName;
	}

	public String retrieveBloodBankPhone(int bloodBankID) {

		String phone1 = null;
		String phone2 = null;

		String bloodBankPhone = null;
		try {

			connection = getConnection();

			String retrieveBloodBankPhoneQuery = QueryMaker.RETRIEVE_BLOOD_BANK_PHONE;

			preparedStatement = connection.prepareStatement(retrieveBloodBankPhoneQuery);

			preparedStatement.setInt(1, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				phone1 = resultSet.getString("phone1");
				phone2 = resultSet.getString("phone2");

				bloodBankPhone = phone1 + "/" + phone2;

			}

			System.out.println("Blood bank phone no is ::: " + bloodBankPhone);

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return bloodBankPhone;
	}

	public String retrieveBloodBankAddress(int bloodBankID) {

		String bloodBankAdd = null;
		try {

			connection = getConnection();

			String retrieveBloodBankAddressQuery = QueryMaker.RETRIEVE_BLOOD_BANK_ADDRESS;

			preparedStatement = connection.prepareStatement(retrieveBloodBankAddressQuery);

			preparedStatement.setInt(1, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				bloodBankAdd = resultSet.getString("address");

			}

			System.out.println("Blood bank address is ::: " + bloodBankAdd);

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return bloodBankAdd;
	}

	public String retrieveProfilePicName(int userID) {

		String profilePicName = null;
		try {

			connection = getConnection();

			String retrieveProfilePicNameQuery = QueryMaker.RETRIEVE_PROFILE_PIC_NAME;

			preparedStatement = connection.prepareStatement(retrieveProfilePicNameQuery);

			preparedStatement.setInt(1, userID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				profilePicName = resultSet.getString("profilePic");

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return profilePicName;
	}

	public List<UserForm> retrieveComponents(int bloodBankID) {

		List<UserForm> list = new ArrayList<UserForm>();

		UserForm userForm = null;

		try {

			connection = getConnection();

			String retrieveComponentsQuery = QueryMaker.RETRIEVE_COMPONENTS;

			preparedStatement = connection.prepareStatement(retrieveComponentsQuery);

			preparedStatement.setInt(1, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				userForm = new UserForm();

				userForm.setComponentID(resultSet.getInt("id"));
				userForm.setComponent(resultSet.getString("component"));
				userForm.setComponentCategory(resultSet.getString("category"));
				userForm.setComponentPrice(resultSet.getDouble("price"));

				list.add(userForm);

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return list;
	}

	public List<UserForm> retrieveConcession(int bloodBankID) {

		List<UserForm> list = new ArrayList<UserForm>();

		UserForm userForm = null;

		try {

			connection = getConnection();

			String retrieveConcessionQuery = QueryMaker.RETRIEVE_CONCESSION;

			preparedStatement = connection.prepareStatement(retrieveConcessionQuery);

			preparedStatement.setInt(1, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				userForm = new UserForm();

				userForm.setConcessionID(resultSet.getInt("id"));
				userForm.setConcessionType(resultSet.getString("concessionType"));
				userForm.setConcessionAmt(resultSet.getDouble("concessionAmt"));

				list.add(userForm);

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return list;
	}

	public List<UserForm> retrieveChargeType(int bloodBankID) {

		List<UserForm> list = new ArrayList<UserForm>();

		UserForm userForm = null;

		try {

			connection = getConnection();

			String retrieveChargeTypeQuery = QueryMaker.RETRIEVE_CHARGE;

			preparedStatement = connection.prepareStatement(retrieveChargeTypeQuery);

			preparedStatement.setInt(1, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				userForm = new UserForm();

				userForm.setChargeID(resultSet.getInt("id"));
				userForm.setChargeType(resultSet.getString("chargeType"));
				userForm.setCharge(resultSet.getDouble("charges"));

				list.add(userForm);

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return list;
	}

	public List<UserForm> retrieveInstructions(int bloodBankID) {

		List<UserForm> list = new ArrayList<UserForm>();

		UserForm userForm = null;

		try {

			connection = getConnection();

			String retrieveChargeTypeQuery = QueryMaker.RETRIEVE_INSTRUCTION;

			preparedStatement = connection.prepareStatement(retrieveChargeTypeQuery);

			preparedStatement.setInt(1, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				userForm = new UserForm();

				userForm.setInstructionID(resultSet.getInt("id"));
				userForm.setInstruction(resultSet.getString("instructions"));

				list.add(userForm);

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return list;
	}

	public boolean verifyPINChange(int userID, String PIN) {

		boolean result = false;
		// Encrypting PIN
		String encryptedPIN = EncDescUtil.EncryptText(PIN);

		try {

			connection = getConnection();

			String verifyPINChangeQuery = QueryMaker.VERIFY_PIN_CHANGE;

			preparedStatement = connection.prepareStatement(verifyPINChangeQuery);

			preparedStatement.setInt(1, userID);
			preparedStatement.setString(2, encryptedPIN);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				result = true;
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			result = false;
		}
		return result;
	}

	public List<UserForm> searchShifts(String shiftName, int bloodBankID) {

		List<UserForm> list = new ArrayList<UserForm>();

		UserForm userForm = null;

		try {

			connection = getConnection();

			String searchShiftsQuery = QueryMaker.SEARCH_SHIFTS;

			preparedStatement = connection.prepareStatement(searchShiftsQuery);

			if (shiftName.contains(" ")) {
				shiftName = shiftName.replace(" ", "%");
			}

			preparedStatement.setString(1, "%" + shiftName + "%");
			preparedStatement.setInt(2, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				userForm = new UserForm();

				userForm.setShiftID(resultSet.getInt("id"));
				userForm.setShiftName(resultSet.getString("name"));
				userForm.setShiftStartTime(resultSet.getString("starTime"));
				userForm.setShiftEndTime(resultSet.getString("endTime"));
				userForm.setShiftOrder(resultSet.getInt("shiftOrder"));
				userForm.setSearchShiftName(shiftName);

				list.add(userForm);

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return list;
	}

	public List<UserForm> retrieveShifts(int bloodBankID) {

		List<UserForm> list = new ArrayList<UserForm>();

		UserForm userForm = null;

		try {

			connection = getConnection();

			String retrieveShiftsQuery = QueryMaker.RETRIEVE_ALL_SHIFTS;

			preparedStatement = connection.prepareStatement(retrieveShiftsQuery);

			preparedStatement.setInt(1, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				userForm = new UserForm();

				userForm.setShiftID(resultSet.getInt("id"));
				userForm.setShiftName(resultSet.getString("name"));
				userForm.setShiftStartTime(resultSet.getString("starTime"));
				userForm.setShiftEndTime(resultSet.getString("endTime"));
				userForm.setShiftOrder(resultSet.getInt("shiftOrder"));

				list.add(userForm);

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return list;
	}

	public String insertShift(UserForm userForm, int bloodBankID) {

		try {

			connection = getConnection();

			String insertShiftQuery = QueryMaker.INSERT_SHIFT_DETAILS;

			preparedStatement = connection.prepareStatement(insertShiftQuery);

			String startTime = userForm.getShiftStartTimeHH() + ":" + userForm.getShiftStartTimeMM() + " "
					+ userForm.getShiftStratTimeAMPM();

			String endTime = userForm.getShiftEndTimeHH() + ":" + userForm.getShiftEndTimeMM() + " "
					+ userForm.getShiftEndTimeAMPM();

			preparedStatement.setString(1, userForm.getShiftName());
			preparedStatement.setString(2, startTime);
			preparedStatement.setString(3, endTime);
			preparedStatement.setInt(4, userForm.getShiftOrder());
			preparedStatement.setInt(5, bloodBankID);

			preparedStatement.execute();

			System.out.println("shift inserted successfully into PVShifts table");
			message = "success";

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}
		return message;
	}

	public List<UserForm> retrieveEditShiftsList(int shiftID, String searchShiftName) {

		List<UserForm> list = new ArrayList<UserForm>();

		UserForm userForm = new UserForm();

		try {

			connection = getConnection();

			String retrieveEditShiftsListQuery = QueryMaker.RETRIEVE_SHIFT_LIST_BY_ID;

			preparedStatement = connection.prepareStatement(retrieveEditShiftsListQuery);

			preparedStatement.setInt(1, shiftID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				// For start time
				String[] array = resultSet.getString("starTime").split(":");

				String starTimeHH = array[0];

				String[] startTimeArray = array[1].split(" ");

				String startTimeMM = startTimeArray[0];

				String startTimeAMPM = startTimeArray[1];

				// For end time
				String[] array1 = resultSet.getString("endTime").split(":");

				String endTimeHH = array1[0];

				String[] endTimeArray = array1[1].split(" ");

				String endTimeMM = endTimeArray[0];

				String endTimeAMPM = endTimeArray[1];

				userForm.setShiftID(resultSet.getInt("id"));
				userForm.setShiftName(resultSet.getString("name"));
				userForm.setShiftStartTimeHH(starTimeHH);
				userForm.setShiftStratTimeAMPM(startTimeAMPM);
				userForm.setShiftStartTimeMM(startTimeMM);
				userForm.setShiftEndTimeHH(endTimeHH);
				userForm.setShiftEndTimeMM(endTimeMM);
				userForm.setShiftEndTimeAMPM(endTimeAMPM);
				userForm.setSearchShiftName(searchShiftName);
				userForm.setShiftOrder(resultSet.getInt("shiftOrder"));

				list.add(userForm);

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return list;
	}

	public String updateShift(UserForm userForm, int bloodBankID) {

		try {

			connection = getConnection();

			String updateShiftQuery = QueryMaker.UPDATE_SHIFT_DETAILS;

			preparedStatement = connection.prepareStatement(updateShiftQuery);

			String startTime = userForm.getShiftStartTimeHH() + ":" + userForm.getShiftStartTimeMM() + " "
					+ userForm.getShiftStratTimeAMPM();

			String endTime = userForm.getShiftEndTimeHH() + ":" + userForm.getShiftEndTimeMM() + " "
					+ userForm.getShiftEndTimeAMPM();

			preparedStatement.setString(1, userForm.getShiftName());
			preparedStatement.setString(2, startTime);
			preparedStatement.setString(3, endTime);
			preparedStatement.setInt(4, bloodBankID);
			preparedStatement.setInt(5, userForm.getShiftOrder());
			preparedStatement.setInt(6, userForm.getShiftID());

			preparedStatement.execute();

			System.out.println("shift udpated successfully into PVShifts table");
			message = "success";

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}
		return message;
	}

	public String deleteShifts(int shiftID) {

		try {

			connection = getConnection();

			String deleteShiftsQuery = QueryMaker.DELETE_SHIFTS;

			preparedStatement = connection.prepareStatement(deleteShiftsQuery);

			preparedStatement.setInt(1, shiftID);

			preparedStatement.execute();

			System.out.println("Shift deleted successfully from PVShifts table");

			message = "success";

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}
		return message;
	}

	public int verifyOpenRegister() {

		int check = 0;

		Date today = new Date();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		try {

			connection = getConnection();

			String verifyOpenRegisterQuery = QueryMaker.VERIFY_OPEN_REGISTER;

			preparedStatement = connection.prepareStatement(verifyOpenRegisterQuery);

			preparedStatement.setString(1, "%" + dateFormat.format(today) + "%");
			preparedStatement.setString(2, ActivityStatus.ACTIVE);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				check = 1;
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return check;
	}

	public HashMap<Integer, String> retrieveReceiptionistUser(int userID) {

		HashMap<Integer, String> map = new HashMap<Integer, String>();

		String fullName = "";

		try {

			connection = getConnection();

			String retrieveReceiptionistUserQuery = QueryMaker.RETRIEVE_RECEPTIONIST_USER;

			preparedStatement = connection.prepareStatement(retrieveReceiptionistUserQuery);

			preparedStatement.setInt(1, userID);
			preparedStatement.setString(2, "receptionist");

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				if (resultSet.getString("middleName") == null || resultSet.getString("middleName") == "") {
					fullName = resultSet.getString("firstName") + " " + resultSet.getString("lastName");
				} else {
					fullName = resultSet.getString("firstName") + " " + resultSet.getString("middleName") + " "
							+ resultSet.getString("lastName");
				}

				map.put(resultSet.getInt("id"), fullName);
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return map;
	}

	public int verifyYesterdaysOpenRegister() {

		int check = 0;

		// getting yesterday's date
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);

		Date yesterday = cal.getTime();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		try {

			connection = getConnection();

			String verifyOpenRegisterQuery = QueryMaker.VERIFY_OPEN_REGISTER;

			preparedStatement = connection.prepareStatement(verifyOpenRegisterQuery);

			preparedStatement.setString(1, "%" + dateFormat.format(yesterday) + "%");
			preparedStatement.setString(2, ActivityStatus.ENABLE);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				check = 1;
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return check;
	}

	public double retrieveExpectedCash() {

		double expectedCash = 0D;

		try {

			connection = getConnection();

			String retrieveExpectedCashQuery = QueryMaker.RETRIEVE_EXPECTED_CASH;

			preparedStatement = connection.prepareStatement(retrieveExpectedCashQuery);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				expectedCash = resultSet.getDouble("sum");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return expectedCash;
	}

	public int verifyFirstRegister() {

		int check = 0;

		try {

			connection = getConnection();

			String verifyFirstRegisterQuery = QueryMaker.VERIFY_FIRST_REGISTER_ENTRY;

			preparedStatement = connection.prepareStatement(verifyFirstRegisterQuery);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				check = 1;
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return check;
	}

	public double retrieveOtherProductCash() {

		double otherProductCash = 0D;

		try {

			connection = getConnection();

			String retrieveOtherProductCashQuery = QueryMaker.RETRIEVE_OTHER_PRODUCT_CASH_BALANCE;

			preparedStatement = connection.prepareStatement(retrieveOtherProductCashQuery);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				otherProductCash = resultSet.getDouble("sum");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return otherProductCash;
	}
}
