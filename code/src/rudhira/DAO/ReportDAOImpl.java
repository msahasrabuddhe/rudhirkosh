package rudhira.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import rudhira.form.BillingForm;
import rudhira.form.ReportForm;
import rudhira.util.ConfigXMLUtil;
import rudhira.util.DBConnection;
import rudhira.util.QueryMaker;

/**
 * 
 * @author Kovid Bioanalytics
 * 
 */
public class ReportDAOImpl extends DBConnection implements ReportDAOInf {

	ConfigXMLUtil xmlUtil = null;

	Connection connection = null;
	PreparedStatement preparedStatement = null;
	ResultSet resultSet = null;

	String message = "error";

	public List<ReportForm> retrievePatientReportList(String searchReportName, String searchCriteria, String startDate,
			String endDate) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");

		SimpleDateFormat dateToBeFormatted = new SimpleDateFormat("dd-MM-yyyy");

		List<ReportForm> list = new ArrayList<ReportForm>();

		ReportForm reportForm = null;

		SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");

		try {

			connection = getConnection();

			/*
			 * Check searchCriteria and based on that select the search query
			 */
			if (searchCriteria.equals("Patient Name")) {

				if (searchReportName.contains(" ")) {
					searchReportName = searchReportName.replace(" ", "%");
				}

				if (startDate.isEmpty() || endDate.isEmpty()) {

					/*
					 * Search query based on patient name
					 */
					String retrievePatientReportListQuery = QueryMaker.SEARCH_PATIENT_RETPORT_ON_PATIENT_NAME;

					preparedStatement = connection.prepareStatement(retrievePatientReportListQuery);

					preparedStatement.setString(1, "%" + searchReportName + "%");

					resultSet = preparedStatement.executeQuery();

					while (resultSet.next()) {

						reportForm = new ReportForm();

						reportForm.setPatientID(resultSet.getInt("patientID"));
						reportForm.setReceiptID(resultSet.getInt("id"));
						reportForm.setPatientName(
								resultSet.getString("firstName") + " " + resultSet.getString("lastName"));
						reportForm.setReceiptNo(resultSet.getString("receiptNo"));
						reportForm.setReceiptDate(
								dateToBeFormatted.format(dateFormat.parse(resultSet.getString("receiptDate"))));
						reportForm.setBBRNo(resultSet.getString("bdrNo"));

						list.add(reportForm);

					}

				} else {

					/*
					 * Search query based on patient name
					 */
					String retrievePatientReportListQuery = QueryMaker.SEARCH_PATIENT_RETPORT_ON_PATIENT_NAME_BY_DATE;

					preparedStatement = connection.prepareStatement(retrievePatientReportListQuery);

					preparedStatement.setString(1, "%" + searchReportName + "%");
					preparedStatement.setString(2, dateFormat1.format(dateToBeFormatted.parse(startDate)) + " 00:00");
					preparedStatement.setString(3, dateFormat1.format(dateToBeFormatted.parse(endDate)) + " 23:59");

					resultSet = preparedStatement.executeQuery();

					while (resultSet.next()) {

						reportForm = new ReportForm();

						reportForm.setPatientID(resultSet.getInt("patientID"));
						reportForm.setReceiptID(resultSet.getInt("id"));
						reportForm.setPatientName(
								resultSet.getString("firstName") + " " + resultSet.getString("lastName"));
						reportForm.setReceiptNo(resultSet.getString("receiptNo"));
						reportForm.setReceiptDate(
								dateToBeFormatted.format(dateFormat.parse(resultSet.getString("receiptDate"))));
						reportForm.setBBRNo(resultSet.getString("bdrNo"));

						list.add(reportForm);

					}

				}

			} else if (searchCriteria.equals("Storage Center")) {

				if (startDate.isEmpty() || endDate.isEmpty()) {

					/*
					 * Search query based on Receipt No
					 */
					String retrievePatientReportListQuery = QueryMaker.SEARCH_PATIENT_RETPORT_ON_STORAGE_CENTRE;

					preparedStatement = connection.prepareStatement(retrievePatientReportListQuery);

					preparedStatement.setString(1, "%" + searchReportName + "%");

					resultSet = preparedStatement.executeQuery();

					while (resultSet.next()) {

						reportForm = new ReportForm();

						reportForm.setPatientID(resultSet.getInt("patientID"));
						reportForm.setReceiptID(resultSet.getInt("id"));
						reportForm.setStorageCenter(resultSet.getString("bloodBankStorage"));
						reportForm.setReceiptNo(resultSet.getString("receiptNo"));
						reportForm.setReceiptDate(
								dateToBeFormatted.format(dateFormat.parse(resultSet.getString("receiptDate"))));
						reportForm.setBBRNo(resultSet.getString("bdrNo"));

						list.add(reportForm);

					}

				} else {

					/*
					 * Search query based on Receipt No
					 */
					String retrievePatientReportListQuery = QueryMaker.SEARCH_PATIENT_RETPORT_ON_STORAGE_CENTRE_BY_DATE;

					preparedStatement = connection.prepareStatement(retrievePatientReportListQuery);

					preparedStatement.setString(1, "%" + searchReportName + "%");
					preparedStatement.setString(2, dateFormat1.format(dateToBeFormatted.parse(startDate)) + " 00:00");
					preparedStatement.setString(3, dateFormat1.format(dateToBeFormatted.parse(endDate)) + " 23:59");

					resultSet = preparedStatement.executeQuery();

					while (resultSet.next()) {

						reportForm = new ReportForm();

						reportForm.setPatientID(resultSet.getInt("patientID"));
						reportForm.setReceiptID(resultSet.getInt("id"));
						reportForm.setStorageCenter(resultSet.getString("bloodBankStorage"));
						reportForm.setReceiptNo(resultSet.getString("receiptNo"));
						reportForm.setReceiptDate(
								dateToBeFormatted.format(dateFormat.parse(resultSet.getString("receiptDate"))));
						reportForm.setBBRNo(resultSet.getString("bdrNo"));

						list.add(reportForm);

					}

				}

			}

			else if (searchCriteria.equals("Receipt No")) {

				if (startDate.isEmpty() || endDate.isEmpty()) {

					/*
					 * Search query based on Receipt No
					 */
					String retrievePatientReportListQuery = QueryMaker.SEARCH_PATIENT_RETPORT_ON_RECEIPT_NO;

					preparedStatement = connection.prepareStatement(retrievePatientReportListQuery);

					preparedStatement.setString(1, "%" + searchReportName + "%");

					resultSet = preparedStatement.executeQuery();

					while (resultSet.next()) {

						reportForm = new ReportForm();

						reportForm.setPatientID(resultSet.getInt("patientID"));
						reportForm.setReceiptID(resultSet.getInt("id"));
						reportForm.setPatientName(
								resultSet.getString("firstName") + " " + resultSet.getString("lastName"));
						reportForm.setReceiptNo(resultSet.getString("receiptNo"));
						reportForm.setReceiptDate(
								dateToBeFormatted.format(dateFormat.parse(resultSet.getString("receiptDate"))));
						reportForm.setBBRNo(resultSet.getString("bdrNo"));

						list.add(reportForm);

					}

				} else {

					/*
					 * Search query based on Receipt No
					 */
					String retrievePatientReportListQuery = QueryMaker.SEARCH_PATIENT_RETPORT_ON_RECEIPT_NO_BY_DATE;

					preparedStatement = connection.prepareStatement(retrievePatientReportListQuery);

					preparedStatement.setString(1, "%" + searchReportName + "%");
					preparedStatement.setString(2, dateFormat1.format(dateToBeFormatted.parse(startDate)) + " 00:00");
					preparedStatement.setString(3, dateFormat1.format(dateToBeFormatted.parse(endDate)) + " 23:59");

					resultSet = preparedStatement.executeQuery();

					while (resultSet.next()) {

						reportForm = new ReportForm();

						reportForm.setPatientID(resultSet.getInt("patientID"));
						reportForm.setReceiptID(resultSet.getInt("id"));
						reportForm.setPatientName(
								resultSet.getString("firstName") + " " + resultSet.getString("lastName"));
						reportForm.setReceiptNo(resultSet.getString("receiptNo"));
						reportForm.setReceiptDate(
								dateToBeFormatted.format(dateFormat.parse(resultSet.getString("receiptDate"))));
						reportForm.setBBRNo(resultSet.getString("bdrNo"));

						list.add(reportForm);

					}

				}

			} else if (searchCriteria.equals("BDR No.")) {

				if (startDate.isEmpty() || endDate.isEmpty()) {

					/*
					 * Search query based on BDR No
					 */
					String retrievePatientReportListQuery = QueryMaker.SEARCH_PATIENT_RETPORT_ON_BDR_NO;

					preparedStatement = connection.prepareStatement(retrievePatientReportListQuery);

					preparedStatement.setString(1, searchReportName);

					resultSet = preparedStatement.executeQuery();

					while (resultSet.next()) {

						reportForm = new ReportForm();

						reportForm.setPatientID(resultSet.getInt("patientID"));
						reportForm.setReceiptID(resultSet.getInt("id"));
						reportForm.setPatientName(
								resultSet.getString("firstName") + " " + resultSet.getString("lastName"));
						reportForm.setReceiptNo(resultSet.getString("receiptNo"));
						reportForm.setReceiptDate(
								dateToBeFormatted.format(dateFormat.parse(resultSet.getString("receiptDate"))));
						reportForm.setBBRNo(resultSet.getString("bdrNo"));

						list.add(reportForm);

					}

				} else {

					/*
					 * Search query based on BDR No
					 */
					String retrievePatientReportListQuery = QueryMaker.SEARCH_PATIENT_RETPORT_ON_BDR_NO_BY_DATE;

					preparedStatement = connection.prepareStatement(retrievePatientReportListQuery);

					preparedStatement.setString(1, searchReportName);
					preparedStatement.setString(2, dateFormat1.format(dateToBeFormatted.parse(startDate)) + " 00:00");
					preparedStatement.setString(3, dateFormat1.format(dateToBeFormatted.parse(endDate)) + " 23:59");

					resultSet = preparedStatement.executeQuery();

					while (resultSet.next()) {

						reportForm = new ReportForm();

						reportForm.setPatientID(resultSet.getInt("patientID"));
						reportForm.setReceiptID(resultSet.getInt("id"));
						reportForm.setPatientName(
								resultSet.getString("firstName") + " " + resultSet.getString("lastName"));
						reportForm.setReceiptNo(resultSet.getString("receiptNo"));
						reportForm.setReceiptDate(
								dateToBeFormatted.format(dateFormat.parse(resultSet.getString("receiptDate"))));
						reportForm.setBBRNo(resultSet.getString("bdrNo"));

						list.add(reportForm);

					}

				}

			} else {

				if (startDate.isEmpty() || endDate.isEmpty()) {

					/*
					 * Search query based on bag no
					 */
					String retrievePatientReportListQuery = QueryMaker.SEARCH_PATIENT_REPORT_ON_BAG_NO;

					preparedStatement = connection.prepareStatement(retrievePatientReportListQuery);

					preparedStatement.setString(1, "%" + searchReportName + "%");

					resultSet = preparedStatement.executeQuery();

					while (resultSet.next()) {

						reportForm = new ReportForm();

						reportForm.setPatientID(resultSet.getInt("patientID"));
						reportForm.setReceiptID(resultSet.getInt("id"));
						reportForm.setPatientName(
								resultSet.getString("firstName") + " " + resultSet.getString("lastName"));
						reportForm.setReceiptNo(resultSet.getString("receiptNo"));
						reportForm.setReceiptDate(
								dateToBeFormatted.format(dateFormat.parse(resultSet.getString("receiptDate"))));
						reportForm.setBBRNo(resultSet.getString("bdrNo"));
						reportForm.setBagNo(resultSet.getString("bagNo"));

						list.add(reportForm);

					}

				} else {

					/*
					 * Search query based on bag no
					 */
					String retrievePatientReportListQuery = QueryMaker.SEARCH_PATIENT_REPORT_ON_BAG_NO_BY_DATE;

					preparedStatement = connection.prepareStatement(retrievePatientReportListQuery);

					preparedStatement.setString(1, "%" + searchReportName + "%");
					preparedStatement.setString(2, dateFormat1.format(dateToBeFormatted.parse(startDate)) + " 00:00");
					preparedStatement.setString(3, dateFormat1.format(dateToBeFormatted.parse(endDate)) + " 23:59");

					resultSet = preparedStatement.executeQuery();

					while (resultSet.next()) {

						reportForm = new ReportForm();

						reportForm.setPatientID(resultSet.getInt("patientID"));
						reportForm.setReceiptID(resultSet.getInt("id"));
						reportForm.setPatientName(
								resultSet.getString("firstName") + " " + resultSet.getString("lastName"));
						reportForm.setReceiptNo(resultSet.getString("receiptNo"));
						reportForm.setReceiptDate(
								dateToBeFormatted.format(dateFormat.parse(resultSet.getString("receiptDate"))));
						reportForm.setBBRNo(resultSet.getString("bdrNo"));
						reportForm.setBagNo(resultSet.getString("bagNo"));

						list.add(reportForm);

					}

				}

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return list;
	}

	public List<ReportForm> retrieveCashHandoverReportList(String startDate, String endDate) {

		List<ReportForm> list = new ArrayList<ReportForm>();

		ReportForm reportForm = null;

		SimpleDateFormat dateToBeFormatted = new SimpleDateFormat("yyyy-MM-dd");

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

		try {
			connection = getConnection();

			String retrieveCashHandoverReportListQuery = QueryMaker.RETRIEVE_CASH_HANDOVER_REPORT_DETAILS;

			preparedStatement = connection.prepareStatement(retrieveCashHandoverReportListQuery);

			preparedStatement.setString(1, dateToBeFormatted.format(dateFormat.parse(startDate)));
			preparedStatement.setString(2, dateToBeFormatted.format(dateFormat.parse(endDate)));

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				reportForm = new ReportForm();

				reportForm.setRegisterDate(dateFormat.format(resultSet.getDate("registerDate")));
				reportForm.setShift(resultSet.getString("shift"));
				reportForm.setCashHandover(resultSet.getDouble("cashHandover"));
				reportForm.setCashDeposited(resultSet.getDouble("cashDeposited"));
				reportForm.setTotalCash(resultSet.getDouble("shiftCash"));
				reportForm.setRegisterBalance(resultSet.getDouble("registerBalance"));
				reportForm.setOtherProductAmount(resultSet.getDouble("otherProduct"));
				reportForm.setStartDate(startDate);
				reportForm.setEndDate(endDate);

				list.add(reportForm);

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return list;
	}

	public List<ReportForm> retrieveComponentReportList(String startDate, String endDate, String searchComponentName) {

		List<ReportForm> list = new ArrayList<ReportForm>();

		ReportForm reportForm = null;

		SimpleDateFormat dateToBeFormatted = new SimpleDateFormat("yyyy-MM-dd");

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

		int srNo = 1;

		try {

			connection = getConnection();

			if (startDate.isEmpty() || endDate.isEmpty()) {

				/*
				 * Checking whether All is selected, if so, then searching the report based on
				 * All components, else searching for only selected component
				 */
				if (searchComponentName.equals("0")) {

					String retrieveComponentReportListQuery = QueryMaker.RETRIEVE_COMPONENT_REPORT_LIST_FOR_ALL;

					preparedStatement = connection.prepareStatement(retrieveComponentReportListQuery);

					resultSet = preparedStatement.executeQuery();

					while (resultSet.next()) {

						reportForm = new ReportForm();

						reportForm.setSrNo(srNo);
						reportForm.setPatientID(resultSet.getInt("patientID"));
						reportForm.setReceiptID(resultSet.getInt("id"));
						reportForm.setPatientName(
								resultSet.getString("firstName") + " " + resultSet.getString("lastName"));
						reportForm.setReceiptNo(resultSet.getString("receiptNo"));
						reportForm.setReceiptDate(dateFormat.format(resultSet.getDate("receiptDate")));
						reportForm.setBBRNo(resultSet.getString("bdrNo"));
						reportForm.setComponentName(resultSet.getString("product"));
						reportForm.setComponentRate(resultSet.getDouble("rate"));

						list.add(reportForm);

						srNo++;

					}

				} else {

					String retrieveComponentReportListQuery = QueryMaker.RETRIEVE_COMPONENT_REPORT_LIST;

					preparedStatement = connection.prepareStatement(retrieveComponentReportListQuery);

					preparedStatement.setString(1, searchComponentName);

					resultSet = preparedStatement.executeQuery();

					while (resultSet.next()) {

						reportForm = new ReportForm();

						reportForm.setSrNo(srNo);
						reportForm.setPatientID(resultSet.getInt("patientID"));
						reportForm.setReceiptID(resultSet.getInt("id"));
						reportForm.setPatientName(
								resultSet.getString("firstName") + " " + resultSet.getString("lastName"));
						reportForm.setReceiptNo(resultSet.getString("receiptNo"));
						reportForm.setReceiptDate(dateFormat.format(resultSet.getDate("receiptDate")));
						reportForm.setBBRNo(resultSet.getString("bdrNo"));
						reportForm.setComponentName(resultSet.getString("product"));
						reportForm.setComponentRate(resultSet.getDouble("rate"));

						list.add(reportForm);

						srNo++;

					}

				}

			} else {

				/*
				 * Checking whether All is selected, if so, then searching the report based on
				 * All components, else searching for only selected component
				 */
				if (searchComponentName.equals("0")) {

					String retrieveComponentReportListQuery = QueryMaker.RETRIEVE_COMPONENT_REPORT_LIST_BY_DATE_FOR_ALL;

					preparedStatement = connection.prepareStatement(retrieveComponentReportListQuery);

					preparedStatement.setString(1, dateToBeFormatted.format(dateFormat.parse(startDate)) + " 00:00");
					preparedStatement.setString(2, dateToBeFormatted.format(dateFormat.parse(endDate)) + " 23:59");

					resultSet = preparedStatement.executeQuery();

					while (resultSet.next()) {

						reportForm = new ReportForm();

						reportForm.setSrNo(srNo);
						reportForm.setPatientID(resultSet.getInt("patientID"));
						reportForm.setReceiptID(resultSet.getInt("id"));
						reportForm.setPatientName(
								resultSet.getString("firstName") + " " + resultSet.getString("lastName"));
						reportForm.setReceiptNo(resultSet.getString("receiptNo"));
						reportForm.setReceiptDate(dateFormat.format(resultSet.getDate("receiptDate")));
						reportForm.setBBRNo(resultSet.getString("bdrNo"));
						reportForm.setComponentName(resultSet.getString("product"));
						reportForm.setComponentRate(resultSet.getDouble("rate"));

						list.add(reportForm);

						srNo++;
					}

				} else {

					String retrieveComponentReportListQuery = QueryMaker.RETRIEVE_COMPONENT_REPORT_LIST_BY_DATE;

					preparedStatement = connection.prepareStatement(retrieveComponentReportListQuery);

					preparedStatement.setString(1, searchComponentName);
					preparedStatement.setString(2, dateToBeFormatted.format(dateFormat.parse(startDate)) + " 00:00");
					preparedStatement.setString(3, dateToBeFormatted.format(dateFormat.parse(endDate)) + " 23:59");

					resultSet = preparedStatement.executeQuery();

					while (resultSet.next()) {

						reportForm = new ReportForm();

						reportForm.setSrNo(srNo);
						reportForm.setPatientID(resultSet.getInt("patientID"));
						reportForm.setReceiptID(resultSet.getInt("id"));
						reportForm.setPatientName(
								resultSet.getString("firstName") + " " + resultSet.getString("lastName"));
						reportForm.setReceiptNo(resultSet.getString("receiptNo"));
						reportForm.setReceiptDate(dateFormat.format(resultSet.getDate("receiptDate")));
						reportForm.setBBRNo(resultSet.getString("bdrNo"));
						reportForm.setComponentName(resultSet.getString("product"));
						reportForm.setComponentRate(resultSet.getDouble("rate"));

						list.add(reportForm);

						srNo++;

					}

				}

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return list;
	}

	public List<ReportForm> retrieveRegiterReportList(String startDate, String endDate) {

		List<ReportForm> list = new ArrayList<ReportForm>();

		ReportForm reportForm = null;

		SimpleDateFormat dateToBeFormatted = new SimpleDateFormat("yyyy-MM-dd");

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

		int srNo = 1;

		try {

			connection = getConnection();

			String retrieveRegiterReportListQuery = QueryMaker.RETRIEVE_REGISTER_REPORT;

			preparedStatement = connection.prepareStatement(retrieveRegiterReportListQuery);

			preparedStatement.setString(1, dateToBeFormatted.format(dateFormat.parse(startDate)));
			preparedStatement.setString(2, dateToBeFormatted.format(dateFormat.parse(endDate)));

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				reportForm = new ReportForm();

				reportForm.setSrNo(srNo);
				reportForm.setCashHandover(resultSet.getDouble("handOverCash"));
				reportForm.setBloodBankBalance(resultSet.getDouble("bloodBankBalance"));
				reportForm.setRegisterDate(dateFormat.format(resultSet.getDate("registerDate")));
				reportForm.setOtherProductBalance(resultSet.getDouble("otherProductBalance"));
				reportForm.setExpectedCashBalance(resultSet.getDouble("expectedCashBalance"));
				reportForm.setActualCashBalance(resultSet.getDouble("actualCashBalance"));
				reportForm.setBankDeposited(resultSet.getDouble("bankDeposit"));
				reportForm.setCashAdjusted(resultSet.getDouble("cashAdjustment"));
				reportForm.setCashMismatch(resultSet.getDouble("cashMismatch"));

				srNo++;

				list.add(reportForm);

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return list;
	}

	public List<String> retrieveConcessionTypeList(int bloodBankID) {

		List<String> list = new ArrayList<String>();

		try {

			connection = getConnection();

			String retrieveConcessionTypeListQuery = QueryMaker.RETRIEVE_CONCESSION_TYPE_LIST;

			preparedStatement = connection.prepareStatement(retrieveConcessionTypeListQuery);

			preparedStatement.setInt(1, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				list.add(resultSet.getString("concessionType"));
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return list;
	}

	public List<ReportForm> retrieveConcessionReportList(String startDate, String endDate,
			String searchConcessionName) {

		List<ReportForm> list = new ArrayList<ReportForm>();

		ReportForm reportForm = null;

		SimpleDateFormat dateToBeFormatted = new SimpleDateFormat("yyyy-MM-dd");

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

		try {

			connection = getConnection();

			if (startDate.isEmpty() || endDate.isEmpty()) {

				/*
				 * Checking whether All is selected, if so, then searching the report based on
				 * All concessions, else searching for only selected concession
				 */
				if (searchConcessionName.equals("0")) {

					String retrieveConcessionReportListQuery = QueryMaker.RETRIEVE_CONCESSION_REPORT_LIST_FOR_ALL;

					preparedStatement = connection.prepareStatement(retrieveConcessionReportListQuery);

					resultSet = preparedStatement.executeQuery();

					while (resultSet.next()) {

						reportForm = new ReportForm();

						reportForm.setPatientID(resultSet.getInt("patientID"));
						reportForm.setReceiptID(resultSet.getInt("id"));
						reportForm.setPatientName(
								resultSet.getString("firstName") + " " + resultSet.getString("lastName"));
						reportForm.setReceiptNo(resultSet.getString("receiptNo"));
						reportForm.setReceiptDate(dateFormat.format(resultSet.getDate("receiptDate")));
						reportForm.setConcessionType(resultSet.getString("concessionType"));
						reportForm.setConcessionAmt(resultSet.getDouble("concessionAmt"));
						// reportForm.setNetReceivableAmt(resultSet.getDouble("netReceivableAmt"));

						list.add(reportForm);

					}

				} else {

					String retrieveConcessionReportListQuery = QueryMaker.RETRIEVE_CONCESSION_REPORT_LIST;

					preparedStatement = connection.prepareStatement(retrieveConcessionReportListQuery);

					preparedStatement.setString(1, searchConcessionName);

					resultSet = preparedStatement.executeQuery();

					while (resultSet.next()) {

						reportForm = new ReportForm();

						reportForm.setPatientID(resultSet.getInt("patientID"));
						reportForm.setReceiptID(resultSet.getInt("id"));
						reportForm.setPatientName(
								resultSet.getString("firstName") + " " + resultSet.getString("lastName"));
						reportForm.setReceiptNo(resultSet.getString("receiptNo"));
						reportForm.setReceiptDate(dateFormat.format(resultSet.getDate("receiptDate")));
						reportForm.setConcessionType(resultSet.getString("concessionType"));
						reportForm.setConcessionAmt(resultSet.getDouble("concessionAmt"));
						// reportForm.setNetReceivableAmt(resultSet.getDouble("netReceivableAmt"));

						list.add(reportForm);

					}

				}

			} else {

				/*
				 * Checking whether All is selected, if so, then searching the report based on
				 * All concessions, else searching for only selected concession
				 */
				if (searchConcessionName.equals("0")) {

					String retrieveConcessionReportListQuery = QueryMaker.RETRIEVE_CONCESSION_REPORT_LIST_BY_DATE_FOR_ALL;

					preparedStatement = connection.prepareStatement(retrieveConcessionReportListQuery);

					preparedStatement.setString(1, dateToBeFormatted.format(dateFormat.parse(startDate)) + " 00:00");
					preparedStatement.setString(2, dateToBeFormatted.format(dateFormat.parse(endDate)) + " 23:59");

					resultSet = preparedStatement.executeQuery();

					while (resultSet.next()) {

						reportForm = new ReportForm();

						reportForm.setPatientID(resultSet.getInt("patientID"));
						reportForm.setReceiptID(resultSet.getInt("id"));
						reportForm.setPatientName(
								resultSet.getString("firstName") + " " + resultSet.getString("lastName"));
						reportForm.setReceiptNo(resultSet.getString("receiptNo"));
						reportForm.setReceiptDate(dateFormat.format(resultSet.getDate("receiptDate")));
						reportForm.setConcessionType(resultSet.getString("concessionType"));
						reportForm.setConcessionAmt(resultSet.getDouble("concessionAmt"));
						// reportForm.setNetReceivableAmt(resultSet.getDouble("netReceivableAmt"));

						list.add(reportForm);

					}

				} else {

					String retrieveConcessionReportListQuery = QueryMaker.RETRIEVE_CONCESSION_REPORT_LIST_BY_DATE;

					preparedStatement = connection.prepareStatement(retrieveConcessionReportListQuery);

					preparedStatement.setString(1, searchConcessionName);
					preparedStatement.setString(2, dateToBeFormatted.format(dateFormat.parse(startDate)) + " 00:00");
					preparedStatement.setString(3, dateToBeFormatted.format(dateFormat.parse(endDate)) + " 23:59");

					resultSet = preparedStatement.executeQuery();

					while (resultSet.next()) {

						reportForm = new ReportForm();

						reportForm.setPatientID(resultSet.getInt("patientID"));
						reportForm.setReceiptID(resultSet.getInt("id"));
						reportForm.setPatientName(
								resultSet.getString("firstName") + " " + resultSet.getString("lastName"));
						reportForm.setReceiptNo(resultSet.getString("receiptNo"));
						reportForm.setReceiptDate(dateFormat.format(resultSet.getDate("receiptDate")));
						reportForm.setConcessionType(resultSet.getString("concessionType"));
						reportForm.setConcessionAmt(resultSet.getDouble("concessionAmt"));
						// reportForm.setNetReceivableAmt(resultSet.getDouble("netReceivableAmt"));

						list.add(reportForm);

					}

				}

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return list;
	}

	public List<ReportForm> retrieveCardChequeReportList(String startDate, String endDate, String searchComponentName) {

		List<ReportForm> list = new ArrayList<ReportForm>();

		ReportForm reportForm = null;

		SimpleDateFormat dateToBeFormatted = new SimpleDateFormat("yyyy-MM-dd");

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

		try {

			connection = getConnection();

			if (startDate.isEmpty() || endDate.isEmpty()) {

				/*
				 * Checking whether Both is selected, if so, then searching the report based on
				 * both card and cheque payment type, else searching for only selected payment
				 * type
				 */
				if (searchComponentName.equals("Both")) {

					String retrieveConcessionReportListQuery = QueryMaker.RETRIEVE_CARD_CHEQUE_REPORT_LIST_FOR_BOTH;

					preparedStatement = connection.prepareStatement(retrieveConcessionReportListQuery);

					resultSet = preparedStatement.executeQuery();

					while (resultSet.next()) {

						reportForm = new ReportForm();

						reportForm.setPatientID(resultSet.getInt("patientID"));
						reportForm.setReceiptID(resultSet.getInt("id"));
						reportForm.setPatientName(
								resultSet.getString("firstName") + " " + resultSet.getString("lastName"));
						reportForm.setReceiptNo(resultSet.getString("receiptNo"));
						reportForm.setReceiptDate(dateFormat.format(resultSet.getDate("receiptDate")));
						reportForm.setPaymentType(resultSet.getString("paymentType"));
						reportForm.setNetReceivableAmt(resultSet.getDouble("netReceivableAmt"));

						list.add(reportForm);

					}

				} else {

					String retrieveConcessionReportListQuery = QueryMaker.RETRIEVE_CARD_CHEQUE_REPORT_LIST;

					preparedStatement = connection.prepareStatement(retrieveConcessionReportListQuery);

					preparedStatement.setString(1, searchComponentName);

					resultSet = preparedStatement.executeQuery();

					while (resultSet.next()) {

						reportForm = new ReportForm();

						reportForm.setPatientID(resultSet.getInt("patientID"));
						reportForm.setReceiptID(resultSet.getInt("id"));
						reportForm.setPatientName(
								resultSet.getString("firstName") + " " + resultSet.getString("lastName"));
						reportForm.setReceiptNo(resultSet.getString("receiptNo"));
						reportForm.setReceiptDate(dateFormat.format(resultSet.getDate("receiptDate")));
						reportForm.setPaymentType(resultSet.getString("paymentType"));
						reportForm.setNetReceivableAmt(resultSet.getDouble("netReceivableAmt"));

						list.add(reportForm);

					}

				}

			} else {

				/*
				 * Checking whether Both is selected, if so, then searching the report based on
				 * both card and cheque payment type, else searching for only selected payment
				 * type
				 */
				if (searchComponentName.equals("Both")) {

					String retrieveConcessionReportListQuery = QueryMaker.RETRIEVE_CARD_CHEQUE_REPORT_LIST_BY_DATE_FOR_BOTH;

					preparedStatement = connection.prepareStatement(retrieveConcessionReportListQuery);

					preparedStatement.setString(1, dateToBeFormatted.format(dateFormat.parse(startDate)) + " 00:00");
					preparedStatement.setString(2, dateToBeFormatted.format(dateFormat.parse(endDate)) + " 23:59");

					resultSet = preparedStatement.executeQuery();

					while (resultSet.next()) {

						reportForm = new ReportForm();

						reportForm.setPatientID(resultSet.getInt("patientID"));
						reportForm.setReceiptID(resultSet.getInt("id"));
						reportForm.setPatientName(
								resultSet.getString("firstName") + " " + resultSet.getString("lastName"));
						reportForm.setReceiptNo(resultSet.getString("receiptNo"));
						reportForm.setReceiptDate(dateFormat.format(resultSet.getDate("receiptDate")));
						reportForm.setPaymentType(resultSet.getString("paymentType"));
						reportForm.setNetReceivableAmt(resultSet.getDouble("netReceivableAmt"));

						list.add(reportForm);

					}

				} else {

					String retrieveConcessionReportListQuery = QueryMaker.RETRIEVE_CARD_CHEQUE_REPORT_LIST_BY_DATE;

					preparedStatement = connection.prepareStatement(retrieveConcessionReportListQuery);

					preparedStatement.setString(1, searchComponentName);
					preparedStatement.setString(2, dateToBeFormatted.format(dateFormat.parse(startDate)) + " 00:00");
					preparedStatement.setString(3, dateToBeFormatted.format(dateFormat.parse(endDate)) + " 23:59");

					resultSet = preparedStatement.executeQuery();

					while (resultSet.next()) {

						reportForm = new ReportForm();

						reportForm.setPatientID(resultSet.getInt("patientID"));
						reportForm.setReceiptID(resultSet.getInt("id"));
						reportForm.setPatientName(
								resultSet.getString("firstName") + " " + resultSet.getString("lastName"));
						reportForm.setReceiptNo(resultSet.getString("receiptNo"));
						reportForm.setReceiptDate(dateFormat.format(resultSet.getDate("receiptDate")));
						reportForm.setPaymentType(resultSet.getString("paymentType"));
						reportForm.setNetReceivableAmt(resultSet.getDouble("netReceivableAmt"));

						list.add(reportForm);

					}

				}

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return list;
	}

	public BillingForm retrieveChequeCardDetails(int receiptID) {

		BillingForm billingForm = null;

		try {
			connection = getConnection();

			String retrieveChequeCardDetailsQuery = QueryMaker.RETRIEVE_CHEQUE_DETAILS;

			preparedStatement = connection.prepareStatement(retrieveChequeCardDetailsQuery);

			preparedStatement.setInt(1, receiptID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				billingForm = new BillingForm();

				billingForm.setChequeBankName(resultSet.getString("bankName"));
				billingForm.setChequeAmt(resultSet.getDouble("chequeAmt"));
				billingForm.setChequeBankBranch(resultSet.getString("bankBranch"));
				billingForm.setChequeIssuedBy(resultSet.getString("chequeIssuedBy"));
				billingForm.setChequeNo(resultSet.getString("chequeNumber"));
				billingForm.setCardMobileNo(resultSet.getString("mobileNo"));
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return billingForm;
	}

	public List<ReportForm> retrievebloodStorageCenterList() {

		List<ReportForm> list = new ArrayList<ReportForm>();

		ReportForm form = null;

		int srNo = 1;

		try {

			connection = getConnection();

			String retrievebloodStorageCenterListQuery = QueryMaker.RETRIEVE_BLOOD_STORAGE_CENTER_LIST;

			preparedStatement = connection.prepareStatement(retrievebloodStorageCenterListQuery);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				form = new ReportForm();

				form.setStorageCenter(resultSet.getString("name"));
				form.setSrNo(srNo);
				form.setStorageCenterID(resultSet.getInt("id"));

				list.add(form);

				srNo++;
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return list;
	}
}
