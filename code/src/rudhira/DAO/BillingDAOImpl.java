package rudhira.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import rudhira.form.BillingForm;
import rudhira.util.ActivityStatus;
import rudhira.util.ConfigXMLUtil;
import rudhira.util.DBConnection;
import rudhira.util.QueryMaker;

/**
 * 
 * @author Kovid Bioanalytics
 * 
 */
public class BillingDAOImpl extends DBConnection implements BillingDAOInf {

	ConfigXMLUtil xmlUtil = null;

	Connection connection = null;
	PreparedStatement preparedStatement = null;
	ResultSet resultSet = null;

	Connection connection1 = null;
	PreparedStatement preparedStatement1 = null;
	ResultSet resultSet1 = null;

	PreparedStatement preparedStatement2 = null;
	ResultSet resultSet2 = null;

	PreparedStatement preparedStatement3 = null;
	ResultSet resultSet3 = null;

	String message = "error";

	public List<String> retrieveComponentList(int bloodBankID) {

		List<String> componentList = new ArrayList<String>();

		try {

			connection = getConnection();

			String retrieveComponentListQuery = QueryMaker.RETRIEVE_COMPONENT_LIST;

			preparedStatement = connection.prepareStatement(retrieveComponentListQuery);

			preparedStatement.setInt(1, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				String component = resultSet.getString("component");
				String category = resultSet.getString("category");

				String finalString = component + "-" + category;

				componentList.add(finalString);
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return componentList;
	}

	public List<String> retrieveConcessionList(int bloodBankID) {

		List<String> concessionList = new ArrayList<String>();

		try {

			connection = getConnection();

			String retrieveConcessionListQuery = QueryMaker.RETRIEVE_CONCESSION_LIST;

			preparedStatement = connection.prepareStatement(retrieveConcessionListQuery);

			preparedStatement.setInt(1, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				String concessionType = resultSet.getString("concessionType");

				concessionList.add(concessionType);
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return concessionList;
	}

	public List<String> retrieveOtherChargesList(int bloodBankID) {

		List<String> otherChargesList = new ArrayList<String>();

		try {

			connection = getConnection();

			String retrieveOtherChargesListQuery = QueryMaker.RETRIEVE_OTHER_CHARGES_LIST;

			preparedStatement = connection.prepareStatement(retrieveOtherChargesListQuery);

			preparedStatement.setInt(1, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				String chargeType = resultSet.getString("chargeType");

				otherChargesList.add(chargeType);
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return otherChargesList;
	}

	public JSONObject retrieveRateByComponent(String component, int bloodBankID) {

		JSONObject values = null;

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		try {
			connection = getConnection();

			String retrieveRateByComponentQuery = QueryMaker.RETRIEVE_RATE_BASED_ON_COMPONENT;

			preparedStatement = connection.prepareStatement(retrieveRateByComponentQuery);

			/*
			 * Spitting component on '-' in order to get separate component, and category
			 * values
			 */
			String[] componenetArr = component.split("-");

			if (componenetArr.length > 1) {

				preparedStatement.setString(1, componenetArr[0]);
				preparedStatement.setString(2, componenetArr[1]);
				preparedStatement.setInt(3, bloodBankID);

			} else {

				preparedStatement.setString(1, componenetArr[0]);
				preparedStatement.setString(2, "");
				preparedStatement.setInt(3, bloodBankID);

			}

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				object.put("componentRate", resultSet.getDouble("price"));

				array.add(object);

				values.put("Release", array);

			}

			resultSet.close();

			preparedStatement.close();

			return values;

		} catch (Exception exception) {
			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while retrieving price based on component");

			array.add(object);

			values.put("Release", array);

			return values;
		}
	}

	public JSONObject retrieveRateByConcession(String concession, int bloodBankID) {

		JSONObject values = null;

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		try {
			connection = getConnection();

			String retrieveRateByConcessionQuery = QueryMaker.RETRIEVE_RATE_BASED_ON_CONCESSION;

			preparedStatement = connection.prepareStatement(retrieveRateByConcessionQuery);

			preparedStatement.setString(1, "%" + concession + "%");
			preparedStatement.setInt(2, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				object.put("concessionRate", resultSet.getDouble("concessionAmt"));

				array.add(object);

				values.put("Release", array);

			}

			resultSet.close();

			preparedStatement.close();

			return values;

		} catch (Exception exception) {
			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while retrieving price based on concession");

			array.add(object);

			values.put("Release", array);

			return values;
		}
	}

	public JSONObject retrieveRateByChargeType(String chargeType, int bloodBankID) {

		JSONObject values = null;

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		try {
			connection = getConnection();

			String retrieveRateByChargeTypeQuery = QueryMaker.RETRIEVE_RATE_BASED_ON_CHARGE_TYPE;

			preparedStatement = connection.prepareStatement(retrieveRateByChargeTypeQuery);

			preparedStatement.setString(1, chargeType);
			preparedStatement.setInt(2, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				object.put("chargeRate", resultSet.getDouble("charges"));

				array.add(object);

				values.put("Release", array);

			}

			resultSet.close();

			preparedStatement.close();

			return values;

		} catch (Exception exception) {
			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while retrieving price based on charge type");

			array.add(object);

			values.put("Release", array);

			return values;
		}
	}

	public double retrieveCrossMatchingRate() {

		double rate = 0D;

		try {

			connection = getConnection();

			String retrieveCrossMatchingRateQuery = QueryMaker.RETRIEVE_CROSS_MATCHING_RATE;

			preparedStatement = connection.prepareStatement(retrieveCrossMatchingRateQuery);

			preparedStatement.setString(1, "Gr Cross Match");

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				rate = resultSet.getDouble("charges");

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return rate;
	}

	public double retrieveABDScreeningRate() {

		double rate = 0D;

		try {

			connection = getConnection();

			String retrieveABDScreeningRateQuery = QueryMaker.RETRIEVE_ABD_SCREENING_RATE;

			preparedStatement = connection.prepareStatement(retrieveABDScreeningRateQuery);

			preparedStatement.setString(1, "ABD Screening");

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				rate = resultSet.getDouble("charges");

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return rate;
	}

	public int retrieveLastReceiptNo() {
		int receiptNo = 0;

		try {

			connection = getConnection();

			String retrieveLastReceiptNoQuery = QueryMaker.RETRIEVE_LAST_RECEIPT_NO;

			preparedStatement = connection.prepareStatement(retrieveLastReceiptNoQuery);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				receiptNo = Integer.parseInt(resultSet.getString("receiptNo"));
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return receiptNo;
	}

	public String insertPatientDetails(BillingForm billingForm) {

		try {

			connection = getConnection();

			String insertPatientDetailsQuery = QueryMaker.INSERT_PATIENT_DETAILS;

			preparedStatement = connection.prepareStatement(insertPatientDetailsQuery);

			preparedStatement.setString(1, billingForm.getPatientIDNo());
			preparedStatement.setString(2, billingForm.getFirstName());
			preparedStatement.setString(3, billingForm.getMiddleName());
			preparedStatement.setString(4, billingForm.getLastName());
			preparedStatement.setString(5, billingForm.getAddress());
			preparedStatement.setString(6, billingForm.getBarCode());
			preparedStatement.setString(7, billingForm.getHospital());
			preparedStatement.setString(8, billingForm.getBbStorageCenter());
			preparedStatement.setString(9, billingForm.getMobile());

			preparedStatement.execute();

			message = "success";

			System.out.println("Patient Details inserted successfully into Patient table.");

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}
		return message;
	}

	public int retrievePatientIDBasedOnIndentifierNo(String patientIDNo) {

		int patientID = 0;

		try {

			connection = getConnection();

			String retrievePatientIDBasedOnIndentifierNoQuery = QueryMaker.RETRIEVE_PATIENT_ID_BASED_ON_INDENTIFIER_NO;

			preparedStatement = connection.prepareStatement(retrievePatientIDBasedOnIndentifierNoQuery);

			preparedStatement.setString(1, patientIDNo);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				patientID = resultSet.getInt("id");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return patientID;
	}

	public String insertReceiptDetails(BillingForm billingForm, int userID, int patientID) {

		/*
		 * Converting outstanding paid date into YYYY-MM-DD format in order insert the
		 * record into database
		 */
		Date date = new Date();

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

		SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd");

		try {

			connection = getConnection();

			String insertReceiptDetailsQuery = QueryMaker.INSERT_RECEIPT_DETAILS;

			/*
			 * Checking whether outstanding date is null or not, if null, insert null value
			 * else parse the datee first and then insert
			 */
			if (billingForm.getOutstandingPaidDate() == null || billingForm.getOutstandingPaidDate() == "") {

				preparedStatement = connection.prepareStatement(insertReceiptDetailsQuery);

				preparedStatement.setString(1, billingForm.getReceiptNo());
				preparedStatement.setDouble(2, billingForm.getTotalAmount());
				preparedStatement.setString(3, billingForm.getConcessionType());
				preparedStatement.setDouble(4, billingForm.getConcessionAmount());
				preparedStatement.setDouble(5, billingForm.getNetReceivableAmt());
				preparedStatement.setDouble(6, billingForm.getActualPayment());
				preparedStatement.setDouble(7, billingForm.getOustandingAmt());
				preparedStatement.setInt(8, userID);
				preparedStatement.setString(9, billingForm.getRefReceiptNo());
				preparedStatement.setString(10, null);
				preparedStatement.setDouble(11, billingForm.getOutstandingPaidAmt());
				preparedStatement.setInt(12, patientID);

				if (billingForm.getBbStorageCenter().equals("000")) {

					preparedStatement.setString(13, billingForm.getPatientIDNo());

				} else {

					preparedStatement.setString(13, billingForm.getStorageBBRNo());

				}

				preparedStatement.setString(14, billingForm.getReceiptType());
				preparedStatement.setString(15, billingForm.getPaymentType());
				preparedStatement.setDouble(16, billingForm.getTdsAmt());
				preparedStatement.setString(17, billingForm.getCharityCaseNo());
				preparedStatement.setString(18, billingForm.getManualReceiptNo());

				preparedStatement.execute();

			} else if (billingForm.getOutstandingPaidDate().isEmpty()) {

				preparedStatement = connection.prepareStatement(insertReceiptDetailsQuery);

				preparedStatement.setString(1, billingForm.getReceiptNo());
				preparedStatement.setDouble(2, billingForm.getTotalAmount());
				preparedStatement.setString(3, billingForm.getConcessionType());
				preparedStatement.setDouble(4, billingForm.getConcessionAmount());
				preparedStatement.setDouble(5, billingForm.getNetReceivableAmt());
				preparedStatement.setDouble(6, billingForm.getActualPayment());
				preparedStatement.setDouble(7, billingForm.getOustandingAmt());
				preparedStatement.setInt(8, userID);
				preparedStatement.setString(9, billingForm.getRefReceiptNo());
				preparedStatement.setString(10, null);
				preparedStatement.setDouble(11, billingForm.getOutstandingPaidAmt());
				preparedStatement.setInt(12, patientID);

				if (billingForm.getBbStorageCenter().equals("000")) {

					preparedStatement.setString(13, billingForm.getBBRNo());

				} else {

					preparedStatement.setString(13, billingForm.getStorageBBRNo());

				}

				preparedStatement.setString(14, billingForm.getReceiptType());
				preparedStatement.setString(15, billingForm.getPaymentType());
				preparedStatement.setDouble(16, billingForm.getTdsAmt());
				preparedStatement.setString(17, billingForm.getCharityCaseNo());
				preparedStatement.setString(18, billingForm.getManualReceiptNo());

				preparedStatement.execute();

			} else {

				// Converting date
				date = dateFormat.parse(billingForm.getOutstandingPaidDate());

				preparedStatement = connection.prepareStatement(insertReceiptDetailsQuery);

				preparedStatement.setString(1, billingForm.getReceiptNo());
				preparedStatement.setDouble(2, billingForm.getTotalAmount());
				preparedStatement.setString(3, billingForm.getConcessionType());
				preparedStatement.setDouble(4, billingForm.getConcessionAmount());
				preparedStatement.setDouble(5, billingForm.getNetReceivableAmt());
				preparedStatement.setDouble(6, billingForm.getActualPayment());
				preparedStatement.setDouble(7, billingForm.getOustandingAmt());
				preparedStatement.setInt(8, userID);
				preparedStatement.setString(9, billingForm.getRefReceiptNo());
				preparedStatement.setString(10, dbDateFormat.format(date));
				preparedStatement.setDouble(11, billingForm.getOutstandingPaidAmt());
				preparedStatement.setInt(12, patientID);

				if (billingForm.getBbStorageCenter().equals("000")) {

					preparedStatement.setString(13, billingForm.getBBRNo());

				} else {

					preparedStatement.setString(13, billingForm.getStorageBBRNo());

				}

				preparedStatement.setString(14, billingForm.getReceiptType());
				preparedStatement.setString(15, billingForm.getPaymentType());
				preparedStatement.setDouble(16, billingForm.getTdsAmt());
				preparedStatement.setString(17, billingForm.getCharityCaseNo());
				preparedStatement.setString(18, billingForm.getManualReceiptNo());

				preparedStatement.execute();

			}

			message = "success";

			System.out.println("Receipt Details inserted successfully into Receipt table.");

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}
		return message;
	}

	public int retrieveReceiptIDBasedOnReceiptNo(String receiptNo) {

		int receiptID = 0;

		try {

			connection = getConnection();

			String retrieveReceiptIDBasedOnReceiptNoQuery = QueryMaker.RETRIEVE_RECEIPT_ID_BASED_ON_RECEIPT_NO;

			preparedStatement = connection.prepareStatement(retrieveReceiptIDBasedOnReceiptNoQuery);

			preparedStatement.setString(1, receiptNo);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				receiptID = resultSet.getInt("id");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return receiptID;
	}

	public String insertReceiptItems(int quantity, String product, double rate, double amount, int receiptID) {

		try {

			connection = getConnection();

			String insertReceiptItemsQuery = QueryMaker.INSERT_RECEIPT_ITEMS;

			preparedStatement = connection.prepareStatement(insertReceiptItemsQuery);

			preparedStatement.setString(1, product);
			preparedStatement.setInt(2, quantity);
			preparedStatement.setDouble(3, rate);
			preparedStatement.setInt(4, receiptID);
			preparedStatement.setDouble(5, amount);

			preparedStatement.execute();

			message = "success";

			System.out.println("Receipt items inserted successfully into ReceiptItems table.");

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}
		return message;
	}

	public int retrieveLastEneteredReceiptItemID() {

		int receiptItemID = 0;

		try {

			connection = getConnection();

			String retrieveLastEneteredReceiptItemIDQuery = QueryMaker.RETRIEVE_LAST_ENTERED_RECEIPT_ITEM_ID;

			preparedStatement = connection.prepareStatement(retrieveLastEneteredReceiptItemIDQuery);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				receiptItemID = resultSet.getInt("id");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return receiptItemID;
	}

	public String insertBagDetails(int receiptItemID, String bagNo) {

		try {

			connection = getConnection();

			String insertBagDetailsQuery = QueryMaker.INSERT_BAG_DETAILS;

			preparedStatement = connection.prepareStatement(insertBagDetailsQuery);

			// Splitting bagNo by $ in order to get bagNo and bloodGroup
			// separately
			String[] bagNoArray = bagNo.split("\\$");

			preparedStatement.setInt(1, receiptItemID);
			preparedStatement.setString(2, bagNoArray[0]);
			preparedStatement.setString(3, bagNoArray[1]);

			preparedStatement.execute();

			message = "success";

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}
		return message;
	}

	public String retrieveReceiptByName(int receiptBy) {
		String receiptByName = null;

		try {

			connection = getConnection();

			String retrieveReceiptByNameQuery = QueryMaker.RETRIEVE_RECEIPT_BY_NAME;

			preparedStatement = connection.prepareStatement(retrieveReceiptByNameQuery);

			preparedStatement.setInt(1, receiptBy);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				if (resultSet.getString("middleName") == null || resultSet.getString("middleName") == "") {

					receiptByName = resultSet.getString("firstName") + " " + resultSet.getString("lastName");

				} else {

					receiptByName = resultSet.getString("firstName") + " " + resultSet.getString("middleName") + " "
							+ resultSet.getString("lastName");

				}

			}

			resultSet.close();
			preparedStatement.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return receiptByName;
	}

	public JSONObject retrievePatientDetailsByBDRNo(int bdrNo) {

		JSONObject values = null;

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		int check = 0;

		try {
			connection = getJankalyanConnection();

			String retrievePatientDetailsByBDRNoQuery = QueryMaker.RETRIEVE_PATIENT_DETILAILS_BASED_ON_BDR_NO;

			preparedStatement = connection.prepareStatement(retrievePatientDetailsByBDRNoQuery);

			preparedStatement.setInt(1, bdrNo);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				object.put("patientCode", resultSet.getInt("patientcode"));
				object.put("firstName", resultSet.getString("fname"));
				object.put("middleName", resultSet.getString("mname"));
				object.put("lastName", resultSet.getString("surname"));
				object.put("hospital", resultSet.getString("hname"));
				object.put("mobile", resultSet.getString("receipentcontactno"));

				String add2 = null;
				String add3 = null;

				if (resultSet.getString("add2") == null || resultSet.getString("add2") == "") {

					add2 = "";

				} else {

					add2 = resultSet.getString("add2");

				}

				if (resultSet.getString("add3") == null || resultSet.getString("add3") == "") {

					add3 = "";

				} else {

					add3 = resultSet.getString("add3");

				}

				String address = resultSet.getString("add1") + " " + add2 + " " + add3;

				check = 1;

				object.put("address", address);
				object.put("check", check);

				array.add(object);

			}
			values.put("Release", array);

			resultSet.close();

			preparedStatement.close();

			return values;

		} catch (Exception exception) {
			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while retrieving patient details based bdrNo no.");

			array.add(object);

			values.put("Release", array);

			return values;
		}
	}

	public String retrieveHospitalName(int hospitalID) {
		String hospitalName = "";

		try {

			connection1 = getJankalyanConnection();

			String retrieveHospitalNameQuery = QueryMaker.RETRIEVE_HOSPITAL_NAME;

			preparedStatement1 = connection.prepareStatement(retrieveHospitalNameQuery);

			preparedStatement1.setInt(1, hospitalID);

			resultSet1 = preparedStatement1.executeQuery();

			while (resultSet1.next()) {
				hospitalName = resultSet1.getString("hname");
			}

			resultSet1.close();
			preparedStatement1.close();
			connection1.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return hospitalName;
	}

	public int retrievePatientIDOnVerifyBDRNo(String bdrNo) {

		int patientID = 0;

		try {

			connection = getConnection();

			String retrievePatientIDOnVerifyBDRNoQuery = QueryMaker.RETRIEVE_PATIENT_ID_ON_VERIFY_BDR_NO;

			preparedStatement = connection.prepareStatement(retrievePatientIDOnVerifyBDRNoQuery);

			preparedStatement.setString(1, bdrNo);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				patientID = resultSet.getInt("patientID");

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return patientID;
	}

	public int retrievePatientIDOnVerifyPatientCode(String patientIDNo) {

		int patientID = 0;

		try {

			connection = getConnection();

			String retrievePatientIDOnVerifyPatientCodeQuery = QueryMaker.RETRIEVE_PATIENT_ID_ON_VERIFY_PATIENT_ID_NO;

			preparedStatement = connection.prepareStatement(retrievePatientIDOnVerifyPatientCodeQuery);

			preparedStatement.setString(1, patientIDNo);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				patientID = resultSet.getInt("id");

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return patientID;
	}

	public String insertOtherCharge(String chargeType, double otherCharge, int receiptID) {

		try {

			connection = getConnection();

			String insertOtherChargeQuery = QueryMaker.INSERT_OTHER_CHARGES;

			preparedStatement = connection.prepareStatement(insertOtherChargeQuery);

			preparedStatement.setString(1, chargeType);
			preparedStatement.setDouble(2, otherCharge);
			preparedStatement.setInt(3, receiptID);

			preparedStatement.execute();

			message = "success";

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}
		return message;
	}

	public JSONObject retrieveReceiptNoByReceiptType(String receiptType, int bloodBankID) {

		JSONObject values = null;

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		String receiptNo = "";

		Date date = new Date();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy");

		String currentYear = dateFormat.format(date);

		Calendar calendar = new GregorianCalendar();

		try {
			connection = getConnection();

			String retrieveReceiptNoByReceiptTypeQuery = QueryMaker.RETRIEVE_RECEIPT_NO_BY_RECEIPT_TYPE;

			preparedStatement = connection.prepareStatement(retrieveReceiptNoByReceiptTypeQuery);

			if (calendar.get(Calendar.MONTH) >= 3) {

				preparedStatement.setString(1, receiptType + "-" + currentYear + "%");
				preparedStatement.setInt(2, bloodBankID);

				resultSet = preparedStatement.executeQuery();

				while (resultSet.next()) {

					receiptNo = resultSet.getString("receiptNo");

					String receiptYear = dateFormat.format(resultSet.getTimestamp("receiptDate"));

					/*
					 * Splitting receiptNo by '-' in order to increase the receiptNo by 1
					 */
					String[] receiptNoArray = receiptNo.split("-");

					int number = Integer.parseInt(receiptNoArray[2]) + 1;

					receiptNo = receiptType + "-" + currentYear + "-" + number;

				}

				/*
				 * Checking whether receipt no is null, if so then add 1 after the prefix
				 */
				if (receiptNo == "" || receiptNo == null) {

					receiptNo = receiptType + "-" + currentYear + "-1";

				}

			} else {

				int number = 0;

				String prevYear = String.valueOf(Integer.parseInt(currentYear) - 1);

				preparedStatement.setString(1, receiptType + "-" + prevYear + "%");
				preparedStatement.setInt(2, bloodBankID);

				resultSet = preparedStatement.executeQuery();

				while (resultSet.next()) {

					receiptNo = resultSet.getString("receiptNo");

					String receiptYear = dateFormat.format(resultSet.getTimestamp("receiptDate"));

					/*
					 * Splitting receiptNo by '-' in order to increase the receiptNo by 1
					 */
					String[] receiptNoArray = receiptNo.split("-");

					number = Integer.parseInt(receiptNoArray[2]) + 1;

				}

				/*
				 * Checking whether receipt no is null, if so then add 1 after the prefix
				 */
				if (receiptNo == "" || receiptNo == null) {

					currentYear = String.valueOf(Integer.parseInt(currentYear) - 1);
					// Appending new number to the prefix to create new
					// receiptNo
					receiptNo = receiptType + "-" + currentYear + "-1";
				} else {

					currentYear = String.valueOf(Integer.parseInt(currentYear) - 1);

					receiptNo = receiptType + "-" + currentYear + "-" + number;

				}

			}

			object.put("receiptNo", receiptNo);

			array.add(object);

			values.put("Release", array);

			resultSet.close();

			preparedStatement.close();

			return values;

		} catch (

		Exception exception) {
			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while retrieving receipt no based on receipt type.");

			array.add(object);

			values.put("Release", array);

			return values;
		}
	}

	public String retrieveReceiptNoForCourier(String receiptType, int bloodBankID) {

		String receiptNo = "";

		try {

			connection = getConnection();

			String retrieveReceiptNoForCourierQuery = QueryMaker.RETRIEVE_RECEIPT_NO_BY_RECEIPT_TYPE;

			preparedStatement = connection.prepareStatement(retrieveReceiptNoForCourierQuery);

			preparedStatement.setString(1, "%" + receiptType + "%");
			preparedStatement.setInt(2, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				receiptNo = resultSet.getString("receiptNo");

				/*
				 * Splitting receiptNo by '-' in order to increase the receiptNo by 1
				 */
				String[] receiptNoArray = receiptNo.split("-");

				int number = Integer.parseInt(receiptNoArray[1]) + 1;

				// Appending new number to the prefix to create new receiptNo
				receiptNo = receiptType + "-" + number;

			}

			/*
			 * Checking whether receipt no is null, if so then add 1 after the prefix
			 */
			if (receiptNo == "" || receiptNo == null) {

				receiptNo = receiptType + "-1";

			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return receiptNo;
	}

	public String insertChequeDetails(BillingForm billingForm) {

		/*
		 * Converting cheque date into YYYY-MM-DD format in order insert the record into
		 * database
		 */
		Date date = new Date();

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

		SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd");

		try {

			connection = getConnection();

			String insertChequeDetailsQuery = QueryMaker.INSERT_CHEQUE_DETAILS;

			preparedStatement = connection.prepareStatement(insertChequeDetailsQuery);

			if (billingForm.getChequeDate() == null || billingForm.getChequeDate() == ""
					|| billingForm.getChequeDate().isEmpty()) {

				preparedStatement.setInt(1, billingForm.getReceiptID());
				preparedStatement.setString(2, billingForm.getChequeIssuedBy());
				preparedStatement.setString(3, billingForm.getChequeNo());
				preparedStatement.setString(4, billingForm.getChequeBankName());
				preparedStatement.setString(5, billingForm.getChequeBankBranch());
				preparedStatement.setString(6, null);
				preparedStatement.setDouble(7, billingForm.getChequeAmt());
				preparedStatement.setString(8, billingForm.getCardMobileNo());

				preparedStatement.execute();

			} else {

				date = dateFormat.parse(billingForm.getChequeDate());

				preparedStatement.setInt(1, billingForm.getReceiptID());
				preparedStatement.setString(2, billingForm.getChequeIssuedBy());
				preparedStatement.setString(3, billingForm.getChequeNo());
				preparedStatement.setString(4, billingForm.getChequeBankName());
				preparedStatement.setString(5, billingForm.getChequeBankBranch());
				preparedStatement.setString(6, dbDateFormat.format(date));
				preparedStatement.setDouble(7, billingForm.getChequeAmt());
				preparedStatement.setString(8, billingForm.getCardMobileNo());

				preparedStatement.execute();

			}

			message = "success";

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}
		return message;
	}

	public String retrieveBagNoByReceiptItemID(int receiptItemID) {

		Connection connection7 = null;
		PreparedStatement preparedStatement7 = null;
		ResultSet resultSet7 = null;

		int check = 1;

		String bagNo = "";

		try {

			connection7 = getConnection();

			String retrieveBagNoByReceiptItemIDQuery = QueryMaker.RETRIEVE_BAG_NO_BY_RECEIPT_ITEM_ID;

			preparedStatement7 = connection7.prepareStatement(retrieveBagNoByReceiptItemIDQuery);

			preparedStatement7.setInt(1, receiptItemID);

			resultSet7 = preparedStatement7.executeQuery();

			while (resultSet7.next()) {

				if (check != 1) {
					bagNo = bagNo + "," + resultSet7.getString("bagNo");
				} else {
					bagNo = resultSet7.getString("bagNo");
					check++;
				}

			}

			resultSet7.close();
			preparedStatement7.close();

			connection7.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return bagNo;
	}

	public List<BillingForm> searchBillByPatientName(String searchPatientName, String startDate, String endDate) {

		List<BillingForm> list = new ArrayList<BillingForm>();

		BillingForm billingForm = null;

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");

		SimpleDateFormat dateToBeFormatted = new SimpleDateFormat("dd-MM-yyyy");

		SimpleDateFormat dateToBeFormatted1 = new SimpleDateFormat("yyyy-MM-dd");

		try {

			connection = getConnection();

			if (startDate.isEmpty() || endDate.isEmpty()) {

				String searchBillByPatientNameQuery = QueryMaker.SEARCH_BILL_BY_PATIENT_NAME_OR_RECEIPT_TYPE;

				preparedStatement = connection.prepareStatement(searchBillByPatientNameQuery);

				if (searchPatientName.contains(" ")) {
					searchPatientName = searchPatientName.replace(" ", "%");
				}

				preparedStatement.setString(1, "%" + searchPatientName + "%");
				preparedStatement.setString(2, searchPatientName);

				resultSet = preparedStatement.executeQuery();

				while (resultSet.next()) {

					billingForm = new BillingForm();

					billingForm.setPatientID(resultSet.getInt("patientID"));
					billingForm.setReceiptID(resultSet.getInt("id"));
					billingForm.setReceiptNo(resultSet.getString("receiptNo"));
					billingForm.setReceiptType(resultSet.getString("receiptType"));
					billingForm.setReceiptDate(
							dateToBeFormatted.format(dateFormat.parse(resultSet.getString("receiptDate"))));
					billingForm
							.setPatientName(resultSet.getString("firstName") + " " + resultSet.getString("lastName"));

					list.add(billingForm);

				}

			} else {

				String searchBillByPatientNameQuery = QueryMaker.SEARCH_BILL_BY_PATIENT_NAME_OR_RECEIPT_TYPE_BY_DATE;

				preparedStatement = connection.prepareStatement(searchBillByPatientNameQuery);

				if (searchPatientName.contains(" ")) {
					searchPatientName = searchPatientName.replace(" ", "%");
				}

				preparedStatement.setString(1, "%" + searchPatientName + "%");
				preparedStatement.setString(2,
						dateToBeFormatted1.format(dateToBeFormatted.parse(startDate)) + " 00:00");
				preparedStatement.setString(3, dateToBeFormatted1.format(dateToBeFormatted.parse(endDate)) + " 23:59");
				preparedStatement.setString(4, searchPatientName);
				preparedStatement.setString(5,
						dateToBeFormatted1.format(dateToBeFormatted.parse(startDate)) + " 00:00");
				preparedStatement.setString(6, dateToBeFormatted1.format(dateToBeFormatted.parse(endDate)) + " 23:59");

				resultSet = preparedStatement.executeQuery();

				while (resultSet.next()) {

					billingForm = new BillingForm();

					billingForm.setPatientID(resultSet.getInt("patientID"));
					billingForm.setReceiptID(resultSet.getInt("id"));
					billingForm.setReceiptNo(resultSet.getString("receiptNo"));
					billingForm.setReceiptType(resultSet.getString("receiptType"));
					billingForm.setReceiptDate(
							dateToBeFormatted.format(dateFormat.parse(resultSet.getString("receiptDate"))));
					billingForm
							.setPatientName(resultSet.getString("firstName") + " " + resultSet.getString("lastName"));

					list.add(billingForm);

				}

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return list;
	}

	public List<BillingForm> retrieveAllReceiptDetails(String startDate, String endDate) {

		List<BillingForm> list = new ArrayList<BillingForm>();

		BillingForm billingForm = null;

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");

		SimpleDateFormat dateToBeFormatted = new SimpleDateFormat("dd-MM-yyyy");

		SimpleDateFormat dateToBeFormatted1 = new SimpleDateFormat("yyyy-MM-dd");

		try {

			connection = getConnection();

			if (startDate.isEmpty() || endDate.isEmpty()) {

				String retrieveAllReceiptDetailsQuery = QueryMaker.RETRIEVE_ALL_RECEIPT_DETAILS;

				preparedStatement = connection.prepareStatement(retrieveAllReceiptDetailsQuery);

				resultSet = preparedStatement.executeQuery();

				while (resultSet.next()) {

					billingForm = new BillingForm();

					billingForm.setPatientID(resultSet.getInt("patientID"));
					billingForm.setReceiptID(resultSet.getInt("id"));
					billingForm.setReceiptNo(resultSet.getString("receiptNo"));
					billingForm.setReceiptType(resultSet.getString("receiptType"));
					billingForm.setReceiptDate(
							dateToBeFormatted.format(dateFormat.parse(resultSet.getString("receiptDate"))));
					billingForm
							.setPatientName(resultSet.getString("firstName") + " " + resultSet.getString("lastName"));

					list.add(billingForm);

				}

			} else {

				String retrieveAllReceiptDetailsByDateQuery = QueryMaker.RETRIEVE_ALL_RECEIPT_DETAILS_BY_DATE;

				preparedStatement = connection.prepareStatement(retrieveAllReceiptDetailsByDateQuery);

				preparedStatement.setString(1,
						dateToBeFormatted1.format(dateToBeFormatted.parse(startDate)) + " 00:00");
				preparedStatement.setString(2, dateToBeFormatted1.format(dateToBeFormatted.parse(endDate)) + " 23:59");

				resultSet = preparedStatement.executeQuery();

				while (resultSet.next()) {

					billingForm = new BillingForm();

					billingForm.setPatientID(resultSet.getInt("patientID"));
					billingForm.setReceiptID(resultSet.getInt("id"));
					billingForm.setReceiptNo(resultSet.getString("receiptNo"));
					billingForm.setReceiptType(resultSet.getString("receiptType"));
					billingForm.setReceiptDate(
							dateToBeFormatted.format(dateFormat.parse(resultSet.getString("receiptDate"))));
					billingForm
							.setPatientName(resultSet.getString("firstName") + " " + resultSet.getString("lastName"));

					list.add(billingForm);

				}

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return list;
	}

	public List<BillingForm> retrieveReceipt(int receiptID, int patientID) {

		List<BillingForm> list = new ArrayList<BillingForm>();

		BillingForm billingForm = new BillingForm();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");

		SimpleDateFormat dateToBeFormatted = new SimpleDateFormat("dd-MM-yyyy HH:mm:SS");

		SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");

		SimpleDateFormat dateToBeFormatted1 = new SimpleDateFormat("dd-MM-yyyy");

		try {

			connection = getConnection();

			String retrieveReceiptQuery = QueryMaker.RETRIEVE_RECEIPT_BY_ID;
			String retrieveChequDetails = QueryMaker.RETRIEVE_CHEQUE_DETAILS;
			String retrievePatientDetails = QueryMaker.RETRIEVE_PATIENT_DETAILS;
			String retrieveTotalOtherChargeAmtQuery = QueryMaker.RETRIEVE_TOTAL_OTHER_CHARGE;

			/*
			 * Retrieving patient Details
			 */
			preparedStatement = connection.prepareStatement(retrievePatientDetails);

			preparedStatement.setInt(1, patientID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				billingForm.setPatientID(resultSet.getInt("id"));
				billingForm.setFirstName(resultSet.getString("firstName"));
				billingForm.setMiddleName(resultSet.getString("middleName"));
				billingForm.setLastName(resultSet.getString("lastName"));
				billingForm.setPatientIDNo(resultSet.getString("patientIdentifier"));
				billingForm.setAddress(resultSet.getString("address"));
				billingForm.setBarCode(resultSet.getString("barcode"));
				billingForm.setHospital(resultSet.getString("hospital"));
				billingForm.setBbStorageCenter(resultSet.getString("bloodBankStorage"));
				billingForm.setMobile(resultSet.getString("mobile"));

			}

			/*
			 * Retrieving receipt details
			 */
			preparedStatement1 = connection.prepareStatement(retrieveReceiptQuery);

			preparedStatement1.setInt(1, receiptID);

			resultSet1 = preparedStatement1.executeQuery();

			while (resultSet1.next()) {

				billingForm.setReceiptID(resultSet1.getInt("id"));
				billingForm.setReceiptDate(
						dateToBeFormatted.format(dateFormat.parse(resultSet1.getString("receiptDate"))));
				billingForm.setReceiptType(resultSet1.getString("receiptType"));
				billingForm.setReceiptNo(resultSet1.getString("receiptNo"));
				billingForm.setBBRNo(resultSet1.getString("bdrNo"));
				billingForm.setTotalAmount(resultSet1.getDouble("totalAmt"));

				if (resultSet1.getString("concessionType").equals("000")) {
					billingForm.setConcessionType("");
				} else {
					billingForm.setConcessionType(resultSet1.getString("concessionType"));
				}
				billingForm.setConcessionAmount(resultSet1.getDouble("concessionAmt"));
				billingForm.setNetReceivableAmt(resultSet1.getDouble("netReceivableAmt"));
				billingForm.setActualPayment(resultSet1.getDouble("actualPayment"));
				billingForm.setOustandingAmt(resultSet1.getDouble("outstandingAmt"));
				billingForm.setReceiptByName(retrieveReceiptByName(resultSet1.getInt("receiptBy")));
				billingForm.setRefReceiptNo(resultSet1.getString("refReceiptNumber"));
				billingForm.setOutstandingPaidAmt(resultSet1.getDouble("outstandingPaidAmt"));
				if (resultSet1.getString("outstandingPaidDate") == null
						|| resultSet1.getString("outstandingPaidDate") == "") {

					billingForm.setOutstandingPaidDate("");

				} else {

					billingForm.setOutstandingPaidDate(
							dateToBeFormatted1.format(dateFormat1.parse(resultSet1.getString("outstandingPaidDate"))));

				}
				billingForm.setPaymentType(resultSet1.getString("paymentType"));
				billingForm.setTdsAmt(resultSet1.getDouble("tdsAmt"));
				billingForm.setCharityCaseNo(resultSet1.getString("charityCaseNo"));

			}

			/*
			 * Retrieving cheque details
			 */
			preparedStatement2 = connection.prepareStatement(retrieveChequDetails);

			preparedStatement2.setInt(1, receiptID);

			resultSet2 = preparedStatement2.executeQuery();

			while (resultSet2.next()) {

				billingForm.setChequeIssuedBy(resultSet2.getString("chequeIssuedBy"));
				billingForm.setChequeNo(resultSet2.getString("chequeNumber"));
				billingForm.setChequeBankBranch(resultSet2.getString("bankBranch"));
				billingForm.setChequeBankName(resultSet2.getString("bankName"));

				if (resultSet2.getString("chequeDate") == null || resultSet2.getString("chequeDate") == "") {

					billingForm.setChequeDate("");

				} else {

					billingForm.setChequeDate(
							dateToBeFormatted.format(dateFormat1.parse(resultSet2.getString("chequeDate"))));

				}

				billingForm.setChequeAmt(resultSet2.getDouble("chequeAmt"));

			}

			/*
			 * Retrieving total other charge amount details
			 */
			preparedStatement3 = connection.prepareStatement(retrieveTotalOtherChargeAmtQuery);

			preparedStatement3.setInt(1, receiptID);

			resultSet3 = preparedStatement3.executeQuery();

			while (resultSet3.next()) {

				billingForm.setTotalOtherCharge(resultSet3.getDouble("sum"));

			}

			list.add(billingForm);

			resultSet3.close();
			preparedStatement3.close();

			resultSet2.close();
			preparedStatement2.close();

			resultSet1.close();
			preparedStatement1.close();

			resultSet.close();
			preparedStatement.close();

			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return list;
	}

	public List<BillingForm> retrieveComponent(int receiptID) {

		List<BillingForm> list = new ArrayList<BillingForm>();

		BillingForm billingForm = null;

		int srNo = 1;

		try {

			connection = getConnection();

			String retrieveComponentQuery = QueryMaker.RETRIEVE_COMPONENT_BY_RECEIPT_ID;

			preparedStatement = connection.prepareStatement(retrieveComponentQuery);

			preparedStatement.setInt(1, receiptID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				billingForm = new BillingForm();

				billingForm.setComponentName(resultSet.getString("product"));
				billingForm.setQuantityName(resultSet.getInt("quantity"));
				billingForm.setRateName(resultSet.getDouble("rate"));
				billingForm.setAmountName(resultSet.getDouble("amount"));
				billingForm.setComponentID(resultSet.getInt("id"));
				billingForm.setSrNo(srNo);

				srNo++;

				list.add(billingForm);

			}

			resultSet.close();
			preparedStatement.close();

			// connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return list;
	}

	public List<BillingForm> retrieveComponentForAdminUser(int receiptID) {

		List<BillingForm> list = new ArrayList<BillingForm>();

		BillingForm billingForm = null;

		int srNo = 1;

		try {

			connection = getConnection();

			String retrieveComponentQuery = QueryMaker.RETRIEVE_COMPONENT_BY_RECEIPT_ID;

			preparedStatement = connection.prepareStatement(retrieveComponentQuery);

			preparedStatement.setInt(1, receiptID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				billingForm = new BillingForm();

				if (resultSet.getString("product").equals("Gr Cross Match")
						|| resultSet.getString("product").equals("ABD Screening")) {
					continue;
				} else {

					billingForm.setComponentName(resultSet.getString("product"));
					billingForm.setQuantityName(resultSet.getInt("quantity"));
					billingForm.setRateName(resultSet.getDouble("rate"));
					billingForm.setAmountName(resultSet.getDouble("amount"));
					billingForm.setComponentID(resultSet.getInt("id"));
					billingForm.setSrNo(srNo);

					list.add(billingForm);

				}

			}

			resultSet.close();
			preparedStatement.close();

			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return list;
	}

	public List<BillingForm> retrieveOtherCharges(int receiptID) {

		List<BillingForm> list = new ArrayList<BillingForm>();

		BillingForm billingForm = null;

		try {

			connection = getConnection();

			String retrieveOtherChargesQuery = QueryMaker.RETRIEVE_OTHER_CHARGES_BY_RECEIPT_ID;

			preparedStatement = connection.prepareStatement(retrieveOtherChargesQuery);

			preparedStatement.setInt(1, receiptID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				billingForm = new BillingForm();

				billingForm.setOtherChargeID(resultSet.getInt("id"));
				billingForm.setChargeTypeName(resultSet.getString("chargeType"));
				billingForm.setOtherChargeName(resultSet.getDouble("otherCharges"));

				list.add(billingForm);

			}

			resultSet.close();
			preparedStatement.close();

			// connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return list;
	}

	public String retrievePaymenyType(int receiptID) {

		String paymentType = "";

		try {

			connection = getConnection();

			String retrievePaymenyTypeQuery = QueryMaker.RETRIEVE_PAYMENT_TYPE;

			preparedStatement = connection.prepareStatement(retrievePaymenyTypeQuery);

			preparedStatement.setInt(1, receiptID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				paymentType = resultSet.getString("paymentType");
			}

			resultSet.close();
			preparedStatement.close();

			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return paymentType;
	}

	public int andScreeningAndCrsMatchingCheck(int receiptID) {

		int check = 0;

		int csrMtchCheck = 0;

		int abdScrCheck = 0;

		try {

			connection = getConnection();

			String andScreeningAndCrsMatchingCheckQuery = QueryMaker.ABD_CSR_CHECK_BY_RECEIPT_ID;

			preparedStatement = connection.prepareStatement(andScreeningAndCrsMatchingCheckQuery);

			preparedStatement.setInt(1, receiptID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				String product = resultSet.getString("product");

				// Checking whether product is abd screening or cross matching
				// or both and depending upon that setting value of check
				if (product.equals("Gr Cross Match")) {

					if (abdScrCheck == 1) {

						check = 3;

					} else {

						check = 1;

					}

					csrMtchCheck = 1;

				} else if (product.equals("ABD Screening")) {

					if (csrMtchCheck == 1) {

						check = 3;

					} else {

						check = 2;

					}

					abdScrCheck = 1;

				}
			}

			resultSet.close();
			preparedStatement.close();

			connection.close();

		} catch (

		Exception exception) {
			exception.printStackTrace();
		}
		return check;
	}

	public List<BillingForm> retrieveCrossMatchingList(int receiptID) {

		List<BillingForm> list = new ArrayList<BillingForm>();

		BillingForm billingForm = null;

		try {

			connection = getConnection();

			String retrieveCrossMatchingListQuery = QueryMaker.RETRIEVE_CRS_MTCH_LIST_BY_RECEIPT_ID;

			preparedStatement = connection.prepareStatement(retrieveCrossMatchingListQuery);

			preparedStatement.setInt(1, receiptID);
			preparedStatement.setString(2, "Gr Cross Match");

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				billingForm = new BillingForm();

				billingForm.setComponentName(resultSet.getString("product"));
				billingForm.setQuantityName(resultSet.getInt("quantity"));
				billingForm.setRateName(resultSet.getDouble("rate"));
				billingForm.setAmountName(resultSet.getDouble("amount"));
				// billingForm.setBagNoName(resultSet.getString("bagNo"));
				billingForm.setComponentID(resultSet.getInt("id"));

			}

			list.add(billingForm);

			resultSet.close();
			preparedStatement.close();

			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return list;
	}

	public List<BillingForm> retrieveABDScreeningList(int receiptID) {

		List<BillingForm> list = new ArrayList<BillingForm>();

		BillingForm billingForm = null;

		try {

			connection = getConnection();

			String retrieveABDScreeningListQuery = QueryMaker.RETRIEVE_CRS_MTCH_LIST_BY_RECEIPT_ID;

			preparedStatement = connection.prepareStatement(retrieveABDScreeningListQuery);

			preparedStatement.setInt(1, receiptID);
			preparedStatement.setString(2, "ABD Screening");

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				billingForm = new BillingForm();

				billingForm.setComponentName(resultSet.getString("product"));
				billingForm.setQuantityName(resultSet.getInt("quantity"));
				billingForm.setRateName(resultSet.getDouble("rate"));
				billingForm.setAmountName(resultSet.getDouble("amount"));
				// billingForm.setBagNoName(resultSet.getString("bagNo"));
				billingForm.setComponentID(resultSet.getInt("id"));

			}
			list.add(billingForm);

			resultSet.close();
			preparedStatement.close();

			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return list;
	}

	public String retrieveConcessionType(int receiptID) {

		String concessionType = "";

		try {

			connection = getConnection();

			String retrieveConcessionTypeQuery = QueryMaker.RETRIEVE_CONCESSION_TYPE;

			preparedStatement = connection.prepareStatement(retrieveConcessionTypeQuery);

			preparedStatement.setInt(1, receiptID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				concessionType = resultSet.getString("concessionType");
			}

			resultSet.close();
			preparedStatement.close();

			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return concessionType;
	}

	public String updateReceiptDetails(BillingForm billingForm, int userID) {

		/*
		 * Converting outstanding paid date into YYYY-MM-DD format in order insert the
		 * record into database
		 */
		Date date = new Date();

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

		SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd");

		SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

		SimpleDateFormat dbDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		try {

			connection = getConnection();

			String updateReceiptDetailsQuery = QueryMaker.UPDATE_RECEIPT_DETAILS;

			/*
			 * Checking whether outstanding date is null or not, if null, insert null value
			 * else parse the datee first and then insert
			 */
			if (billingForm.getOutstandingPaidDate() == null || billingForm.getOutstandingPaidDate() == "") {

				preparedStatement = connection.prepareStatement(updateReceiptDetailsQuery);

				preparedStatement.setString(1, billingForm.getReceiptNo());
				preparedStatement.setDouble(2, billingForm.getTotalAmount());
				preparedStatement.setString(3, billingForm.getConcessionType());
				preparedStatement.setDouble(4, billingForm.getConcessionAmount());
				preparedStatement.setDouble(5, billingForm.getNetReceivableAmt());
				preparedStatement.setDouble(6, billingForm.getActualPayment());
				preparedStatement.setDouble(7, billingForm.getOustandingAmt());
				preparedStatement.setInt(8, userID);
				preparedStatement.setString(9, billingForm.getRefReceiptNo());
				preparedStatement.setString(10, null);
				preparedStatement.setDouble(11, billingForm.getOutstandingPaidAmt());
				preparedStatement.setInt(12, billingForm.getPatientID());

				if (billingForm.getBbStorageCenter() == null || billingForm.getBbStorageCenter() == "") {

					preparedStatement.setString(13, billingForm.getPatientIDNo());

				} else if (billingForm.getBbStorageCenter().isEmpty()) {

					preparedStatement.setString(13, billingForm.getPatientIDNo());

				} else if (billingForm.getBbStorageCenter().equals("000")) {

					preparedStatement.setString(13, billingForm.getPatientIDNo());

				} else {

					preparedStatement.setString(13, billingForm.getStorageBBRNo());

				}

				preparedStatement.setString(14, billingForm.getReceiptType());
				preparedStatement.setString(15, billingForm.getPaymentType());
				preparedStatement.setDouble(16, billingForm.getTdsAmt());
				preparedStatement.setString(17, billingForm.getCharityCaseNo());
				preparedStatement.setString(18, dbDateFormat1.format(dateFormat1.parse(billingForm.getReceiptDate())));
				preparedStatement.setInt(19, billingForm.getReceiptID());

				preparedStatement.execute();

			} else if (billingForm.getOutstandingPaidDate().isEmpty()) {

				preparedStatement = connection.prepareStatement(updateReceiptDetailsQuery);

				preparedStatement.setString(1, billingForm.getReceiptNo());
				preparedStatement.setDouble(2, billingForm.getTotalAmount());
				preparedStatement.setString(3, billingForm.getConcessionType());
				preparedStatement.setDouble(4, billingForm.getConcessionAmount());
				preparedStatement.setDouble(5, billingForm.getNetReceivableAmt());
				preparedStatement.setDouble(6, billingForm.getActualPayment());
				preparedStatement.setDouble(7, billingForm.getOustandingAmt());
				preparedStatement.setInt(8, userID);
				preparedStatement.setString(9, billingForm.getRefReceiptNo());
				preparedStatement.setString(10, null);
				preparedStatement.setDouble(11, billingForm.getOutstandingPaidAmt());
				preparedStatement.setInt(12, billingForm.getPatientID());

				if (billingForm.getBbStorageCenter() == null || billingForm.getBbStorageCenter() == "") {

					preparedStatement.setString(13, billingForm.getBBRNo());

				} else if (billingForm.getBbStorageCenter().isEmpty()) {

					preparedStatement.setString(13, billingForm.getBBRNo());

				} else if (billingForm.getBbStorageCenter().equals("000")) {

					preparedStatement.setString(13, billingForm.getBBRNo());

				} else {

					preparedStatement.setString(13, billingForm.getStorageBBRNo());

				}

				preparedStatement.setString(14, billingForm.getReceiptType());
				preparedStatement.setString(15, billingForm.getPaymentType());
				preparedStatement.setDouble(16, billingForm.getTdsAmt());
				preparedStatement.setString(17, billingForm.getCharityCaseNo());
				preparedStatement.setString(18, dbDateFormat1.format(dateFormat1.parse(billingForm.getReceiptDate())));
				preparedStatement.setInt(19, billingForm.getReceiptID());

				preparedStatement.execute();

			} else {

				// Converting date
				date = dateFormat.parse(billingForm.getOutstandingPaidDate());

				preparedStatement = connection.prepareStatement(updateReceiptDetailsQuery);

				preparedStatement.setString(1, billingForm.getReceiptNo());
				preparedStatement.setDouble(2, billingForm.getTotalAmount());
				preparedStatement.setString(3, billingForm.getConcessionType());
				preparedStatement.setDouble(4, billingForm.getConcessionAmount());
				preparedStatement.setDouble(5, billingForm.getNetReceivableAmt());
				preparedStatement.setDouble(6, billingForm.getActualPayment());
				preparedStatement.setDouble(7, billingForm.getOustandingAmt());
				preparedStatement.setInt(8, userID);
				preparedStatement.setString(9, billingForm.getRefReceiptNo());
				preparedStatement.setString(10, dbDateFormat.format(date));
				preparedStatement.setDouble(11, billingForm.getOutstandingPaidAmt());
				preparedStatement.setInt(12, billingForm.getPatientID());

				if (billingForm.getBbStorageCenter() == null || billingForm.getBbStorageCenter() == "") {

					preparedStatement.setString(13, billingForm.getBBRNo());

				} else if (billingForm.getBbStorageCenter().isEmpty()) {

					preparedStatement.setString(13, billingForm.getBBRNo());

				} else if (billingForm.getBbStorageCenter().equals("000")) {

					preparedStatement.setString(13, billingForm.getBBRNo());

				} else {

					preparedStatement.setString(13, billingForm.getStorageBBRNo());

				}

				preparedStatement.setString(14, billingForm.getReceiptType());
				preparedStatement.setString(15, billingForm.getPaymentType());
				preparedStatement.setDouble(16, billingForm.getTdsAmt());
				preparedStatement.setString(17, billingForm.getCharityCaseNo());
				preparedStatement.setString(18, dbDateFormat1.format(dateFormat1.parse(billingForm.getReceiptDate())));
				preparedStatement.setInt(19, billingForm.getReceiptID());

				preparedStatement.execute();

			}

			message = "success";

			System.out.println("Receipt Details updated successfully into Receipt table.");

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}
		return message;
	}

	public boolean verifyChequeExists(int receiptID) {

		boolean check = false;

		try {

			connection = getConnection();

			String verifyChequeExistsQuery = QueryMaker.VERIFY_CHEQUE_EXIST;

			preparedStatement = connection.prepareStatement(verifyChequeExistsQuery);

			preparedStatement.setInt(1, receiptID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				check = true;
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			check = false;
		}
		return check;
	}

	public String updateChequeDetails(BillingForm billingForm) {

		/*
		 * Converting cheque date into YYYY-MM-DD format in order insert the record into
		 * database
		 */
		Date date = new Date();

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

		SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd");

		try {

			connection = getConnection();

			String updateChequeDetailsQuery = QueryMaker.UPDATE_CHEQUE_DETAILS;

			preparedStatement = connection.prepareStatement(updateChequeDetailsQuery);

			if (billingForm.getChequeDate() == null || billingForm.getChequeDate() == ""
					|| billingForm.getChequeDate().isEmpty()) {

				preparedStatement.setInt(1, billingForm.getReceiptID());
				preparedStatement.setString(2, billingForm.getChequeIssuedBy());
				preparedStatement.setString(3, billingForm.getChequeNo());
				preparedStatement.setString(4, billingForm.getChequeBankName());
				preparedStatement.setString(5, billingForm.getChequeBankBranch());
				preparedStatement.setString(6, null);
				preparedStatement.setDouble(7, billingForm.getChequeAmt());
				preparedStatement.setString(8, billingForm.getCardMobileNo());
				preparedStatement.setInt(9, billingForm.getReceiptID());

				preparedStatement.execute();

			} else {

				date = dateFormat.parse(billingForm.getChequeDate());

				preparedStatement.setInt(1, billingForm.getReceiptID());
				preparedStatement.setString(2, billingForm.getChequeIssuedBy());
				preparedStatement.setString(3, billingForm.getChequeNo());
				preparedStatement.setString(4, billingForm.getChequeBankName());
				preparedStatement.setString(5, billingForm.getChequeBankBranch());
				preparedStatement.setString(6, dbDateFormat.format(date));
				preparedStatement.setDouble(7, billingForm.getChequeAmt());
				preparedStatement.setString(8, billingForm.getCardMobileNo());
				preparedStatement.setInt(9, billingForm.getReceiptID());

				preparedStatement.execute();

			}

			message = "success";

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}
		return message;
	}

	public String retrieveReceiptType(int receiptID) {

		String receiptType = "";

		try {

			connection = getConnection();

			String retrieveReceiptTypeQuery = QueryMaker.RETRIEVE_RECEIPT_TYPE;

			preparedStatement = connection.prepareStatement(retrieveReceiptTypeQuery);

			preparedStatement.setInt(1, receiptID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				receiptType = resultSet.getString("receiptType");
			}

			resultSet.close();
			preparedStatement.close();

			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return receiptType;
	}

	public JSONObject deleteProduct(int componentID) {

		JSONObject values = null;

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		try {
			connection = getConnection();

			String deleteProductQuery = QueryMaker.DELETE_PRODUCT_COMPONENT_FROM_RECEIPT_ITEMS;

			preparedStatement = connection.prepareStatement(deleteProductQuery);

			preparedStatement.setInt(1, componentID);

			preparedStatement.executeUpdate();

			object.put("check", "1");

			array.add(object);

			values.put("Release", array);

			preparedStatement.close();
			connection.close();

			return values;

		} catch (Exception exception) {
			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while deleting product component from ReceiptItems");

			array.add(object);

			values.put("Release", array);

			return values;
		}
	}

	public JSONObject retrieveBagDetails(int componentID) {

		JSONObject values = null;

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = null;

		try {
			connection = getConnection();

			String retrieveBagDetailsQuery = QueryMaker.RETRIEVE_BAG_DETAILS;

			preparedStatement = connection.prepareStatement(retrieveBagDetailsQuery);

			preparedStatement.setInt(1, componentID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				object = new JSONObject();

				object.put("bagNo", resultSet.getString("bagNo"));
				object.put("bloodGroup", resultSet.getString("bloodGroup"));

				array.add(object);

				values.put("Release", array);

			}

			preparedStatement.close();
			connection.close();

			return values;

		} catch (Exception exception) {
			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while retrieving bag details based on receiptItemID");

			array.add(object);

			values.put("Release", array);

			return values;
		}
	}

	public JSONObject retrieveShiftTime(String shiftName) {

		JSONObject values = null;

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = null;

		try {
			connection = getConnection();

			String retrieveShiftTimeQuery = QueryMaker.RETRIEVE_SHIFT_TIME;

			preparedStatement = connection.prepareStatement(retrieveShiftTimeQuery);

			preparedStatement.setString(1, shiftName);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				object = new JSONObject();

				object.put("starTime", resultSet.getString("starTime"));
				object.put("endTime", resultSet.getString("endTime"));
				object.put("shiftOrder", resultSet.getInt("shiftOrder"));

			}
			array.add(object);

			values.put("Release", array);

			preparedStatement.close();
			connection.close();

			return values;

		} catch (Exception exception) {
			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while retrieving shift time based on Shift ");

			array.add(object);

			values.put("Release", array);

			return values;
		}
	}

	public List<String> retrieveShiftName(int bloodBankID) {

		List<String> list = new ArrayList<String>();

		try {

			connection = getConnection();

			String retrieveShiftNameQuery = QueryMaker.RETRIEVE_SHIFT_NAME;

			preparedStatement = connection.prepareStatement(retrieveShiftNameQuery);

			preparedStatement.setInt(1, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				list.add(resultSet.getString("name"));
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return list;
	}

	public JSONObject retrieveCashDeposited() {

		JSONObject values = null;

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = null;

		Date today = new Date();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		try {
			connection = getConnection();

			String retrieveCashDepositedQuery = QueryMaker.RETRIEVE_CASH_DEPOSITED;

			preparedStatement = connection.prepareStatement(retrieveCashDepositedQuery);

			preparedStatement.setString(1, "%" + dateFormat.format(today) + "%");
			preparedStatement.setString(2, ActivityStatus.ENABLE);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				object = new JSONObject();

				object.put("cashDeposited", resultSet.getString("cashDeposited"));

				array.add(object);

				values.put("Release", array);

			}

			preparedStatement.close();
			connection.close();

			return values;

		} catch (Exception exception) {
			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while retrieving cashDeposited");

			array.add(object);

			values.put("Release", array);

			return values;
		}
	}

	public String updateCashDeposited(double cashDeposited) {

		Date today = new Date();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		try {

			connection = getConnection();

			String updateCashDepositedQuery = QueryMaker.UPDATE_CASH_DEPOSITED;

			preparedStatement = connection.prepareStatement(updateCashDepositedQuery);

			preparedStatement.setDouble(1, cashDeposited);
			preparedStatement.setString(2, dateFormat.format(today));
			preparedStatement.setString(3, ActivityStatus.ENABLE);

			preparedStatement.executeUpdate();

			message = "success";

			System.out.println("Register updated successfully.");

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}

		return message;
	}

	public double retrieveAdvancePaymentSum(String startTime, String endTime) {

		double advancePayment = 0D;

		Date today = new Date();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		try {

			connection = getConnection();

			String retrieveAdvancePaymentSumQuery = QueryMaker.RETRIEVE_ADVANCE_PAYMENT_SUM;

			preparedStatement = connection.prepareStatement(retrieveAdvancePaymentSumQuery);

			preparedStatement.setString(1, "%" + dateFormat.format(today) + "%");
			preparedStatement.setString(2, dateFormat.format(today) + " " + startTime);
			preparedStatement.setString(3, dateFormat.format(today) + " " + endTime);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				advancePayment = resultSet.getDouble("SUM");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return advancePayment;
	}

	public double retrieveCashHandover() {

		double cashHandover = 0D;

		Date today = new Date();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		try {

			connection = getConnection();

			String retrieveCashHandoverQuery = QueryMaker.RETRIEVE_CASH_HANDOVER;

			preparedStatement = connection.prepareStatement(retrieveCashHandoverQuery);

			preparedStatement.setString(1, "%" + dateFormat.format(today) + "%");
			preparedStatement.setString(2, ActivityStatus.ENABLE);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				cashHandover = resultSet.getDouble("cashHandover");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return cashHandover;
	}

	public String updateRegisterBalance(double registerBalance, int cashHandoverTo) {

		Date today = new Date();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		try {

			connection = getConnection();

			String updateRegisterBalanceQuery = QueryMaker.UPDATE_REGISTER_BALANCE;

			preparedStatement = connection.prepareStatement(updateRegisterBalanceQuery);

			preparedStatement.setDouble(1, registerBalance);
			preparedStatement.setString(2, ActivityStatus.DISABLE);
			preparedStatement.setInt(3, cashHandoverTo);
			preparedStatement.setString(4, dateFormat.format(today));
			preparedStatement.setString(5, ActivityStatus.ENABLE);

			preparedStatement.executeUpdate();

			message = "success";

			System.out.println("Successfully udpated register balance into register");

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}

		return message;
	}

	public String updateTotalBill(double totalBill) {

		Date today = new Date();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		try {

			connection = getConnection();

			String updateTotalBillQuery = QueryMaker.UPDATE_TOTAL_BILL_INTO_REGISTER;

			preparedStatement = connection.prepareStatement(updateTotalBillQuery);

			preparedStatement.setDouble(1, totalBill);
			preparedStatement.setString(2, dateFormat.format(today));
			preparedStatement.setString(3, ActivityStatus.ENABLE);

			preparedStatement.executeUpdate();

			message = "success";

			System.out.println("Successfully udpated total cash into register");

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}

		return message;

	}

	public double retrieveCashdeposited() {

		double cashDeposited = 0D;

		Date today = new Date();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		try {

			connection = getConnection();

			String retrieveCashdepositedQuery = QueryMaker.RETRIEVE_CASH_DEPOSITED;

			preparedStatement = connection.prepareStatement(retrieveCashdepositedQuery);

			preparedStatement.setString(1, "%" + dateFormat.format(today) + "%");
			preparedStatement.setString(2, ActivityStatus.ENABLE);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				cashDeposited = resultSet.getDouble("cashDeposited");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return cashDeposited;
	}

	public double retrieveOtherProductCash() {

		double otherProductCash = 0D;

		Date today = new Date();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		try {

			connection = getConnection();

			String retrieveOtherProductCashQuery = QueryMaker.RETRIEVE_OTHER_PRODUCT_CASH;

			preparedStatement = connection.prepareStatement(retrieveOtherProductCashQuery);

			preparedStatement.setString(1, "%" + dateFormat.format(today) + "%");
			preparedStatement.setString(2, ActivityStatus.ENABLE);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				otherProductCash = resultSet.getDouble("otherProduct");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return otherProductCash;
	}

	public String retrieveRegisterShiftTime() {

		String registerShiftTime = "";

		Date today = new Date();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		try {

			connection = getConnection();

			String retrieveRegisterShiftTimeQuery = QueryMaker.RETRIEVE_REGISTER_SHIFT_TIME;

			preparedStatement = connection.prepareStatement(retrieveRegisterShiftTimeQuery);

			preparedStatement.setString(1, "%" + dateFormat.format(today) + "%");
			preparedStatement.setString(2, ActivityStatus.ENABLE);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				String startTime = resultSet.getString("registerStartTime");
				String endTime = resultSet.getString("registerEndTime");

				registerShiftTime = startTime + "+" + endTime;

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return registerShiftTime;
	}

	public String insertOtherProduct(BillingForm billingForm) {

		try {
			connection = getConnection();

			String insertOtherProductQuery = QueryMaker.INSERT_OTHER_PRODUCT;

			preparedStatement = connection.prepareStatement(insertOtherProductQuery);

			preparedStatement.setDouble(1, billingForm.getOtherProductAmount());
			preparedStatement.setString(2, billingForm.getOtherProduct());
			preparedStatement.setInt(3, billingForm.getUserID());

			preparedStatement.execute();

			message = "success";

			System.out.println("Successfully inserted Other product details");

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}

		return message;
	}

	public double retrieveTodaysOtherProductAmount() {

		double todaysOtherProductAmount = 0D;

		Date today = new Date();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		try {

			connection = getConnection();

			String retrieveTodaysOtherProductAmountQuery = QueryMaker.RETRIEVE_TODAYS_OTHER_PRODUCT_AMOUNT;

			preparedStatement = connection.prepareStatement(retrieveTodaysOtherProductAmountQuery);

			preparedStatement.setString(1, "%" + dateFormat.format(today) + "%");

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				todaysOtherProductAmount = resultSet.getDouble("SUM");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return todaysOtherProductAmount;
	}

	public String updateOtherProductIntoRegister(double otherProductAmount) {

		Date today = new Date();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		try {

			connection = getConnection();

			String updateOtherProductIntoRegisterQuery = QueryMaker.UPDATE_OTHER_PRODUCT_INTO_REGISTER;

			preparedStatement = connection.prepareStatement(updateOtherProductIntoRegisterQuery);

			preparedStatement.setDouble(1, otherProductAmount);
			preparedStatement.setString(2, dateFormat.format(today));
			preparedStatement.setString(3, ActivityStatus.ENABLE);

			preparedStatement.executeUpdate();

			message = "success";

			System.out.println("Other product updated into Register");

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}

		return message;
	}

	public JSONObject retrieveBalanceDetails(String startDate, String endDate) {

		JSONObject values = null;

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = null;

		int check = 0;

		SimpleDateFormat dateToBeFormatted = new SimpleDateFormat("yyyy-MM-dd");

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

		try {
			connection = getConnection();

			String retrieveBalanceDetailsQuery = QueryMaker.RETRIEVE_BALANCE_DETAILS;

			preparedStatement = connection.prepareStatement(retrieveBalanceDetailsQuery);

			preparedStatement.setString(1, dateToBeFormatted.format(dateFormat.parse(startDate)) + " 00:00");
			preparedStatement.setString(2, dateToBeFormatted.format(dateFormat.parse(endDate)) + " 23:59");

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				check = 1;

				object = new JSONObject();

				object.put("registerDate", dateFormat.format(resultSet.getDate("registerDate")));
				object.put("registerBalance", resultSet.getDouble("expectedCashBalance"));
				object.put("cashDeposited", resultSet.getDouble("bankDeposit"));
				object.put("check", "1");

				array.add(object);

				values.put("Release", array);

			}

			if (check == 0) {
				object = new JSONObject();

				object.put("check", "0");

				array.add(object);

				values.put("Release", array);
			}

			preparedStatement.close();
			connection.close();

			return values;

		} catch (Exception exception) {
			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while retrieving balance details");

			array.add(object);

			values.put("Release", array);

			return values;
		}
	}

	public JSONObject retrieveBloodGroup(String bloodBagNo) {

		JSONObject values = null;

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		int check = 0;

		try {
			connection = getJankalyanConnection();

			String retrieveBloodGroupQuery = QueryMaker.RETRIEVE_BLOOD_GROUP;

			preparedStatement = connection.prepareStatement(retrieveBloodGroupQuery);

			preparedStatement.setString(1, bloodBagNo);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				check = 1;

				object.put("bloodGroup",
						resultSet.getString("actualbloodgroup") + " " + resultSet.getString("actualrh"));
				object.put("check", check);

				array.add(object);

				values.put("Release", array);

			}

			if (check == 0) {

				object.put("check", check);

				array.add(object);

				values.put("Release", array);
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

			return values;

		} catch (Exception exception) {
			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while retrieving blood group based on bag no");

			array.add(object);

			values.put("Release", array);

			return values;
		}
	}

	public JSONObject verifyLastRegisterOpen(int shiftOrder) {

		JSONObject values = null;

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		int check = 0;

		// getting yesterday's date
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);

		Date yesterday = cal.getTime();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		Date today = new Date();

		try {
			connection = getConnection();

			System.out.println("Shift order is :: " + shiftOrder);

			/*
			 * Checking whether shift order is 1, if 1, then check if last register of
			 * yesterday is open or not, and if shift order is other than 1 subtract 1 from
			 * it and check whether today's last register remained opened or not
			 */
			if (shiftOrder == 1) {

				String verifyLastRegisterOpenQuery = QueryMaker.VERIFY_YESTERDAYS_LAST_REGISTER_OPEN;

				preparedStatement = connection.prepareStatement(verifyLastRegisterOpenQuery);

				preparedStatement.setString(1, dateFormat.format(yesterday));
				preparedStatement.setString(2, ActivityStatus.ENABLE);

				resultSet = preparedStatement.executeQuery();

				while (resultSet.next()) {

					check = 1;

				}

				object.put("check", check);

				array.add(object);

				values.put("Release", array);

			} else {

				shiftOrder = shiftOrder - 1;

				String verifyLastRegisterOpenQuery = QueryMaker.VERIFY_TODAYS_LAST_REGISTER_OPEN;

				preparedStatement = connection.prepareStatement(verifyLastRegisterOpenQuery);

				preparedStatement.setString(1, dateFormat.format(today));
				preparedStatement.setString(2, ActivityStatus.ENABLE);
				preparedStatement.setInt(3, shiftOrder);

				resultSet = preparedStatement.executeQuery();

				while (resultSet.next()) {

					check = 1;

				}

				object.put("check", check);

				array.add(object);

				values.put("Release", array);

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

			return values;

		} catch (Exception exception) {
			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while verifying whether last register was opened or not");

			array.add(object);

			values.put("Release", array);

			return values;
		}
	}

	public String retrieveRegisterShiftTime(int shiftOrder) {

		String registerShiftTime = "";

		// getting yesterday's date
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);

		Date yesterday = cal.getTime();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		Date today = new Date();

		try {

			connection = getConnection();

			if (shiftOrder == 1) {

				String retrieveRegisterShiftTimeQuery = QueryMaker.RETRIEVE_YESTERDAYS_LAST_REGISTER_SHIFT_TIME;

				preparedStatement = connection.prepareStatement(retrieveRegisterShiftTimeQuery);

				preparedStatement.setString(1, "%" + dateFormat.format(yesterday) + "%");
				preparedStatement.setString(2, ActivityStatus.ENABLE);

				resultSet = preparedStatement.executeQuery();

				while (resultSet.next()) {
					String startTime = resultSet.getString("registerStartTime");
					String endTime = resultSet.getString("registerEndTime");

					registerShiftTime = startTime + "+" + endTime;

				}

			} else {

				shiftOrder = shiftOrder - 1;

				String retrieveRegisterShiftTimeQuery = QueryMaker.RETRIEVE_TODAYS_LAST_REGISTER_SHIFT_TIME;

				preparedStatement = connection.prepareStatement(retrieveRegisterShiftTimeQuery);

				preparedStatement.setString(1, "%" + dateFormat.format(today) + "%");
				preparedStatement.setString(2, ActivityStatus.ENABLE);
				preparedStatement.setInt(3, shiftOrder);

				resultSet = preparedStatement.executeQuery();

				while (resultSet.next()) {
					String startTime = resultSet.getString("registerStartTime");
					String endTime = resultSet.getString("registerEndTime");

					registerShiftTime = startTime + "+" + endTime;

				}

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return registerShiftTime;
	}

	public double retrieveAdvancePaymentSum(String startTime, String endTime, int shiftOrder) {

		double advancePayment = 0D;

		// getting yesterday's date
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);

		Date yesterday = cal.getTime();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		Date today = new Date();

		try {

			connection = getConnection();

			if (shiftOrder == 1) {

				String retrieveAdvancePaymentSumQuery = QueryMaker.RETRIEVE_ADVANCE_PAYMENT_SUM;

				preparedStatement = connection.prepareStatement(retrieveAdvancePaymentSumQuery);

				preparedStatement.setString(1, "%" + dateFormat.format(yesterday) + "%");
				preparedStatement.setString(2, dateFormat.format(yesterday) + " " + startTime);
				preparedStatement.setString(3, dateFormat.format(yesterday) + " " + endTime);

				resultSet = preparedStatement.executeQuery();

				while (resultSet.next()) {
					advancePayment = resultSet.getDouble("SUM");
				}

			} else {

				String retrieveAdvancePaymentSumQuery = QueryMaker.RETRIEVE_ADVANCE_PAYMENT_SUM;

				preparedStatement = connection.prepareStatement(retrieveAdvancePaymentSumQuery);

				preparedStatement.setString(1, "%" + dateFormat.format(today) + "%");
				preparedStatement.setString(2, dateFormat.format(today) + " " + startTime);
				preparedStatement.setString(3, dateFormat.format(today) + " " + endTime);

				resultSet = preparedStatement.executeQuery();

				while (resultSet.next()) {
					advancePayment = resultSet.getDouble("SUM");
				}

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return advancePayment;
	}

	public double retrieveCashHandover(int shiftOrder) {

		double cashHandover = 0D;

		// getting yesterday's date
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);

		Date yesterday = cal.getTime();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		Date today = new Date();

		try {

			connection = getConnection();

			if (shiftOrder == 1) {

				String retrieveCashHandoverQuery = QueryMaker.RETRIEVE_YESTERDAYS_CASH_HANDOVER;

				preparedStatement = connection.prepareStatement(retrieveCashHandoverQuery);

				preparedStatement.setString(1, "%" + dateFormat.format(yesterday) + "%");
				preparedStatement.setString(2, ActivityStatus.ENABLE);

				resultSet = preparedStatement.executeQuery();

				while (resultSet.next()) {
					cashHandover = resultSet.getDouble("cashHandover");
				}

			} else {

				shiftOrder = shiftOrder - 1;

				String retrieveCashHandoverQuery = QueryMaker.RETRIEVE_TODAYS_CASH_HANDOVER;

				preparedStatement = connection.prepareStatement(retrieveCashHandoverQuery);

				preparedStatement.setString(1, "%" + dateFormat.format(today) + "%");
				preparedStatement.setString(2, ActivityStatus.ENABLE);

				resultSet = preparedStatement.executeQuery();

				while (resultSet.next()) {
					cashHandover = resultSet.getDouble("cashHandover");
				}

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return cashHandover;
	}

	public double retrieveCashdeposited(int shiftOrder) {

		double cashDeposited = 0D;

		// getting yesterday's date
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);

		Date yesterday = cal.getTime();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		Date today = new Date();

		try {

			connection = getConnection();

			if (shiftOrder == 1) {

				String retrieveCashdepositedQuery = QueryMaker.RETRIEVE_YESTERDAYS_CASH_DEPOSITED;

				preparedStatement = connection.prepareStatement(retrieveCashdepositedQuery);

				preparedStatement.setString(1, "%" + dateFormat.format(yesterday) + "%");
				preparedStatement.setString(2, ActivityStatus.ENABLE);

				resultSet = preparedStatement.executeQuery();

				while (resultSet.next()) {
					cashDeposited = resultSet.getDouble("cashDeposited");
				}

			} else {

				shiftOrder = shiftOrder - 1;

				String retrieveCashdepositedQuery = QueryMaker.RETRIEVE_TODAYS_CASH_DEPOSITED;

				preparedStatement = connection.prepareStatement(retrieveCashdepositedQuery);

				preparedStatement.setString(1, "%" + dateFormat.format(today) + "%");
				preparedStatement.setString(2, ActivityStatus.ENABLE);
				preparedStatement.setInt(3, shiftOrder);

				resultSet = preparedStatement.executeQuery();

				while (resultSet.next()) {
					cashDeposited = resultSet.getDouble("cashDeposited");
				}

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return cashDeposited;
	}

	public double retrieveOtherProductCash(int shiftOrder) {

		double otherProductCash = 0D;

		// getting yesterday's date
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);

		Date yesterday = cal.getTime();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		Date today = new Date();

		try {

			connection = getConnection();

			if (shiftOrder == 1) {

				String retrieveOtherProductCashQuery = QueryMaker.RETRIEVE_YESTERDAYS_OTHER_PRODUCT_CASH;

				preparedStatement = connection.prepareStatement(retrieveOtherProductCashQuery);

				preparedStatement.setString(1, "%" + dateFormat.format(yesterday) + "%");
				preparedStatement.setString(2, ActivityStatus.ENABLE);

				resultSet = preparedStatement.executeQuery();

				while (resultSet.next()) {
					otherProductCash = resultSet.getDouble("otherProduct");
				}

			} else {

				shiftOrder = shiftOrder - 1;

				String retrieveOtherProductCashQuery = QueryMaker.RETRIEVE_TODAYS_OTHER_PRODUCT_CASH;

				preparedStatement = connection.prepareStatement(retrieveOtherProductCashQuery);

				preparedStatement.setString(1, "%" + dateFormat.format(today) + "%");
				preparedStatement.setString(2, ActivityStatus.ENABLE);
				preparedStatement.setInt(3, shiftOrder);

				resultSet = preparedStatement.executeQuery();

				while (resultSet.next()) {
					otherProductCash = resultSet.getDouble("otherProduct");
				}

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return otherProductCash;
	}

	public List<BillingForm> searchOtherProducts(String searchOtherProductName) {

		List<BillingForm> list = new ArrayList<BillingForm>();

		BillingForm billingForm = null;

		try {

			connection = getConnection();

			String searchOtherProductsQuery = QueryMaker.SEARCH_OTHER_PRODUCT_BY_PRODUCT_NAME;

			preparedStatement = connection.prepareStatement(searchOtherProductsQuery);

			if (searchOtherProductName.contains(" ")) {
				searchOtherProductName = searchOtherProductName.replace(" ", "%");
			}

			preparedStatement.setString(1, "%" + searchOtherProductName + "%");

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				billingForm = new BillingForm();

				billingForm.setOtherProduct(resultSet.getString("product"));
				billingForm.setOtherProductAmount(resultSet.getDouble("amount"));
				billingForm.setOtherProductTransactionDate(resultSet.getString("transactionDate").split("\\.")[0]);
				billingForm.setOtherProductAddedByName(retrieveUserNamebyID(resultSet.getInt("userID")));

				list.add(billingForm);

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return list;
	}

	public String retrieveUserNamebyID(int userID) {

		String userName = "";

		try {

			connection1 = getConnection();

			String retrieveUserNamebyIDQuery = QueryMaker.RETRIEVE_RECEIPT_BY_NAME;

			preparedStatement1 = connection1.prepareStatement(retrieveUserNamebyIDQuery);

			preparedStatement1.setInt(1, userID);

			resultSet1 = preparedStatement1.executeQuery();

			while (resultSet1.next()) {
				if (resultSet1.getString("middleName") == "" || resultSet1.getString("middleName") == null) {
					userName = resultSet1.getString("firstName") + " " + resultSet1.getString("lastName");
				} else {
					userName = resultSet1.getString("firstName") + " " + resultSet1.getString("middleName") + " "
							+ resultSet1.getString("lastName");
				}
			}

			resultSet1.close();
			preparedStatement1.close();
			connection1.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return userName;
	}

	public List<BillingForm> retrieveAllOtherProcucts() {

		List<BillingForm> list = new ArrayList<BillingForm>();

		BillingForm billingForm = null;

		try {

			connection = getConnection();

			String retrieveAllOtherProcuctsQuery = QueryMaker.RETRIEVE_ALL_OTHER_PRODUCT;

			preparedStatement = connection.prepareStatement(retrieveAllOtherProcuctsQuery);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				billingForm = new BillingForm();

				billingForm.setOtherProduct(resultSet.getString("product"));
				billingForm.setOtherProductAmount(resultSet.getDouble("amount"));
				billingForm.setOtherProductTransactionDate(resultSet.getString("transactionDate").split("\\.")[0]);
				billingForm.setOtherProductAddedByName(retrieveUserNamebyID(resultSet.getInt("userID")));

				list.add(billingForm);

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return list;
	}

	public int retrievePatientIdByBloodBankStorageCenter(String bbStorageCenter) {

		int patientID = 0;

		try {

			connection = getConnection();

			String retrievePatientIdByBloodBankStorageCenterQuery = QueryMaker.RETRIEVE_PATIENT_ID_BASED_ON_BB_STORAGE_CENTER;

			preparedStatement = connection.prepareStatement(retrievePatientIdByBloodBankStorageCenterQuery);

			preparedStatement.setString(1, bbStorageCenter);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				patientID = resultSet.getInt("id");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return patientID;
	}

	public JSONObject retireveReceiptDetailsByPatientID(int patientID) {

		JSONObject values = new JSONObject();

		JSONArray array = new JSONArray();

		JSONObject object = null;

		int check = 0;

		try {

			connection = getConnection();

			String retireveReceiptDetailsByPatientIDQuery = QueryMaker.RETRIEVE_RECEIPT_DETAILS_BY_PATIENT_ID;

			preparedStatement = connection.prepareStatement(retireveReceiptDetailsByPatientIDQuery);

			preparedStatement.setInt(1, patientID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				check++;

				object = new JSONObject();

				object.put("receiptNo", resultSet.getString("receiptNo"));
				object.put("receiptDate", resultSet.getString("receiptDate").split("\\.")[0]);
				object.put("netAmt", resultSet.getDouble("netReceivableAmt"));
				object.put("outstadingAmt", resultSet.getDouble("outstandingAmt"));

				if (resultSet.getString("product").equals("ABD Screening")
						&& resultSet.getString("product").equals("Gr Cross Match")) {

					object.put("ABD", "Yes");
					object.put("CR", "Yes");

				} else if (resultSet.getString("product").equals("ABD Screening")) {

					object.put("ABD", "Yes");
					object.put("CR", "");

				} else {

					object.put("ABD", "");
					object.put("CR", "Yes");

				}

				object.put("check", check);

				array.add(object);

				values.put("Release", array);

			}

			if (check == 0) {

				object = new JSONObject();

				object.put("noRecord", "No records found.");

				object.put("check", check);

				array.add(object);

				values.put("Release", array);

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

			return values;

		} catch (Exception exception) {
			exception.printStackTrace();

			object = new JSONObject();

			object.put("ErrMsg", "Exception occurred while retrieving receipt details based on patientID");

			array.add(object);

			values.put("Release", array);

			return values;
		}

	}

	public JSONObject insertFirstRegisterDetails(double startCash) {

		JSONObject values = new JSONObject();

		JSONObject object = new JSONObject();

		JSONArray array = new JSONArray();

		try {

			connection = getConnection();

			String insertFirstRegisterDetailsQuery = QueryMaker.INSERT_FIRST_REGISTER_DETAILS;

			preparedStatement = connection.prepareStatement(insertFirstRegisterDetailsQuery);

			preparedStatement.setDouble(1, startCash);
			preparedStatement.setDouble(2, startCash);

			preparedStatement.execute();

			message = "success";

			object.put("status", "success");

			array.add(object);

			values.put("Release", array);

			preparedStatement.close();
			connection.close();

			return values;

		} catch (Exception exception) {
			exception.printStackTrace();

			object = new JSONObject();

			object.put("status", "error");

			array.add(object);

			values.put("Release", array);

			return values;
		}

	}

	public String udpatedFirstRegister(int registerID, int userID, BillingForm billingForm) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

		SimpleDateFormat currentDate = new SimpleDateFormat("dd-MM-yyyy HH:mm");

		try {

			connection = getConnection();

			String udpatedFirstRegisterQuery = QueryMaker.UPDATED_FIRST_REGISTER_DETAILS;

			preparedStatement = connection.prepareStatement(udpatedFirstRegisterQuery);

			preparedStatement.setString(1, dateFormat.format(currentDate.parse(billingForm.getRegisterDate())));
			preparedStatement.setString(2, timeFormat.format(currentDate.parse(billingForm.getRegisterDate())));
			preparedStatement.setDouble(3, billingForm.getBloodBankBalance());
			preparedStatement.setDouble(4, billingForm.getOtherProductBalance());
			preparedStatement.setDouble(5, billingForm.getExpectedCash());
			preparedStatement.setDouble(6, billingForm.getActualCash());
			preparedStatement.setDouble(7, billingForm.getCashMatchingError());
			preparedStatement.setDouble(8, billingForm.getCashMismatchCarryOver());
			preparedStatement.setString(9, ActivityStatus.ACTIVE);
			preparedStatement.setInt(10, userID);
			preparedStatement.setInt(11, registerID);

			preparedStatement.execute();

			System.out.println("Register started successfully.");

			message = "success";

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}

		return message;
	}

	public String insertRegister(BillingForm billingForm, int userID) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

		SimpleDateFormat currentDate = new SimpleDateFormat("dd-MM-yyyy HH:mm");

		try {

			connection = getConnection();

			String insertRegisterQuery = QueryMaker.INSERT_REGISTER;

			preparedStatement = connection.prepareStatement(insertRegisterQuery);

			preparedStatement.setString(1, dateFormat.format(currentDate.parse(billingForm.getRegisterDate())));
			preparedStatement.setString(2, timeFormat.format(currentDate.parse(billingForm.getRegisterDate())));
			preparedStatement.setDouble(3, billingForm.getExpectedCash());
			preparedStatement.setDouble(4, billingForm.getBloodBankBalance());
			preparedStatement.setDouble(5, billingForm.getOtherProductBalance());
			preparedStatement.setDouble(6, billingForm.getExpectedCash());
			preparedStatement.setDouble(7, billingForm.getActualCash());
			preparedStatement.setDouble(8, billingForm.getCashMatchingError());
			preparedStatement.setDouble(9, billingForm.getCashMismatchCarryOver());
			preparedStatement.setString(10, ActivityStatus.ACTIVE);
			preparedStatement.setInt(11, userID);

			preparedStatement.execute();

			message = "success";

			System.out.println("Register opened successfully.");

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}

		return message;
	}

	public double retrieveLastCashMismatchCarryOver() {

		double cashMismatchCarryOver = 0D;

		try {

			connection = getConnection();

			String retrieveLastCashMismatchCarryOverQuery = QueryMaker.RETRIEVE_LAST_CASH_MISMATCH_CARRY_OVER;

			preparedStatement = connection.prepareStatement(retrieveLastCashMismatchCarryOverQuery);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				cashMismatchCarryOver = resultSet.getDouble("cashMismatchCarryOver");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return cashMismatchCarryOver;
	}

	public JSONObject retrievePreviousExpectedCash() {

		JSONObject values = new JSONObject();

		JSONObject object = new JSONObject();

		JSONArray array = new JSONArray();

		double expectedCash = 0D;

		try {

			connection = getConnection();

			String retrievePreviousExpectedCashQuery = QueryMaker.RETRIEVE_PREVIOUS_EXPECTED_END_CASH;

			preparedStatement = connection.prepareStatement(retrievePreviousExpectedCashQuery);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				expectedCash = resultSet.getDouble("expectedCashBalance");
			}

			object.put("expectedCash", expectedCash);

			array.add(object);

			values.put("Release", array);

			preparedStatement.close();
			connection.close();

			return values;

		} catch (Exception exception) {
			exception.printStackTrace();

			object = new JSONObject();

			object.put("expectedCash", "0");

			array.add(object);

			values.put("Release", array);

			return values;
		}

	}

	public JSONObject retrieveBankDepositeList() {

		JSONObject values = new JSONObject();

		JSONObject object = null;

		JSONArray array = new JSONArray();

		int check = 0;

		try {

			connection = getConnection();

			String retrieveBankDepositeListQuery = QueryMaker.RETRIEVE_BANK_DEPOSITE_LIST;

			preparedStatement = connection.prepareStatement(retrieveBankDepositeListQuery);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				check++;

				object = new JSONObject();

				object.put("bankDepID", resultSet.getInt("id"));
				object.put("cashDeposited", resultSet.getDouble("cashDeposited"));
				object.put("comment", resultSet.getString("comment"));
				object.put("registerID", resultSet.getInt("registerID"));
				object.put("check", check);

				array.add(object);

				values.put("Release", array);
			}

			if (check == 0) {

				object = new JSONObject();

				object.put("check", check);

				array.add(object);

				values.put("Release", array);

			}

			preparedStatement.close();
			connection.close();

			return values;

		} catch (Exception exception) {
			exception.printStackTrace();

			object = new JSONObject();

			object.put("expectedCash", "0");

			array.add(object);

			values.put("Release", array);

			return values;
		}

	}

	public JSONObject retrieveCashAdjustmentList() {

		JSONObject values = new JSONObject();

		JSONObject object = null;

		JSONArray array = new JSONArray();

		int check = 0;

		try {

			connection = getConnection();

			String retrieveCashAdjustmentListQuery = QueryMaker.RETRIEVE_CASH_ADJUSTMENT_LIST;

			preparedStatement = connection.prepareStatement(retrieveCashAdjustmentListQuery);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				check++;

				object = new JSONObject();

				object.put("cashAdjID", resultSet.getInt("id"));
				object.put("cashAdjusted", resultSet.getDouble("cashAdjusted"));
				object.put("reason", resultSet.getString("reason"));
				object.put("resgiterID", resultSet.getInt("registerID"));
				object.put("check", check);

				array.add(object);

				values.put("Release", array);
			}

			if (check == 0) {

				object = new JSONObject();

				object.put("check", check);

				array.add(object);

				values.put("Release", array);

			}

			preparedStatement.close();
			connection.close();

			return values;

		} catch (Exception exception) {
			exception.printStackTrace();

			object = new JSONObject();

			object.put("expectedCash", "0");

			array.add(object);

			values.put("Release", array);

			return values;
		}

	}

	public JSONObject retrieveCashDepositeByID(int cashDepID) {

		JSONObject values = new JSONObject();

		JSONObject object = new JSONObject();

		JSONArray array = new JSONArray();

		try {

			connection = getConnection();

			String retrieveCashDepositeByIDQuery = QueryMaker.RETRIEVE_CASH_DEPOSITED_BY_ID;

			preparedStatement = connection.prepareStatement(retrieveCashDepositeByIDQuery);

			preparedStatement.setInt(1, cashDepID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				object.put("cashDepID", resultSet.getInt("id"));
				object.put("cashDeposited", resultSet.getDouble("cashDeposited"));
				object.put("comment", resultSet.getString("comment"));

				array.add(object);

				values.put("Release", array);

			}

			preparedStatement.close();
			connection.close();

			return values;

		} catch (Exception exception) {
			exception.printStackTrace();

			object = new JSONObject();

			object.put("ErrMsg", "Exception occurred while retrieving cash deposit details based on ID");

			array.add(object);

			values.put("Release", array);

			return values;
		}

	}

	public JSONObject deleteCashDepositeByID(int cashDepID) {

		JSONObject values = new JSONObject();

		JSONObject object = new JSONObject();

		JSONArray array = new JSONArray();

		try {

			connection = getConnection();

			String deleteCashDepositeByIDQuery = QueryMaker.DELETE_CASH_DEPOSITED_BY_ID;

			preparedStatement = connection.prepareStatement(deleteCashDepositeByIDQuery);

			preparedStatement.setInt(1, cashDepID);

			preparedStatement.executeUpdate();

			System.out.println("Cash deposit deleted successfully.");

			object.put("msg", "success");

			array.add(object);

			values.put("Release", array);

			preparedStatement.close();
			connection.close();

			return values;

		} catch (Exception exception) {
			exception.printStackTrace();

			object = new JSONObject();

			object.put("msg", "error");

			array.add(object);

			values.put("Release", array);

			return values;
		}

	}

	public JSONObject retrieveCashAdjustedByID(int cashAdjID) {

		JSONObject values = new JSONObject();

		JSONObject object = new JSONObject();

		JSONArray array = new JSONArray();

		try {

			connection = getConnection();

			String retrieveCashAdjustedByIDQuery = QueryMaker.RETRIEVE_CASH_ADJUSTED_BY_ID;

			preparedStatement = connection.prepareStatement(retrieveCashAdjustedByIDQuery);

			preparedStatement.setInt(1, cashAdjID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				object.put("cashAdjID", resultSet.getInt("id"));
				object.put("cashAdjusted", resultSet.getDouble("cashAdjusted"));
				object.put("reason", resultSet.getString("reason"));

				array.add(object);

				values.put("Release", array);

			}

			preparedStatement.close();
			connection.close();

			return values;

		} catch (Exception exception) {
			exception.printStackTrace();

			object = new JSONObject();

			object.put("ErrMsg", "Exception occurred while retrieving cash adjustment details based on ID");

			array.add(object);

			values.put("Release", array);

			return values;
		}

	}

	public JSONObject deleteCashAdjustedByID(int cashAdjID) {

		JSONObject values = new JSONObject();

		JSONObject object = new JSONObject();

		JSONArray array = new JSONArray();

		try {

			connection = getConnection();

			String deleteCashAdjustedByIDQuery = QueryMaker.DELETE_CASH_ADJUSTED_BY_ID;

			preparedStatement = connection.prepareStatement(deleteCashAdjustedByIDQuery);

			preparedStatement.setInt(1, cashAdjID);

			preparedStatement.executeUpdate();

			System.out.println("Cash adjustment deleted successfully.");

			object.put("msg", "success");

			array.add(object);

			values.put("Release", array);

			preparedStatement.close();
			connection.close();

			return values;

		} catch (Exception exception) {
			exception.printStackTrace();

			object = new JSONObject();

			object.put("msg", "error");

			array.add(object);

			values.put("Release", array);

			return values;
		}

	}

	public JSONObject insertCashAdjustment(BillingForm billingForm, int userID, int registerID) {

		JSONObject values = new JSONObject();

		JSONObject object = new JSONObject();

		JSONArray array = new JSONArray();

		Date date = new Date();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		try {

			connection = getConnection();

			String insertCashAdjustmentQuery = QueryMaker.INSERT_CASH_ADJUSTMENT;

			preparedStatement = connection.prepareStatement(insertCashAdjustmentQuery);

			preparedStatement.setDouble(1, billingForm.getCashAdjusted());
			preparedStatement.setString(2, billingForm.getReason());
			preparedStatement.setString(3, dateFormat.format(date));
			preparedStatement.setInt(4, userID);
			preparedStatement.setInt(5, registerID);

			preparedStatement.execute();

			System.out.println("Cash adjustment added successfully.");

			object.put("msg", "success");

			array.add(object);

			values.put("Release", array);

			preparedStatement.close();
			connection.close();

			return values;

		} catch (Exception exception) {
			exception.printStackTrace();

			object = new JSONObject();

			object.put("msg", "error");

			array.add(object);

			values.put("Release", array);

			return values;
		}

	}

	public int retrieveRegisterID() {

		int registerID = 0;

		try {

			connection = getConnection();

			String retrieveRegisterIDQuery = QueryMaker.RETRIEVE_REGISTER_ID;

			preparedStatement = connection.prepareStatement(retrieveRegisterIDQuery);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				registerID = resultSet.getInt("id");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return registerID;
	}

	public JSONObject updateCashAdjustment(BillingForm billingForm, int userID) {

		JSONObject values = new JSONObject();

		JSONObject object = new JSONObject();

		JSONArray array = new JSONArray();

		Date date = new Date();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		try {

			connection = getConnection();

			String updateCashAdjustmentQuery = QueryMaker.UPDATE_CASH_ADJUSTMENT;

			preparedStatement = connection.prepareStatement(updateCashAdjustmentQuery);

			preparedStatement.setDouble(1, billingForm.getCashAdjusted());
			preparedStatement.setString(2, billingForm.getReason());
			preparedStatement.setInt(3, userID);
			preparedStatement.setInt(4, billingForm.getCashAdjID());

			preparedStatement.execute();

			System.out.println("Cash adjustment updated successfully.");

			object.put("msg", "success");

			array.add(object);

			values.put("Release", array);

			preparedStatement.close();
			connection.close();

			return values;

		} catch (Exception exception) {
			exception.printStackTrace();

			object = new JSONObject();

			object.put("msg", "error");

			array.add(object);

			values.put("Release", array);

			return values;
		}

	}

	public JSONObject insertCashDeposit(BillingForm billingForm, int userID, int registerID) {

		JSONObject values = new JSONObject();

		JSONObject object = new JSONObject();

		JSONArray array = new JSONArray();

		Date date = new Date();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		try {

			connection = getConnection();

			String insertCashDepositQuery = QueryMaker.INSERT_CASH_DEPOSIT;

			preparedStatement = connection.prepareStatement(insertCashDepositQuery);

			preparedStatement.setDouble(1, billingForm.getCashDeposited());
			preparedStatement.setString(2, billingForm.getComment());
			preparedStatement.setString(3, dateFormat.format(date));
			preparedStatement.setInt(4, userID);
			preparedStatement.setInt(5, registerID);

			preparedStatement.execute();

			System.out.println("Cash deposit added successfully.");

			object.put("msg", "success");

			array.add(object);

			values.put("Release", array);

			preparedStatement.close();
			connection.close();

			return values;

		} catch (Exception exception) {
			exception.printStackTrace();

			object = new JSONObject();

			object.put("msg", "error");

			array.add(object);

			values.put("Release", array);

			return values;
		}

	}

	public JSONObject updateCashDeposit(BillingForm billingForm, int userID) {

		JSONObject values = new JSONObject();

		JSONObject object = new JSONObject();

		JSONArray array = new JSONArray();

		Date date = new Date();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		try {

			connection = getConnection();

			String insertCashDepositQuery = QueryMaker.UPDATE_CASH_DEPOSIT;

			preparedStatement = connection.prepareStatement(insertCashDepositQuery);

			preparedStatement.setDouble(1, billingForm.getCashDeposited());
			preparedStatement.setString(2, billingForm.getComment());
			preparedStatement.setInt(3, userID);
			preparedStatement.setInt(4, billingForm.getCashDepID());

			preparedStatement.execute();

			System.out.println("Cash deposit udpated successfully.");

			object.put("msg", "success");

			array.add(object);

			values.put("Release", array);

			preparedStatement.close();
			connection.close();

			return values;

		} catch (Exception exception) {
			exception.printStackTrace();

			object = new JSONObject();

			object.put("msg", "error");

			array.add(object);

			values.put("Release", array);

			return values;
		}

	}

	public String retrieveRegisterStartDateAndTime(int registerID) {

		String startDateAndTime = "";

		try {

			connection = getConnection();

			String retrieveRegisterStartDateAndTimeQuery = QueryMaker.RETRIEVE_REGISTER_START_DATE_AND_TIME;

			preparedStatement = connection.prepareStatement(retrieveRegisterStartDateAndTimeQuery);

			preparedStatement.setInt(1, registerID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				startDateAndTime = resultSet.getString("registerDate") + " " + resultSet.getString("registerStartTime");
			}

			System.out.println("Register start date and time :: " + startDateAndTime);

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return startDateAndTime;
	}

	public double retrieveBloodBankBalance(String registerStartDateAndTime) {

		double bbBalance = 0D;

		try {

			connection = getConnection();

			String retrieveBloodBankBalanceQuery = QueryMaker.RETRIEVE_BLOOD_BANK_BALANCE;

			preparedStatement = connection.prepareStatement(retrieveBloodBankBalanceQuery);

			preparedStatement.setString(1, registerStartDateAndTime);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				bbBalance = resultSet.getDouble("sum");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return bbBalance;
	}

	public double retrieveOtherProductBalance(String registerStartDateAndTime) {

		double otherProductBalance = 0D;

		try {

			connection = getConnection();

			String retrieveOtherProductBalanceQuery = QueryMaker.RETRIEVE_OTHER_PRODUCT_BALANCE;

			preparedStatement = connection.prepareStatement(retrieveOtherProductBalanceQuery);

			preparedStatement.setString(1, registerStartDateAndTime);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				otherProductBalance = resultSet.getDouble("sum");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return otherProductBalance;
	}

	public double retrieveTotalCashDepositedByRegisterID(int registerID) {

		double totalCashDeposited = 0D;

		try {

			connection = getConnection();

			String retrieveCashDepositedByRegisterIDQuery = QueryMaker.RETRIEVE_TOTAL_CASH_DEPOSITED_REGISTER_ID;

			preparedStatement = connection.prepareStatement(retrieveCashDepositedByRegisterIDQuery);

			preparedStatement.setInt(1, registerID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				totalCashDeposited = resultSet.getDouble("sum");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return totalCashDeposited;
	}

	public double retrieveTotalCashAdjustedByRegisterID(int registerID) {

		double totalCashAdjusted = 0D;

		try {

			connection = getConnection();

			String retrieveTotalCashAdjustedByRegisterIDQuery = QueryMaker.RETRIEVE_TOTAL_CASH_ADJUSTED_REGISTER_ID;

			preparedStatement = connection.prepareStatement(retrieveTotalCashAdjustedByRegisterIDQuery);

			preparedStatement.setInt(1, registerID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				totalCashAdjusted = resultSet.getDouble("sum");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return totalCashAdjusted;
	}

	public double retrieveCashHandoverByRegisterID(int registerID) {

		double cashHandover = 0D;

		try {

			connection = getConnection();

			String retrieveCashHandoverByRegisterIDQuery = QueryMaker.RETRIEVE_CASH_HANDOVER_BY_ID;

			preparedStatement = connection.prepareStatement(retrieveCashHandoverByRegisterIDQuery);

			preparedStatement.setInt(1, registerID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				cashHandover = resultSet.getDouble("handOverCash");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return cashHandover;
	}

	public String updateRegister(BillingForm billingForm) {

		SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

		SimpleDateFormat currentDate = new SimpleDateFormat("dd-MM-yyyy HH:mm");

		try {

			connection = getConnection();

			String updateRegisterQuery = QueryMaker.UPDATE_REGISTER;

			preparedStatement = connection.prepareStatement(updateRegisterQuery);

			preparedStatement.setString(1, timeFormat.format(currentDate.parse(billingForm.getRegisterDate())));
			preparedStatement.setDouble(2, billingForm.getCashHandover());
			preparedStatement.setDouble(3, billingForm.getBloodBankBalance());
			preparedStatement.setDouble(4, billingForm.getOtherProductBalance());
			preparedStatement.setDouble(5, billingForm.getExpectedCash());
			preparedStatement.setDouble(6, billingForm.getActualCash());
			preparedStatement.setDouble(7, billingForm.getCashDeposited());
			preparedStatement.setDouble(8, billingForm.getCashMatchingError());
			preparedStatement.setDouble(9, billingForm.getCashMismatchCarryOver());
			preparedStatement.setString(10, ActivityStatus.INACTIVE);
			preparedStatement.setDouble(11, billingForm.getCashAdjusted());
			preparedStatement.setInt(12, billingForm.getRegisterID());

			preparedStatement.executeUpdate();

			System.out.println("Register closed successfully.");

			message = "success";

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}

		return message;
	}

	public List<BillingForm> retrieveMyReceiptDetails(String startDate, String endDate, int userID) {

		List<BillingForm> list = new ArrayList<BillingForm>();

		BillingForm billingForm = null;

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");

		SimpleDateFormat dateToBeFormatted = new SimpleDateFormat("dd-MM-yyyy");

		SimpleDateFormat dateToBeFormatted1 = new SimpleDateFormat("yyyy-MM-dd");

		try {

			connection = getConnection();

			if (startDate.isEmpty() || endDate.isEmpty()) {

				String retrieveMyReceiptDetailsQuery = QueryMaker.RETRIEVE_MY_RECEIPT_DETAILS;

				preparedStatement = connection.prepareStatement(retrieveMyReceiptDetailsQuery);

				preparedStatement.setInt(1, userID);

				resultSet = preparedStatement.executeQuery();

				while (resultSet.next()) {

					billingForm = new BillingForm();

					billingForm.setPatientID(resultSet.getInt("patientID"));
					billingForm.setReceiptID(resultSet.getInt("id"));
					billingForm.setReceiptNo(resultSet.getString("receiptNo"));
					billingForm.setReceiptType(resultSet.getString("receiptType"));
					billingForm.setReceiptDate(
							dateToBeFormatted.format(dateFormat.parse(resultSet.getString("receiptDate"))));
					billingForm
							.setPatientName(resultSet.getString("firstName") + " " + resultSet.getString("lastName"));

					list.add(billingForm);

				}

			} else {

				String retrieveMyReceiptDetailsByDateQuery = QueryMaker.RETRIEVE_MY_RECEIPT_DETAILS_BY_DATE;

				preparedStatement = connection.prepareStatement(retrieveMyReceiptDetailsByDateQuery);

				preparedStatement.setInt(1, userID);
				preparedStatement.setString(2,
						dateToBeFormatted1.format(dateToBeFormatted.parse(startDate)) + " 00:00");
				preparedStatement.setString(3, dateToBeFormatted1.format(dateToBeFormatted.parse(endDate)) + " 23:59");

				resultSet = preparedStatement.executeQuery();

				while (resultSet.next()) {

					billingForm = new BillingForm();

					billingForm.setPatientID(resultSet.getInt("patientID"));
					billingForm.setReceiptID(resultSet.getInt("id"));
					billingForm.setReceiptNo(resultSet.getString("receiptNo"));
					billingForm.setReceiptType(resultSet.getString("receiptType"));
					billingForm.setReceiptDate(
							dateToBeFormatted.format(dateFormat.parse(resultSet.getString("receiptDate"))));
					billingForm
							.setPatientName(resultSet.getString("firstName") + " " + resultSet.getString("lastName"));

					list.add(billingForm);

				}

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return list;
	}

	public List<String> retrieveBloodStorageNameList() {

		List<String> list = new ArrayList<String>();

		try {

			connection = getConnection();

			String retrieveBloodStorageNameListQuery = QueryMaker.RETRIEVE_BLOOD_STORAGE_NAME_LIST;

			preparedStatement = connection.prepareStatement(retrieveBloodStorageNameListQuery);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				list.add(resultSet.getString("name"));
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return list;
	}

	public String insertBloodStorageName(String bscName) {

		try {

			connection = getConnection();

			String insertBloodStorageNameQuery = QueryMaker.INSERT_NEW_BLOOD_STORAGE_CENTER_NAME;

			preparedStatement = connection.prepareStatement(insertBloodStorageNameQuery);

			preparedStatement.setString(1, bscName);

			preparedStatement.execute();

			message = "success";

			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			message = "error";
		}

		return message;
	}

	public boolean verifyBloodStorageExists(String bloodStorageCenter) {

		boolean check = false;

		try {

			connection = getConnection();

			String verifyBloodStorageExistsQuery = QueryMaker.VERIFY_BSC_EXISTS;

			preparedStatement = connection.prepareStatement(verifyBloodStorageExistsQuery);

			preparedStatement.setString(1, bloodStorageCenter);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				check = true;
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			check = false;
		}

		return check;
	}

	
}
