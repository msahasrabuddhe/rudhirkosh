package rudhira.DAO;

import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONObject;

import rudhira.form.UserForm;

/**
 * @author Kovid Bioanalytics
 * 
 */

public interface UserDAOInf {

	/**
	 * 
	 * @param loginForm
	 * @return
	 */
	public String validateUserCredentials(UserForm loginForm);

	/**
	 * 
	 * @param userName
	 * @return
	 */
	public UserForm getUserDetails(String userName);

	/**
	 * 
	 * @return
	 */
	public HashMap<Integer, String> retrieveBloodBankList();

	/**
	 * 
	 * @param username
	 * @return
	 */
	public int verifyUsernameExist(String username);

	/**
	 * 
	 * @param userForm
	 * @return
	 */
	public String inserUserDetails(UserForm userForm);

	/**
	 * 
	 * @return
	 */
	public List<UserForm> retrieveUserList();

	/**
	 * 
	 * @param userID
	 * @return
	 */
	public List<UserForm> retrieveEditUserListByUserID(int userID);

	/**
	 * 
	 * @param userForm
	 * @return
	 */
	public String updateUserDetails(UserForm userForm);

	/**
	 * 
	 * @param username
	 * @return
	 */
	public List<UserForm> searchUser(String username);

	/**
	 * 
	 * @param userID
	 * @return
	 */
	public String disableUser(int userID);

	/**
	 * 
	 * @param userID
	 * @param password
	 * @return
	 */
	public String insertPasswordHistory(int userID, String password);

	/**
	 * 
	 * @param userID
	 * @param password
	 * @return
	 */
	public boolean verifyPassword(int userID, String password);

	/**
	 * 
	 * @param userID
	 * @param PIN
	 * @return
	 */
	public boolean verifyPINChange(int userID, String PIN);

	/**
	 * 
	 * @param userID
	 * @param password
	 * @return
	 */
	public boolean verifyPasswordHistory(int userID, String password);

	/**
	 * 
	 * @param bloodBankID
	 * @return
	 */
	public String retrieveBloodBankName(int bloodBankID);

	/**
	 * 
	 * @param userName
	 * @return
	 */
	public int retrieveUserIDByUsername(String userName);

	/**
	 * 
	 * @param userForm
	 * @return
	 */
	public String insertBloodBank(UserForm userForm);

	/**
	 * loginDAOInf
	 * 
	 * @param userForm
	 * @return
	 */
	public String updateBloodBank(UserForm userForm);

	/**
	 * 
	 * @param bloodBankName
	 * @return
	 */
	public int verifyBloodBankNameExists(String bloodBankName);

	/**
	 * 
	 * @param bloodBankName
	 * @return
	 */
	public List<UserForm> searchBloodBank(String bloodBankName);

	/**
	 * 
	 * @return
	 */
	public List<UserForm> retrieveEditBloodBankList();

	/**
	 * 
	 * @param bloodBankID
	 * @return
	 */
	public List<UserForm> retrieveEditBloodBankListByID(int bloodBankID);

	/**
	 * 
	 * @param bloodBankName
	 * @param bloodBankID
	 * @return
	 */
	public int verifyBloodBankNameExists(String bloodBankName, int bloodBankID);

	/**
	 * 
	 * @param bloodBankID
	 * @return
	 */
	public String retrieveBloodBankLogo(int bloodBankID);

	/**
	 * 
	 * @param PIN
	 * @return
	 */
	public JSONObject verifyUserPIN(String PIN, int userID);

	/**
	 * 
	 * @param oldPassword
	 * @param userID
	 * @return
	 */
	public JSONObject verifyUserOldPassword(String oldPassword, int userID);

	/**
	 * 
	 * @param oldPassword
	 * @param newPass
	 * @param confirmNewPass
	 * @param newPIN
	 * @param confirmNewPIN
	 * @param userID
	 * @return
	 */
	public JSONObject updateUserSecurityCredentials(String newPass, String newPIN, int userID);

	/**
	 * 
	 * @param userID
	 * @param IPAddress
	 * @param action
	 * @return
	 */
	public String insertAuditDetails(int userID, String IPAddress, String action);

	/**
	 * 
	 * @param concessionType
	 * @return
	 */
	public List<UserForm> searchConcession(String concessionType, int bloodBankID);

	/**
	 * 
	 * @param bloodBankID
	 * @return
	 */
	public List<UserForm> retrieveConcession(int bloodBankID);

	/**
	 * 
	 * @param userForm
	 * @return
	 */
	public String insertConcession(UserForm userForm, int bloodBankID);

	/**
	 * 
	 * @param concessionID
	 * @return
	 */
	public List<UserForm> retrieveEditConcessionList(int concessionID, String searchConcessionName);

	/**
	 * 
	 * @param userForm
	 * @return
	 */
	public String updateConcession(UserForm userForm, int bloodBankID);

	/**
	 * 
	 * @param concessionID
	 * @return
	 */
	public String deleteConcession(int concessionID);

	/**
	 * 
	 * @param chargeType
	 * @return
	 */
	public List<UserForm> searchChargeType(String chargeType, int bloodBankID);

	/**
	 * 
	 * @param bloodBankID
	 * @return
	 */
	public List<UserForm> retrieveChargeType(int bloodBankID);

	/**
	 * 
	 * @param userForm
	 * @return
	 */
	public String insertChargeType(UserForm userForm, int bloodBankID);

	/**
	 * 
	 * @param chargeTypeID
	 * @param searchChargeTypeName
	 * @return
	 */
	public List<UserForm> retrieveEditChargeTypeList(int chargeTypeID, String searchChargeTypeName);

	/**
	 * 
	 * @param userForm
	 * @return
	 */
	public String updateChargeType(UserForm userForm, int bloodBankID);

	/**
	 * 
	 * @param chargeTypeID
	 * @return
	 */
	public String deleteChargeTypeID(int chargeTypeID);

	/**
	 * 
	 * @param chargeType
	 * @param bloodBankID
	 * @return
	 */
	public List<UserForm> searchComponents(String components, int bloodBankID);

	/**
	 * 
	 * @param bloodBankID
	 * @return
	 */
	public List<UserForm> retrieveComponents(int bloodBankID);

	/**
	 * 
	 * @param userForm
	 * @param bloodBankID
	 * @return
	 */
	public String insertComponents(UserForm userForm, int bloodBankID);

	/**
	 * 
	 * @param chargeTypeID
	 * @param searchChargeTypeName
	 * @return
	 */
	public List<UserForm> retrieveEditComponentsList(int componentsID, String searchComponentName);

	/**
	 * 
	 * @param userForm
	 * @param bloodBankID
	 * @return
	 */
	public String updateComponents(UserForm userForm, int bloodBankID);

	/**
	 * 
	 * @param chargeTypeID
	 * @return
	 */
	public String deleteComponents(int chargeTypeID);

	/**
	 * 
	 * @param chargeType
	 * @param bloodBankID
	 * @return
	 */
	public List<UserForm> searchInstructions(String instructions, int bloodBankID);

	/**
	 * 
	 * @param bloodBankID
	 * @return
	 */
	public List<UserForm> retrieveInstructions(int bloodBankID);

	/**
	 * 
	 * @param userForm
	 * @param bloodBankID
	 * @return
	 */
	public String insertInstructions(UserForm userForm, int bloodBankID);

	/**
	 * 
	 * @param chargeTypeID
	 * @param searchChargeTypeName
	 * @return
	 */
	public List<UserForm> retrieveEditInstructionsList(int instructionID, String searchInstructionName);

	/**
	 * 
	 * @param userForm
	 * @param bloodBankID
	 * @return
	 */
	public String updateInstructions(UserForm userForm, int bloodBankID);

	/**
	 * 
	 * @param chargeTypeID
	 * @return
	 */
	public String deleteInstructions(int instructionID);

	/**
	 * 
	 * @param bloodBankID
	 * @return
	 */
	public String retrieveBloodBankRegNo(int bloodBankID);

	/**
	 * 
	 * @param bloodBankID
	 * @return
	 */
	public String retrieveBloodBankPhone(int bloodBankID);

	/**
	 * 
	 * @param bloodBankID
	 * @return
	 */
	public String retrieveBloodBankAddress(int bloodBankID);

	/**
	 * 
	 * @param userID
	 * @return
	 */
	public String retrieveProfilePicName(int userID);

	/**
	 * 
	 * @param shiftName
	 * @param bloodBankID
	 * @return
	 */
	public List<UserForm> searchShifts(String shiftName, int bloodBankID);

	/**
	 * 
	 * @param bloodBankID
	 * @return
	 */
	public List<UserForm> retrieveShifts(int bloodBankID);

	/**
	 * 
	 * @param userForm
	 * @param bloodBankID
	 * @return
	 */
	public String insertShift(UserForm userForm, int bloodBankID);

	/**
	 * 
	 * @param shiftID
	 * @param searchShiftName
	 * @return
	 */
	public List<UserForm> retrieveEditShiftsList(int shiftID, String searchShiftName);

	/**
	 * 
	 * @param userForm
	 * @param bloodBankID
	 * @return
	 */
	public String updateShift(UserForm userForm, int bloodBankID);

	/**
	 * 
	 * @param shiftID
	 * @return
	 */
	public String deleteShifts(int shiftID);

	/**
	 * 
	 * @return
	 */
	public int verifyOpenRegister();

	/**
	 * 
	 * @param userID
	 * @return
	 */
	public HashMap<Integer, String> retrieveReceiptionistUser(int userID);

	/**
	 * 
	 * @return
	 */
	public int verifyYesterdaysOpenRegister();

	/**
	 * 
	 * @return
	 */
	public double retrieveExpectedCash();

	/**
	 * 
	 * @return
	 */
	public int verifyFirstRegister();

	/**
	 * 
	 * @return
	 */
	public double retrieveOtherProductCash();

}
