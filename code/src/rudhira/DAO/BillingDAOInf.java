package rudhira.DAO;

import java.util.List;

import org.json.simple.JSONObject;

import rudhira.form.BillingForm;

/**
 * 
 * @author Kovid Bioanalytics
 * 
 */
public interface BillingDAOInf {

	/**
	 * 
	 * @param bloodBankID
	 * @return
	 */
	public List<String> retrieveComponentList(int bloodBankID);

	/**
	 * 
	 * @param bloodBankID
	 * @return
	 */
	public List<String> retrieveConcessionList(int bloodBankID);

	/**
	 * 
	 * @param bloodBankID
	 * @return
	 */
	public List<String> retrieveOtherChargesList(int bloodBankID);

	/**
	 * 
	 * @param component
	 * @return
	 */
	public JSONObject retrieveRateByComponent(String component, int bloodBankID);

	/**
	 * 
	 * @param concession
	 * @param bloodBankID
	 * @return
	 */
	public JSONObject retrieveRateByConcession(String concession, int bloodBankID);

	/**
	 * 
	 * @param chargeType
	 * @param bloodBankID
	 * @return
	 */
	public JSONObject retrieveRateByChargeType(String chargeType, int bloodBankID);

	/**
	 * 
	 * @return
	 */
	public double retrieveCrossMatchingRate();

	/**
	 * 
	 * @return
	 */
	public double retrieveABDScreeningRate();

	/**
	 * 
	 * @return
	 */
	public int retrieveLastReceiptNo();

	/**
	 * 
	 * @param bdrNo
	 * @return
	 */
	public int retrievePatientIDOnVerifyBDRNo(String bdrNo);

	/**
	 * 
	 * @param billingForm
	 * @return
	 */
	public String insertPatientDetails(BillingForm billingForm);

	/**
	 * 
	 * @param patientIDNo
	 * @return
	 */
	public int retrievePatientIDBasedOnIndentifierNo(String patientIDNo);

	/**
	 * 
	 * @param billingForm
	 * @param userID
	 * @param patientID
	 * @return
	 */
	public String insertReceiptDetails(BillingForm billingForm, int userID, int patientID);

	/**
	 * 
	 * @param receiptNo
	 * @return
	 */
	public int retrieveReceiptIDBasedOnReceiptNo(String receiptNo);

	/**
	 * 
	 * @param chargeType
	 * @param otherCharge
	 * @param receiptID
	 * @return
	 */
	public String insertOtherCharge(String chargeType, double otherCharge, int receiptID);

	/**
	 * 
	 * @param quantity
	 * @param product
	 * @param rate
	 * @param amount
	 * @param receiptID
	 * @return
	 */
	public String insertReceiptItems(int quantity, String product, double rate, double amount, int receiptID);

	/**
	 * 
	 * @return
	 */
	public int retrieveLastEneteredReceiptItemID();

	/**
	 * 
	 * @param receiptItemID
	 * @param bagNo
	 * @return
	 */
	public String insertBagDetails(int receiptItemID, String bagNo);

	/**
	 * 
	 * @param receiptBy
	 * @return
	 */
	public String retrieveReceiptByName(int receiptBy);

	/**
	 * 
	 * @param bdrNo
	 * @return
	 */
	public JSONObject retrievePatientDetailsByBDRNo(int bdrNo);

	/**
	 * 
	 * @param patientIDNo
	 * @return
	 */
	public int retrievePatientIDOnVerifyPatientCode(String patientIDNo);

	/**
	 * 
	 * @param receiptType
	 * @param bloodBankID
	 * @return
	 */
	public JSONObject retrieveReceiptNoByReceiptType(String receiptType, int bloodBankID);

	/**
	 * 
	 * @param receiptType
	 * @param bloodBankID
	 * @return
	 */
	public String retrieveReceiptNoForCourier(String receiptType, int bloodBankID);

	/**
	 * 
	 * @param billingForm
	 * @return
	 */
	public String insertChequeDetails(BillingForm billingForm);

	/**
	 * 
	 * @param receiptItemID
	 * @return
	 */
	public String retrieveBagNoByReceiptItemID(int receiptItemID);

	/**
	 * 
	 * @param searchPatientName
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<BillingForm> searchBillByPatientName(String searchPatientName, String startDate, String endDate);

	/**
	 * 
	 * @param starTDate
	 * @param endDate
	 * @return
	 */
	public List<BillingForm> retrieveAllReceiptDetails(String starTDate, String endDate);

	/**
	 * 
	 * @param startDate
	 * @param endDate
	 * @param userID
	 * @return
	 */
	public List<BillingForm> retrieveMyReceiptDetails(String startDate, String endDate, int userID);

	/**
	 * 
	 * @return
	 */
	public List<BillingForm> retrieveReceipt(int receiptID, int patientID);

	/**
	 * 
	 * @param receiptID
	 * @return
	 */
	public List<BillingForm> retrieveComponent(int receiptID);

	/**
	 * 
	 * @param receiptID
	 * @return
	 */
	public List<BillingForm> retrieveComponentForAdminUser(int receiptID);

	/**
	 * 
	 * @param receiptID
	 * @return
	 */
	public List<BillingForm> retrieveOtherCharges(int receiptID);

	/**
	 * 
	 * @param receiptID
	 * @return
	 */
	public String retrievePaymenyType(int receiptID);

	/**
	 * 
	 * @param receiptID
	 * @return
	 */
	public int andScreeningAndCrsMatchingCheck(int receiptID);

	/**
	 * 
	 * @param receiptID
	 * @return
	 */
	public List<BillingForm> retrieveCrossMatchingList(int receiptID);

	/**
	 * 
	 * @param receiptID
	 * @return
	 */
	public List<BillingForm> retrieveABDScreeningList(int receiptID);

	/**
	 * 
	 * @param receiptID
	 * @return
	 */
	public String retrieveConcessionType(int receiptID);

	/**
	 * 
	 * @param billingForm
	 * @return
	 */
	public String updateReceiptDetails(BillingForm billingForm, int userID);

	/**
	 * 
	 * @param receiptID
	 * @return
	 */
	public boolean verifyChequeExists(int receiptID);

	/**
	 * 
	 * @param billingForm
	 * @return
	 */
	public String updateChequeDetails(BillingForm billingForm);

	/**
	 * 
	 * @param receiptID
	 * @return
	 */
	public String retrieveReceiptType(int receiptID);

	/**
	 * 
	 * @param componentID
	 * @return
	 */
	public JSONObject deleteProduct(int componentID);

	/**
	 * 
	 * @param componentID
	 * @return
	 */
	public JSONObject retrieveBagDetails(int componentID);

	/**
	 * 
	 * @param shiftName
	 * @return
	 */
	public JSONObject retrieveShiftTime(String shiftName);

	/**
	 * 
	 * @param bloodBankID
	 * @return
	 */
	public List<String> retrieveShiftName(int bloodBankID);

	/**
	 * 
	 * @param billingForm
	 * @param userID
	 * @return
	 */
	public String insertRegister(BillingForm billingForm, int userID);

	/**
	 * 
	 * @return
	 */
	public JSONObject retrieveCashDeposited();

	/**
	 * 
	 * @param cashDeposited
	 * @return
	 */
	public String updateCashDeposited(double cashDeposited);

	/**
	 * 
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public double retrieveAdvancePaymentSum(String startTime, String endTime);

	/**
	 * 
	 * @return
	 */
	public double retrieveCashHandover();

	/**
	 * 
	 * @param registerBalance
	 * @param cashHandoverTo
	 * @return
	 */
	public String updateRegisterBalance(double registerBalance, int cashHandoverTo);

	/**
	 * 
	 * @param totalBill
	 * @return
	 */
	public String updateTotalBill(double totalBill);

	/**
	 * 
	 * @return
	 */
	public double retrieveCashdeposited();

	/**
	 * 
	 * @return
	 */
	public double retrieveOtherProductCash();

	/**
	 * 
	 * @return
	 */
	public String retrieveRegisterShiftTime();

	/**
	 * 
	 * @param billingForm
	 * @return
	 */
	public String insertOtherProduct(BillingForm billingForm);

	/**
	 * 
	 * @return
	 */
	public double retrieveTodaysOtherProductAmount();

	/**
	 * 
	 * @param otherProductAmount
	 * @return
	 */
	public String updateOtherProductIntoRegister(double otherProductAmount);

	/**
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public JSONObject retrieveBalanceDetails(String startDate, String endDate);

	/**
	 * 
	 * @param bloodBagNo
	 * @return
	 */
	public JSONObject retrieveBloodGroup(String bloodBagNo);

	/**
	 * 
	 * @param shiftOrder
	 * @return
	 */
	public JSONObject verifyLastRegisterOpen(int shiftOrder);

	/**
	 * 
	 * @param shiftOrder
	 * @return
	 */
	public String retrieveRegisterShiftTime(int shiftOrder);

	/**
	 * 
	 * @param startTime
	 * @param endTime
	 * @param shiftOrder
	 * @return
	 */
	public double retrieveAdvancePaymentSum(String startTime, String endTime, int shiftOrder);

	/**
	 * 
	 * @param shiftOrder
	 * @return
	 */
	public double retrieveCashHandover(int shiftOrder);

	/**
	 * 
	 * @param shiftOrder
	 * @return
	 */
	public double retrieveCashdeposited(int shiftOrder);

	/**
	 * 
	 * @param shiftOrder
	 * @return
	 */
	public double retrieveOtherProductCash(int shiftOrder);

	/**
	 * 
	 * @param searchOtherProductName
	 * @return
	 */
	public List<BillingForm> searchOtherProducts(String searchOtherProductName);

	/**
	 * 
	 * @return
	 */
	public List<BillingForm> retrieveAllOtherProcucts();

	/**
	 * 
	 * @param bbStorageCenter
	 * @return
	 */
	public int retrievePatientIdByBloodBankStorageCenter(String bbStorageCenter);

	/**
	 * 
	 * @param patientID
	 * @return
	 */
	public JSONObject retireveReceiptDetailsByPatientID(int patientID);

	/**
	 * 
	 * @param startCash
	 * @return
	 */
	public JSONObject insertFirstRegisterDetails(double startCash);

	/**
	 * 
	 * @param registerID
	 * @param userID
	 * @param billingForm
	 * @return
	 */
	public String udpatedFirstRegister(int registerID, int userID, BillingForm billingForm);

	/**
	 * 
	 * @return
	 */
	public double retrieveLastCashMismatchCarryOver();

	/**
	 * 
	 * @return
	 */
	public JSONObject retrievePreviousExpectedCash();

	/**
	 * 
	 * @return
	 */
	public JSONObject retrieveBankDepositeList();

	/**
	 * 
	 * @return
	 */
	public JSONObject retrieveCashAdjustmentList();

	/**
	 * 
	 * @param cashDepID
	 * @return
	 */
	public JSONObject retrieveCashDepositeByID(int cashDepID);

	/**
	 * 
	 * @param cashDepID
	 * @return
	 */
	public JSONObject deleteCashDepositeByID(int cashDepID);

	/**
	 * 
	 * @param cashAdjID
	 * @return
	 */
	public JSONObject retrieveCashAdjustedByID(int cashAdjID);

	/**
	 * 
	 * @param cashAdjID
	 * @return
	 */
	public JSONObject deleteCashAdjustedByID(int cashAdjID);

	/**
	 * 
	 * @param billingForm
	 * @param userID
	 * @param registerID
	 * @return
	 */
	public JSONObject insertCashAdjustment(BillingForm billingForm, int userID, int registerID);

	/**
	 * 
	 * @return
	 */
	public int retrieveRegisterID();

	/**
	 * 
	 * @param billingForm
	 * @param userID
	 * @return
	 */
	public JSONObject updateCashAdjustment(BillingForm billingForm, int userID);

	/**
	 * 
	 * @param billingForm
	 * @param userID
	 * @param registerID
	 * @return
	 */
	public JSONObject insertCashDeposit(BillingForm billingForm, int userID, int registerID);

	/**
	 * 
	 * @param billingForm
	 * @param userID
	 * @return
	 */
	public JSONObject updateCashDeposit(BillingForm billingForm, int userID);

	/**
	 * 
	 * @param registerID
	 * @return
	 */
	public String retrieveRegisterStartDateAndTime(int registerID);

	/**
	 * 
	 * @param registerStartDateAndTime
	 * @return
	 */
	public double retrieveBloodBankBalance(String registerStartDateAndTime);

	/**
	 * 
	 * @param registerStartDateAndTime
	 * @return
	 */
	public double retrieveOtherProductBalance(String registerStartDateAndTime);

	/**
	 * 
	 * @param registerID
	 * @return
	 */
	public double retrieveTotalCashDepositedByRegisterID(int registerID);

	/**
	 * 
	 * @param registerID
	 * @return
	 */
	public double retrieveTotalCashAdjustedByRegisterID(int registerID);

	/**
	 * 
	 * @param registerID
	 * @return
	 */
	public double retrieveCashHandoverByRegisterID(int registerID);

	/**
	 * 
	 * @param billingForm
	 * @return
	 */
	public String updateRegister(BillingForm billingForm);

	/**
	 * 
	 * @return
	 */
	public List<String> retrieveBloodStorageNameList();

	/**
	 * 
	 * @param bscName
	 * @return
	 */
	public String insertBloodStorageName(String bscName);

	/**
	 * 
	 * @param bloodStorageCenter
	 * @return
	 */
	public boolean verifyBloodStorageExists(String bloodStorageCenter);

}
