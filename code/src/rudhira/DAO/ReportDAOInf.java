package rudhira.DAO;

import java.util.List;

import rudhira.form.BillingForm;
import rudhira.form.ReportForm;

/**
 * 
 * @author Kovid Bioanalytics
 * 
 */
public interface ReportDAOInf {

	/**
	 * 
	 * @param searchReportName
	 * @param searchCriteria
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<ReportForm> retrievePatientReportList(String searchReportName, String searchCriteria, String startDate,
			String endDate);

	/**
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<ReportForm> retrieveCashHandoverReportList(String startDate, String endDate);

	/**
	 * 
	 * @param startDate
	 * @param endDate
	 * @param searchComponentName
	 * @return
	 */
	public List<ReportForm> retrieveComponentReportList(String startDate, String endDate, String searchComponentName);

	/**
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<ReportForm> retrieveRegiterReportList(String startDate, String endDate);

	/**
	 * 
	 * @param bloodBankID
	 * @return
	 */
	public List<String> retrieveConcessionTypeList(int bloodBankID);

	/**
	 * 
	 * @param startDate
	 * @param endDate
	 * @param searchConcessionName
	 * @return
	 */
	public List<ReportForm> retrieveConcessionReportList(String startDate, String endDate, String searchConcessionName);

	/**
	 * 
	 * @param startDate
	 * @param endDate
	 * @param searchComponentName
	 * @return
	 */
	public List<ReportForm> retrieveCardChequeReportList(String startDate, String endDate, String searchComponentName);

	/**
	 * 
	 * 
	 * @param receiptID
	 * @return
	 */
	public BillingForm retrieveChequeCardDetails(int receiptID);

	/**
	 * 
	 * @return
	 */
	public List<ReportForm> retrievebloodStorageCenterList();

}
