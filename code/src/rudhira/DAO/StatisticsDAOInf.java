package rudhira.DAO;

/**
 * 
 * @author Kovid Bioanalytics
 * 
 */
public interface StatisticsDAOInf {

	/**
	 * 
	 * @param bloodBankID
	 * @return
	 */
	public int countActiveAdministrator(int bloodBankID);

	/**
	 * 
	 * @param bloodBankID
	 * @return
	 */
	public int countActiveAccountants(int bloodBankID);

	/**
	 * 
	 * @return
	 */
	public int countTotalPatients();

	/**
	 * 
	 * @param bloodBankID
	 * @return
	 */
	public int countTotalReceipts(int bloodBankID);

	/**
	 * 
	 * @param bloodBankID
	 * @return
	 */
	public int countTotalComponents(int bloodBankID);

	/**
	 * 
	 * @param bloodBankID
	 * @return
	 */
	public int countTotalCharges(int bloodBankID);

	/**
	 * 
	 * @return
	 */
	public int countWeeklyTotalPatients();

	/**
	 * 
	 * @param bloodBankID
	 * @return
	 */
	public int countWeeklyTotalReceipt(int bloodBankID);

	/**
	 * 
	 * @param bloodBankID
	 * @return
	 */
	public int countWeeklyTotalReceiptPendingPayment(int bloodBankID);

	/**
	 * 
	 * @param bloodBankID
	 * @return
	 */
	public int countWeeklyTotalCrossMatching(int bloodBankID);

	/**
	 * 
	 * @param bloodBankID
	 * @return
	 */
	public int countWeeklyTotalABDScreening(int bloodBankID);

	/**
	 * 
	 * @param bloodBankID
	 * @return
	 */
	public int countWeeklyTotalBilling(int bloodBankID);

	/**
	 * 
	 * @param userID
	 * @return
	 */
	public int countWeeklyTotalLogin(int userID);

	/**
	 * 
	 * @param userID
	 * @return
	 */
	public int countWeeklyTotalMyReceipt(int userID);

	/**
	 * 
	 * @param userID
	 * @return
	 */
	public int countWeeklyTotalMyBilling(int userID);

	/**
	 * 
	 * @param userID
	 * @return
	 */
	public String retrieveLastLogin(int userID);

	/**
	 * 
	 * @param userID
	 * @return
	 */
	public String retrieverLastPasswordChange(int userID);

	/**
	 * 
	 * @param userID
	 * @return
	 */
	public String retrieveLastPINChange(int userID);

}
