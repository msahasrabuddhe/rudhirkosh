package rudhira.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;

import rudhira.util.ActivityStatus;
import rudhira.util.DBConnection;
import rudhira.util.QueryMaker;

/**
 * 
 * @author Kovid Bioanalytics
 * 
 */
public class StatisticsDAOImpl extends DBConnection implements StatisticsDAOInf {

	Connection connection = null;
	PreparedStatement preparedStatement = null;
	ResultSet resultSet = null;

	String message = "error";

	public int countActiveAdministrator(int bloodBankID) {

		int count = 0;

		try {

			connection = getConnection();

			String countActiveAdministratorQuery = QueryMaker.COUNT_TOTAL_ACTIVE_ADMINISTRATOR;

			preparedStatement = connection.prepareStatement(countActiveAdministratorQuery);

			preparedStatement.setString(1, "administrator");
			preparedStatement.setString(2, ActivityStatus.ACTIVE);
			preparedStatement.setInt(3, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				count = resultSet.getInt("count");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return count;
	}

	public int countActiveAccountants(int bloodBankID) {

		int count = 0;

		try {

			connection = getConnection();

			String countActiveAccountantsQuery = QueryMaker.COUNT_TOTAL_ACTIVE_ACCOUNTANTS;

			preparedStatement = connection.prepareStatement(countActiveAccountantsQuery);

			preparedStatement.setString(1, "receptionist");
			preparedStatement.setString(2, ActivityStatus.ACTIVE);
			preparedStatement.setInt(3, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				count = resultSet.getInt("count");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return count;
	}

	public int countTotalPatients() {

		int count = 0;

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");

		try {

			connection = getConnection();

			String countTotalPatientsQuery = QueryMaker.COUNT_TOTAL_PATIENTS;

			preparedStatement = connection.prepareStatement(countTotalPatientsQuery);

			int nextYear = Integer.parseInt(simpleDateFormat.format(new Date())) + 1;

			preparedStatement.setString(1, simpleDateFormat.format(new Date()) + "-04-01 00:00:00");
			preparedStatement.setString(2, "" + nextYear + "-03-31 23:59");

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				count = resultSet.getInt("count");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return count;
	}

	public int countTotalReceipts(int bloodBankID) {

		int count = 0;

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");

		try {

			connection = getConnection();

			String countTotalReceiptsQuery = QueryMaker.COUNT_TOTAL_RECEIPTS;

			preparedStatement = connection.prepareStatement(countTotalReceiptsQuery);

			int nextYear = Integer.parseInt(simpleDateFormat.format(new Date())) + 1;

			preparedStatement.setString(1, simpleDateFormat.format(new Date()) + "-04-01 00:00:00");
			preparedStatement.setString(2, "" + nextYear + "-03-31 23:59");
			preparedStatement.setInt(3, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				count = resultSet.getInt("count");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return count;
	}

	public int countTotalComponents(int bloodBankID) {

		int count = 0;

		try {

			connection = getConnection();

			String countTotalComponentsQuery = QueryMaker.COUNT_TOTAL_COMPONENTS;

			preparedStatement = connection.prepareStatement(countTotalComponentsQuery);

			preparedStatement.setInt(1, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				count = resultSet.getInt("count");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return count;
	}

	public int countTotalCharges(int bloodBankID) {

		int count = 0;

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");

		try {

			connection = getConnection();

			String countTotalChargesQuery = QueryMaker.COUNT_TOTAL_CHARGES;

			preparedStatement = connection.prepareStatement(countTotalChargesQuery);

			int nextYear = Integer.parseInt(simpleDateFormat.format(new Date())) + 1;

			preparedStatement.setString(1, simpleDateFormat.format(new Date()) + "-04-01 00:00:00");
			preparedStatement.setString(2, "" + nextYear + "-03-31 23:59");
			preparedStatement.setInt(3, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				count = resultSet.getInt("count");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return count;
	}

	public int countWeeklyTotalPatients() {

		int count = 0;

		try {

			connection = getConnection();

			String countWeeklyTotalPatientsQuery = QueryMaker.COUNT_WEEKLY_TOTAL_PATIENTS;

			preparedStatement = connection.prepareStatement(countWeeklyTotalPatientsQuery);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				count = resultSet.getInt("count");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return count;
	}

	public int countWeeklyTotalReceipt(int bloodBankID) {

		int count = 0;

		try {

			connection = getConnection();

			String countWeeklyTotalReceiptQuery = QueryMaker.COUNT_WEEKLY_TOTAL_RECEIPTS;

			preparedStatement = connection.prepareStatement(countWeeklyTotalReceiptQuery);

			preparedStatement.setInt(1, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				count = resultSet.getInt("count");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return count;
	}

	public int countWeeklyTotalReceiptPendingPayment(int bloodBankID) {

		int count = 0;

		try {

			connection = getConnection();

			String countWeeklyTotalReceiptPendingPaymentQuery = QueryMaker.COUNT_WEEKLY_RECEIPT_PENDING_PAYMENT;

			preparedStatement = connection.prepareStatement(countWeeklyTotalReceiptPendingPaymentQuery);

			preparedStatement.setInt(1, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				count = resultSet.getInt("count");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return count;
	}

	public int countWeeklyTotalCrossMatching(int bloodBankID) {

		int count = 0;

		try {

			connection = getConnection();

			String countWeeklyTotalCrossMatchingQuery = QueryMaker.COUNT_TOTAL_CROSS_MATCHING;

			preparedStatement = connection.prepareStatement(countWeeklyTotalCrossMatchingQuery);

			preparedStatement.setInt(1, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				count = resultSet.getInt("count");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return count;
	}

	public int countWeeklyTotalABDScreening(int bloodBankID) {

		int count = 0;

		try {

			connection = getConnection();

			String countWeeklyTotalABDScreeningQuery = QueryMaker.COUNT_TOTAL_ABD_SCREENING;

			preparedStatement = connection.prepareStatement(countWeeklyTotalABDScreeningQuery);

			preparedStatement.setInt(1, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				count = resultSet.getInt("count");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return count;
	}

	public int countWeeklyTotalBilling(int bloodBankID) {

		int count = 0;

		try {

			connection = getConnection();

			String countWeeklyTotalBillingQuery = QueryMaker.COUNT_WEEKLY_TOTAL_BILLING;

			preparedStatement = connection.prepareStatement(countWeeklyTotalBillingQuery);

			preparedStatement.setInt(1, bloodBankID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				count = resultSet.getInt("count");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return count;
	}

	public int countWeeklyTotalLogin(int userID) {

		int count = 0;

		try {

			connection = getConnection();

			String countWeeklyTotalLoginQuery = QueryMaker.COUNT_MY_WEEKLY_TOTAL_LOGINS;

			preparedStatement = connection.prepareStatement(countWeeklyTotalLoginQuery);

			preparedStatement.setInt(1, userID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				count = resultSet.getInt("count");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return count;
	}

	public int countWeeklyTotalMyReceipt(int userID) {

		int count = 0;

		try {

			connection = getConnection();

			String countWeeklyTotalMyReceiptQuery = QueryMaker.COUNT_MY_WEEKLY_TOTAL_RECEIPT;

			preparedStatement = connection.prepareStatement(countWeeklyTotalMyReceiptQuery);

			preparedStatement.setInt(1, userID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				count = resultSet.getInt("count");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return count;
	}

	public int countWeeklyTotalMyBilling(int userID) {

		int count = 0;

		try {

			connection = getConnection();

			String countWeeklyTotalMyBillingQuery = QueryMaker.COUNT_MY_WEEKLY_TOTAL_BILLING;

			preparedStatement = connection.prepareStatement(countWeeklyTotalMyBillingQuery);

			preparedStatement.setInt(1, userID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				count = resultSet.getInt("count");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return count;
	}

	public String retrieveLastLogin(int userID) {

		String loginDate = null;

		Date date = new Date();

		SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-MM-DD HH:mm:SS");

		try {

			connection = getConnection();

			String retrieveLastLoginQuery = QueryMaker.RETRIEVE_LAST_LOGIN_DATE_TIME;

			preparedStatement = connection.prepareStatement(retrieveLastLoginQuery);

			preparedStatement.setString(1, "Login");
			preparedStatement.setInt(2, userID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				loginDate = resultSet.getString("timeStampLog");

				String[] array = loginDate.split("\\.");

				loginDate = array[0];
			}

			/*
			 * If loginDate is null then set current date time as loginDate
			 */
			if (loginDate == null || loginDate == "") {

				loginDate = dateFormat.format(date);

			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return loginDate;
	}

	public String retrieverLastPasswordChange(int userID) {

		String loginDate = "";

		try {

			connection = getConnection();

			String retrieverLastPasswordChangeQuery = QueryMaker.RETRIEVE_LAST_PASSWORD_CHANGE_DATE_TIME;

			preparedStatement = connection.prepareStatement(retrieverLastPasswordChangeQuery);

			preparedStatement.setString(1, "Password changed");
			preparedStatement.setInt(2, userID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				loginDate = resultSet.getString("timeStampLog");

				String[] array = loginDate.split("\\.");

				loginDate = array[0];
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return loginDate;
	}

	public String retrieveLastPINChange(int userID) {

		String loginDate = "";

		try {

			connection = getConnection();

			String retrieveLastPINChangeQuery = QueryMaker.RETRIEVE_LAST_PIN_CHANGE_DATE_TIME;

			preparedStatement = connection.prepareStatement(retrieveLastPINChangeQuery);

			preparedStatement.setString(1, "PIN changed");
			preparedStatement.setInt(2, userID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				loginDate = resultSet.getString("timeStampLog");

				String[] array = loginDate.split("\\.");

				loginDate = array[0];
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return loginDate;
	}

}
