package rudhira.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import rudhira.form.BillingForm;
import rudhira.util.DBConnection;
import rudhira.util.QueryMaker;

public class ListnerDAOImpl extends DBConnection implements ListnerDAOInf {

	String message = "error";

	Connection connection = null;
	PreparedStatement preparedStatement = null;
	ResultSet resultSet = null;

	public BillingForm retrieveChequeCardDetails(String realPath, int receiptID) {

		BillingForm billingForm = null;

		try {
			connection = getConnection(realPath);

			String retrieveChequeCardDetailsQuery = QueryMaker.RETRIEVE_CHEQUE_DETAILS;

			preparedStatement = connection.prepareStatement(retrieveChequeCardDetailsQuery);

			preparedStatement.setInt(1, receiptID);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				billingForm = new BillingForm();

				billingForm.setChequeBankName(resultSet.getString("bankName"));
				billingForm.setChequeAmt(resultSet.getDouble("chequeAmt"));
				billingForm.setChequeBankBranch(resultSet.getString("bankBranch"));
				billingForm.setChequeIssuedBy(resultSet.getString("chequeIssuedBy"));
				billingForm.setChequeNo(resultSet.getString("chequeNumber"));
				billingForm.setCardMobileNo(resultSet.getString("mobileNo"));
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return billingForm;
	}
}
