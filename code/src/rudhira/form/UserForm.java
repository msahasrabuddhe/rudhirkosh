package rudhira.form;

import java.io.File;

/**
 * 
 * @author Kovid Bioanalytics
 * 
 */
public class UserForm {

	private String username;
	private String password;
	private int userID;
	private String firstName;
	private String middleName;
	private String lastName;
	private String activityStatus;
	private String email;
	private int bloodBankID;
	private String userRole;
	private String fullname;
	private String phone;
	private String address;
	private String city;
	private String state;
	private String country;
	private String searchUserName;
	private File profilePic;
	private String profilePicFileName;
	private String profilePicDBName;
	private String PIN;
	private String bloodBankName;
	private String bloodBankRegNo;
	private File bloodBankLogo;
	private String bloodBankLogoFileName;
	private String bloodBankAddress;
	private String bloodBankPhone1;
	private String bloodBankPhone2;
	private String bloodBankEmail;
	private String bloodBankWebsite;
	private String bloodBankTagline;
	private String searchBloodBankName;
	private String lockPIN;
	private String oldPassword;
	private String newPassword;
	private String confirmNewPassword;
	private String newPIN;
	private String confirmNewPIN;
	private String searchConcessionName;
	private int concessionID;
	private String concessionType;
	private double concessionAmt;
	private String searchChargeName;
	private int chargeID;
	private String chargeType;
	private double charge;
	private String searchComponentName;
	private int componentID;
	private String component;
	private String componentCategory;
	private String componentRequirement;
	private double componentPrice;
	private int instructionID;
	private String instruction;
	private String searchInstruction;
	private String componentListStrig;
	private String searchShiftName;
	private int shiftID;
	private String shiftName;
	private String shiftStartTimeHH;
	private String shiftStartTimeMM;
	private String shiftStratTimeAMPM;
	private String shiftEndTimeHH;
	private String shiftEndTimeMM;
	private String shiftEndTimeAMPM;
	private String shiftStartTime;
	private String shiftEndTime;
	private int shiftOrder;

	/**
	 * @return the shiftOrder
	 */
	public int getShiftOrder() {
		return shiftOrder;
	}

	/**
	 * @param shiftOrder
	 *            the shiftOrder to set
	 */
	public void setShiftOrder(int shiftOrder) {
		this.shiftOrder = shiftOrder;
	}

	/**
	 * @return the searchShiftName
	 */
	public String getSearchShiftName() {
		return searchShiftName;
	}

	/**
	 * @param searchShiftName
	 *            the searchShiftName to set
	 */
	public void setSearchShiftName(String searchShiftName) {
		this.searchShiftName = searchShiftName;
	}

	/**
	 * @return the shiftID
	 */
	public int getShiftID() {
		return shiftID;
	}

	/**
	 * @param shiftID
	 *            the shiftID to set
	 */
	public void setShiftID(int shiftID) {
		this.shiftID = shiftID;
	}

	/**
	 * @return the shiftName
	 */
	public String getShiftName() {
		return shiftName;
	}

	/**
	 * @param shiftName
	 *            the shiftName to set
	 */
	public void setShiftName(String shiftName) {
		this.shiftName = shiftName;
	}

	/**
	 * @return the shiftStartTimeHH
	 */
	public String getShiftStartTimeHH() {
		return shiftStartTimeHH;
	}

	/**
	 * @param shiftStartTimeHH
	 *            the shiftStartTimeHH to set
	 */
	public void setShiftStartTimeHH(String shiftStartTimeHH) {
		this.shiftStartTimeHH = shiftStartTimeHH;
	}

	/**
	 * @return the shiftStartTimeMM
	 */
	public String getShiftStartTimeMM() {
		return shiftStartTimeMM;
	}

	/**
	 * @param shiftStartTimeMM
	 *            the shiftStartTimeMM to set
	 */
	public void setShiftStartTimeMM(String shiftStartTimeMM) {
		this.shiftStartTimeMM = shiftStartTimeMM;
	}

	/**
	 * @return the shiftStratTimeAMPM
	 */
	public String getShiftStratTimeAMPM() {
		return shiftStratTimeAMPM;
	}

	/**
	 * @param shiftStratTimeAMPM
	 *            the shiftStratTimeAMPM to set
	 */
	public void setShiftStratTimeAMPM(String shiftStratTimeAMPM) {
		this.shiftStratTimeAMPM = shiftStratTimeAMPM;
	}

	/**
	 * @return the shiftEndTimeHH
	 */
	public String getShiftEndTimeHH() {
		return shiftEndTimeHH;
	}

	/**
	 * @param shiftEndTimeHH
	 *            the shiftEndTimeHH to set
	 */
	public void setShiftEndTimeHH(String shiftEndTimeHH) {
		this.shiftEndTimeHH = shiftEndTimeHH;
	}

	/**
	 * @return the shiftEndTimeMM
	 */
	public String getShiftEndTimeMM() {
		return shiftEndTimeMM;
	}

	/**
	 * @param shiftEndTimeMM
	 *            the shiftEndTimeMM to set
	 */
	public void setShiftEndTimeMM(String shiftEndTimeMM) {
		this.shiftEndTimeMM = shiftEndTimeMM;
	}

	/**
	 * @return the shiftEndTimeAMPM
	 */
	public String getShiftEndTimeAMPM() {
		return shiftEndTimeAMPM;
	}

	/**
	 * @param shiftEndTimeAMPM
	 *            the shiftEndTimeAMPM to set
	 */
	public void setShiftEndTimeAMPM(String shiftEndTimeAMPM) {
		this.shiftEndTimeAMPM = shiftEndTimeAMPM;
	}

	/**
	 * @return the shiftStartTime
	 */
	public String getShiftStartTime() {
		return shiftStartTime;
	}

	/**
	 * @param shiftStartTime
	 *            the shiftStartTime to set
	 */
	public void setShiftStartTime(String shiftStartTime) {
		this.shiftStartTime = shiftStartTime;
	}

	/**
	 * @return the shiftEndTime
	 */
	public String getShiftEndTime() {
		return shiftEndTime;
	}

	/**
	 * @param shiftEndTime
	 *            the shiftEndTime to set
	 */
	public void setShiftEndTime(String shiftEndTime) {
		this.shiftEndTime = shiftEndTime;
	}

	/**
	 * @return the componentListStrig
	 */
	public String getComponentListStrig() {
		return componentListStrig;
	}

	/**
	 * @param componentListStrig
	 *            the componentListStrig to set
	 */
	public void setComponentListStrig(String componentListStrig) {
		this.componentListStrig = componentListStrig;
	}

	/**
	 * @return the instructionID
	 */
	public int getInstructionID() {
		return instructionID;
	}

	/**
	 * @param instructionID
	 *            the instructionID to set
	 */
	public void setInstructionID(int instructionID) {
		this.instructionID = instructionID;
	}

	/**
	 * @return the instruction
	 */
	public String getInstruction() {
		return instruction;
	}

	/**
	 * @param instruction
	 *            the instruction to set
	 */
	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}

	/**
	 * @return the searchInstruction
	 */
	public String getSearchInstruction() {
		return searchInstruction;
	}

	/**
	 * @param searchInstruction
	 *            the searchInstruction to set
	 */
	public void setSearchInstruction(String searchInstruction) {
		this.searchInstruction = searchInstruction;
	}

	/**
	 * @return the searchComponentName
	 */
	public String getSearchComponentName() {
		return searchComponentName;
	}

	/**
	 * @param searchComponentName
	 *            the searchComponentName to set
	 */
	public void setSearchComponentName(String searchComponentName) {
		this.searchComponentName = searchComponentName;
	}

	/**
	 * @return the componentID
	 */
	public int getComponentID() {
		return componentID;
	}

	/**
	 * @param componentID
	 *            the componentID to set
	 */
	public void setComponentID(int componentID) {
		this.componentID = componentID;
	}

	/**
	 * @return the component
	 */
	public String getComponent() {
		return component;
	}

	/**
	 * @param component
	 *            the component to set
	 */
	public void setComponent(String component) {
		this.component = component;
	}

	/**
	 * @return the componentCategory
	 */
	public String getComponentCategory() {
		return componentCategory;
	}

	/**
	 * @param componentCategory
	 *            the componentCategory to set
	 */
	public void setComponentCategory(String componentCategory) {
		this.componentCategory = componentCategory;
	}

	/**
	 * @return the componentRequirement
	 */
	public String getComponentRequirement() {
		return componentRequirement;
	}

	/**
	 * @param componentRequirement
	 *            the componentRequirement to set
	 */
	public void setComponentRequirement(String componentRequirement) {
		this.componentRequirement = componentRequirement;
	}

	/**
	 * @return the componentPrice
	 */
	public double getComponentPrice() {
		return componentPrice;
	}

	/**
	 * @param componentPrice
	 *            the componentPrice to set
	 */
	public void setComponentPrice(double componentPrice) {
		this.componentPrice = componentPrice;
	}

	/**
	 * @return the searchChargeName
	 */
	public String getSearchChargeName() {
		return searchChargeName;
	}

	/**
	 * @param searchChargeName
	 *            the searchChargeName to set
	 */
	public void setSearchChargeName(String searchChargeName) {
		this.searchChargeName = searchChargeName;
	}

	/**
	 * @return the chargeID
	 */
	public int getChargeID() {
		return chargeID;
	}

	/**
	 * @param chargeID
	 *            the chargeID to set
	 */
	public void setChargeID(int chargeID) {
		this.chargeID = chargeID;
	}

	/**
	 * @return the chargeType
	 */
	public String getChargeType() {
		return chargeType;
	}

	/**
	 * @param chargeType
	 *            the chargeType to set
	 */
	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	/**
	 * @return the charge
	 */
	public double getCharge() {
		return charge;
	}

	/**
	 * @param charge
	 *            the charge to set
	 */
	public void setCharge(double charge) {
		this.charge = charge;
	}

	/**
	 * @return the concessionID
	 */
	public int getConcessionID() {
		return concessionID;
	}

	/**
	 * @param concessionID
	 *            the concessionID to set
	 */
	public void setConcessionID(int concessionID) {
		this.concessionID = concessionID;
	}

	/**
	 * @return the concessionType
	 */
	public String getConcessionType() {
		return concessionType;
	}

	/**
	 * @param concessionType
	 *            the concessionType to set
	 */
	public void setConcessionType(String concessionType) {
		this.concessionType = concessionType;
	}

	/**
	 * @return the concessionAmt
	 */
	public double getConcessionAmt() {
		return concessionAmt;
	}

	/**
	 * @param concessionAmt
	 *            the concessionAmt to set
	 */
	public void setConcessionAmt(double concessionAmt) {
		this.concessionAmt = concessionAmt;
	}

	/**
	 * @return the searchConcessionName
	 */
	public String getSearchConcessionName() {
		return searchConcessionName;
	}

	/**
	 * @param searchConcessionName
	 *            the searchConcessionName to set
	 */
	public void setSearchConcessionName(String searchConcessionName) {
		this.searchConcessionName = searchConcessionName;
	}

	/**
	 * @return the oldPassword
	 */
	public String getOldPassword() {
		return oldPassword;
	}

	/**
	 * @param oldPassword
	 *            the oldPassword to set
	 */
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	/**
	 * @return the newPassword
	 */
	public String getNewPassword() {
		return newPassword;
	}

	/**
	 * @param newPassword
	 *            the newPassword to set
	 */
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	/**
	 * @return the confirmNewPassword
	 */
	public String getConfirmNewPassword() {
		return confirmNewPassword;
	}

	/**
	 * @param confirmNewPassword
	 *            the confirmNewPassword to set
	 */
	public void setConfirmNewPassword(String confirmNewPassword) {
		this.confirmNewPassword = confirmNewPassword;
	}

	/**
	 * @return the newPIN
	 */
	public String getNewPIN() {
		return newPIN;
	}

	/**
	 * @param newPIN
	 *            the newPIN to set
	 */
	public void setNewPIN(String newPIN) {
		this.newPIN = newPIN;
	}

	/**
	 * @return the confirmNewPIN
	 */
	public String getConfirmNewPIN() {
		return confirmNewPIN;
	}

	/**
	 * @param confirmNewPIN
	 *            the confirmNewPIN to set
	 */
	public void setConfirmNewPIN(String confirmNewPIN) {
		this.confirmNewPIN = confirmNewPIN;
	}

	/**
	 * @return the lockPIN
	 */
	public String getLockPIN() {
		return lockPIN;
	}

	/**
	 * @param lockPIN
	 *            the lockPIN to set
	 */
	public void setLockPIN(String lockPIN) {
		this.lockPIN = lockPIN;
	}

	/**
	 * @return the searchBloodBankName
	 */
	public String getSearchBloodBankName() {
		return searchBloodBankName;
	}

	/**
	 * @param searchBloodBankName
	 *            the searchBloodBankName to set
	 */
	public void setSearchBloodBankName(String searchBloodBankName) {
		this.searchBloodBankName = searchBloodBankName;
	}

	/**
	 * @return the bloodBankName
	 */
	public String getBloodBankName() {
		return bloodBankName;
	}

	/**
	 * @param bloodBankName
	 *            the bloodBankName to set
	 */
	public void setBloodBankName(String bloodBankName) {
		this.bloodBankName = bloodBankName;
	}

	/**
	 * @return the bloodBankRegNo
	 */
	public String getBloodBankRegNo() {
		return bloodBankRegNo;
	}

	/**
	 * @param bloodBankRegNo
	 *            the bloodBankRegNo to set
	 */
	public void setBloodBankRegNo(String bloodBankRegNo) {
		this.bloodBankRegNo = bloodBankRegNo;
	}

	/**
	 * @return the bloodBankLogo
	 */
	public File getBloodBankLogo() {
		return bloodBankLogo;
	}

	/**
	 * @param bloodBankLogo
	 *            the bloodBankLogo to set
	 */
	public void setBloodBankLogo(File bloodBankLogo) {
		this.bloodBankLogo = bloodBankLogo;
	}

	/**
	 * @return the bloodBankLogoFileName
	 */
	public String getBloodBankLogoFileName() {
		return bloodBankLogoFileName;
	}

	/**
	 * @param bloodBankLogoFileName
	 *            the bloodBankLogoFileName to set
	 */
	public void setBloodBankLogoFileName(String bloodBankLogoFileName) {
		this.bloodBankLogoFileName = bloodBankLogoFileName;
	}

	/**
	 * @return the bloodBankAddress
	 */
	public String getBloodBankAddress() {
		return bloodBankAddress;
	}

	/**
	 * @param bloodBankAddress
	 *            the bloodBankAddress to set
	 */
	public void setBloodBankAddress(String bloodBankAddress) {
		this.bloodBankAddress = bloodBankAddress;
	}

	/**
	 * @return the bloodBankPhone1
	 */
	public String getBloodBankPhone1() {
		return bloodBankPhone1;
	}

	/**
	 * @param bloodBankPhone1
	 *            the bloodBankPhone1 to set
	 */
	public void setBloodBankPhone1(String bloodBankPhone1) {
		this.bloodBankPhone1 = bloodBankPhone1;
	}

	/**
	 * @return the bloodBankPhone2
	 */
	public String getBloodBankPhone2() {
		return bloodBankPhone2;
	}

	/**
	 * @param bloodBankPhone2
	 *            the bloodBankPhone2 to set
	 */
	public void setBloodBankPhone2(String bloodBankPhone2) {
		this.bloodBankPhone2 = bloodBankPhone2;
	}

	/**
	 * @return the bloodBankEmail
	 */
	public String getBloodBankEmail() {
		return bloodBankEmail;
	}

	/**
	 * @param bloodBankEmail
	 *            the bloodBankEmail to set
	 */
	public void setBloodBankEmail(String bloodBankEmail) {
		this.bloodBankEmail = bloodBankEmail;
	}

	/**
	 * @return the bloodBankWebsite
	 */
	public String getBloodBankWebsite() {
		return bloodBankWebsite;
	}

	/**
	 * @param bloodBankWebsite
	 *            the bloodBankWebsite to set
	 */
	public void setBloodBankWebsite(String bloodBankWebsite) {
		this.bloodBankWebsite = bloodBankWebsite;
	}

	/**
	 * @return the bloodBankTagline
	 */
	public String getBloodBankTagline() {
		return bloodBankTagline;
	}

	/**
	 * @param bloodBankTagline
	 *            the bloodBankTagline to set
	 */
	public void setBloodBankTagline(String bloodBankTagline) {
		this.bloodBankTagline = bloodBankTagline;
	}

	/**
	 * @return the profilePicDBName
	 */
	public String getProfilePicDBName() {
		return profilePicDBName;
	}

	/**
	 * @return the pIN
	 */
	public String getPIN() {
		return PIN;
	}

	/**
	 * @param pIN
	 *            the pIN to set
	 */
	public void setPIN(String pIN) {
		PIN = pIN;
	}

	/**
	 * @param profilePicDBName
	 *            the profilePicDBName to set
	 */
	public void setProfilePicDBName(String profilePicDBName) {
		this.profilePicDBName = profilePicDBName;
	}

	/**
	 * @return the profilePicFileName
	 */
	public String getProfilePicFileName() {
		return profilePicFileName;
	}

	/**
	 * @param profilePicFileName
	 *            the profilePicFileName to set
	 */
	public void setProfilePicFileName(String profilePicFileName) {
		this.profilePicFileName = profilePicFileName;
	}

	/**
	 * @return the profilePic
	 */
	public File getProfilePic() {
		return profilePic;
	}

	/**
	 * @param profilePic
	 *            the profilePic to set
	 */
	public void setProfilePic(File profilePic) {
		this.profilePic = profilePic;
	}

	/**
	 * @return the searchUserName
	 */
	public String getSearchUserName() {
		return searchUserName;
	}

	/**
	 * @param searchUserName
	 *            the searchUserName to set
	 */
	public void setSearchUserName(String searchUserName) {
		this.searchUserName = searchUserName;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone
	 *            the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the fullname
	 */
	public String getFullname() {
		return fullname;
	}

	/**
	 * @param fullname
	 *            the fullname to set
	 */
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @param middleName
	 *            the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the activityStatus
	 */
	public String getActivityStatus() {
		return activityStatus;
	}

	/**
	 * @param activityStatus
	 *            the activityStatus to set
	 */
	public void setActivityStatus(String activityStatus) {
		this.activityStatus = activityStatus;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the bloodBankID
	 */
	public int getBloodBankID() {
		return bloodBankID;
	}

	/**
	 * @param bloodBankID
	 *            the bloodBankID to set
	 */
	public void setBloodBankID(int bloodBankID) {
		this.bloodBankID = bloodBankID;
	}

	/**
	 * @return the userRole
	 */
	public String getUserRole() {
		return userRole;
	}

	/**
	 * @param userRole
	 *            the userRole to set
	 */
	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	/**
	 * @return the userID
	 */
	public int getUserID() {
		return userID;
	}

	/**
	 * @param userID
	 *            the userID to set
	 */
	public void setUserID(int userID) {
		this.userID = userID;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

}
