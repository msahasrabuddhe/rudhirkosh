package rudhira.form;

/**
 * 
 * @author Kovid Bioanalytics
 * 
 */
public class BillingForm {

	private int bloodBankID;
	private int receiptID;
	private int patientID;
	private int userID;
	private String patientName;
	private String firstName;
	private String middleName;
	private String lastName;
	private String hospital;
	private String address;
	private String BBRNo;
	private String barCode;
	private String crossMatchBagNo;
	private int crossMatchQuantity;
	private double crossMatchRate;
	private double crossMatchAmount;
	private String abcScreeningBagNo;
	private int abcScreeningQuantity;
	private double abcScreeningRate;
	private double abcScreeningAmount;
	private String[] chargeType;
	private String[] component;
	private String[] bagNo;
	private String[] quantity;
	private String[] rate;
	private String[] amount;
	private double totalAmount;
	private String[] otherCharge;
	private String concessionType;
	private double concessionAmount;
	private double netReceivableAmt;
	private double actualPayment;
	private double oustandingAmt;
	private String refReceiptNo;
	private String outstandingPaidDate;
	private double outstandingPaidAmt;
	private String receiptNo;
	private String patientIDNo;
	private String receiptType;
	private String paymentType;
	private double tdsAmt;
	private String chequeIssuedBy;
	private String chequeNo;
	private String chequeBankName;
	private String chequeBankBranch;
	private String chequeDate;
	private int chequeID;
	private double chequeAmt;
	private String searchBillName;
	private String receiptDate;
	public int srNo;
	private String componentName;
	private String bagNoName;
	private int quantityName;
	private double rateName;
	private double amountName;
	private String chargeTypeName;
	private double otherChargeName;
	private String receiptByName;
	private int componentID;
	private int otherChargeID;
	private String patientReportCriteria;
	private String searchPatientReportName;
	private String bbStorageCenter;
	private String charityCaseNo;
	private double cashHandover;
	private double cashDeposited;
	private double totalCash;
	private double registerBalance;
	private String registerStartTime;
	private String registerEndTime;
	private String registerDate;
	private int registerID;
	private String shiftName;
	private int cashHandoverToID;
	private double otherProductAmount;
	private String otherProduct;
	private String otherProductTransactionDate;
	private String startDate;
	private String endDate;
	private String bloodBagNo;
	private int shiftOrder;
	private String searchOtherProductName;
	private String otherProductAddedByName;
	private String storageBBRNo;
	private String manualReceiptNo;
	private String cardMobileNo;
	private double expectedCash;
	private double bloodBankBalance;
	private double otherProductBalance;
	private double actualCash;
	private double cashMatchingError;
	private double startCash;
	private int firstRegisterCheck;
	private double cashMismatchCarryOver;
	private double bankDeposite;
	private int cashDepID;
	private int cashAdjID;
	private double cashAdjusted;
	private String comment;
	private String reason;
	private double totalOtherCharge;
	private String mobile;

	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @param mobile
	 *            the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @return the totalOtherCharge
	 */
	public double getTotalOtherCharge() {
		return totalOtherCharge;
	}

	/**
	 * @param totalOtherCharge
	 *            the totalOtherCharge to set
	 */
	public void setTotalOtherCharge(double totalOtherCharge) {
		this.totalOtherCharge = totalOtherCharge;
	}

	/**
	 * @return the cashDepID
	 */
	public int getCashDepID() {
		return cashDepID;
	}

	/**
	 * @param cashDepID
	 *            the cashDepID to set
	 */
	public void setCashDepID(int cashDepID) {
		this.cashDepID = cashDepID;
	}

	/**
	 * @return the cashAdjID
	 */
	public int getCashAdjID() {
		return cashAdjID;
	}

	/**
	 * @param cashAdjID
	 *            the cashAdjID to set
	 */
	public void setCashAdjID(int cashAdjID) {
		this.cashAdjID = cashAdjID;
	}

	/**
	 * @return the cashAdjusted
	 */
	public double getCashAdjusted() {
		return cashAdjusted;
	}

	/**
	 * @param cashAdjusted
	 *            the cashAdjusted to set
	 */
	public void setCashAdjusted(double cashAdjusted) {
		this.cashAdjusted = cashAdjusted;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment
	 *            the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * @param reason
	 *            the reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * @return the bankDeposite
	 */
	public double getBankDeposite() {
		return bankDeposite;
	}

	/**
	 * @param bankDeposite
	 *            the bankDeposite to set
	 */
	public void setBankDeposite(double bankDeposite) {
		this.bankDeposite = bankDeposite;
	}

	/**
	 * @return the cashMismatchCarryOver
	 */
	public double getCashMismatchCarryOver() {
		return cashMismatchCarryOver;
	}

	/**
	 * @param cashMismatchCarryOver
	 *            the cashMismatchCarryOver to set
	 */
	public void setCashMismatchCarryOver(double cashMismatchCarryOver) {
		this.cashMismatchCarryOver = cashMismatchCarryOver;
	}

	/**
	 * @return the firstRegisterCheck
	 */
	public int getFirstRegisterCheck() {
		return firstRegisterCheck;
	}

	/**
	 * @param firstRegisterCheck
	 *            the firstRegisterCheck to set
	 */
	public void setFirstRegisterCheck(int firstRegisterCheck) {
		this.firstRegisterCheck = firstRegisterCheck;
	}

	/**
	 * @return the startCash
	 */
	public double getStartCash() {
		return startCash;
	}

	/**
	 * @param startCash
	 *            the startCash to set
	 */
	public void setStartCash(double startCash) {
		this.startCash = startCash;
	}

	/**
	 * @return the expectedCash
	 */
	public double getExpectedCash() {
		return expectedCash;
	}

	/**
	 * @param expectedCash
	 *            the expectedCash to set
	 */
	public void setExpectedCash(double expectedCash) {
		this.expectedCash = expectedCash;
	}

	/**
	 * @return the bloodBankBalance
	 */
	public double getBloodBankBalance() {
		return bloodBankBalance;
	}

	/**
	 * @param bloodBankBalance
	 *            the bloodBankBalance to set
	 */
	public void setBloodBankBalance(double bloodBankBalance) {
		this.bloodBankBalance = bloodBankBalance;
	}

	/**
	 * @return the otherProductBalance
	 */
	public double getOtherProductBalance() {
		return otherProductBalance;
	}

	/**
	 * @param otherProductBalance
	 *            the otherProductBalance to set
	 */
	public void setOtherProductBalance(double otherProductBalance) {
		this.otherProductBalance = otherProductBalance;
	}

	/**
	 * @return the actualCash
	 */
	public double getActualCash() {
		return actualCash;
	}

	/**
	 * @param actualCash
	 *            the actualCash to set
	 */
	public void setActualCash(double actualCash) {
		this.actualCash = actualCash;
	}

	/**
	 * @return the cashMatchingError
	 */
	public double getCashMatchingError() {
		return cashMatchingError;
	}

	/**
	 * @param cashMatchingError
	 *            the cashMatchingError to set
	 */
	public void setCashMatchingError(double cashMatchingError) {
		this.cashMatchingError = cashMatchingError;
	}

	/**
	 * @return the cardMobileNo
	 */
	public String getCardMobileNo() {
		return cardMobileNo;
	}

	/**
	 * @param cardMobileNo
	 *            the cardMobileNo to set
	 */
	public void setCardMobileNo(String cardMobileNo) {
		this.cardMobileNo = cardMobileNo;
	}

	/**
	 * @return the manualReceiptNo
	 */
	public String getManualReceiptNo() {
		return manualReceiptNo;
	}

	/**
	 * @param manualReceiptNo
	 *            the manualReceiptNo to set
	 */
	public void setManualReceiptNo(String manualReceiptNo) {
		this.manualReceiptNo = manualReceiptNo;
	}

	/**
	 * @return the storageBBRNo
	 */
	public String getStorageBBRNo() {
		return storageBBRNo;
	}

	/**
	 * @param storageBBRNo
	 *            the storageBBRNo to set
	 */
	public void setStorageBBRNo(String storageBBRNo) {
		this.storageBBRNo = storageBBRNo;
	}

	/**
	 * @return the otherProductAddedByName
	 */
	public String getOtherProductAddedByName() {
		return otherProductAddedByName;
	}

	/**
	 * @param otherProductAddedByName
	 *            the otherProductAddedByName to set
	 */
	public void setOtherProductAddedByName(String otherProductAddedByName) {
		this.otherProductAddedByName = otherProductAddedByName;
	}

	/**
	 * @return the searchOtherProductName
	 */
	public String getSearchOtherProductName() {
		return searchOtherProductName;
	}

	/**
	 * @param searchOtherProductName
	 *            the searchOtherProductName to set
	 */
	public void setSearchOtherProductName(String searchOtherProductName) {
		this.searchOtherProductName = searchOtherProductName;
	}

	/**
	 * @return the shiftOrder
	 */
	public int getShiftOrder() {
		return shiftOrder;
	}

	/**
	 * @param shiftOrder
	 *            the shiftOrder to set
	 */
	public void setShiftOrder(int shiftOrder) {
		this.shiftOrder = shiftOrder;
	}

	/**
	 * @return the bloodBagNo
	 */
	public String getBloodBagNo() {
		return bloodBagNo;
	}

	/**
	 * @param bloodBagNo
	 *            the bloodBagNo to set
	 */
	public void setBloodBagNo(String bloodBagNo) {
		this.bloodBagNo = bloodBagNo;
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the otherProductAmount
	 */
	public double getOtherProductAmount() {
		return otherProductAmount;
	}

	/**
	 * @param otherProductAmount
	 *            the otherProductAmount to set
	 */
	public void setOtherProductAmount(double otherProductAmount) {
		this.otherProductAmount = otherProductAmount;
	}

	/**
	 * @return the otherProduct
	 */
	public String getOtherProduct() {
		return otherProduct;
	}

	/**
	 * @param otherProduct
	 *            the otherProduct to set
	 */
	public void setOtherProduct(String otherProduct) {
		this.otherProduct = otherProduct;
	}

	/**
	 * @return the otherProductTransactionDate
	 */
	public String getOtherProductTransactionDate() {
		return otherProductTransactionDate;
	}

	/**
	 * @param otherProductTransactionDate
	 *            the otherProductTransactionDate to set
	 */
	public void setOtherProductTransactionDate(String otherProductTransactionDate) {
		this.otherProductTransactionDate = otherProductTransactionDate;
	}

	/**
	 * @return the cashHandoverToID
	 */
	public int getCashHandoverToID() {
		return cashHandoverToID;
	}

	/**
	 * @param cashHandoverToID
	 *            the cashHandoverToID to set
	 */
	public void setCashHandoverToID(int cashHandoverToID) {
		this.cashHandoverToID = cashHandoverToID;
	}

	/**
	 * @return the shiftName
	 */
	public String getShiftName() {
		return shiftName;
	}

	/**
	 * @param shiftName
	 *            the shiftName to set
	 */
	public void setShiftName(String shiftName) {
		this.shiftName = shiftName;
	}

	/**
	 * @return the cashHandover
	 */
	public double getCashHandover() {
		return cashHandover;
	}

	/**
	 * @param cashHandover
	 *            the cashHandover to set
	 */
	public void setCashHandover(double cashHandover) {
		this.cashHandover = cashHandover;
	}

	/**
	 * @return the cashDeposited
	 */
	public double getCashDeposited() {
		return cashDeposited;
	}

	/**
	 * @param cashDeposited
	 *            the cashDeposited to set
	 */
	public void setCashDeposited(double cashDeposited) {
		this.cashDeposited = cashDeposited;
	}

	/**
	 * @return the totalCash
	 */
	public double getTotalCash() {
		return totalCash;
	}

	/**
	 * @param totalCash
	 *            the totalCash to set
	 */
	public void setTotalCash(double totalCash) {
		this.totalCash = totalCash;
	}

	/**
	 * @return the registerBalance
	 */
	public double getRegisterBalance() {
		return registerBalance;
	}

	/**
	 * @param registerBalance
	 *            the registerBalance to set
	 */
	public void setRegisterBalance(double registerBalance) {
		this.registerBalance = registerBalance;
	}

	/**
	 * @return the registerStartTime
	 */
	public String getRegisterStartTime() {
		return registerStartTime;
	}

	/**
	 * @param registerStartTime
	 *            the registerStartTime to set
	 */
	public void setRegisterStartTime(String registerStartTime) {
		this.registerStartTime = registerStartTime;
	}

	/**
	 * @return the registerEndTime
	 */
	public String getRegisterEndTime() {
		return registerEndTime;
	}

	/**
	 * @param registerEndTime
	 *            the registerEndTime to set
	 */
	public void setRegisterEndTime(String registerEndTime) {
		this.registerEndTime = registerEndTime;
	}

	/**
	 * @return the registerDate
	 */
	public String getRegisterDate() {
		return registerDate;
	}

	/**
	 * @param registerDate
	 *            the registerDate to set
	 */
	public void setRegisterDate(String registerDate) {
		this.registerDate = registerDate;
	}

	/**
	 * @return the registerID
	 */
	public int getRegisterID() {
		return registerID;
	}

	/**
	 * @param registerID
	 *            the registerID to set
	 */
	public void setRegisterID(int registerID) {
		this.registerID = registerID;
	}

	/**
	 * @return the charityCaseNo
	 */
	public String getCharityCaseNo() {
		return charityCaseNo;
	}

	/**
	 * @param charityCaseNo
	 *            the charityCaseNo to set
	 */
	public void setCharityCaseNo(String charityCaseNo) {
		this.charityCaseNo = charityCaseNo;
	}

	/**
	 * @return the bbStorageCenter
	 */
	public String getBbStorageCenter() {
		return bbStorageCenter;
	}

	/**
	 * @param bbStorageCenter
	 *            the bbStorageCenter to set
	 */
	public void setBbStorageCenter(String bbStorageCenter) {
		this.bbStorageCenter = bbStorageCenter;
	}

	/**
	 * @return the patientReportCriteria
	 */
	public String getPatientReportCriteria() {
		return patientReportCriteria;
	}

	/**
	 * @param patientReportCriteria
	 *            the patientReportCriteria to set
	 */
	public void setPatientReportCriteria(String patientReportCriteria) {
		this.patientReportCriteria = patientReportCriteria;
	}

	/**
	 * @return the searchPatientReportName
	 */
	public String getSearchPatientReportName() {
		return searchPatientReportName;
	}

	/**
	 * @param searchPatientReportName
	 *            the searchPatientReportName to set
	 */
	public void setSearchPatientReportName(String searchPatientReportName) {
		this.searchPatientReportName = searchPatientReportName;
	}

	/**
	 * @return the componentID
	 */
	public int getComponentID() {
		return componentID;
	}

	/**
	 * @param componentID
	 *            the componentID to set
	 */
	public void setComponentID(int componentID) {
		this.componentID = componentID;
	}

	/**
	 * @return the otherChargeID
	 */
	public int getOtherChargeID() {
		return otherChargeID;
	}

	/**
	 * @param otherChargeID
	 *            the otherChargeID to set
	 */
	public void setOtherChargeID(int otherChargeID) {
		this.otherChargeID = otherChargeID;
	}

	/**
	 * @return the receiptByName
	 */
	public String getReceiptByName() {
		return receiptByName;
	}

	/**
	 * @param receiptByName
	 *            the receiptByName to set
	 */
	public void setReceiptByName(String receiptByName) {
		this.receiptByName = receiptByName;
	}

	/**
	 * @return the componentName
	 */
	public String getComponentName() {
		return componentName;
	}

	/**
	 * @param componentName
	 *            the componentName to set
	 */
	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public String getBagNoName() {
		return bagNoName;
	}

	public void setBagNoName(String bagNoName) {
		this.bagNoName = bagNoName;
	}

	/**
	 * @return the quantityName
	 */
	public int getQuantityName() {
		return quantityName;
	}

	/**
	 * @param quantityName
	 *            the quantityName to set
	 */
	public void setQuantityName(int quantityName) {
		this.quantityName = quantityName;
	}

	/**
	 * @return the rateName
	 */
	public double getRateName() {
		return rateName;
	}

	/**
	 * @param rateName
	 *            the rateName to set
	 */
	public void setRateName(double rateName) {
		this.rateName = rateName;
	}

	/**
	 * @return the amountName
	 */
	public double getAmountName() {
		return amountName;
	}

	/**
	 * @param amountName
	 *            the amountName to set
	 */
	public void setAmountName(double amountName) {
		this.amountName = amountName;
	}

	/**
	 * @return the chargeTypeName
	 */
	public String getChargeTypeName() {
		return chargeTypeName;
	}

	/**
	 * @param chargeTypeName
	 *            the chargeTypeName to set
	 */
	public void setChargeTypeName(String chargeTypeName) {
		this.chargeTypeName = chargeTypeName;
	}

	/**
	 * @return the otherChargeName
	 */
	public double getOtherChargeName() {
		return otherChargeName;
	}

	/**
	 * @param otherChargeName
	 *            the otherChargeName to set
	 */
	public void setOtherChargeName(double otherChargeName) {
		this.otherChargeName = otherChargeName;
	}

	/**
	 * @return the srNo
	 */
	public int getSrNo() {
		return srNo;
	}

	/**
	 * @param srNo
	 *            the srNo to set
	 */
	public void setSrNo(int srNo) {
		this.srNo = srNo;
	}

	/**
	 * @return the receiptDate
	 */
	public String getReceiptDate() {
		return receiptDate;
	}

	/**
	 * @param receiptDate
	 *            the receiptDate to set
	 */
	public void setReceiptDate(String receiptDate) {
		this.receiptDate = receiptDate;
	}

	/**
	 * @return the searchBillName
	 */
	public String getSearchBillName() {
		return searchBillName;
	}

	/**
	 * @param searchBillName
	 *            the searchBillName to set
	 */
	public void setSearchBillName(String searchBillName) {
		this.searchBillName = searchBillName;
	}

	/**
	 * @return the chequeIssuedBy
	 */
	public String getChequeIssuedBy() {
		return chequeIssuedBy;
	}

	/**
	 * @param chequeIssuedBy
	 *            the chequeIssuedBy to set
	 */
	public void setChequeIssuedBy(String chequeIssuedBy) {
		this.chequeIssuedBy = chequeIssuedBy;
	}

	/**
	 * @return the chequeNo
	 */
	public String getChequeNo() {
		return chequeNo;
	}

	/**
	 * @param chequeNo
	 *            the chequeNo to set
	 */
	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}

	/**
	 * @return the chequeBankName
	 */
	public String getChequeBankName() {
		return chequeBankName;
	}

	/**
	 * @param chequeBankName
	 *            the chequeBankName to set
	 */
	public void setChequeBankName(String chequeBankName) {
		this.chequeBankName = chequeBankName;
	}

	/**
	 * @return the chequeBankBranch
	 */
	public String getChequeBankBranch() {
		return chequeBankBranch;
	}

	/**
	 * @param chequeBankBranch
	 *            the chequeBankBranch to set
	 */
	public void setChequeBankBranch(String chequeBankBranch) {
		this.chequeBankBranch = chequeBankBranch;
	}

	/**
	 * @return the chequeDate
	 */
	public String getChequeDate() {
		return chequeDate;
	}

	/**
	 * @param chequeDate
	 *            the chequeDate to set
	 */
	public void setChequeDate(String chequeDate) {
		this.chequeDate = chequeDate;
	}

	/**
	 * @return the chequeID
	 */
	public int getChequeID() {
		return chequeID;
	}

	/**
	 * @param chequeID
	 *            the chequeID to set
	 */
	public void setChequeID(int chequeID) {
		this.chequeID = chequeID;
	}

	/**
	 * @return the chequeAmt
	 */
	public double getChequeAmt() {
		return chequeAmt;
	}

	/**
	 * @param chequeAmt
	 *            the chequeAmt to set
	 */
	public void setChequeAmt(double chequeAmt) {
		this.chequeAmt = chequeAmt;
	}

	/**
	 * @return the paymentType
	 */
	public String getPaymentType() {
		return paymentType;
	}

	/**
	 * @param paymentType
	 *            the paymentType to set
	 */
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	/**
	 * @return the tdsAmt
	 */
	public double getTdsAmt() {
		return tdsAmt;
	}

	/**
	 * @param tdsAmt
	 *            the tdsAmt to set
	 */
	public void setTdsAmt(double tdsAmt) {
		this.tdsAmt = tdsAmt;
	}

	/**
	 * @return the receiptType
	 */
	public String getReceiptType() {
		return receiptType;
	}

	/**
	 * @param receiptType
	 *            the receiptType to set
	 */
	public void setReceiptType(String receiptType) {
		this.receiptType = receiptType;
	}

	/**
	 * @return the chargeType
	 */
	public String[] getChargeType() {
		return chargeType;
	}

	/**
	 * @param chargeType
	 *            the chargeType to set
	 */
	public void setChargeType(String[] chargeType) {
		this.chargeType = chargeType;
	}

	/**
	 * @return the otherCharge
	 */
	public String[] getOtherCharge() {
		return otherCharge;
	}

	public void setOtherCharge(String[] otherCharge) {
		this.otherCharge = otherCharge;
	}

	/**
	 * @return the abcScreeningBagNo
	 */
	public String getAbcScreeningBagNo() {
		return abcScreeningBagNo;
	}

	/**
	 * @param abcScreeningBagNo
	 *            the abcScreeningBagNo to set
	 */
	public void setAbcScreeningBagNo(String abcScreeningBagNo) {
		this.abcScreeningBagNo = abcScreeningBagNo;
	}

	/**
	 * @return the component
	 */
	public String[] getComponent() {
		return component;
	}

	/**
	 * @param component
	 *            the component to set
	 */
	public void setComponent(String[] component) {
		this.component = component;
	}

	/**
	 * @return the receiptNo
	 */
	public String getReceiptNo() {
		return receiptNo;
	}

	/**
	 * @param receiptNo
	 *            the receiptNo to set
	 */
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	/**
	 * @return the patientIDNo
	 */
	public String getPatientIDNo() {
		return patientIDNo;
	}

	/**
	 * @param patientIDNo
	 *            the patientIDNo to set
	 */
	public void setPatientIDNo(String patientIDNo) {
		this.patientIDNo = patientIDNo;
	}

	/**
	 * @return the barCode
	 */
	public String getBarCode() {
		return barCode;
	}

	/**
	 * @param barCode
	 *            the barCode to set
	 */
	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	/**
	 * @return the bagNo
	 */
	public String[] getBagNo() {
		return bagNo;
	}

	/**
	 * @param bagNo
	 *            the bagNo to set
	 */
	public void setBagNo(String[] bagNo) {
		this.bagNo = bagNo;
	}

	/**
	 * @return the quantity
	 */
	public String[] getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity
	 *            the quantity to set
	 */
	public void setQuantity(String[] quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the rate
	 */
	public String[] getRate() {
		return rate;
	}

	/**
	 * @param rate
	 *            the rate to set
	 */
	public void setRate(String[] rate) {
		this.rate = rate;
	}

	/**
	 * @return the amount
	 */
	public String[] getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(String[] amount) {
		this.amount = amount;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @param middleName
	 *            the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the crossMatchBagNo
	 */
	public String getCrossMatchBagNo() {
		return crossMatchBagNo;
	}

	/**
	 * @param crossMatchBagNo
	 *            the crossMatchBagNo to set
	 */
	public void setCrossMatchBagNo(String crossMatchBagNo) {
		this.crossMatchBagNo = crossMatchBagNo;
	}

	/**
	 * @return the crossMatchQuantity
	 */
	public int getCrossMatchQuantity() {
		return crossMatchQuantity;
	}

	/**
	 * @param crossMatchQuantity
	 *            the crossMatchQuantity to set
	 */
	public void setCrossMatchQuantity(int crossMatchQuantity) {
		this.crossMatchQuantity = crossMatchQuantity;
	}

	/**
	 * @return the crossMatchRate
	 */
	public double getCrossMatchRate() {
		return crossMatchRate;
	}

	/**
	 * @param crossMatchRate
	 *            the crossMatchRate to set
	 */
	public void setCrossMatchRate(double crossMatchRate) {
		this.crossMatchRate = crossMatchRate;
	}

	/**
	 * @return the crossMatchAmount
	 */
	public double getCrossMatchAmount() {
		return crossMatchAmount;
	}

	/**
	 * @param crossMatchAmount
	 *            the crossMatchAmount to set
	 */
	public void setCrossMatchAmount(double crossMatchAmount) {
		this.crossMatchAmount = crossMatchAmount;
	}

	/**
	 * @return the abcScreeningQuantity
	 */
	public int getAbcScreeningQuantity() {
		return abcScreeningQuantity;
	}

	/**
	 * @param abcScreeningQuantity
	 *            the abcScreeningQuantity to set
	 */
	public void setAbcScreeningQuantity(int abcScreeningQuantity) {
		this.abcScreeningQuantity = abcScreeningQuantity;
	}

	/**
	 * @return the abcScreeningRate
	 */
	public double getAbcScreeningRate() {
		return abcScreeningRate;
	}

	/**
	 * @param abcScreeningRate
	 *            the abcScreeningRate to set
	 */
	public void setAbcScreeningRate(double abcScreeningRate) {
		this.abcScreeningRate = abcScreeningRate;
	}

	/**
	 * @return the abcScreeningAmount
	 */
	public double getAbcScreeningAmount() {
		return abcScreeningAmount;
	}

	/**
	 * @param abcScreeningAmount
	 *            the abcScreeningAmount to set
	 */
	public void setAbcScreeningAmount(double abcScreeningAmount) {
		this.abcScreeningAmount = abcScreeningAmount;
	}

	/**
	 * @return the totalAmount
	 */
	public double getTotalAmount() {
		return totalAmount;
	}

	/**
	 * @param totalAmount
	 *            the totalAmount to set
	 */
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	/**
	 * @return the concessionType
	 */
	public String getConcessionType() {
		return concessionType;
	}

	/**
	 * @param concessionType
	 *            the concessionType to set
	 */
	public void setConcessionType(String concessionType) {
		this.concessionType = concessionType;
	}

	/**
	 * @return the concessionAmount
	 */
	public double getConcessionAmount() {
		return concessionAmount;
	}

	/**
	 * @param concessionAmount
	 *            the concessionAmount to set
	 */
	public void setConcessionAmount(double concessionAmount) {
		this.concessionAmount = concessionAmount;
	}

	/**
	 * @return the netReceivableAmt
	 */
	public double getNetReceivableAmt() {
		return netReceivableAmt;
	}

	/**
	 * @param netReceivableAmt
	 *            the netReceivableAmt to set
	 */
	public void setNetReceivableAmt(double netReceivableAmt) {
		this.netReceivableAmt = netReceivableAmt;
	}

	/**
	 * @return the actualPayment
	 */
	public double getActualPayment() {
		return actualPayment;
	}

	/**
	 * @param actualPayment
	 *            the actualPayment to set
	 */
	public void setActualPayment(double actualPayment) {
		this.actualPayment = actualPayment;
	}

	/**
	 * @return the oustandingAmt
	 */
	public double getOustandingAmt() {
		return oustandingAmt;
	}

	/**
	 * @param oustandingAmt
	 *            the oustandingAmt to set
	 */
	public void setOustandingAmt(double oustandingAmt) {
		this.oustandingAmt = oustandingAmt;
	}

	/**
	 * @return the refReceiptNo
	 */
	public String getRefReceiptNo() {
		return refReceiptNo;
	}

	/**
	 * @param refReceiptNo
	 *            the refReceiptNo to set
	 */
	public void setRefReceiptNo(String refReceiptNo) {
		this.refReceiptNo = refReceiptNo;
	}

	/**
	 * @return the outstandingPaidDate
	 */
	public String getOutstandingPaidDate() {
		return outstandingPaidDate;
	}

	/**
	 * @param outstandingPaidDate
	 *            the outstandingPaidDate to set
	 */
	public void setOutstandingPaidDate(String outstandingPaidDate) {
		this.outstandingPaidDate = outstandingPaidDate;
	}

	/**
	 * @return the outstandingPaidAmt
	 */
	public double getOutstandingPaidAmt() {
		return outstandingPaidAmt;
	}

	/**
	 * @param outstandingPaidAmt
	 *            the outstandingPaidAmt to set
	 */
	public void setOutstandingPaidAmt(double outstandingPaidAmt) {
		this.outstandingPaidAmt = outstandingPaidAmt;
	}

	/**
	 * @return the bloodBankID
	 */
	public int getBloodBankID() {
		return bloodBankID;
	}

	/**
	 * @param bloodBankID
	 *            the bloodBankID to set
	 */
	public void setBloodBankID(int bloodBankID) {
		this.bloodBankID = bloodBankID;
	}

	/**
	 * @return the receiptID
	 */
	public int getReceiptID() {
		return receiptID;
	}

	/**
	 * @param receiptID
	 *            the receiptID to set
	 */
	public void setReceiptID(int receiptID) {
		this.receiptID = receiptID;
	}

	/**
	 * @return the patientID
	 */
	public int getPatientID() {
		return patientID;
	}

	/**
	 * @param patientID
	 *            the patientID to set
	 */
	public void setPatientID(int patientID) {
		this.patientID = patientID;
	}

	/**
	 * @return the userID
	 */
	public int getUserID() {
		return userID;
	}

	/**
	 * @param userID
	 *            the userID to set
	 */
	public void setUserID(int userID) {
		this.userID = userID;
	}

	/**
	 * @return the patientName
	 */
	public String getPatientName() {
		return patientName;
	}

	/**
	 * @param patientName
	 *            the patientName to set
	 */
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	/**
	 * @return the hospital
	 */
	public String getHospital() {
		return hospital;
	}

	/**
	 * @param hospital
	 *            the hospital to set
	 */
	public void setHospital(String hospital) {
		this.hospital = hospital;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the bBRNo
	 */
	public String getBBRNo() {
		return BBRNo;
	}

	/**
	 * @param bBRNo
	 *            the bBRNo to set
	 */
	public void setBBRNo(String bBRNo) {
		BBRNo = bBRNo;
	}

}
