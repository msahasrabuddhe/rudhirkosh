package rudhira.form;

import java.io.InputStream;

/**
 * 
 * @author Kovid Bioanalytics
 * 
 */
public class ReportForm {

	private String patientReportCriteria;
	private String searchPatientReportName;
	private int bloodBankID;
	private int receiptID;
	private int patientID;
	private int userID;
	private String patientName;
	private String firstName;
	private String middleName;
	private String lastName;
	private String BBRNo;
	private String receiptNo;
	private String receiptType;
	private String receiptDate;
	private String bagNo;
	private String[] reportPatientID;
	private String[] reportReceiptID;
	private String submitButton;
	private InputStream fileInputStream;
	private String fileName;
	private String startDate;
	private String endDate;
	private double cashHandover;
	private double cashDeposited;
	private double totalCash;
	private double registerBalance;
	private String registerDate;
	private String shift;
	private double otherProductAmount;
	private String searchComponentName;
	private String componentName;
	private double componentRate;
	private String searchExportForAcctName;
	private String paymentType;
	private double netReceivableAmt;
	private double actualPayment;
	private double outstandingAmt;
	private String storageCenter;
	private int srNo;
	private double bloodBankBalance;
	private double otherProductBalance;
	private double expectedCashBalance;
	private double actualCashBalance;
	private double bankDeposited;
	private double cashAdjusted;
	private double cashMismatch;
	private String searchConcessionName;
	private String concessionType;
	private double concessionAmt;
	private String[] reportComponents;
	private int storageCenterID;

	/**
	 * @return the storageCenterID
	 */
	public int getStorageCenterID() {
		return storageCenterID;
	}

	/**
	 * @param storageCenterID the storageCenterID to set
	 */
	public void setStorageCenterID(int storageCenterID) {
		this.storageCenterID = storageCenterID;
	}

	/**
	 * @return the reportComponents
	 */
	public String[] getReportComponents() {
		return reportComponents;
	}

	/**
	 * @param reportComponents the reportComponents to set
	 */
	public void setReportComponents(String[] reportComponents) {
		this.reportComponents = reportComponents;
	}

	/**
	 * @return the concessionType
	 */
	public String getConcessionType() {
		return concessionType;
	}

	/**
	 * @param concessionType the concessionType to set
	 */
	public void setConcessionType(String concessionType) {
		this.concessionType = concessionType;
	}

	/**
	 * @return the concessionAmt
	 */
	public double getConcessionAmt() {
		return concessionAmt;
	}

	/**
	 * @param concessionAmt the concessionAmt to set
	 */
	public void setConcessionAmt(double concessionAmt) {
		this.concessionAmt = concessionAmt;
	}

	/**
	 * @return the searchConcessionName
	 */
	public String getSearchConcessionName() {
		return searchConcessionName;
	}

	/**
	 * @param searchConcessionName the searchConcessionName to set
	 */
	public void setSearchConcessionName(String searchConcessionName) {
		this.searchConcessionName = searchConcessionName;
	}

	/**
	 * @return the cashMismatch
	 */
	public double getCashMismatch() {
		return cashMismatch;
	}

	/**
	 * @param cashMismatch the cashMismatch to set
	 */
	public void setCashMismatch(double cashMismatch) {
		this.cashMismatch = cashMismatch;
	}

	/**
	 * @return the srNo
	 */
	public int getSrNo() {
		return srNo;
	}

	/**
	 * @param srNo the srNo to set
	 */
	public void setSrNo(int srNo) {
		this.srNo = srNo;
	}

	/**
	 * @return the bloodBankBalance
	 */
	public double getBloodBankBalance() {
		return bloodBankBalance;
	}

	/**
	 * @param bloodBankBalance the bloodBankBalance to set
	 */
	public void setBloodBankBalance(double bloodBankBalance) {
		this.bloodBankBalance = bloodBankBalance;
	}

	/**
	 * @return the otherProductBalance
	 */
	public double getOtherProductBalance() {
		return otherProductBalance;
	}

	/**
	 * @param otherProductBalance the otherProductBalance to set
	 */
	public void setOtherProductBalance(double otherProductBalance) {
		this.otherProductBalance = otherProductBalance;
	}

	/**
	 * @return the expectedCashBalance
	 */
	public double getExpectedCashBalance() {
		return expectedCashBalance;
	}

	/**
	 * @param expectedCashBalance the expectedCashBalance to set
	 */
	public void setExpectedCashBalance(double expectedCashBalance) {
		this.expectedCashBalance = expectedCashBalance;
	}

	/**
	 * @return the actualCashBalance
	 */
	public double getActualCashBalance() {
		return actualCashBalance;
	}

	/**
	 * @param actualCashBalance the actualCashBalance to set
	 */
	public void setActualCashBalance(double actualCashBalance) {
		this.actualCashBalance = actualCashBalance;
	}

	/**
	 * @return the bankDeposited
	 */
	public double getBankDeposited() {
		return bankDeposited;
	}

	/**
	 * @param bankDeposited the bankDeposited to set
	 */
	public void setBankDeposited(double bankDeposited) {
		this.bankDeposited = bankDeposited;
	}

	/**
	 * @return the cashAdjusted
	 */
	public double getCashAdjusted() {
		return cashAdjusted;
	}

	/**
	 * @param cashAdjusted the cashAdjusted to set
	 */
	public void setCashAdjusted(double cashAdjusted) {
		this.cashAdjusted = cashAdjusted;
	}

	/**
	 * @return the storageCenter
	 */
	public String getStorageCenter() {
		return storageCenter;
	}

	/**
	 * @param storageCenter the storageCenter to set
	 */
	public void setStorageCenter(String storageCenter) {
		this.storageCenter = storageCenter;
	}

	/**
	 * @return the paymentType
	 */
	public String getPaymentType() {
		return paymentType;
	}

	/**
	 * @param paymentType the paymentType to set
	 */
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	/**
	 * @return the netReceivableAmt
	 */
	public double getNetReceivableAmt() {
		return netReceivableAmt;
	}

	/**
	 * @param netReceivableAmt the netReceivableAmt to set
	 */
	public void setNetReceivableAmt(double netReceivableAmt) {
		this.netReceivableAmt = netReceivableAmt;
	}

	/**
	 * @return the actualPayment
	 */
	public double getActualPayment() {
		return actualPayment;
	}

	/**
	 * @param actualPayment the actualPayment to set
	 */
	public void setActualPayment(double actualPayment) {
		this.actualPayment = actualPayment;
	}

	/**
	 * @return the outstandingAmt
	 */
	public double getOutstandingAmt() {
		return outstandingAmt;
	}

	/**
	 * @param outstandingAmt the outstandingAmt to set
	 */
	public void setOutstandingAmt(double outstandingAmt) {
		this.outstandingAmt = outstandingAmt;
	}

	/**
	 * @return the searchExportForAcctName
	 */
	public String getSearchExportForAcctName() {
		return searchExportForAcctName;
	}

	/**
	 * @param searchExportForAcctName the searchExportForAcctName to set
	 */
	public void setSearchExportForAcctName(String searchExportForAcctName) {
		this.searchExportForAcctName = searchExportForAcctName;
	}

	/**
	 * @return the componentName
	 */
	public String getComponentName() {
		return componentName;
	}

	/**
	 * @param componentName the componentName to set
	 */
	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	/**
	 * @return the componentRate
	 */
	public double getComponentRate() {
		return componentRate;
	}

	/**
	 * @param componentRate the componentRate to set
	 */
	public void setComponentRate(double componentRate) {
		this.componentRate = componentRate;
	}

	/**
	 * @return the searchComponentName
	 */
	public String getSearchComponentName() {
		return searchComponentName;
	}

	/**
	 * @param searchComponentName the searchComponentName to set
	 */
	public void setSearchComponentName(String searchComponentName) {
		this.searchComponentName = searchComponentName;
	}

	/**
	 * @return the otherProductAmount
	 */
	public double getOtherProductAmount() {
		return otherProductAmount;
	}

	/**
	 * @param otherProductAmount the otherProductAmount to set
	 */
	public void setOtherProductAmount(double otherProductAmount) {
		this.otherProductAmount = otherProductAmount;
	}

	/**
	 * @return the cashHandover
	 */
	public double getCashHandover() {
		return cashHandover;
	}

	/**
	 * @param cashHandover the cashHandover to set
	 */
	public void setCashHandover(double cashHandover) {
		this.cashHandover = cashHandover;
	}

	/**
	 * @return the cashDeposited
	 */
	public double getCashDeposited() {
		return cashDeposited;
	}

	/**
	 * @param cashDeposited the cashDeposited to set
	 */
	public void setCashDeposited(double cashDeposited) {
		this.cashDeposited = cashDeposited;
	}

	/**
	 * @return the totalCash
	 */
	public double getTotalCash() {
		return totalCash;
	}

	/**
	 * @param totalCash the totalCash to set
	 */
	public void setTotalCash(double totalCash) {
		this.totalCash = totalCash;
	}

	/**
	 * @return the registerBalance
	 */
	public double getRegisterBalance() {
		return registerBalance;
	}

	/**
	 * @param registerBalance the registerBalance to set
	 */
	public void setRegisterBalance(double registerBalance) {
		this.registerBalance = registerBalance;
	}

	/**
	 * @return the registerDate
	 */
	public String getRegisterDate() {
		return registerDate;
	}

	/**
	 * @param registerDate the registerDate to set
	 */
	public void setRegisterDate(String registerDate) {
		this.registerDate = registerDate;
	}

	/**
	 * @return the shift
	 */
	public String getShift() {
		return shift;
	}

	/**
	 * @param shift the shift to set
	 */
	public void setShift(String shift) {
		this.shift = shift;
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the fileInputStream
	 */
	public InputStream getFileInputStream() {
		return fileInputStream;
	}

	/**
	 * @param fileInputStream the fileInputStream to set
	 */
	public void setFileInputStream(InputStream fileInputStream) {
		this.fileInputStream = fileInputStream;
	}

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return the submitButton
	 */
	public String getSubmitButton() {
		return submitButton;
	}

	/**
	 * @param submitButton the submitButton to set
	 */
	public void setSubmitButton(String submitButton) {
		this.submitButton = submitButton;
	}

	/**
	 * @return the reportPatientID
	 */
	public String[] getReportPatientID() {
		return reportPatientID;
	}

	/**
	 * @param reportPatientID the reportPatientID to set
	 */
	public void setReportPatientID(String[] reportPatientID) {
		this.reportPatientID = reportPatientID;
	}

	/**
	 * @return the reportReceiptID
	 */
	public String[] getReportReceiptID() {
		return reportReceiptID;
	}

	/**
	 * @param reportReceiptID the reportReceiptID to set
	 */
	public void setReportReceiptID(String[] reportReceiptID) {
		this.reportReceiptID = reportReceiptID;
	}

	/**
	 * @return the bagNo
	 */
	public String getBagNo() {
		return bagNo;
	}

	/**
	 * @param bagNo the bagNo to set
	 */
	public void setBagNo(String bagNo) {
		this.bagNo = bagNo;
	}

	/**
	 * @return the receiptDate
	 */
	public String getReceiptDate() {
		return receiptDate;
	}

	/**
	 * @param receiptDate the receiptDate to set
	 */
	public void setReceiptDate(String receiptDate) {
		this.receiptDate = receiptDate;
	}

	/**
	 * @return the bloodBankID
	 */
	public int getBloodBankID() {
		return bloodBankID;
	}

	/**
	 * @param bloodBankID the bloodBankID to set
	 */
	public void setBloodBankID(int bloodBankID) {
		this.bloodBankID = bloodBankID;
	}

	/**
	 * @return the receiptID
	 */
	public int getReceiptID() {
		return receiptID;
	}

	/**
	 * @param receiptID the receiptID to set
	 */
	public void setReceiptID(int receiptID) {
		this.receiptID = receiptID;
	}

	/**
	 * @return the patientID
	 */
	public int getPatientID() {
		return patientID;
	}

	/**
	 * @param patientID the patientID to set
	 */
	public void setPatientID(int patientID) {
		this.patientID = patientID;
	}

	/**
	 * @return the userID
	 */
	public int getUserID() {
		return userID;
	}

	/**
	 * @param userID the userID to set
	 */
	public void setUserID(int userID) {
		this.userID = userID;
	}

	/**
	 * @return the patientName
	 */
	public String getPatientName() {
		return patientName;
	}

	/**
	 * @param patientName the patientName to set
	 */
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @param middleName the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the bBRNo
	 */
	public String getBBRNo() {
		return BBRNo;
	}

	/**
	 * @param bBRNo the bBRNo to set
	 */
	public void setBBRNo(String bBRNo) {
		BBRNo = bBRNo;
	}

	/**
	 * @return the receiptNo
	 */
	public String getReceiptNo() {
		return receiptNo;
	}

	/**
	 * @param receiptNo the receiptNo to set
	 */
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	/**
	 * @return the receiptType
	 */
	public String getReceiptType() {
		return receiptType;
	}

	/**
	 * @param receiptType the receiptType to set
	 */
	public void setReceiptType(String receiptType) {
		this.receiptType = receiptType;
	}

	/**
	 * @return the patientReportCriteria
	 */
	public String getPatientReportCriteria() {
		return patientReportCriteria;
	}

	/**
	 * @param patientReportCriteria the patientReportCriteria to set
	 */
	public void setPatientReportCriteria(String patientReportCriteria) {
		this.patientReportCriteria = patientReportCriteria;
	}

	/**
	 * @return the searchPatientReportName
	 */
	public String getSearchPatientReportName() {
		return searchPatientReportName;
	}

	/**
	 * @param searchPatientReportName the searchPatientReportName to set
	 */
	public void setSearchPatientReportName(String searchPatientReportName) {
		this.searchPatientReportName = searchPatientReportName;
	}

}
