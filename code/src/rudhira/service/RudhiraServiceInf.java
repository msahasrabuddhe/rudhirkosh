package rudhira.service;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;

import rudhira.form.BillingForm;
import rudhira.form.UserForm;

/**
 * 
 * @author Kovid Bioanalytics
 * 
 */
public interface RudhiraServiceInf {

	/**
	 * 
	 * @param userForm
	 * @return
	 */
	public String addUser(UserForm userForm, String realPath, HttpServletRequest request);

	/**
	 * 
	 * @param userForm
	 * @param request
	 * @return
	 */
	public String editUser(UserForm userForm, String realPath, HttpServletRequest request);

	/**
	 * 
	 * @param userForm
	 * @param realPath
	 * @return
	 */
	public String addBloodBank(UserForm userForm, String realPath);

	/**
	 * 
	 * @param userForm
	 * @param realPath
	 * @return
	 */
	public String editBloodBank(UserForm userForm, String realPath);

	/**
	 * 
	 * @param billingForm
	 * @param userID
	 * @return
	 */
	public String addNewBill(BillingForm billingForm, int userID);

	/**
	 * 
	 * @param billingForm
	 * @param userID
	 * @return
	 */
	public String editBill(BillingForm billingForm, int userID, String userType);

	/**
	 * 
	 * @param billingForm
	 * @return
	 */
	public String closeRegister(BillingForm billingForm);

	/**
	 * 
	 * @param billingForm
	 * @return
	 */
	public String addOtherProduct(BillingForm billingForm);

	/**
	 * 
	 * @param billingForm
	 * @param userID
	 * @return
	 */
	public String startRegister(BillingForm billingForm, int userID);

	/**
	 * 
	 * @param registerID
	 * @return
	 */
	public JSONObject retrieveCloseRegisterDetails(int registerID);

}
