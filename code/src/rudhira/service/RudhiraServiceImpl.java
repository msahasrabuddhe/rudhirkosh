package rudhira.service;

import java.io.File;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import rudhira.DAO.BillingDAOImpl;
import rudhira.DAO.BillingDAOInf;
import rudhira.DAO.UserDAOImpl;
import rudhira.DAO.UserDAOInf;
import rudhira.form.BillingForm;
import rudhira.form.UserForm;
import rudhira.util.ActivityStatus;

/**
 * 
 * @author Kovid Bioanalytics
 * 
 */
public class RudhiraServiceImpl implements RudhiraServiceInf {

	String statusMessage = "error";

	UserDAOInf daoInf = null;

	BillingDAOInf billingDAOInf = null;

	public String addUser(UserForm userForm, String realPath, HttpServletRequest request) {
		daoInf = new UserDAOImpl();

		int status = 0;

		// Inserting User details into AppUser table
		status = daoInf.verifyUsernameExist(userForm.getUsername());

		try {
			/*
			 * Storing user uploaded profile pic file into image/profilePic directory of
			 * Project, only if the profilePic variable is not null
			 */
			if (userForm.getProfilePic() != null) {

				String[] array = userForm.getProfilePicFileName().split("\\.");

				String fileExtension = array[1];

				String fileNameToBeStored = userForm.getUsername() + "." + fileExtension;

				System.out.println("Original File name is ::::" + userForm.getProfilePicFileName());

				System.out.println("File Name to be stored into DB is ::: " + fileNameToBeStored);
				/*
				 * Setting file name to be inserted into AppUser table into profilePicDBName
				 * variable of UserForm
				 */
				userForm.setProfilePicDBName("images/profilePics/" + fileNameToBeStored);

				String destPath = realPath + "images/profilePics/" + fileNameToBeStored;

				File destFile = new File(destPath);

				// Copying file into the destination directory
				FileUtils.copyFile(userForm.getProfilePic(), destFile);

			} else {
				userForm.setProfilePicDBName(null);
			}

			if (status != 1) {

				if (userForm.getBloodBankID() != -1) {
					/*
					 * Inserting user detail into AppUser table
					 */
					statusMessage = daoInf.inserUserDetails(userForm);
					if (statusMessage.equalsIgnoreCase("success")) {

						System.out.println("Successfully inserted user detail into AppUser Table.");

						/*
						 * Retrieving last user ID based on username
						 */
						int userID = daoInf.retrieveUserIDByUsername(userForm.getUsername());

						/*
						 * Inserting password value into PasswordHistory table in order to maintain
						 * password history in case of password change
						 */
						statusMessage = daoInf.insertPasswordHistory(userID, userForm.getPassword());

						if (statusMessage.equalsIgnoreCase("success")) {

							// Inserting password change log into Audit
							daoInf.insertAuditDetails(userID, request.getRemoteAddr(), "PIN changed");

							// Inserting PIN change log into Audit
							daoInf.insertAuditDetails(userID, request.getRemoteAddr(), "Password changed");

							return statusMessage;

						} else {

							System.out.println("Failed to insert password into PasswordHistory table.");
							statusMessage = "error";
							return statusMessage;

						}

					} else {
						System.out.println("Failed to insert user detail into AppUser table.");
						statusMessage = "error";
						return statusMessage;
					}
				} else {
					System.out.println("No blood bank selected");
					statusMessage = "bloodBankSelect";
					return statusMessage;
				}
			} else {
				System.out.println("Username already exists into AppUser table...");
				statusMessage = "input";
				return statusMessage;
			}
		} catch (Exception exception) {
			System.out.println("Exception occured while copying file into destination directory");

			statusMessage = "error";

			return statusMessage;

		}

	}

	public String editUser(UserForm userForm, String realPath, HttpServletRequest request) {
		daoInf = new UserDAOImpl();

		boolean check = false;

		try {
			/*
			 * Storing user uploaded profile pic file into image/profilePic directory of
			 * Project, only if the profilePic variable is not null
			 */
			if (userForm.getProfilePicDBName() == null || userForm.getProfilePicDBName() == "") {
				userForm.setProfilePicDBName(null);
			}

			if (userForm.getProfilePic() != null) {

				String[] array = userForm.getProfilePicFileName().split("\\.");

				String fileExtension = array[1];

				String fileNameToBeStored = userForm.getUsername() + "." + fileExtension;

				System.out.println("Original File name is ::::" + userForm.getProfilePicFileName());

				System.out.println("File Name to be stored into DB is ::: " + fileNameToBeStored);
				/*
				 * Setting file name to be inserted into AppUser table
				 */
				userForm.setProfilePicDBName("images/profilePics/" + fileNameToBeStored);

				String destPath = realPath + "images/profilePics/" + fileNameToBeStored;

				File destFile = new File(destPath);

				// Copying file into the destination directory
				FileUtils.copyFile(userForm.getProfilePic(), destFile);

			}

			/*
			 * Checking whether PIN has been changed by user, if so, then insert the PIN
			 * changed log into Audit table
			 */
			check = daoInf.verifyPINChange(userForm.getUserID(), userForm.getPIN());
			if (!check) {

				/*
				 * Inserting PIN change activity into Audit table
				 */
				daoInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "PIN changed");

			}

			/*
			 * Check whether password from table for userID is same as that entered by user
			 * for the same userID, then proceed for updation else check for last five
			 * password existence from PasswordHistory table
			 */
			check = daoInf.verifyPassword(userForm.getUserID(), userForm.getPassword());

			if (check) {

				/*
				 * Updating user details from AppUser table
				 */
				statusMessage = daoInf.updateUserDetails(userForm);

				if (statusMessage.equalsIgnoreCase("success")) {

					return statusMessage;

				} else {

					System.out.println("Failed to update user details into AppUser table");

					statusMessage = "error";
					return statusMessage;

				}

				/*
				 * Password match check; checking whether entered password matches last five
				 * passwords from Password History, then give error message else proceed further
				 */
			} else {

				check = daoInf.verifyPasswordHistory(userForm.getUserID(), userForm.getPassword());

				if (check) {

					System.out.println("Entered password lies within last 5 passwords.");

					statusMessage = "input";

					return statusMessage;

				} else {

					/*
					 * Updating user details from AppUser table
					 */
					statusMessage = daoInf.updateUserDetails(userForm);

					if (statusMessage.equalsIgnoreCase("success")) {

						/*
						 * Inserting password value into PasswordHistory table in order to maintain
						 * password history in case of password change
						 */
						statusMessage = daoInf.insertPasswordHistory(userForm.getUserID(), userForm.getPassword());

						/*
						 * Inserting password change activity into Audit table
						 */
						daoInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Password changed");

						return statusMessage;

					} else {

						System.out.println("Failed to update user details into AppUser table");

						statusMessage = "error";
						return statusMessage;

					}

				}

			}

		} catch (Exception exception) {
			System.out.println("Exception occured while copying file into destination directory");

			statusMessage = "error";

			return statusMessage;

		}
	}

	public String addBloodBank(UserForm userForm, String realPath) {
		daoInf = new UserDAOImpl();

		int status = 0;

		// Inserting Blood Bank Name details into BloodBank table
		status = daoInf.verifyBloodBankNameExists(userForm.getBloodBankName());

		try {
			/*
			 * Storing user uploaded blood bank logo file into image/bloodBankLogo directory
			 * of Project
			 */
			if (userForm.getBloodBankLogo() == null) {

				System.out.println("Blood bank logo file is not selected by user");

				statusMessage = "logoFileSelect";

				return statusMessage;

			} else {

				String[] array = userForm.getBloodBankLogoFileName().split("\\.");

				String fileExtension = array[1];

				/*
				 * If blood bank name contains space in between, remove that space and attach
				 * the words
				 */
				String bloodBankName = userForm.getBloodBankName();

				if (bloodBankName.contains(" ")) {
					bloodBankName = bloodBankName.replace(" ", "");
				}

				String fileNameToBeStored = bloodBankName + "." + fileExtension;

				System.out.println("Original Blood Bank logo File name is ::::" + userForm.getBloodBankLogoFileName());

				System.out.println("Blood Bank File Name to be stored into DB is ::: " + fileNameToBeStored);
				/*
				 * Setting file name to be inserted into BloodBank table
				 */
				userForm.setBloodBankLogoFileName("images/bloodBankLogo/" + fileNameToBeStored);

				String destPath = realPath + "images/bloodBankLogo/" + fileNameToBeStored;

				File destFile = new File(destPath);

				// Copying file into the destination directory
				FileUtils.copyFile(userForm.getBloodBankLogo(), destFile);

				if (status != 1) {

					/*
					 * Inserting blood bank detail into BloodBank table
					 */
					statusMessage = daoInf.insertBloodBank(userForm);
					if (statusMessage.equalsIgnoreCase("success")) {

						System.out.println("Successfully inserted blood bank detail into BloodBank Table.");

						return statusMessage;

					} else {
						System.out.println("Failed to insert blood bank detail into BloodBank table.");

						statusMessage = "error";
						return statusMessage;
					}

				} else {
					System.out.println("BloodBankName already exists into BloodBank table...");

					statusMessage = "input";
					return statusMessage;
				}

			}

		} catch (Exception exception) {
			System.out.println("Exception occured while copying file into destination directory");

			statusMessage = "error";

			return statusMessage;

		}

	}

	public String editBloodBank(UserForm userForm, String realPath) {
		daoInf = new UserDAOImpl();

		int status = 0;

		// Inserting Blood Bank Name details into BloodBank table
		status = daoInf.verifyBloodBankNameExists(userForm.getBloodBankName(), userForm.getBloodBankID());

		try {
			/*
			 * Storing user uploaded blood bank logo file into image/bloodBankLogo directory
			 * of Project
			 */
			if (userForm.getBloodBankLogo() == null) {

				System.out.println("Blood bank logo file is not selected by user");

				statusMessage = "logoFileSelect";

				return statusMessage;

			} else {

				String[] array = userForm.getBloodBankLogoFileName().split("\\.");

				String fileExtension = array[1];

				/*
				 * If blood bank name contains space in between, remove that space and attach
				 * the words
				 */
				String bloodBankName = userForm.getBloodBankName();

				if (bloodBankName.contains(" ")) {
					bloodBankName = bloodBankName.replace(" ", "");
				}

				String fileNameToBeStored = bloodBankName + "." + fileExtension;

				System.out.println("Original Blood Bank logo File name is ::::" + userForm.getBloodBankLogoFileName());

				System.out.println("Blood Bank File Name to be stored into DB is ::: " + fileNameToBeStored);
				/*
				 * Setting file name to be inserted into BloodBank table
				 */
				userForm.setBloodBankLogoFileName("images/bloodBankLogo/" + fileNameToBeStored);

				String destPath = realPath + "images/bloodBankLogo/" + fileNameToBeStored;

				File destFile = new File(destPath);

				// Copying file into the destination directory
				FileUtils.copyFile(userForm.getBloodBankLogo(), destFile);

				if (status != 1) {

					/*
					 * Inserting blood bank detail into BloodBank table
					 */
					statusMessage = daoInf.updateBloodBank(userForm);
					if (statusMessage.equalsIgnoreCase("success")) {

						return statusMessage;

					} else {
						System.out.println("Failed to update blood bank detail into BloodBank table.");

						statusMessage = "error";
						return statusMessage;
					}

				} else {
					System.out.println("BloodBankName already exists into BloodBank table...");

					statusMessage = "input";
					return statusMessage;
				}

			}

		} catch (Exception exception) {
			System.out.println("Exception occured while copying file into destination directory");

			statusMessage = "error";

			return statusMessage;

		}

	}

	public String addNewBill_old(BillingForm billingForm, int userID) {

		billingDAOInf = new BillingDAOImpl();

		int patientID = 0;

		System.out.println("BB Storage ... " + billingForm.getBbStorageCenter());

		if (!billingForm.getBbStorageCenter().equals("000")) {

			System.out.println("inside bb storage ");

			/*
			 * verifying whether bsc already exists into Patient table or not, if exists,
			 * then retrieving patientID based on BSC else inserting new record with new BSC
			 * name
			 */
			boolean check = billingDAOInf.verifyBloodStorageExists(billingForm.getBbStorageCenter());

			if (check) {

				/*
				 * Retrieving patientID based on bb storage center number
				 */
				patientID = billingDAOInf.retrievePatientIdByBloodBankStorageCenter(billingForm.getBbStorageCenter());

				if (patientID == 0) {

					System.out.println("Failed to retrieve patient ID based on blood bank storage center");

					statusMessage = "error";

					return statusMessage;

				} else {

					System.out.println(
							"Successfully retrieved patient ID based on blood bank storage center is ::: " + patientID);

					/*
					 * Setting retrieved patientID into BillingForm patientID variable
					 */
					billingForm.setPatientID(patientID);

					/*
					 * Inserting receipt details into Receipt table
					 */
					statusMessage = billingDAOInf.insertReceiptDetails(billingForm, userID, patientID);

					if (statusMessage.equalsIgnoreCase("success")) {

						/*
						 * Retrieving receipt ID based on receipt no
						 */
						int receiptID = billingDAOInf.retrieveReceiptIDBasedOnReceiptNo(billingForm.getReceiptNo());

						if (receiptID == 0) {

							System.out.println("Failed to retrieve receipt ID based on Receipt no.");

							statusMessage = "error";

							return statusMessage;

						} else {

							System.out
									.println("Successfully retrieve receipt ID based on receipt no is :: " + receiptID);

							/*
							 * Setting retrieved receipt ID into Billing Form receiptID variable
							 */
							billingForm.setReceiptID(receiptID);

							/*
							 * Checking whether chargeType is added for billing or not, if added then only
							 * inserting it into ChargeType table
							 */
							if (billingForm.getChargeType() == null) {

								System.out.println("No other charges selected.");

								statusMessage = "success";

							} else {

								/*
								 * Inserting OtherCharges details into OtherCharge table by iterating over
								 * chargeType variable from BillingForm
								 */
								for (int i = 0; i < billingForm.getChargeType().length; i++) {

									statusMessage = billingDAOInf.insertOtherCharge(billingForm.getChargeType()[i],
											Double.parseDouble(billingForm.getOtherCharge()[i]), receiptID);

									if (statusMessage.equalsIgnoreCase("success")) {

										System.out.println(
												"Other charges inserted successfully into OtehrCharges table.");

									} else {

										System.out.println(
												"Failed to insert other charges details into OtherCharges table.");

										statusMessage = "error";

									}

								}

							}

							/*
							 * Checking whether any of component is added for billing or not, if added then
							 * only insert component details into ReceiptItems table
							 */
							if (billingForm.getComponent() == null) {

								System.out.println("No component selected.");

								statusMessage = "success";

								/*
								 * Checking whether payment type is cheque or cash, if it is cheque, then
								 * inserting cheque details into Cheque details table
								 */
								if (billingForm.getPaymentType().equals("Cheque")
										|| billingForm.getPaymentType().equals("Credit/Debit Card")) {

									statusMessage = billingDAOInf.insertChequeDetails(billingForm);

									if (statusMessage.equalsIgnoreCase("success")) {

										System.out.println(
												"Successfully inserted cheque details into ChequeDetails table.");

									} else {

										System.out.println("Failed to insert cheque details into table.");

										statusMessage = "error";

									}

								}

								return statusMessage;

							} else {

								/*
								 * Inserting component details one by one into ReceiptItems tables
								 */

								/*
								 * Iterating over the component items
								 */
								for (int i = 0; i < billingForm.getComponent().length; i++) {

									statusMessage = billingDAOInf.insertReceiptItems(
											Integer.parseInt(billingForm.getQuantity()[i]),
											billingForm.getComponent()[i], Double.parseDouble(billingForm.getRate()[i]),
											Double.parseDouble(billingForm.getAmount()[i]), receiptID);

									if (statusMessage.equalsIgnoreCase("success")) {

										/*
										 * retrieving last inserted receipt item ID
										 */
										int receiptItemID = billingDAOInf.retrieveLastEneteredReceiptItemID();

										if (billingForm.getBagNo() == null) {

											System.out.println("No bags added");

											statusMessage = "success";

										} else {

											/*
											 * check wehther bag added or not, if not then return success else proceed
											 * to add bag details into BagLog table
											 */
											if (billingForm.getBagNo()[i] == null || billingForm.getBagNo()[i] == "") {

												System.out.println("No bags added");

												statusMessage = "success";

											} else {

												String bagNo = billingForm.getBagNo()[i];

												/*
												 * checking whether bag no starts with ',' and if yes removing the first
												 * , from String
												 */
												if (bagNo.startsWith(",")) {
													bagNo = bagNo.substring(1);
												}

												/*
												 * Checking whether bag no contains multiple values seperated by commas,
												 * if yes then iterate over it and insert one by one values into
												 * BagLogTable
												 */
												if (bagNo.contains(",")) {

													// Iterate over Bag no
													String[] bagNoArray = bagNo.split(",");

													for (int j = 0; j < bagNoArray.length; j++) {

														/*
														 * Inserting bag details into BagLog table
														 */
														statusMessage = billingDAOInf.insertBagDetails(receiptItemID,
																bagNoArray[j]);

														if (statusMessage.equalsIgnoreCase("success")) {

															System.out.println(
																	"Successfully inserted bag details into BagLog table.");

														} else {

															System.out.println(
																	"Failed to insert bag details into BagLog table.");

															statusMessage = "error";

														}

													}

												} else {

													/*
													 * Inserting bag details into BagLog table
													 */
													statusMessage = billingDAOInf.insertBagDetails(receiptItemID,
															bagNo);

													if (statusMessage.equalsIgnoreCase("success")) {

														System.out.println(
																"Successfully inserted bag details into BagLog table.");

													} else {

														System.out.println(
																"Failed to insert bag details into BagLog table.");

														statusMessage = "error";

													}

												}

											}

										}

									} else {

										System.out.println("Failed to insert receipt items into ReceiptItems table.");

										statusMessage = "error";

									}

								}

								/*
								 * Checking whether payment type is cheque or cash, if it is cheque, then
								 * inserting cheque details into Cheque details table
								 */
								if (billingForm.getPaymentType().equals("Cheque")
										|| billingForm.getPaymentType().equals("Credit/Debit Card")) {

									statusMessage = billingDAOInf.insertChequeDetails(billingForm);

									if (statusMessage.equalsIgnoreCase("success")) {

										System.out.println(
												"Successfully inserted cheque details into ChequeDetails table.");

									} else {

										System.out.println("Failed to insert cheque details into table.");

										statusMessage = "error";

									}

								}

								return statusMessage;

							}

						}

					} else {

						System.out.println("Failed to insert receipt details into Receipt table");

						statusMessage = "error";

						return statusMessage;
					}

				}

			} else {

				/*
				 * Inserting patient details into Patient table
				 */
				statusMessage = billingDAOInf.insertPatientDetails(billingForm);

				if (statusMessage.equalsIgnoreCase("success")) {

					/*
					 * Retrieving patientID based on bb storage center number
					 */
					patientID = billingDAOInf
							.retrievePatientIdByBloodBankStorageCenter(billingForm.getBbStorageCenter());

					if (patientID == 0) {

						System.out.println("Failed to retrieve patient ID based on blood bank storage center");

						statusMessage = "error";

						return statusMessage;

					} else {

						System.out
								.println("Successfully retrieved patient ID based on blood bank storage center is ::: "
										+ patientID);

						/*
						 * Setting retrieved patientID into BillingForm patientID variable
						 */
						billingForm.setPatientID(patientID);

						/*
						 * Inserting receipt details into Receipt table
						 */
						statusMessage = billingDAOInf.insertReceiptDetails(billingForm, userID, patientID);

						if (statusMessage.equalsIgnoreCase("success")) {

							/*
							 * Retrieving receipt ID based on receipt no
							 */
							int receiptID = billingDAOInf.retrieveReceiptIDBasedOnReceiptNo(billingForm.getReceiptNo());

							if (receiptID == 0) {

								System.out.println("Failed to retrieve receipt ID based on Receipt no.");

								statusMessage = "error";

								return statusMessage;

							} else {

								System.out.println(
										"Successfully retrieve receipt ID based on receipt no is :: " + receiptID);

								/*
								 * Setting retrieved receipt ID into Billing Form receiptID variable
								 */
								billingForm.setReceiptID(receiptID);

								/*
								 * Checking whether chargeType is added for billing or not, if added then only
								 * inserting it into ChargeType table
								 */
								if (billingForm.getChargeType() == null) {

									System.out.println("No other charges selected.");

									statusMessage = "success";

								} else {

									/*
									 * Inserting OtherCharges details into OtherCharge table by iterating over
									 * chargeType variable from BillingForm
									 */
									for (int i = 0; i < billingForm.getChargeType().length; i++) {

										statusMessage = billingDAOInf.insertOtherCharge(billingForm.getChargeType()[i],
												Double.parseDouble(billingForm.getOtherCharge()[i]), receiptID);

										if (statusMessage.equalsIgnoreCase("success")) {

											System.out.println(
													"Other charges inserted successfully into OtehrCharges table.");

										} else {

											System.out.println(
													"Failed to insert other charges details into OtherCharges table.");

											statusMessage = "error";

										}

									}

								}

								/*
								 * Checking whether any of component is added for billing or not, if added then
								 * only insert component details into ReceiptItems table
								 */
								if (billingForm.getComponent() == null) {

									System.out.println("No component selected.");

									statusMessage = "success";

									/*
									 * Checking whether payment type is cheque or cash, if it is cheque, then
									 * inserting cheque details into Cheque details table
									 */
									if (billingForm.getPaymentType().equals("Cheque")
											|| billingForm.getPaymentType().equals("Credit/Debit Card")) {

										statusMessage = billingDAOInf.insertChequeDetails(billingForm);

										if (statusMessage.equalsIgnoreCase("success")) {

											System.out.println(
													"Successfully inserted cheque details into ChequeDetails table.");

										} else {

											System.out.println("Failed to insert cheque details into table.");

											statusMessage = "error";

										}

									}

									return statusMessage;

								} else {

									/*
									 * Inserting component details one by one into ReceiptItems tables
									 */

									/*
									 * Iterating over the component items
									 */
									for (int i = 0; i < billingForm.getComponent().length; i++) {

										statusMessage = billingDAOInf.insertReceiptItems(
												Integer.parseInt(billingForm.getQuantity()[i]),
												billingForm.getComponent()[i],
												Double.parseDouble(billingForm.getRate()[i]),
												Double.parseDouble(billingForm.getAmount()[i]), receiptID);

										if (statusMessage.equalsIgnoreCase("success")) {

											/*
											 * retrieving last inserted receipt item ID
											 */
											int receiptItemID = billingDAOInf.retrieveLastEneteredReceiptItemID();

											if (billingForm.getBagNo() == null) {

												System.out.println("No bags added");

												statusMessage = "success";

											} else {

												/*
												 * check wehther bag added or not, if not then return success else
												 * proceed to add bag details into BagLog table
												 */
												if (billingForm.getBagNo()[i] == null
														|| billingForm.getBagNo()[i] == "") {

													System.out.println("No bags added");

													statusMessage = "success";

												} else {

													String bagNo = billingForm.getBagNo()[i];

													/*
													 * checking whether bag no starts with ',' and if yes removing the
													 * first , from String
													 */
													if (bagNo.startsWith(",")) {
														bagNo = bagNo.substring(1);
													}

													/*
													 * Checking whether bag no contains multiple values seperated by
													 * commas, if yes then iterate over it and insert one by one values
													 * into BagLogTable
													 */
													if (bagNo.contains(",")) {

														// Iterate over Bag no
														String[] bagNoArray = bagNo.split(",");

														for (int j = 0; j < bagNoArray.length; j++) {

															/*
															 * Inserting bag details into BagLog table
															 */
															statusMessage = billingDAOInf
																	.insertBagDetails(receiptItemID, bagNoArray[j]);

															if (statusMessage.equalsIgnoreCase("success")) {

																System.out.println(
																		"Successfully inserted bag details into BagLog table.");

															} else {

																System.out.println(
																		"Failed to insert bag details into BagLog table.");

																statusMessage = "error";

															}

														}

													} else {

														/*
														 * Inserting bag details into BagLog table
														 */
														statusMessage = billingDAOInf.insertBagDetails(receiptItemID,
																bagNo);

														if (statusMessage.equalsIgnoreCase("success")) {

															System.out.println(
																	"Successfully inserted bag details into BagLog table.");

														} else {

															System.out.println(
																	"Failed to insert bag details into BagLog table.");

															statusMessage = "error";

														}

													}

												}

											}

										} else {

											System.out
													.println("Failed to insert receipt items into ReceiptItems table.");

											statusMessage = "error";

										}

									}

									/*
									 * Checking whether payment type is cheque or cash, if it is cheque, then
									 * inserting cheque details into Cheque details table
									 */
									if (billingForm.getPaymentType().equals("Cheque")
											|| billingForm.getPaymentType().equals("Credit/Debit Card")) {

										statusMessage = billingDAOInf.insertChequeDetails(billingForm);

										if (statusMessage.equalsIgnoreCase("success")) {

											System.out.println(
													"Successfully inserted cheque details into ChequeDetails table.");

										} else {

											System.out.println("Failed to insert cheque details into table.");

											statusMessage = "error";

										}

									}

									return statusMessage;

								}

							}

						} else {

							System.out.println("Failed to insert receipt details into Receipt table");

							statusMessage = "error";

							return statusMessage;
						}

					}

				} else {

					System.out.println("Failed to insert patient details into table.");

					statusMessage = "error";

					return statusMessage;
				}
			}

		} else {

			/*
			 * Verify patientIdentifier already exist into Patient or not, if not then only
			 * insert into Patient table otherwise fetch patientID on the basis of
			 * patientIdentifier and insert that patientID into Receipt table
			 */

			patientID = billingDAOInf.retrievePatientIDOnVerifyPatientCode(billingForm.getPatientIDNo());

			if (patientID == 0) {

				/*
				 * Inserting patient details into Patient table
				 */
				statusMessage = billingDAOInf.insertPatientDetails(billingForm);

				if (statusMessage.equalsIgnoreCase("success")) {

					/*
					 * Retrieving patientID based on patientIdentifier number
					 */
					patientID = billingDAOInf.retrievePatientIDBasedOnIndentifierNo(billingForm.getPatientIDNo());

					if (patientID == 0) {

						System.out.println("Failed to retrieve patient ID based on indentifier no.");

						statusMessage = "error";

						return statusMessage;

					} else {

						System.out.println(
								"Successfully retrieved patient ID based on indentifier no is ::: " + patientID);

						/*
						 * Setting retrieved patientID into BillingForm patientID variable
						 */
						billingForm.setPatientID(patientID);

						/*
						 * Inserting receipt details into Receipt table
						 */
						statusMessage = billingDAOInf.insertReceiptDetails(billingForm, userID, patientID);

						if (statusMessage.equalsIgnoreCase("success")) {

							/*
							 * Retrieving receipt ID based on receipt no
							 */
							int receiptID = billingDAOInf.retrieveReceiptIDBasedOnReceiptNo(billingForm.getReceiptNo());

							if (receiptID == 0) {

								System.out.println("Failed to retrieve receipt ID based on Receipt no.");

								statusMessage = "error";

								return statusMessage;

							} else {

								System.out.println(
										"Successfully retrieve receipt ID based on receipt no is :: " + receiptID);

								/*
								 * Setting retrieved receipt ID into Billing Form receiptID variable
								 */
								billingForm.setReceiptID(receiptID);

								/*
								 * Checking whether chargeType is added for billing or not, if added then only
								 * inserting it into ChargeType table
								 */
								if (billingForm.getChargeType() == null) {

									System.out.println("No other charges selected.");

									statusMessage = "success";

								} else {

									/*
									 * Inserting OtherCharges details into OtherCharge table by iterating over
									 * chargeType variable from BillingForm
									 */
									for (int i = 0; i < billingForm.getChargeType().length; i++) {

										statusMessage = billingDAOInf.insertOtherCharge(billingForm.getChargeType()[i],
												Double.parseDouble(billingForm.getOtherCharge()[i]), receiptID);

										if (statusMessage.equalsIgnoreCase("success")) {

											System.out.println(
													"Other charges inserted successfully into OtehrCharges table.");

										} else {

											System.out.println(
													"Failed to insert other charges details into OtherCharges table.");

											statusMessage = "error";

										}

									}

								}

								/*
								 * Checking whether any of component is added for billing or not, if added then
								 * only insert component details into ReceiptItems table
								 */
								if (billingForm.getComponent() == null) {

									System.out.println("No component selected.");

									statusMessage = "success";

									/*
									 * Checking whether payment type is cheque or cash, if it is cheque, then
									 * inserting cheque details into Cheque details table
									 */
									if (billingForm.getPaymentType().equals("Cheque")
											|| billingForm.getPaymentType().equals("Credit/Debit Card")) {

										statusMessage = billingDAOInf.insertChequeDetails(billingForm);

										if (statusMessage.equalsIgnoreCase("success")) {

											System.out.println(
													"Successfully inserted cheque details into ChequeDetails table.");

										} else {

											System.out.println("Failed to insert cheque details into table.");

											statusMessage = "error";

										}

									}

									return statusMessage;

								} else {

									/*
									 * Inserting component details one by one into ReceiptItems tables
									 */

									/*
									 * Iterating over the component items
									 */
									for (int i = 0; i < billingForm.getComponent().length; i++) {

										statusMessage = billingDAOInf.insertReceiptItems(
												Integer.parseInt(billingForm.getQuantity()[i]),
												billingForm.getComponent()[i],
												Double.parseDouble(billingForm.getRate()[i]),
												Double.parseDouble(billingForm.getAmount()[i]), receiptID);

										if (statusMessage.equalsIgnoreCase("success")) {

											/*
											 * retrieving last inserted receipt item ID
											 */
											int receiptItemID = billingDAOInf.retrieveLastEneteredReceiptItemID();

											if (billingForm.getBagNo() == null) {

												System.out.println("No bags added");

												statusMessage = "success";

											} else {

												/*
												 * check wehther bag added or not, if not then return success else
												 * proceed to add bag details into BagLog table
												 */
												if (billingForm.getBagNo()[i] == null
														|| billingForm.getBagNo()[i] == "") {

													System.out.println("No bags added");

													statusMessage = "success";

												} else {

													String bagNo = billingForm.getBagNo()[i];

													/*
													 * checking whether bag no starts with ',' and if yes removing the
													 * first , from String
													 */
													if (bagNo.startsWith(",")) {
														bagNo = bagNo.substring(1);
													}

													/*
													 * Checking whether bag no contains multiple values seperated by
													 * commas, if yes then iterate over it and insert one by one values
													 * into BagLogTable
													 */
													if (bagNo.contains(",")) {

														// Iterate over Bag no
														String[] bagNoArray = bagNo.split(",");

														for (int j = 0; j < bagNoArray.length; j++) {

															/*
															 * Inserting bag details into BagLog table
															 */
															statusMessage = billingDAOInf
																	.insertBagDetails(receiptItemID, bagNoArray[j]);

															if (statusMessage.equalsIgnoreCase("success")) {

																System.out.println(
																		"Successfully inserted bag details into BagLog table.");

															} else {

																System.out.println(
																		"Failed to insert bag details into BagLog table.");

																statusMessage = "error";

															}

														}

													} else {

														/*
														 * Inserting bag details into BagLog table
														 */
														statusMessage = billingDAOInf.insertBagDetails(receiptItemID,
																bagNo);

														if (statusMessage.equalsIgnoreCase("success")) {

															System.out.println(
																	"Successfully inserted bag details into BagLog table.");

														} else {

															System.out.println(
																	"Failed to insert bag details into BagLog table.");

															statusMessage = "error";

														}

													}

												}

											}

										} else {

											System.out
													.println("Failed to insert receipt items into ReceiptItems table.");

											statusMessage = "error";

										}

									}

									/*
									 * Checking whether payment type is cheque or cash, if it is cheque, then
									 * inserting cheque details into Cheque details table
									 */
									if (billingForm.getPaymentType().equals("Cheque")
											|| billingForm.getPaymentType().equals("Credit/Debit Card")) {

										statusMessage = billingDAOInf.insertChequeDetails(billingForm);

										if (statusMessage.equalsIgnoreCase("success")) {

											System.out.println(
													"Successfully inserted cheque details into ChequeDetails table.");

										} else {

											System.out.println("Failed to insert cheque details into table.");

											statusMessage = "error";

										}

									}

									return statusMessage;

								}

							}

						} else {

							System.out.println("Failed to insert receipt details into Receipt table");

							statusMessage = "error";

							return statusMessage;
						}

					}

				} else {

					System.out.println("Failed to insert patient details into table.");

					statusMessage = "error";

					return statusMessage;
				}

			} else {

				System.out.println("Successfully retrieved patient ID based on indentifier no is ::: " + patientID);

				/*
				 * Setting retrieved patientID into BillingForm patientID variable
				 */
				billingForm.setPatientID(patientID);

				/*
				 * Inserting receipt details into Receipt table
				 */
				statusMessage = billingDAOInf.insertReceiptDetails(billingForm, userID, patientID);

				if (statusMessage.equalsIgnoreCase("success")) {

					/*
					 * Retrieving receipt ID based on receipt no
					 */
					int receiptID = billingDAOInf.retrieveReceiptIDBasedOnReceiptNo(billingForm.getReceiptNo());

					if (receiptID == 0) {

						System.out.println("Failed to retrieve receipt ID based on Receipt no.");

						statusMessage = "error";

						return statusMessage;

					} else {

						System.out.println("Successfully retrieve receipt ID based on receipt no is :: " + receiptID);

						/*
						 * Setting retrieved receipt ID into Billing Form receiptID variable
						 */
						billingForm.setReceiptID(receiptID);

						/*
						 * Checking whether chargeType is added for billing or not, if added then only
						 * inserting it into ChargeType table
						 */
						if (billingForm.getChargeType() == null) {

							System.out.println("No other charges selected.");

							statusMessage = "success";

						} else {

							/*
							 * Inserting OtherCharges details into OtherCharge table by iterating over
							 * chargeType variable from BillingForm
							 */
							for (int i = 0; i < billingForm.getChargeType().length; i++) {

								statusMessage = billingDAOInf.insertOtherCharge(billingForm.getChargeType()[i],
										Double.parseDouble(billingForm.getOtherCharge()[i]), receiptID);

								if (statusMessage.equalsIgnoreCase("success")) {

									System.out.println("Other charges inserted successfully into OtehrCharges table.");

								} else {

									System.out
											.println("Failed to insert other charges details into OtherCharges table.");

									statusMessage = "error";

								}

							}

						}

						/*
						 * Checking whether any of component is added for billing or not, if added then
						 * only insert component details into ReceiptItems table
						 */
						if (billingForm.getComponent() == null) {

							System.out.println("No component selected.");

							statusMessage = "success";

							/*
							 * Checking whether payment type is cheque or cash, if it is cheque, then
							 * inserting cheque details into Cheque details table
							 */
							if (billingForm.getPaymentType().equals("Cheque")
									|| billingForm.getPaymentType().equals("Credit/Debit Card")) {

								statusMessage = billingDAOInf.insertChequeDetails(billingForm);

								if (statusMessage.equalsIgnoreCase("success")) {

									System.out
											.println("Successfully inserted cheque details into ChequeDetails table.");

								} else {

									System.out.println("Failed to insert cheque details into table.");

									statusMessage = "error";

								}

							}

							return statusMessage;

						} else {

							/*
							 * Inserting component details one by one into ReceiptItems tables
							 */

							/*
							 * Iterating over the component items
							 */
							for (int i = 0; i < billingForm.getComponent().length; i++) {

								statusMessage = billingDAOInf.insertReceiptItems(
										Integer.parseInt(billingForm.getQuantity()[i]), billingForm.getComponent()[i],
										Double.parseDouble(billingForm.getRate()[i]),
										Double.parseDouble(billingForm.getAmount()[i]), receiptID);

								if (statusMessage.equalsIgnoreCase("success")) {

									/*
									 * retrieving last inserted receipt item ID
									 */
									int receiptItemID = billingDAOInf.retrieveLastEneteredReceiptItemID();

									if (billingForm.getBagNo() == null) {

										System.out.println("No bags added");

										statusMessage = "success";

									} else {

										/*
										 * check wehther bag added or not, if not then return success else proceed to
										 * add bag details into BagLog table
										 */
										if (billingForm.getBagNo()[i] == null || billingForm.getBagNo()[i] == "") {

											System.out.println("No bags added");

											statusMessage = "success";

										} else {

											String bagNo = billingForm.getBagNo()[i];

											/*
											 * checking whether bag no starts with ',' and if yes removing the first ,
											 * from String
											 */
											if (bagNo.startsWith(",")) {
												bagNo = bagNo.substring(1);
											}

											/*
											 * Checking whether bag no contains multiple values seperated by commas, if
											 * yes then iterate over it and insert one by one values into BagLogTable
											 */
											if (bagNo.contains(",")) {

												// Iterate over Bag no
												String[] bagNoArray = bagNo.split(",");

												for (int j = 0; j < bagNoArray.length; j++) {

													/*
													 * Inserting bag details into BagLog table
													 */
													statusMessage = billingDAOInf.insertBagDetails(receiptItemID,
															bagNoArray[j]);

													if (statusMessage.equalsIgnoreCase("success")) {

														System.out.println(
																"Successfully inserted bag details into BagLog table.");

													} else {

														System.out.println(
																"Failed to insert bag details into BagLog table.");

														statusMessage = "error";

													}

												}

											} else {

												/*
												 * Inserting bag details into BagLog table
												 */
												statusMessage = billingDAOInf.insertBagDetails(receiptItemID, bagNo);

												if (statusMessage.equalsIgnoreCase("success")) {

													System.out.println(
															"Successfully inserted bag details into BagLog table.");

												} else {

													System.out
															.println("Failed to insert bag details into BagLog table.");

													statusMessage = "error";

												}

											}

										}

									}

								} else {

									System.out.println("Failed to insert receipt items into ReceiptItems table.");

									statusMessage = "error";

								}

							}

							/*
							 * Checking whether payment type is cheque or cash, if it is cheque, then
							 * inserting cheque details into Cheque details table
							 */
							if (billingForm.getPaymentType().equals("Cheque")
									|| billingForm.getPaymentType().equals("Credit/Debit Card")) {

								statusMessage = billingDAOInf.insertChequeDetails(billingForm);

								if (statusMessage.equalsIgnoreCase("success")) {

									System.out
											.println("Successfully inserted cheque details into ChequeDetails table.");

								} else {

									System.out.println("Failed to insert cheque details into table.");

									statusMessage = "error";

								}

							}

							return statusMessage;

						}

					}

				} else {

					System.out.println("Failed to insert receipt details into Receipt table");

					statusMessage = "error";

					return statusMessage;
				}

			}

		}

	}

	public String editBill(BillingForm billingForm, int userID, String userType) {

		billingDAOInf = new BillingDAOImpl();

		/*
		 * Checking whether user type is administrator or other
		 */
		if (userType.contains(ActivityStatus.ADMINISTRATOR) || userType.contains(ActivityStatus.RECEPTIONIST)) {

			System.out.println("administrator loop");

			/*
			 * Updating receipt details into Receipt table
			 */
			statusMessage = billingDAOInf.updateReceiptDetails(billingForm, userID);

			if (statusMessage.equalsIgnoreCase("success")) {

				/*
				 * Checking whether chargeType is added for billing or not, if added then only
				 * inserting it into ChargeType table
				 */
				if (billingForm.getChargeType() == null) {

					System.out.println("No other charges selected.");

					statusMessage = "success";

				} else {

					/*
					 * Inserting OtherCharges details into OtherCharge table by iterating over
					 * chargeType variable from BillingForm
					 */
					for (int i = 0; i < billingForm.getChargeType().length; i++) {

						statusMessage = billingDAOInf.insertOtherCharge(billingForm.getChargeType()[i],
								Double.parseDouble(billingForm.getOtherCharge()[i]), billingForm.getReceiptID());

						if (statusMessage.equalsIgnoreCase("success")) {

							System.out.println("Other charges inserted successfully into OtehrCharges table.");

						} else {

							System.out.println("Failed to insert other charges details into OtherCharges table.");

							statusMessage = "error";

						}

					}

				}

				/*
				 * Checking whether any of component is added for billing or not, if added then
				 * only insert component details into ReceiptItems table
				 */
				if (billingForm.getComponent() == null) {

					System.out.println("No component selected.");

					statusMessage = "success";

					/*
					 * checking receiptID from ChequeDetails, if yes, update cheque details else
					 * insert cheque details
					 */
					boolean check = billingDAOInf.verifyChequeExists(billingForm.getReceiptID());
					if (check) {

						/*
						 * Checking whether payment type is cheque or cash, if it is cheque, then
						 * updating cheque details into Cheque details table
						 */
						if (billingForm.getPaymentType().equals("Cheque")
								|| billingForm.getPaymentType().equals("Credit/Debit Card")) {

							statusMessage = billingDAOInf.updateChequeDetails(billingForm);

							if (statusMessage.equalsIgnoreCase("success")) {

								System.out.println("Successfully udpated cheque details into ChequeDetails table.");

							} else {

								System.out.println("Failed to udpate cheque details into table.");

								statusMessage = "error";

							}

						}

					} else {

						/*
						 * Checking whether payment type is cheque or cash, if it is cheque, then
						 * inserting cheque details into Cheque details table
						 */
						if (billingForm.getPaymentType().equals("Cheque")
								|| billingForm.getPaymentType().equals("Credit/Debit Card")) {

							statusMessage = billingDAOInf.insertChequeDetails(billingForm);

							if (statusMessage.equalsIgnoreCase("success")) {

								System.out.println("Successfully inserted cheque details into ChequeDetails table.");

							} else {

								System.out.println("Failed to insert cheque details into table.");

								statusMessage = "error";

							}

						}

					}

					return statusMessage;

				} else {

					/*
					 * Inserting component details one by one into ReceiptItems tables
					 */

					/*
					 * Iterating over the component items
					 */
					for (int i = 0; i < billingForm.getComponent().length; i++) {

						statusMessage = billingDAOInf.insertReceiptItems(Integer.parseInt(billingForm.getQuantity()[i]),
								billingForm.getComponent()[i], Double.parseDouble(billingForm.getRate()[i]),
								Double.parseDouble(billingForm.getAmount()[i]), billingForm.getReceiptID());

						if (statusMessage.equalsIgnoreCase("success")) {

							/*
							 * retrieving last inserted receipt item ID
							 */
							int receiptItemID = billingDAOInf.retrieveLastEneteredReceiptItemID();

							if (billingForm.getBagNo() == null) {

								System.out.println("No bags added");

								statusMessage = "success";

							} else {

								/*
								 * check wehther bag added or not, if not then return success else proceed to
								 * add bag details into BagLog table
								 */
								if (billingForm.getBagNo()[i] == null || billingForm.getBagNo()[i] == "") {

									System.out.println("No bags added");

									statusMessage = "success";

								} else {

									String bagNo = billingForm.getBagNo()[i];

									/*
									 * checking whether bag no starts with ',' and if yes removing the first , from
									 * String
									 */
									if (bagNo.startsWith(",")) {
										bagNo = bagNo.substring(1);
									}

									/*
									 * Checking whether bag no contains multiple values seperated by commas, if yes
									 * then iterate over it and insert one by one values into BagLogTable
									 */
									if (bagNo.contains(",")) {

										// Iterate over Bag no
										String[] bagNoArray = bagNo.split(",");

										for (int j = 0; j < bagNoArray.length; j++) {

											/*
											 * Inserting bag details into BagLog table
											 */
											statusMessage = billingDAOInf.insertBagDetails(receiptItemID,
													bagNoArray[j]);

											if (statusMessage.equalsIgnoreCase("success")) {

												System.out.println(
														"Successfully inserted bag details into BagLog table.");

											} else {

												System.out.println("Failed to insert bag details into BagLog table.");

												statusMessage = "error";

											}

										}

									} else {

										/*
										 * Inserting bag details into BagLog table
										 */
										statusMessage = billingDAOInf.insertBagDetails(receiptItemID, bagNo);

										if (statusMessage.equalsIgnoreCase("success")) {

											System.out.println("Successfully inserted bag details into BagLog table.");

										} else {

											System.out.println("Failed to insert bag details into BagLog table.");

											statusMessage = "error";

										}

									}

								}

							}

						} else {

							System.out.println("Failed to insert receipt items into ReceiptItems table.");

							statusMessage = "error";

						}

					}

					/*
					 * checking receiptID from ChequeDetails, if yes, update cheque details else
					 * insert cheque details
					 */
					boolean check = billingDAOInf.verifyChequeExists(billingForm.getReceiptID());
					if (check) {

						/*
						 * Checking whether payment type is cheque or cash, if it is cheque, then
						 * updating cheque details into Cheque details table
						 */
						if (billingForm.getPaymentType().equals("Cheque")
								|| billingForm.getPaymentType().equals("Credit/Debit Card")) {

							statusMessage = billingDAOInf.updateChequeDetails(billingForm);

							if (statusMessage.equalsIgnoreCase("success")) {

								System.out.println("Successfully udpated cheque details into ChequeDetails table.");

							} else {

								System.out.println("Failed to udpate cheque details into table.");

								statusMessage = "error";

							}

						}

					} else {

						/*
						 * Checking whether payment type is cheque or cash, if it is cheque, then
						 * inserting cheque details into Cheque details table
						 */
						if (billingForm.getPaymentType().equals("Cheque")
								|| billingForm.getPaymentType().equals("Credit/Debit Card")) {

							statusMessage = billingDAOInf.insertChequeDetails(billingForm);

							if (statusMessage.equalsIgnoreCase("success")) {

								System.out.println("Successfully inserted cheque details into ChequeDetails table.");

							} else {

								System.out.println("Failed to insert cheque details into table.");

								statusMessage = "error";

							}

						}

					}

					return statusMessage;

				}

			} else {

				System.out.println("Failed to update receipt details into Receipt table");

				statusMessage = "error";

				return statusMessage;

			}

		} else {

			/*
			 * Updating receipt details into Receipt table
			 */
			statusMessage = billingDAOInf.updateReceiptDetails(billingForm, userID);

			if (statusMessage.equalsIgnoreCase("success")) {
				/*
				 * checking receiptID from ChequeDetails, if yes, update cheque details else
				 * insert cheque details
				 */
				boolean check = billingDAOInf.verifyChequeExists(billingForm.getReceiptID());
				if (check) {

					/*
					 * Checking whether payment type is cheque or cash, if it is cheque, then
					 * updating cheque details into Cheque details table
					 */
					if (billingForm.getPaymentType().equals("Cheque")
							|| billingForm.getPaymentType().equals("Credit/Debit Card")) {

						statusMessage = billingDAOInf.updateChequeDetails(billingForm);

						if (statusMessage.equalsIgnoreCase("success")) {

							System.out.println("Successfully udpated cheque details into ChequeDetails table.");

						} else {

							System.out.println("Failed to udpate cheque details into table.");

							statusMessage = "error";

						}

					}

				} else {

					/*
					 * Checking whether payment type is cheque or cash, if it is cheque, then
					 * inserting cheque details into Cheque details table
					 */
					if (billingForm.getPaymentType().equals("Cheque")
							|| billingForm.getPaymentType().equals("Credit/Debit Card")) {

						statusMessage = billingDAOInf.insertChequeDetails(billingForm);

						if (statusMessage.equalsIgnoreCase("success")) {

							System.out.println("Successfully inserted cheque details into ChequeDetails table.");

						} else {

							System.out.println("Failed to insert cheque details into table.");

							statusMessage = "error";

						}

					}

				}

				return statusMessage;
			} else {

				System.out.println("Failed to update receipt details into Receipt table");

				statusMessage = "error";

				return statusMessage;

			}

		}

	}

	public String closeRegister(BillingForm billingForm) {

		billingDAOInf = new BillingDAOImpl();

		/*
		 * Calculating cashMismatchError by subtracting actualEndCash from expectedCash
		 */
		billingForm.setCashMatchingError(billingForm.getExpectedCash() - billingForm.getActualCash());

		// retrieve last cashMismatchcarryOver amount and adding current
		// cashMismatch amount into it in order to get current
		// cashMismatchCarryOver amount
		double currentCashMismatchCaryOverAmt = billingDAOInf.retrieveLastCashMismatchCarryOver()
				+ billingForm.getCashMatchingError();

		System.out.println("Current cash mismatch carry over amount is :: " + currentCashMismatchCaryOverAmt);

		// setting same cash mismatch amount into cashMismatchcarryover
		// amount
		billingForm.setCashMismatchCarryOver(currentCashMismatchCaryOverAmt);

		/*
		 * Updating register
		 */
		statusMessage = billingDAOInf.updateRegister(billingForm);

		return statusMessage;

	}

	public String addOtherProduct(BillingForm billingForm) {

		billingDAOInf = new BillingDAOImpl();

		/*
		 * Inserting into OtherProduct details
		 */
		statusMessage = billingDAOInf.insertOtherProduct(billingForm);

		if (statusMessage.equalsIgnoreCase("success")) {

			return statusMessage;

		} else {

			System.out.println("Failed to insert other product details into OtherProduct table");

			statusMessage = "error";

			return statusMessage;

		}

	}

	public String startRegister(BillingForm billingForm, int userID) {

		billingDAOInf = new BillingDAOImpl();

		/*
		 * checking whether first register entry is already made or not i.e., checking
		 * whether firstRegCheck is 0 or 1, if it is 0, then updating register details
		 * with registerID as 1, else, inserting new records into Register with auto
		 * incremented ID
		 */
		System.out.println("First register check == " + billingForm.getFirstRegisterCheck());
		if (billingForm.getFirstRegisterCheck() == 0) {

			// Calculating cash mismatch amount
			double cashMismatch = billingForm.getExpectedCash() - billingForm.getActualCash();

			billingForm.setCashMatchingError(cashMismatch);

			// setting same cash mismatch amount into cashMismatchcarryover
			// amount
			billingForm.setCashMismatchCarryOver(cashMismatch);

			// updating register details with ID as 1
			statusMessage = billingDAOInf.udpatedFirstRegister(1, userID, billingForm);

			if (statusMessage.equals("success")) {

				return statusMessage;

			} else {

				System.out.println("Failed to update first register deails into Register table.");

				statusMessage = "error";

				return statusMessage;

			}

		} else {

			// setting current expected cash balance as cashHandover amount
			billingForm.setCashHandover(billingForm.getExpectedCash());

			System.out.println("Current cash handover ::: " + billingForm.getCashHandover());

			// calculating new expected cash by adding current cashHandover,
			// bloodBankBalance and otherProductBalance into it and subtracting
			// bankDeposite amount from it
			double expectedCash = billingForm.getCashHandover() + billingForm.getBloodBankBalance()
					+ billingForm.getOtherProductBalance() - billingForm.getBankDeposite();

			billingForm.setExpectedCash(expectedCash);

			System.out.println("Expected cash balance :: " + billingForm.getExpectedCash());

			// Calculating cash mismatch amount
			double cashMismatch = billingForm.getExpectedCash() - billingForm.getActualCash();

			billingForm.setCashMatchingError(cashMismatch);

			// retrieve last cashMismatchcarryOver amount and adding current
			// cashMismatch amount into it in order to get current
			// cashMismatchCarryOver amount
			double currentCashMismatchCaryOverAmt = billingDAOInf.retrieveLastCashMismatchCarryOver() + cashMismatch;

			System.out.println("Current cash mismatch carry over amount is :: " + currentCashMismatchCaryOverAmt);

			// setting same cash mismatch amount into cashMismatchcarryover
			// amount
			billingForm.setCashMismatchCarryOver(currentCashMismatchCaryOverAmt);

			// inserting register details into Register table
			statusMessage = billingDAOInf.insertRegister(billingForm, userID);

			if (statusMessage.equals("success")) {

				return statusMessage;

			} else {

				System.out.println("Failed to insert register deails into Register table.");

				statusMessage = "error";

				return statusMessage;

			}

		}

	}

	public JSONObject retrieveCloseRegisterDetails(int registerID) {

		billingDAOInf = new BillingDAOImpl();

		JSONObject values = new JSONObject();

		JSONArray array = new JSONArray();

		JSONObject object = new JSONObject();

		try {

			/*
			 * Retrieving register start date and time based on registerID
			 */
			String startDateAndTime = billingDAOInf.retrieveRegisterStartDateAndTime(registerID);

			/*
			 * Retrieve blood bank balance from Receipt table based on register start date
			 * and time
			 */
			double bloodBankBalance = billingDAOInf.retrieveBloodBankBalance(startDateAndTime);

			System.out.println("Blood bank balance :: " + bloodBankBalance);

			/*
			 * Retrieving other product balance from OtherProducts table on register start
			 * date and time
			 */
			double otherProductBalance = billingDAOInf.retrieveOtherProductBalance(startDateAndTime);

			System.out.println("Other product balance :: " + otherProductBalance);

			/*
			 * Retrieving total cash deposited from BankDeposit table based on registerID
			 */
			double totalCashDeposited = billingDAOInf.retrieveTotalCashDepositedByRegisterID(registerID);

			/*
			 * Retrieving total cash adjusted from CashAdjustment table based on registerID
			 */
			double totalCashAdjusted = billingDAOInf.retrieveTotalCashAdjustedByRegisterID(registerID);

			System.out.println("Cash Deposited :: " + totalCashDeposited + "\nCash Adjusted :: " + totalCashAdjusted);

			/*
			 * Retrieving cashHandover for registerID from Register table
			 */
			double cashHandover = billingDAOInf.retrieveCashHandoverByRegisterID(registerID);

			System.out.println("Cash handover for register ID :: " + cashHandover);

			/*
			 * calculating expected cash balance by adding cashHandover, bloodBankBalance,
			 * otherProductBalance, cashAdjusted and subtracting bankDeposit from it
			 */
			double expectedCash = cashHandover + bloodBankBalance + otherProductBalance - totalCashDeposited
					+ totalCashAdjusted;

			System.out.println("Expected cash :: " + expectedCash);

			object.put("cashHandover", cashHandover);
			object.put("bloodBankBalance", bloodBankBalance);
			object.put("otherProductBalance", otherProductBalance);
			object.put("totalCashDeposited", totalCashDeposited);
			object.put("totalCashAdjusted", totalCashAdjusted);
			object.put("expectedCash", expectedCash);
			object.put("registerID", registerID);

			array.add(object);

			values.put("Release", array);

			return values;

		} catch (Exception exception) {
			exception.printStackTrace();

			object.put("ErrMsg", "Exception occurred while retrieving register details for closing.");

			array.add(object);

			values.put("Release", array);

			return values;
		}

	}

	public String addNewBill(BillingForm billingForm, int userID) {

		billingDAOInf = new BillingDAOImpl();

		int patientID = 0;

		System.out.println("BB Storage ... " + billingForm.getBbStorageCenter());

		if (!billingForm.getBbStorageCenter().equals("000")) {

			System.out.println("inside bb storage ");

			/*
			 * verifying whether bsc already exists into Patient table or not, if exists,
			 * then retrieving patientID based on BSC else inserting new record with new BSC
			 * name
			 */
			boolean check = billingDAOInf.verifyBloodStorageExists(billingForm.getBbStorageCenter());

			if (check) {

				/*
				 * Retrieving patientID based on bb storage center number
				 */
				patientID = billingDAOInf.retrievePatientIdByBloodBankStorageCenter(billingForm.getBbStorageCenter());

				if (patientID == 0) {

					System.out.println("Failed to retrieve patient ID based on blood bank storage center");

					statusMessage = "error";

					return statusMessage;

				} else {

					System.out.println(
							"Successfully retrieved patient ID based on blood bank storage center is ::: " + patientID);

					/*
					 * Setting retrieved patientID into BillingForm patientID variable
					 */
					billingForm.setPatientID(patientID);

					/*
					 * Inserting receipt details into Receipt table
					 */
					statusMessage = billingDAOInf.insertReceiptDetails(billingForm, userID, patientID);

					if (statusMessage.equalsIgnoreCase("success")) {

						/*
						 * Retrieving receipt ID based on receipt no
						 */
						int receiptID = billingDAOInf.retrieveReceiptIDBasedOnReceiptNo(billingForm.getReceiptNo());

						if (receiptID == 0) {

							System.out.println("Failed to retrieve receipt ID based on Receipt no.");

							statusMessage = "error";

							return statusMessage;

						} else {

							System.out
									.println("Successfully retrieve receipt ID based on receipt no is :: " + receiptID);

							/*
							 * Setting retrieved receipt ID into Billing Form receiptID variable
							 */
							billingForm.setReceiptID(receiptID);

							/*
							 * Checking whether chargeType is added for billing or not, if added then only
							 * inserting it into ChargeType table
							 */
							if (billingForm.getChargeType() == null) {

								System.out.println("No other charges selected.");

								statusMessage = "success";

							} else {

								/*
								 * Inserting OtherCharges details into OtherCharge table by iterating over
								 * chargeType variable from BillingForm
								 */
								for (int i = 0; i < billingForm.getChargeType().length; i++) {

									statusMessage = billingDAOInf.insertOtherCharge(billingForm.getChargeType()[i],
											Double.parseDouble(billingForm.getOtherCharge()[i]), receiptID);

									if (statusMessage.equalsIgnoreCase("success")) {

										System.out.println(
												"Other charges inserted successfully into OtehrCharges table.");

									} else {

										System.out.println(
												"Failed to insert other charges details into OtherCharges table.");

										statusMessage = "error";

									}

								}

							}

							/*
							 * Checking whether any of component is added for billing or not, if added then
							 * only insert component details into ReceiptItems table
							 */
							if (billingForm.getComponent() == null) {

								System.out.println("No component selected.");

								statusMessage = "success";

								/*
								 * Checking whether payment type is cheque or cash, if it is cheque, then
								 * inserting cheque details into Cheque details table
								 */
								if (billingForm.getPaymentType().equals("Cheque")
										|| billingForm.getPaymentType().equals("Credit/Debit Card")) {

									statusMessage = billingDAOInf.insertChequeDetails(billingForm);

									if (statusMessage.equalsIgnoreCase("success")) {

										System.out.println(
												"Successfully inserted cheque details into ChequeDetails table.");

									} else {

										System.out.println("Failed to insert cheque details into table.");

										statusMessage = "error";

									}

								}

								return statusMessage;

							} else {

								/*
								 * Inserting component details one by one into ReceiptItems tables
								 */

								/*
								 * Iterating over the component items
								 */
								for (int i = 0; i < billingForm.getComponent().length; i++) {

									statusMessage = billingDAOInf.insertReceiptItems(
											Integer.parseInt(billingForm.getQuantity()[i]),
											billingForm.getComponent()[i], Double.parseDouble(billingForm.getRate()[i]),
											Double.parseDouble(billingForm.getAmount()[i]), receiptID);

									if (statusMessage.equalsIgnoreCase("success")) {

										/*
										 * retrieving last inserted receipt item ID
										 */
										int receiptItemID = billingDAOInf.retrieveLastEneteredReceiptItemID();

										if (billingForm.getBagNo() == null) {

											System.out.println("No bags added");

											statusMessage = "success";

										} else {

											/*
											 * check wehther bag added or not, if not then return success else proceed
											 * to add bag details into BagLog table
											 */
											if (billingForm.getBagNo()[i] == null || billingForm.getBagNo()[i] == "") {

												System.out.println("No bags added");

												statusMessage = "success";

											} else {

												String bagNo = billingForm.getBagNo()[i];

												/*
												 * checking whether bag no starts with ',' and if yes removing the first
												 * , from String
												 */
												if (bagNo.startsWith(",")) {
													bagNo = bagNo.substring(1);
												}

												/*
												 * Checking whether bag no contains multiple values seperated by commas,
												 * if yes then iterate over it and insert one by one values into
												 * BagLogTable
												 */
												if (bagNo.contains(",")) {

													// Iterate over Bag no
													String[] bagNoArray = bagNo.split(",");

													for (int j = 0; j < bagNoArray.length; j++) {

														/*
														 * Inserting bag details into BagLog table
														 */
														statusMessage = billingDAOInf.insertBagDetails(receiptItemID,
																bagNoArray[j]);

														if (statusMessage.equalsIgnoreCase("success")) {

															System.out.println(
																	"Successfully inserted bag details into BagLog table.");

														} else {

															System.out.println(
																	"Failed to insert bag details into BagLog table.");

															statusMessage = "error";

														}

													}

												} else {

													/*
													 * Inserting bag details into BagLog table
													 */
													statusMessage = billingDAOInf.insertBagDetails(receiptItemID,
															bagNo);

													if (statusMessage.equalsIgnoreCase("success")) {

														System.out.println(
																"Successfully inserted bag details into BagLog table.");

													} else {

														System.out.println(
																"Failed to insert bag details into BagLog table.");

														statusMessage = "error";

													}

												}

											}

										}

									} else {

										System.out.println("Failed to insert receipt items into ReceiptItems table.");

										statusMessage = "error";

									}

								}

								/*
								 * Checking whether payment type is cheque or cash, if it is cheque, then
								 * inserting cheque details into Cheque details table
								 */
								if (billingForm.getPaymentType().equals("Cheque")
										|| billingForm.getPaymentType().equals("Credit/Debit Card")) {

									statusMessage = billingDAOInf.insertChequeDetails(billingForm);

									if (statusMessage.equalsIgnoreCase("success")) {

										System.out.println(
												"Successfully inserted cheque details into ChequeDetails table.");

									} else {

										System.out.println("Failed to insert cheque details into table.");

										statusMessage = "error";

									}

								}

								return statusMessage;

							}

						}

					} else {

						System.out.println("Failed to insert receipt details into Receipt table");

						statusMessage = "error";

						return statusMessage;
					}

				}

			} else {

				/*
				 * Inserting patient details into Patient table
				 */
				statusMessage = billingDAOInf.insertPatientDetails(billingForm);

				if (statusMessage.equalsIgnoreCase("success")) {

					/*
					 * Retrieving patientID based on bb storage center number
					 */
					patientID = billingDAOInf
							.retrievePatientIdByBloodBankStorageCenter(billingForm.getBbStorageCenter());

					if (patientID == 0) {

						System.out.println("Failed to retrieve patient ID based on blood bank storage center");

						statusMessage = "error";

						return statusMessage;

					} else {

						System.out
								.println("Successfully retrieved patient ID based on blood bank storage center is ::: "
										+ patientID);

						/*
						 * Setting retrieved patientID into BillingForm patientID variable
						 */
						billingForm.setPatientID(patientID);

						/*
						 * Inserting receipt details into Receipt table
						 */
						statusMessage = billingDAOInf.insertReceiptDetails(billingForm, userID, patientID);

						if (statusMessage.equalsIgnoreCase("success")) {

							/*
							 * Retrieving receipt ID based on receipt no
							 */
							int receiptID = billingDAOInf.retrieveReceiptIDBasedOnReceiptNo(billingForm.getReceiptNo());

							if (receiptID == 0) {

								System.out.println("Failed to retrieve receipt ID based on Receipt no.");

								statusMessage = "error";

								return statusMessage;

							} else {

								System.out.println(
										"Successfully retrieve receipt ID based on receipt no is :: " + receiptID);

								/*
								 * Setting retrieved receipt ID into Billing Form receiptID variable
								 */
								billingForm.setReceiptID(receiptID);

								/*
								 * Checking whether chargeType is added for billing or not, if added then only
								 * inserting it into ChargeType table
								 */
								if (billingForm.getChargeType() == null) {

									System.out.println("No other charges selected.");

									statusMessage = "success";

								} else {

									/*
									 * Inserting OtherCharges details into OtherCharge table by iterating over
									 * chargeType variable from BillingForm
									 */
									for (int i = 0; i < billingForm.getChargeType().length; i++) {

										statusMessage = billingDAOInf.insertOtherCharge(billingForm.getChargeType()[i],
												Double.parseDouble(billingForm.getOtherCharge()[i]), receiptID);

										if (statusMessage.equalsIgnoreCase("success")) {

											System.out.println(
													"Other charges inserted successfully into OtehrCharges table.");

										} else {

											System.out.println(
													"Failed to insert other charges details into OtherCharges table.");

											statusMessage = "error";

										}

									}

								}

								/*
								 * Checking whether any of component is added for billing or not, if added then
								 * only insert component details into ReceiptItems table
								 */
								if (billingForm.getComponent() == null) {

									System.out.println("No component selected.");

									statusMessage = "success";

									/*
									 * Checking whether payment type is cheque or cash, if it is cheque, then
									 * inserting cheque details into Cheque details table
									 */
									if (billingForm.getPaymentType().equals("Cheque")
											|| billingForm.getPaymentType().equals("Credit/Debit Card")) {

										statusMessage = billingDAOInf.insertChequeDetails(billingForm);

										if (statusMessage.equalsIgnoreCase("success")) {

											System.out.println(
													"Successfully inserted cheque details into ChequeDetails table.");

										} else {

											System.out.println("Failed to insert cheque details into table.");

											statusMessage = "error";

										}

									}

									return statusMessage;

								} else {

									/*
									 * Inserting component details one by one into ReceiptItems tables
									 */

									/*
									 * Iterating over the component items
									 */
									for (int i = 0; i < billingForm.getComponent().length; i++) {

										statusMessage = billingDAOInf.insertReceiptItems(
												Integer.parseInt(billingForm.getQuantity()[i]),
												billingForm.getComponent()[i],
												Double.parseDouble(billingForm.getRate()[i]),
												Double.parseDouble(billingForm.getAmount()[i]), receiptID);

										if (statusMessage.equalsIgnoreCase("success")) {

											/*
											 * retrieving last inserted receipt item ID
											 */
											int receiptItemID = billingDAOInf.retrieveLastEneteredReceiptItemID();

											if (billingForm.getBagNo() == null) {

												System.out.println("No bags added");

												statusMessage = "success";

											} else {

												/*
												 * check wehther bag added or not, if not then return success else
												 * proceed to add bag details into BagLog table
												 */
												if (billingForm.getBagNo()[i] == null
														|| billingForm.getBagNo()[i] == "") {

													System.out.println("No bags added");

													statusMessage = "success";

												} else {

													String bagNo = billingForm.getBagNo()[i];

													/*
													 * checking whether bag no starts with ',' and if yes removing the
													 * first , from String
													 */
													if (bagNo.startsWith(",")) {
														bagNo = bagNo.substring(1);
													}

													/*
													 * Checking whether bag no contains multiple values seperated by
													 * commas, if yes then iterate over it and insert one by one values
													 * into BagLogTable
													 */
													if (bagNo.contains(",")) {

														// Iterate over Bag no
														String[] bagNoArray = bagNo.split(",");

														for (int j = 0; j < bagNoArray.length; j++) {

															/*
															 * Inserting bag details into BagLog table
															 */
															statusMessage = billingDAOInf
																	.insertBagDetails(receiptItemID, bagNoArray[j]);

															if (statusMessage.equalsIgnoreCase("success")) {

																System.out.println(
																		"Successfully inserted bag details into BagLog table.");

															} else {

																System.out.println(
																		"Failed to insert bag details into BagLog table.");

																statusMessage = "error";

															}

														}

													} else {

														/*
														 * Inserting bag details into BagLog table
														 */
														statusMessage = billingDAOInf.insertBagDetails(receiptItemID,
																bagNo);

														if (statusMessage.equalsIgnoreCase("success")) {

															System.out.println(
																	"Successfully inserted bag details into BagLog table.");

														} else {

															System.out.println(
																	"Failed to insert bag details into BagLog table.");

															statusMessage = "error";

														}

													}

												}

											}

										} else {

											System.out
													.println("Failed to insert receipt items into ReceiptItems table.");

											statusMessage = "error";

										}

									}

									/*
									 * Checking whether payment type is cheque or cash, if it is cheque, then
									 * inserting cheque details into Cheque details table
									 */
									if (billingForm.getPaymentType().equals("Cheque")
											|| billingForm.getPaymentType().equals("Credit/Debit Card")) {

										statusMessage = billingDAOInf.insertChequeDetails(billingForm);

										if (statusMessage.equalsIgnoreCase("success")) {

											System.out.println(
													"Successfully inserted cheque details into ChequeDetails table.");

										} else {

											System.out.println("Failed to insert cheque details into table.");

											statusMessage = "error";

										}

									}

									return statusMessage;

								}

							}

						} else {

							System.out.println("Failed to insert receipt details into Receipt table");

							statusMessage = "error";

							return statusMessage;
						}

					}

				} else {

					System.out.println("Failed to insert patient details into table.");

					statusMessage = "error";

					return statusMessage;
				}
			}

		} else {

			/*
			 * Verify patientIdentifier already exist into Patient or not, if not then only
			 * insert into Patient table otherwise fetch patientID on the basis of
			 * patientIdentifier and insert that patientID into Receipt table
			 */

			patientID = billingDAOInf.retrievePatientIDOnVerifyPatientCode(billingForm.getPatientIDNo());

			if (patientID == 0) {

				/*
				 * Inserting patient details into Patient table
				 */
				statusMessage = billingDAOInf.insertPatientDetails(billingForm);

				if (statusMessage.equalsIgnoreCase("success")) {

					/*
					 * Retrieving patientID based on patientIdentifier number
					 */
					patientID = billingDAOInf.retrievePatientIDBasedOnIndentifierNo(billingForm.getPatientIDNo());

					if (patientID == 0) {

						System.out.println("Failed to retrieve patient ID based on indentifier no.");

						statusMessage = "error";

						return statusMessage;

					} else {

						System.out.println(
								"Successfully retrieved patient ID based on indentifier no is ::: " + patientID);

						/*
						 * Setting retrieved patientID into BillingForm patientID variable
						 */
						billingForm.setPatientID(patientID);

						/*
						 * Inserting receipt details into Receipt table
						 */
						statusMessage = billingDAOInf.insertReceiptDetails(billingForm, userID, patientID);

						if (statusMessage.equalsIgnoreCase("success")) {

							/*
							 * Retrieving receipt ID based on receipt no
							 */
							int receiptID = billingDAOInf.retrieveReceiptIDBasedOnReceiptNo(billingForm.getReceiptNo());

							if (receiptID == 0) {

								System.out.println("Failed to retrieve receipt ID based on Receipt no.");

								statusMessage = "error";

								return statusMessage;

							} else {

								System.out.println(
										"Successfully retrieve receipt ID based on receipt no is :: " + receiptID);

								/*
								 * Setting retrieved receipt ID into Billing Form receiptID variable
								 */
								billingForm.setReceiptID(receiptID);

								/*
								 * Checking whether chargeType is added for billing or not, if added then only
								 * inserting it into ChargeType table
								 */
								if (billingForm.getChargeType() == null) {

									System.out.println("No other charges selected.");

									statusMessage = "success";

								} else {

									/*
									 * Inserting OtherCharges details into OtherCharge table by iterating over
									 * chargeType variable from BillingForm
									 */
									for (int i = 0; i < billingForm.getChargeType().length; i++) {

										statusMessage = billingDAOInf.insertOtherCharge(billingForm.getChargeType()[i],
												Double.parseDouble(billingForm.getOtherCharge()[i]), receiptID);

										if (statusMessage.equalsIgnoreCase("success")) {

											System.out.println(
													"Other charges inserted successfully into OtehrCharges table.");

										} else {

											System.out.println(
													"Failed to insert other charges details into OtherCharges table.");

											statusMessage = "error";

										}

									}

								}

								/*
								 * Checking whether any of component is added for billing or not, if added then
								 * only insert component details into ReceiptItems table
								 */
								if (billingForm.getComponent() == null) {

									System.out.println("No component selected.");

									statusMessage = "success";

									/*
									 * Checking whether payment type is cheque or cash, if it is cheque, then
									 * inserting cheque details into Cheque details table
									 */
									if (billingForm.getPaymentType().equals("Cheque")
											|| billingForm.getPaymentType().equals("Credit/Debit Card")) {

										statusMessage = billingDAOInf.insertChequeDetails(billingForm);

										if (statusMessage.equalsIgnoreCase("success")) {

											System.out.println(
													"Successfully inserted cheque details into ChequeDetails table.");

										} else {

											System.out.println("Failed to insert cheque details into table.");

											statusMessage = "error";

										}

									}

									return statusMessage;

								} else {

									/*
									 * Inserting component details one by one into ReceiptItems tables
									 */

									/*
									 * Iterating over the component items
									 */
									for (int i = 0; i < billingForm.getComponent().length; i++) {

										statusMessage = billingDAOInf.insertReceiptItems(
												Integer.parseInt(billingForm.getQuantity()[i]),
												billingForm.getComponent()[i],
												Double.parseDouble(billingForm.getRate()[i]),
												Double.parseDouble(billingForm.getAmount()[i]), receiptID);

										if (statusMessage.equalsIgnoreCase("success")) {

											/*
											 * retrieving last inserted receipt item ID
											 */
											int receiptItemID = billingDAOInf.retrieveLastEneteredReceiptItemID();

											if (billingForm.getBagNo() == null) {

												System.out.println("No bags added");

												statusMessage = "success";

											} else {

												/*
												 * check wehther bag added or not, if not then return success else
												 * proceed to add bag details into BagLog table
												 */
												if (billingForm.getBagNo()[i] == null
														|| billingForm.getBagNo()[i] == "") {

													System.out.println("No bags added");

													statusMessage = "success";

												} else {

													String bagNo = billingForm.getBagNo()[i];

													/*
													 * checking whether bag no starts with ',' and if yes removing the
													 * first , from String
													 */
													if (bagNo.startsWith(",")) {
														bagNo = bagNo.substring(1);
													}

													/*
													 * Checking whether bag no contains multiple values seperated by
													 * commas, if yes then iterate over it and insert one by one values
													 * into BagLogTable
													 */
													if (bagNo.contains(",")) {

														// Iterate over Bag no
														String[] bagNoArray = bagNo.split(",");

														for (int j = 0; j < bagNoArray.length; j++) {

															/*
															 * Inserting bag details into BagLog table
															 */
															statusMessage = billingDAOInf
																	.insertBagDetails(receiptItemID, bagNoArray[j]);

															if (statusMessage.equalsIgnoreCase("success")) {

																System.out.println(
																		"Successfully inserted bag details into BagLog table.");

															} else {

																System.out.println(
																		"Failed to insert bag details into BagLog table.");

																statusMessage = "error";

															}

														}

													} else {

														/*
														 * Inserting bag details into BagLog table
														 */
														statusMessage = billingDAOInf.insertBagDetails(receiptItemID,
																bagNo);

														if (statusMessage.equalsIgnoreCase("success")) {

															System.out.println(
																	"Successfully inserted bag details into BagLog table.");

														} else {

															System.out.println(
																	"Failed to insert bag details into BagLog table.");

															statusMessage = "error";

														}

													}

												}

											}

										} else {

											System.out
													.println("Failed to insert receipt items into ReceiptItems table.");

											statusMessage = "error";

										}

									}

									/*
									 * Checking whether payment type is cheque or cash, if it is cheque, then
									 * inserting cheque details into Cheque details table
									 */
									if (billingForm.getPaymentType().equals("Cheque")
											|| billingForm.getPaymentType().equals("Credit/Debit Card")) {

										statusMessage = billingDAOInf.insertChequeDetails(billingForm);

										if (statusMessage.equalsIgnoreCase("success")) {

											System.out.println(
													"Successfully inserted cheque details into ChequeDetails table.");

										} else {

											System.out.println("Failed to insert cheque details into table.");

											statusMessage = "error";

										}

									}

									return statusMessage;

								}

							}

						} else {

							System.out.println("Failed to insert receipt details into Receipt table");

							statusMessage = "error";

							return statusMessage;
						}

					}

				} else {

					System.out.println("Failed to insert patient details into table.");

					statusMessage = "error";

					return statusMessage;
				}

			} else {

				System.out.println("Successfully retrieved patient ID based on indentifier no is ::: " + patientID);

				/*
				 * Setting retrieved patientID into BillingForm patientID variable
				 */
				billingForm.setPatientID(patientID);

				/*
				 * Inserting receipt details into Receipt table
				 */
				statusMessage = billingDAOInf.insertReceiptDetails(billingForm, userID, patientID);

				if (statusMessage.equalsIgnoreCase("success")) {

					/*
					 * Retrieving receipt ID based on receipt no
					 */
					int receiptID = billingDAOInf.retrieveReceiptIDBasedOnReceiptNo(billingForm.getReceiptNo());

					if (receiptID == 0) {

						System.out.println("Failed to retrieve receipt ID based on Receipt no.");

						statusMessage = "error";

						return statusMessage;

					} else {

						System.out.println("Successfully retrieve receipt ID based on receipt no is :: " + receiptID);

						/*
						 * Setting retrieved receipt ID into Billing Form receiptID variable
						 */
						billingForm.setReceiptID(receiptID);

						/*
						 * Checking whether chargeType is added for billing or not, if added then only
						 * inserting it into ChargeType table
						 */
						if (billingForm.getChargeType() == null) {

							System.out.println("No other charges selected.");

							statusMessage = "success";

						} else {

							/*
							 * Inserting OtherCharges details into OtherCharge table by iterating over
							 * chargeType variable from BillingForm
							 */
							for (int i = 0; i < billingForm.getChargeType().length; i++) {

								statusMessage = billingDAOInf.insertOtherCharge(billingForm.getChargeType()[i],
										Double.parseDouble(billingForm.getOtherCharge()[i]), receiptID);

								if (statusMessage.equalsIgnoreCase("success")) {

									System.out.println("Other charges inserted successfully into OtehrCharges table.");

								} else {

									System.out
											.println("Failed to insert other charges details into OtherCharges table.");

									statusMessage = "error";

								}

							}

						}

						/*
						 * Checking whether any of component is added for billing or not, if added then
						 * only insert component details into ReceiptItems table
						 */
						if (billingForm.getComponent() == null) {

							System.out.println("No component selected.");

							statusMessage = "success";

							/*
							 * Checking whether payment type is cheque or cash, if it is cheque, then
							 * inserting cheque details into Cheque details table
							 */
							if (billingForm.getPaymentType().equals("Cheque")
									|| billingForm.getPaymentType().equals("Credit/Debit Card")) {

								statusMessage = billingDAOInf.insertChequeDetails(billingForm);

								if (statusMessage.equalsIgnoreCase("success")) {

									System.out
											.println("Successfully inserted cheque details into ChequeDetails table.");

								} else {

									System.out.println("Failed to insert cheque details into table.");

									statusMessage = "error";

								}

							}

							return statusMessage;

						} else {

							/*
							 * Inserting component details one by one into ReceiptItems tables
							 */

							/*
							 * Iterating over the component items
							 */
							for (int i = 0; i < billingForm.getComponent().length; i++) {

								statusMessage = billingDAOInf.insertReceiptItems(
										Integer.parseInt(billingForm.getQuantity()[i]), billingForm.getComponent()[i],
										Double.parseDouble(billingForm.getRate()[i]),
										Double.parseDouble(billingForm.getAmount()[i]), receiptID);

								if (statusMessage.equalsIgnoreCase("success")) {

									/*
									 * retrieving last inserted receipt item ID
									 */
									int receiptItemID = billingDAOInf.retrieveLastEneteredReceiptItemID();

									if (billingForm.getBagNo() == null) {

										System.out.println("No bags added");

										statusMessage = "success";

									} else {

										/*
										 * check wehther bag added or not, if not then return success else proceed to
										 * add bag details into BagLog table
										 */
										if (billingForm.getBagNo()[i] == null || billingForm.getBagNo()[i] == "") {

											System.out.println("No bags added");

											statusMessage = "success";

										} else {

											String bagNo = billingForm.getBagNo()[i];

											/*
											 * checking whether bag no starts with ',' and if yes removing the first ,
											 * from String
											 */
											if (bagNo.startsWith(",")) {
												bagNo = bagNo.substring(1);
											}

											/*
											 * Checking whether bag no contains multiple values seperated by commas, if
											 * yes then iterate over it and insert one by one values into BagLogTable
											 */
											if (bagNo.contains(",")) {

												// Iterate over Bag no
												String[] bagNoArray = bagNo.split(",");

												for (int j = 0; j < bagNoArray.length; j++) {

													/*
													 * Inserting bag details into BagLog table
													 */
													statusMessage = billingDAOInf.insertBagDetails(receiptItemID,
															bagNoArray[j]);

													if (statusMessage.equalsIgnoreCase("success")) {

														System.out.println(
																"Successfully inserted bag details into BagLog table.");

													} else {

														System.out.println(
																"Failed to insert bag details into BagLog table.");

														statusMessage = "error";

													}

												}

											} else {

												/*
												 * Inserting bag details into BagLog table
												 */
												statusMessage = billingDAOInf.insertBagDetails(receiptItemID, bagNo);

												if (statusMessage.equalsIgnoreCase("success")) {

													System.out.println(
															"Successfully inserted bag details into BagLog table.");

												} else {

													System.out
															.println("Failed to insert bag details into BagLog table.");

													statusMessage = "error";

												}

											}

										}

									}

								} else {

									System.out.println("Failed to insert receipt items into ReceiptItems table.");

									statusMessage = "error";

								}

							}

							/*
							 * Checking whether payment type is cheque or cash, if it is cheque, then
							 * inserting cheque details into Cheque details table
							 */
							if (billingForm.getPaymentType().equals("Cheque")
									|| billingForm.getPaymentType().equals("Credit/Debit Card")) {

								statusMessage = billingDAOInf.insertChequeDetails(billingForm);

								if (statusMessage.equalsIgnoreCase("success")) {

									System.out
											.println("Successfully inserted cheque details into ChequeDetails table.");

								} else {

									System.out.println("Failed to insert cheque details into table.");

									statusMessage = "error";

								}

							}

							return statusMessage;

						}

					}

				} else {

					System.out.println("Failed to insert receipt details into Receipt table");

					statusMessage = "error";

					return statusMessage;
				}

			}

		}

	}
}
