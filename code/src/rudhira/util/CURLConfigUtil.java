package rudhira.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class CURLConfigUtil {

	String status = "error";

	static ConfigXMLUtil configXMLUtil = new ConfigXMLUtil();

	static String tallyIP = configXMLUtil.getTallyIPAddress();

	static String tallyPort = configXMLUtil.getTallyPort();

	public String sendToTally(String filePath) {

		try {

			File file = new File(filePath);

			if (file.exists()) {

				status = checkConnection();

				if (status.equals("success")) {

					String curlComand = "curl -X POST " + tallyIP + ":" + tallyPort + " --data @" + filePath;

					System.out.println("CURL Command ... " + curlComand);

					Runtime runtime = Runtime.getRuntime();

					Process process = runtime.exec(curlComand);

					BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
					String line = null;
					String newline = "";
					System.out.println("----------------LOG --------------------");
					while ((line = reader.readLine()) != null) {
						newline += line;
						System.out.println(line);
					}
					System.out.println("-----------------------------------");
					// System.out.println(newline);

					if (newline == "" || newline == null) {
						System.out.println(
								"Curl not executed.Either curl command is incorrect or tally server is shut down.");

						status = "input";
					} else if (newline.isEmpty()) {
						System.out.println(
								"Curl not executed.Either curl command is incorrect or tally server is shut down.");

						status = "input";
					} else if (newline.contains("<CREATED>1</CREATED>")) {
						System.out.println("Voucher created successfully....");

						status = "success";
					} else if (newline.contains("<LINEERROR>")) {
						System.out.println("Failed to create voucher....");

						status = "failed";
					} else if (newline.contains("<ERRORS>1</ERRORS>")) {
						System.out.println("Failed to create voucher....");

						status = "failed";
					}

					reader.close();
					process.destroy();

					return status;

				} else {
					System.out.println("Tally server is shut down. Please check tally server is connected or not.");

					return status;
				}

			} else {
				System.out.println("XML File not found.");

				status = "xmlNotFound";

				return status;
			}

		} catch (Exception exception) {
			exception.printStackTrace();
			status = "exception";
		}

		return status;
	}

	public static String checkConnection() {
		String check = "error";

		try {

			String url1 = "http://" + tallyIP + ":" + tallyPort;

			URL obj = new URL(url1);
			HttpURLConnection conn = (HttpURLConnection) obj.openConnection();

			System.out.println("Response message .. " + conn.getResponseMessage());
			System.out.println("Response Code .. " + conn.getResponseCode());

			if (conn.getResponseCode() == 200 || conn.getResponseMessage().equals("OK")) {
				check = "success";
			}

		} catch (Exception exception) {
			exception.printStackTrace();
			check = "error";
		}

		return check;
	}

}
