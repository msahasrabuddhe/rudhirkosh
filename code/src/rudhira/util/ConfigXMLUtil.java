package rudhira.util;

import java.io.File;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

public class ConfigXMLUtil {

	HttpServletRequest request = ServletActionContext.getRequest();

	/**
	 * 
	 * @return
	 */
	public String getDBIP() {
		String DBIP = null;

		String realPath = request.getServletContext().getRealPath("/");

		File xmlFile = new File(realPath + File.separator + "configuration.xml");

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("rudhira-config");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					DBIP = eElement.getElementsByTagName("DB-IP").item(0).getTextContent();
				}
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return DBIP;
	}

	/**
	 * 
	 * @return
	 */
	public String getDBPort() {

		String DBPort = null;

		String realPath = request.getServletContext().getRealPath("/");

		File xmlFile = new File(realPath + File.separator + "configuration.xml");

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("rudhira-config");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					DBPort = eElement.getElementsByTagName("DB-Port").item(0).getTextContent();
				}
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return DBPort;

	}

	/**
	 * 
	 * @return
	 */
	public String getDBUser() {

		String DBUser = null;

		String realPath = request.getServletContext().getRealPath("/");

		File xmlFile = new File(realPath + File.separator + "configuration.xml");

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("rudhira-config");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					DBUser = eElement.getElementsByTagName("DB-Username").item(0).getTextContent();
				}
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return DBUser;

	}

	/**
	 * 
	 * @return
	 */
	public String getDBName() {

		String DBName = null;

		String realPath = request.getServletContext().getRealPath("/");

		File xmlFile = new File(realPath + File.separator + "configuration.xml");

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("rudhira-config");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					DBName = eElement.getElementsByTagName("DB-Name").item(0).getTextContent();
				}
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return DBName;

	}

	/**
	 * 
	 * @return
	 */
	public String getDBPass() {

		String DBPass = null;

		String realPath = request.getServletContext().getRealPath("/");

		File xmlFile = new File(realPath + File.separator + "configuration.xml");

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("rudhira-config");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					DBPass = eElement.getElementsByTagName("DB-Password").item(0).getTextContent();
				}
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return DBPass;

	}

	/**
	 * 
	 * @return
	 */
	public String getJankalyanDBIP() {
		String DBIP = null;

		String realPath = request.getServletContext().getRealPath("/");

		File xmlFile = new File(realPath + File.separator + "configuration.xml");

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("rudhira-config");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					DBIP = eElement.getElementsByTagName("Jankalyan-DB-IP").item(0).getTextContent();
				}
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return DBIP;
	}

	/**
	 * 
	 * @return
	 */
	public String getJankalyanDBPort() {

		String DBPort = null;

		String realPath = request.getServletContext().getRealPath("/");

		File xmlFile = new File(realPath + File.separator + "configuration.xml");

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("rudhira-config");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					DBPort = eElement.getElementsByTagName("Jankalyan-DB-Port").item(0).getTextContent();
				}
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return DBPort;

	}

	/**
	 * 
	 * @return
	 */
	public String getJankalyanDBUser() {

		String DBUser = null;

		String realPath = request.getServletContext().getRealPath("/");

		File xmlFile = new File(realPath + File.separator + "configuration.xml");

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("rudhira-config");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					DBUser = eElement.getElementsByTagName("Jankalyan-DB-Username").item(0).getTextContent();
				}
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return DBUser;

	}

	/**
	 * 
	 * @return
	 */
	public String getJankalyanDBName() {

		String DBName = null;

		String realPath = request.getServletContext().getRealPath("/");

		File xmlFile = new File(realPath + File.separator + "configuration.xml");

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("rudhira-config");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					DBName = eElement.getElementsByTagName("Jankalyan-DB-Name").item(0).getTextContent();
				}
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return DBName;

	}

	/**
	 * 
	 * @return
	 */
	public String getJankalyanDBPass() {

		String DBPass = null;

		String realPath = request.getServletContext().getRealPath("/");

		File xmlFile = new File(realPath + File.separator + "configuration.xml");

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("rudhira-config");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					DBPass = eElement.getElementsByTagName("Jankalyan-DB-Password").item(0).getTextContent();
				}
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return DBPass;

	}

	/**
	 * 
	 * @return
	 */
	public String getTallyIPAddress() {

		String tallyIP = null;

		String realPath = request.getServletContext().getRealPath("/");

		File xmlFile = new File(realPath + File.separator + "configuration.xml");

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("rudhira-config");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					tallyIP = eElement.getElementsByTagName("Tally-IP").item(0).getTextContent();
				}
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return tallyIP;

	}

	/**
	 * 
	 * @return
	 */
	public String getTallyPort() {

		String tallyPort = null;

		String realPath = request.getServletContext().getRealPath("/");

		File xmlFile = new File(realPath + File.separator + "configuration.xml");

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("rudhira-config");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					tallyPort = eElement.getElementsByTagName("Tally-Port").item(0).getTextContent();
				}
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return tallyPort;

	}
	
	/**
	 * 
	 * @return
	 */
	public String getTallyFilePath() {

		String tallyFilePath = null;

		String realPath = request.getServletContext().getRealPath("/");

		File xmlFile = new File(realPath + File.separator + "configuration.xml");

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("rudhira-config");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					tallyFilePath = eElement.getElementsByTagName("Tally-File-Path").item(0).getTextContent();
				}
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return tallyFilePath;

	}

}
