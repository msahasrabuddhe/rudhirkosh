package rudhira.util;

public class ActivityStatus {

	public static final String PENDING = "Pending";

	public static final String INACTIVE = "Inactive";

	public static final String ACTIVE = "Active";

	public static final String LOCKED = "Locked";

	public static final String REJECTED = "Rejected";

	public static final String ADMINISTRATOR = "administrator";

	public static final String PATIENT = "patient";

	public static final String APPROVED = "Approved";

	public static final String BLOCKED = "Blocked";

	public static final String ID_DOCUMENT = "Aadhaar";

	public static final String DISABLE = "Disabled";

	public static final String ENABLE = "Enabled";
	
	public static final String RECEPTIONIST = "receptionist";
}
