package rudhira.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.quartz.CronScheduleBuilder;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

/**
 * Application Lifecycle Listener implementation class SchedulerListnerUtil
 * 
 */
public class SchedulerListnerUtil implements ServletContextListener {

	private static String contextPath = "";

	static String schedulerTime = "";

	static String schedulerTime1 = "";

	/**
	 * Default constructor.
	 */
	public SchedulerListnerUtil() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		System.out.println("Inside Listner....");

		/*
		 * Getting realPaht from ServletContextEvent object
		 */
		contextPath = servletContextEvent.getServletContext().getRealPath("/");

		System.out.println("Context path from Listener ::: " + contextPath);

		/*
		 * Creating Quartz scheduler for Appointment reminder SMS and MAIL to patient
		 * and doctor respectively
		 */
		JobDetail appointmentJob = JobBuilder.newJob(TallyExportScheduler.class)
				.usingJobData("contextPath", contextPath).withIdentity("tallyJob", "tallyGroup").build();

		// Trigger class is to execute the particular task for the mentioned
		// time interval(every morning 7AM), interval is given as
		// CronScheduleBuilder's
		// cronSchedule method, which is similar to crontab in linux
		Trigger tallyTrigger = TriggerBuilder.newTrigger().withIdentity("tallyTrigger", "tallyGroup")
				.withSchedule(CronScheduleBuilder.cronSchedule("0 0 0 * * ?")).build();

		System.out.println(".....Tally reminder.." + tallyTrigger.getFireTimeAfter(tallyTrigger.getStartTime()));

		try {

			// Scheduling appointment job and apptTrigger
			Scheduler apptScheduler = new StdSchedulerFactory().getScheduler();
			apptScheduler.start();
			apptScheduler.scheduleJob(appointmentJob, tallyTrigger);

		} catch (Exception exception) {

			StringWriter stringWriter = new StringWriter();

			exception.printStackTrace(new PrintWriter(stringWriter));

			exception.printStackTrace();
		}

	}

	/**
	 * @see ServletContextListener#contextDestroyed(ServletContextEvent)
	 */
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
	}

	/**
	 * 
	 * @author roshany
	 * 
	 */
	public static class MyRunnable implements Runnable {
		private final String url;

		MyRunnable(String url) {
			this.url = url;
		}

		public void run() {

			System.out.println(url + ": Result returned...");
		}
	}

}
