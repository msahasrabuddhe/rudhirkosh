package rudhira.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import rudhira.DAO.BillingDAOImpl;
import rudhira.DAO.BillingDAOInf;
import rudhira.DAO.ListnerDAOImpl;
import rudhira.DAO.ListnerDAOInf;
import rudhira.DAO.ReportDAOImpl;
import rudhira.DAO.ReportDAOInf;
import rudhira.DAO.UserDAOImpl;
import rudhira.DAO.UserDAOInf;
import rudhira.form.BillingForm;
import rudhira.form.ReportForm;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * 
 * @author Kovid Bioanalytics
 * 
 */
public class ExcelUtil extends DBConnection {

	Connection connection = null;
	Connection connection1 = null;
	PreparedStatement preparedStatement = null;
	ResultSet resultSet = null;
	PreparedStatement preparedStatement1 = null;
	ResultSet resultSet1 = null;
	PreparedStatement preparedStatement2 = null;
	ResultSet resultSet2 = null;
	PreparedStatement preparedStatement3 = null;
	ResultSet resultSet3 = null;
	PreparedStatement preparedStatement4 = null;
	ResultSet resultSet4 = null;
	PreparedStatement preparedStatement5 = null;
	ResultSet resultSet5 = null;
	PreparedStatement preparedStatement6 = null;
	ResultSet resultSet6 = null;
	PreparedStatement preparedStatement7 = null;
	ResultSet resultSet7 = null;
	PreparedStatement preparedStatement8 = null;
	ResultSet resultSet8 = null;
	PreparedStatement preparedStatement9 = null;
	ResultSet resultSet9 = null;

	String status = "error";

	BillingDAOInf billingDAOInf = null;

	UserDAOInf userDAOInf = null;

	public String generatePatientBasedReport(ReportForm reportForm, String realPath, String excelFileName,
			String userFullName) {

		billingDAOInf = new BillingDAOImpl();
		userDAOInf = new UserDAOImpl();

		int currentRow = 9;

		int multipleIDcheck = 1;

		String patientIDNO = null;
		String patientName = null;
		String hospital = null;
		String address = null;
		String bbrNo = null;
		String receiptNo = null;
		String receiptDate = null;

		double totalAmt = 0D;
		double netReceivableAmt = 0D;
		double actualPayment = 0D;
		double outstandingAmt = 0D;

		String receiptType = "";
		String paymentType = "";
		double tdsAmt = 0D;

		String chargeType = null;
		double otherCharges = 0D;

		String concession = null;
		double concessionAmt = 0D;

		int receiptBy = 0;
		String outstandingPaidDate = null;
		String refReceiptNo = null;
		double outstandingPaidAmt = 0D;

		String chequNo = "";
		String chequDate = "";
		String chequeIssuedBy = "";
		String chequBankName = "";
		String chequeBankBranch = "";
		double chequeAmt = 0D;

		int patientID = 0;

		double totalActualPayment = 0D;
		double totalOutstandings = 0D;

		String components = "";

		String bloodBankStorage = "";

		int srNo = 0;

		HashMap<String, Double> otherChargeMap = new HashMap<String, Double>();

		String previousCharge = "";
		double previousChargeAmt = 0D;

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");

		SimpleDateFormat dateToBeFormatted = new SimpleDateFormat("dd-MM-yyyy");

		SimpleDateFormat dateToBeFormatted1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:SS");

		/*
		 * Creating hashMap for payment type and its abbreviations
		 */
		HashMap<String, String> paymentTypeMap = new HashMap<String, String>();

		paymentTypeMap.put("Cash", "CS");
		paymentTypeMap.put("Cheque", "CH");
		paymentTypeMap.put("Credit Note", "CR");
		paymentTypeMap.put("Credit/Debit Card", "EP");

		try {

			connection = getConnection();

			// Workbook wb = new HSSFWorkbook();
			XSSFWorkbook wb = new XSSFWorkbook();

			XSSFSheet spreadSheet = null;

			spreadSheet = wb.createSheet("Patient Report");

			Row row;

			int rowNumber = 0;

			row = spreadSheet.createRow(rowNumber);

			/*
			 * Creating header in XLSXd
			 */
			CellStyle headerCellStyle = wb.createCellStyle();

			/*
			 * Setting up the font
			 */
			Font setFont = wb.createFont();
			setFont.setFontHeightInPoints((short) 9);
			setFont.setBold(true);
			headerCellStyle.setFont(setFont);
			// headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
			// headerCellStyle.setBorderTop(CellStyle.BORDER_THIN);
			// headerCellStyle.setBorderRight(CellStyle.BORDER_THIN);
			// headerCellStyle.setBorderLeft(CellStyle.BORDER_THIN);
			headerCellStyle.setWrapText(true);

			/*
			 * Setting up Data style in XLSX
			 */
			CellStyle dataCellStyle = wb.createCellStyle();
			Font setDataFont = wb.createFont();
			setDataFont.setFontHeightInPoints((short) 9);
			dataCellStyle.setFont(setDataFont);
			// dataCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
			// dataCellStyle.setBorderTop(CellStyle.BORDER_THIN);
			// dataCellStyle.setBorderRight(CellStyle.BORDER_THIN);
			// dataCellStyle.setBorderLeft(CellStyle.BORDER_THIN);
			dataCellStyle.setWrapText(true);

			/*
			 * Initializing Cell reference variable
			 */
			Cell cell = null;

			/*
			 * Generating XLSX Header value for Patient Information
			 */
			spreadSheet.setColumnWidth((short) 0, (short) (256 * 4));
			spreadSheet.setColumnWidth((short) 1, (short) (256 * 11));
			spreadSheet.setColumnWidth((short) 2, (short) (256 * 10));
			spreadSheet.setColumnWidth((short) 3, (short) (256 * 11));
			spreadSheet.setColumnWidth((short) 4, (short) (256 * 15));
			spreadSheet.setColumnWidth((short) 5, (short) (256 * 10));
			spreadSheet.setColumnWidth((short) 6, (short) (256 * 7));
			spreadSheet.setColumnWidth((short) 7, (short) (256 * 9));
			spreadSheet.setColumnWidth((short) 8, (short) (256 * 7));
			spreadSheet.setColumnWidth((short) 9, (short) (256 * 9));
			spreadSheet.setColumnWidth((short) 10, (short) (256 * 7));
			spreadSheet.setColumnWidth((short) 11, (short) (256 * 7));
			spreadSheet.setColumnWidth((short) 12, (short) (256 * 7));
			spreadSheet.setColumnWidth((short) 13, (short) (256 * 7));
			spreadSheet.setColumnWidth((short) 14, (short) (256 * 11));

			/*
			 * Giving values to header
			 */
			cell = row.createCell((short) 0);
			cell.setCellValue("Sr.No.");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 1);
			cell.setCellValue("Receipt No");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 2);
			cell.setCellValue("Receipt Date");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 3);
			cell.setCellValue("Ref.Recpt.No.");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 4);
			cell.setCellValue("Hospital/Storage Center\n(Patient Name)");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 5);
			cell.setCellValue("Components");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 6);
			cell.setCellValue("Total Amt.");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 7);
			cell.setCellValue("Other Charges");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 8);
			cell.setCellValue("Total Other Charge Amt.");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 9);
			cell.setCellValue("Conc. Cat.");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 10);
			cell.setCellValue("Total Conc. Amt.");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 11);
			cell.setCellValue("Net Receivable Amt.\n[Payment Type]");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 12);
			cell.setCellValue("Actual Payment");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 13);
			cell.setCellValue("Outstanding Amt.");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 14);
			cell.setCellValue("Narration");
			cell.setCellStyle(headerCellStyle);

			// Sorting reportReceiptID array
			Arrays.sort(reportForm.getReportReceiptID());

			/*
			 * Iterating over the generated receipd and patient IDs
			 */
			for (int i = 0; i < reportForm.getReportReceiptID().length; i++) {

				rowNumber++;

				components = "";

				chargeType = "";

				otherCharges = 0D;

				srNo++;

				/*
				 * Generate the query to fetch Receipt Details
				 */
				String fetchDetailQuery3 = QueryMaker.BILL_PDF_RETRIEVE_RECEIPT_DETAILS2;

				preparedStatement3 = connection.prepareStatement(fetchDetailQuery3);
				preparedStatement3.setInt(1, Integer.parseInt(reportForm.getReportReceiptID()[i]));
				resultSet3 = preparedStatement3.executeQuery();

				while (resultSet3.next()) {

					totalAmt = resultSet3.getDouble("totalAmt");
					netReceivableAmt = resultSet3.getDouble("netReceivableAmt");
					actualPayment = resultSet3.getDouble("actualPayment");
					outstandingAmt = resultSet3.getDouble("outstandingAmt");

					patientID = resultSet3.getInt("patientID");

					concession = resultSet3.getString("concessionType");
					concessionAmt = resultSet3.getDouble("concessionAmt");

					receiptBy = resultSet3.getInt("receiptBy");
					refReceiptNo = resultSet3.getString("refReceiptNumber");
					outstandingPaidDate = resultSet3.getString("outstandingPaidDate");

					if (outstandingPaidDate == null || outstandingPaidDate == "") {

						outstandingPaidDate = "";

					} else {

						outstandingPaidDate = dateToBeFormatted.format(dateFormat.parse(outstandingPaidDate));

					}
					outstandingPaidAmt = resultSet3.getDouble("outstandingPaidAmt");
					paymentType = resultSet3.getString("paymentType");
					tdsAmt = resultSet3.getDouble("tdsAmt");
					bbrNo = resultSet3.getString("bdrNo");

					receiptNo = resultSet3.getString("receiptNo");
					receiptDate = resultSet3.getString("receiptDate");

					if (receiptDate == null || receiptDate == "") {

						receiptDate = "";

					} else {

						receiptDate = dateToBeFormatted1.format(dateFormat1.parse(receiptDate));

					}

					receiptType = resultSet3.getString("receiptType");

					// Calculating total actual payment
					totalActualPayment += actualPayment;

					// Calculating total outstandings
					totalOutstandings += outstandingAmt;

				}

				/*
				 * Generate the query to fetch Patient Details
				 */
				String fetchDetailQuery1 = QueryMaker.BILL_PDF_RETRIEVE_PATIENT_DETAILS1;

				preparedStatement1 = connection.prepareStatement(fetchDetailQuery1);
				preparedStatement1.setInt(1, patientID);
				resultSet1 = preparedStatement1.executeQuery();

				while (resultSet1.next()) {
					// patientIDNO = resultSet1.getString("patientIdentifier");

					if (resultSet1.getString("middleName") == null || resultSet1.getString("middleName") == "") {

						patientName = resultSet1.getString("firstName") + " " + resultSet1.getString("lastName");

					} else {

						patientName = resultSet1.getString("firstName") + " " + resultSet1.getString("middleName") + " "
								+ resultSet1.getString("lastName");

					}

					hospital = resultSet1.getString("hospital");
					// address = resultSet1.getString("address");
					bloodBankStorage = resultSet1.getString("bloodBankStorage");
				}

				/*
				 * Generate the query to fetch ReceiptItems details
				 */
				String fetchDetailQuery4 = QueryMaker.BILL_PDF_RETRIEVE_RECEIPT_ITEM_DETAILS;

				preparedStatement4 = connection.prepareStatement(fetchDetailQuery4);
				preparedStatement4.setInt(1, Integer.parseInt(reportForm.getReportReceiptID()[i]));
				resultSet4 = preparedStatement4.executeQuery();

				while (resultSet4.next()) {

					components += resultSet4.getString("product") + ",";

				}

				/*
				 * Generate the query to fetch Other Charges details
				 */
				String fetchDetailQuery5 = QueryMaker.BILL_PDF_RETRIEVE_OTHER_CHARGES;

				preparedStatement5 = connection.prepareStatement(fetchDetailQuery5);
				preparedStatement5.setInt(1, Integer.parseInt(reportForm.getReportReceiptID()[i]));
				resultSet5 = preparedStatement5.executeQuery();

				while (resultSet5.next()) {
					String charge = resultSet5.getString("chargeType");

					if (charge.equals("Gr Cross Match")) {
						charge = "X Match";
					} else if (charge.equals("ABD Screening")) {
						charge = "ABD";
					}

					chargeType += charge + ",";
				}

				/*
				 * Generate the query to other charges amount details
				 */
				String fetchDetailQuery6 = QueryMaker.BILL_PDF_RETRIEVE_OTHER_CHARGES1;

				preparedStatement6 = connection.prepareStatement(fetchDetailQuery6);
				preparedStatement6.setInt(1, Integer.parseInt(reportForm.getReportReceiptID()[i]));
				resultSet6 = preparedStatement6.executeQuery();

				while (resultSet6.next()) {

					otherCharges += resultSet6.getDouble("otherCharges");

				}

				/*
				 * For other charges total
				 */
				/*
				 * Generate the query to fetch Other Charges details
				 */
				String fetchDetailQuery7 = QueryMaker.BILL_PDF_RETRIEVE_OTHER_CHARGES;

				preparedStatement7 = connection.prepareStatement(fetchDetailQuery7);
				preparedStatement7.setInt(1, Integer.parseInt(reportForm.getReportReceiptID()[i]));
				resultSet7 = preparedStatement7.executeQuery();

				while (resultSet7.next()) {

					previousCharge = resultSet7.getString("chargeType");

					if (otherChargeMap.containsKey(previousCharge)) {

						double amount = otherChargeMap.get(previousCharge);

						amount += resultSet7.getDouble("otherCharges");

						otherChargeMap.put(previousCharge, amount);

						previousChargeAmt = 0D;

					} else {

						previousChargeAmt = resultSet7.getDouble("otherCharges");

						otherChargeMap.put(previousCharge, previousChargeAmt);

					}

				}

				row = spreadSheet.createRow(rowNumber);

				cell = row.createCell((short) 0);
				cell.setCellValue(srNo);
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 1);
				cell.setCellValue(receiptNo);
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 2);
				cell.setCellValue(receiptDate);
				cell.setCellStyle(dataCellStyle);

				if (hospital.isEmpty()) {

					cell = row.createCell((short) 4);
					cell.setCellValue(bloodBankStorage + "\n(" + patientName + ")");
					cell.setCellStyle(dataCellStyle);

				} else {

					cell = row.createCell((short) 4);
					cell.setCellValue(hospital + "\n(" + patientName + ")");
					cell.setCellStyle(dataCellStyle);

				}

				cell = row.createCell((short) 3);
				cell.setCellValue(refReceiptNo);
				cell.setCellStyle(dataCellStyle);

				// removing last , from string
				if (components != null && components.length() > 0
						&& components.charAt(components.length() - 1) == ',') {
					components = components.substring(0, components.length() - 1);
				}
				cell = row.createCell((short) 5);
				cell.setCellValue(components);
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 6);
				cell.setCellValue(totalAmt);
				cell.setCellStyle(dataCellStyle);

				// removing last , from string
				if (chargeType != null && chargeType.length() > 0
						&& chargeType.charAt(chargeType.length() - 1) == ',') {
					chargeType = chargeType.substring(0, chargeType.length() - 1);
				}
				cell = row.createCell((short) 7);
				cell.setCellValue(chargeType);
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 8);
				cell.setCellValue(otherCharges);
				cell.setCellStyle(dataCellStyle);

				if (concession.equals("000")) {
					concession = "";
				}
				cell = row.createCell((short) 9);
				cell.setCellValue(concession);
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 10);
				cell.setCellValue(concessionAmt);
				cell.setCellStyle(dataCellStyle);

				String finalPaymentType = "";

				for (String key : paymentTypeMap.keySet()) {
					if (key.equals(paymentType)) {
						finalPaymentType = paymentTypeMap.get(key);
					}
				}

				cell = row.createCell((short) 11);
				cell.setCellValue(netReceivableAmt + "\n[" + finalPaymentType + "]");
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 12);
				cell.setCellValue(actualPayment);
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 13);
				cell.setCellValue(outstandingAmt);
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 14);
				cell.setCellValue(receiptNo);
				cell.setCellStyle(dataCellStyle);

			}

			rowNumber++;

			srNo++;

			/*
			 * For total actual payment amount
			 */
			row = spreadSheet.createRow(rowNumber);

			cell = row.createCell((short) 0);
			cell.setCellValue(srNo);
			cell.setCellStyle(dataCellStyle);

			cell = row.createCell((short) 1);
			cell.setCellValue("Total Actual Payment");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 2);
			cell.setCellValue(totalActualPayment);
			cell.setCellStyle(dataCellStyle);

			rowNumber++;

			srNo++;

			/*
			 * For total outstandings
			 */
			row = spreadSheet.createRow(rowNumber);

			cell = row.createCell((short) 0);
			cell.setCellValue(srNo);
			cell.setCellStyle(dataCellStyle);

			cell = row.createCell((short) 1);
			cell.setCellValue("Total Outstandings");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 2);
			cell.setCellValue(totalOutstandings);
			cell.setCellStyle(dataCellStyle);

			rowNumber++;

			srNo++;

			// Calculating total service by adding totalActualPayment and
			// totaloutstanding amount
			double totalServiceCharge = totalActualPayment + totalOutstandings;

			/*
			 * For total service charges
			 */
			row = spreadSheet.createRow(rowNumber);

			cell = row.createCell((short) 0);
			cell.setCellValue(srNo);
			cell.setCellStyle(dataCellStyle);

			cell = row.createCell((short) 1);
			cell.setCellValue("Total Service Charge");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 2);
			cell.setCellValue("" + totalServiceCharge);
			cell.setCellStyle(dataCellStyle);

			/*
			 * Iterating over otherChargeMap hash map in order to get unique other charges
			 * and sum of their respective charges
			 */
			for (String key : otherChargeMap.keySet()) {

				rowNumber++;

				srNo++;

				/*
				 * For total unique other charges and their sum amount
				 */
				row = spreadSheet.createRow(rowNumber);

				cell = row.createCell((short) 0);
				cell.setCellValue(srNo);
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 1);
				cell.setCellValue(key);
				cell.setCellStyle(headerCellStyle);

				cell = row.createCell((short) 2);
				cell.setCellValue(otherChargeMap.get(key));
				cell.setCellStyle(dataCellStyle);

			}

			/*
			 * writing to the xlsx file
			 */
			ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
			wb.write(outByteStream);
			byte[] outArray = outByteStream.toByteArray();

			/*
			 * Write the output to a file
			 */
			FileOutputStream fileOut = new FileOutputStream(new File(excelFileName));
			fileOut.write(outArray);
			fileOut.flush();
			fileOut.close();

			System.out.println("Excel sheet created successfully.");

			status = "success";

			/*
			 * Closing resultSet, preparedStatement, connection objects
			 */
			resultSet6.close();
			preparedStatement6.close();

			resultSet5.close();
			preparedStatement5.close();

			resultSet4.close();
			preparedStatement4.close();

			resultSet3.close();
			preparedStatement3.close();

			resultSet1.close();
			preparedStatement1.close();

			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			status = "error";
		}

		return status;

	}

	public String generateCashHandoverReport(String startDate, String endDate, String realPath, String excelFileName) {

		billingDAOInf = new BillingDAOImpl();
		userDAOInf = new UserDAOImpl();

		int currentRow = 1;

		int srNo = 1;

		SimpleDateFormat dateToBeFormatted = new SimpleDateFormat("yyyy-MM-dd");

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

		try {

			connection = getConnection();

			// Workbook wb = new HSSFWorkbook();
			XSSFWorkbook wb = new XSSFWorkbook();

			/*
			 * Generate the query to fetch Patient Details
			 */
			String fetchDetailQuery1 = QueryMaker.RETRIEVE_CASH_HANDOVER_REPORT_DETAILS;

			preparedStatement1 = connection.prepareStatement(fetchDetailQuery1);

			preparedStatement1.setString(1, dateToBeFormatted.format(dateFormat.parse(startDate)));
			preparedStatement1.setString(2, dateToBeFormatted.format(dateFormat.parse(endDate)));

			resultSet1 = preparedStatement1.executeQuery();

			XSSFSheet spreadSheet = wb.createSheet("Cash hand-over report");
			Row row;

			row = spreadSheet.createRow(0);

			/*
			 * Creating header in XLSX
			 */
			CellStyle headerCellStyle = wb.createCellStyle();

			/*
			 * Setting up the font
			 */
			Font setFont = wb.createFont();
			setFont.setFontHeightInPoints((short) 12);
			setFont.setBold(true);
			headerCellStyle.setFont(setFont);
			// headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
			// headerCellStyle.setBorderTop(CellStyle.BORDER_THIN);
			// headerCellStyle.setBorderRight(CellStyle.BORDER_THIN);
			// headerCellStyle.setBorderLeft(CellStyle.BORDER_THIN);

			/*
			 * Setting up Data style in XLSX
			 */
			CellStyle dataCellStyle = wb.createCellStyle();
			Font setDataFont = wb.createFont();
			setDataFont.setFontHeightInPoints((short) 12);
			dataCellStyle.setFont(setDataFont);
			// dataCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
			// dataCellStyle.setBorderTop(CellStyle.BORDER_THIN);
			// dataCellStyle.setBorderRight(CellStyle.BORDER_THIN);
			// dataCellStyle.setBorderLeft(CellStyle.BORDER_THIN);

			/*
			 * Initializing Cell reference variable
			 */
			Cell cell = null;

			/*
			 * Generating XLSX Header value for Patient Information
			 */
			spreadSheet.setColumnWidth((short) 0, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 1, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 2, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 3, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 4, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 5, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 6, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 7, (short) (256 * 25));

			/*
			 * Giving values to header
			 */
			cell = row.createCell((short) 0);
			cell.setCellValue("Sr No.");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 1);
			cell.setCellValue("Register Date");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 2);
			cell.setCellValue("Shift");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 3);
			cell.setCellValue("Cash Handover");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 4);
			cell.setCellValue("Cash Deposited");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 5);
			cell.setCellValue("Shift Total");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 6);
			cell.setCellValue("Other Product");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 7);
			cell.setCellValue("Balance");
			cell.setCellStyle(headerCellStyle);

			/*
			 * For Receipt items values
			 */
			while (resultSet1.next()) {

				row = spreadSheet.createRow(currentRow++);

				cell = row.createCell((short) 0);
				cell.setCellValue(srNo);
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 1);
				cell.setCellValue(dateFormat.format(resultSet1.getDate("registerDate")));
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 2);
				cell.setCellValue(resultSet1.getString("shift"));
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 3);
				cell.setCellValue(resultSet1.getDouble("cashHandover"));
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 4);
				cell.setCellValue(resultSet1.getDouble("cashDeposited"));
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 5);
				cell.setCellValue(resultSet1.getDouble("shiftCash"));
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 6);
				cell.setCellValue(resultSet1.getDouble("otherProduct"));
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 7);
				cell.setCellValue(resultSet1.getDouble("registerBalance"));
				cell.setCellStyle(dataCellStyle);

				srNo++;

			}

			/*
			 * writing to the xlsx file
			 */
			ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
			wb.write(outByteStream);
			byte[] outArray = outByteStream.toByteArray();

			/*
			 * Write the output to a file
			 */
			FileOutputStream fileOut = new FileOutputStream(new File(excelFileName));
			fileOut.write(outArray);
			fileOut.flush();
			fileOut.close();

			System.out.println("Excel sheet for cash handover report is created successfully.");

			status = "success";

			/*
			 * Closing resultSet, preparedStatement, connection objects
			 */
			resultSet1.close();
			preparedStatement1.close();

			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			status = "error";
		}

		return status;

	}

	public String generateComponentBasedReport(ReportForm reportForm, String realPath, String excelFileName,
			String userFullName) {

		billingDAOInf = new BillingDAOImpl();
		userDAOInf = new UserDAOImpl();

		String patientName = null;

		int counter = 0;

		int srNo = 1;

		SimpleDateFormat dateToBeFormatted2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:SS");

		SimpleDateFormat dateToBeFormatted = new SimpleDateFormat("dd-MM-yyyy");

		SimpleDateFormat dateToBeFormatted1 = new SimpleDateFormat("yyyy-MM-dd");

		try {

			connection = getConnection();

			// Workbook wb = new HSSFWorkbook();
			XSSFWorkbook wb = new XSSFWorkbook();

			// Sorting reportReceiptID array
			Arrays.sort(reportForm.getReportComponents());

			// Sheet spreadSheet = wb.createSheet("Component based report");
			XSSFSheet spreadSheet = wb.createSheet("Component based report");

			Row row;

			/*
			 * Creating header in XLSX
			 */
			CellStyle headerCellStyle = wb.createCellStyle();

			/*
			 * Setting up the font
			 */
			Font setFont = wb.createFont();
			setFont.setFontHeightInPoints((short) 9);
			setFont.setBold(true);
			headerCellStyle.setFont(setFont);
			// headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
			// headerCellStyle.setBorderTop(CellStyle.BORDER_THIN);
			// headerCellStyle.setBorderRight(CellStyle.BORDER_THIN);
			// headerCellStyle.setBorderLeft(CellStyle.BORDER_THIN);

			/*
			 * Setting up Data style in XLSX
			 */
			CellStyle dataCellStyle = wb.createCellStyle();
			Font setDataFont = wb.createFont();
			setDataFont.setFontHeightInPoints((short) 9);
			dataCellStyle.setFont(setDataFont);
			// dataCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
			// dataCellStyle.setBorderTop(CellStyle.BORDER_THIN);
			// dataCellStyle.setBorderRight(CellStyle.BORDER_THIN);
			// dataCellStyle.setBorderLeft(CellStyle.BORDER_THIN);

			/*
			 * Initializing Cell reference variable
			 */
			Cell cell = null;

			/*
			 * Generating XLSX Header value for Patient Information
			 */
			spreadSheet.setColumnWidth((short) 0, (short) (256 * 10));
			spreadSheet.setColumnWidth((short) 1, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 2, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 3, (short) (256 * 15));
			spreadSheet.setColumnWidth((short) 4, (short) (256 * 20));
			spreadSheet.setColumnWidth((short) 5, (short) (256 * 10));
			spreadSheet.setColumnWidth((short) 6, (short) (256 * 15));
			spreadSheet.setColumnWidth((short) 7, (short) (256 * 15));

			row = spreadSheet.createRow(0);

			/*
			 * Giving values to header
			 */
			cell = row.createCell((short) 0);
			cell.setCellValue("Sr.No.");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 1);
			cell.setCellValue("Patient Name/Storage Centre");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 2);
			cell.setCellValue("Component");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 3);
			cell.setCellValue("Receipt No");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 4);
			cell.setCellValue("Receipt Date");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 5);
			cell.setCellValue("Component Rate");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 6);
			cell.setCellValue("Component Quantity");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 7);
			cell.setCellValue("Component Amount");
			cell.setCellStyle(headerCellStyle);

			/*
			 * Iterating over the generated report component
			 */
			for (int i = 0; i < reportForm.getReportComponents().length; i++) {

//				counter++;

				String[] array = reportForm.getReportComponents()[i].split("\\$");

				String component = array[0];
				int receiptID = Integer.parseInt(array[1]);

				/*
				 * Generate the query to component based list
				 */
				String fetchDetailQuery1 = QueryMaker.RETRIEVE_COMPONENT_BASED_LIST;

				preparedStatement1 = connection.prepareStatement(fetchDetailQuery1);
				preparedStatement1.setString(1, component);
				preparedStatement1.setInt(2, receiptID);
				resultSet1 = preparedStatement1.executeQuery();

				while (resultSet1.next()) {

					if (resultSet1.getString("middleName") == null || resultSet1.getString("middleName") == "") {

						patientName = resultSet1.getString("firstName") + " " + resultSet1.getString("lastName");

					} else {

						patientName = resultSet1.getString("firstName") + " " + resultSet1.getString("middleName") + " "
								+ resultSet1.getString("lastName");

					}

					String bbStorage = resultSet1.getString("bloodBankStorage");

					row = spreadSheet.createRow(counter++);
					/*
					 * Giving values to header
					 */
					cell = row.createCell((short) 0);
					cell.setCellValue("" + srNo);
					cell.setCellStyle(dataCellStyle);

					if (bbStorage == null || bbStorage == "" || bbStorage.isEmpty()) {

						cell = row.createCell((short) 1);
						cell.setCellValue(patientName);
						cell.setCellStyle(dataCellStyle);

					} else {

						cell = row.createCell((short) 1);
						cell.setCellValue(bbStorage);
						cell.setCellStyle(dataCellStyle);

					}

					cell = row.createCell((short) 2);
					cell.setCellValue(resultSet1.getString("product"));
					cell.setCellStyle(dataCellStyle);

					cell = row.createCell((short) 3);
					cell.setCellValue(resultSet1.getString("receiptNo"));
					cell.setCellStyle(dataCellStyle);

					cell = row.createCell((short) 4);
					cell.setCellValue(dateToBeFormatted2.format(resultSet1.getTimestamp("receiptDate")));
					cell.setCellStyle(dataCellStyle);

					cell = row.createCell((short) 5);
					cell.setCellValue(resultSet1.getDouble("rate"));
					cell.setCellStyle(dataCellStyle);

					cell = row.createCell((short) 6);
					cell.setCellValue(resultSet1.getInt("quantity"));
					cell.setCellStyle(dataCellStyle);

					cell = row.createCell((short) 7);
					cell.setCellValue(resultSet1.getDouble("amount"));
					cell.setCellStyle(dataCellStyle);

					srNo++;
				}

			}

			/*
			 * writing to the xlsx file
			 */
			ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
			wb.write(outByteStream);
			byte[] outArray = outByteStream.toByteArray();

			/*
			 * Write the output to a file
			 */
			FileOutputStream fileOut = new FileOutputStream(new File(excelFileName));
			fileOut.write(outArray);
			fileOut.flush();
			fileOut.close();

			System.out.println("Excel sheet created successfully.");

			status = "success";

			/*
			 * Closing resultSet, preparedStatement, connection objects
			 */
			resultSet1.close();
			preparedStatement1.close();

			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			status = "error";
		}

		return status;

	}

	public String generateExportForAccountsReport(ReportForm reportForm, String realPath, String excelFileName) {

		billingDAOInf = new BillingDAOImpl();
		userDAOInf = new UserDAOImpl();

		int currentRow = 1;

		int srNo = 1;

		SimpleDateFormat dateToBeFormatted = new SimpleDateFormat("yyyy-MM-dd");

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

		String patientName = "";

		try {

			connection = getConnection();

			// Workbook wb = new HSSFWorkbook();
			XSSFWorkbook wb = new XSSFWorkbook();

			/*
			 * If searchValue is 1, then display today's record
			 */
			if (reportForm.getSearchExportForAcctName().equals("1")) {

				/*
				 * Generate the query to fetch Patient Details
				 */
				String fetchDetailQuery1 = QueryMaker.RETRIEVE_EXPORT_FOR_ACCOUNT_DETAILS_TODAY;

				preparedStatement = connection.prepareStatement(fetchDetailQuery1);

				resultSet = preparedStatement.executeQuery();

				/*
				 * If searchValue is 2, then display this week's record
				 */
			} else if (reportForm.getSearchExportForAcctName().equals("2")) {

				/*
				 * Generate the query to fetch Patient Details
				 */
				String fetchDetailQuery1 = QueryMaker.RETRIEVE_EXPORT_FOR_ACCOUNT_DETAILS_THIS_WEEK;

				preparedStatement = connection.prepareStatement(fetchDetailQuery1);

				resultSet = preparedStatement.executeQuery();

				/*
				 * If searchValue is 3, then display this month's record
				 */
			} else if (reportForm.getSearchExportForAcctName().equals("3")) {

				/*
				 * Generate the query to fetch Patient Details
				 */
				String fetchDetailQuery1 = QueryMaker.RETRIEVE_EXPORT_FOR_ACCOUNT_DETAILS_THIS_MONTH;

				preparedStatement = connection.prepareStatement(fetchDetailQuery1);

				resultSet = preparedStatement.executeQuery();

				/*
				 * If searchValue is 4, then display record from start date to end date
				 */
			} else {

				/*
				 * Generate the query to fetch Patient Details
				 */
				String fetchDetailQuery1 = QueryMaker.RETRIEVE_EXPORT_FOR_ACCOUNT_DETAILS_BY_DATE;

				preparedStatement = connection.prepareStatement(fetchDetailQuery1);

				preparedStatement.setString(1,
						dateToBeFormatted.format(dateFormat.parse(reportForm.getStartDate())) + " 00:00");
				preparedStatement.setString(2,
						dateToBeFormatted.format(dateFormat.parse(reportForm.getEndDate())) + " 23:59");

				resultSet = preparedStatement.executeQuery();
			}

			// Sheet spreadSheet = wb.createSheet("Export For Accounts");
			XSSFSheet spreadSheet = wb.createSheet("Export For Accounts");

			Row row;

			row = spreadSheet.createRow(0);

			/*
			 * Creating header in XLSX
			 */
			CellStyle headerCellStyle = wb.createCellStyle();

			/*
			 * Setting up the font
			 */
			Font setFont = wb.createFont();
			setFont.setFontHeightInPoints((short) 12);
			setFont.setBold(true);
			headerCellStyle.setFont(setFont);
			// headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
			// headerCellStyle.setBorderTop(CellStyle.BORDER_THIN);
			// headerCellStyle.setBorderRight(CellStyle.BORDER_THIN);
			// headerCellStyle.setBorderLeft(CellStyle.BORDER_THIN);

			/*
			 * Setting up Data style in XLSX
			 */
			CellStyle dataCellStyle = wb.createCellStyle();
			Font setDataFont = wb.createFont();
			setDataFont.setFontHeightInPoints((short) 12);
			dataCellStyle.setFont(setDataFont);
			// dataCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
			// dataCellStyle.setBorderTop(CellStyle.BORDER_THIN);
			// dataCellStyle.setBorderRight(CellStyle.BORDER_THIN);
			// dataCellStyle.setBorderLeft(CellStyle.BORDER_THIN);

			/*
			 * Initializing Cell reference variable
			 */
			Cell cell = null;

			/*
			 * Generating XLSX Header value for Patient Information
			 */
			spreadSheet.setColumnWidth((short) 0, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 1, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 2, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 3, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 4, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 5, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 6, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 7, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 8, (short) (256 * 25));

			/*
			 * Giving values to header
			 */
			cell = row.createCell((short) 0);
			cell.setCellValue("Sr No.");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 1);
			cell.setCellValue("Receipt No");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 2);
			cell.setCellValue("Receipt Type");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 3);
			cell.setCellValue("Patient Name");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 4);
			cell.setCellValue("Receipt Date");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 5);
			cell.setCellValue("Payment Type");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 6);
			cell.setCellValue("Net Receivable Amount");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 7);
			cell.setCellValue("Actual Payment");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 8);
			cell.setCellValue("Outstanding Amount");
			cell.setCellStyle(headerCellStyle);

			/*
			 * For Receipt items values
			 */
			while (resultSet.next()) {

				row = spreadSheet.createRow(currentRow++);

				cell = row.createCell((short) 0);
				cell.setCellValue(srNo);
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 1);
				cell.setCellValue(resultSet.getString("receiptNo"));
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 2);
				cell.setCellValue(resultSet.getString("receiptType"));
				cell.setCellStyle(dataCellStyle);

				if (resultSet.getString("middleName") == "" || resultSet.getString("middleName") == null) {
					patientName = resultSet.getString("firstName") + " " + resultSet.getString("lastName");
				} else {
					patientName = resultSet.getString("firstName") + " " + resultSet.getString("middleName") + " "
							+ resultSet.getString("lastName");
				}

				cell = row.createCell((short) 3);
				cell.setCellValue(patientName);
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 4);
				cell.setCellValue(dateFormat.format(resultSet.getTimestamp("receiptDate")));
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 5);
				cell.setCellValue(resultSet.getString("paymentType"));
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 6);
				cell.setCellValue(resultSet.getDouble("netReceivableAmt"));
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 7);
				cell.setCellValue(resultSet.getDouble("actualPayment"));
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 8);
				cell.setCellValue(resultSet.getDouble("outstandingAmt"));
				cell.setCellStyle(dataCellStyle);

				srNo++;

			}

			/*
			 * writing to the xlsx file
			 */
			ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
			wb.write(outByteStream);
			byte[] outArray = outByteStream.toByteArray();

			/*
			 * Write the output to a file
			 */
			FileOutputStream fileOut = new FileOutputStream(new File(excelFileName));
			fileOut.write(outArray);
			fileOut.flush();
			fileOut.close();

			System.out.println("Excel sheet for export for accounts is created successfully.");

			status = "success";

			/*
			 * Closing resultSet, preparedStatement, connection objects
			 */
			resultSet.close();
			preparedStatement.close();

			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			status = "error";
		}

		return status;

	}

	public String generateRegisterReport(String startDate, String endDate, String realPath, String excelFileName) {

		billingDAOInf = new BillingDAOImpl();
		userDAOInf = new UserDAOImpl();

		int currentRow = 1;

		int srNo = 1;

		SimpleDateFormat dateToBeFormatted = new SimpleDateFormat("yyyy-MM-dd");

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

		try {

			connection = getConnection();

			// Workbook wb = new HSSFWorkbook();
			XSSFWorkbook wb = new XSSFWorkbook();

			/*
			 * Generate the query to fetch Patient Details
			 */
			String fetchDetailQuery1 = QueryMaker.RETRIEVE_REGISTER_REPORT;

			preparedStatement1 = connection.prepareStatement(fetchDetailQuery1);

			preparedStatement1.setString(1, dateToBeFormatted.format(dateFormat.parse(startDate)));
			preparedStatement1.setString(2, dateToBeFormatted.format(dateFormat.parse(endDate)));

			resultSet1 = preparedStatement1.executeQuery();

			// Sheet spreadSheet = wb.createSheet("Register report");

			XSSFSheet spreadSheet = wb.createSheet("Register report");

			Row row;

			row = spreadSheet.createRow(0);

			/*
			 * Creating header in XLSX
			 */
			CellStyle headerCellStyle = wb.createCellStyle();

			/*
			 * Setting up the font
			 */
			Font setFont = wb.createFont();
			setFont.setFontHeightInPoints((short) 9);
			setFont.setBold(true);
			headerCellStyle.setFont(setFont);
			// headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
			// headerCellStyle.setBorderTop(CellStyle.BORDER_THIN);
			// headerCellStyle.setBorderRight(CellStyle.BORDER_THIN);
			// headerCellStyle.setBorderLeft(CellStyle.BORDER_THIN);

			/*
			 * Setting up Data style in XLSX
			 */
			CellStyle dataCellStyle = wb.createCellStyle();
			Font setDataFont = wb.createFont();
			setDataFont.setFontHeightInPoints((short) 9);
			dataCellStyle.setFont(setDataFont);
			// dataCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
			// dataCellStyle.setBorderTop(CellStyle.BORDER_THIN);
			// dataCellStyle.setBorderRight(CellStyle.BORDER_THIN);
			// dataCellStyle.setBorderLeft(CellStyle.BORDER_THIN);

			/*
			 * Initializing Cell reference variable
			 */
			Cell cell = null;

			/*
			 * Generating XLSX Header value for Patient Information
			 */
			spreadSheet.setColumnWidth((short) 0, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 1, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 2, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 3, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 4, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 5, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 6, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 7, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 8, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 9, (short) (256 * 25));

			/*
			 * Giving values to header
			 */
			cell = row.createCell((short) 0);
			cell.setCellValue("Sr No.");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 1);
			cell.setCellValue("Register Date");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 2);
			cell.setCellValue("Cash Handover");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 3);
			cell.setCellValue("Blood Bank Balance");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 4);
			cell.setCellValue("Other Product Balance");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 5);
			cell.setCellValue("Expected Cash Balance");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 6);
			cell.setCellValue("Actual Cash Balance");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 7);
			cell.setCellValue("Bank Deposit");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 8);
			cell.setCellValue("Cash Mismatch");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 9);
			cell.setCellValue("Cash Adjustment");
			cell.setCellStyle(headerCellStyle);

			/*
			 * For Receipt items values
			 */
			while (resultSet1.next()) {

				row = spreadSheet.createRow(currentRow++);

				cell = row.createCell((short) 0);
				cell.setCellValue(srNo);
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 1);
				cell.setCellValue(dateFormat.format(resultSet1.getDate("registerDate")));
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 2);
				cell.setCellValue(resultSet1.getDouble("handOverCash"));
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 3);
				cell.setCellValue(resultSet1.getDouble("bloodBankBalance"));
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 4);
				cell.setCellValue(resultSet1.getDouble("otherProductBalance"));
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 5);
				cell.setCellValue(resultSet1.getDouble("expectedCashBalance"));
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 6);
				cell.setCellValue(resultSet1.getDouble("actualCashBalance"));
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 7);
				cell.setCellValue(resultSet1.getDouble("bankDeposit"));
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 8);
				cell.setCellValue(resultSet1.getDouble("cashMismatch"));
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 9);
				cell.setCellValue(resultSet1.getDouble("cashAdjustment"));
				cell.setCellStyle(dataCellStyle);

				srNo++;

			}

			/*
			 * writing to the xlsx file
			 */
			ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
			wb.write(outByteStream);
			byte[] outArray = outByteStream.toByteArray();

			/*
			 * Write the output to a file
			 */
			FileOutputStream fileOut = new FileOutputStream(new File(excelFileName));
			fileOut.write(outArray);
			fileOut.flush();
			fileOut.close();

			System.out.println("Excel sheet for register report is created successfully.");

			status = "success";

			/*
			 * Closing resultSet, preparedStatement, connection objects
			 */
			resultSet1.close();
			preparedStatement1.close();

			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			status = "error";
		}

		return status;

	}

	public String generateConcessionBasedReport(ReportForm reportForm, String realPath, String excelFileName) {

		billingDAOInf = new BillingDAOImpl();
		userDAOInf = new UserDAOImpl();

		int currentRow = 1;

		String receiptNo = "";
		String receiptDate = "";

		String patientName = "";
		String concessionType = "";

		double concessionAmt = 0D;
		double netReceivableAmt = 0D;

		String charityCaseNo = "";

		int srNo = 0;

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");

		SimpleDateFormat dateToBeFormatted = new SimpleDateFormat("dd-MM-yyyy");

		SimpleDateFormat dateToBeFormatted1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:SS");

		try {

			connection = getConnection();

			// Workbook wb = new HSSFWorkbook();
			XSSFWorkbook wb = new XSSFWorkbook();

			XSSFSheet spreadSheet = null;

			spreadSheet = wb.createSheet("Concession-Based Report");

			Row row;

			int rowNumber = 0;

			row = spreadSheet.createRow(rowNumber);

			/*
			 * Creating header in XLSXd
			 */
			CellStyle headerCellStyle = wb.createCellStyle();

			/*
			 * Setting up the font
			 */
			Font setFont = wb.createFont();
			setFont.setFontHeightInPoints((short) 9);
			setFont.setBold(true);
			headerCellStyle.setFont(setFont);
			// headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
			// headerCellStyle.setBorderTop(CellStyle.BORDER_THIN);
			// headerCellStyle.setBorderRight(CellStyle.BORDER_THIN);
			// headerCellStyle.setBorderLeft(CellStyle.BORDER_THIN);
			headerCellStyle.setWrapText(true);

			/*
			 * Setting up Data style in XLSX
			 */
			CellStyle dataCellStyle = wb.createCellStyle();
			Font setDataFont = wb.createFont();
			setDataFont.setFontHeightInPoints((short) 9);
			dataCellStyle.setFont(setDataFont);
			// dataCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
			// dataCellStyle.setBorderTop(CellStyle.BORDER_THIN);
			// dataCellStyle.setBorderRight(CellStyle.BORDER_THIN);
			// dataCellStyle.setBorderLeft(CellStyle.BORDER_THIN);
			dataCellStyle.setWrapText(true);

			/*
			 * Initializing Cell reference variable
			 */
			Cell cell = null;

			/*
			 * Generating XLSX Header value for Patient Information
			 */
			spreadSheet.setColumnWidth((short) 0, (short) (256 * 10));
			spreadSheet.setColumnWidth((short) 1, (short) (256 * 20));
			spreadSheet.setColumnWidth((short) 2, (short) (256 * 15));
			spreadSheet.setColumnWidth((short) 3, (short) (256 * 15));
			spreadSheet.setColumnWidth((short) 4, (short) (256 * 10));
			spreadSheet.setColumnWidth((short) 5, (short) (256 * 10));
			spreadSheet.setColumnWidth((short) 6, (short) (256 * 10));
			spreadSheet.setColumnWidth((short) 7, (short) (256 * 15));

			/*
			 * Giving values to header
			 */
			cell = row.createCell((short) 0);
			cell.setCellValue("Sr.No.");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 1);
			cell.setCellValue("Patient Name");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 2);
			cell.setCellValue("Receipt No");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 3);
			cell.setCellValue("Receipt Date");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 4);
			cell.setCellValue("Concession Type");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 5);
			cell.setCellValue("Concession Amt.");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 6);
			cell.setCellValue("Net Receivable Amt.");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 7);
			cell.setCellValue("Charity Case No.");
			cell.setCellStyle(headerCellStyle);

			// Sorting reportReceiptID array
			Arrays.sort(reportForm.getReportReceiptID());

			/*
			 * Iterating over the generated receipd and patient IDs
			 */
			for (int i = 0; i < reportForm.getReportReceiptID().length; i++) {

				rowNumber++;

				srNo++;

				int patientID = 0;

				/*
				 * Generate the query to fetch Receipt Details
				 */
				String fetchDetailQuery3 = QueryMaker.BILL_PDF_RETRIEVE_RECEIPT_DETAILS2;

				preparedStatement3 = connection.prepareStatement(fetchDetailQuery3);
				preparedStatement3.setInt(1, Integer.parseInt(reportForm.getReportReceiptID()[i]));
				resultSet3 = preparedStatement3.executeQuery();

				while (resultSet3.next()) {

					netReceivableAmt = resultSet3.getDouble("netReceivableAmt");
					concessionType = resultSet3.getString("concessionType");
					concessionAmt = resultSet3.getDouble("concessionAmt");
					receiptNo = resultSet3.getString("receiptNo");
					receiptDate = resultSet3.getString("receiptDate");
					charityCaseNo = resultSet3.getString("charityCaseNo");

					if (receiptDate == null || receiptDate == "") {

						receiptDate = "";

					} else {

						receiptDate = dateToBeFormatted1.format(dateFormat1.parse(receiptDate));

					}

					patientID = resultSet3.getInt("patientID");

				}

				System.out.println("Patient ID is .. " + patientID + " for receiptID.."
						+ Integer.parseInt(reportForm.getReportReceiptID()[i]));

				/*
				 * Generate the query to fetch Patient Details
				 */
				String fetchDetailQuery1 = QueryMaker.BILL_PDF_RETRIEVE_PATIENT_DETAILS1;

				preparedStatement1 = connection.prepareStatement(fetchDetailQuery1);
				preparedStatement1.setInt(1, patientID);
				resultSet1 = preparedStatement1.executeQuery();

				while (resultSet1.next()) {
					// patientIDNO = resultSet1.getString("patientIdentifier");

					if (resultSet1.getString("middleName") == null || resultSet1.getString("middleName") == "") {

						patientName = resultSet1.getString("firstName") + " " + resultSet1.getString("lastName");

					} else {

						patientName = resultSet1.getString("firstName") + " " + resultSet1.getString("middleName") + " "
								+ resultSet1.getString("lastName");

					}

				}

				row = spreadSheet.createRow(rowNumber);

				cell = row.createCell((short) 0);
				cell.setCellValue(srNo);
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 1);
				cell.setCellValue(patientName);
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 2);
				cell.setCellValue(receiptNo);
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 3);
				cell.setCellValue(receiptDate);
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 4);
				cell.setCellValue(concessionType);
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 5);
				cell.setCellValue(concessionAmt);
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 6);
				cell.setCellValue(netReceivableAmt);
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 7);
				cell.setCellValue(charityCaseNo);
				cell.setCellStyle(dataCellStyle);

			}

			/*
			 * writing to the xlsx file
			 */
			ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
			wb.write(outByteStream);
			byte[] outArray = outByteStream.toByteArray();

			/*
			 * Write the output to a file
			 */
			FileOutputStream fileOut = new FileOutputStream(new File(excelFileName));
			fileOut.write(outArray);
			fileOut.flush();
			fileOut.close();

			System.out.println("Excel sheet for concession based report created successfully.");

			status = "success";

			/*
			 * Closing resultSet, preparedStatement, connection objects
			 */
			resultSet3.close();
			preparedStatement3.close();

			resultSet1.close();
			preparedStatement1.close();

			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			status = "error";
		}

		return status;

	}

	public String generateCardChequeBasedReport(ReportForm reportForm, String realPath, String excelFileName,
			String userFullName) {

		billingDAOInf = new BillingDAOImpl();
		userDAOInf = new UserDAOImpl();

		int currentRow = 8;

		int multipleIDcheck = 1;

		String patientIDNO = null;
		String patientName = null;
		String hospital = null;
		String address = null;
		String bbrNo = null;
		String receiptNo = null;
		String receiptDate = null;

		double totalAmt = 0D;
		double netReceivableAmt = 0D;
		double actualPayment = 0D;
		double outstandingAmt = 0D;

		String receiptType = "";
		String paymentType = "";
		double tdsAmt = 0D;

		String chargeType = null;
		double otherCharges = 0D;

		String concession = null;
		double concessionAmt = 0D;

		int receiptBy = 0;
		String outstandingPaidDate = null;
		String refReceiptNo = null;
		double outstandingPaidAmt = 0D;

		String chequNo = "";
		String chequDate = "";
		String chequeIssuedBy = "";
		String chequBankName = "";
		String chequeBankBranch = "";
		double chequeAmt = 0D;

		int counter = 0;

		int srNo = 1;

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");

		SimpleDateFormat dateToBeFormatted = new SimpleDateFormat("dd-MM-yyyy");

		SimpleDateFormat dateToBeFormatted1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:SS");

		try {

			connection = getConnection();

			// Workbook wb = new HSSFWorkbook();
			XSSFWorkbook wb = new XSSFWorkbook();

			// Sorting reportReceiptID array
			Arrays.sort(reportForm.getReportReceiptID());

			/*
			 * Iterating over the generated receipd and patient IDs
			 */
			for (int i = 0; i < reportForm.getReportReceiptID().length; i++) {

				counter++;

				/*
				 * Generate the query to fetch Patient Details
				 */
				String fetchDetailQuery1 = QueryMaker.BILL_PDF_RETRIEVE_PATIENT_DETAILS1;

				preparedStatement1 = connection.prepareStatement(fetchDetailQuery1);
				preparedStatement1.setInt(1, Integer.parseInt(reportForm.getReportPatientID()[i]));
				resultSet1 = preparedStatement1.executeQuery();

				while (resultSet1.next()) {
					patientIDNO = resultSet1.getString("patientIdentifier");

					if (resultSet1.getString("middleName") == null || resultSet1.getString("middleName") == "") {

						patientName = resultSet1.getString("firstName") + " " + resultSet1.getString("lastName");

					} else {

						patientName = resultSet1.getString("firstName") + " " + resultSet1.getString("middleName") + " "
								+ resultSet1.getString("lastName");

					}

					hospital = resultSet1.getString("hospital");
					address = resultSet1.getString("address");
				}

				/*
				 * Generate the query to fetch Receipt Details
				 */
				String fetchDetailQuery3 = QueryMaker.BILL_PDF_RETRIEVE_RECEIPT_DETAILS2;

				preparedStatement3 = connection.prepareStatement(fetchDetailQuery3);
				preparedStatement3.setInt(1, Integer.parseInt(reportForm.getReportReceiptID()[i]));
				resultSet3 = preparedStatement3.executeQuery();

				while (resultSet3.next()) {

					totalAmt = resultSet3.getDouble("totalAmt");
					netReceivableAmt = resultSet3.getDouble("netReceivableAmt");
					actualPayment = resultSet3.getDouble("actualPayment");
					outstandingAmt = resultSet3.getDouble("outstandingAmt");

					concession = resultSet3.getString("concessionType");
					concessionAmt = resultSet3.getDouble("concessionAmt");

					receiptBy = resultSet3.getInt("receiptBy");
					refReceiptNo = resultSet3.getString("refReceiptNumber");
					outstandingPaidDate = resultSet3.getString("outstandingPaidDate");

					if (outstandingPaidDate == null || outstandingPaidDate == "") {

						outstandingPaidDate = "";

					} else {

						outstandingPaidDate = dateToBeFormatted.format(dateFormat.parse(outstandingPaidDate));

					}
					outstandingPaidAmt = resultSet3.getDouble("outstandingPaidAmt");
					paymentType = resultSet3.getString("paymentType");
					tdsAmt = resultSet3.getDouble("tdsAmt");
					bbrNo = resultSet3.getString("bdrNo");

					receiptNo = resultSet3.getString("receiptNo");
					receiptDate = resultSet3.getString("receiptDate");

					if (receiptDate == null || receiptDate == "") {

						receiptDate = "";

					} else {

						receiptDate = dateToBeFormatted1.format(dateFormat1.parse(receiptDate));

					}

					receiptType = resultSet3.getString("receiptType");

				}

				/*
				 * Generate the query to fetch ReceiptItems details
				 */
				String fetchDetailQuery4 = QueryMaker.BILL_PDF_RETRIEVE_RECEIPT_ITEM_DETAILS;

				preparedStatement4 = connection.prepareStatement(fetchDetailQuery4);
				preparedStatement4.setInt(1, Integer.parseInt(reportForm.getReportReceiptID()[i]));
				resultSet4 = preparedStatement4.executeQuery();

				/*
				 * Generate the query to fetch Other Charges details
				 */
				String fetchDetailQuery5 = QueryMaker.BILL_PDF_RETRIEVE_OTHER_CHARGES;

				preparedStatement5 = connection.prepareStatement(fetchDetailQuery5);
				preparedStatement5.setInt(1, Integer.parseInt(reportForm.getReportReceiptID()[i]));
				resultSet5 = preparedStatement5.executeQuery();

				/*
				 * Generate the query to fetch Cheque details
				 */
				String fetchDetailQuery6 = QueryMaker.BILL_PDF_RETRIEVE_CHEQUE_DETAILS;

				preparedStatement6 = connection.prepareStatement(fetchDetailQuery6);
				preparedStatement6.setInt(1, Integer.parseInt(reportForm.getReportReceiptID()[i]));
				resultSet6 = preparedStatement6.executeQuery();

				while (resultSet6.next()) {
					chequeIssuedBy = resultSet6.getString("chequeIssuedBy");
					chequNo = resultSet6.getString("chequeNumber");
					chequDate = resultSet6.getString("chequeDate");

					if (chequDate == null || chequDate == "") {
						chequDate = "";
					} else {
						chequDate = dateToBeFormatted.format(dateFormat.parse(chequDate));
					}
					chequBankName = resultSet6.getString("bankName");
					chequeBankBranch = resultSet6.getString("bankBranch");
					chequeAmt = resultSet6.getDouble("chequeAmt");
				}

				patientName = patientName.replaceAll("[\\-\\+\\.\\^:,/]", "");

				// Sheet spreadSheet = wb.createSheet(patientName + "-" +
				// counter);
				XSSFSheet spreadSheet = wb.createSheet(patientName + "-" + counter);

				Row row;

				row = spreadSheet.createRow(0);

				/*
				 * Creating header in XLSX
				 */
				CellStyle headerCellStyle = wb.createCellStyle();

				/*
				 * Setting up the font
				 */
				Font setFont = wb.createFont();
				setFont.setFontHeightInPoints((short) 9);
				setFont.setBold(true);
				headerCellStyle.setFont(setFont);
				// headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
				// headerCellStyle.setBorderTop(CellStyle.BORDER_THIN);
				// headerCellStyle.setBorderRight(CellStyle.BORDER_THIN);
				// headerCellStyle.setBorderLeft(CellStyle.BORDER_THIN);

				/*
				 * Setting up Data style in XLSX
				 */
				CellStyle dataCellStyle = wb.createCellStyle();
				Font setDataFont = wb.createFont();
				setDataFont.setFontHeightInPoints((short) 9);
				dataCellStyle.setFont(setDataFont);
				// dataCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
				// dataCellStyle.setBorderTop(CellStyle.BORDER_THIN);
				// dataCellStyle.setBorderRight(CellStyle.BORDER_THIN);
				// dataCellStyle.setBorderLeft(CellStyle.BORDER_THIN);

				/*
				 * Initializing Cell reference variable
				 */
				Cell cell = null;

				Cell cell1 = null;
				Cell cell2 = null;

				/*
				 * Generating XLSX Header value for Patient Information
				 */
				spreadSheet.setColumnWidth((short) 0, (short) (256 * 25));
				spreadSheet.setColumnWidth((short) 1, (short) (256 * 25));
				spreadSheet.setColumnWidth((short) 2, (short) (256 * 25));
				spreadSheet.setColumnWidth((short) 3, (short) (256 * 25));
				spreadSheet.setColumnWidth((short) 4, (short) (256 * 25));
				spreadSheet.setColumnWidth((short) 5, (short) (256 * 25));

				/*
				 * Giving values to header
				 */
				cell = row.createCell((short) 0);
				cell.setCellValue("Patient Name");
				cell.setCellStyle(headerCellStyle);

				cell = row.createCell((short) 1);
				cell.setCellValue(patientName);
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 2);
				cell.setCellValue("Net Receivable");
				cell.setCellStyle(headerCellStyle);

				cell = row.createCell((short) 3);
				cell.setCellValue("" + netReceivableAmt);
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 4);
				cell.setCellValue("Outstanding Paid Date");
				cell.setCellStyle(headerCellStyle);

				cell = row.createCell((short) 5);
				cell.setCellValue(outstandingPaidDate);
				cell.setCellStyle(dataCellStyle);

				row = spreadSheet.createRow(1);

				/*
				 * Giving values to header
				 */
				cell = row.createCell((short) 0);
				cell.setCellValue("BDR No.");
				cell.setCellStyle(headerCellStyle);

				cell = row.createCell((short) 1);
				cell.setCellValue(bbrNo);
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 2);
				cell.setCellValue("Actual Payment");
				cell.setCellStyle(headerCellStyle);

				cell = row.createCell((short) 3);
				cell.setCellValue("" + actualPayment);
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 4);
				cell.setCellValue("Outstanding Paid Amount");
				cell.setCellStyle(headerCellStyle);

				cell = row.createCell((short) 5);
				cell.setCellValue("" + outstandingPaidAmt);
				cell.setCellStyle(dataCellStyle);

				row = spreadSheet.createRow(2);

				/*
				 * Giving values to header
				 */
				cell = row.createCell((short) 0);
				cell.setCellValue("Patient Barcode");
				cell.setCellStyle(headerCellStyle);

				cell = row.createCell((short) 1);
				cell.setCellValue("");
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 2);
				cell.setCellValue("Outstanding Amount");
				cell.setCellStyle(headerCellStyle);

				cell = row.createCell((short) 3);
				cell.setCellValue("" + outstandingAmt);
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 4);
				cell.setCellValue("Receipt By");
				cell.setCellStyle(headerCellStyle);

				cell = row.createCell((short) 5);
				cell.setCellValue(userFullName);
				cell.setCellStyle(dataCellStyle);

				row = spreadSheet.createRow(3);

				/*
				 * Giving values to header
				 */
				cell = row.createCell((short) 0);
				cell.setCellValue("Receipt No");
				cell.setCellStyle(headerCellStyle);

				cell = row.createCell((short) 1);
				cell.setCellValue(receiptNo);
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 2);
				cell.setCellValue("Payment Type");
				cell.setCellStyle(headerCellStyle);

				cell = row.createCell((short) 3);
				cell.setCellValue(paymentType);
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 4);
				cell.setCellValue("Date of receipt");
				cell.setCellStyle(headerCellStyle);

				cell = row.createCell((short) 5);
				cell.setCellValue(receiptDate);
				cell.setCellStyle(dataCellStyle);

				row = spreadSheet.createRow(4);

				/*
				 * Giving values to header
				 */
				cell = row.createCell((short) 0);
				cell.setCellValue("TDS Amount");
				cell.setCellStyle(headerCellStyle);

				cell = row.createCell((short) 1);
				cell.setCellValue("" + tdsAmt);
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 2);
				cell.setCellValue("Receipt Barcode");
				cell.setCellStyle(headerCellStyle);

				cell = row.createCell((short) 3);
				cell.setCellValue("");
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 4);
				cell.setCellValue("Ref Receipt Number");
				cell.setCellStyle(headerCellStyle);

				cell = row.createCell((short) 5);
				cell.setCellValue(refReceiptNo);
				cell.setCellStyle(dataCellStyle);

				row = spreadSheet.createRow(5);

				/*
				 * Giving values to header
				 */
				cell = row.createCell((short) 0);
				cell.setCellValue("Receipt Details");
				cell.setCellStyle(headerCellStyle);

				cell = row.createCell((short) 1);
				cell.setCellValue("");
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 2);
				cell.setCellValue("");
				cell.setCellStyle(headerCellStyle);

				cell = row.createCell((short) 3);
				cell.setCellValue("");
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 4);
				cell.setCellValue("");
				cell.setCellStyle(headerCellStyle);

				cell = row.createCell((short) 5);
				cell.setCellValue("");
				cell.setCellStyle(dataCellStyle);

				row = spreadSheet.createRow(6);

				/*
				 * Giving values to header
				 */
				cell = row.createCell((short) 0);
				cell.setCellValue("");
				cell.setCellStyle(headerCellStyle);

				cell = row.createCell((short) 1);
				cell.setCellValue("");
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 2);
				cell.setCellValue("");
				cell.setCellStyle(headerCellStyle);

				cell = row.createCell((short) 3);
				cell.setCellValue("");
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 4);
				cell.setCellValue("");
				cell.setCellStyle(headerCellStyle);

				cell = row.createCell((short) 5);
				cell.setCellValue("");
				cell.setCellStyle(dataCellStyle);

				row = spreadSheet.createRow(7);

				/*
				 * Giving values to header
				 */
				cell = row.createCell((short) 0);
				cell.setCellValue("Sr No.");
				cell.setCellStyle(headerCellStyle);

				cell = row.createCell((short) 1);
				cell.setCellValue("Product Req.");
				cell.setCellStyle(headerCellStyle);

				cell = row.createCell((short) 2);
				cell.setCellValue("Bag No.");
				cell.setCellStyle(headerCellStyle);

				cell = row.createCell((short) 3);
				cell.setCellValue("Quantity");
				cell.setCellStyle(headerCellStyle);

				cell = row.createCell((short) 4);
				cell.setCellValue("Rate");
				cell.setCellStyle(headerCellStyle);

				cell = row.createCell((short) 5);
				cell.setCellValue("Amount");
				cell.setCellStyle(headerCellStyle);

				/*
				 * For Receipt items values
				 */
				while (resultSet4.next()) {

					int receiptItemID = resultSet4.getInt("id");

					row = spreadSheet.createRow(currentRow++);

					/*
					 * Retrieving BagNo based on receiptItemID from BagLog table
					 */
					String bagNo = billingDAOInf.retrieveBagNoByReceiptItemID(receiptItemID);

					cell = row.createCell((short) 0);
					cell.setCellValue(srNo);
					cell.setCellStyle(dataCellStyle);

					cell = row.createCell((short) 1);
					cell.setCellValue(resultSet4.getString("product"));
					cell.setCellStyle(dataCellStyle);

					cell = row.createCell((short) 2);
					cell.setCellValue(bagNo);
					cell.setCellStyle(dataCellStyle);

					cell = row.createCell((short) 3);
					cell.setCellValue(resultSet4.getInt("quantity"));
					cell.setCellStyle(dataCellStyle);

					cell = row.createCell((short) 4);
					cell.setCellValue(resultSet4.getDouble("rate"));
					cell.setCellStyle(dataCellStyle);

					cell = row.createCell((short) 5);
					cell.setCellValue(resultSet4.getDouble("amount"));
					cell.setCellStyle(dataCellStyle);

					srNo++;

				}

				/*
				 * for other charges
				 */
				while (resultSet5.next()) {

					row = spreadSheet.createRow(currentRow);

					cell = row.createCell((short) 0);
					cell.setCellValue("");
					cell.setCellStyle(dataCellStyle);

					cell = row.createCell((short) 1);
					cell.setCellValue("");
					cell.setCellStyle(dataCellStyle);

					cell = row.createCell((short) 2);
					cell.setCellValue("");
					cell.setCellStyle(dataCellStyle);

					cell = row.createCell((short) 3);
					cell.setCellValue("Other Charges");
					cell.setCellStyle(headerCellStyle);

					cell = row.createCell((short) 4);
					cell.setCellValue(resultSet5.getString("chargeType"));
					cell.setCellStyle(dataCellStyle);

					cell = row.createCell((short) 5);
					cell.setCellValue(resultSet5.getDouble("otherCharges"));
					cell.setCellStyle(dataCellStyle);

					currentRow++;

				}

				row = spreadSheet.createRow(currentRow);

				cell = row.createCell((short) 0);
				cell.setCellValue("");
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 1);
				cell.setCellValue("");
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 2);
				cell.setCellValue("");
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 3);
				cell.setCellValue("Concession");
				cell.setCellStyle(headerCellStyle);

				cell = row.createCell((short) 4);
				cell.setCellValue(concession);
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 5);
				cell.setCellValue("" + concessionAmt);
				cell.setCellStyle(dataCellStyle);

				/*
				 * Checking whether paymentType is Card or Cheque, if its cheque then showing
				 * cheque details
				 */
				if (paymentType.equals("Cheque")) {

					currentRow++;

					row = spreadSheet.createRow(currentRow);

					/*
					 * Giving values to header
					 */
					cell = row.createCell((short) 0);
					cell.setCellValue("Receipt Details");
					cell.setCellStyle(headerCellStyle);

					cell = row.createCell((short) 1);
					cell.setCellValue("");
					cell.setCellStyle(dataCellStyle);

					cell = row.createCell((short) 2);
					cell.setCellValue("");
					cell.setCellStyle(headerCellStyle);

					cell = row.createCell((short) 3);
					cell.setCellValue("");
					cell.setCellStyle(dataCellStyle);

					cell = row.createCell((short) 4);
					cell.setCellValue("");
					cell.setCellStyle(headerCellStyle);

					cell = row.createCell((short) 5);
					cell.setCellValue("");
					cell.setCellStyle(dataCellStyle);

					currentRow++;

					row = spreadSheet.createRow(currentRow);

					cell = row.createCell((short) 0);
					cell.setCellValue("");
					cell.setCellStyle(headerCellStyle);

					cell = row.createCell((short) 1);
					cell.setCellValue("");
					cell.setCellStyle(dataCellStyle);

					cell = row.createCell((short) 2);
					cell.setCellValue("");
					cell.setCellStyle(headerCellStyle);

					cell = row.createCell((short) 3);
					cell.setCellValue("");
					cell.setCellStyle(dataCellStyle);

					cell = row.createCell((short) 4);
					cell.setCellValue("");
					cell.setCellStyle(headerCellStyle);

					cell = row.createCell((short) 5);
					cell.setCellValue("");
					cell.setCellStyle(dataCellStyle);

					currentRow++;

					row = spreadSheet.createRow(currentRow);

					cell = row.createCell((short) 0);
					cell.setCellValue("Cheque Issued By");
					cell.setCellStyle(headerCellStyle);

					cell = row.createCell((short) 1);
					cell.setCellValue(chequeIssuedBy);
					cell.setCellStyle(dataCellStyle);

					cell = row.createCell((short) 2);
					cell.setCellValue("Cheque No.");
					cell.setCellStyle(headerCellStyle);

					cell = row.createCell((short) 3);
					cell.setCellValue(chequNo);
					cell.setCellStyle(dataCellStyle);

					cell = row.createCell((short) 4);
					cell.setCellValue("Bank Name");
					cell.setCellStyle(headerCellStyle);

					cell = row.createCell((short) 5);
					cell.setCellValue(chequBankName);
					cell.setCellStyle(dataCellStyle);

					currentRow++;

					row = spreadSheet.createRow(currentRow);

					cell = row.createCell((short) 0);
					cell.setCellValue("Bank Branch");
					cell.setCellStyle(headerCellStyle);

					cell = row.createCell((short) 1);
					cell.setCellValue(chequeBankBranch);
					cell.setCellStyle(dataCellStyle);

					cell = row.createCell((short) 2);
					cell.setCellValue("Cheque Date");
					cell.setCellStyle(headerCellStyle);

					cell = row.createCell((short) 3);
					cell.setCellValue(chequDate);
					cell.setCellStyle(dataCellStyle);

					cell = row.createCell((short) 4);
					cell.setCellValue("Cheque Amount");
					cell.setCellStyle(headerCellStyle);

					cell = row.createCell((short) 5);
					cell.setCellValue(chequeAmt);
					cell.setCellStyle(dataCellStyle);

				}

				currentRow = 8;

				srNo = 1;
			}

			/*
			 * writing to the xlsx file
			 */
			ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
			wb.write(outByteStream);
			byte[] outArray = outByteStream.toByteArray();

			/*
			 * Write the output to a file
			 */
			FileOutputStream fileOut = new FileOutputStream(new File(excelFileName));
			fileOut.write(outArray);
			fileOut.flush();
			fileOut.close();

			System.out.println("Excel sheet created successfully.");

			status = "success";

			/*
			 * Closing resultSet, preparedStatement, connection objects
			 */
			resultSet6.close();
			preparedStatement6.close();

			resultSet5.close();
			preparedStatement5.close();

			resultSet4.close();
			preparedStatement4.close();

			resultSet3.close();
			preparedStatement3.close();

			resultSet1.close();
			preparedStatement1.close();

			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			status = "error";
		}

		return status;

	}

	/**
	 * 
	 * @param realPath
	 * @param excelFileName
	 * @return
	 */
	public String generateExportForTallyAccountsReport(String realPath, String excelFileName) {

		ListnerDAOInf daoInf = new ListnerDAOImpl();

		int currentRow = 1;

		int srNo = 1;

		final Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

		SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");

		String yesterdayDate = dateFormat1.format(cal.getTime());

		int count = 0;

		try {

			connection = getConnection(realPath);

			// Workbook wb = new HSSFWorkbook();
			XSSFWorkbook wb = new XSSFWorkbook();

			/*
			 * Generate the query to fetch Patient Details
			 */
			String fetchDetailQuery1 = QueryMaker.RETRIEVE_EXPORT_FOR_TALLY_ACCOUNT_DETAILS_BY_DATE;

			preparedStatement = connection.prepareStatement(fetchDetailQuery1);

			preparedStatement.setString(1, yesterdayDate + " 00:00");
			preparedStatement.setString(2, yesterdayDate + " 23:59");

			ResultSet resultSet1 = preparedStatement.executeQuery();

			System.out.println(resultSet1.getRow());

			// Sheet spreadSheet = wb.createSheet("Export For Accounts");
			XSSFSheet spreadSheet = wb.createSheet("Receipts");

			Row row;

			row = spreadSheet.createRow(0);

			/*
			 * Creating header in XLSX
			 */
			CellStyle headerCellStyle = wb.createCellStyle();

			/*
			 * Setting up the font
			 */
			Font setFont = wb.createFont();
			setFont.setFontHeightInPoints((short) 12);
			setFont.setBold(true);
			headerCellStyle.setFont(setFont);
			// headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
			// headerCellStyle.setBorderTop(CellStyle.BORDER_THIN);
			// headerCellStyle.setBorderRight(CellStyle.BORDER_THIN);
			// headerCellStyle.setBorderLeft(CellStyle.BORDER_THIN);

			/*
			 * Setting up Data style in XLSX
			 */
			CellStyle dataCellStyle = wb.createCellStyle();
			Font setDataFont = wb.createFont();
			setDataFont.setFontHeightInPoints((short) 12);
			dataCellStyle.setFont(setDataFont);
			// dataCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
			// dataCellStyle.setBorderTop(CellStyle.BORDER_THIN);
			// dataCellStyle.setBorderRight(CellStyle.BORDER_THIN);
			// dataCellStyle.setBorderLeft(CellStyle.BORDER_THIN);

			/*
			 * Initializing Cell reference variable
			 */
			Cell cell = null;

			/*
			 * Generating XLSX Header value for Patient Information
			 */
			spreadSheet.setColumnWidth((short) 0, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 1, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 2, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 3, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 4, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 5, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 6, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 7, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 8, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 9, (short) (256 * 25));

			/*
			 * Giving values to header
			 */
			cell = row.createCell((short) 0);
			cell.setCellValue("SrNo");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 1);
			cell.setCellValue("Voucher Number");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 2);
			cell.setCellValue("Date");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 3);
			cell.setCellValue("LedgerName");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 4);
			cell.setCellValue("Bill No/Reference No");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 5);
			cell.setCellValue("Cash/Bank Name");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 6);
			cell.setCellValue("Cheque No");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 7);
			cell.setCellValue("Amount");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 8);
			cell.setCellValue("Narration");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 9);
			cell.setCellValue("Deduction if any");
			cell.setCellStyle(headerCellStyle);

			/*
			 * For Receipt items values
			 */
			while (resultSet1.next()) {

				count = 1;

				row = spreadSheet.createRow(currentRow++);

				cell = row.createCell((short) 0);
				cell.setCellValue(srNo);
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 1);
				cell.setCellValue(resultSet1.getInt("id"));
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 2);
				cell.setCellValue(dateFormat.format(resultSet1.getTimestamp("receiptDate")));
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 3);
				cell.setCellValue(resultSet1.getString("product"));
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 4);
				cell.setCellValue(resultSet1.getString("receiptNo"));
				cell.setCellStyle(dataCellStyle);

				if (resultSet1.getString("paymentType").equals("Cash")) {
					cell = row.createCell((short) 5);
					cell.setCellValue("Cash in Hand");
					cell.setCellStyle(dataCellStyle);
				} else if (resultSet1.getString("paymentType").equals("Cheque")) {

					BillingForm billingForm = daoInf.retrieveChequeCardDetails(realPath, resultSet1.getInt("id"));

					cell = row.createCell((short) 5);
					cell.setCellValue(billingForm.getChequeBankName());
					cell.setCellStyle(dataCellStyle);
				} else if (resultSet1.getString("paymentType").equals("Credit/Debit Card")) {

					// BillingForm billingForm = daoInf.retrieveChequeCardDetails(realPath,
					// resultSet.getInt("id"));

					cell = row.createCell((short) 5);
					cell.setCellValue("M-Swep Sale");
					cell.setCellStyle(dataCellStyle);

				} else if (!resultSet1.getString("bloodBankStorage").equals("000")) {

					// BillingForm billingForm = daoInf.retrieveChequeCardDetails(realPath,
					// resultSet.getInt("id"));

					cell = row.createCell((short) 5);
					cell.setCellValue(resultSet1.getString("bloodBankStorage"));
					cell.setCellStyle(dataCellStyle);

				} else {
					continue;
				}

				if (resultSet1.getString("paymentType").equals("Cash")) {
					cell = row.createCell((short) 6);
					cell.setCellValue("");
					cell.setCellStyle(dataCellStyle);
				} else if (resultSet1.getString("paymentType").equals("Cheque")) {

					BillingForm billingForm = daoInf.retrieveChequeCardDetails(realPath, resultSet1.getInt("id"));

					cell = row.createCell((short) 6);
					cell.setCellValue(billingForm.getChequeNo());
					cell.setCellStyle(dataCellStyle);
				} else if (resultSet1.getString("paymentType").equals("Credit/Debit Card")) {

					BillingForm billingForm = daoInf.retrieveChequeCardDetails(realPath, resultSet1.getInt("id"));

					cell = row.createCell((short) 6);
					cell.setCellValue(billingForm.getCardMobileNo());
					cell.setCellStyle(dataCellStyle);

				} else if (!resultSet1.getString("bloodBankStorage").equals("000")) {

					// BillingForm billingForm = daoInf.retrieveChequeCardDetails(realPath,
					// resultSet.getInt("id"));

					cell = row.createCell((short) 6);
					cell.setCellValue("");
					cell.setCellStyle(dataCellStyle);

				} else {
					continue;
				}

				cell = row.createCell((short) 7);
				cell.setCellValue(resultSet1.getDouble("netReceivableAmt"));
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 8);
				cell.setCellValue(resultSet1.getString("receiptNo"));
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 9);
				cell.setCellValue("");
				cell.setCellStyle(dataCellStyle);

				srNo++;

			}

			if (count == 1) {

				/*
				 * writing to the xlsx file
				 */
				ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
				wb.write(outByteStream);
				byte[] outArray = outByteStream.toByteArray();

				/*
				 * Write the output to a file
				 */
				FileOutputStream fileOut = new FileOutputStream(new File(excelFileName));
				fileOut.write(outArray);
				fileOut.flush();
				fileOut.close();

				System.out.println("Excel sheet for export for tally accounts is created successfully.");

				status = "success";

			} else {

				System.out.println("No records found for the date: " + yesterdayDate);

				status = "input";

			}

			/*
			 * Closing resultSet, preparedStatement, connection objects
			 */
			resultSet1.close();
			preparedStatement.close();

			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			status = "error";
		}

		return status;

	}

	/**
	 * 
	 * @param reportForm
	 * @param realPath
	 * @param excelFileName
	 * @return
	 */
	public String generateExportForTallyAccountsExcel(ReportForm reportForm, String realPath, String excelFileName) {

		ReportDAOInf daoInf = new ReportDAOImpl();

		int currentRow = 1;

		int srNo = 1;

		SimpleDateFormat dateToBeFormatted = new SimpleDateFormat("yyyy-MM-dd");

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

		int count = 0;

		try {

			connection1 = getConnection();

			// Workbook wb = new HSSFWorkbook();
			XSSFWorkbook wb = new XSSFWorkbook();

			/*
			 * If searchValue is 1, then display today's record
			 */
			if (reportForm.getSearchExportForAcctName().equals("1")) {

				/*
				 * Generate the query to fetch Patient Details
				 */
				String fetchDetailQuery1 = QueryMaker.RETRIEVE_EXPORT_FOR_TALLY_ACCOUNT_DETAILS_TODAY;

				preparedStatement1 = connection1.prepareStatement(fetchDetailQuery1);

				resultSet1 = preparedStatement1.executeQuery();

				/*
				 * If searchValue is 2, then display this week's record
				 */
			} else if (reportForm.getSearchExportForAcctName().equals("2")) {

				/*
				 * Generate the query to fetch Patient Details
				 */
				String fetchDetailQuery1 = QueryMaker.RETRIEVE_EXPORT_FOR_TALLY_ACCOUNT_DETAILS_THIS_WEEK;

				preparedStatement1 = connection1.prepareStatement(fetchDetailQuery1);

				resultSet1 = preparedStatement1.executeQuery();

				/*
				 * If searchValue is 3, then display this month's record
				 */
			} else if (reportForm.getSearchExportForAcctName().equals("3")) {

				/*
				 * Generate the query to fetch Patient Details
				 */
				String fetchDetailQuery1 = QueryMaker.RETRIEVE_EXPORT_FOR_TALLY_ACCOUNT_DETAILS_THIS_MONTH;

				preparedStatement1 = connection1.prepareStatement(fetchDetailQuery1);

				resultSet1 = preparedStatement1.executeQuery();

				/*
				 * If searchValue is 4, then display record from start date to end date
				 */
			} else {

				/*
				 * Generate the query to fetch Patient Details
				 */
				String fetchDetailQuery1 = QueryMaker.RETRIEVE_EXPORT_FOR_TALLY_ACCOUNT_DETAILS_BY_DATE;

				preparedStatement1 = connection1.prepareStatement(fetchDetailQuery1);

				preparedStatement1.setString(1,
						dateToBeFormatted.format(dateFormat.parse(reportForm.getStartDate())) + " 00:00");
				preparedStatement1.setString(2,
						dateToBeFormatted.format(dateFormat.parse(reportForm.getEndDate())) + " 23:59");

				resultSet1 = preparedStatement1.executeQuery();
			}

			ResultSet resultSet1 = preparedStatement1.executeQuery();

			System.out.println(resultSet1.getRow());

			// Sheet spreadSheet = wb.createSheet("Export For Accounts");
			XSSFSheet spreadSheet = wb.createSheet("Receipts");

			Row row;

			row = spreadSheet.createRow(0);

			/*
			 * Creating header in XLSX
			 */
			CellStyle headerCellStyle = wb.createCellStyle();

			/*
			 * Setting up the font
			 */
			Font setFont = wb.createFont();
			setFont.setFontHeightInPoints((short) 12);
			setFont.setBold(true);
			headerCellStyle.setFont(setFont);
			// headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
			// headerCellStyle.setBorderTop(CellStyle.BORDER_THIN);
			// headerCellStyle.setBorderRight(CellStyle.BORDER_THIN);
			// headerCellStyle.setBorderLeft(CellStyle.BORDER_THIN);

			/*
			 * Setting up Data style in XLSX
			 */
			CellStyle dataCellStyle = wb.createCellStyle();
			Font setDataFont = wb.createFont();
			setDataFont.setFontHeightInPoints((short) 12);
			dataCellStyle.setFont(setDataFont);
			// dataCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
			// dataCellStyle.setBorderTop(CellStyle.BORDER_THIN);
			// dataCellStyle.setBorderRight(CellStyle.BORDER_THIN);
			// dataCellStyle.setBorderLeft(CellStyle.BORDER_THIN);

			/*
			 * Initializing Cell reference variable
			 */
			Cell cell = null;

			/*
			 * Generating XLSX Header value for Patient Information
			 */
			spreadSheet.setColumnWidth((short) 0, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 1, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 2, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 3, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 4, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 5, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 6, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 7, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 8, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 9, (short) (256 * 25));

			/*
			 * Giving values to header
			 */
			cell = row.createCell((short) 0);
			cell.setCellValue("SrNo");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 1);
			cell.setCellValue("Voucher Number");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 2);
			cell.setCellValue("Date");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 3);
			cell.setCellValue("LedgerName");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 4);
			cell.setCellValue("Bill No/Reference No");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 5);
			cell.setCellValue("Cash/Bank Name");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 6);
			cell.setCellValue("Cheque No");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 7);
			cell.setCellValue("Amount");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 8);
			cell.setCellValue("Narration");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 9);
			cell.setCellValue("Deduction if any");
			cell.setCellStyle(headerCellStyle);

			/*
			 * For Receipt items values
			 */
			while (resultSet1.next()) {

				count = 1;

				row = spreadSheet.createRow(currentRow++);

				cell = row.createCell((short) 0);
				cell.setCellValue(srNo);
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 1);
				cell.setCellValue(resultSet1.getInt("id"));
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 2);
				cell.setCellValue(dateFormat.format(resultSet1.getTimestamp("receiptDate")));
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 3);
				cell.setCellValue(resultSet1.getString("product"));
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 4);
				cell.setCellValue(resultSet1.getString("receiptNo"));
				cell.setCellStyle(dataCellStyle);

				if (resultSet1.getString("paymentType").equals("Cash")) {
					cell = row.createCell((short) 5);
					cell.setCellValue("Cash in Hand");
					cell.setCellStyle(dataCellStyle);
				} else if (resultSet1.getString("paymentType").equals("Cheque")) {

					BillingForm billingForm = daoInf.retrieveChequeCardDetails(resultSet1.getInt("id"));

					cell = row.createCell((short) 5);
					cell.setCellValue(billingForm.getChequeBankName());
					cell.setCellStyle(dataCellStyle);
				} else if (resultSet1.getString("paymentType").equals("Credit/Debit Card")) {

					// BillingForm billingForm = daoInf.retrieveChequeCardDetails(realPath,
					// resultSet.getInt("id"));

					cell = row.createCell((short) 5);
					cell.setCellValue("M-Swep Sale");
					cell.setCellStyle(dataCellStyle);

				} else if (!resultSet1.getString("bloodBankStorage").equals("000")) {

					// BillingForm billingForm = daoInf.retrieveChequeCardDetails(realPath,
					// resultSet.getInt("id"));

					cell = row.createCell((short) 5);
					cell.setCellValue(resultSet1.getString("bloodBankStorage"));
					cell.setCellStyle(dataCellStyle);

				} else {
					continue;
				}

				if (resultSet1.getString("paymentType").equals("Cash")) {
					cell = row.createCell((short) 6);
					cell.setCellValue("");
					cell.setCellStyle(dataCellStyle);
				} else if (resultSet1.getString("paymentType").equals("Cheque")) {

					BillingForm billingForm = daoInf.retrieveChequeCardDetails(resultSet1.getInt("id"));

					cell = row.createCell((short) 6);
					cell.setCellValue(billingForm.getChequeNo());
					cell.setCellStyle(dataCellStyle);
				} else if (resultSet1.getString("paymentType").equals("Credit/Debit Card")) {

					BillingForm billingForm = daoInf.retrieveChequeCardDetails(resultSet1.getInt("id"));

					cell = row.createCell((short) 6);
					cell.setCellValue(billingForm.getCardMobileNo());
					cell.setCellStyle(dataCellStyle);

				} else if (!resultSet1.getString("bloodBankStorage").equals("000")) {

					// BillingForm billingForm = daoInf.retrieveChequeCardDetails(realPath,
					// resultSet.getInt("id"));

					cell = row.createCell((short) 6);
					cell.setCellValue("");
					cell.setCellStyle(dataCellStyle);

				} else {
					continue;
				}

				cell = row.createCell((short) 7);
				cell.setCellValue(resultSet1.getDouble("netReceivableAmt"));
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 8);
				cell.setCellValue(resultSet1.getString("receiptNo"));
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 9);
				cell.setCellValue("");
				cell.setCellStyle(dataCellStyle);

				srNo++;

			}

			if (count == 1) {

				/*
				 * writing to the xlsx file
				 */
				ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
				wb.write(outByteStream);
				byte[] outArray = outByteStream.toByteArray();

				/*
				 * Write the output to a file
				 */
				FileOutputStream fileOut = new FileOutputStream(new File(excelFileName));
				fileOut.write(outArray);
				fileOut.flush();
				fileOut.close();

				System.out.println("Excel sheet for export for tally accounts is created successfully.");

				status = "success";

			} else {

				System.out.println("No records found for the date: ");

				status = "input";

			}

			/*
			 * Closing resultSet, preparedStatement, connection objects
			 */
			resultSet1.close();
			preparedStatement1.close();

			connection1.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			status = "error";
		}

		return status;

	}

	/**
	 * 
	 * @param storageCenterList
	 * @param realPath
	 * @param excelFileName
	 * @return
	 */
	public String generateStorageCenterExcel(List<ReportForm> storageCenterList, String realPath,
			String excelFileName) {

		int currentRow = 1;

		int srNo = 1;

		try {

			// Workbook wb = new HSSFWorkbook();
			XSSFWorkbook wb = new XSSFWorkbook();

			XSSFSheet spreadSheet = wb.createSheet("Storage Centers");
			Row row;

			row = spreadSheet.createRow(0);

			/*
			 * Creating header in XLSX
			 */
			CellStyle headerCellStyle = wb.createCellStyle();

			/*
			 * Setting up the font
			 */
			Font setFont = wb.createFont();
			setFont.setFontHeightInPoints((short) 12);
			setFont.setBold(true);
			headerCellStyle.setFont(setFont);
			// headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
			// headerCellStyle.setBorderTop(CellStyle.BORDER_THIN);
			// headerCellStyle.setBorderRight(CellStyle.BORDER_THIN);
			// headerCellStyle.setBorderLeft(CellStyle.BORDER_THIN);

			/*
			 * Setting up Data style in XLSX
			 */
			CellStyle dataCellStyle = wb.createCellStyle();
			Font setDataFont = wb.createFont();
			setDataFont.setFontHeightInPoints((short) 12);
			dataCellStyle.setFont(setDataFont);
			// dataCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
			// dataCellStyle.setBorderTop(CellStyle.BORDER_THIN);
			// dataCellStyle.setBorderRight(CellStyle.BORDER_THIN);
			// dataCellStyle.setBorderLeft(CellStyle.BORDER_THIN);

			/*
			 * Initializing Cell reference variable
			 */
			Cell cell = null;

			/*
			 * Generating XLSX Header value for Patient Information
			 */
			spreadSheet.setColumnWidth((short) 0, (short) (256 * 25));
			spreadSheet.setColumnWidth((short) 1, (short) (256 * 75));

			/*
			 * Giving values to header
			 */
			cell = row.createCell((short) 0);
			cell.setCellValue("Sr No.");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell((short) 1);
			cell.setCellValue("Storage Center");
			cell.setCellStyle(headerCellStyle);

			/*
			 * For Receipt items values
			 */
			for (ReportForm form : storageCenterList) {

				row = spreadSheet.createRow(currentRow++);

				cell = row.createCell((short) 0);
				cell.setCellValue(srNo);
				cell.setCellStyle(dataCellStyle);

				cell = row.createCell((short) 1);
				cell.setCellValue(form.getStorageCenter());
				cell.setCellStyle(dataCellStyle);

				srNo++;

			}

			/*
			 * writing to the xlsx file
			 */
			ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
			wb.write(outByteStream);
			byte[] outArray = outByteStream.toByteArray();

			/*
			 * Write the output to a file
			 */
			FileOutputStream fileOut = new FileOutputStream(new File(excelFileName));
			fileOut.write(outArray);
			fileOut.flush();
			fileOut.close();

			System.out.println("Excel sheet for storage center is created successfully.");

			status = "success";

		} catch (Exception exception) {
			exception.printStackTrace();

			status = "error";
		}

		return status;

	}

}
