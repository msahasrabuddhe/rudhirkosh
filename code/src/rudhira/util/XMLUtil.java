package rudhira.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import rudhira.DAO.BillingDAOImpl;
import rudhira.DAO.BillingDAOInf;
import rudhira.DAO.UserDAOImpl;
import rudhira.DAO.UserDAOInf;
import rudhira.form.BillingForm;
import rudhira.form.ReportForm;

public class XMLUtil extends DBConnection {

	Connection connection = null;

	PreparedStatement preparedStatement = null;
	ResultSet resultSet = null;

	PreparedStatement preparedStatement1 = null;
	ResultSet resultSet1 = null;

	String status = "error";

	BillingDAOInf billingDAOInf = null;

	UserDAOInf userDAOInf = null;

	public String generateAccountXML(ReportForm reportForm, int bloodBankID, String realPath, String xmlFileName,
			String sendCheck) {

		userDAOInf = new UserDAOImpl();

		billingDAOInf = new BillingDAOImpl();

		SimpleDateFormat dateToBeFormatted = new SimpleDateFormat("yyyy-MM-dd");

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

		SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyyMMdd");

		SimpleDateFormat dateTimeFormat = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");

		CURLConfigUtil curlConfigUtil = new CURLConfigUtil();
		
		ConfigXMLUtil configXMLUtil = new ConfigXMLUtil();

		/*
		 * Retrieving bloodBank name
		 */
		// String bloodBankName = userDAOInf.retrieveBloodBankName(bloodBankID);

		int receiptID = 0;
		String receiptNo = "";

		double netAmount = 0D;
		double outstandingAmount = 0D;
		double concessionAmount = 0D;
		String concessionType = "";
		double actualPayment = 0D;

		String receiptType = "";

		String hospitalName = "";
		String storageCenter = "";

		String receiptDate = "";

		int counter = 1;

		try {

			File zipFile = new File(xmlFileName);

			// OutputStream outputStream = new
			// FileOutputStream(zipFile.getAbsolutePath());
			FileOutputStream outputStream = new FileOutputStream(zipFile.getAbsolutePath());

			ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(outputStream));

			connection = getConnection();

			/*
			 * If searchValue is 1, then display today's record
			 */
			if (reportForm.getSearchExportForAcctName().equals("1")) {

				/*
				 * Generate the query to fetch Patient Details
				 */
				String fetchDetailQuery1 = QueryMaker.RETRIEVE_EXPORT_FOR_ACCOUNT_DETAILS_TODAY;

				preparedStatement1 = connection.prepareStatement(fetchDetailQuery1);

				resultSet1 = preparedStatement1.executeQuery();

				/*
				 * If searchValue is 2, then display this week's record
				 */
			} else if (reportForm.getSearchExportForAcctName().equals("2")) {

				/*
				 * Generate the query to fetch Patient Details
				 */
				String fetchDetailQuery1 = QueryMaker.RETRIEVE_EXPORT_FOR_ACCOUNT_DETAILS_THIS_WEEK;

				preparedStatement1 = connection.prepareStatement(fetchDetailQuery1);

				resultSet1 = preparedStatement1.executeQuery();

				/*
				 * If searchValue is 3, then display this month's record
				 */
			} else if (reportForm.getSearchExportForAcctName().equals("3")) {

				/*
				 * Generate the query to fetch Patient Details
				 */
				String fetchDetailQuery1 = QueryMaker.RETRIEVE_EXPORT_FOR_ACCOUNT_DETAILS_THIS_MONTH;

				preparedStatement1 = connection.prepareStatement(fetchDetailQuery1);

				resultSet1 = preparedStatement1.executeQuery();

				/*
				 * If searchValue is 4, then display record from start date to
				 * end date
				 */
			} else {

				/*
				 * Generate the query to fetch Patient Details
				 */
				String fetchDetailQuery1 = QueryMaker.RETRIEVE_EXPORT_FOR_ACCOUNT_DETAILS_BY_DATE;

				preparedStatement1 = connection.prepareStatement(fetchDetailQuery1);

				preparedStatement1.setString(1,
						dateToBeFormatted.format(dateFormat.parse(reportForm.getStartDate())) + " 00:00");
				preparedStatement1.setString(2,
						dateToBeFormatted.format(dateFormat.parse(reportForm.getEndDate())) + " 23:59");

				resultSet1 = preparedStatement1.executeQuery();
			}

			while (resultSet1.next()) {

				/*
				 * Creating XML tags
				 */
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();

				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

				Document doc = dBuilder.newDocument();

				// Creating root element
				Element rootElement = doc.createElement("ENVELOPE");

				// Appending root element to the doc object
				doc.appendChild(rootElement);

				// Creating HEADER element inside ENVELOPE root element
				Element headerElement = doc.createElement("HEADER");

				// Appending headerElement to the rootElement
				rootElement.appendChild(headerElement);

				// Creating TALLYREQUEST element inside headerElement
				Element tallyRequestElement = doc.createElement("TALLYREQUEST");

				// Adding test into tallyRequestElement
				tallyRequestElement.appendChild(doc.createTextNode("Import Data"));

				// Appending tallyRequestElement to headerELement
				headerElement.appendChild(tallyRequestElement);

				// Creating BODY element inside rootElement
				Element bodyElement = doc.createElement("BODY");

				// Appending bodyElement to the rootElement
				rootElement.appendChild(bodyElement);

				// Creating importData element inside body element
				Element importDataElement = doc.createElement("IMPORTDATA");

				// Appending importDataElement to the bodyElement
				bodyElement.appendChild(importDataElement);

				// Creating REQUESTDESC element inside importDataElement
				Element requestDescElement = doc.createElement("REQUESTDESC");

				// Appending requestDescElement to importDataElement
				importDataElement.appendChild(requestDescElement);

				// Creating reportName element inside requestDescElement
				Element reportNameElement = doc.createElement("REPORTNAME");

				// Adding text into reportNameElement
				reportNameElement.appendChild(doc.createTextNode("Vouchers"));

				// Appending reportNameElement to the requestDescElement
				requestDescElement.appendChild(reportNameElement);

				// Creating static variable element inside requestDescElement
				Element staticVariableElement = doc.createElement("STATICVARIABLES");

				// Appending staticVariableElement to the requestDescElement
				requestDescElement.appendChild(staticVariableElement);

				// Creating svCurrentCOmpany element inside
				// staticVariableElement
				Element svCurrentCompanyElement = doc.createElement("SVCURRENTCOMPANY");

				// Adding text into svCurrentCompanyElement
				svCurrentCompanyElement
						.appendChild(doc.createTextNode("Janakalyan Rakta Pedhi,Pune - (from 1-Apr-2016)"));

				// Appending svCurrentCompanyElement to the
				// staticVariableElement
				staticVariableElement.appendChild(svCurrentCompanyElement);

				// Creating requestData element inside importData element
				Element requestDataElement = doc.createElement("REQUESTDATA");

				// Appending requestDataElement to the importDataElement
				importDataElement.appendChild(requestDataElement);

				// Creating tallyMessage element inside requestDataElement
				Element tallyMessageElement = doc.createElement("TALLYMESSAGE");

				// Appending tallyMessageElement to the requestDateElement
				requestDataElement.appendChild(tallyMessageElement);

				// Creating attribute inside tallyElement
				Attr tallyMessageAttr = doc.createAttribute("xmlns:UDF");

				// Setting value to created attribute
				tallyMessageAttr.setValue("TallyUDF");

				// Setting this attribute to tallyMessageElement
				tallyMessageElement.setAttributeNode(tallyMessageAttr);

				// Generating UUID for voucher remoteID and GUID
				UUID remoteKey = UUID.randomUUID();

				UUID guid = UUID.randomUUID();

				receiptID = resultSet1.getInt("id");
				receiptNo = resultSet1.getString("receiptNo");

				netAmount = resultSet1.getDouble("netReceivableAmt");
				actualPayment = resultSet1.getDouble("actualPayment");
				outstandingAmount = resultSet1.getDouble("outstandingAmt");

				concessionAmount = resultSet1.getDouble("concessionAmt");
				concessionType = resultSet1.getString("concessionType");

				receiptDate = dateFormat1.format(resultSet1.getTimestamp("receiptDate"));

				receiptType = resultSet1.getString("receiptType");
				hospitalName = resultSet1.getString("hospital");
				storageCenter = resultSet1.getString("bloodBankStorage");

				/*
				 * Retrieving other charges details from OtherCharges table
				 * based on receiptID
				 */
				List<BillingForm> otherChargeList = billingDAOInf.retrieveOtherCharges(receiptID);

				/*
				 * Retrieving component List based on receiptID from
				 * ReceitpItems table
				 */
				List<BillingForm> componentList = billingDAOInf.retrieveComponent(receiptID);

				// Creating voucher element inside tallYmessageElement
				Element voucherElement = doc.createElement("VOUCHER");

				// Appending voucherElement to the tallyMessageElement
				tallyMessageElement.appendChild(voucherElement);

				// Adding remoteID attribute to voucherElement
				Attr remoteIDAttr = doc.createAttribute("REMOTEID");

				// Setting value to remoteIDAttr
				remoteIDAttr.setValue(remoteKey.toString());

				// Appending remoteIDAttr to voucherElement
				voucherElement.setAttributeNode(remoteIDAttr);

				// Creating vhkey atrribute
				Attr vhKeyAttr = doc.createAttribute("VCHKEY");

				// Setting value to vhKeyAttr
				vhKeyAttr.setValue(guid.toString());

				// Adding vhKeyAttr to voucherElement
				voucherElement.setAttributeNode(vhKeyAttr);

				// Creating attribute vchType
				Attr vchTypeAttr = doc.createAttribute("VCHTYPE");

				// Seting value to it
				vchTypeAttr.setValue("Sales");

				// Adding vchTypeAttr to voucherElement
				voucherElement.setAttributeNode(vchTypeAttr);

				// Creating attribute ACTION
				Attr actionAttr = doc.createAttribute("ACTION");

				// Setting value to it
				actionAttr.setValue("Create");

				// Adding this attribute to voucherElement
				voucherElement.setAttributeNode(actionAttr);

				// Creating attribute OBJVIEw
				Attr objViewAttr = doc.createAttribute("OBJVIEW");

				// Setting value to it
				objViewAttr.setValue("Accounting Voucher View");

				// Adding this attribute to voucherElement
				voucherElement.setAttributeNode(objViewAttr);

				// Creating date element inside tallyMessageElement
				Element dateElement = doc.createElement("DATE");

				// Setting value into it
				dateElement.appendChild(doc.createTextNode(receiptDate));

				// Appending this element to voucherElement
				voucherElement.appendChild(dateElement);

				// Creating element GUID inside voucherElement
				Element guidElement = doc.createElement("GUID");

				// Setting value into it
				guidElement.appendChild(doc.createTextNode(remoteKey.toString()));

				// Appending this element to voucherElement
				voucherElement.appendChild(guidElement);

				// Creating element narration inside voucherElement
				Element narrationElement = doc.createElement("NARRATION");

				// Setting value into it
				narrationElement.appendChild(doc.createTextNode(receiptNo));

				// Appending this element to voucherElement
				voucherElement.appendChild(narrationElement);

				// Creating element vouchertypename inside voucherElement
				Element voucherTypeNameElement = doc.createElement("VOUCHERTYPENAME");

				// Setting value into it
				voucherTypeNameElement.appendChild(doc.createTextNode("Sales"));

				// Appending this element to voucherElement
				voucherElement.appendChild(voucherTypeNameElement);

				// Creating element referenceElement inside voucherElement
				Element referenceElement = doc.createElement("REFERENCE");

				// Setting value into it
				referenceElement.appendChild(doc.createTextNode(receiptNo));

				// Appending this element to voucherElement
				voucherElement.appendChild(referenceElement);

				// Creating element voucherNo inside voucherElement
				Element voucherNoElement = doc.createElement("VOUCHERNUMBER");

				// Setting value into it
				voucherNoElement.appendChild(doc.createTextNode("" + receiptID));

				// Appending this element to voucherElement
				voucherElement.appendChild(voucherNoElement);

				// Creating element cstFormIssueType inside voucherElement
				Element cstFormIssueType = doc.createElement("CSTFORMISSUETYPE");

				// Appending this element to voucherElement
				voucherElement.appendChild(cstFormIssueType);

				// Creating element cstFormRecvType inside voucherElement
				Element cstFormRecvType = doc.createElement("CSTFORMRECVTYPE");

				// Appending this element to voucherElement
				voucherElement.appendChild(cstFormRecvType);

				/*
				 * Checking whether receiptType is other than Patient, if so,
				 * then adding PARTYLEDGERNAME tag, else not
				 */
				if (receiptType.equals("BSC") || receiptType.equals("IBBT")) {

					// Creating element partyLedgerName inside voucherElement
					Element partyLedgerNameElement = doc.createElement("PARTYLEDGERNAME");

					// Setting value into it
					partyLedgerNameElement.appendChild(doc.createTextNode(storageCenter));

					// Appending this element to voucherElement
					voucherElement.appendChild(partyLedgerNameElement);

				} else if (receiptType.equals("SNT") || receiptType.equals("TPP") || receiptType.equals("TPB")) {

					// Creating element partyLedgerNameElement inside
					// voucherElement
					Element partyLedgerNameElement = doc.createElement("PARTYLEDGERNAME");

					// Setting value into it
					partyLedgerNameElement.appendChild(doc.createTextNode(hospitalName));

					// Appending this element to voucherElement
					voucherElement.appendChild(partyLedgerNameElement);

				}

				// Creating element persistedView inside voucherElement
				Element persistedViewElement = doc.createElement("PERSISTEDVIEW");

				// Setting value into it
				persistedViewElement.appendChild(doc.createTextNode("Accounting Voucher View"));

				// Appending this element to voucherElement
				voucherElement.appendChild(persistedViewElement);

				// Creating element vchGSTClass inside voucherElement
				Element vchGSTClass = doc.createElement("VCHGSTCLASS");

				// Appending this element to voucherElement
				voucherElement.appendChild(vchGSTClass);

				// Creating element deffactualQty inside voucherElement
				Element deffactualQtyElement = doc.createElement("DIFFACTUALQTY");

				// Setting value into it
				deffactualQtyElement.appendChild(doc.createTextNode("No"));

				// Appending this element to voucherElement
				voucherElement.appendChild(deffactualQtyElement);

				// Creating element ASORIGINAL inside voucherElement
				Element ASORIGINALElement = doc.createElement("ASORIGINAL");

				// Setting value into it
				ASORIGINALElement.appendChild(doc.createTextNode("No"));

				// Appending this element to voucherElement
				voucherElement.appendChild(ASORIGINALElement);

				// Creating element FORJOBCOSTING inside voucherElement
				Element FORJOBCOSTINGElement = doc.createElement("FORJOBCOSTING");

				// Setting value into it
				FORJOBCOSTINGElement.appendChild(doc.createTextNode("No"));

				// Appending this element to voucherElement
				voucherElement.appendChild(FORJOBCOSTINGElement);

				// Creating element FORJOBCOSTING inside voucherElement
				Element ISOPTIONALElement = doc.createElement("FORJOBCOSTING");

				// Setting value into it
				ISOPTIONALElement.appendChild(doc.createTextNode("No"));

				// Appending this element to voucherElement
				voucherElement.appendChild(ISOPTIONALElement);

				// Creating element EFFECTIVEDATE inside voucherElement
				Element EFFECTIVEDATEElement = doc.createElement("EFFECTIVEDATE");

				// Setting value into it
				EFFECTIVEDATEElement.appendChild(doc.createTextNode(receiptDate));

				// Appending this element to voucherElement
				voucherElement.appendChild(EFFECTIVEDATEElement);

				// Creating element USEFOREXCISE inside voucherElement
				Element USEFOREXCISEElement = doc.createElement("USEFOREXCISE");

				// Setting value into it
				USEFOREXCISEElement.appendChild(doc.createTextNode("No"));

				// Appending this element to voucherElement
				voucherElement.appendChild(USEFOREXCISEElement);

				// Creating element USEFORINTEREST inside voucherElement
				Element USEFORINTERESTElement = doc.createElement("USEFORINTEREST");

				// Setting value into it
				USEFORINTERESTElement.appendChild(doc.createTextNode("No"));

				// Appending this element to voucherElement
				voucherElement.appendChild(USEFORINTERESTElement);

				// Creating element USEFORGAINLOSS inside voucherElement
				Element USEFORGAINLOSSElement = doc.createElement("USEFORGAINLOSS");

				// Setting value into it
				USEFORGAINLOSSElement.appendChild(doc.createTextNode("No"));

				// Appending this element to voucherElement
				voucherElement.appendChild(USEFORGAINLOSSElement);

				// Creating element USEFORGODOWNTRANSFER inside
				// voucherElement
				Element USEFORGODOWNTRANSFERElement = doc.createElement("USEFORGODOWNTRANSFER");

				// Setting value into it
				USEFORGODOWNTRANSFERElement.appendChild(doc.createTextNode("No"));

				// Appending this element to voucherElement
				voucherElement.appendChild(USEFORGODOWNTRANSFERElement);

				// Creating element USEFORCOMPOUND inside
				// voucherElement
				Element USEFORCOMPOUNDElement = doc.createElement("USEFORCOMPOUND");

				// Setting value into it
				USEFORCOMPOUNDElement.appendChild(doc.createTextNode("No"));

				// Appending this element to voucherElement
				voucherElement.appendChild(USEFORCOMPOUNDElement);

				// Creating element USEFORSERVICETAX inside
				// voucherElement
				Element USEFORSERVICETAXElement = doc.createElement("USEFORSERVICETAX");

				// Setting value into it
				USEFORSERVICETAXElement.appendChild(doc.createTextNode("No"));

				// Appending this element to voucherElement
				voucherElement.appendChild(USEFORSERVICETAXElement);

				// Creating element EXCISETAXOVERRIDE inside
				// voucherElement
				Element EXCISETAXOVERRIDEElement = doc.createElement("EXCISETAXOVERRIDE");

				// Setting value into it
				EXCISETAXOVERRIDEElement.appendChild(doc.createTextNode("No"));

				// Appending this element to voucherElement
				voucherElement.appendChild(EXCISETAXOVERRIDEElement);

				// Creating element ISTDSOVERRIDDEN inside
				// voucherElement
				Element ISTDSOVERRIDDENElement = doc.createElement("ISTDSOVERRIDDEN");

				// Setting value into it
				ISTDSOVERRIDDENElement.appendChild(doc.createTextNode("No"));

				// Appending this element to voucherElement
				voucherElement.appendChild(ISTDSOVERRIDDENElement);

				// Creating element ISTCSOVERRIDDEN inside
				// voucherElement
				Element ISTCSOVERRIDDENElement = doc.createElement("ISTCSOVERRIDDEN");

				// Setting value into it
				ISTCSOVERRIDDENElement.appendChild(doc.createTextNode("No"));

				// Appending this element to voucherElement
				voucherElement.appendChild(ISTCSOVERRIDDENElement);

				// Creating element ISVATOVERRIDDENElement inside
				// voucherElement
				Element ISVATOVERRIDDENElement = doc.createElement("ISVATOVERRIDDEN");

				// Setting value into it
				ISVATOVERRIDDENElement.appendChild(doc.createTextNode("No"));

				// Appending this element to voucherElement
				voucherElement.appendChild(ISVATOVERRIDDENElement);

				// Creating element ISSERVICETAXOVERRIDDEN inside
				// voucherElement
				Element ISSERVICETAXOVERRIDDENElement = doc.createElement("ISSERVICETAXOVERRIDDEN");

				// Setting value into it
				ISSERVICETAXOVERRIDDENElement.appendChild(doc.createTextNode("No"));

				// Appending this element to voucherElement
				voucherElement.appendChild(ISSERVICETAXOVERRIDDENElement);

				// Creating element ISEXCISEOVERRIDDEN inside
				// voucherElement
				Element ISEXCISEOVERRIDDENElement = doc.createElement("ISEXCISEOVERRIDDEN");

				// Setting value into it
				ISEXCISEOVERRIDDENElement.appendChild(doc.createTextNode("No"));

				// Appending this element to voucherElement
				voucherElement.appendChild(ISEXCISEOVERRIDDENElement);

				// Creating element ISCANCELLED inside
				// voucherElement
				Element ISCANCELLEDElement = doc.createElement("ISCANCELLED");

				// Setting value into it
				ISCANCELLEDElement.appendChild(doc.createTextNode("No"));

				// Appending this element to voucherElement
				voucherElement.appendChild(ISCANCELLEDElement);

				// Creating element HASCASHFLOW inside
				// voucherElement
				Element HASCASHFLOWElement = doc.createElement("HASCASHFLOW");

				// Setting value into it
				HASCASHFLOWElement.appendChild(doc.createTextNode("No"));

				// Appending this element to voucherElement
				voucherElement.appendChild(HASCASHFLOWElement);

				// Creating element ISPOSTDATED inside
				// voucherElement
				Element ISPOSTDATEDElement = doc.createElement("ISPOSTDATED");

				// Setting value into it
				ISPOSTDATEDElement.appendChild(doc.createTextNode("No"));

				// Appending this element to voucherElement
				voucherElement.appendChild(ISPOSTDATEDElement);

				// Creating element USETRACKINGNUMBER inside
				// voucherElement
				Element USETRACKINGNUMBERElement = doc.createElement("USETRACKINGNUMBER");

				// Setting value into it
				USETRACKINGNUMBERElement.appendChild(doc.createTextNode("No"));

				// Appending this element to voucherElement
				voucherElement.appendChild(USETRACKINGNUMBERElement);

				// Creating element ISINVOICE inside
				// voucherElement
				Element ISINVOICEElement = doc.createElement("ISINVOICE");

				// Setting value into it
				ISINVOICEElement.appendChild(doc.createTextNode("No"));

				// Appending this element to voucherElement
				voucherElement.appendChild(ISINVOICEElement);

				// Creating element MFGJOURNAL inside
				// voucherElement
				Element MFGJOURNALElement = doc.createElement("MFGJOURNAL");

				// Setting value into it
				MFGJOURNALElement.appendChild(doc.createTextNode("No"));

				// Appending this element to voucherElement
				voucherElement.appendChild(MFGJOURNALElement);

				// Creating element HASDISCOUNTS inside
				// voucherElement
				Element HASDISCOUNTSElement = doc.createElement("HASDISCOUNTS");

				// Setting value into it
				HASDISCOUNTSElement.appendChild(doc.createTextNode("No"));

				// Appending this element to voucherElement
				voucherElement.appendChild(HASDISCOUNTSElement);

				// Creating element ASPAYSLIP inside
				// voucherElement
				Element ASPAYSLIPElement = doc.createElement("ASPAYSLIP");

				// Setting value into it
				ASPAYSLIPElement.appendChild(doc.createTextNode("No"));

				// Appending this element to voucherElement
				voucherElement.appendChild(ASPAYSLIPElement);

				// Creating element ISCOSTCENTRE inside
				// voucherElement
				Element ISCOSTCENTREElement = doc.createElement("ISCOSTCENTRE");

				// Setting value into it
				ISCOSTCENTREElement.appendChild(doc.createTextNode("No"));

				// Appending this element to voucherElement
				voucherElement.appendChild(ISCOSTCENTREElement);

				// Creating element ISSTXNONREALIZEDVCH inside
				// voucherElement
				Element ISSTXNONREALIZEDVCHElement = doc.createElement("ISSTXNONREALIZEDVCH");

				// Setting value into it
				ISSTXNONREALIZEDVCHElement.appendChild(doc.createTextNode("No"));

				// Appending this element to voucherElement
				voucherElement.appendChild(ISSTXNONREALIZEDVCHElement);

				// Creating element ISBLANKCHEQUE inside
				// voucherElement
				Element ISBLANKCHEQUEElement = doc.createElement("ISBLANKCHEQUE");

				// Setting value into it
				ISBLANKCHEQUEElement.appendChild(doc.createTextNode("No"));

				// Appending this element to voucherElement
				voucherElement.appendChild(ISBLANKCHEQUEElement);

				// Creating element ISVOID inside
				// voucherElement
				Element ISVOIDElement = doc.createElement("ISVOID");

				// Setting value into it
				ISVOIDElement.appendChild(doc.createTextNode("No"));

				// Appending this element to voucherElement
				voucherElement.appendChild(ISVOIDElement);

				// Creating element ISONHOLD inside
				// tallyMessageElement
				Element ISONHOLDElement = doc.createElement("ISONHOLD");

				// Setting value into it
				ISONHOLDElement.appendChild(doc.createTextNode("No"));

				// Appending this element to voucherElement
				voucherElement.appendChild(ISONHOLDElement);

				// Creating element ORDERLINESTATUS inside
				// voucherElement
				Element ORDERLINESTATUSElement = doc.createElement("ORDERLINESTATUS");

				// Setting value into it
				ORDERLINESTATUSElement.appendChild(doc.createTextNode("No"));

				// Appending this element to voucherElement
				voucherElement.appendChild(ORDERLINESTATUSElement);

				// Creating element ISVATDUTYPAID inside
				// voucherElement
				Element ISVATDUTYPAIDElement = doc.createElement("ISVATDUTYPAID");

				// Setting value into it
				ISVATDUTYPAIDElement.appendChild(doc.createTextNode("Yes"));

				// Appending this element to voucherElement
				voucherElement.appendChild(ISVATDUTYPAIDElement);

				// Creating element ISDELETED inside
				// voucherElement
				Element ISDELETEDElement = doc.createElement("ISDELETED");

				// Setting value into it
				ISDELETEDElement.appendChild(doc.createTextNode("No"));

				// Appending this element to voucherElement
				voucherElement.appendChild(ISDELETEDElement);

				// Creating element EXCLUDEDTAXATIONSElement inside
				// voucherElement
				Element EXCLUDEDTAXATIONSElement = doc.createElement("EXCLUDEDTAXATIONS.LIST");

				// Setting value into it
				EXCLUDEDTAXATIONSElement.appendChild(doc.createTextNode(" "));

				// Appending this element to voucherElement
				voucherElement.appendChild(EXCLUDEDTAXATIONSElement);

				// Creating element OLDAUDITENTRIESElement inside
				// voucherElement
				Element OLDAUDITENTRIESElement = doc.createElement("OLDAUDITENTRIES.LIST");

				// Setting value into it
				OLDAUDITENTRIESElement.appendChild(doc.createTextNode(" "));

				// Appending this element to voucherElement
				voucherElement.appendChild(OLDAUDITENTRIESElement);

				// Creating element ACCOUNTAUDITENTRIESElement inside
				// voucherElement
				Element ACCOUNTAUDITENTRIESElement = doc.createElement("ACCOUNTAUDITENTRIES.LIST");

				// Setting value into it
				ACCOUNTAUDITENTRIESElement.appendChild(doc.createTextNode(" "));

				// Appending this element to voucherElement
				voucherElement.appendChild(ACCOUNTAUDITENTRIESElement);

				// Creating element AUDITENTRIESElement inside
				// voucherElement
				Element AUDITENTRIESElement = doc.createElement("AUDITENTRIES.LIST");

				// Setting value into it
				AUDITENTRIESElement.appendChild(doc.createTextNode(" "));

				// Appending this element to voucherElement
				voucherElement.appendChild(AUDITENTRIESElement);

				// Creating element DUTYHEADDETAILSElement inside
				// voucherElement
				Element DUTYHEADDETAILSElement = doc.createElement("DUTYHEADDETAILS.LIST");

				// Setting value into it
				DUTYHEADDETAILSElement.appendChild(doc.createTextNode(" "));

				// Appending this element to voucherElement
				voucherElement.appendChild(DUTYHEADDETAILSElement);

				// Creating element SUPPLEMENTARYDUTYHEADDETAILSElement inside
				// voucherElement
				Element SUPPLEMENTARYDUTYHEADDETAILSElement = doc.createElement("SUPPLEMENTARYDUTYHEADDETAILS.LIST");

				// Setting value into it
				SUPPLEMENTARYDUTYHEADDETAILSElement.appendChild(doc.createTextNode(" "));

				// Appending this element to voucherElement
				voucherElement.appendChild(SUPPLEMENTARYDUTYHEADDETAILSElement);

				// Creating element INVOICEDELNOTESElement inside
				// voucherElement
				Element INVOICEDELNOTESElement = doc.createElement("INVOICEDELNOTES.LIST");

				// Setting value into it
				INVOICEDELNOTESElement.appendChild(doc.createTextNode(" "));

				// Appending this element to voucherElement
				voucherElement.appendChild(INVOICEDELNOTESElement);

				// Creating element INVOICEORDERLISTElement inside
				// voucherElement
				Element INVOICEORDERLISTElement = doc.createElement("INVOICEORDERLIST.LIST");

				// Setting value into it
				INVOICEORDERLISTElement.appendChild(doc.createTextNode(" "));

				// Appending this element to voucherElement
				voucherElement.appendChild(INVOICEORDERLISTElement);

				// Creating element INVOICEINDENTLISTElement inside
				// voucherElement
				Element INVOICEINDENTLISTElement = doc.createElement("INVOICEINDENTLIST.LIST");

				// Setting value into it
				INVOICEINDENTLISTElement.appendChild(doc.createTextNode(" "));

				// Appending this element to voucherElement
				voucherElement.appendChild(INVOICEINDENTLISTElement);

				// Creating element ATTENDANCEENTRIESElement inside
				// voucherElement
				Element ATTENDANCEENTRIESElement = doc.createElement("ATTENDANCEENTRIES.LIST");

				// Setting value into it
				ATTENDANCEENTRIESElement.appendChild(doc.createTextNode(" "));

				// Appending this element to voucherElement
				voucherElement.appendChild(ATTENDANCEENTRIESElement);

				// Creating element ORIGINVOICEDETAILSElement inside
				// voucherElement
				Element ORIGINVOICEDETAILSElement = doc.createElement("ORIGINVOICEDETAILS.LIST");

				// Setting value into it
				ORIGINVOICEDETAILSElement.appendChild(doc.createTextNode(" "));

				// Appending this element to voucherElement
				voucherElement.appendChild(ORIGINVOICEDETAILSElement);

				// Creating element INVOICEEXPORTLISTElement inside
				// voucherElement
				Element INVOICEEXPORTLISTElement = doc.createElement("INVOICEEXPORTLIST.LIST");

				// Setting value into it
				INVOICEEXPORTLISTElement.appendChild(doc.createTextNode(" "));

				// Appending this element to voucherElement
				voucherElement.appendChild(INVOICEEXPORTLISTElement);

				/*
				 * Checking whether receiptType is other than Patient, if so,
				 * then setting amount to be credited into Amount tag else
				 * setting amount to be debited into Amount tag as
				 */
				if (receiptType.equals("BSC") || receiptType.equals("IBBT")) {

					/*
					 * Generating tags for Cash in hand and Outstanding cash
					 */
					// Creating element ALLLEDGERENTRIES.LIST inside
					// voucherElement
					Element cashInHandALLLEDGERENTRIESLISTElement = doc.createElement("ALLLEDGERENTRIES.LIST");

					// Appending this element to voucherElement
					voucherElement.appendChild(cashInHandALLLEDGERENTRIESLISTElement);

					// Creating element cashInhandLedgerNameElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element cashInhandLedgerNameElement = doc.createElement("LEDGERNAME");

					// Setting value into it
					cashInhandLedgerNameElement.appendChild(doc.createTextNode(storageCenter));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(cashInhandLedgerNameElement);

					// Creating element VOUCHERFBTCATEGORY inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element VOUCHERFBTCATEGORYElement = doc.createElement("VOUCHERFBTCATEGORY");

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(VOUCHERFBTCATEGORYElement);

					// Creating element GSTCLASS inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element GSTCLASSElement = doc.createElement("GSTCLASS");

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(GSTCLASSElement);

					// Creating element ISDEEMEDPOSITIVE inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element ISDEEMEDPOSITIVEElement = doc.createElement("ISDEEMEDPOSITIVE");

					// Setting value into it
					ISDEEMEDPOSITIVEElement.appendChild(doc.createTextNode("Yes"));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(ISDEEMEDPOSITIVEElement);

					// Creating element LEDGERFROMITEM inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element LEDGERFROMITEMElement = doc.createElement("LEDGERFROMITEM");

					// Setting value into it
					LEDGERFROMITEMElement.appendChild(doc.createTextNode("No"));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(LEDGERFROMITEMElement);

					// Creating element ISPARTYLEDGER inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element ISPARTYLEDGERElement = doc.createElement("ISPARTYLEDGER");

					// Setting value into it
					ISPARTYLEDGERElement.appendChild(doc.createTextNode("Yes"));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(ISPARTYLEDGERElement);

					// Creating element ISLASTDEEMEDPOSITIVE inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element ISLASTDEEMEDPOSITIVEElement = doc.createElement("ISLASTDEEMEDPOSITIVE");

					// Setting value into it
					ISLASTDEEMEDPOSITIVEElement.appendChild(doc.createTextNode("Yes"));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(ISLASTDEEMEDPOSITIVEElement);

					// Creating element AMOUNT inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element AMOUNTElement = doc.createElement("AMOUNT");

					// Setting value into it
					AMOUNTElement.appendChild(doc.createTextNode("-" + netAmount));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(AMOUNTElement);

					// Creating element VATEXPAMOUNTElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element VATEXPAMOUNTElement = doc.createElement("VATEXPAMOUNT");

					// Setting value into it
					VATEXPAMOUNTElement.appendChild(doc.createTextNode("-" + netAmount));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(VATEXPAMOUNTElement);

					// Creating element SERVICETAXDETAILSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element SERVICETAXDETAILSElement = doc.createElement("SERVICETAXDETAILS.LIST");

					// Setting value into it
					SERVICETAXDETAILSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(SERVICETAXDETAILSElement);

					// Creating element CATEGORYALLOCATIONSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element CATEGORYALLOCATIONSElement = doc.createElement("CATEGORYALLOCATIONS.LIST");

					// Setting value into it
					CATEGORYALLOCATIONSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(CATEGORYALLOCATIONSElement);

					// Creating element BANKALLOCATIONSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element BANKALLOCATIONSElement = doc.createElement("BANKALLOCATIONS.LIST");

					// Setting value into it
					BANKALLOCATIONSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(BANKALLOCATIONSElement);

					// Creating element BILLALLOCATIONSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element BILLALLOCATIONSElement = doc.createElement("BILLALLOCATIONS.LIST");

					// Setting value into it
					BILLALLOCATIONSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(BILLALLOCATIONSElement);

					// Creating element INTERESTCOLLECTIONElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element INTERESTCOLLECTIONElement = doc.createElement("INTERESTCOLLECTION.LIST");

					// Setting value into it
					INTERESTCOLLECTIONElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(INTERESTCOLLECTIONElement);

					// Creating element OLDAUDITENTRIESElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element OLDAUDITENTRIESElement1 = doc.createElement("OLDAUDITENTRIES.LIST");

					// Setting value into it
					OLDAUDITENTRIESElement1.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(OLDAUDITENTRIESElement1);

					// Creating element ACCOUNTAUDITENTRIESElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element ACCOUNTAUDITENTRIESElement1 = doc.createElement("ACCOUNTAUDITENTRIES.LIST");

					// Setting value into it
					ACCOUNTAUDITENTRIESElement1.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(ACCOUNTAUDITENTRIESElement1);

					// Creating element AUDITENTRIESElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element AUDITENTRIESElement1 = doc.createElement("AUDITENTRIES.LIST");

					// Setting value into it
					AUDITENTRIESElement1.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(AUDITENTRIESElement1);

					// Creating element INPUTCRALLOCSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element INPUTCRALLOCSElement = doc.createElement("INPUTCRALLOCS.LIST");

					// Setting value into it
					INPUTCRALLOCSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(INPUTCRALLOCSElement);

					// Creating element INVENTORYALLOCATIONSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element INVENTORYALLOCATIONSElement = doc.createElement("INVENTORYALLOCATIONS.LIST");

					// Setting value into it
					INVENTORYALLOCATIONSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(INVENTORYALLOCATIONSElement);

					// Creating element DUTYHEADDETAILSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element DUTYHEADDETAILSElement1 = doc.createElement("DUTYHEADDETAILS.LIST");

					// Setting value into it
					DUTYHEADDETAILSElement1.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(DUTYHEADDETAILSElement1);

					// Creating element EXCISEDUTYHEADDETAILSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element EXCISEDUTYHEADDETAILSElement = doc.createElement("EXCISEDUTYHEADDETAILS.LIST");

					// Setting value into it
					EXCISEDUTYHEADDETAILSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(EXCISEDUTYHEADDETAILSElement);

					// Creating element SUMMARYALLOCSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element SUMMARYALLOCSElement = doc.createElement("SUMMARYALLOCS.LIST");

					// Setting value into it
					SUMMARYALLOCSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(SUMMARYALLOCSElement);

					// Creating element STPYMTDETAILSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element STPYMTDETAILSElement = doc.createElement("STPYMTDETAILS.LIST");

					// Setting value into it
					STPYMTDETAILSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(STPYMTDETAILSElement);

					// Creating element EXCISEPAYMENTALLOCATIONSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element EXCISEPAYMENTALLOCATIONSElement = doc.createElement("EXCISEPAYMENTALLOCATIONS.LIST");

					// Setting value into it
					EXCISEPAYMENTALLOCATIONSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(EXCISEPAYMENTALLOCATIONSElement);

					// Creating element TAXBILLALLOCATIONSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element TAXBILLALLOCATIONSElement = doc.createElement("TAXBILLALLOCATIONS.LIST");

					// Setting value into it
					TAXBILLALLOCATIONSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(TAXBILLALLOCATIONSElement);

					// Creating element TAXOBJECTALLOCATIONSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element TAXOBJECTALLOCATIONSElement = doc.createElement("TAXOBJECTALLOCATIONS.LIST");

					// Setting value into it
					TAXOBJECTALLOCATIONSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(TAXOBJECTALLOCATIONSElement);

					// Creating element TDSEXPENSEALLOCATIONSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element TDSEXPENSEALLOCATIONSElement = doc.createElement("TDSEXPENSEALLOCATIONS.LIST");

					// Setting value into it
					TDSEXPENSEALLOCATIONSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(TDSEXPENSEALLOCATIONSElement);

					// Creating element VATSTATUTORYDETAILSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element VATSTATUTORYDETAILSElement = doc.createElement("VATSTATUTORYDETAILS.LIST");

					// Setting value into it
					VATSTATUTORYDETAILSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(VATSTATUTORYDETAILSElement);

					// Creating element REFVOUCHERDETAILSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element REFVOUCHERDETAILSElement = doc.createElement("REFVOUCHERDETAILS.LIST");

					// Setting value into it
					REFVOUCHERDETAILSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(REFVOUCHERDETAILSElement);

					// Creating element INVOICEWISEDETAILSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element INVOICEWISEDETAILSElement = doc.createElement("INVOICEWISEDETAILS.LIST");

					// Setting value into it
					INVOICEWISEDETAILSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(INVOICEWISEDETAILSElement);

					// Creating element VATITCDETAILSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element VATITCDETAILSElement = doc.createElement("VATITCDETAILS.LIST");

					// Setting value into it
					VATITCDETAILSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(VATITCDETAILSElement);

				} else if (receiptType.equals("SNT") || receiptType.equals("TPP") || receiptType.equals("TPB")) {

					/*
					 * Generating tags for Cash in hand and Outstanding cash
					 */
					// Creating element ALLLEDGERENTRIES.LIST inside
					// voucherElement
					Element cashInHandALLLEDGERENTRIESLISTElement = doc.createElement("ALLLEDGERENTRIES.LIST");

					// Appending this element to voucherElement
					voucherElement.appendChild(cashInHandALLLEDGERENTRIESLISTElement);

					// Creating element cashInhandLedgerNameElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element cashInhandLedgerNameElement = doc.createElement("LEDGERNAME");

					// Setting value into it
					cashInhandLedgerNameElement.appendChild(doc.createTextNode(hospitalName));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(cashInhandLedgerNameElement);

					// Creating element VOUCHERFBTCATEGORY inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element VOUCHERFBTCATEGORYElement = doc.createElement("VOUCHERFBTCATEGORY");

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(VOUCHERFBTCATEGORYElement);

					// Creating element GSTCLASS inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element GSTCLASSElement = doc.createElement("GSTCLASS");

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(GSTCLASSElement);

					// Creating element ISDEEMEDPOSITIVE inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element ISDEEMEDPOSITIVEElement = doc.createElement("ISDEEMEDPOSITIVE");

					// Setting value into it
					ISDEEMEDPOSITIVEElement.appendChild(doc.createTextNode("Yes"));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(ISDEEMEDPOSITIVEElement);

					// Creating element LEDGERFROMITEM inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element LEDGERFROMITEMElement = doc.createElement("LEDGERFROMITEM");

					// Setting value into it
					LEDGERFROMITEMElement.appendChild(doc.createTextNode("No"));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(LEDGERFROMITEMElement);

					// Creating element ISPARTYLEDGER inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element ISPARTYLEDGERElement = doc.createElement("ISPARTYLEDGER");

					// Setting value into it
					ISPARTYLEDGERElement.appendChild(doc.createTextNode("Yes"));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(ISPARTYLEDGERElement);

					// Creating element ISLASTDEEMEDPOSITIVE inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element ISLASTDEEMEDPOSITIVEElement = doc.createElement("ISLASTDEEMEDPOSITIVE");

					// Setting value into it
					ISLASTDEEMEDPOSITIVEElement.appendChild(doc.createTextNode("Yes"));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(ISLASTDEEMEDPOSITIVEElement);

					// Creating element AMOUNT inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element AMOUNTElement = doc.createElement("AMOUNT");

					// Setting value into it
					AMOUNTElement.appendChild(doc.createTextNode("-" + netAmount));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(AMOUNTElement);

					// Creating element VATEXPAMOUNTElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element VATEXPAMOUNTElement = doc.createElement("VATEXPAMOUNT");

					// Setting value into it
					VATEXPAMOUNTElement.appendChild(doc.createTextNode("-" + netAmount));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(VATEXPAMOUNTElement);

					// Creating element SERVICETAXDETAILSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element SERVICETAXDETAILSElement = doc.createElement("SERVICETAXDETAILS.LIST");

					// Setting value into it
					SERVICETAXDETAILSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(SERVICETAXDETAILSElement);

					// Creating element CATEGORYALLOCATIONSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element CATEGORYALLOCATIONSElement = doc.createElement("CATEGORYALLOCATIONS.LIST");

					// Setting value into it
					CATEGORYALLOCATIONSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(CATEGORYALLOCATIONSElement);

					// Creating element BANKALLOCATIONSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element BANKALLOCATIONSElement = doc.createElement("BANKALLOCATIONS.LIST");

					// Setting value into it
					BANKALLOCATIONSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(BANKALLOCATIONSElement);

					// Creating element BILLALLOCATIONSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element BILLALLOCATIONSElement = doc.createElement("BILLALLOCATIONS.LIST");

					// Setting value into it
					BILLALLOCATIONSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(BILLALLOCATIONSElement);

					// Creating element INTERESTCOLLECTIONElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element INTERESTCOLLECTIONElement = doc.createElement("INTERESTCOLLECTION.LIST");

					// Setting value into it
					INTERESTCOLLECTIONElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(INTERESTCOLLECTIONElement);

					// Creating element OLDAUDITENTRIESElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element OLDAUDITENTRIESElement1 = doc.createElement("OLDAUDITENTRIES.LIST");

					// Setting value into it
					OLDAUDITENTRIESElement1.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(OLDAUDITENTRIESElement1);

					// Creating element ACCOUNTAUDITENTRIESElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element ACCOUNTAUDITENTRIESElement1 = doc.createElement("ACCOUNTAUDITENTRIES.LIST");

					// Setting value into it
					ACCOUNTAUDITENTRIESElement1.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(ACCOUNTAUDITENTRIESElement1);

					// Creating element AUDITENTRIESElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element AUDITENTRIESElement1 = doc.createElement("AUDITENTRIES.LIST");

					// Setting value into it
					AUDITENTRIESElement1.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(AUDITENTRIESElement1);

					// Creating element INPUTCRALLOCSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element INPUTCRALLOCSElement = doc.createElement("INPUTCRALLOCS.LIST");

					// Setting value into it
					INPUTCRALLOCSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(INPUTCRALLOCSElement);

					// Creating element INVENTORYALLOCATIONSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element INVENTORYALLOCATIONSElement = doc.createElement("INVENTORYALLOCATIONS.LIST");

					// Setting value into it
					INVENTORYALLOCATIONSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(INVENTORYALLOCATIONSElement);

					// Creating element DUTYHEADDETAILSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element DUTYHEADDETAILSElement1 = doc.createElement("DUTYHEADDETAILS.LIST");

					// Setting value into it
					DUTYHEADDETAILSElement1.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(DUTYHEADDETAILSElement1);

					// Creating element EXCISEDUTYHEADDETAILSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element EXCISEDUTYHEADDETAILSElement = doc.createElement("EXCISEDUTYHEADDETAILS.LIST");

					// Setting value into it
					EXCISEDUTYHEADDETAILSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(EXCISEDUTYHEADDETAILSElement);

					// Creating element SUMMARYALLOCSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element SUMMARYALLOCSElement = doc.createElement("SUMMARYALLOCS.LIST");

					// Setting value into it
					SUMMARYALLOCSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(SUMMARYALLOCSElement);

					// Creating element STPYMTDETAILSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element STPYMTDETAILSElement = doc.createElement("STPYMTDETAILS.LIST");

					// Setting value into it
					STPYMTDETAILSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(STPYMTDETAILSElement);

					// Creating element EXCISEPAYMENTALLOCATIONSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element EXCISEPAYMENTALLOCATIONSElement = doc.createElement("EXCISEPAYMENTALLOCATIONS.LIST");

					// Setting value into it
					EXCISEPAYMENTALLOCATIONSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(EXCISEPAYMENTALLOCATIONSElement);

					// Creating element TAXBILLALLOCATIONSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element TAXBILLALLOCATIONSElement = doc.createElement("TAXBILLALLOCATIONS.LIST");

					// Setting value into it
					TAXBILLALLOCATIONSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(TAXBILLALLOCATIONSElement);

					// Creating element TAXOBJECTALLOCATIONSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element TAXOBJECTALLOCATIONSElement = doc.createElement("TAXOBJECTALLOCATIONS.LIST");

					// Setting value into it
					TAXOBJECTALLOCATIONSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(TAXOBJECTALLOCATIONSElement);

					// Creating element TDSEXPENSEALLOCATIONSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element TDSEXPENSEALLOCATIONSElement = doc.createElement("TDSEXPENSEALLOCATIONS.LIST");

					// Setting value into it
					TDSEXPENSEALLOCATIONSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(TDSEXPENSEALLOCATIONSElement);

					// Creating element VATSTATUTORYDETAILSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element VATSTATUTORYDETAILSElement = doc.createElement("VATSTATUTORYDETAILS.LIST");

					// Setting value into it
					VATSTATUTORYDETAILSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(VATSTATUTORYDETAILSElement);

					// Creating element REFVOUCHERDETAILSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element REFVOUCHERDETAILSElement = doc.createElement("REFVOUCHERDETAILS.LIST");

					// Setting value into it
					REFVOUCHERDETAILSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(REFVOUCHERDETAILSElement);

					// Creating element INVOICEWISEDETAILSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element INVOICEWISEDETAILSElement = doc.createElement("INVOICEWISEDETAILS.LIST");

					// Setting value into it
					INVOICEWISEDETAILSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(INVOICEWISEDETAILSElement);

					// Creating element VATITCDETAILSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element VATITCDETAILSElement = doc.createElement("VATITCDETAILS.LIST");

					// Setting value into it
					VATITCDETAILSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(VATITCDETAILSElement);

				} else {

					/*
					 * Generating tags for Cash in hand and Outstanding cash
					 */
					// Creating element ALLLEDGERENTRIES.LIST inside
					// voucherElement
					Element cashInHandALLLEDGERENTRIESLISTElement = doc.createElement("ALLLEDGERENTRIES.LIST");

					// Appending this element to voucherElement
					voucherElement.appendChild(cashInHandALLLEDGERENTRIESLISTElement);

					// Creating element cashInhandLedgerNameElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element cashInhandLedgerNameElement = doc.createElement("LEDGERNAME");

					// Setting value into it
					cashInhandLedgerNameElement.appendChild(doc.createTextNode("Cash In Hand"));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(cashInhandLedgerNameElement);

					// Creating element VOUCHERFBTCATEGORY inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element VOUCHERFBTCATEGORYElement = doc.createElement("VOUCHERFBTCATEGORY");

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(VOUCHERFBTCATEGORYElement);

					// Creating element GSTCLASS inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element GSTCLASSElement = doc.createElement("GSTCLASS");

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(GSTCLASSElement);

					// Creating element ISDEEMEDPOSITIVE inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element ISDEEMEDPOSITIVEElement = doc.createElement("ISDEEMEDPOSITIVE");

					// Setting value into it
					ISDEEMEDPOSITIVEElement.appendChild(doc.createTextNode("Yes"));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(ISDEEMEDPOSITIVEElement);

					// Creating element LEDGERFROMITEM inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element LEDGERFROMITEMElement = doc.createElement("LEDGERFROMITEM");

					// Setting value into it
					LEDGERFROMITEMElement.appendChild(doc.createTextNode("No"));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(LEDGERFROMITEMElement);

					// Creating element ISPARTYLEDGER inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element ISPARTYLEDGERElement = doc.createElement("ISPARTYLEDGER");

					// Setting value into it
					ISPARTYLEDGERElement.appendChild(doc.createTextNode("No"));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(ISPARTYLEDGERElement);

					// Creating element ISLASTDEEMEDPOSITIVE inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element ISLASTDEEMEDPOSITIVEElement = doc.createElement("ISLASTDEEMEDPOSITIVE");

					// Setting value into it
					ISLASTDEEMEDPOSITIVEElement.appendChild(doc.createTextNode("Yes"));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(ISLASTDEEMEDPOSITIVEElement);

					// Creating element AMOUNT inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element AMOUNTElement = doc.createElement("AMOUNT");

					// Setting value into it
					AMOUNTElement.appendChild(doc.createTextNode("-" + netAmount));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(AMOUNTElement);

					// Creating element VATEXPAMOUNTElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element VATEXPAMOUNTElement = doc.createElement("VATEXPAMOUNT");

					// Setting value into it
					VATEXPAMOUNTElement.appendChild(doc.createTextNode("-" + netAmount));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(VATEXPAMOUNTElement);

					// Creating element SERVICETAXDETAILSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element SERVICETAXDETAILSElement = doc.createElement("SERVICETAXDETAILS.LIST");

					// Setting value into it
					SERVICETAXDETAILSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(SERVICETAXDETAILSElement);

					// Creating element CATEGORYALLOCATIONSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element CATEGORYALLOCATIONSElement = doc.createElement("CATEGORYALLOCATIONS.LIST");

					// Setting value into it
					CATEGORYALLOCATIONSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(CATEGORYALLOCATIONSElement);

					// Creating element BANKALLOCATIONSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element BANKALLOCATIONSElement = doc.createElement("BANKALLOCATIONS.LIST");

					// Setting value into it
					BANKALLOCATIONSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(BANKALLOCATIONSElement);

					// Creating element BILLALLOCATIONSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element BILLALLOCATIONSElement = doc.createElement("BILLALLOCATIONS.LIST");

					// Setting value into it
					BILLALLOCATIONSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(BILLALLOCATIONSElement);

					// Creating element INTERESTCOLLECTIONElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element INTERESTCOLLECTIONElement = doc.createElement("INTERESTCOLLECTION.LIST");

					// Setting value into it
					INTERESTCOLLECTIONElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(INTERESTCOLLECTIONElement);

					// Creating element OLDAUDITENTRIESElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element OLDAUDITENTRIESElement1 = doc.createElement("OLDAUDITENTRIES.LIST");

					// Setting value into it
					OLDAUDITENTRIESElement1.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(OLDAUDITENTRIESElement1);

					// Creating element ACCOUNTAUDITENTRIESElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element ACCOUNTAUDITENTRIESElement1 = doc.createElement("ACCOUNTAUDITENTRIES.LIST");

					// Setting value into it
					ACCOUNTAUDITENTRIESElement1.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(ACCOUNTAUDITENTRIESElement1);

					// Creating element AUDITENTRIESElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element AUDITENTRIESElement1 = doc.createElement("AUDITENTRIES.LIST");

					// Setting value into it
					AUDITENTRIESElement1.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(AUDITENTRIESElement1);

					// Creating element INPUTCRALLOCSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element INPUTCRALLOCSElement = doc.createElement("INPUTCRALLOCS.LIST");

					// Setting value into it
					INPUTCRALLOCSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(INPUTCRALLOCSElement);

					// Creating element INVENTORYALLOCATIONSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element INVENTORYALLOCATIONSElement = doc.createElement("INVENTORYALLOCATIONS.LIST");

					// Setting value into it
					INVENTORYALLOCATIONSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(INVENTORYALLOCATIONSElement);

					// Creating element DUTYHEADDETAILSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element DUTYHEADDETAILSElement1 = doc.createElement("DUTYHEADDETAILS.LIST");

					// Setting value into it
					DUTYHEADDETAILSElement1.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(DUTYHEADDETAILSElement1);

					// Creating element EXCISEDUTYHEADDETAILSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element EXCISEDUTYHEADDETAILSElement = doc.createElement("EXCISEDUTYHEADDETAILS.LIST");

					// Setting value into it
					EXCISEDUTYHEADDETAILSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(EXCISEDUTYHEADDETAILSElement);

					// Creating element SUMMARYALLOCSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element SUMMARYALLOCSElement = doc.createElement("SUMMARYALLOCS.LIST");

					// Setting value into it
					SUMMARYALLOCSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(SUMMARYALLOCSElement);

					// Creating element STPYMTDETAILSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element STPYMTDETAILSElement = doc.createElement("STPYMTDETAILS.LIST");

					// Setting value into it
					STPYMTDETAILSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(STPYMTDETAILSElement);

					// Creating element EXCISEPAYMENTALLOCATIONSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element EXCISEPAYMENTALLOCATIONSElement = doc.createElement("EXCISEPAYMENTALLOCATIONS.LIST");

					// Setting value into it
					EXCISEPAYMENTALLOCATIONSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(EXCISEPAYMENTALLOCATIONSElement);

					// Creating element TAXBILLALLOCATIONSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element TAXBILLALLOCATIONSElement = doc.createElement("TAXBILLALLOCATIONS.LIST");

					// Setting value into it
					TAXBILLALLOCATIONSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(TAXBILLALLOCATIONSElement);

					// Creating element TAXOBJECTALLOCATIONSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element TAXOBJECTALLOCATIONSElement = doc.createElement("TAXOBJECTALLOCATIONS.LIST");

					// Setting value into it
					TAXOBJECTALLOCATIONSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(TAXOBJECTALLOCATIONSElement);

					// Creating element TDSEXPENSEALLOCATIONSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element TDSEXPENSEALLOCATIONSElement = doc.createElement("TDSEXPENSEALLOCATIONS.LIST");

					// Setting value into it
					TDSEXPENSEALLOCATIONSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(TDSEXPENSEALLOCATIONSElement);

					// Creating element VATSTATUTORYDETAILSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element VATSTATUTORYDETAILSElement = doc.createElement("VATSTATUTORYDETAILS.LIST");

					// Setting value into it
					VATSTATUTORYDETAILSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(VATSTATUTORYDETAILSElement);

					// Creating element REFVOUCHERDETAILSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element REFVOUCHERDETAILSElement = doc.createElement("REFVOUCHERDETAILS.LIST");

					// Setting value into it
					REFVOUCHERDETAILSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(REFVOUCHERDETAILSElement);

					// Creating element INVOICEWISEDETAILSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element INVOICEWISEDETAILSElement = doc.createElement("INVOICEWISEDETAILS.LIST");

					// Setting value into it
					INVOICEWISEDETAILSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(INVOICEWISEDETAILSElement);

					// Creating element VATITCDETAILSElement inside
					// cashInHandALLLEDGERENTRIESLISTElement
					Element VATITCDETAILSElement = doc.createElement("VATITCDETAILS.LIST");

					// Setting value into it
					VATITCDETAILSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// cashInHandALLLEDGERENTRIESLISTElement
					cashInHandALLLEDGERENTRIESLISTElement.appendChild(VATITCDETAILSElement);

				}

				/*
				 * Generating tags for Components
				 */
				if (componentList.size() > 0) {

					for (BillingForm billingForm : componentList) {

						// Creating element ALLLEDGERENTRIES.LIST for
						// outstanding
						// amount inside
						// voucherElement
						Element componentAmtALLLEDGERENTRIESLISTElement = doc.createElement("ALLLEDGERENTRIES.LIST");

						// Appending this element to tallyMessageElement
						voucherElement.appendChild(componentAmtALLLEDGERENTRIESLISTElement);

						// Creating element ledgerElement inside
						// outstandingAmtALLLEDGERENTRIESLISTElement
						Element ledgerElement = doc.createElement("LEDGERNAME");

						// Setting value into it
						ledgerElement.appendChild(doc.createTextNode(billingForm.getComponentName()));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(ledgerElement);

						// Creating element VOUCHERFBTCATEGORY inside
						// cashInHandALLLEDGERENTRIESLISTElement
						Element VOUCHERFBTCATEGORYElement1 = doc.createElement("VOUCHERFBTCATEGORY");

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(VOUCHERFBTCATEGORYElement1);

						// Creating element GSTCLASSElement inside
						// cashInHandALLLEDGERENTRIESLISTElement
						Element GSTCLASSElement1 = doc.createElement("GSTCLASS");

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(GSTCLASSElement1);

						// Creating element ISDEEMEDPOSITIVE inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element ISDEEMEDPOSITIVEElement1 = doc.createElement("ISDEEMEDPOSITIVE");

						// Setting value into it
						ISDEEMEDPOSITIVEElement1.appendChild(doc.createTextNode("No"));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(ISDEEMEDPOSITIVEElement1);

						// Creating element LEDGERFROMITEM inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element LEDGERFROMITEMElement1 = doc.createElement("LEDGERFROMITEM");

						// Setting value into it
						LEDGERFROMITEMElement1.appendChild(doc.createTextNode("No"));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(LEDGERFROMITEMElement1);

						// Creating element ISPARTYLEDGER inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element ISPARTYLEDGERElement1 = doc.createElement("ISPARTYLEDGER");

						// Setting value into it
						ISPARTYLEDGERElement1.appendChild(doc.createTextNode("No"));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(ISPARTYLEDGERElement1);

						// Creating element ISLASTDEEMEDPOSITIVE inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element ISLASTDEEMEDPOSITIVEElement1 = doc.createElement("ISLASTDEEMEDPOSITIVE");

						// Setting value into it
						ISLASTDEEMEDPOSITIVEElement1.appendChild(doc.createTextNode("No"));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(ISLASTDEEMEDPOSITIVEElement1);

						// Creating element AMOUNT inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element AMOUNTElement1 = doc.createElement("AMOUNT");

						// Setting value into it
						AMOUNTElement1.appendChild(doc.createTextNode("" + billingForm.getAmountName()));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(AMOUNTElement1);

						// Creating element VATEXPAMOUNTElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element VATEXPAMOUNTElement1 = doc.createElement("VATEXPAMOUNT");

						// Setting value into it
						VATEXPAMOUNTElement1.appendChild(doc.createTextNode("" + billingForm.getAmountName()));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(VATEXPAMOUNTElement1);

						// Creating element SERVICETAXDETAILSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element SERVICETAXDETAILSElement = doc.createElement("SERVICETAXDETAILS.LIST");

						// Setting value into it
						SERVICETAXDETAILSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(SERVICETAXDETAILSElement);

						// Creating element CATEGORYALLOCATIONSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element CATEGORYALLOCATIONSElement = doc.createElement("CATEGORYALLOCATIONS.LIST");

						// Setting value into it
						CATEGORYALLOCATIONSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(CATEGORYALLOCATIONSElement);

						// Creating element BANKALLOCATIONSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element BANKALLOCATIONSElement = doc.createElement("BANKALLOCATIONS.LIST");

						// Setting value into it
						BANKALLOCATIONSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(BANKALLOCATIONSElement);

						// Creating element BILLALLOCATIONSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element BILLALLOCATIONSElement = doc.createElement("BILLALLOCATIONS.LIST");

						// Setting value into it
						BILLALLOCATIONSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(BILLALLOCATIONSElement);

						// Creating element INTERESTCOLLECTIONElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element INTERESTCOLLECTIONElement = doc.createElement("INTERESTCOLLECTION.LIST");

						// Setting value into it
						INTERESTCOLLECTIONElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(INTERESTCOLLECTIONElement);

						// Creating element OLDAUDITENTRIESElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element OLDAUDITENTRIESElement1 = doc.createElement("OLDAUDITENTRIES.LIST");

						// Setting value into it
						OLDAUDITENTRIESElement1.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(OLDAUDITENTRIESElement1);

						// Creating element ACCOUNTAUDITENTRIESElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element ACCOUNTAUDITENTRIESElement1 = doc.createElement("ACCOUNTAUDITENTRIES.LIST");

						// Setting value into it
						ACCOUNTAUDITENTRIESElement1.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(ACCOUNTAUDITENTRIESElement1);

						// Creating element AUDITENTRIESElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element AUDITENTRIESElement1 = doc.createElement("AUDITENTRIES.LIST");

						// Setting value into it
						AUDITENTRIESElement1.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(AUDITENTRIESElement1);

						// Creating element INPUTCRALLOCSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element INPUTCRALLOCSElement = doc.createElement("INPUTCRALLOCS.LIST");

						// Setting value into it
						INPUTCRALLOCSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(INPUTCRALLOCSElement);

						// Creating element INVENTORYALLOCATIONSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element INVENTORYALLOCATIONSElement = doc.createElement("INVENTORYALLOCATIONS.LIST");

						// Setting value into it
						INVENTORYALLOCATIONSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(INVENTORYALLOCATIONSElement);

						// Creating element DUTYHEADDETAILSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element DUTYHEADDETAILSElement1 = doc.createElement("DUTYHEADDETAILS.LIST");

						// Setting value into it
						DUTYHEADDETAILSElement1.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(DUTYHEADDETAILSElement1);

						// Creating element EXCISEDUTYHEADDETAILSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element EXCISEDUTYHEADDETAILSElement = doc.createElement("EXCISEDUTYHEADDETAILS.LIST");

						// Setting value into it
						EXCISEDUTYHEADDETAILSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(EXCISEDUTYHEADDETAILSElement);

						// Creating element SUMMARYALLOCSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element SUMMARYALLOCSElement = doc.createElement("SUMMARYALLOCS.LIST");

						// Setting value into it
						SUMMARYALLOCSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(SUMMARYALLOCSElement);

						// Creating element STPYMTDETAILSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element STPYMTDETAILSElement = doc.createElement("STPYMTDETAILS.LIST");

						// Setting value into it
						STPYMTDETAILSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(STPYMTDETAILSElement);

						// Creating element EXCISEPAYMENTALLOCATIONSElement
						// inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element EXCISEPAYMENTALLOCATIONSElement = doc.createElement("EXCISEPAYMENTALLOCATIONS.LIST");

						// Setting value into it
						EXCISEPAYMENTALLOCATIONSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(EXCISEPAYMENTALLOCATIONSElement);

						// Creating element TAXBILLALLOCATIONSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element TAXBILLALLOCATIONSElement = doc.createElement("TAXBILLALLOCATIONS.LIST");

						// Setting value into it
						TAXBILLALLOCATIONSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(TAXBILLALLOCATIONSElement);

						// Creating element TAXOBJECTALLOCATIONSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element TAXOBJECTALLOCATIONSElement = doc.createElement("TAXOBJECTALLOCATIONS.LIST");

						// Setting value into it
						TAXOBJECTALLOCATIONSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(TAXOBJECTALLOCATIONSElement);

						// Creating element TDSEXPENSEALLOCATIONSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element TDSEXPENSEALLOCATIONSElement = doc.createElement("TDSEXPENSEALLOCATIONS.LIST");

						// Setting value into it
						TDSEXPENSEALLOCATIONSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(TDSEXPENSEALLOCATIONSElement);

						// Creating element VATSTATUTORYDETAILSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element VATSTATUTORYDETAILSElement = doc.createElement("VATSTATUTORYDETAILS.LIST");

						// Setting value into it
						VATSTATUTORYDETAILSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(VATSTATUTORYDETAILSElement);

						// Creating element REFVOUCHERDETAILSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element REFVOUCHERDETAILSElement = doc.createElement("REFVOUCHERDETAILS.LIST");

						// Setting value into it
						REFVOUCHERDETAILSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(REFVOUCHERDETAILSElement);

						// Creating element INVOICEWISEDETAILSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element INVOICEWISEDETAILSElement = doc.createElement("INVOICEWISEDETAILS.LIST");

						// Setting value into it
						INVOICEWISEDETAILSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(INVOICEWISEDETAILSElement);

						// Creating element VATITCDETAILSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element VATITCDETAILSElement = doc.createElement("VATITCDETAILS.LIST");

						// Setting value into it
						VATITCDETAILSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(VATITCDETAILSElement);

					}

				} else {
					System.out.println("No components list found.");
				}

				/*
				 * Generating tags for Other charges
				 */
				if (otherChargeList.size() > 0) {

					for (BillingForm billingForm : otherChargeList) {

						// Creating element ALLLEDGERENTRIES.LIST for
						// outstanding
						// amount inside
						// voucherElement
						Element componentAmtALLLEDGERENTRIESLISTElement = doc.createElement("ALLLEDGERENTRIES.LIST");

						// Appending this element to tallyMessageElement
						voucherElement.appendChild(componentAmtALLLEDGERENTRIESLISTElement);

						// Creating element ledgerElement inside
						// outstandingAmtALLLEDGERENTRIESLISTElement
						Element ledgerElement = doc.createElement("LEDGERNAME");

						// Setting value into it
						ledgerElement.appendChild(doc.createTextNode(billingForm.getChargeTypeName()));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(ledgerElement);

						// Creating element VOUCHERFBTCATEGORY inside
						// cashInHandALLLEDGERENTRIESLISTElement
						Element VOUCHERFBTCATEGORYElement1 = doc.createElement("VOUCHERFBTCATEGORY");

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(VOUCHERFBTCATEGORYElement1);

						// Creating element GSTCLASSElement inside
						// cashInHandALLLEDGERENTRIESLISTElement
						Element GSTCLASSElement1 = doc.createElement("GSTCLASS");

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(GSTCLASSElement1);

						// Creating element ISDEEMEDPOSITIVE inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element ISDEEMEDPOSITIVEElement1 = doc.createElement("ISDEEMEDPOSITIVE");

						// Setting value into it
						ISDEEMEDPOSITIVEElement1.appendChild(doc.createTextNode("No"));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(ISDEEMEDPOSITIVEElement1);

						// Creating element LEDGERFROMITEM inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element LEDGERFROMITEMElement1 = doc.createElement("LEDGERFROMITEM");

						// Setting value into it
						LEDGERFROMITEMElement1.appendChild(doc.createTextNode("No"));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(LEDGERFROMITEMElement1);

						// Creating element ISPARTYLEDGER inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element ISPARTYLEDGERElement1 = doc.createElement("ISPARTYLEDGER");

						// Setting value into it
						ISPARTYLEDGERElement1.appendChild(doc.createTextNode("No"));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(ISPARTYLEDGERElement1);

						// Creating element ISLASTDEEMEDPOSITIVE inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element ISLASTDEEMEDPOSITIVEElement1 = doc.createElement("ISLASTDEEMEDPOSITIVE");

						// Setting value into it
						ISLASTDEEMEDPOSITIVEElement1.appendChild(doc.createTextNode("No"));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(ISLASTDEEMEDPOSITIVEElement1);

						// Creating element AMOUNT inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element AMOUNTElement1 = doc.createElement("AMOUNT");

						// Setting value into it
						AMOUNTElement1.appendChild(doc.createTextNode("" + billingForm.getOtherChargeName()));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(AMOUNTElement1);

						// Creating element VATEXPAMOUNTElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element VATEXPAMOUNTElement1 = doc.createElement("VATEXPAMOUNT");

						// Setting value into it
						VATEXPAMOUNTElement1.appendChild(doc.createTextNode("" + billingForm.getOtherChargeName()));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(VATEXPAMOUNTElement1);

						// Creating element SERVICETAXDETAILSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element SERVICETAXDETAILSElement = doc.createElement("SERVICETAXDETAILS.LIST");

						// Setting value into it
						SERVICETAXDETAILSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(SERVICETAXDETAILSElement);

						// Creating element CATEGORYALLOCATIONSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element CATEGORYALLOCATIONSElement = doc.createElement("CATEGORYALLOCATIONS.LIST");

						// Setting value into it
						CATEGORYALLOCATIONSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(CATEGORYALLOCATIONSElement);

						// Creating element BANKALLOCATIONSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element BANKALLOCATIONSElement = doc.createElement("BANKALLOCATIONS.LIST");

						// Setting value into it
						BANKALLOCATIONSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(BANKALLOCATIONSElement);

						// Creating element BILLALLOCATIONSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element BILLALLOCATIONSElement = doc.createElement("BILLALLOCATIONS.LIST");

						// Setting value into it
						BILLALLOCATIONSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(BILLALLOCATIONSElement);

						// Creating element INTERESTCOLLECTIONElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element INTERESTCOLLECTIONElement = doc.createElement("INTERESTCOLLECTION.LIST");

						// Setting value into it
						INTERESTCOLLECTIONElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(INTERESTCOLLECTIONElement);

						// Creating element OLDAUDITENTRIESElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element OLDAUDITENTRIESElement1 = doc.createElement("OLDAUDITENTRIES.LIST");

						// Setting value into it
						OLDAUDITENTRIESElement1.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(OLDAUDITENTRIESElement1);

						// Creating element ACCOUNTAUDITENTRIESElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element ACCOUNTAUDITENTRIESElement1 = doc.createElement("ACCOUNTAUDITENTRIES.LIST");

						// Setting value into it
						ACCOUNTAUDITENTRIESElement1.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(ACCOUNTAUDITENTRIESElement1);

						// Creating element AUDITENTRIESElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element AUDITENTRIESElement1 = doc.createElement("AUDITENTRIES.LIST");

						// Setting value into it
						AUDITENTRIESElement1.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(AUDITENTRIESElement1);

						// Creating element INPUTCRALLOCSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element INPUTCRALLOCSElement = doc.createElement("INPUTCRALLOCS.LIST");

						// Setting value into it
						INPUTCRALLOCSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(INPUTCRALLOCSElement);

						// Creating element INVENTORYALLOCATIONSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element INVENTORYALLOCATIONSElement = doc.createElement("INVENTORYALLOCATIONS.LIST");

						// Setting value into it
						INVENTORYALLOCATIONSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(INVENTORYALLOCATIONSElement);

						// Creating element DUTYHEADDETAILSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element DUTYHEADDETAILSElement1 = doc.createElement("DUTYHEADDETAILS.LIST");

						// Setting value into it
						DUTYHEADDETAILSElement1.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(DUTYHEADDETAILSElement1);

						// Creating element EXCISEDUTYHEADDETAILSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element EXCISEDUTYHEADDETAILSElement = doc.createElement("EXCISEDUTYHEADDETAILS.LIST");

						// Setting value into it
						EXCISEDUTYHEADDETAILSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(EXCISEDUTYHEADDETAILSElement);

						// Creating element SUMMARYALLOCSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element SUMMARYALLOCSElement = doc.createElement("SUMMARYALLOCS.LIST");

						// Setting value into it
						SUMMARYALLOCSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(SUMMARYALLOCSElement);

						// Creating element STPYMTDETAILSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element STPYMTDETAILSElement = doc.createElement("STPYMTDETAILS.LIST");

						// Setting value into it
						STPYMTDETAILSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(STPYMTDETAILSElement);

						// Creating element EXCISEPAYMENTALLOCATIONSElement
						// inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element EXCISEPAYMENTALLOCATIONSElement = doc.createElement("EXCISEPAYMENTALLOCATIONS.LIST");

						// Setting value into it
						EXCISEPAYMENTALLOCATIONSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(EXCISEPAYMENTALLOCATIONSElement);

						// Creating element TAXBILLALLOCATIONSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element TAXBILLALLOCATIONSElement = doc.createElement("TAXBILLALLOCATIONS.LIST");

						// Setting value into it
						TAXBILLALLOCATIONSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(TAXBILLALLOCATIONSElement);

						// Creating element TAXOBJECTALLOCATIONSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element TAXOBJECTALLOCATIONSElement = doc.createElement("TAXOBJECTALLOCATIONS.LIST");

						// Setting value into it
						TAXOBJECTALLOCATIONSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(TAXOBJECTALLOCATIONSElement);

						// Creating element TDSEXPENSEALLOCATIONSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element TDSEXPENSEALLOCATIONSElement = doc.createElement("TDSEXPENSEALLOCATIONS.LIST");

						// Setting value into it
						TDSEXPENSEALLOCATIONSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(TDSEXPENSEALLOCATIONSElement);

						// Creating element VATSTATUTORYDETAILSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element VATSTATUTORYDETAILSElement = doc.createElement("VATSTATUTORYDETAILS.LIST");

						// Setting value into it
						VATSTATUTORYDETAILSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(VATSTATUTORYDETAILSElement);

						// Creating element REFVOUCHERDETAILSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element REFVOUCHERDETAILSElement = doc.createElement("REFVOUCHERDETAILS.LIST");

						// Setting value into it
						REFVOUCHERDETAILSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(REFVOUCHERDETAILSElement);

						// Creating element INVOICEWISEDETAILSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element INVOICEWISEDETAILSElement = doc.createElement("INVOICEWISEDETAILS.LIST");

						// Setting value into it
						INVOICEWISEDETAILSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(INVOICEWISEDETAILSElement);

						// Creating element VATITCDETAILSElement inside
						// componentAmtALLLEDGERENTRIESLISTElement
						Element VATITCDETAILSElement = doc.createElement("VATITCDETAILS.LIST");

						// Setting value into it
						VATITCDETAILSElement.appendChild(doc.createTextNode(" "));

						// Appending this element to
						// componentAmtALLLEDGERENTRIESLISTElement
						componentAmtALLLEDGERENTRIESLISTElement.appendChild(VATITCDETAILSElement);

					}

				} else {
					System.out.println("No other charges list found.");
				}

				/*
				 * Generating tags for Concession
				 */
				// Check whether concession is null or "" or "000", is not, then
				// creating tag for concession
				if (concessionType.equals("000")) {
					System.out.println("No concession found");
				} else {

					// Creating element ALLLEDGERENTRIES.LIST for
					// outstanding
					// amount inside
					// voucherElement
					Element concessionALLLEDGERENTRIESLISTElement = doc.createElement("ALLLEDGERENTRIES.LIST");

					// Appending this element to tallyMessageElement
					voucherElement.appendChild(concessionALLLEDGERENTRIESLISTElement);

					// Creating element ledgerElement inside
					// concessionALLLEDGERENTRIESLISTElement
					Element ledgerElement = doc.createElement("LEDGERNAME");

					// Setting value into it
					ledgerElement.appendChild(doc.createTextNode(concessionType));

					// Appending this element to
					// concessionALLLEDGERENTRIESLISTElement
					concessionALLLEDGERENTRIESLISTElement.appendChild(ledgerElement);

					// Creating element VOUCHERFBTCATEGORY inside
					// concessionALLLEDGERENTRIESLISTElement
					Element VOUCHERFBTCATEGORYElement1 = doc.createElement("VOUCHERFBTCATEGORY");

					// Appending this element to
					// concessionALLLEDGERENTRIESLISTElement
					concessionALLLEDGERENTRIESLISTElement.appendChild(VOUCHERFBTCATEGORYElement1);

					// Creating element GSTCLASSElement inside
					// concessionALLLEDGERENTRIESLISTElement
					Element GSTCLASSElement1 = doc.createElement("GSTCLASS");

					// Appending this element to
					// concessionALLLEDGERENTRIESLISTElement
					concessionALLLEDGERENTRIESLISTElement.appendChild(GSTCLASSElement1);

					// Creating element ISDEEMEDPOSITIVE inside
					// concessionALLLEDGERENTRIESLISTElement
					Element ISDEEMEDPOSITIVEElement1 = doc.createElement("ISDEEMEDPOSITIVE");

					// Setting value into it
					ISDEEMEDPOSITIVEElement1.appendChild(doc.createTextNode("Yes"));

					// Appending this element to
					// concessionALLLEDGERENTRIESLISTElement
					concessionALLLEDGERENTRIESLISTElement.appendChild(ISDEEMEDPOSITIVEElement1);

					// Creating element LEDGERFROMITEM inside
					// concessionALLLEDGERENTRIESLISTElement
					Element LEDGERFROMITEMElement1 = doc.createElement("LEDGERFROMITEM");

					// Setting value into it
					LEDGERFROMITEMElement1.appendChild(doc.createTextNode("No"));

					// Appending this element to
					// concessionALLLEDGERENTRIESLISTElement
					concessionALLLEDGERENTRIESLISTElement.appendChild(LEDGERFROMITEMElement1);

					// Creating element ISPARTYLEDGER inside
					// concessionALLLEDGERENTRIESLISTElement
					Element ISPARTYLEDGERElement1 = doc.createElement("ISPARTYLEDGER");

					// Setting value into it
					ISPARTYLEDGERElement1.appendChild(doc.createTextNode("No"));

					// Appending this element to
					// concessionALLLEDGERENTRIESLISTElement
					concessionALLLEDGERENTRIESLISTElement.appendChild(ISPARTYLEDGERElement1);

					// Creating element ISLASTDEEMEDPOSITIVE inside
					// concessionALLLEDGERENTRIESLISTElement
					Element ISLASTDEEMEDPOSITIVEElement1 = doc.createElement("ISLASTDEEMEDPOSITIVE");

					// Setting value into it
					ISLASTDEEMEDPOSITIVEElement1.appendChild(doc.createTextNode("Yes"));

					// Appending this element to
					// concessionALLLEDGERENTRIESLISTElement
					concessionALLLEDGERENTRIESLISTElement.appendChild(ISLASTDEEMEDPOSITIVEElement1);

					// Creating element AMOUNT inside
					// concessionALLLEDGERENTRIESLISTElement
					Element AMOUNTElement1 = doc.createElement("AMOUNT");

					// Setting value into it
					AMOUNTElement1.appendChild(doc.createTextNode("-" + concessionAmount));

					// Appending this element to
					// concessionALLLEDGERENTRIESLISTElement
					concessionALLLEDGERENTRIESLISTElement.appendChild(AMOUNTElement1);

					// Creating element VATEXPAMOUNTElement inside
					// concessionALLLEDGERENTRIESLISTElement
					Element VATEXPAMOUNTElement1 = doc.createElement("VATEXPAMOUNT");

					// Setting value into it
					VATEXPAMOUNTElement1.appendChild(doc.createTextNode("-" + concessionAmount));

					// Appending this element to
					// concessionALLLEDGERENTRIESLISTElement
					concessionALLLEDGERENTRIESLISTElement.appendChild(VATEXPAMOUNTElement1);

					// Creating element SERVICETAXDETAILSElement inside
					// concessionALLLEDGERENTRIESLISTElement
					Element SERVICETAXDETAILSElement = doc.createElement("SERVICETAXDETAILS.LIST");

					// Setting value into it
					SERVICETAXDETAILSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// concessionALLLEDGERENTRIESLISTElement
					concessionALLLEDGERENTRIESLISTElement.appendChild(SERVICETAXDETAILSElement);

					// Creating element CATEGORYALLOCATIONSElement inside
					// concessionALLLEDGERENTRIESLISTElement
					Element CATEGORYALLOCATIONSElement = doc.createElement("CATEGORYALLOCATIONS.LIST");

					// Setting value into it
					CATEGORYALLOCATIONSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// concessionALLLEDGERENTRIESLISTElement
					concessionALLLEDGERENTRIESLISTElement.appendChild(CATEGORYALLOCATIONSElement);

					// Creating element BANKALLOCATIONSElement inside
					// concessionALLLEDGERENTRIESLISTElement
					Element BANKALLOCATIONSElement = doc.createElement("BANKALLOCATIONS.LIST");

					// Setting value into it
					BANKALLOCATIONSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// concessionALLLEDGERENTRIESLISTElement
					concessionALLLEDGERENTRIESLISTElement.appendChild(BANKALLOCATIONSElement);

					// Creating element BILLALLOCATIONSElement inside
					// concessionALLLEDGERENTRIESLISTElement
					Element BILLALLOCATIONSElement = doc.createElement("BILLALLOCATIONS.LIST");

					// Setting value into it
					BILLALLOCATIONSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// concessionALLLEDGERENTRIESLISTElement
					concessionALLLEDGERENTRIESLISTElement.appendChild(BILLALLOCATIONSElement);

					// Creating element INTERESTCOLLECTIONElement inside
					// concessionALLLEDGERENTRIESLISTElement
					Element INTERESTCOLLECTIONElement = doc.createElement("INTERESTCOLLECTION.LIST");

					// Setting value into it
					INTERESTCOLLECTIONElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// concessionALLLEDGERENTRIESLISTElement
					concessionALLLEDGERENTRIESLISTElement.appendChild(INTERESTCOLLECTIONElement);

					// Creating element OLDAUDITENTRIESElement inside
					// concessionALLLEDGERENTRIESLISTElement
					Element OLDAUDITENTRIESElement1 = doc.createElement("OLDAUDITENTRIES.LIST");

					// Setting value into it
					OLDAUDITENTRIESElement1.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// concessionALLLEDGERENTRIESLISTElement
					concessionALLLEDGERENTRIESLISTElement.appendChild(OLDAUDITENTRIESElement1);

					// Creating element ACCOUNTAUDITENTRIESElement inside
					// concessionALLLEDGERENTRIESLISTElement
					Element ACCOUNTAUDITENTRIESElement1 = doc.createElement("ACCOUNTAUDITENTRIES.LIST");

					// Setting value into it
					ACCOUNTAUDITENTRIESElement1.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// concessionALLLEDGERENTRIESLISTElement
					concessionALLLEDGERENTRIESLISTElement.appendChild(ACCOUNTAUDITENTRIESElement1);

					// Creating element AUDITENTRIESElement inside
					// concessionALLLEDGERENTRIESLISTElement
					Element AUDITENTRIESElement1 = doc.createElement("AUDITENTRIES.LIST");

					// Setting value into it
					AUDITENTRIESElement1.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// concessionALLLEDGERENTRIESLISTElement
					concessionALLLEDGERENTRIESLISTElement.appendChild(AUDITENTRIESElement1);

					// Creating element INPUTCRALLOCSElement inside
					// concessionALLLEDGERENTRIESLISTElement
					Element INPUTCRALLOCSElement = doc.createElement("INPUTCRALLOCS.LIST");

					// Setting value into it
					INPUTCRALLOCSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// concessionALLLEDGERENTRIESLISTElement
					concessionALLLEDGERENTRIESLISTElement.appendChild(INPUTCRALLOCSElement);

					// Creating element INVENTORYALLOCATIONSElement inside
					// concessionALLLEDGERENTRIESLISTElement
					Element INVENTORYALLOCATIONSElement = doc.createElement("INVENTORYALLOCATIONS.LIST");

					// Setting value into it
					INVENTORYALLOCATIONSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// concessionALLLEDGERENTRIESLISTElement
					concessionALLLEDGERENTRIESLISTElement.appendChild(INVENTORYALLOCATIONSElement);

					// Creating element DUTYHEADDETAILSElement inside
					// concessionALLLEDGERENTRIESLISTElement
					Element DUTYHEADDETAILSElement1 = doc.createElement("DUTYHEADDETAILS.LIST");

					// Setting value into it
					DUTYHEADDETAILSElement1.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// concessionALLLEDGERENTRIESLISTElement
					concessionALLLEDGERENTRIESLISTElement.appendChild(DUTYHEADDETAILSElement1);

					// Creating element EXCISEDUTYHEADDETAILSElement inside
					// concessionALLLEDGERENTRIESLISTElement
					Element EXCISEDUTYHEADDETAILSElement = doc.createElement("EXCISEDUTYHEADDETAILS.LIST");

					// Setting value into it
					EXCISEDUTYHEADDETAILSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// concessionALLLEDGERENTRIESLISTElement
					concessionALLLEDGERENTRIESLISTElement.appendChild(EXCISEDUTYHEADDETAILSElement);

					// Creating element SUMMARYALLOCSElement inside
					// concessionALLLEDGERENTRIESLISTElement
					Element SUMMARYALLOCSElement = doc.createElement("SUMMARYALLOCS.LIST");

					// Setting value into it
					SUMMARYALLOCSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// concessionALLLEDGERENTRIESLISTElement
					concessionALLLEDGERENTRIESLISTElement.appendChild(SUMMARYALLOCSElement);

					// Creating element STPYMTDETAILSElement inside
					// concessionALLLEDGERENTRIESLISTElement
					Element STPYMTDETAILSElement = doc.createElement("STPYMTDETAILS.LIST");

					// Setting value into it
					STPYMTDETAILSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// concessionALLLEDGERENTRIESLISTElement
					concessionALLLEDGERENTRIESLISTElement.appendChild(STPYMTDETAILSElement);

					// Creating element EXCISEPAYMENTALLOCATIONSElement
					// inside
					// concessionALLLEDGERENTRIESLISTElement
					Element EXCISEPAYMENTALLOCATIONSElement = doc.createElement("EXCISEPAYMENTALLOCATIONS.LIST");

					// Setting value into it
					EXCISEPAYMENTALLOCATIONSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// concessionALLLEDGERENTRIESLISTElement
					concessionALLLEDGERENTRIESLISTElement.appendChild(EXCISEPAYMENTALLOCATIONSElement);

					// Creating element TAXBILLALLOCATIONSElement inside
					// concessionALLLEDGERENTRIESLISTElement
					Element TAXBILLALLOCATIONSElement = doc.createElement("TAXBILLALLOCATIONS.LIST");

					// Setting value into it
					TAXBILLALLOCATIONSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// concessionALLLEDGERENTRIESLISTElement
					concessionALLLEDGERENTRIESLISTElement.appendChild(TAXBILLALLOCATIONSElement);

					// Creating element TAXOBJECTALLOCATIONSElement inside
					// concessionALLLEDGERENTRIESLISTElement
					Element TAXOBJECTALLOCATIONSElement = doc.createElement("TAXOBJECTALLOCATIONS.LIST");

					// Setting value into it
					TAXOBJECTALLOCATIONSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// concessionALLLEDGERENTRIESLISTElement
					concessionALLLEDGERENTRIESLISTElement.appendChild(TAXOBJECTALLOCATIONSElement);

					// Creating element TDSEXPENSEALLOCATIONSElement inside
					// concessionALLLEDGERENTRIESLISTElement
					Element TDSEXPENSEALLOCATIONSElement = doc.createElement("TDSEXPENSEALLOCATIONS.LIST");

					// Setting value into it
					TDSEXPENSEALLOCATIONSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// concessionALLLEDGERENTRIESLISTElement
					concessionALLLEDGERENTRIESLISTElement.appendChild(TDSEXPENSEALLOCATIONSElement);

					// Creating element VATSTATUTORYDETAILSElement inside
					// concessionALLLEDGERENTRIESLISTElement
					Element VATSTATUTORYDETAILSElement = doc.createElement("VATSTATUTORYDETAILS.LIST");

					// Setting value into it
					VATSTATUTORYDETAILSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// concessionALLLEDGERENTRIESLISTElement
					concessionALLLEDGERENTRIESLISTElement.appendChild(VATSTATUTORYDETAILSElement);

					// Creating element REFVOUCHERDETAILSElement inside
					// concessionALLLEDGERENTRIESLISTElement
					Element REFVOUCHERDETAILSElement = doc.createElement("REFVOUCHERDETAILS.LIST");

					// Setting value into it
					REFVOUCHERDETAILSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// concessionALLLEDGERENTRIESLISTElement
					concessionALLLEDGERENTRIESLISTElement.appendChild(REFVOUCHERDETAILSElement);

					// Creating element INVOICEWISEDETAILSElement inside
					// concessionALLLEDGERENTRIESLISTElement
					Element INVOICEWISEDETAILSElement = doc.createElement("INVOICEWISEDETAILS.LIST");

					// Setting value into it
					INVOICEWISEDETAILSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// concessionALLLEDGERENTRIESLISTElement
					concessionALLLEDGERENTRIESLISTElement.appendChild(INVOICEWISEDETAILSElement);

					// Creating element VATITCDETAILSElement inside
					// concessionALLLEDGERENTRIESLISTElement
					Element VATITCDETAILSElement = doc.createElement("VATITCDETAILS.LIST");

					// Setting value into it
					VATITCDETAILSElement.appendChild(doc.createTextNode(" "));

					// Appending this element to
					// concessionALLLEDGERENTRIESLISTElement
					concessionALLLEDGERENTRIESLISTElement.appendChild(VATITCDETAILSElement);

				}

				for (int i = 0; i < 2; i++) {
					// Creating tallyMessage element inside requestDataElement
					Element newTallyMessageElement = doc.createElement("TALLYMESSAGE");

					// Appending tallyMessageElement to the requestDateElement
					requestDataElement.appendChild(newTallyMessageElement);

					// Creating attribute inside tallyElement
					Attr newTallyMessageAttr = doc.createAttribute("xmlns:UDF");

					// Setting value to created attribute
					newTallyMessageAttr.setValue("TallyUDF");

					// Setting this attribute to newTallyMessageElement
					newTallyMessageElement.setAttributeNode(newTallyMessageAttr);

					// Creating company element inside newTallyMessageElement
					Element companyElement = doc.createElement("COMPANY");

					// Appending companyElement to the
					// newTallyMessageElement
					newTallyMessageElement.appendChild(companyElement);

					// Creating REMOTECMPINFOLIST element inside
					// requestDataElement
					Element REMOTECMPINFOLISTElement = doc.createElement("REMOTECMPINFO.LIST");

					// Appending REMOTECMPINFOLISTElement to the
					// companyElement
					companyElement.appendChild(REMOTECMPINFOLISTElement);

					// Creating attribute inside tallyElement
					Attr Attr = doc.createAttribute("MERGE");

					// Setting value to created attribute
					Attr.setValue("Yes");

					// Setting this attribute to REMOTECMPINFOLISTElement
					REMOTECMPINFOLISTElement.setAttributeNode(Attr);

					// Creating name element inside REMOTECMPINFOLISTElement
					Element nameElement = doc.createElement("NAME");

					// Adding text to nameElement
					nameElement.appendChild(doc.createTextNode("a4677d69-8a1e-438a-ba75-2e96ca6ea1fe"));

					// Appending nameElement to the
					// REMOTECMPINFOLISTElement
					REMOTECMPINFOLISTElement.appendChild(nameElement);

					// Creating remoteCompnayName element inside
					// REMOTECMPINFOLISTElement
					Element remoteComNameElement = doc.createElement("REMOTECMPNAME");

					// Adding text to remoteComNameElement
					remoteComNameElement
							.appendChild(doc.createTextNode("Janakalyan Rakta Pedhi,Pune - (from 1-Apr-2016)"));

					// Appending remoteComNameElement to the
					// REMOTECMPINFOLISTElement
					REMOTECMPINFOLISTElement.appendChild(remoteComNameElement);

					// Creating remoteCompnayName element inside
					// REMOTECMPINFOLISTElement
					Element remoteComStateElement = doc.createElement("REMOTECMPSTATE");

					// Adding text to remoteComStateElement
					remoteComStateElement.appendChild(doc.createTextNode("Maharashtra"));

					// Appending remoteComStateElement to the
					// REMOTECMPINFOLISTElement
					REMOTECMPINFOLISTElement.appendChild(remoteComStateElement);

				}

				// Write the entire contents into XML file
				TransformerFactory transformerFactory = TransformerFactory.newInstance();

				Transformer transformer = transformerFactory.newTransformer();

				DOMSource source = new DOMSource(doc);
				
				/*
				 * Getting file path from XML in order to store tally XML files
				 */
				String tallyFilePath = configXMLUtil.getTallyFilePath();
				
				System.out.println("Tally XML File path is ..." +tallyFilePath);

				String XMLFileName = tallyFilePath + "receipt" + counter + "_" + dateTimeFormat.format(new Date()) + ".xml";

				File xmlFile = new File(XMLFileName);

				StreamResult result = new StreamResult(new File(XMLFileName));

				transformer.transform(source, result);

				System.out.println("XML Writtent successfully.");

				/*
				 * Checking whether sendCheck is Yes or No, if Yes then calling
				 * sendToTally method of CURLConfigUtil in order to import XML
				 * into tally else binding XMLs one by one into zip folder
				 */
				if (sendCheck.equals("Yes")) {

					status = curlConfigUtil.sendToTally(XMLFileName);

					if (!status.equals("success")) {

						zos.close();

						return status;
					}

					counter++;

				} else {

					zos.putNextEntry(new ZipEntry(xmlFile.getName()));

					FileInputStream fis = null;
					try {
						fis = new FileInputStream(xmlFile);

					} catch (FileNotFoundException fnfe) {
						// If the file does not exists, write an error entry
						// instead
						// of
						// file
						// contents
						zos.write(("ERRORld not find file " + xmlFile.getName()).getBytes());
						zos.closeEntry();
						System.out.println("Couldfind file " + xmlFile.getAbsolutePath());
						continue;
					}

					BufferedInputStream fif = new BufferedInputStream(fis);

					// Write the contents of the file
					int data = 0;
					while ((data = fif.read()) != -1) {
						zos.write(data);
					}
					fif.close();

					zos.closeEntry();
					System.out.println("Finish sending file " + xmlFile.getName());

					status = "success";

					counter++;

				}

			}

			zos.close();

			resultSet1.close();
			preparedStatement1.close();

			return status;

		} catch (Exception exception) {
			exception.printStackTrace();
			status = "error";

			return status;
		}
	}

}
