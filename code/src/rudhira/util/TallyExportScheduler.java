package rudhira.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import rudhira.util.SchedulerListnerUtil.MyRunnable;

public class TallyExportScheduler implements Job {

	private static String contextPath = "";

	private static final int MYTHREADS = 60;

	public void execute(JobExecutionContext context) throws JobExecutionException {

		JobDataMap dataMap = context.getJobDetail().getJobDataMap();

		contextPath = dataMap.getString("contextPath");

		ExecutorService executor = Executors.newFixedThreadPool(MYTHREADS);

		System.out.println("Tally excel export scheduler started");

		String[] hostList = { generateTallyExcel(contextPath) };

		for (int i = 0; i < hostList.length; i++) {

			String url = hostList[i];
			Runnable worker = new MyRunnable(url);
			executor.execute(worker);
		}

		executor.shutdown();
		// Wait until all threads are finish
		while (!executor.isTerminated()) {

		}

	}

	public static String generateTallyExcel(String contextPath) {
		ExcelUtil excelUtil = new ExcelUtil();

		ConfigListenerUtil configListenerUtil = new ConfigListenerUtil();

		String tallyFilePath = configListenerUtil.getTallyFilePath(contextPath);

		final Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

		String yesterdayDate = dateFormat.format(cal.getTime());

		String excelFileName = tallyFilePath + "TallyReceipt_" + yesterdayDate + ".xls";

		System.out.println("File name...." + excelFileName);

		String status = excelUtil.generateExportForTallyAccountsReport(contextPath, excelFileName);

		if (status.equals("success")) {
			System.out.println("Tally excel for the date: " + yesterdayDate + " is created successfully.");
		} else if (status.equals("success")) {
			System.out.println("No receipt records found for the date: " + yesterdayDate + ".");
		} else {
			System.out.println("Failed to create Tally excel for the date: " + yesterdayDate + ".");
		}

		return status;

	}

}
