package rudhira.util;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ConfigListenerUtil {

	/**
	 * 
	 * @param filePath
	 * @return
	 */
	public String getDBIP(String realPath) {

		String DBIP = null;

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(new File(realPath + File.separator + "configuration.xml"));

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("rudhira-config");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					DBIP = eElement.getElementsByTagName("DB-IP").item(0).getTextContent();
				}
			}

		} catch (Exception exception) {

			exception.printStackTrace();
		}
		return DBIP;
	}

	/**
	 * 
	 * @param filePath
	 * @return
	 */
	public String getDBPort(String realPath) {
		String DBPort = null;

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(new File(realPath + File.separator + "configuration.xml"));

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("rudhira-config");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					DBPort = eElement.getElementsByTagName("DB-Port").item(0).getTextContent();
				}
			}

		} catch (Exception exception) {

			exception.printStackTrace();
		}
		return DBPort;
	}

	/**
	 * 
	 * @param filePath
	 * @return
	 */
	public String getDBName(String realPath) {
		String DBName = null;

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(new File(realPath + File.separator + "configuration.xml"));

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("rudhira-config");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					DBName = eElement.getElementsByTagName("DB-Name").item(0).getTextContent();
				}
			}

		} catch (Exception exception) {

			exception.printStackTrace();
		}
		return DBName;
	}

	/**
	 * 
	 * @param filePath
	 * @return
	 */
	public String getDBUsername(String realPath) {
		String DBUsername = null;

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(new File(realPath + File.separator + "configuration.xml"));

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("rudhira-config");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					DBUsername = eElement.getElementsByTagName("DB-Username").item(0).getTextContent();
				}
			}

		} catch (Exception exception) {

			exception.printStackTrace();
		}
		return DBUsername;
	}

	/**
	 * 
	 * @param filePath
	 * @return
	 */
	public String getDBPassword(String realPath) {
		String DBPassword = null;

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(new File(realPath + File.separator + "configuration.xml"));

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("rudhira-config");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					DBPassword = eElement.getElementsByTagName("DB-Password").item(0).getTextContent();
				}
			}

		} catch (Exception exception) {

			exception.printStackTrace();
		}
		return DBPassword;
	}

	/**
	 * 
	 * @return
	 */
	public String getJankalyanDBIP(String realPath) {
		String DBIP = null;

		File xmlFile = new File(realPath + File.separator + "configuration.xml");

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("rudhira-config");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					DBIP = eElement.getElementsByTagName("Jankalyan-DB-IP").item(0).getTextContent();
				}
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return DBIP;
	}

	/**
	 * 
	 * @return
	 */
	public String getJankalyanDBPort(String realPath) {

		String DBPort = null;

		File xmlFile = new File(realPath + File.separator + "configuration.xml");

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("rudhira-config");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					DBPort = eElement.getElementsByTagName("Jankalyan-DB-Port").item(0).getTextContent();
				}
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return DBPort;

	}

	/**
	 * 
	 * @return
	 */
	public String getJankalyanDBUser(String realPath) {

		String DBUser = null;

		File xmlFile = new File(realPath + File.separator + "configuration.xml");

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("rudhira-config");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					DBUser = eElement.getElementsByTagName("Jankalyan-DB-Username").item(0).getTextContent();
				}
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return DBUser;

	}

	/**
	 * 
	 * @return
	 */
	public String getJankalyanDBName(String realPath) {

		String DBName = null;

		File xmlFile = new File(realPath + File.separator + "configuration.xml");

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("rudhira-config");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					DBName = eElement.getElementsByTagName("Jankalyan-DB-Name").item(0).getTextContent();
				}
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return DBName;

	}

	/**
	 * 
	 * @return
	 */
	public String getJankalyanDBPass(String realPath) {

		String DBPass = null;

		File xmlFile = new File(realPath + File.separator + "configuration.xml");

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("rudhira-config");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					DBPass = eElement.getElementsByTagName("Jankalyan-DB-Password").item(0).getTextContent();
				}
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return DBPass;

	}

	/**
	 * 
	 * @return
	 */
	public String getTallyIPAddress(String realPath) {

		String tallyIP = null;

		File xmlFile = new File(realPath + File.separator + "configuration.xml");

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("rudhira-config");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					tallyIP = eElement.getElementsByTagName("Tally-IP").item(0).getTextContent();
				}
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return tallyIP;

	}

	/**
	 * 
	 * @return
	 */
	public String getTallyPort(String realPath) {

		String tallyPort = null;

		File xmlFile = new File(realPath + File.separator + "configuration.xml");

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("rudhira-config");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					tallyPort = eElement.getElementsByTagName("Tally-Port").item(0).getTextContent();
				}
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return tallyPort;

	}

	/**
	 * 
	 * @return
	 */
	public String getTallyFilePath(String realPath) {

		String tallyFilePath = null;

		File xmlFile = new File(realPath + File.separator + "configuration.xml");

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("rudhira-config");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					tallyFilePath = eElement.getElementsByTagName("Tally-File-Path").item(0).getTextContent();
				}
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return tallyFilePath;

	}

}
