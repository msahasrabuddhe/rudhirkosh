package rudhira.util;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Arrays;

import rudhira.DAO.BillingDAOImpl;
import rudhira.DAO.BillingDAOInf;
import rudhira.DAO.UserDAOImpl;
import rudhira.DAO.UserDAOInf;
import rudhira.form.ReportForm;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Document;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.Barcode128;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * 
 * @author Kovid Bioanalytics
 * 
 */
public class PDFUtil extends DBConnection {

	Connection connection = null;
	PreparedStatement preparedStatement = null;
	ResultSet resultSet = null;
	PreparedStatement preparedStatement1 = null;
	ResultSet resultSet1 = null;
	PreparedStatement preparedStatement2 = null;
	ResultSet resultSet2 = null;
	PreparedStatement preparedStatement3 = null;
	ResultSet resultSet3 = null;
	PreparedStatement preparedStatement4 = null;
	ResultSet resultSet4 = null;
	PreparedStatement preparedStatement5 = null;
	ResultSet resultSet5 = null;
	PreparedStatement preparedStatement6 = null;
	ResultSet resultSet6 = null;
	PreparedStatement preparedStatement7 = null;
	ResultSet resultSet7 = null;
	PreparedStatement preparedStatement8 = null;
	ResultSet resultSet8 = null;
	PreparedStatement preparedStatement9 = null;
	ResultSet resultSet9 = null;
	PreparedStatement preparedStatement10 = null;
	ResultSet resultSet10 = null;
	PreparedStatement preparedStatement11 = null;
	ResultSet resultSet11 = null;
	PreparedStatement preparedStatement12 = null;
	ResultSet resultSet12 = null;
	PreparedStatement preparedStatement13 = null;
	ResultSet resultSet13 = null;
	PreparedStatement preparedStatement14 = null;
	ResultSet resultSet14 = null;
	PreparedStatement preparedStatement15 = null;
	ResultSet resultSet15 = null;

	String status = "error";

	BillingDAOInf billingDAOInf = null;

	UserDAOInf userDAOInf = null;

	public String generateBillPDF(int bloodBankID, String userFullName, String patientIDNo, int receiptID,
			String PDFFileName, String realPath, String bbStorageCenter) {

		billingDAOInf = new BillingDAOImpl();
		userDAOInf = new UserDAOImpl();

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

		String patientIDNO = null;
		String patientName = null;
		String hospital = null;
		String address = null;
		String bbrNo = null;
		String receiptNo = null;
		String receiptDate = null;
		String manualReceiptNo = null;
		String mobile = "";

		double totalAmt = 0D;
		double netReceivableAmt = 0D;
		double actualPayment = 0D;
		double outstandingAmt = 0D;

		String receiptType = "";
		String paymentType = "";
		double tdsAmt = 0D;

		String concession = "";
		double concessionAmt = 0D;

		String refReceiptNo = null;
		String outstandingPaidDate = null;
		double outstandingPaidAmt = 0D;

		String bloodBankName = null;
		String bloodBankregNo = null;
		String bloodBankAddress = null;
		String bloodBankPhone = null;

		String chequNo = "";
		String chequDate = "";
		String chequeIssuedBy = "";
		String chequBankName = "";
		String chequeBankBranch = "";
		double chequeAmt = 0D;
		String cardMobileNo = "";

		String charityCaseNo = "";

		String bloodBagStorageCentre = "";

		int srNo = 1;

		String marathiFontDir = realPath + "fonts/FreeSans.ttf";

		int componentCount = 0;

		try {

			connection = getConnection();

			/*
			 * Generate the query to fetch Blood Bank Details
			 */
			String fetchDetailQuery1 = QueryMaker.BILL_PDF_RETRIEVE_BLOOD_BANK_DETAILS;

			preparedStatement = connection.prepareStatement(fetchDetailQuery1);
			preparedStatement.setInt(1, bloodBankID);
			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				bloodBankName = resultSet.getString("name");
				bloodBankAddress = resultSet.getString("address");
				bloodBankPhone = resultSet.getString("phone1") + " / " + resultSet.getString("phone2");
				bloodBankregNo = resultSet.getString("registrationNo");
			}

			/*
			 * Check whether patientIDNo is null, if so, the retrieve only blood
			 * bank storage center details and display them else display patient
			 * details
			 */
			if (patientIDNo == null || patientIDNo == "" || patientIDNo.isEmpty()) {

				bloodBagStorageCentre = bbStorageCenter;

			} else {

				/*
				 * Generate the query to fetch Patient Details
				 */
				String fetchDetailQuery2 = QueryMaker.BILL_PDF_RETRIEVE_PATIENT_DETAILS;

				preparedStatement1 = connection.prepareStatement(fetchDetailQuery2);
				preparedStatement1.setString(1, patientIDNo);
				resultSet1 = preparedStatement1.executeQuery();

				while (resultSet1.next()) {
					patientIDNO = resultSet1.getString("patientIdentifier");

					if (resultSet1.getString("middleName") == null || resultSet1.getString("middleName") == "") {

						patientName = resultSet1.getString("firstName") + " " + resultSet1.getString("lastName");

					} else {

						patientName = resultSet1.getString("firstName") + " " + resultSet1.getString("middleName") + " "
								+ resultSet1.getString("lastName");

					}

					hospital = resultSet1.getString("hospital");
					address = resultSet1.getString("address");
					bloodBagStorageCentre = resultSet1.getString("bloodBankStorage");
					mobile = resultSet1.getString("mobile");
				}

			}

			/*
			 * Generate the query to fetch Receipt No and Receipt Date From
			 * Receipt
			 */
			String fetchDetailQuery3 = QueryMaker.BILL_PDF_RETRIEVE_RECEIPT_DETAILS1;

			preparedStatement2 = connection.prepareStatement(fetchDetailQuery3);
			preparedStatement2.setInt(1, receiptID);
			resultSet2 = preparedStatement2.executeQuery();

			while (resultSet2.next()) {

				receiptNo = resultSet2.getString("receiptNo");
				receiptDate = dateFormat.format(resultSet2.getTimestamp("receiptDate"));
				receiptType = resultSet2.getString("receiptType");
				manualReceiptNo = resultSet2.getString("manualReceiptNo");

				String[] array = receiptDate.split("\\.");
				receiptDate = array[0];

			}

			/*
			 * Generate the query to fetch Receipt Details
			 */
			String fetchDetailQuery4 = QueryMaker.BILL_PDF_RETRIEVE_RECEIPT_DETAILS2;

			preparedStatement3 = connection.prepareStatement(fetchDetailQuery4);
			preparedStatement3.setInt(1, receiptID);
			resultSet3 = preparedStatement3.executeQuery();

			while (resultSet3.next()) {

				totalAmt = resultSet3.getDouble("totalAmt");
				netReceivableAmt = resultSet3.getDouble("netReceivableAmt");
				actualPayment = resultSet3.getDouble("actualPayment");
				outstandingAmt = resultSet3.getDouble("outstandingAmt");

				concession = resultSet3.getString("concessionType");
				concessionAmt = resultSet3.getDouble("concessionAmt");

				refReceiptNo = resultSet3.getString("refReceiptNumber");
				outstandingPaidDate = resultSet3.getString("outstandingPaidDate");
				outstandingPaidAmt = resultSet3.getDouble("outstandingPaidAmt");
				paymentType = resultSet3.getString("paymentType");
				tdsAmt = resultSet3.getDouble("tdsAmt");
				bbrNo = resultSet3.getString("bdrNo");
				charityCaseNo = resultSet3.getString("charityCaseNo");

			}

			/*
			 * Generate the query to fetch ReceiptItems details
			 */
			String fetchDetailQuery5 = QueryMaker.BILL_PDF_RETRIEVE_RECEIPT_ITEM_DETAILS;

			preparedStatement4 = connection.prepareStatement(fetchDetailQuery5);
			preparedStatement4.setInt(1, receiptID);
			resultSet4 = preparedStatement4.executeQuery();

			/*
			 * Generate the query to fetch Other Charges details
			 */
			String fetchDetailQuery6 = QueryMaker.BILL_PDF_RETRIEVE_OTHER_CHARGES;

			preparedStatement5 = connection.prepareStatement(fetchDetailQuery6);
			preparedStatement5.setInt(1, receiptID);
			resultSet5 = preparedStatement5.executeQuery();

			/*
			 * Generate the query to fetch Cheque details
			 */
			String fetchDetailQuery7 = QueryMaker.BILL_PDF_RETRIEVE_CHEQUE_DETAILS;

			preparedStatement6 = connection.prepareStatement(fetchDetailQuery7);
			preparedStatement6.setInt(1, receiptID);
			resultSet6 = preparedStatement6.executeQuery();

			while (resultSet6.next()) {
				chequeIssuedBy = resultSet6.getString("chequeIssuedBy");
				chequNo = resultSet6.getString("chequeNumber");
				chequDate = resultSet6.getString("chequeDate");
				chequBankName = resultSet6.getString("bankName");
				chequeBankBranch = resultSet6.getString("bankBranch");
				chequeAmt = resultSet6.getDouble("chequeAmt");
				cardMobileNo = resultSet6.getString("mobileNo");
			}

			/*
			 * For office use details
			 */
			preparedStatement7 = connection.prepareStatement(fetchDetailQuery5);
			preparedStatement7.setInt(1, receiptID);
			resultSet7 = preparedStatement7.executeQuery();

			preparedStatement8 = connection.prepareStatement(fetchDetailQuery6);
			preparedStatement8.setInt(1, receiptID);
			resultSet8 = preparedStatement8.executeQuery();

			/*
			 * Retrieving receipt item count from ReceiptItems table
			 */
			String fetchDetailQuery12 = QueryMaker.BILL_PDF_RETRIEVE_RECEIPT_ITEM_COUNT;

			preparedStatement15 = connection.prepareStatement(fetchDetailQuery12);
			preparedStatement15.setInt(1, receiptID);
			resultSet15 = preparedStatement15.executeQuery();

			while (resultSet15.next()) {
				componentCount = resultSet15.getInt("count");
			}

			/*
			 * Setting path to store PDF file
			 */
			File file = new File(PDFFileName);
			/*
			 * Creating Document for PDF
			 */
			Document document = new Document(PageSize.A4);

			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file));

			Font Font1 = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.BOLD);
			Font Font3 = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
			Font Font2 = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.STRIKETHRU);
			Font mainContent = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.NORMAL);
			Font mainContent1 = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.NORMAL);
			// Font mainContent2 = new Font(Font.FontFamily.TIMES_ROMAN, 8,
			// Font.NORMAL);
			mainContent1.setColor(BaseColor.GRAY);

			/*
			 * Defining marathi font
			 */
			Font marathiFont = FontFactory.getFont(marathiFontDir, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			marathiFont.setSize(9f);

			document.open();

			PdfContentByte cb = writer.getDirectContent();

			// Barcode for patientIDNo
			Barcode128 code128 = new Barcode128();
			code128.setCode(String.valueOf(patientIDNO));
			code128.setCodeType(Barcode128.CODE128);
			Image patientIDImage = code128.createImageWithBarcode(cb, null, null);
			patientIDImage.setAbsolutePosition(30, 200);
			patientIDImage.scalePercent(55);

			// Barcode for BDRNo
			Barcode128 code1281 = new Barcode128();
			code1281.setCode(String.valueOf(bbrNo));
			code1281.setCodeType(Barcode128.CODE128);
			Image patientbdrNoImage = code1281.createImageWithBarcode(cb, null, null);
			patientbdrNoImage.setAbsolutePosition(30, 200);
			patientbdrNoImage.scalePercent(55);

			/*
			 * Setting header
			 */
			document.addCreator("Rudhira");
			document.addTitle("Bill");

			/*
			 * For Receipt type
			 */
			PdfPTable table0 = new PdfPTable(3);
			table0.setWidthPercentage(100);
			Rectangle rect0 = new Rectangle(270, 700);
			table0.setWidthPercentage(new float[] { 30, 90, 150 }, rect0);

			PdfPCell cell0 = new PdfPCell(new Paragraph("Receipt Type: ", Font1));
			cell0.setPaddingBottom(5);
			cell0.setBorderWidthBottom(1f);
			cell0.setPaddingTop(-15);
			cell0.setBorderColor(BaseColor.WHITE);

			PdfPCell cell01 = new PdfPCell(new Paragraph(receiptType, mainContent));
			cell01.setPaddingBottom(5);
			cell01.setBorderWidthBottom(1f);
			cell01.setColspan(2);
			cell01.setPaddingTop(-15);
			cell01.setBorderColor(BaseColor.WHITE);

			PdfPCell cell02 = new PdfPCell(new Paragraph("", mainContent));
			cell02.setPaddingBottom(0);
			cell02.setBorderWidthBottom(1f);
			cell02.setColspan(3);
			cell02.setPaddingTop(-15);
			cell02.setBorderColorBottom(BaseColor.BLACK);
			cell02.setBorderColor(BaseColor.WHITE);

			table0.addCell(cell0);
			table0.addCell(cell01);
			table0.addCell(cell02);

			document.add(table0);

			/*
			 * For Blood Bank
			 */
			PdfPTable table = new PdfPTable(3);

			table.setFooterRows(1);
			table.setWidthPercentage(100);
			Rectangle rect = new Rectangle(270, 700);
			table.setWidthPercentage(new float[] { 90, 90, 90 }, rect);

			PdfPCell cell = new PdfPCell(new Paragraph("Non Refundable", Font1));
			cell.setPaddingBottom(2);
			cell.setBorderWidthBottom(1f);
			cell.setBorderColor(BaseColor.WHITE);

			/*
			 * For Blood Bank Name
			 */
			PdfPCell cell2 = new PdfPCell(new Paragraph(bloodBankName, Font3));
			cell2.setBorderWidth(0.01f);
			cell2.setPaddingBottom(2);
			cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell2.setBorderWidthLeft(0.2f);
			cell2.setBorderColor(BaseColor.WHITE);

			PdfPCell cell3 = new PdfPCell(new Paragraph("Patient Copy", Font1));
			cell3.setBorderWidth(0.2f);
			cell3.setPaddingBottom(2);
			cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell3.setBorderColor(BaseColor.WHITE);

			/*
			 * For registration no
			 */
			PdfPCell cell4 = new PdfPCell(new Paragraph("Reg. No.: " + bloodBankregNo, Font1));
			cell4.setBorderWidth(0.01f);
			cell4.setPaddingBottom(3);
			cell4.setBorderWidthLeft(0.2f);
			cell4.setBorderColor(BaseColor.WHITE);

			/*
			 * For address
			 */
			PdfPCell cell5 = new PdfPCell(new Paragraph(bloodBankAddress, Font1));
			cell5.setBorderWidth(0.2f);
			cell5.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell5.setPaddingBottom(3);
			cell5.setBorderColor(BaseColor.WHITE);

			/*
			 * For Phone
			 */
			PdfPCell cell6 = new PdfPCell(new Paragraph("Phone: " + bloodBankPhone, Font1));
			cell6.setBorderWidth(0.01f);
			cell6.setPaddingBottom(3);
			cell6.setBorderWidthLeft(0.2f);
			cell6.setBorderColor(BaseColor.WHITE);

			/*
			 * For border at the bottom
			 */
			PdfPCell cell7 = new PdfPCell(new Paragraph("", Font1));
			cell7.setPaddingBottom(0);
			cell7.setBorderWidthBottom(1.3f);
			cell7.setColspan(3);
			cell7.setBorderColorBottom(BaseColor.BLACK);
			cell7.setBorderColor(BaseColor.WHITE);

			/*
			 * adding all cell to the table to create tabular structure
			 */

			table.addCell(cell);
			table.addCell(cell2);
			table.addCell(cell3);
			table.addCell(cell4);
			table.addCell(cell5);
			table.addCell(cell6);
			table.addCell(cell7);

			document.add(table);

			if (patientIDNo == null || patientIDNo == "") {

				/*
				 * for Patient Details
				 */
				PdfPTable table2 = new PdfPTable(3);
				table2.setWidthPercentage(100);
				Rectangle rect1 = new Rectangle(270, 700);
				table2.setWidthPercentage(new float[] { 130, 70, 70 }, rect1);

				/*
				 * Creating Table header
				 */

				// For blank space
				PdfPCell cell8 = new PdfPCell(new Paragraph("", Font2));
				cell8.setColspan(3);
				cell8.setPadding(0);
				cell8.setPaddingBottom(2);
				cell8.setBorderColor(BaseColor.WHITE);

				// For Receipt Date and time
				PdfPCell cell9 = new PdfPCell(new Paragraph("Receipt Date and Time: " + receiptDate, mainContent));
				cell9.setPaddingBottom(2);
				cell9.setPaddingTop(-1);
				cell9.setBorderWidthRight(0f);
				cell9.setBorderWidthLeft(0f);
				cell9.setBorderWidthTop(0f);
				cell9.setPaddingLeft(20);
				cell9.setBorderWidthBottom(0f);
				cell9.setBorderColor(BaseColor.WHITE);

				// For receipt no.
				PdfPCell cell10 = new PdfPCell(new Paragraph("Receipt No.: " + receiptNo, mainContent));
				cell10.setBorderWidth(0.01f);
				cell10.setPaddingBottom(2);
				cell10.setPaddingTop(-1);
				cell10.setBorderColor(BaseColor.WHITE);

				// For manual receipt no.
				PdfPCell cell101 = new PdfPCell(new Paragraph("Manual Receipt No.: " + manualReceiptNo, mainContent));
				cell101.setBorderWidth(0.01f);
				cell101.setPaddingBottom(2);
				cell101.setPaddingTop(-1);
				cell101.setBorderColor(BaseColor.WHITE);

				// For patient Name
				PdfPCell cell12 = new PdfPCell(
						new Paragraph("Blood Bank Storage Center: " + bloodBagStorageCentre, mainContent));
				cell12.setBorderWidth(0.01f);
				cell12.setPaddingBottom(2);
				cell12.setPaddingTop(2);
				cell12.setColspan(3);
				cell12.setPaddingLeft(20);
				cell12.setBorderColor(BaseColor.WHITE);

				// For bbrNo
				PdfPCell cell17 = new PdfPCell(new Paragraph("", mainContent));
				cell17.setBorderWidth(0.01f);
				cell17.setPaddingBottom(0);
				cell17.setPaddingTop(2);
				cell17.setPaddingLeft(20);
				cell17.setPaddingBottom(2);
				cell17.setBorderColor(BaseColor.WHITE);

				// For patientID barcode
				PdfPCell cell144 = new PdfPCell(new Paragraph("", mainContent));
				cell144.setBorderWidth(0.01f);
				cell144.setPaddingBottom(7);
				cell144.setPaddingTop(2);
				cell144.setColspan(2);
				cell144.setBorderColor(BaseColor.WHITE);

				table2.addCell(cell8);
				table2.addCell(cell9);
				table2.addCell(cell10);
				table2.addCell(cell101);
				table2.addCell(cell12);
				table2.addCell(cell17);
				table2.addCell(cell144);

				document.add(table2);

			} else if (patientIDNo.isEmpty()) {

				/*
				 * for Patient Details
				 */
				PdfPTable table2 = new PdfPTable(3);
				table2.setWidthPercentage(100);
				Rectangle rect1 = new Rectangle(270, 700);
				table2.setWidthPercentage(new float[] { 130, 70, 70 }, rect1);

				/*
				 * Creating Table header
				 */

				// For blank space
				PdfPCell cell8 = new PdfPCell(new Paragraph("", Font2));
				cell8.setColspan(3);
				cell8.setPadding(0);
				cell8.setPaddingBottom(2);
				cell8.setBorderColor(BaseColor.WHITE);

				// For Receipt Date and time
				PdfPCell cell9 = new PdfPCell(new Paragraph("Receipt Date and Time: " + receiptDate, mainContent));
				cell9.setPaddingBottom(2);
				cell9.setPaddingTop(-1);
				cell9.setBorderWidthRight(0f);
				cell9.setBorderWidthLeft(0f);
				cell9.setBorderWidthTop(0f);
				cell9.setPaddingLeft(20);
				cell9.setBorderWidthBottom(0f);
				cell9.setBorderColor(BaseColor.WHITE);

				// For receipt no.
				PdfPCell cell10 = new PdfPCell(new Paragraph("Receipt No.: " + receiptNo, mainContent));
				cell10.setBorderWidth(0.01f);
				cell10.setPaddingBottom(2);
				cell10.setPaddingTop(-1);
				cell10.setBorderColor(BaseColor.WHITE);

				// For manual receipt no.
				PdfPCell cell101 = new PdfPCell(new Paragraph("Manual Receipt No.: " + manualReceiptNo, mainContent));
				cell101.setBorderWidth(0.01f);
				cell101.setPaddingBottom(2);
				cell101.setPaddingTop(-1);
				cell101.setBorderColor(BaseColor.WHITE);

				// For patient Name
				PdfPCell cell12 = new PdfPCell(
						new Paragraph("Blood Bank Storage Center: " + bloodBagStorageCentre, mainContent));
				cell12.setBorderWidth(0.01f);
				cell12.setPaddingBottom(2);
				cell12.setPaddingTop(2);
				cell12.setColspan(3);
				cell12.setPaddingLeft(20);
				cell12.setBorderColor(BaseColor.WHITE);

				// For bbrNo
				PdfPCell cell17 = new PdfPCell(new Paragraph("", mainContent));
				cell17.setBorderWidth(0.01f);
				cell17.setPaddingBottom(0);
				cell17.setPaddingTop(2);
				cell17.setPaddingLeft(20);
				cell17.setPaddingBottom(2);
				cell17.setBorderColor(BaseColor.WHITE);

				// For patientID barcode
				PdfPCell cell144 = new PdfPCell(new Paragraph("", mainContent));
				cell144.setBorderWidth(0.01f);
				cell144.setPaddingBottom(7);
				cell144.setPaddingTop(2);
				cell144.setColspan(2);
				cell144.setBorderColor(BaseColor.WHITE);

				table2.addCell(cell8);
				table2.addCell(cell9);
				table2.addCell(cell10);
				table2.addCell(cell101);
				table2.addCell(cell12);
				table2.addCell(cell17);
				table2.addCell(cell144);

				document.add(table2);

			} else {

				/*
				 * for Patient Details
				 */
				PdfPTable table2 = new PdfPTable(3);
				table2.setWidthPercentage(100);
				Rectangle rect1 = new Rectangle(270, 700);
				table2.setWidthPercentage(new float[] { 130, 70, 70 }, rect1);

				/*
				 * Creating Table header
				 */

				// For blank space
				PdfPCell cell8 = new PdfPCell(new Paragraph("", Font2));
				cell8.setColspan(3);
				cell8.setPadding(0);
				cell8.setPaddingBottom(2);
				cell8.setBorderColor(BaseColor.WHITE);

				// For Receipt Date and time
				PdfPCell cell9 = new PdfPCell(new Paragraph("Receipt Date and Time: " + receiptDate, mainContent));
				cell9.setPaddingBottom(2);
				cell9.setPaddingTop(-1);
				cell9.setBorderWidthRight(0f);
				cell9.setBorderWidthLeft(0f);
				cell9.setBorderWidthTop(0f);
				cell9.setPaddingLeft(20);
				cell9.setBorderWidthBottom(0f);
				cell9.setBorderColor(BaseColor.WHITE);

				// For receipt no.
				PdfPCell cell10 = new PdfPCell(new Paragraph("Receipt No.: " + receiptNo, mainContent));
				cell10.setBorderWidth(0.01f);
				cell10.setPaddingBottom(2);
				cell10.setPaddingTop(-1);
				cell10.setBorderColor(BaseColor.WHITE);

				// For manual receipt no.
				PdfPCell cell101 = new PdfPCell(new Paragraph("Manual Receipt No.: " + manualReceiptNo, mainContent));
				cell101.setBorderWidth(0.01f);
				cell101.setPaddingBottom(2);
				cell101.setPaddingTop(-1);
				cell101.setBorderColor(BaseColor.WHITE);

				// For patient Name
				PdfPCell cell12 = new PdfPCell(new Paragraph("Patient Name: " + patientName, mainContent));
				cell12.setBorderWidth(0.01f);
				cell12.setPaddingBottom(2);
				cell12.setPaddingTop(2);
				cell12.setPaddingLeft(20);
				cell12.setBorderColor(BaseColor.WHITE);

				// For patient identifiers no
				PdfPCell cell13 = new PdfPCell(new Paragraph("Patient ID: " + patientIDNO, mainContent));
				cell13.setBorderWidth(0.01f);
				cell13.setPaddingBottom(2);
				cell13.setPaddingTop(2);
				cell13.setBorderColor(BaseColor.WHITE);

				// For patientID barcode
				PdfPCell cell14 = new PdfPCell(patientIDImage);
				cell14.setBorderWidth(0.01f);
				cell14.setPaddingBottom(2);
				cell14.setPaddingTop(2);
				cell14.setBorderColor(BaseColor.WHITE);

				// For hospital
				PdfPCell cell15 = new PdfPCell(new Paragraph("Hospital: " + hospital, mainContent));
				cell15.setBorderWidth(0.01f);
				cell15.setPaddingBottom(2);
				cell15.setPaddingTop(-7);
				cell15.setColspan(3);
				cell15.setPaddingLeft(20);
				cell15.setBorderColor(BaseColor.WHITE);

				// For address
				PdfPCell cell16 = new PdfPCell(new Paragraph("Address: " + address, mainContent));
				cell16.setBorderWidth(0.01f);
				cell16.setPaddingBottom(0);
				cell16.setPaddingTop(2);
				cell16.setPaddingLeft(20);
				cell16.setPaddingBottom(2);
				cell16.setBorderColor(BaseColor.WHITE);

				// For bbrNo
				PdfPCell cell17 = new PdfPCell(new Paragraph("", mainContent));
				cell17.setBorderWidth(0.01f);
				cell17.setPaddingBottom(0);
				cell17.setPaddingTop(2);
				cell17.setPaddingBottom(2);
				cell17.setBorderColor(BaseColor.WHITE);

				// For patientID barcode
				PdfPCell cell144 = new PdfPCell(new Paragraph("", mainContent));
				cell144.setBorderWidth(0.01f);
				cell144.setPaddingBottom(7);
				cell144.setPaddingTop(2);
				cell144.setBorderColor(BaseColor.WHITE);

				// For Charity case no
				PdfPCell cell1445 = new PdfPCell(new Paragraph("Mobile No.: " + mobile, mainContent));
				cell1445.setBorderWidth(0.01f);
				cell1445.setPaddingBottom(2);
				cell1445.setPaddingTop(-7);
				cell1445.setBorderColor(BaseColor.WHITE);
				cell1445.setColspan(3);
				cell1445.setPaddingLeft(20);

				// For Charity case no
				PdfPCell cell1444 = new PdfPCell(new Paragraph("Charity Case No.: " + charityCaseNo, mainContent));
				cell1444.setBorderWidth(0.01f);
				cell1444.setPaddingBottom(2);
				cell1444.setPaddingTop(-7);
				cell1444.setBorderColor(BaseColor.WHITE);

				table2.addCell(cell8);
				table2.addCell(cell9);
				table2.addCell(cell10);
				table2.addCell(cell101);
				table2.addCell(cell12);
				table2.addCell(cell13);
				table2.addCell(cell14);
				table2.addCell(cell15);
				table2.addCell(cell16);
				table2.addCell(cell17);
				table2.addCell(cell144);
				table2.addCell(cell1445);
				table2.addCell(cell1444);

				document.add(table2);

			}

			/*
			 * For Component details table
			 */
			PdfPTable table3 = new PdfPTable(5);
			table3.setWidthPercentage(100);
			Rectangle rect2 = new Rectangle(310, 700);
			table3.setWidthPercentage(new float[] { 20, 135, 40, 45, 45 }, rect2);

			/*
			 * For blank space
			 */
			PdfPCell cell18 = new PdfPCell(new Paragraph("", Font2));
			cell18.setColspan(6);
			cell18.setBorderColor(BaseColor.WHITE);

			/*
			 * Creating header titles for table
			 */
			// For Sr.No.
			PdfPCell cell19 = new PdfPCell(new Paragraph("Sr.No.", Font1));
			cell19.setBorderWidth(0.01f);
			cell19.setPaddingBottom(3);
			cell19.setBorderColor(BaseColor.GRAY);

			// For Product req.
			PdfPCell cell20 = new PdfPCell(new Paragraph("Product Req.", Font1));
			cell20.setBorderWidth(0.01f);
			cell20.setPaddingBottom(3);
			cell20.setBorderColor(BaseColor.GRAY);

			// For Quantity
			PdfPCell cell22 = new PdfPCell(new Paragraph("Quantity", Font1));
			cell22.setBorderWidth(0.01f);
			cell22.setPaddingBottom(3);
			cell22.setBorderColor(BaseColor.GRAY);

			// For Rate
			PdfPCell cell23 = new PdfPCell(new Paragraph("Rate", Font1));
			cell23.setBorderWidth(0.01f);
			cell23.setPaddingBottom(3);
			cell23.setBorderColor(BaseColor.GRAY);

			// For Amount
			PdfPCell cell24 = new PdfPCell(new Paragraph("Amount", Font1));
			cell24.setBorderWidth(0.01f);
			cell24.setPaddingBottom(3);
			cell24.setBorderColor(BaseColor.GRAY);

			table3.addCell(cell18);
			table3.addCell(cell19);
			table3.addCell(cell20);
			table3.addCell(cell22);
			table3.addCell(cell23);
			table3.addCell(cell24);

			/*
			 * For Receipt items values
			 */
			while (resultSet4.next()) {

				// For Sr.No.
				PdfPCell cell37 = new PdfPCell(new Paragraph("" + srNo, mainContent));
				cell37.setBorderWidth(0.01f);
				cell37.setPaddingBottom(3);
				cell37.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell37.setBorderColor(BaseColor.GRAY);

				// For Product req.
				PdfPCell cell38 = new PdfPCell(new Paragraph(resultSet4.getString("product"), mainContent));
				cell38.setBorderWidth(0.01f);
				cell38.setPaddingBottom(3);
				cell38.setBorderColor(BaseColor.GRAY);

				int receiptItemID = resultSet4.getInt("id");

				// For Bag No.
				PdfPCell cell40 = new PdfPCell(new Paragraph("" + resultSet4.getInt("quantity"), mainContent));
				cell40.setBorderWidth(0.01f);
				cell40.setPaddingBottom(3);
				cell40.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell40.setBorderColor(BaseColor.GRAY);

				// For Rate
				PdfPCell cell41 = new PdfPCell(new Paragraph("" + resultSet4.getDouble("rate"), mainContent));
				cell41.setBorderWidth(0.01f);
				cell41.setPaddingBottom(3);
				cell41.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell41.setBorderColor(BaseColor.GRAY);

				// For Amount
				PdfPCell cell42 = new PdfPCell(new Paragraph("" + resultSet4.getDouble("amount"), mainContent));
				cell42.setBorderWidth(0.01f);
				cell42.setPaddingBottom(3);
				cell42.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell42.setBorderColor(BaseColor.GRAY);

				srNo++;

				table3.addCell(cell37);
				table3.addCell(cell38);
				table3.addCell(cell40);
				table3.addCell(cell41);
				table3.addCell(cell42);

			}

			/*
			 * For total
			 */
			PdfPCell cell43 = new PdfPCell(new Paragraph("Total Amount", Font1));
			cell43.setBorderWidth(0.01f);
			cell43.setPaddingBottom(3);
			cell43.setColspan(4);
			cell43.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell43.setBorderColor(BaseColor.GRAY);

			PdfPCell cell44 = new PdfPCell(new Paragraph("" + totalAmt, mainContent));
			cell44.setBorderWidth(0.01f);
			cell44.setPaddingBottom(3);
			cell44.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell44.setBorderColor(BaseColor.GRAY);

			table3.addCell(cell43);
			table3.addCell(cell44);

			/*
			 * For Other charges
			 */
			while (resultSet5.next()) {

				PdfPCell cell45 = new PdfPCell(new Paragraph("Other Charges", Font1));
				cell45.setBorderWidth(0.01f);
				cell45.setPaddingBottom(3);
				cell45.setColspan(3);
				cell45.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell45.setBorderColor(BaseColor.GRAY);

				PdfPCell cell46 = new PdfPCell(new Paragraph(resultSet5.getString("chargeType"), mainContent));
				cell46.setBorderWidth(0.01f);
				cell46.setPaddingBottom(3);
				cell46.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell46.setBorderColor(BaseColor.GRAY);

				PdfPCell cell47 = new PdfPCell(new Paragraph("" + resultSet5.getDouble("otherCharges"), mainContent));
				cell47.setBorderWidth(0.01f);
				cell47.setPaddingBottom(3);
				cell47.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell47.setBorderColor(BaseColor.GRAY);

				table3.addCell(cell45);
				table3.addCell(cell46);
				table3.addCell(cell47);

			}

			PdfPCell cell48 = null;
			PdfPCell cell49 = null;
			PdfPCell cell50 = null;

			if (!concession.equals("000")) {

				/*
				 * For Concession
				 */
				cell48 = new PdfPCell(new Paragraph("Concession", Font1));
				cell48.setBorderWidth(0.01f);
				cell48.setPaddingBottom(3);
				cell48.setColspan(3);
				cell48.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell48.setBorderColor(BaseColor.GRAY);

				cell49 = new PdfPCell(new Paragraph(concession, mainContent));
				cell49.setBorderWidth(0.01f);
				cell49.setPaddingBottom(3);
				cell49.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell49.setBorderColor(BaseColor.GRAY);

				cell50 = new PdfPCell(new Paragraph("" + concessionAmt, mainContent));
				cell50.setBorderWidth(0.01f);
				cell50.setPaddingBottom(3);
				cell50.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell50.setBorderColor(BaseColor.GRAY);

			}

			/*
			 * For Net receivable amount
			 */
			PdfPCell cell51 = new PdfPCell(new Paragraph("Net Receivable Amount", Font1));
			cell51.setBorderWidth(0.01f);
			cell51.setPaddingBottom(3);
			cell51.setColspan(4);
			cell51.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell51.setBorderColor(BaseColor.GRAY);

			PdfPCell cell52 = new PdfPCell(new Paragraph("" + netReceivableAmt, mainContent));
			cell52.setBorderWidth(0.01f);
			cell52.setPaddingBottom(3);
			cell52.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell52.setBorderColor(BaseColor.GRAY);

			/*
			 * For actual payment
			 */
			PdfPCell cell53 = new PdfPCell(new Paragraph("Actual Payment", Font1));
			cell53.setBorderWidth(0.01f);
			cell53.setPaddingBottom(3);
			cell53.setColspan(4);
			cell53.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell53.setBorderColor(BaseColor.GRAY);

			PdfPCell cell54 = new PdfPCell(new Paragraph("" + actualPayment, mainContent));
			cell54.setBorderWidth(0.01f);
			cell54.setPaddingBottom(3);
			cell54.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell54.setBorderColor(BaseColor.GRAY);

			/*
			 * For outstanding amount
			 */
			PdfPCell cell55 = new PdfPCell(new Paragraph("Outstanding Amount", Font1));
			cell55.setBorderWidth(0.01f);
			cell55.setPaddingBottom(3);
			cell55.setColspan(4);
			cell55.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell55.setBorderWidthBottom(1.2f);
			cell55.setBorderColor(BaseColor.GRAY);

			PdfPCell cell56 = new PdfPCell(new Paragraph("" + outstandingAmt, mainContent));
			cell56.setBorderWidth(0.01f);
			cell56.setPaddingBottom(3);
			cell56.setBorderWidthBottom(1.2f);
			cell56.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell56.setBorderColor(BaseColor.GRAY);

			if (!concession.equals("000")) {

				table3.addCell(cell48);
				table3.addCell(cell49);
				table3.addCell(cell50);

			}
			table3.addCell(cell51);
			table3.addCell(cell52);
			table3.addCell(cell53);
			table3.addCell(cell54);
			table3.addCell(cell55);
			table3.addCell(cell56);

			document.add(table3);

			/*
			 * For remaining details
			 */
			PdfPTable table4 = new PdfPTable(4);
			table4.setWidthPercentage(100);
			Rectangle rect3 = new Rectangle(270, 700);
			table4.setWidthPercentage(new float[] { 65, 70, 65, 70 }, rect3);

			/*
			 * Checking whether payment type is cash or cheque, if its cheque
			 * then display cheque details on PDF else if it is debit/credit
			 * card then showing mobile no as card details don't
			 */
			if (paymentType.equals("Cheque")) {

				/*
				 * For Payment Type
				 */
				PdfPCell cell57 = new PdfPCell(new Paragraph("Payment Type: ", mainContent));
				cell57.setBorderWidth(0.01f);
				cell57.setPaddingBottom(2);
				cell57.setPaddingTop(2);
				cell57.setPaddingLeft(20);
				cell57.setBorderColor(BaseColor.WHITE);

				PdfPCell cell58 = new PdfPCell(new Paragraph(paymentType, mainContent));
				cell58.setBorderWidth(0.01f);
				cell58.setPaddingBottom(2);
				cell58.setPaddingTop(2);
				cell58.setBorderColor(BaseColor.WHITE);

				/*
				 * For TDS Amount
				 */
				PdfPCell cell59 = new PdfPCell(new Paragraph("TDS Amount", mainContent));
				cell59.setBorderWidth(0.01f);
				cell59.setPaddingBottom(2);
				cell59.setPaddingTop(2);
				cell59.setBorderColor(BaseColor.WHITE);

				PdfPCell cell60 = new PdfPCell(new Paragraph("" + tdsAmt, mainContent));
				cell60.setBorderWidth(0.01f);
				cell60.setPaddingBottom(2);
				cell60.setPaddingTop(2);
				cell60.setBorderColor(BaseColor.WHITE);

				PdfPCell cell61 = new PdfPCell(new Paragraph("", mainContent));
				cell61.setBorderWidth(0.01f);
				cell61.setColspan(4);
				cell61.setBorderWidthBottom(0.5f);
				cell61.setBorderColorBottom(BaseColor.GRAY);
				cell61.setBorderColor(BaseColor.WHITE);

				/*
				 * For cheque details
				 */
				// For cheque Issued By
				PdfPCell cell62 = new PdfPCell(new Paragraph("Cheque Issued By", mainContent));
				cell62.setBorderWidth(0.01f);
				cell62.setPaddingBottom(2);
				cell62.setPaddingTop(2);
				cell62.setPaddingLeft(20);
				cell62.setBorderColor(BaseColor.WHITE);

				PdfPCell cell63 = new PdfPCell(new Paragraph(chequeIssuedBy, mainContent));
				cell63.setBorderWidth(0.01f);
				cell63.setPaddingBottom(2);
				cell63.setPaddingTop(2);
				cell63.setBorderColor(BaseColor.WHITE);

				// For cheque No
				PdfPCell cell64 = new PdfPCell(new Paragraph("Cheque No.", mainContent));
				cell64.setBorderWidth(0.01f);
				cell64.setPaddingBottom(2);
				cell64.setPaddingTop(2);
				cell64.setBorderColor(BaseColor.WHITE);

				PdfPCell cell65 = new PdfPCell(new Paragraph(chequNo, mainContent));
				cell65.setBorderWidth(0.01f);
				cell65.setPaddingBottom(2);
				cell65.setPaddingTop(2);
				cell65.setBorderColor(BaseColor.WHITE);

				// For cheque Bank Name
				PdfPCell cell66 = new PdfPCell(new Paragraph("Bank Name", mainContent));
				cell66.setBorderWidth(0.01f);
				cell66.setPaddingBottom(2);
				cell66.setPaddingTop(2);
				cell66.setPaddingLeft(20);
				cell66.setBorderColor(BaseColor.WHITE);

				PdfPCell cell67 = new PdfPCell(new Paragraph(chequBankName, mainContent));
				cell67.setBorderWidth(0.01f);
				cell67.setPaddingBottom(2);
				cell67.setPaddingTop(2);
				cell67.setBorderColor(BaseColor.WHITE);

				// For cheque Bank Branch
				PdfPCell cell68 = new PdfPCell(new Paragraph("Branch", mainContent));
				cell68.setBorderWidth(0.01f);
				cell68.setPaddingBottom(2);
				cell68.setPaddingTop(2);
				cell68.setBorderColor(BaseColor.WHITE);

				PdfPCell cell69 = new PdfPCell(new Paragraph(chequeBankBranch, mainContent));
				cell69.setBorderWidth(0.01f);
				cell69.setPaddingBottom(2);
				cell69.setPaddingTop(2);
				cell69.setBorderColor(BaseColor.WHITE);

				// For cheque Date
				PdfPCell cell70 = new PdfPCell(new Paragraph("Date", mainContent));
				cell70.setBorderWidth(0.01f);
				cell70.setPaddingBottom(2);
				cell70.setPaddingTop(2);
				cell70.setPaddingLeft(20);
				cell70.setBorderColor(BaseColor.WHITE);

				PdfPCell cell71 = new PdfPCell(new Paragraph(chequDate, mainContent));
				cell71.setBorderWidth(0.01f);
				cell71.setPaddingBottom(2);
				cell71.setPaddingTop(2);
				cell71.setBorderColor(BaseColor.WHITE);

				// For cheque amount
				PdfPCell cell72 = new PdfPCell(new Paragraph("Amount", mainContent));
				cell72.setBorderWidth(0.01f);
				cell72.setPaddingBottom(2);
				cell72.setPaddingTop(2);
				cell72.setBorderColor(BaseColor.WHITE);

				PdfPCell cell73 = new PdfPCell(new Paragraph("" + chequeAmt, mainContent));
				cell73.setBorderWidth(0.01f);
				cell73.setPaddingBottom(2);
				cell73.setPaddingTop(2);
				cell73.setBorderColor(BaseColor.WHITE);

				PdfPCell cell74 = new PdfPCell(new Paragraph("", mainContent));
				cell74.setBorderWidth(0.01f);
				cell74.setColspan(4);
				cell74.setBorderWidthBottom(0.5f);
				cell74.setBorderColorBottom(BaseColor.GRAY);
				cell74.setBorderColor(BaseColor.WHITE);

				// For ref receipt No.
				PdfPCell cell75 = new PdfPCell(
						new Paragraph("Ref. Receipt No. (Cross Matching Referance)", mainContent));
				cell75.setBorderWidth(0.01f);
				cell75.setPaddingBottom(2);
				cell75.setPaddingTop(2);
				cell75.setColspan(3);
				cell75.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell75.setBorderColor(BaseColor.WHITE);

				// For ref receipt No.
				PdfPCell cell76 = new PdfPCell(new Paragraph(refReceiptNo, mainContent));
				cell76.setBorderWidth(0.01f);
				cell76.setPaddingBottom(2);
				cell76.setPaddingTop(2);
				cell76.setBorderColor(BaseColor.WHITE);

				/*
				 * For outstanding paid date
				 */
				PdfPCell cell77 = new PdfPCell(new Paragraph("Outstanding Paid Date", mainContent));
				cell77.setBorderWidth(0.01f);
				cell77.setPaddingBottom(2);
				cell77.setPaddingTop(2);
				cell77.setPaddingLeft(20);
				cell77.setBorderColor(BaseColor.WHITE);

				PdfPCell cell78 = new PdfPCell(new Paragraph(outstandingPaidDate, mainContent));
				cell78.setBorderWidth(0.01f);
				cell78.setPaddingBottom(2);
				cell78.setPaddingTop(2);
				cell78.setBorderColor(BaseColor.WHITE);

				/*
				 * For outstanding paid amount
				 */
				PdfPCell cell79 = new PdfPCell(new Paragraph("Outstanding Paid Amount", mainContent));
				cell79.setBorderWidth(0.01f);
				cell79.setPaddingBottom(2);
				cell79.setPaddingTop(2);
				cell79.setBorderColor(BaseColor.WHITE);

				PdfPCell cell80 = new PdfPCell(new Paragraph("" + outstandingPaidAmt, mainContent));
				cell80.setBorderWidth(0.01f);
				cell80.setPaddingBottom(2);
				cell80.setPaddingTop(2);
				cell80.setBorderColor(BaseColor.WHITE);

				/*
				 * For receipt by
				 */
				PdfPCell cell81 = new PdfPCell(new Paragraph("Receipt Given By", mainContent));
				cell81.setBorderWidth(0.01f);
				cell81.setPaddingBottom(2);
				cell81.setPaddingTop(3);
				cell81.setPaddingLeft(20);
				cell81.setBorderColor(BaseColor.WHITE);

				PdfPCell cell82 = new PdfPCell(new Paragraph(userFullName, mainContent));
				cell82.setBorderWidth(0.01f);
				cell82.setPaddingBottom(2);
				cell82.setPaddingTop(3);
				cell82.setBorderColor(BaseColor.WHITE);

				/*
				 * For Receivers Signature
				 */
				PdfPCell cell83 = new PdfPCell(new Paragraph("Receivers Signature", mainContent));
				cell83.setBorderWidth(0.01f);
				cell83.setPaddingBottom(2);
				cell83.setPaddingTop(3);
				cell83.setColspan(2);
				cell83.setPaddingRight(20);
				cell83.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell83.setBorderColor(BaseColor.WHITE);

				table4.addCell(cell57);
				table4.addCell(cell58);
				table4.addCell(cell59);
				table4.addCell(cell60);
				table4.addCell(cell61);
				table4.addCell(cell62);
				table4.addCell(cell63);
				table4.addCell(cell64);
				table4.addCell(cell65);
				table4.addCell(cell66);
				table4.addCell(cell67);
				table4.addCell(cell68);
				table4.addCell(cell69);
				table4.addCell(cell70);
				table4.addCell(cell71);
				table4.addCell(cell72);
				table4.addCell(cell73);
				table4.addCell(cell74);
				table4.addCell(cell75);
				table4.addCell(cell76);
				table4.addCell(cell77);
				table4.addCell(cell78);
				table4.addCell(cell79);
				table4.addCell(cell80);
				table4.addCell(cell81);
				table4.addCell(cell82);
				table4.addCell(cell83);

			} else if (paymentType.equals("Credit/Debit Card")) {

				/*
				 * For Payment Type
				 */
				PdfPCell cell57 = new PdfPCell(new Paragraph("Payment Type: ", mainContent));
				cell57.setBorderWidth(0.01f);
				cell57.setPaddingBottom(2);
				cell57.setPaddingTop(2);
				cell57.setPaddingLeft(20);
				cell57.setBorderColor(BaseColor.WHITE);

				PdfPCell cell58 = new PdfPCell(new Paragraph(paymentType, mainContent));
				cell58.setBorderWidth(0.01f);
				cell58.setPaddingBottom(2);
				cell58.setPaddingTop(2);
				cell58.setBorderColor(BaseColor.WHITE);

				/*
				 * For TDS Amount
				 */
				PdfPCell cell59 = new PdfPCell(new Paragraph("TDS Amount", mainContent));
				cell59.setBorderWidth(0.01f);
				cell59.setPaddingBottom(2);
				cell59.setPaddingTop(2);
				cell59.setBorderColor(BaseColor.WHITE);

				PdfPCell cell60 = new PdfPCell(new Paragraph("" + tdsAmt, mainContent));
				cell60.setBorderWidth(0.01f);
				cell60.setPaddingBottom(2);
				cell60.setPaddingTop(2);
				cell60.setBorderColor(BaseColor.WHITE);

				PdfPCell cell61 = new PdfPCell(new Paragraph("", mainContent));
				cell61.setBorderWidth(0.01f);
				cell61.setColspan(4);
				cell61.setBorderWidthBottom(0.5f);
				cell61.setBorderColorBottom(BaseColor.GRAY);
				cell61.setBorderColor(BaseColor.WHITE);

				/*
				 * For credit/debit card details
				 */
				// For card mobile no
				PdfPCell cell62 = new PdfPCell(new Paragraph("Mobile No.", mainContent));
				cell62.setBorderWidth(0.01f);
				cell62.setPaddingBottom(2);
				cell62.setPaddingTop(2);
				cell62.setPaddingLeft(20);
				cell62.setBorderColor(BaseColor.WHITE);

				PdfPCell cell63 = new PdfPCell(new Paragraph(cardMobileNo, mainContent));
				cell63.setBorderWidth(0.01f);
				cell63.setPaddingBottom(2);
				cell63.setPaddingTop(2);
				cell63.setBorderColor(BaseColor.WHITE);

				// For blank
				PdfPCell cell64 = new PdfPCell(new Paragraph("", mainContent));
				cell64.setBorderWidth(0.01f);
				cell64.setPaddingBottom(2);
				cell64.setPaddingTop(2);
				cell64.setBorderColor(BaseColor.WHITE);

				PdfPCell cell65 = new PdfPCell(new Paragraph("", mainContent));
				cell65.setBorderWidth(0.01f);
				cell65.setPaddingBottom(2);
				cell65.setPaddingTop(2);
				cell65.setBorderColor(BaseColor.WHITE);

				PdfPCell cell74 = new PdfPCell(new Paragraph("", mainContent));
				cell74.setBorderWidth(0.01f);
				cell74.setColspan(4);
				cell74.setBorderWidthBottom(0.5f);
				cell74.setBorderColorBottom(BaseColor.GRAY);
				cell74.setBorderColor(BaseColor.WHITE);

				// For ref receipt No.
				PdfPCell cell75 = new PdfPCell(
						new Paragraph("Ref. Receipt No. (Cross Matching Referance)", mainContent));
				cell75.setBorderWidth(0.01f);
				cell75.setPaddingBottom(2);
				cell75.setPaddingTop(2);
				cell75.setColspan(3);
				cell75.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell75.setBorderColor(BaseColor.WHITE);

				// For ref receipt No.
				PdfPCell cell76 = new PdfPCell(new Paragraph(refReceiptNo, mainContent));
				cell76.setBorderWidth(0.01f);
				cell76.setPaddingBottom(2);
				cell76.setPaddingTop(2);
				cell76.setBorderColor(BaseColor.WHITE);

				/*
				 * For outstanding paid date
				 */
				PdfPCell cell77 = new PdfPCell(new Paragraph("Outstanding Paid Date", mainContent));
				cell77.setBorderWidth(0.01f);
				cell77.setPaddingBottom(2);
				cell77.setPaddingTop(2);
				cell77.setPaddingLeft(20);
				cell77.setBorderColor(BaseColor.WHITE);

				PdfPCell cell78 = new PdfPCell(new Paragraph(outstandingPaidDate, mainContent));
				cell78.setBorderWidth(0.01f);
				cell78.setPaddingBottom(2);
				cell78.setPaddingTop(2);
				cell78.setBorderColor(BaseColor.WHITE);

				/*
				 * For outstanding paid amount
				 */
				PdfPCell cell79 = new PdfPCell(new Paragraph("Outstanding Paid Amount", mainContent));
				cell79.setBorderWidth(0.01f);
				cell79.setPaddingBottom(2);
				cell79.setPaddingTop(2);
				cell79.setBorderColor(BaseColor.WHITE);

				PdfPCell cell80 = new PdfPCell(new Paragraph("" + outstandingPaidAmt, mainContent));
				cell80.setBorderWidth(0.01f);
				cell80.setPaddingBottom(2);
				cell80.setPaddingTop(2);
				cell80.setBorderColor(BaseColor.WHITE);

				/*
				 * For receipt by
				 */
				PdfPCell cell81 = new PdfPCell(new Paragraph("Receipt Given By", mainContent));
				cell81.setBorderWidth(0.01f);
				cell81.setPaddingBottom(2);
				cell81.setPaddingTop(3);
				cell81.setPaddingLeft(20);
				cell81.setBorderColor(BaseColor.WHITE);

				PdfPCell cell82 = new PdfPCell(new Paragraph(userFullName, mainContent));
				cell82.setBorderWidth(0.01f);
				cell82.setPaddingBottom(2);
				cell82.setPaddingTop(3);
				cell82.setBorderColor(BaseColor.WHITE);

				/*
				 * For Receivers Signature
				 */
				PdfPCell cell83 = new PdfPCell(new Paragraph("Receivers Signature", mainContent));
				cell83.setBorderWidth(0.01f);
				cell83.setPaddingBottom(2);
				cell83.setPaddingTop(3);
				cell83.setColspan(2);
				cell83.setPaddingRight(20);
				cell83.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell83.setBorderColor(BaseColor.WHITE);

				table4.addCell(cell57);
				table4.addCell(cell58);
				table4.addCell(cell59);
				table4.addCell(cell60);
				table4.addCell(cell61);
				table4.addCell(cell62);
				table4.addCell(cell63);
				table4.addCell(cell64);
				table4.addCell(cell65);
				table4.addCell(cell74);
				table4.addCell(cell75);
				table4.addCell(cell76);
				table4.addCell(cell77);
				table4.addCell(cell78);
				table4.addCell(cell79);
				table4.addCell(cell80);
				table4.addCell(cell81);
				table4.addCell(cell82);
				table4.addCell(cell83);

			} else {

				/*
				 * For Payment Type
				 */
				PdfPCell cell57 = new PdfPCell(new Paragraph("Payment Type: ", mainContent));
				cell57.setBorderWidth(0.01f);
				cell57.setPaddingBottom(2);
				cell57.setPaddingTop(2);
				cell57.setPaddingLeft(20);
				cell57.setBorderColor(BaseColor.WHITE);

				PdfPCell cell58 = new PdfPCell(new Paragraph(paymentType, mainContent));
				cell58.setBorderWidth(0.01f);
				cell58.setPaddingBottom(2);
				cell58.setPaddingTop(2);
				cell58.setBorderColor(BaseColor.WHITE);

				/*
				 * For TDS Amount
				 */
				PdfPCell cell59 = new PdfPCell(new Paragraph("TDS Amount", mainContent));
				cell59.setBorderWidth(0.01f);
				cell59.setPaddingBottom(2);
				cell59.setPaddingTop(2);
				cell59.setBorderColor(BaseColor.WHITE);

				PdfPCell cell60 = new PdfPCell(new Paragraph("" + tdsAmt, mainContent));
				cell60.setBorderWidth(0.01f);
				cell60.setPaddingBottom(2);
				cell60.setPaddingTop(2);
				cell60.setBorderColor(BaseColor.WHITE);

				// For ref receipt No.
				PdfPCell cell61 = new PdfPCell(
						new Paragraph("Ref. Receipt No. (Cross Matching Referance)", mainContent));
				cell61.setBorderWidth(0.01f);
				cell61.setPaddingBottom(2);
				cell61.setPaddingTop(2);
				cell61.setColspan(3);
				cell61.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell61.setBorderColor(BaseColor.WHITE);

				// For ref receipt No.
				PdfPCell cell62 = new PdfPCell(new Paragraph(refReceiptNo, mainContent));
				cell62.setBorderWidth(0.01f);
				cell62.setPaddingBottom(2);
				cell62.setPaddingTop(2);
				cell62.setBorderColor(BaseColor.WHITE);

				/*
				 * For outstanding paid date
				 */
				PdfPCell cell63 = new PdfPCell(new Paragraph("Outstanding Paid Date", mainContent));
				cell63.setBorderWidth(0.01f);
				cell63.setPaddingBottom(2);
				cell63.setPaddingTop(2);
				cell63.setPaddingLeft(20);
				cell63.setBorderColor(BaseColor.WHITE);

				PdfPCell cell64 = new PdfPCell(new Paragraph(outstandingPaidDate, mainContent));
				cell64.setBorderWidth(0.01f);
				cell64.setPaddingBottom(2);
				cell64.setPaddingTop(2);
				cell64.setBorderColor(BaseColor.WHITE);

				/*
				 * For outstanding paid amount
				 */
				PdfPCell cell65 = new PdfPCell(new Paragraph("Outstanding Paid Amount", mainContent));
				cell65.setBorderWidth(0.01f);
				cell65.setPaddingBottom(2);
				cell65.setPaddingTop(2);
				cell65.setBorderColor(BaseColor.WHITE);

				PdfPCell cell66 = new PdfPCell(new Paragraph("" + outstandingPaidAmt, mainContent));
				cell66.setBorderWidth(0.01f);
				cell66.setPaddingBottom(2);
				cell66.setPaddingTop(2);
				cell66.setBorderColor(BaseColor.WHITE);

				/*
				 * For receipt by
				 */
				PdfPCell cell67 = new PdfPCell(new Paragraph("Receipt Given By", mainContent));
				cell67.setBorderWidth(0.01f);
				cell67.setPaddingBottom(2);
				cell67.setPaddingTop(3);
				cell67.setPaddingLeft(20);
				cell67.setBorderColor(BaseColor.WHITE);

				PdfPCell cell68 = new PdfPCell(new Paragraph(userFullName, mainContent));
				cell68.setBorderWidth(0.01f);
				cell68.setPaddingBottom(2);
				cell68.setPaddingTop(3);
				cell68.setBorderColor(BaseColor.WHITE);

				/*
				 * For Receivers Signature
				 */
				PdfPCell cell69 = new PdfPCell(new Paragraph("Receivers Signature", mainContent));
				cell69.setBorderWidth(0.01f);
				cell69.setPaddingBottom(2);
				cell69.setPaddingTop(3);
				cell69.setColspan(2);
				cell69.setPaddingRight(20);
				cell69.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell69.setBorderColor(BaseColor.WHITE);

				table4.addCell(cell57);
				table4.addCell(cell58);
				table4.addCell(cell59);
				table4.addCell(cell60);
				table4.addCell(cell61);
				table4.addCell(cell62);
				table4.addCell(cell63);
				table4.addCell(cell64);
				table4.addCell(cell65);
				table4.addCell(cell66);
				table4.addCell(cell67);
				table4.addCell(cell68);
				table4.addCell(cell69);

			}

			document.add(table4);

			/*
			 * table for instructions and blood bag details
			 */
			PdfPTable table15 = new PdfPTable(1);
			table15.setWidthPercentage(100);
			Rectangle rec14 = new Rectangle(270, 700);
			table15.setWidthPercentage(new float[] { 270 }, rec14);

			marathiFont.setSize(11f);
			marathiFont.setStyle(Font.BOLD);
			PdfPCell cell571 = new PdfPCell(
					new Paragraph("सूचना [Instructions]: \n-----------------------", marathiFont));
			cell571.setBorderWidthBottom(1.5f);
			cell571.setPaddingBottom(3);
			// cell57.setBorderColorBottom(BaseColor.BLACK);
			cell571.setBorderColor(BaseColor.WHITE);

			marathiFont.setSize(7f);
			marathiFont.setStyle(Font.NORMAL);
			PdfPCell cell5711 = new PdfPCell(new Paragraph(
					"१. मागणी केलेल्या तारखेपासून २४ तासात रक्ताची बॅग न नेल्यास ती दुसऱ्या रुग्णास दिली जाईल."
							+ "[Blood bag is reserved only for 24 hours from the date of demand.] "
							+ "२. अन्न व औषध प्रशासनाच्या नियमानुसार एकदा दिलेली बॅग कोणत्याही कारणास्तव परत घेतली जाणार नाही."
							+ "[On no account the blood bag once issued shall be accepted back as per F.D.A Rule.] "
							+ "३. एक वर्षाच्या आतील सवलत पत्रावर आपल्याला सवलतीच्या दराने रक्त मिळू शकेल."
							+ "[Blood is issued at concessional rate agianst coupon if it is valid.] "
							+ "४. रक्ताची बॅग परगावी नेताना ती बर्फातून नेणे आवश्यक आहे."
							+ "[Kepp the blood bag in ice-box to preserve it for longer time.] "
							+ "५. रक्ताची बॅग दवाखान्यात नेताना फार हिंदकळू देऊ नये."
							+ "[While transporting don't allow the bag to shake vigorously.] "
							+ "६. सोबत दर्शविल्याप्रमाणे डॉक्टरांची चिट्ठी न आणल्यास रक्त दिले जाणार नाही."
							+ "[Blood will not be issued without a note to that effect from your doctor.] "
							+ "७. रुग्णांच्या सोयीसाठी 'कुरियर एजन्सीच्या' माध्यमातून रक्ताचे नमुने, रक्त पिशव्यांची ने-आण करण्याची सुविधा आवश्यकता असल्यास सेवाशुल्क घेऊन पुरवण्यात येईल. त्यात सर्वस्वी कुरियर एजन्सी जबाबदारी असेल.",
					marathiFont));
			cell5711.setBorderWidth(0.01f);
			cell5711.setPaddingBottom(2);
			cell5711.setPaddingTop(0);
			cell5711.setBorderColor(BaseColor.WHITE);

			table15.addCell(cell571);
			table15.addCell(cell5711);

			if (componentCount > 4) {

				document.add(table15);

			} else {

				PdfPCell cell588 = new PdfPCell(new Paragraph("", Font1));
				cell588.setBorderWidth(2f);
				cell588.setColspan(4);
				cell588.setPaddingBottom(3);
				cell588.setBorderColor(BaseColor.WHITE);

				PdfPCell cell58 = new PdfPCell(new Paragraph("", Font1));
				cell58.setPaddingBottom(0);
				cell58.setBorderWidth(2f);
				cell58.setColspan(4);
				cell58.setBorderColor(BaseColor.BLACK);

				table15.addCell(cell588);
				table15.addCell(cell58);

				document.add(table15);
			}

			/*
			 * Checking whether component count is greater than 4, if yes, then
			 * print office copy part onto new page else print office copy part
			 * onto same page
			 */
			if (componentCount > 4) {

				document.newPage();

			}

			/*
			 * For office copy
			 */

			/*
			 * For Receipt type
			 */
			PdfPTable table6 = new PdfPTable(3);
			table6.setWidthPercentage(100);
			Rectangle rect5 = new Rectangle(270, 700);
			table6.setWidthPercentage(new float[] { 30, 90, 150 }, rect5);

			PdfPCell cell000 = new PdfPCell(new Paragraph("", mainContent));
			cell000.setColspan(3);
			cell000.setBorderWidthBottom(1f);
			cell000.setBorderColor(BaseColor.WHITE);

			PdfPCell cell00 = new PdfPCell(new Paragraph("Receipt Type: ", Font1));
			cell00.setPaddingBottom(5);
			cell00.setBorderWidthBottom(1f);
			cell00.setBorderColor(BaseColor.WHITE);

			PdfPCell cell011 = new PdfPCell(new Paragraph(receiptType, mainContent));
			cell011.setPaddingBottom(5);
			cell011.setBorderWidthBottom(1f);
			cell011.setColspan(2);
			cell011.setBorderColor(BaseColor.WHITE);

			PdfPCell cell022 = new PdfPCell(new Paragraph("", mainContent));
			cell022.setPaddingBottom(0);
			cell022.setBorderWidthBottom(1f);
			cell022.setColspan(3);
			cell022.setBorderColorBottom(BaseColor.BLACK);
			cell022.setBorderColor(BaseColor.WHITE);

			table6.addCell(cell000);
			table6.addCell(cell00);
			table6.addCell(cell011);
			table6.addCell(cell022);

			document.add(table6);

			/*
			 * For Blood Bank
			 */
			PdfPTable table7 = new PdfPTable(3);

			table7.setFooterRows(1);
			table7.setWidthPercentage(100);
			Rectangle rect6 = new Rectangle(270, 700);
			table7.setWidthPercentage(new float[] { 90, 90, 90 }, rect6);

			PdfPCell cell59 = new PdfPCell(new Paragraph("", Font1));
			cell59.setPaddingBottom(2);
			cell59.setBorderWidthBottom(1f);
			cell59.setBorderColor(BaseColor.WHITE);

			/*
			 * For Blood Bank Name
			 */
			PdfPCell cell60 = new PdfPCell(new Paragraph(bloodBankName, Font3));
			cell60.setBorderWidth(0.01f);
			cell60.setPaddingBottom(2);
			cell60.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell60.setBorderWidthLeft(0.2f);
			cell60.setBorderColor(BaseColor.WHITE);

			PdfPCell cell61 = new PdfPCell(new Paragraph("Office Copy", Font1));
			cell61.setBorderWidth(0.2f);
			cell61.setPaddingBottom(2);
			cell61.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell61.setBorderColor(BaseColor.WHITE);

			/*
			 * For registration no
			 */
			PdfPCell cell62 = new PdfPCell(new Paragraph("Reg. No.: " + bloodBankregNo, Font1));
			cell62.setBorderWidth(0.01f);
			cell62.setPaddingBottom(3);
			cell62.setBorderWidthLeft(0.2f);
			cell62.setBorderColor(BaseColor.WHITE);

			/*
			 * For address
			 */
			PdfPCell cell63 = new PdfPCell(new Paragraph(bloodBankAddress, Font1));
			cell63.setBorderWidth(0.2f);
			cell63.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell63.setPaddingBottom(3);
			cell63.setBorderColor(BaseColor.WHITE);

			/*
			 * For Phone
			 */
			PdfPCell cell64 = new PdfPCell(new Paragraph("Phone: " + bloodBankPhone, Font1));
			cell64.setBorderWidth(0.01f);
			cell64.setPaddingBottom(3);
			cell64.setBorderWidthLeft(0.2f);
			cell64.setBorderColor(BaseColor.WHITE);

			/*
			 * For border at the bottom
			 */
			PdfPCell cell65 = new PdfPCell(new Paragraph("", Font1));
			cell65.setPaddingBottom(0);
			cell65.setBorderWidthBottom(1.3f);
			cell65.setColspan(3);
			cell65.setBorderColorBottom(BaseColor.BLACK);
			cell65.setBorderColor(BaseColor.WHITE);

			/*
			 * adding all cell to the table to create tabular structure
			 */

			table7.addCell(cell59);
			table7.addCell(cell60);
			table7.addCell(cell61);
			table7.addCell(cell62);
			table7.addCell(cell63);
			table7.addCell(cell64);
			table7.addCell(cell65);

			document.add(table7);

			if (patientIDNo == null || patientIDNo == "") {

				/*
				 * for Patient Details
				 */
				PdfPTable table8 = new PdfPTable(3);
				table8.setWidthPercentage(100);
				Rectangle rect7 = new Rectangle(270, 700);
				table8.setWidthPercentage(new float[] { 130, 70, 70 }, rect7);

				/*
				 * Creating Table header
				 */

				// For blank space
				PdfPCell cell66 = new PdfPCell(new Paragraph("", Font2));
				cell66.setColspan(3);
				cell66.setPadding(0);
				cell66.setPaddingBottom(2);
				cell66.setBorderColor(BaseColor.WHITE);

				// For Receipt Date and time
				PdfPCell cell67 = new PdfPCell(new Paragraph("Receipt Date and Time: " + receiptDate, mainContent));
				cell67.setPaddingBottom(2);
				cell67.setPaddingTop(-1);
				cell67.setBorderWidthRight(0f);
				cell67.setBorderWidthLeft(0f);
				cell67.setBorderWidthTop(0f);
				cell67.setPaddingLeft(20);
				cell67.setBorderWidthBottom(0f);
				cell67.setBorderColor(BaseColor.WHITE);

				// For receipt no.
				PdfPCell cell68 = new PdfPCell(new Paragraph("Receipt No.: " + receiptNo, mainContent));
				cell68.setBorderWidth(0.01f);
				cell68.setPaddingBottom(2);
				cell68.setPaddingTop(-1);
				cell68.setBorderColor(BaseColor.WHITE);

				// For manual receipt no.
				PdfPCell cell681 = new PdfPCell(new Paragraph("Manual Receipt No.: " + manualReceiptNo, mainContent));
				cell681.setBorderWidth(0.01f);
				cell681.setPaddingBottom(2);
				cell681.setPaddingTop(-1);
				cell681.setBorderColor(BaseColor.WHITE);

				// For patient Name
				PdfPCell cell70 = new PdfPCell(
						new Paragraph("Blood Bank Storage Center: " + bloodBagStorageCentre, mainContent));
				cell70.setBorderWidth(0.01f);
				cell70.setPaddingBottom(2);
				cell70.setColspan(3);
				cell70.setPaddingTop(2);
				cell70.setPaddingLeft(20);
				cell70.setBorderColor(BaseColor.WHITE);

				// For bbrNo
				PdfPCell cell17 = new PdfPCell(new Paragraph("", mainContent));
				cell17.setBorderWidth(0.01f);
				cell17.setPaddingBottom(0);
				cell17.setPaddingTop(2);
				cell17.setPaddingLeft(20);
				cell17.setPaddingBottom(2);
				cell17.setBorderColor(BaseColor.WHITE);

				// For patientID barcode
				PdfPCell cell144 = new PdfPCell(new Paragraph("", mainContent));
				cell144.setBorderWidth(0.01f);
				cell144.setPaddingBottom(7);
				cell144.setPaddingTop(2);
				cell144.setColspan(2);
				cell144.setBorderColor(BaseColor.WHITE);

				table8.addCell(cell66);
				table8.addCell(cell67);
				table8.addCell(cell68);
				table8.addCell(cell681);
				table8.addCell(cell70);
				table8.addCell(cell17);
				table8.addCell(cell144);

				document.add(table8);

			} else if (patientIDNo.isEmpty()) {

				/*
				 * for Patient Details
				 */
				PdfPTable table8 = new PdfPTable(3);
				table8.setWidthPercentage(100);
				Rectangle rect7 = new Rectangle(270, 700);
				table8.setWidthPercentage(new float[] { 130, 70, 70 }, rect7);

				/*
				 * Creating Table header
				 */

				// For blank space
				PdfPCell cell66 = new PdfPCell(new Paragraph("", Font2));
				cell66.setColspan(3);
				cell66.setPadding(0);
				cell66.setPaddingBottom(2);
				cell66.setBorderColor(BaseColor.WHITE);

				// For Receipt Date and time
				PdfPCell cell67 = new PdfPCell(new Paragraph("Receipt Date and Time: " + receiptDate, mainContent));
				cell67.setPaddingBottom(2);
				cell67.setPaddingTop(-1);
				cell67.setBorderWidthRight(0f);
				cell67.setBorderWidthLeft(0f);
				cell67.setBorderWidthTop(0f);
				cell67.setPaddingLeft(20);
				cell67.setBorderWidthBottom(0f);
				cell67.setBorderColor(BaseColor.WHITE);

				// For receipt no.
				PdfPCell cell68 = new PdfPCell(new Paragraph("Receipt No.: " + receiptNo, mainContent));
				cell68.setBorderWidth(0.01f);
				cell68.setPaddingBottom(2);
				cell68.setPaddingTop(-1);
				cell68.setBorderColor(BaseColor.WHITE);

				// For manual receipt no.
				PdfPCell cell681 = new PdfPCell(new Paragraph("Manual Receipt No.: " + manualReceiptNo, mainContent));
				cell681.setBorderWidth(0.01f);
				cell681.setPaddingBottom(2);
				cell681.setPaddingTop(-1);
				cell681.setBorderColor(BaseColor.WHITE);

				// For patient Name
				PdfPCell cell70 = new PdfPCell(
						new Paragraph("Blood Bank Storage Center: " + bloodBagStorageCentre, mainContent));
				cell70.setBorderWidth(0.01f);
				cell70.setPaddingBottom(2);
				cell70.setColspan(3);
				cell70.setPaddingTop(2);
				cell70.setPaddingLeft(20);
				cell70.setBorderColor(BaseColor.WHITE);

				// For bbrNo
				PdfPCell cell17 = new PdfPCell(new Paragraph("", mainContent));
				cell17.setBorderWidth(0.01f);
				cell17.setPaddingBottom(0);
				cell17.setPaddingTop(2);
				cell17.setPaddingLeft(20);
				cell17.setPaddingBottom(2);
				cell17.setBorderColor(BaseColor.WHITE);

				// For patientID barcode
				PdfPCell cell144 = new PdfPCell(new Paragraph("", mainContent));
				cell144.setBorderWidth(0.01f);
				cell144.setPaddingBottom(7);
				cell144.setPaddingTop(2);
				cell144.setColspan(2);
				cell144.setBorderColor(BaseColor.WHITE);

				table8.addCell(cell66);
				table8.addCell(cell67);
				table8.addCell(cell68);
				table8.addCell(cell681);
				table8.addCell(cell70);
				table8.addCell(cell17);
				table8.addCell(cell144);

				document.add(table8);

			} else {

				/*
				 * for Patient Details
				 */
				PdfPTable table8 = new PdfPTable(3);
				table8.setWidthPercentage(100);
				Rectangle rect7 = new Rectangle(270, 700);
				table8.setWidthPercentage(new float[] { 130, 70, 70 }, rect7);

				/*
				 * Creating Table header
				 */

				// For blank space
				PdfPCell cell66 = new PdfPCell(new Paragraph("", Font2));
				cell66.setColspan(3);
				cell66.setPadding(0);
				cell66.setPaddingBottom(2);
				cell66.setBorderColor(BaseColor.WHITE);

				// For Receipt Date and time
				PdfPCell cell67 = new PdfPCell(new Paragraph("Receipt Date and Time: " + receiptDate, mainContent));
				cell67.setPaddingBottom(2);
				cell67.setPaddingTop(-1);
				cell67.setBorderWidthRight(0f);
				cell67.setBorderWidthLeft(0f);
				cell67.setBorderWidthTop(0f);
				cell67.setPaddingLeft(20);
				cell67.setBorderWidthBottom(0f);
				cell67.setBorderColor(BaseColor.WHITE);

				// For receipt no.
				PdfPCell cell68 = new PdfPCell(new Paragraph("Receipt No.: " + receiptNo, mainContent));
				cell68.setBorderWidth(0.01f);
				cell68.setPaddingBottom(2);
				cell68.setPaddingTop(-1);
				cell68.setBorderColor(BaseColor.WHITE);

				// For manual receipt no.
				PdfPCell cell681 = new PdfPCell(new Paragraph("Manual Receipt No.: " + manualReceiptNo, mainContent));
				cell681.setBorderWidth(0.01f);
				cell681.setPaddingBottom(2);
				cell681.setPaddingTop(-1);
				cell681.setBorderColor(BaseColor.WHITE);

				// For patient Name
				PdfPCell cell70 = new PdfPCell(new Paragraph("Patient Name: " + patientName, mainContent));
				cell70.setBorderWidth(0.01f);
				cell70.setPaddingBottom(2);
				cell70.setPaddingTop(2);
				cell70.setPaddingLeft(20);
				cell70.setBorderColor(BaseColor.WHITE);

				// For patient identifiers no
				PdfPCell cell71 = new PdfPCell(new Paragraph("Patient ID: " + patientIDNO, mainContent));
				cell71.setBorderWidth(0.01f);
				cell71.setPaddingBottom(2);
				cell71.setPaddingTop(2);
				cell71.setBorderColor(BaseColor.WHITE);

				// For patient id barcode
				PdfPCell cell72 = new PdfPCell(patientIDImage);
				cell72.setBorderWidth(0.01f);
				cell72.setPaddingBottom(2);
				cell72.setPaddingTop(2);
				cell72.setBorderColor(BaseColor.WHITE);

				// For hospital
				PdfPCell cell73 = new PdfPCell(new Paragraph("Hospital: " + hospital, mainContent));
				cell73.setBorderWidth(0.01f);
				cell73.setPaddingBottom(2);
				cell73.setPaddingTop(2);
				cell73.setPaddingTop(-7);
				cell73.setColspan(3);
				cell73.setPaddingLeft(20);
				cell73.setBorderColor(BaseColor.WHITE);

				// For address
				PdfPCell cell74 = new PdfPCell(new Paragraph("Address: " + address, mainContent));
				cell74.setBorderWidth(0.01f);
				cell74.setPaddingBottom(0);
				cell74.setPaddingTop(2);
				cell74.setPaddingLeft(20);
				cell74.setPaddingBottom(7);
				cell74.setBorderColor(BaseColor.WHITE);

				// For bbrNo
				PdfPCell cell75 = new PdfPCell(new Paragraph("", mainContent));
				cell75.setBorderWidth(0.01f);
				cell75.setPaddingBottom(0);
				cell75.setPaddingTop(2);
				cell75.setPaddingBottom(7);
				cell75.setBorderColor(BaseColor.WHITE);

				// For patient id barcode
				PdfPCell cell721 = new PdfPCell(new Paragraph("", mainContent));
				cell721.setBorderWidth(0.01f);
				cell721.setPaddingBottom(7);
				cell721.setPaddingTop(2);
				cell721.setBorderColor(BaseColor.WHITE);

				// For Charity case no
				PdfPCell cell7212 = new PdfPCell(new Paragraph("Mobile No.: " + mobile, mainContent));
				cell7212.setBorderWidth(0.01f);
				cell7212.setPaddingBottom(2);
				cell7212.setPaddingTop(-7);
				cell7212.setBorderColor(BaseColor.WHITE);
				cell7212.setPaddingLeft(20);

				// For Charity case no
				PdfPCell cell7211 = new PdfPCell(new Paragraph("Charity Case No.: " + charityCaseNo, mainContent));
				cell7211.setBorderWidth(0.01f);
				cell7211.setPaddingBottom(2);
				cell7211.setPaddingTop(-7);
				cell7211.setBorderColor(BaseColor.WHITE);
				cell7211.setColspan(2);

				table8.addCell(cell66);
				table8.addCell(cell67);
				table8.addCell(cell68);
				table8.addCell(cell681);
				table8.addCell(cell70);
				table8.addCell(cell71);
				table8.addCell(cell72);
				table8.addCell(cell73);
				table8.addCell(cell74);
				table8.addCell(cell75);
				table8.addCell(cell721);
				table8.addCell(cell7212);
				table8.addCell(cell7211);

				document.add(table8);

			}

			/*
			 * For Component details table
			 */
			PdfPTable table9 = new PdfPTable(5);
			table9.setWidthPercentage(100);
			Rectangle rect8 = new Rectangle(310, 700);
			table9.setWidthPercentage(new float[] { 20, 135, 40, 45, 45 }, rect8);

			/*
			 * For blank space
			 */
			PdfPCell cell76 = new PdfPCell(new Paragraph("", Font2));
			cell76.setColspan(5);
			cell76.setBorderColor(BaseColor.WHITE);

			/*
			 * Creating header titles for table
			 */
			// For Sr.No.
			PdfPCell cell77 = new PdfPCell(new Paragraph("Sr.No.", Font1));
			cell77.setBorderWidth(0.01f);
			cell77.setPaddingBottom(3);
			cell77.setBorderColor(BaseColor.GRAY);

			// For Product req.
			PdfPCell cell78 = new PdfPCell(new Paragraph("Product Req.", Font1));
			cell78.setBorderWidth(0.01f);
			cell78.setPaddingBottom(3);
			cell78.setBorderColor(BaseColor.GRAY);

			// For Quantity
			PdfPCell cell80 = new PdfPCell(new Paragraph("Quantity", Font1));
			cell80.setBorderWidth(0.01f);
			cell80.setPaddingBottom(3);
			cell80.setBorderColor(BaseColor.GRAY);

			// For Rate
			PdfPCell cell81 = new PdfPCell(new Paragraph("Rate", Font1));
			cell81.setBorderWidth(0.01f);
			cell81.setPaddingBottom(3);
			cell81.setBorderColor(BaseColor.GRAY);

			// For Amount
			PdfPCell cell82 = new PdfPCell(new Paragraph("Amount", Font1));
			cell82.setBorderWidth(0.01f);
			cell82.setPaddingBottom(3);
			cell82.setBorderColor(BaseColor.GRAY);

			table9.addCell(cell76);
			table9.addCell(cell77);
			table9.addCell(cell78);
			table9.addCell(cell80);
			table9.addCell(cell81);
			table9.addCell(cell82);

			srNo = 1;

			/*
			 * For Receipt items values
			 */
			while (resultSet7.next()) {

				// For Sr.No.
				PdfPCell cell37 = new PdfPCell(new Paragraph("" + srNo, mainContent));
				cell37.setBorderWidth(0.01f);
				cell37.setPaddingBottom(3);
				cell37.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell37.setBorderColor(BaseColor.GRAY);

				// For Product req.
				PdfPCell cell38 = new PdfPCell(new Paragraph(resultSet7.getString("product"), mainContent));
				cell38.setBorderWidth(0.01f);
				cell38.setPaddingBottom(3);
				cell38.setBorderColor(BaseColor.GRAY);

				int receiptItemID = resultSet7.getInt("id");

				// For Bag No.
				PdfPCell cell40 = new PdfPCell(new Paragraph("" + resultSet7.getInt("quantity"), mainContent));
				cell40.setBorderWidth(0.01f);
				cell40.setPaddingBottom(3);
				cell40.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell40.setBorderColor(BaseColor.GRAY);

				// For Rate
				PdfPCell cell41 = new PdfPCell(new Paragraph("" + resultSet7.getDouble("rate"), mainContent));
				cell41.setBorderWidth(0.01f);
				cell41.setPaddingBottom(3);
				cell41.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell41.setBorderColor(BaseColor.GRAY);

				// For Amount
				PdfPCell cell42 = new PdfPCell(new Paragraph("" + resultSet7.getDouble("amount"), mainContent));
				cell42.setBorderWidth(0.01f);
				cell42.setPaddingBottom(3);
				cell42.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell42.setBorderColor(BaseColor.GRAY);

				srNo++;

				table9.addCell(cell37);
				table9.addCell(cell38);
				table9.addCell(cell40);
				table9.addCell(cell41);
				table9.addCell(cell42);

			}

			/*
			 * For total
			 */
			PdfPCell cell83 = new PdfPCell(new Paragraph("Total Amount", Font1));
			cell83.setBorderWidth(0.01f);
			cell83.setPaddingBottom(3);
			cell83.setColspan(4);
			cell83.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell83.setBorderColor(BaseColor.GRAY);

			PdfPCell cell84 = new PdfPCell(new Paragraph("" + totalAmt, mainContent));
			cell84.setBorderWidth(0.01f);
			cell84.setPaddingBottom(3);
			cell84.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell84.setBorderColor(BaseColor.GRAY);

			table9.addCell(cell83);
			table9.addCell(cell84);

			/*
			 * For Other charges
			 */
			while (resultSet8.next()) {

				PdfPCell cell45 = new PdfPCell(new Paragraph("Other Charges", Font1));
				cell45.setBorderWidth(0.01f);
				cell45.setPaddingBottom(3);
				cell45.setColspan(3);
				cell45.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell45.setBorderColor(BaseColor.GRAY);

				PdfPCell cell46 = new PdfPCell(new Paragraph(resultSet8.getString("chargeType"), mainContent));
				cell46.setBorderWidth(0.01f);
				cell46.setPaddingBottom(3);
				cell46.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell46.setBorderColor(BaseColor.GRAY);

				PdfPCell cell47 = new PdfPCell(new Paragraph("" + resultSet8.getDouble("otherCharges"), mainContent));
				cell47.setBorderWidth(0.01f);
				cell47.setPaddingBottom(3);
				cell47.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell47.setBorderColor(BaseColor.GRAY);

				table9.addCell(cell45);
				table9.addCell(cell46);
				table9.addCell(cell47);

			}

			PdfPCell cell85 = null;
			PdfPCell cell86 = null;
			PdfPCell cell87 = null;

			if (!concession.equals("000")) {

				/*
				 * For Concession
				 */
				cell85 = new PdfPCell(new Paragraph("Concession", Font1));
				cell85.setBorderWidth(0.01f);
				cell85.setPaddingBottom(3);
				cell85.setColspan(3);
				cell85.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell85.setBorderColor(BaseColor.GRAY);

				cell86 = new PdfPCell(new Paragraph(concession, mainContent));
				cell86.setBorderWidth(0.01f);
				cell86.setPaddingBottom(3);
				cell86.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell86.setBorderColor(BaseColor.GRAY);

				cell87 = new PdfPCell(new Paragraph("" + concessionAmt, mainContent));
				cell87.setBorderWidth(0.01f);
				cell87.setPaddingBottom(3);
				cell87.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell87.setBorderColor(BaseColor.GRAY);

			}

			/*
			 * For Net receivable amount
			 */
			PdfPCell cell88 = new PdfPCell(new Paragraph("Net Receivable Amount", Font1));
			cell88.setBorderWidth(0.01f);
			cell88.setPaddingBottom(3);
			cell88.setColspan(4);
			cell88.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell88.setBorderColor(BaseColor.GRAY);

			PdfPCell cell89 = new PdfPCell(new Paragraph("" + netReceivableAmt, mainContent));
			cell89.setBorderWidth(0.01f);
			cell89.setPaddingBottom(3);
			cell89.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell89.setBorderColor(BaseColor.GRAY);

			/*
			 * For actual payment
			 */
			PdfPCell cell90 = new PdfPCell(new Paragraph("Actual Payment", Font1));
			cell90.setBorderWidth(0.01f);
			cell90.setPaddingBottom(3);
			cell90.setColspan(4);
			cell90.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell90.setBorderColor(BaseColor.GRAY);

			PdfPCell cell91 = new PdfPCell(new Paragraph("" + actualPayment, mainContent));
			cell91.setBorderWidth(0.01f);
			cell91.setPaddingBottom(3);
			cell91.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell91.setBorderColor(BaseColor.GRAY);

			/*
			 * For outstanding amount
			 */
			PdfPCell cell92 = new PdfPCell(new Paragraph("Outstanding Amount", Font1));
			cell92.setBorderWidth(0.01f);
			cell92.setPaddingBottom(3);
			cell92.setColspan(4);
			cell92.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell92.setBorderWidthBottom(0.9f);
			cell92.setBorderColor(BaseColor.GRAY);

			PdfPCell cell93 = new PdfPCell(new Paragraph("" + outstandingAmt, mainContent));
			cell93.setBorderWidth(0.01f);
			cell93.setPaddingBottom(3);
			cell93.setBorderWidthBottom(0.9f);
			cell93.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell93.setBorderColor(BaseColor.GRAY);

			if (!concession.equals("000")) {

				table9.addCell(cell85);
				table9.addCell(cell86);
				table9.addCell(cell87);

			}
			table9.addCell(cell88);
			table9.addCell(cell89);
			table9.addCell(cell90);
			table9.addCell(cell91);
			table9.addCell(cell92);
			table9.addCell(cell93);

			document.add(table9);

			/*
			 * For remaining details
			 */
			PdfPTable table10 = new PdfPTable(4);
			table10.setWidthPercentage(100);
			Rectangle rect9 = new Rectangle(270, 700);
			table10.setWidthPercentage(new float[] { 65, 70, 65, 70 }, rect9);

			/*
			 * Checking whether payment type is cash or cheque, if its cheque
			 * then display cheque details on PDF else don't
			 */
			if (paymentType.equals("Cheque")) {

				/*
				 * For Payment Type
				 */
				PdfPCell cell94 = new PdfPCell(new Paragraph("Payment Type: ", mainContent));
				cell94.setBorderWidth(0.01f);
				cell94.setPaddingBottom(2);
				cell94.setPaddingTop(2);
				cell94.setPaddingLeft(20);
				cell94.setBorderColor(BaseColor.WHITE);

				PdfPCell cell95 = new PdfPCell(new Paragraph(paymentType, mainContent));
				cell95.setBorderWidth(0.01f);
				cell95.setPaddingBottom(2);
				cell95.setPaddingTop(2);
				cell95.setBorderColor(BaseColor.WHITE);

				/*
				 * For TDS Amount
				 */
				PdfPCell cell96 = new PdfPCell(new Paragraph("TDS Amount", mainContent));
				cell96.setBorderWidth(0.01f);
				cell96.setPaddingBottom(2);
				cell96.setPaddingTop(2);
				cell96.setBorderColor(BaseColor.WHITE);

				PdfPCell cell97 = new PdfPCell(new Paragraph("" + tdsAmt, mainContent));
				cell97.setBorderWidth(0.01f);
				cell97.setPaddingBottom(2);
				cell97.setPaddingTop(2);
				cell97.setBorderColor(BaseColor.WHITE);

				PdfPCell cell98 = new PdfPCell(new Paragraph("", mainContent));
				cell98.setBorderWidth(0.01f);
				cell98.setColspan(4);
				cell98.setBorderWidthBottom(0.5f);
				cell98.setBorderColorBottom(BaseColor.GRAY);
				cell98.setBorderColor(BaseColor.WHITE);

				/*
				 * For cheque details
				 */
				// For cheque Issued By
				PdfPCell cell99 = new PdfPCell(new Paragraph("Cheque Issued By", mainContent));
				cell99.setBorderWidth(0.01f);
				cell99.setPaddingBottom(2);
				cell99.setPaddingTop(2);
				cell99.setPaddingLeft(20);
				cell99.setBorderColor(BaseColor.WHITE);

				PdfPCell cell100 = new PdfPCell(new Paragraph(chequeIssuedBy, mainContent));
				cell100.setBorderWidth(0.01f);
				cell100.setPaddingBottom(3);
				cell100.setPaddingTop(3);
				cell100.setBorderColor(BaseColor.WHITE);

				// For cheque No
				PdfPCell cell101 = new PdfPCell(new Paragraph("Cheque No.", mainContent));
				cell101.setBorderWidth(0.01f);
				cell101.setPaddingBottom(2);
				cell101.setPaddingTop(2);
				cell101.setBorderColor(BaseColor.WHITE);

				PdfPCell cell102 = new PdfPCell(new Paragraph(chequNo, mainContent));
				cell102.setBorderWidth(0.01f);
				cell102.setPaddingBottom(2);
				cell102.setPaddingTop(2);
				cell102.setBorderColor(BaseColor.WHITE);

				// For cheque Bank Name
				PdfPCell cell103 = new PdfPCell(new Paragraph("Bank Name", mainContent));
				cell103.setBorderWidth(0.01f);
				cell103.setPaddingBottom(2);
				cell103.setPaddingTop(2);
				cell103.setPaddingLeft(20);
				cell103.setBorderColor(BaseColor.WHITE);

				PdfPCell cell104 = new PdfPCell(new Paragraph(chequBankName, mainContent));
				cell104.setBorderWidth(0.01f);
				cell104.setPaddingBottom(3);
				cell104.setPaddingTop(3);
				cell104.setBorderColor(BaseColor.WHITE);

				// For cheque Bank Branch
				PdfPCell cell105 = new PdfPCell(new Paragraph("Branch", mainContent));
				cell105.setBorderWidth(0.01f);
				cell105.setPaddingBottom(2);
				cell105.setPaddingTop(2);
				cell105.setBorderColor(BaseColor.WHITE);

				PdfPCell cell106 = new PdfPCell(new Paragraph(chequeBankBranch, mainContent));
				cell106.setBorderWidth(0.01f);
				cell106.setPaddingBottom(2);
				cell106.setPaddingTop(2);
				cell106.setBorderColor(BaseColor.WHITE);

				// For cheque Date
				PdfPCell cell107 = new PdfPCell(new Paragraph("Date", mainContent));
				cell107.setBorderWidth(0.01f);
				cell107.setPaddingBottom(2);
				cell107.setPaddingTop(2);
				cell107.setPaddingLeft(20);
				cell107.setBorderColor(BaseColor.WHITE);

				PdfPCell cell108 = new PdfPCell(new Paragraph(chequDate, mainContent));
				cell108.setBorderWidth(0.01f);
				cell108.setPaddingBottom(2);
				cell108.setPaddingTop(2);
				cell108.setBorderColor(BaseColor.WHITE);

				// For cheque amount
				PdfPCell cell109 = new PdfPCell(new Paragraph("Amount", mainContent));
				cell109.setBorderWidth(0.01f);
				cell109.setPaddingBottom(2);
				cell109.setPaddingTop(2);
				cell109.setBorderColor(BaseColor.WHITE);

				PdfPCell cell110 = new PdfPCell(new Paragraph("" + chequeAmt, mainContent));
				cell110.setBorderWidth(0.01f);
				cell110.setPaddingBottom(2);
				cell110.setPaddingTop(2);
				cell110.setBorderColor(BaseColor.WHITE);

				PdfPCell cell111 = new PdfPCell(new Paragraph("", mainContent));
				cell111.setBorderWidth(0.01f);
				cell111.setColspan(4);
				cell111.setBorderWidthBottom(0.5f);
				cell111.setBorderColorBottom(BaseColor.GRAY);
				cell111.setBorderColor(BaseColor.WHITE);

				// For ref receipt No.
				PdfPCell cell112 = new PdfPCell(
						new Paragraph("Ref. Receipt No. (Cross Matching Referance)", mainContent));
				cell112.setBorderWidth(0.01f);
				cell112.setPaddingBottom(2);
				cell112.setPaddingTop(2);
				cell112.setColspan(3);
				cell112.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell112.setBorderColor(BaseColor.WHITE);

				// For ref receipt No.
				PdfPCell cell113 = new PdfPCell(new Paragraph(refReceiptNo, mainContent));
				cell113.setBorderWidth(0.01f);
				cell113.setPaddingBottom(2);
				cell113.setPaddingTop(2);
				cell113.setBorderColor(BaseColor.WHITE);

				/*
				 * For outstanding paid date
				 */
				PdfPCell cell114 = new PdfPCell(new Paragraph("Outstanding Paid Date", mainContent));
				cell114.setBorderWidth(0.01f);
				cell114.setPaddingBottom(2);
				cell114.setPaddingTop(2);
				cell114.setPaddingLeft(20);
				cell114.setBorderColor(BaseColor.WHITE);

				PdfPCell cell115 = new PdfPCell(new Paragraph(outstandingPaidDate, mainContent));
				cell115.setBorderWidth(0.01f);
				cell115.setPaddingBottom(2);
				cell115.setPaddingTop(2);
				cell115.setBorderColor(BaseColor.WHITE);

				/*
				 * For outstanding paid amount
				 */
				PdfPCell cell116 = new PdfPCell(new Paragraph("Outstanding Paid Amount", mainContent));
				cell116.setBorderWidth(0.01f);
				cell116.setPaddingBottom(2);
				cell116.setPaddingTop(2);
				cell116.setBorderColor(BaseColor.WHITE);

				PdfPCell cell117 = new PdfPCell(new Paragraph("" + outstandingPaidAmt, mainContent));
				cell117.setBorderWidth(0.01f);
				cell117.setPaddingBottom(2);
				cell117.setPaddingTop(2);
				cell117.setBorderColor(BaseColor.WHITE);

				/*
				 * For receipt by
				 */
				PdfPCell cell118 = new PdfPCell(new Paragraph("Receipt Given By", mainContent));
				cell118.setBorderWidth(0.01f);
				cell118.setPaddingBottom(2);
				cell118.setPaddingTop(3);
				cell118.setPaddingLeft(20);
				cell118.setBorderColor(BaseColor.WHITE);

				PdfPCell cell119 = new PdfPCell(new Paragraph(userFullName, mainContent));
				cell119.setBorderWidth(0.01f);
				cell119.setPaddingBottom(2);
				cell119.setPaddingTop(3);
				cell119.setBorderColor(BaseColor.WHITE);

				/*
				 * For Receivers Signature
				 */
				PdfPCell cell120 = new PdfPCell(new Paragraph("Receivers Signature", mainContent));
				cell120.setBorderWidth(0.01f);
				cell120.setPaddingBottom(2);
				cell120.setPaddingTop(3);
				cell120.setColspan(2);
				cell120.setPaddingRight(20);
				cell120.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell120.setBorderColor(BaseColor.WHITE);

				table10.addCell(cell94);
				table10.addCell(cell95);
				table10.addCell(cell96);
				table10.addCell(cell97);
				table10.addCell(cell98);
				table10.addCell(cell99);
				table10.addCell(cell100);
				table10.addCell(cell101);
				table10.addCell(cell102);
				table10.addCell(cell103);
				table10.addCell(cell104);
				table10.addCell(cell105);
				table10.addCell(cell106);
				table10.addCell(cell107);
				table10.addCell(cell108);
				table10.addCell(cell109);
				table10.addCell(cell110);
				table10.addCell(cell111);
				table10.addCell(cell112);
				table10.addCell(cell113);
				table10.addCell(cell114);
				table10.addCell(cell115);
				table10.addCell(cell116);
				table10.addCell(cell117);
				table10.addCell(cell118);
				table10.addCell(cell119);
				table10.addCell(cell120);

			} else if (paymentType.equals("Credit/Debit Card")) {

				/*
				 * For Payment Type
				 */
				PdfPCell cell57 = new PdfPCell(new Paragraph("Payment Type: ", mainContent));
				cell57.setBorderWidth(0.01f);
				cell57.setPaddingBottom(2);
				cell57.setPaddingTop(2);
				cell57.setPaddingLeft(20);
				cell57.setBorderColor(BaseColor.WHITE);

				PdfPCell cell58 = new PdfPCell(new Paragraph(paymentType, mainContent));
				cell58.setBorderWidth(0.01f);
				cell58.setPaddingBottom(2);
				cell58.setPaddingTop(2);
				cell58.setBorderColor(BaseColor.WHITE);

				/*
				 * For TDS Amount
				 */
				PdfPCell cell591 = new PdfPCell(new Paragraph("TDS Amount", mainContent));
				cell591.setBorderWidth(0.01f);
				cell591.setPaddingBottom(2);
				cell591.setPaddingTop(2);
				cell591.setBorderColor(BaseColor.WHITE);

				PdfPCell cell601 = new PdfPCell(new Paragraph("" + tdsAmt, mainContent));
				cell601.setBorderWidth(0.01f);
				cell601.setPaddingBottom(2);
				cell601.setPaddingTop(2);
				cell601.setBorderColor(BaseColor.WHITE);

				PdfPCell cell611 = new PdfPCell(new Paragraph("", mainContent));
				cell611.setBorderWidth(0.01f);
				cell611.setColspan(4);
				cell611.setBorderWidthBottom(0.5f);
				cell611.setBorderColorBottom(BaseColor.GRAY);
				cell611.setBorderColor(BaseColor.WHITE);

				/*
				 * For credit/debit card details
				 */
				// For card mobile no
				PdfPCell cell621 = new PdfPCell(new Paragraph("Mobile No.", mainContent));
				cell621.setBorderWidth(0.01f);
				cell621.setPaddingBottom(2);
				cell621.setPaddingTop(2);
				cell621.setPaddingLeft(20);
				cell621.setBorderColor(BaseColor.WHITE);

				PdfPCell cell631 = new PdfPCell(new Paragraph(cardMobileNo, mainContent));
				cell631.setBorderWidth(0.01f);
				cell631.setPaddingBottom(2);
				cell631.setPaddingTop(2);
				cell631.setBorderColor(BaseColor.WHITE);

				// For blank
				PdfPCell cell641 = new PdfPCell(new Paragraph("", mainContent));
				cell641.setBorderWidth(0.01f);
				cell641.setPaddingBottom(2);
				cell641.setPaddingTop(2);
				cell641.setBorderColor(BaseColor.WHITE);

				PdfPCell cell651 = new PdfPCell(new Paragraph("", mainContent));
				cell651.setBorderWidth(0.01f);
				cell651.setPaddingBottom(2);
				cell651.setPaddingTop(2);
				cell651.setBorderColor(BaseColor.WHITE);

				PdfPCell cell74 = new PdfPCell(new Paragraph("", mainContent));
				cell74.setBorderWidth(0.01f);
				cell74.setColspan(4);
				cell74.setBorderWidthBottom(0.5f);
				cell74.setBorderColorBottom(BaseColor.GRAY);
				cell74.setBorderColor(BaseColor.WHITE);

				// For ref receipt No.
				PdfPCell cell75 = new PdfPCell(
						new Paragraph("Ref. Receipt No. (Cross Matching Referance)", mainContent));
				cell75.setBorderWidth(0.01f);
				cell75.setPaddingBottom(2);
				cell75.setPaddingTop(2);
				cell75.setColspan(3);
				cell75.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell75.setBorderColor(BaseColor.WHITE);

				// For ref receipt No.
				PdfPCell cell761 = new PdfPCell(new Paragraph(refReceiptNo, mainContent));
				cell761.setBorderWidth(0.01f);
				cell761.setPaddingBottom(2);
				cell761.setPaddingTop(2);
				cell761.setBorderColor(BaseColor.WHITE);

				/*
				 * For outstanding paid date
				 */
				PdfPCell cell771 = new PdfPCell(new Paragraph("Outstanding Paid Date", mainContent));
				cell771.setBorderWidth(0.01f);
				cell771.setPaddingBottom(2);
				cell771.setPaddingTop(2);
				cell771.setPaddingLeft(20);
				cell771.setBorderColor(BaseColor.WHITE);

				PdfPCell cell781 = new PdfPCell(new Paragraph(outstandingPaidDate, mainContent));
				cell781.setBorderWidth(0.01f);
				cell781.setPaddingBottom(2);
				cell781.setPaddingTop(2);
				cell781.setBorderColor(BaseColor.WHITE);

				/*
				 * For outstanding paid amount
				 */
				PdfPCell cell79 = new PdfPCell(new Paragraph("Outstanding Paid Amount", mainContent));
				cell79.setBorderWidth(0.01f);
				cell79.setPaddingBottom(2);
				cell79.setPaddingTop(2);
				cell79.setBorderColor(BaseColor.WHITE);

				PdfPCell cell801 = new PdfPCell(new Paragraph("" + outstandingPaidAmt, mainContent));
				cell801.setBorderWidth(0.01f);
				cell801.setPaddingBottom(2);
				cell801.setPaddingTop(2);
				cell801.setBorderColor(BaseColor.WHITE);

				/*
				 * For receipt by
				 */
				PdfPCell cell811 = new PdfPCell(new Paragraph("Receipt Given By", mainContent));
				cell811.setBorderWidth(0.01f);
				cell811.setPaddingBottom(2);
				cell811.setPaddingTop(3);
				cell811.setPaddingLeft(20);
				cell811.setBorderColor(BaseColor.WHITE);

				PdfPCell cell821 = new PdfPCell(new Paragraph(userFullName, mainContent));
				cell821.setBorderWidth(0.01f);
				cell821.setPaddingBottom(2);
				cell821.setPaddingTop(3);
				cell821.setBorderColor(BaseColor.WHITE);

				/*
				 * For Receivers Signature
				 */
				PdfPCell cell831 = new PdfPCell(new Paragraph("Receivers Signature", mainContent));
				cell831.setBorderWidth(0.01f);
				cell831.setPaddingBottom(2);
				cell831.setPaddingTop(3);
				cell831.setColspan(2);
				cell831.setPaddingRight(20);
				cell831.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell831.setBorderColor(BaseColor.WHITE);

				table10.addCell(cell57);
				table10.addCell(cell58);
				table10.addCell(cell591);
				table10.addCell(cell601);
				table10.addCell(cell611);
				table10.addCell(cell621);
				table10.addCell(cell631);
				table10.addCell(cell641);
				table10.addCell(cell651);
				table10.addCell(cell74);
				table10.addCell(cell75);
				table10.addCell(cell761);
				table10.addCell(cell771);
				table10.addCell(cell781);
				table10.addCell(cell79);
				table10.addCell(cell801);
				table10.addCell(cell811);
				table10.addCell(cell821);
				table10.addCell(cell831);

			} else {

				/*
				 * For Payment Type
				 */
				PdfPCell cell94 = new PdfPCell(new Paragraph("Payment Type: ", mainContent));
				cell94.setBorderWidth(0.01f);
				cell94.setPaddingBottom(2);
				cell94.setPaddingTop(2);
				cell94.setPaddingLeft(20);
				cell94.setBorderColor(BaseColor.WHITE);

				PdfPCell cell95 = new PdfPCell(new Paragraph(paymentType, mainContent));
				cell95.setBorderWidth(0.01f);
				cell95.setPaddingBottom(2);
				cell95.setPaddingTop(2);
				cell95.setBorderColor(BaseColor.WHITE);

				/*
				 * For TDS Amount
				 */
				PdfPCell cell96 = new PdfPCell(new Paragraph("TDS Amount", mainContent));
				cell96.setBorderWidth(0.01f);
				cell96.setPaddingBottom(2);
				cell96.setPaddingTop(2);
				cell96.setBorderColor(BaseColor.WHITE);

				PdfPCell cell97 = new PdfPCell(new Paragraph("" + tdsAmt, mainContent));
				cell97.setBorderWidth(0.01f);
				cell97.setPaddingBottom(2);
				cell97.setPaddingTop(2);
				cell97.setBorderColor(BaseColor.WHITE);

				// For ref receipt No.
				PdfPCell cell98 = new PdfPCell(
						new Paragraph("Ref. Receipt No. (Cross Matching Referance)", mainContent));
				cell98.setBorderWidth(0.01f);
				cell98.setPaddingBottom(2);
				cell98.setPaddingTop(2);
				cell98.setColspan(3);
				cell98.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell98.setBorderColor(BaseColor.WHITE);

				// For ref receipt No.
				PdfPCell cell99 = new PdfPCell(new Paragraph(refReceiptNo, mainContent));
				cell99.setBorderWidth(0.01f);
				cell99.setPaddingBottom(2);
				cell99.setPaddingTop(2);
				cell99.setBorderColor(BaseColor.WHITE);

				/*
				 * For outstanding paid date
				 */
				PdfPCell cell100 = new PdfPCell(new Paragraph("Outstanding Paid Date", mainContent));
				cell100.setBorderWidth(0.01f);
				cell100.setPaddingBottom(2);
				cell100.setPaddingTop(2);
				cell100.setPaddingLeft(20);
				cell100.setBorderColor(BaseColor.WHITE);

				PdfPCell cell101 = new PdfPCell(new Paragraph(outstandingPaidDate, mainContent));
				cell101.setBorderWidth(0.01f);
				cell101.setPaddingBottom(2);
				cell101.setPaddingTop(2);
				cell101.setBorderColor(BaseColor.WHITE);

				/*
				 * For outstanding paid amount
				 */
				PdfPCell cell102 = new PdfPCell(new Paragraph("Outstanding Paid Amount", mainContent));
				cell102.setBorderWidth(0.01f);
				cell102.setPaddingBottom(2);
				cell102.setPaddingTop(2);
				cell102.setBorderColor(BaseColor.WHITE);

				PdfPCell cell103 = new PdfPCell(new Paragraph("" + outstandingPaidAmt, mainContent));
				cell103.setBorderWidth(0.01f);
				cell103.setPaddingBottom(2);
				cell103.setPaddingTop(2);
				cell103.setBorderColor(BaseColor.WHITE);

				/*
				 * For receipt by
				 */
				PdfPCell cell104 = new PdfPCell(new Paragraph("Receipt Given By", mainContent));
				cell104.setBorderWidth(0.01f);
				cell104.setPaddingBottom(2);
				cell104.setPaddingTop(3);
				cell104.setPaddingLeft(20);
				cell104.setBorderColor(BaseColor.WHITE);

				PdfPCell cell105 = new PdfPCell(new Paragraph(userFullName, mainContent));
				cell105.setBorderWidth(0.01f);
				cell105.setPaddingBottom(2);
				cell105.setPaddingTop(3);
				cell105.setBorderColor(BaseColor.WHITE);

				/*
				 * For Receivers Signature
				 */
				PdfPCell cell106 = new PdfPCell(new Paragraph("Receivers Signature", mainContent));
				cell106.setBorderWidth(0.01f);
				cell106.setPaddingBottom(2);
				cell106.setPaddingTop(3);
				cell106.setColspan(2);
				cell106.setPaddingRight(20);
				cell106.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell106.setBorderColor(BaseColor.WHITE);

				table10.addCell(cell94);
				table10.addCell(cell95);
				table10.addCell(cell96);
				table10.addCell(cell97);
				table10.addCell(cell98);
				table10.addCell(cell99);
				table10.addCell(cell100);
				table10.addCell(cell101);
				table10.addCell(cell102);
				table10.addCell(cell103);
				table10.addCell(cell104);
				table10.addCell(cell105);
				table10.addCell(cell106);

			}

			document.add(table10);

			document.newPage();

			/*
			 * For signature in footer
			 */
			// PdfPTable table5 = new PdfPTable(1);
			// table5.setTotalWidth(350);
			//
			// PdfPCell cell1111 = new PdfPCell(new Paragraph("Signature",
			// mainContent1));
			// cell1111.setBackgroundColor(BaseColor.WHITE);
			// cell1111.setHorizontalAlignment(Element.ALIGN_RIGHT);
			// cell1111.setBorderColor(BaseColor.WHITE);
			// table5.addCell(cell1111);
			//
			// FooterTable event = new FooterTable(table);
			// writer.setPageEvent(event);

			document.close();

			System.out.println("Successfully written and generated Bill PDF");

			status = "success";

			/*
			 * Closing resultSet, preparedStatement, connection objects
			 */
			resultSet15.close();
			preparedStatement15.close();

			resultSet8.close();
			preparedStatement8.close();

			resultSet7.close();
			preparedStatement7.close();

			resultSet6.close();
			preparedStatement6.close();

			resultSet5.close();
			preparedStatement5.close();

			resultSet4.close();
			preparedStatement4.close();

			resultSet3.close();
			preparedStatement3.close();

			resultSet2.close();
			preparedStatement2.close();

			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			status = "error";
		}

		return status;
	}

	public String generatePatientBasedReport(ReportForm reportForm, String realPath, String PDFFileName,
			String userFullName) {

		billingDAOInf = new BillingDAOImpl();
		userDAOInf = new UserDAOImpl();

		String patientIDNO = null;
		String patientName = null;
		String hospital = null;
		String address = null;
		String bbrNo = null;
		String receiptNo = null;
		String receiptDate = null;

		double totalAmt = 0D;
		double netReceivableAmt = 0D;
		double actualPayment = 0D;
		double outstandingAmt = 0D;

		String receiptType = "";
		String paymentType = "";
		double tdsAmt = 0D;

		String concession = null;
		double concessionAmt = 0D;

		int receiptBy = 0;
		String refReceiptNo = null;
		String outstandingPaidDate = null;
		double outstandingPaidAmt = 0D;

		String bloodBankName = null;
		String bloodBankregNo = null;
		String bloodBankAddress = null;
		String bloodBankPhone = null;

		String chequNo = "";
		String chequDate = "";
		String chequeIssuedBy = "";
		String chequBankName = "";
		String chequeBankBranch = "";
		double chequeAmt = 0D;

		String receiptByName = null;

		String charityCaseNo = "";

		String bloodBankSotrage = "";

		int patientID = 0;

		int srNo = 1;

		int multipleIDcheck = 1;

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");

		SimpleDateFormat dateToBeFormatted = new SimpleDateFormat("dd-MM-yyyy");

		SimpleDateFormat dateToBeFormatted1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:SS");

		try {

			connection = getConnection();

			File file = new File(PDFFileName);

			/*
			 * Creating Document for PDF
			 */
			Document document = new Document(PageSize.A4);

			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file));

			Font Font1 = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.BOLD);
			Font Font3 = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
			Font Font2 = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.STRIKETHRU);
			Font mainContent = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.NORMAL);
			Font mainContent1 = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.NORMAL);
			Font mainContent2 = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.NORMAL);
			mainContent1.setColor(BaseColor.GRAY);

			document.open();

			/*
			 * Setting header
			 */
			document.addCreator("Rudhira");
			document.addTitle("Patient-Based report");

			// Sorting reportReceiptID array
			Arrays.sort(reportForm.getReportReceiptID());

			/*
			 * Iterating over the generated receipd and patient IDs
			 */
			for (int i = 0; i < reportForm.getReportReceiptID().length; i++) {

				if (reportForm.getReportReceiptID().length > 1) {
					multipleIDcheck++;
				}

				/*
				 * Generate the query to fetch Receipt Details
				 */
				String fetchDetailQuery3 = QueryMaker.BILL_PDF_RETRIEVE_RECEIPT_DETAILS2;

				preparedStatement3 = connection.prepareStatement(fetchDetailQuery3);
				preparedStatement3.setInt(1, Integer.parseInt(reportForm.getReportReceiptID()[i]));
				resultSet3 = preparedStatement3.executeQuery();

				while (resultSet3.next()) {

					totalAmt = resultSet3.getDouble("totalAmt");
					netReceivableAmt = resultSet3.getDouble("netReceivableAmt");
					actualPayment = resultSet3.getDouble("actualPayment");
					outstandingAmt = resultSet3.getDouble("outstandingAmt");

					patientID = resultSet3.getInt("patientID");

					concession = resultSet3.getString("concessionType");
					concessionAmt = resultSet3.getDouble("concessionAmt");

					receiptBy = resultSet3.getInt("receiptBy");
					refReceiptNo = resultSet3.getString("refReceiptNumber");
					outstandingPaidDate = resultSet3.getString("outstandingPaidDate");

					if (outstandingPaidDate == null || outstandingPaidDate == "") {

						outstandingPaidDate = "";

					} else {

						outstandingPaidDate = dateToBeFormatted.format(dateFormat.parse(outstandingPaidDate));

					}
					outstandingPaidAmt = resultSet3.getDouble("outstandingPaidAmt");
					paymentType = resultSet3.getString("paymentType");
					tdsAmt = resultSet3.getDouble("tdsAmt");
					bbrNo = resultSet3.getString("bdrNo");

					receiptNo = resultSet3.getString("receiptNo");
					receiptDate = resultSet3.getString("receiptDate");

					if (receiptDate == null || receiptDate == "") {

						receiptDate = "";

					} else {

						receiptDate = dateToBeFormatted1.format(dateFormat1.parse(receiptDate));

					}

					receiptType = resultSet3.getString("receiptType");
					charityCaseNo = resultSet3.getString("charityCaseNo");

				}

				/*
				 * Generate the query to fetch Patient Details
				 */
				String fetchDetailQuery1 = QueryMaker.BILL_PDF_RETRIEVE_PATIENT_DETAILS1;

				preparedStatement1 = connection.prepareStatement(fetchDetailQuery1);
				preparedStatement1.setInt(1, patientID);
				resultSet1 = preparedStatement1.executeQuery();

				while (resultSet1.next()) {
					patientIDNO = resultSet1.getString("patientIdentifier");

					if (resultSet1.getString("middleName") == null || resultSet1.getString("middleName") == "") {

						patientName = resultSet1.getString("firstName") + " " + resultSet1.getString("lastName");

					} else {

						patientName = resultSet1.getString("firstName") + " " + resultSet1.getString("middleName") + " "
								+ resultSet1.getString("lastName");

					}

					hospital = resultSet1.getString("hospital");
					address = resultSet1.getString("address");
					bloodBankSotrage = resultSet1.getString("bloodBankStorage");
				}

				/*
				 * Generate the query to fetch ReceiptItems details
				 */
				String fetchDetailQuery4 = QueryMaker.BILL_PDF_RETRIEVE_RECEIPT_ITEM_DETAILS;

				preparedStatement4 = connection.prepareStatement(fetchDetailQuery4);
				preparedStatement4.setInt(1, Integer.parseInt(reportForm.getReportReceiptID()[i]));
				resultSet4 = preparedStatement4.executeQuery();

				/*
				 * Generate the query to fetch Other Charges details
				 */
				String fetchDetailQuery5 = QueryMaker.BILL_PDF_RETRIEVE_OTHER_CHARGES;

				preparedStatement5 = connection.prepareStatement(fetchDetailQuery5);
				preparedStatement5.setInt(1, Integer.parseInt(reportForm.getReportReceiptID()[i]));
				resultSet5 = preparedStatement5.executeQuery();

				/*
				 * Generate the query to fetch Cheque details
				 */
				String fetchDetailQuery6 = QueryMaker.BILL_PDF_RETRIEVE_CHEQUE_DETAILS;

				preparedStatement6 = connection.prepareStatement(fetchDetailQuery6);
				preparedStatement6.setInt(1, Integer.parseInt(reportForm.getReportReceiptID()[i]));
				resultSet6 = preparedStatement6.executeQuery();

				while (resultSet6.next()) {
					chequeIssuedBy = resultSet6.getString("chequeIssuedBy");
					chequNo = resultSet6.getString("chequeNumber");
					chequDate = resultSet6.getString("chequeDate");

					if (chequDate == null || chequDate == "") {
						chequDate = "";
					} else {
						chequDate = dateToBeFormatted.format(dateFormat.parse(chequDate));
					}
					chequBankName = resultSet6.getString("bankName");
					chequeBankBranch = resultSet6.getString("bankBranch");
					chequeAmt = resultSet6.getDouble("chequeAmt");
				}

				PdfContentByte cb = writer.getDirectContent();

				// Barcode for patientIDNo
				Barcode128 code128 = new Barcode128();
				code128.setCode(String.valueOf(patientIDNO));
				code128.setCodeType(Barcode128.CODE128);
				Image patientIDImage = code128.createImageWithBarcode(cb, null, null);
				patientIDImage.setAbsolutePosition(30, 200);
				patientIDImage.scalePercent(55);

				// Barcode for BDRNo
				Barcode128 code1281 = new Barcode128();
				code1281.setCode(String.valueOf(bbrNo));
				code1281.setCodeType(Barcode128.CODE128);
				Image patientbdrNoImage = code1281.createImageWithBarcode(cb, null, null);
				patientbdrNoImage.setAbsolutePosition(30, 200);
				patientbdrNoImage.scalePercent(55);

				/*
				 * For Receipt type
				 */
				PdfPTable table0 = new PdfPTable(3);
				table0.setWidthPercentage(100);
				Rectangle rect0 = new Rectangle(270, 700);
				table0.setWidthPercentage(new float[] { 90, 90, 90 }, rect0);

				PdfPCell cell0 = null;

				/*
				 * Checking whether patient report search was on basis of
				 * storage center, if so, printing storage center name into PDF
				 * else printing patient name
				 */
				if (reportForm.getStorageCenter().equals("Storage Center")) {

					cell0 = new PdfPCell(new Paragraph("Storage Center : " + bloodBankSotrage, mainContent));
					cell0.setPaddingBottom(5);
					cell0.setBorderWidthBottom(1f);
					cell0.setBorderColor(BaseColor.WHITE);

				} else {

					cell0 = new PdfPCell(new Paragraph("Patient Name : " + patientName, mainContent));
					cell0.setPaddingBottom(5);
					cell0.setBorderWidthBottom(1f);
					cell0.setBorderColor(BaseColor.WHITE);

				}

				PdfPCell cell01 = new PdfPCell(new Paragraph("Net Receivable : " + netReceivableAmt, mainContent));
				cell01.setPaddingBottom(5);
				cell01.setBorderWidthBottom(1f);
				cell01.setBorderColor(BaseColor.WHITE);

				PdfPCell cell02 = new PdfPCell(
						new Paragraph("Outstanding Paid Date : " + outstandingPaidDate, mainContent));
				cell02.setPaddingBottom(5);
				cell02.setBorderWidthBottom(1f);
				cell02.setBorderColor(BaseColor.WHITE);

				PdfPCell cell03 = new PdfPCell(new Paragraph("BDR No. : ", mainContent));
				cell03.setPaddingBottom(5);
				cell03.setBorderWidthBottom(1f);
				cell03.setBorderColor(BaseColor.WHITE);

				PdfPCell cell04 = new PdfPCell(new Paragraph("Actual Payment : " + actualPayment, mainContent));
				cell04.setPaddingBottom(5);
				cell04.setBorderWidthBottom(1f);
				cell04.setBorderColor(BaseColor.WHITE);

				PdfPCell cell05 = new PdfPCell(
						new Paragraph("Outstanding Paid Amount : " + outstandingPaidAmt, mainContent));
				cell05.setPaddingBottom(5);
				cell05.setBorderWidthBottom(1f);
				cell05.setBorderColor(BaseColor.WHITE);

				// Creating table for barcode image
				PdfPTable tableOD = new PdfPTable(2);

				PdfPCell imageCellOD = new PdfPCell(patientIDImage, true);
				imageCellOD.setBorderColor(BaseColor.WHITE);
				imageCellOD.setFixedHeight(20f);

				PdfPCell textCellOD = new PdfPCell(new Paragraph("Patient Barcode : ", mainContent));
				textCellOD.setBorderColor(BaseColor.WHITE);

				tableOD.addCell(textCellOD);
				tableOD.addCell(imageCellOD);

				PdfPCell cell06 = new PdfPCell(tableOD);
				cell06.setPaddingBottom(5);
				cell06.setBorderWidthBottom(1f);
				cell06.setBorderColor(BaseColor.WHITE);

				PdfPCell cell07 = new PdfPCell(new Paragraph("Outstanding Amount : " + outstandingAmt, mainContent));
				cell07.setPaddingBottom(5);
				cell07.setBorderWidthBottom(1f);
				cell07.setBorderColor(BaseColor.WHITE);

				PdfPCell cell08 = new PdfPCell(new Paragraph("Receipt By : " + userFullName, mainContent));
				cell08.setPaddingBottom(5);
				cell08.setBorderWidthBottom(1f);
				cell08.setBorderColor(BaseColor.WHITE);

				PdfPCell cell09 = new PdfPCell(new Paragraph("Receipt No. : " + receiptNo, mainContent));
				cell09.setPaddingBottom(5);
				cell09.setPaddingTop(-5);
				cell09.setBorderWidthBottom(1f);
				cell09.setBorderColor(BaseColor.WHITE);

				PdfPCell cell010 = new PdfPCell(new Paragraph("Payment Type : " + paymentType, mainContent));
				cell010.setPaddingBottom(5);
				cell010.setPaddingTop(-5);
				cell010.setBorderWidthBottom(1f);
				cell010.setBorderColor(BaseColor.WHITE);

				PdfPCell cell011 = new PdfPCell(new Paragraph("Cheque No. : " + chequNo, mainContent));
				cell011.setPaddingBottom(5);
				cell011.setPaddingTop(-5);
				cell011.setBorderWidthBottom(1f);
				cell011.setBorderColor(BaseColor.WHITE);

				PdfPCell cell012 = new PdfPCell(new Paragraph("Date of Receipt : " + receiptDate, mainContent));
				cell012.setPaddingBottom(5);
				cell012.setBorderWidthBottom(1f);
				cell012.setBorderColor(BaseColor.WHITE);

				PdfPCell cell013 = new PdfPCell(new Paragraph("TDS Amount : " + tdsAmt, mainContent));
				cell013.setPaddingBottom(5);
				cell013.setBorderWidthBottom(1f);
				cell013.setBorderColor(BaseColor.WHITE);

				PdfPCell cell014 = new PdfPCell(new Paragraph("Bank Name : " + chequBankName, mainContent));
				cell014.setPaddingBottom(5);
				cell014.setBorderWidthBottom(1f);
				cell014.setBorderColor(BaseColor.WHITE);

				// Creating table for receipt barcode image
				PdfPTable tableOD1 = new PdfPTable(2);

				PdfPCell imageCellOD1 = new PdfPCell(patientbdrNoImage, true);
				imageCellOD1.setBorderColor(BaseColor.WHITE);
				imageCellOD1.setFixedHeight(20f);

				PdfPCell textCellOD1 = new PdfPCell(new Paragraph("Receipt Barcode : ", mainContent));
				textCellOD1.setBorderColor(BaseColor.WHITE);

				tableOD1.addCell(textCellOD1);
				tableOD1.addCell(imageCellOD1);

				PdfPCell cell015 = new PdfPCell(tableOD1);
				cell015.setPaddingBottom(5);
				cell015.setBorderWidthBottom(1f);
				cell015.setBorderColor(BaseColor.WHITE);

				PdfPCell cell016 = new PdfPCell(new Paragraph("Ref Receipt Number : " + refReceiptNo, mainContent));
				cell016.setPaddingBottom(5);
				cell016.setBorderWidthBottom(1f);
				cell016.setBorderColor(BaseColor.WHITE);

				PdfPCell cell017 = new PdfPCell(new Paragraph("Cheque Date : " + chequDate, mainContent));
				cell017.setPaddingBottom(5);
				cell017.setBorderWidthBottom(1f);
				cell017.setBorderColor(BaseColor.WHITE);

				PdfPCell cell018 = new PdfPCell(new Paragraph("Receipt Details", Font1));
				cell018.setPaddingBottom(15);
				cell018.setBorderWidthBottom(1f);
				cell018.setColspan(3);
				cell018.setBorderColor(BaseColor.WHITE);

				table0.addCell(cell0);
				table0.addCell(cell01);
				table0.addCell(cell02);
				table0.addCell(cell03);
				table0.addCell(cell04);
				table0.addCell(cell05);
				table0.addCell(cell06);
				table0.addCell(cell07);
				table0.addCell(cell08);
				table0.addCell(cell09);
				table0.addCell(cell010);
				table0.addCell(cell011);
				table0.addCell(cell012);
				table0.addCell(cell013);
				table0.addCell(cell014);
				table0.addCell(cell015);
				table0.addCell(cell016);
				table0.addCell(cell017);
				table0.addCell(cell018);

				document.add(table0);

				/*
				 * For Component details table
				 */
				PdfPTable table3 = new PdfPTable(6);
				table3.setWidthPercentage(100);
				Rectangle rect2 = new Rectangle(310, 700);
				table3.setWidthPercentage(new float[] { 20, 125, 40, 30, 35, 35 }, rect2);

				/*
				 * For blank space
				 */
				PdfPCell cell18 = new PdfPCell(new Paragraph("", Font2));
				cell18.setColspan(6);
				cell18.setBorderColor(BaseColor.WHITE);

				/*
				 * Creating header titles for table
				 */
				// For Sr.No.
				PdfPCell cell19 = new PdfPCell(new Paragraph("Sr.No.", Font1));
				cell19.setBorderWidth(0.01f);
				cell19.setPaddingBottom(3);
				cell19.setBorderColor(BaseColor.GRAY);

				// For Product req.
				PdfPCell cell20 = new PdfPCell(new Paragraph("Product Req.", Font1));
				cell20.setBorderWidth(0.01f);
				cell20.setPaddingBottom(3);
				cell20.setBorderColor(BaseColor.GRAY);

				// For Bag No
				PdfPCell cell21 = new PdfPCell(new Paragraph("Bag No.", Font1));
				cell21.setBorderWidth(0.01f);
				cell21.setPaddingBottom(3);
				cell21.setBorderColor(BaseColor.GRAY);

				// For Quantity
				PdfPCell cell22 = new PdfPCell(new Paragraph("Quantity", Font1));
				cell22.setBorderWidth(0.01f);
				cell22.setPaddingBottom(3);
				cell22.setBorderColor(BaseColor.GRAY);

				// For Rate
				PdfPCell cell23 = new PdfPCell(new Paragraph("Rate", Font1));
				cell23.setBorderWidth(0.01f);
				cell23.setPaddingBottom(3);
				cell23.setBorderColor(BaseColor.GRAY);

				// For Amount
				PdfPCell cell24 = new PdfPCell(new Paragraph("Amount", Font1));
				cell24.setBorderWidth(0.01f);
				cell24.setPaddingBottom(3);
				cell24.setBorderColor(BaseColor.GRAY);

				table3.addCell(cell18);
				table3.addCell(cell19);
				table3.addCell(cell20);
				table3.addCell(cell21);
				table3.addCell(cell22);
				table3.addCell(cell23);
				table3.addCell(cell24);

				/*
				 * For Receipt items values
				 */
				while (resultSet4.next()) {

					// For Sr.No.
					PdfPCell cell37 = new PdfPCell(new Paragraph("" + srNo, mainContent));
					cell37.setBorderWidth(0.01f);
					cell37.setPaddingBottom(3);
					cell37.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell37.setBorderColor(BaseColor.GRAY);

					// For Product req.
					PdfPCell cell38 = new PdfPCell(new Paragraph(resultSet4.getString("product"), mainContent));
					cell38.setBorderWidth(0.01f);
					cell38.setPaddingBottom(3);
					cell38.setBorderColor(BaseColor.GRAY);

					int receiptItemID = resultSet4.getInt("id");

					/*
					 * Retrieving BagNo based on receiptItemID from BagLog table
					 */
					String bagNo = billingDAOInf.retrieveBagNoByReceiptItemID(receiptItemID);

					// For Bag No.
					PdfPCell cell39 = new PdfPCell(new Paragraph("" + bagNo, mainContent));
					cell39.setBorderWidth(0.01f);
					cell39.setPaddingBottom(3);
					cell39.setBorderColor(BaseColor.GRAY);

					// For Bag No.
					PdfPCell cell40 = new PdfPCell(new Paragraph("" + resultSet4.getInt("quantity"), mainContent));
					cell40.setBorderWidth(0.01f);
					cell40.setPaddingBottom(3);
					cell40.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell40.setBorderColor(BaseColor.GRAY);

					// For Rate
					PdfPCell cell41 = new PdfPCell(new Paragraph("" + resultSet4.getDouble("rate"), mainContent));
					cell41.setBorderWidth(0.01f);
					cell41.setPaddingBottom(3);
					cell41.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell41.setBorderColor(BaseColor.GRAY);

					// For Amount
					PdfPCell cell42 = new PdfPCell(new Paragraph("" + resultSet4.getDouble("amount"), mainContent));
					cell42.setBorderWidth(0.01f);
					cell42.setPaddingBottom(3);
					cell42.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell42.setBorderColor(BaseColor.GRAY);

					srNo++;

					table3.addCell(cell37);
					table3.addCell(cell38);
					table3.addCell(cell39);
					table3.addCell(cell40);
					table3.addCell(cell41);
					table3.addCell(cell42);

				}

				/*
				 * For Other charges
				 */
				while (resultSet5.next()) {

					PdfPCell cell45 = new PdfPCell(new Paragraph("Other Charges", Font1));
					cell45.setBorderWidth(0.01f);
					cell45.setPaddingBottom(3);
					cell45.setColspan(4);
					cell45.setHorizontalAlignment(Element.ALIGN_RIGHT);
					cell45.setBorderColor(BaseColor.GRAY);

					PdfPCell cell46 = new PdfPCell(new Paragraph(resultSet5.getString("chargeType"), mainContent));
					cell46.setBorderWidth(0.01f);
					cell46.setPaddingBottom(3);
					cell46.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell46.setBorderColor(BaseColor.GRAY);

					PdfPCell cell47 = new PdfPCell(
							new Paragraph("" + resultSet5.getDouble("otherCharges"), mainContent));
					cell47.setBorderWidth(0.01f);
					cell47.setPaddingBottom(3);
					cell47.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell47.setBorderColor(BaseColor.GRAY);

					table3.addCell(cell45);
					table3.addCell(cell46);
					table3.addCell(cell47);

				}

				if (concession.equals("000")) {
					System.out.println("no concession.");

					document.add(table3);

				} else {

					/*
					 * For Concession
					 */
					PdfPCell cell85 = new PdfPCell(new Paragraph("Concession", Font1));
					cell85.setBorderWidth(0.01f);
					cell85.setPaddingBottom(3);
					cell85.setColspan(4);
					cell85.setHorizontalAlignment(Element.ALIGN_RIGHT);
					cell85.setBorderColor(BaseColor.GRAY);

					PdfPCell cell86 = new PdfPCell(new Paragraph(concession, mainContent));
					cell86.setBorderWidth(0.01f);
					cell86.setPaddingBottom(3);
					cell86.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell86.setBorderColor(BaseColor.GRAY);

					PdfPCell cell87 = new PdfPCell(new Paragraph("" + concessionAmt, mainContent));
					cell87.setBorderWidth(0.01f);
					cell87.setPaddingBottom(3);
					cell87.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell87.setBorderColor(BaseColor.GRAY);

					table3.addCell(cell85);
					table3.addCell(cell86);
					table3.addCell(cell87);

					document.add(table3);

					if (multipleIDcheck > 1) {

						PdfPTable table2 = new PdfPTable(1);
						table2.setWidthPercentage(100);
						Rectangle rect3 = new Rectangle(270, 700);
						table2.setWidthPercentage(new float[] { 270 }, rect3);

						PdfPCell cell88 = new PdfPCell(new Paragraph("\n", mainContent));
						cell88.setPaddingBottom(5);
						cell88.setBorderWidthBottom(1f);
						cell88.setBorderColor(BaseColor.WHITE);

						PdfPCell cell89 = new PdfPCell(new Paragraph("", mainContent));
						cell89.setPaddingBottom(5);
						cell89.setBorderWidthBottom(1f);
						cell89.setBorderColorBottom(BaseColor.BLACK);
						cell89.setBorderColor(BaseColor.WHITE);

						PdfPCell cell90 = new PdfPCell(new Paragraph("\n", mainContent));
						cell90.setPaddingBottom(5);
						cell90.setBorderWidthBottom(1f);
						cell90.setBorderColor(BaseColor.WHITE);

						table2.addCell(cell88);
						table2.addCell(cell89);
						table2.addCell(cell90);

						document.add(table2);

					}

					multipleIDcheck++;

					srNo = 1;

				}

			}

			document.close();

			System.out.println("Successfully written and generated Bill PDF");

			status = "success";

			/*
			 * Closing resultSet, preparedStatement, connection objects
			 */
			resultSet6.close();
			preparedStatement6.close();

			resultSet5.close();
			preparedStatement5.close();

			resultSet4.close();
			preparedStatement4.close();

			resultSet3.close();
			preparedStatement3.close();

			resultSet1.close();
			preparedStatement1.close();

			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			status = "error";
		}

		return status;
	}

	public String generateCashHandoverReport(String startDate, String endDate, String realPath, String PDFFileName) {

		billingDAOInf = new BillingDAOImpl();
		userDAOInf = new UserDAOImpl();

		int srNo = 1;

		SimpleDateFormat dateToBeFormatted = new SimpleDateFormat("yyyy-MM-dd");

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

		try {

			connection = getConnection();

			File file = new File(PDFFileName);

			/*
			 * Creating Document for PDF
			 */
			Document document = new Document(PageSize.A4);

			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file));

			Font Font1 = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.BOLD);
			Font Font3 = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.BOLD);
			Font Font2 = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.STRIKETHRU);
			Font mainContent = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.NORMAL);
			Font mainContent1 = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.NORMAL);
			Font mainContent2 = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.NORMAL);
			mainContent1.setColor(BaseColor.GRAY);

			document.open();

			PdfContentByte cb = writer.getDirectContent();

			/*
			 * Setting header
			 */
			document.addCreator("Rudhira");
			document.addTitle("Cash hand-over report");

			/*
			 * Generate the query to fetch Patient Details
			 */
			String fetchDetailQuery1 = QueryMaker.RETRIEVE_CASH_HANDOVER_REPORT_DETAILS;

			preparedStatement1 = connection.prepareStatement(fetchDetailQuery1);

			preparedStatement1.setString(1, dateToBeFormatted.format(dateFormat.parse(startDate)));
			preparedStatement1.setString(2, dateToBeFormatted.format(dateFormat.parse(endDate)));

			resultSet1 = preparedStatement1.executeQuery();

			/*
			 * For cash handover details
			 */
			PdfPTable table3 = new PdfPTable(8);
			table3.setWidthPercentage(100);
			Rectangle rect2 = new Rectangle(310, 700);
			table3.setWidthPercentage(new float[] { 20, 40, 40, 40, 40, 40, 40, 40 }, rect2);

			/*
			 * For blank space
			 */
			PdfPCell cell18 = new PdfPCell(new Paragraph("Cash hand-over report", Font3));
			cell18.setColspan(8);
			cell18.setPaddingBottom(10);
			cell18.setPaddingTop(15);
			cell18.setBorderColor(BaseColor.WHITE);

			/*
			 * Creating header titles for table
			 */
			// For Sr.No.
			PdfPCell cell19 = new PdfPCell(new Paragraph("Sr.No.", Font1));
			cell19.setBorderWidth(0.01f);
			cell19.setPaddingBottom(3);
			cell19.setBorderColor(BaseColor.GRAY);

			// For Product req.
			PdfPCell cell20 = new PdfPCell(new Paragraph("Register Date", Font1));
			cell20.setBorderWidth(0.01f);
			cell20.setPaddingBottom(3);
			cell20.setBorderColor(BaseColor.GRAY);

			// For Bag No
			PdfPCell cell21 = new PdfPCell(new Paragraph("Shift", Font1));
			cell21.setBorderWidth(0.01f);
			cell21.setPaddingBottom(3);
			cell21.setBorderColor(BaseColor.GRAY);

			// For Quantity
			PdfPCell cell22 = new PdfPCell(new Paragraph("Cash Handover", Font1));
			cell22.setBorderWidth(0.01f);
			cell22.setPaddingBottom(3);
			cell22.setBorderColor(BaseColor.GRAY);

			// For Rate
			PdfPCell cell23 = new PdfPCell(new Paragraph("Cash Deposited", Font1));
			cell23.setBorderWidth(0.01f);
			cell23.setPaddingBottom(3);
			cell23.setBorderColor(BaseColor.GRAY);

			// For Amount
			PdfPCell cell24 = new PdfPCell(new Paragraph("Shift Cash", Font1));
			cell24.setBorderWidth(0.01f);
			cell24.setPaddingBottom(3);
			cell24.setBorderColor(BaseColor.GRAY);

			// For Amount
			PdfPCell cell25 = new PdfPCell(new Paragraph("Other Product", Font1));
			cell25.setBorderWidth(0.01f);
			cell25.setPaddingBottom(3);
			cell25.setBorderColor(BaseColor.GRAY);

			// For Amount
			PdfPCell cell26 = new PdfPCell(new Paragraph("Balance", Font1));
			cell26.setBorderWidth(0.01f);
			cell26.setPaddingBottom(3);
			cell26.setBorderColor(BaseColor.GRAY);

			table3.addCell(cell18);
			table3.addCell(cell19);
			table3.addCell(cell20);
			table3.addCell(cell21);
			table3.addCell(cell22);
			table3.addCell(cell23);
			table3.addCell(cell24);
			table3.addCell(cell25);
			table3.addCell(cell26);

			/*
			 * For Receipt items values
			 */
			while (resultSet1.next()) {

				// For Sr.No.
				PdfPCell cell37 = new PdfPCell(new Paragraph("" + srNo, mainContent));
				cell37.setBorderWidth(0.01f);
				cell37.setPaddingBottom(3);
				cell37.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell37.setBorderColor(BaseColor.GRAY);

				// For register date
				PdfPCell cell38 = new PdfPCell(
						new Paragraph(dateFormat.format(resultSet1.getDate("registerDate")), mainContent));
				cell38.setBorderWidth(0.01f);
				cell38.setPaddingBottom(3);
				cell38.setBorderColor(BaseColor.GRAY);

				// For Shift
				PdfPCell cell39 = new PdfPCell(new Paragraph("" + resultSet1.getString("shift"), mainContent));
				cell39.setBorderWidth(0.01f);
				cell39.setPaddingBottom(3);
				cell39.setBorderColor(BaseColor.GRAY);

				// For cash handover
				PdfPCell cell40 = new PdfPCell(new Paragraph("" + resultSet1.getDouble("cashHandover"), mainContent));
				cell40.setBorderWidth(0.01f);
				cell40.setPaddingBottom(3);
				cell40.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell40.setBorderColor(BaseColor.GRAY);

				// For cash deposited
				PdfPCell cell41 = new PdfPCell(new Paragraph("" + resultSet1.getDouble("cashDeposited"), mainContent));
				cell41.setBorderWidth(0.01f);
				cell41.setPaddingBottom(3);
				cell41.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell41.setBorderColor(BaseColor.GRAY);

				// For shift cash
				PdfPCell cell42 = new PdfPCell(new Paragraph("" + resultSet1.getDouble("shiftCash"), mainContent));
				cell42.setBorderWidth(0.01f);
				cell42.setPaddingBottom(3);
				cell42.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell42.setBorderColor(BaseColor.GRAY);

				// For other product
				PdfPCell cell43 = new PdfPCell(new Paragraph("" + resultSet1.getDouble("otherProduct"), mainContent));
				cell43.setBorderWidth(0.01f);
				cell43.setPaddingBottom(3);
				cell43.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell43.setBorderColor(BaseColor.GRAY);

				// For balance
				PdfPCell cell44 = new PdfPCell(
						new Paragraph("" + resultSet1.getDouble("registerBalance"), mainContent));
				cell44.setBorderWidth(0.01f);
				cell44.setPaddingBottom(3);
				cell44.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell44.setBorderColor(BaseColor.GRAY);

				srNo++;

				table3.addCell(cell37);
				table3.addCell(cell38);
				table3.addCell(cell39);
				table3.addCell(cell40);
				table3.addCell(cell41);
				table3.addCell(cell42);
				table3.addCell(cell43);
				table3.addCell(cell44);

			}

			document.add(table3);

			document.close();

			System.out.println("Successfully written and generated cash handover PDF");

			status = "success";

			/*
			 * Closing resultSet, preparedStatement, connection objects
			 */
			resultSet1.close();
			preparedStatement1.close();

			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			status = "error";
		}

		return status;

	}

	public String generateComponentBasedReport(ReportForm reportForm, String realPath, String PDFFileName,
			String userFullName) {

		billingDAOInf = new BillingDAOImpl();
		userDAOInf = new UserDAOImpl();

		SimpleDateFormat dateToBeFormatted = new SimpleDateFormat("dd-MM-yyyy");

		SimpleDateFormat dateToBeFormatted1 = new SimpleDateFormat("yyyy-MM-dd");

		String patientIDNO = null;
		String patientName = null;
		String hospital = null;
		String address = null;
		String bbrNo = null;
		String receiptNo = null;
		String receiptDate = null;

		double totalAmt = 0D;
		double netReceivableAmt = 0D;
		double actualPayment = 0D;
		double outstandingAmt = 0D;

		String receiptType = "";
		String paymentType = "";
		double tdsAmt = 0D;

		String concession = null;
		double concessionAmt = 0D;

		int receiptBy = 0;
		String refReceiptNo = null;
		String outstandingPaidDate = null;
		double outstandingPaidAmt = 0D;

		String bloodBankName = null;
		String bloodBankregNo = null;
		String bloodBankAddress = null;
		String bloodBankPhone = null;

		String chequNo = "";
		String chequDate = "";
		String chequeIssuedBy = "";
		String chequBankName = "";
		String chequeBankBranch = "";
		double chequeAmt = 0D;

		String receiptByName = null;

		String charityCaseNo = "";

		int srNo = 1;

		SimpleDateFormat dateToBeFormatted2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:SS");

		try {

			connection = getConnection();

			File file = new File(PDFFileName);

			/*
			 * Creating Document for PDF
			 */
			Document document = new Document(PageSize.A4);

			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file));

			Font Font1 = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.BOLD);
			Font Font3 = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
			Font Font2 = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.STRIKETHRU);
			Font mainContent = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.NORMAL);
			Font mainContent1 = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.NORMAL);
			Font mainContent2 = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.NORMAL);
			mainContent1.setColor(BaseColor.GRAY);

			document.open();

			/*
			 * Setting header
			 */
			document.addCreator("Rudhira");
			document.addTitle("Component-Based report");

			// Sorting reportReceiptID array
			Arrays.sort(reportForm.getReportComponents());

			/*
			 * For Component details table
			 */
			PdfPTable table3 = new PdfPTable(8);
			table3.setWidthPercentage(100);
			Rectangle rect2 = new Rectangle(310, 700);
			table3.setWidthPercentage(new float[] { 20, 70, 40, 30, 35, 35, 30, 30 }, rect2);

			/*
			 * For blank space
			 */
			PdfPCell cell18 = new PdfPCell(new Paragraph("", Font2));
			cell18.setColspan(8);
			cell18.setBorderColor(BaseColor.WHITE);

			/*
			 * Creating header titles for table
			 */
			// For Sr.No.
			PdfPCell cell19 = new PdfPCell(new Paragraph("Sr.No.", Font1));
			cell19.setBorderWidth(0.01f);
			cell19.setPaddingBottom(3);
			cell19.setBorderColor(BaseColor.GRAY);

			// For Product req.
			PdfPCell cell20 = new PdfPCell(new Paragraph("Patient Name/ Storage Centre", Font1));
			cell20.setBorderWidth(0.01f);
			cell20.setPaddingBottom(3);
			cell20.setBorderColor(BaseColor.GRAY);

			// For Bag No
			PdfPCell cell21 = new PdfPCell(new Paragraph("Component", Font1));
			cell21.setBorderWidth(0.01f);
			cell21.setPaddingBottom(3);
			cell21.setBorderColor(BaseColor.GRAY);

			// For Quantity
			PdfPCell cell22 = new PdfPCell(new Paragraph("Receipt No", Font1));
			cell22.setBorderWidth(0.01f);
			cell22.setPaddingBottom(3);
			cell22.setBorderColor(BaseColor.GRAY);

			// For Rate
			PdfPCell cell23 = new PdfPCell(new Paragraph("Receipt Date", Font1));
			cell23.setBorderWidth(0.01f);
			cell23.setPaddingBottom(3);
			cell23.setBorderColor(BaseColor.GRAY);

			// For Amount
			PdfPCell cell24 = new PdfPCell(new Paragraph("Rate", Font1));
			cell24.setBorderWidth(0.01f);
			cell24.setPaddingBottom(3);
			cell24.setBorderColor(BaseColor.GRAY);

			// For Amount
			PdfPCell cell25 = new PdfPCell(new Paragraph("Quantity", Font1));
			cell25.setBorderWidth(0.01f);
			cell25.setPaddingBottom(3);
			cell25.setBorderColor(BaseColor.GRAY);

			// For Amount
			PdfPCell cell26 = new PdfPCell(new Paragraph("Amount", Font1));
			cell26.setBorderWidth(0.01f);
			cell26.setPaddingBottom(3);
			cell26.setBorderColor(BaseColor.GRAY);

			table3.addCell(cell18);
			table3.addCell(cell19);
			table3.addCell(cell20);
			table3.addCell(cell21);
			table3.addCell(cell22);
			table3.addCell(cell23);
			table3.addCell(cell24);
			table3.addCell(cell25);
			table3.addCell(cell26);

			/*
			 * Iterating over the generated receipd and patient IDs
			 */
			for (int i = 0; i < reportForm.getReportComponents().length; i++) {

				String[] array = reportForm.getReportComponents()[i].split("\\$");

				String component = array[0];
				int receiptID = Integer.parseInt(array[1]);

				/*
				 * Generate the query to component based list
				 */
				String fetchDetailQuery1 = QueryMaker.RETRIEVE_COMPONENT_BASED_LIST;

				preparedStatement1 = connection.prepareStatement(fetchDetailQuery1);
				preparedStatement1.setString(1, component);
				preparedStatement1.setInt(2, receiptID);
				resultSet1 = preparedStatement1.executeQuery();

				/*
				 * For Receipt items values
				 */
				while (resultSet1.next()) {

					if (resultSet1.getString("middleName") == null || resultSet1.getString("middleName") == "") {

						patientName = resultSet1.getString("firstName") + " " + resultSet1.getString("lastName");

					} else {

						patientName = resultSet1.getString("firstName") + " " + resultSet1.getString("middleName") + " "
								+ resultSet1.getString("lastName");

					}

					String bbStorage = resultSet1.getString("bloodBankStorage");

					// For Sr.No.
					PdfPCell cell37 = new PdfPCell(new Paragraph("" + srNo, mainContent));
					cell37.setBorderWidth(0.01f);
					cell37.setPaddingBottom(3);
					cell37.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell37.setBorderColor(BaseColor.GRAY);

					PdfPCell cell38 = null;

					if (bbStorage == null || bbStorage == "" || bbStorage.isEmpty()) {

						// For Product req.
						cell38 = new PdfPCell(new Paragraph(patientName, mainContent));
						cell38.setBorderWidth(0.01f);
						cell38.setPaddingBottom(3);
						cell38.setBorderColor(BaseColor.GRAY);

					} else {

						// For Product req.
						cell38 = new PdfPCell(new Paragraph(bbStorage, mainContent));
						cell38.setBorderWidth(0.01f);
						cell38.setPaddingBottom(3);
						cell38.setBorderColor(BaseColor.GRAY);

					}

					// For Bag No.
					PdfPCell cell39 = new PdfPCell(new Paragraph(resultSet1.getString("product"), mainContent));
					cell39.setBorderWidth(0.01f);
					cell39.setPaddingBottom(3);
					cell39.setBorderColor(BaseColor.GRAY);

					// For Bag No.
					PdfPCell cell40 = new PdfPCell(new Paragraph(resultSet1.getString("receiptNo"), mainContent));
					cell40.setBorderWidth(0.01f);
					cell40.setPaddingBottom(3);
					cell40.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell40.setBorderColor(BaseColor.GRAY);

					// For Rate
					PdfPCell cell41 = new PdfPCell(new Paragraph(
							dateToBeFormatted2.format(resultSet1.getTimestamp("receiptDate")), mainContent));
					cell41.setBorderWidth(0.01f);
					cell41.setPaddingBottom(3);
					cell41.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell41.setBorderColor(BaseColor.GRAY);

					// For Amount
					PdfPCell cell42 = new PdfPCell(new Paragraph("" + resultSet1.getDouble("rate"), mainContent));
					cell42.setBorderWidth(0.01f);
					cell42.setPaddingBottom(3);
					cell42.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell42.setBorderColor(BaseColor.GRAY);

					// For Amount
					PdfPCell cell43 = new PdfPCell(new Paragraph("" + resultSet1.getInt("quantity"), mainContent));
					cell43.setBorderWidth(0.01f);
					cell43.setPaddingBottom(3);
					cell43.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell43.setBorderColor(BaseColor.GRAY);

					// For Amount
					PdfPCell cell44 = new PdfPCell(new Paragraph("" + resultSet1.getDouble("amount"), mainContent));
					cell44.setBorderWidth(0.01f);
					cell44.setPaddingBottom(3);
					cell44.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell44.setBorderColor(BaseColor.GRAY);

					srNo++;

					table3.addCell(cell37);
					table3.addCell(cell38);
					table3.addCell(cell39);
					table3.addCell(cell40);
					table3.addCell(cell41);
					table3.addCell(cell42);
					table3.addCell(cell43);
					table3.addCell(cell44);

				}

			}

			document.add(table3);

			document.close();

			System.out.println("Successfully written and generated component based report PDF");

			status = "success";

			/*
			 * Closing resultSet, preparedStatement, connection objects
			 */
			resultSet1.close();
			preparedStatement1.close();

			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			status = "error";
		}

		return status;
	}

	public String generateRegisterReport(String startDate, String endDate, String realPath, String PDFFileName) {

		billingDAOInf = new BillingDAOImpl();
		userDAOInf = new UserDAOImpl();

		int srNo = 1;

		SimpleDateFormat dateToBeFormatted = new SimpleDateFormat("yyyy-MM-dd");

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

		try {

			connection = getConnection();

			File file = new File(PDFFileName);

			/*
			 * Creating Document for PDF
			 */
			Document document = new Document(PageSize.A4);

			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file));

			Font Font1 = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.BOLD);
			Font Font3 = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.BOLD);
			Font Font2 = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.STRIKETHRU);
			Font mainContent = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.NORMAL);
			Font mainContent1 = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.NORMAL);
			Font mainContent2 = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.NORMAL);
			mainContent1.setColor(BaseColor.GRAY);

			document.open();

			PdfContentByte cb = writer.getDirectContent();

			/*
			 * Setting header
			 */
			document.addCreator("Rudhira");
			document.addTitle("Register report");

			/*
			 * Generate the query to fetch Register details
			 */
			String fetchDetailQuery1 = QueryMaker.RETRIEVE_REGISTER_REPORT;

			preparedStatement1 = connection.prepareStatement(fetchDetailQuery1);

			preparedStatement1.setString(1, dateToBeFormatted.format(dateFormat.parse(startDate)));
			preparedStatement1.setString(2, dateToBeFormatted.format(dateFormat.parse(endDate)));

			resultSet1 = preparedStatement1.executeQuery();

			/*
			 * For cash handover details
			 */
			PdfPTable table3 = new PdfPTable(10);
			table3.setWidthPercentage(100);
			Rectangle rect2 = new Rectangle(310, 700);
			table3.setWidthPercentage(new float[] { 20, 30, 30, 30, 30, 30, 30, 30, 30, 30 }, rect2);

			/*
			 * For blank space
			 */
			PdfPCell cell18 = new PdfPCell(new Paragraph("Register report", Font3));
			cell18.setColspan(10);
			cell18.setPaddingBottom(10);
			cell18.setPaddingTop(15);
			cell18.setBorderColor(BaseColor.WHITE);

			/*
			 * Creating header titles for table
			 */
			// For Sr.No.
			PdfPCell cell19 = new PdfPCell(new Paragraph("Sr.No.", Font1));
			cell19.setBorderWidth(0.01f);
			cell19.setPaddingBottom(3);
			cell19.setBorderColor(BaseColor.GRAY);

			// For register date.
			PdfPCell cell20 = new PdfPCell(new Paragraph("Register Date", Font1));
			cell20.setBorderWidth(0.01f);
			cell20.setPaddingBottom(3);
			cell20.setBorderColor(BaseColor.GRAY);

			// For cash handover
			PdfPCell cell21 = new PdfPCell(new Paragraph("Cash Handover", Font1));
			cell21.setBorderWidth(0.01f);
			cell21.setPaddingBottom(3);
			cell21.setBorderColor(BaseColor.GRAY);

			// For blood bank balance
			PdfPCell cell22 = new PdfPCell(new Paragraph("Blood Bank Balance", Font1));
			cell22.setBorderWidth(0.01f);
			cell22.setPaddingBottom(3);
			cell22.setBorderColor(BaseColor.GRAY);

			// For other product balance
			PdfPCell cell23 = new PdfPCell(new Paragraph("Other Products Balance", Font1));
			cell23.setBorderWidth(0.01f);
			cell23.setPaddingBottom(3);
			cell23.setBorderColor(BaseColor.GRAY);

			// For expected cash balance
			PdfPCell cell24 = new PdfPCell(new Paragraph("Expected Cash Balance", Font1));
			cell24.setBorderWidth(0.01f);
			cell24.setPaddingBottom(3);
			cell24.setBorderColor(BaseColor.GRAY);

			// For actual cash balance
			PdfPCell cell25 = new PdfPCell(new Paragraph("Actual Cash Balance", Font1));
			cell25.setBorderWidth(0.01f);
			cell25.setPaddingBottom(3);
			cell25.setBorderColor(BaseColor.GRAY);

			// For bank deposit
			PdfPCell cell26 = new PdfPCell(new Paragraph("Bank Deposit", Font1));
			cell26.setBorderWidth(0.01f);
			cell26.setPaddingBottom(3);
			cell26.setBorderColor(BaseColor.GRAY);

			// For cash mismatch
			PdfPCell cell27 = new PdfPCell(new Paragraph("Cash Mismatch", Font1));
			cell27.setBorderWidth(0.01f);
			cell27.setPaddingBottom(3);
			cell27.setBorderColor(BaseColor.GRAY);

			// For cash adjustment
			PdfPCell cell28 = new PdfPCell(new Paragraph("Cash Adjustment", Font1));
			cell28.setBorderWidth(0.01f);
			cell28.setPaddingBottom(3);
			cell28.setBorderColor(BaseColor.GRAY);

			table3.addCell(cell18);
			table3.addCell(cell19);
			table3.addCell(cell20);
			table3.addCell(cell21);
			table3.addCell(cell22);
			table3.addCell(cell23);
			table3.addCell(cell24);
			table3.addCell(cell25);
			table3.addCell(cell26);
			table3.addCell(cell27);
			table3.addCell(cell28);

			/*
			 * For Receipt items values
			 */
			while (resultSet1.next()) {

				// For Sr.No.
				PdfPCell cell37 = new PdfPCell(new Paragraph("" + srNo, mainContent));
				cell37.setBorderWidth(0.01f);
				cell37.setPaddingBottom(3);
				cell37.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell37.setBorderColor(BaseColor.GRAY);

				// For register date
				PdfPCell cell38 = new PdfPCell(
						new Paragraph(dateFormat.format(resultSet1.getDate("registerDate")), mainContent));
				cell38.setBorderWidth(0.01f);
				cell38.setPaddingBottom(3);
				cell38.setBorderColor(BaseColor.GRAY);

				// For cash handover
				PdfPCell cell39 = new PdfPCell(new Paragraph("" + resultSet1.getDouble("handOverCash"), mainContent));
				cell39.setBorderWidth(0.01f);
				cell39.setPaddingBottom(3);
				cell39.setBorderColor(BaseColor.GRAY);

				// For blood bank balance
				PdfPCell cell40 = new PdfPCell(
						new Paragraph("" + resultSet1.getDouble("bloodBankBalance"), mainContent));
				cell40.setBorderWidth(0.01f);
				cell40.setPaddingBottom(3);
				cell40.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell40.setBorderColor(BaseColor.GRAY);

				// For other product balance
				PdfPCell cell41 = new PdfPCell(
						new Paragraph("" + resultSet1.getDouble("otherProductBalance"), mainContent));
				cell41.setBorderWidth(0.01f);
				cell41.setPaddingBottom(3);
				cell41.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell41.setBorderColor(BaseColor.GRAY);

				// For expected cash balance
				PdfPCell cell42 = new PdfPCell(
						new Paragraph("" + resultSet1.getDouble("expectedCashBalance"), mainContent));
				cell42.setBorderWidth(0.01f);
				cell42.setPaddingBottom(3);
				cell42.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell42.setBorderColor(BaseColor.GRAY);

				// For actual cash balance
				PdfPCell cell43 = new PdfPCell(
						new Paragraph("" + resultSet1.getDouble("actualCashBalance"), mainContent));
				cell43.setBorderWidth(0.01f);
				cell43.setPaddingBottom(3);
				cell43.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell43.setBorderColor(BaseColor.GRAY);

				// For bank deposit
				PdfPCell cell44 = new PdfPCell(new Paragraph("" + resultSet1.getDouble("bankDeposit"), mainContent));
				cell44.setBorderWidth(0.01f);
				cell44.setPaddingBottom(3);
				cell44.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell44.setBorderColor(BaseColor.GRAY);

				// For cash mismatch
				PdfPCell cell45 = new PdfPCell(new Paragraph("" + resultSet1.getDouble("cashMismatch"), mainContent));
				cell45.setBorderWidth(0.01f);
				cell45.setPaddingBottom(3);
				cell45.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell45.setBorderColor(BaseColor.GRAY);

				// For cash adjustment
				PdfPCell cell46 = new PdfPCell(new Paragraph("" + resultSet1.getDouble("cashAdjustment"), mainContent));
				cell46.setBorderWidth(0.01f);
				cell46.setPaddingBottom(3);
				cell46.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell46.setBorderColor(BaseColor.GRAY);

				srNo++;

				table3.addCell(cell37);
				table3.addCell(cell38);
				table3.addCell(cell39);
				table3.addCell(cell40);
				table3.addCell(cell41);
				table3.addCell(cell42);
				table3.addCell(cell43);
				table3.addCell(cell44);
				table3.addCell(cell45);
				table3.addCell(cell46);

			}

			document.add(table3);

			document.close();

			System.out.println("Successfully written and generated register report PDF");

			status = "success";

			/*
			 * Closing resultSet, preparedStatement, connection objects
			 */
			resultSet1.close();
			preparedStatement1.close();

			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			status = "error";
		}

		return status;

	}

	public String generateCardChequeBasedReport(ReportForm reportForm, String realPath, String PDFFileName,
			String userFullName) {

		billingDAOInf = new BillingDAOImpl();
		userDAOInf = new UserDAOImpl();

		String patientIDNO = null;
		String patientName = null;
		String hospital = null;
		String address = null;
		String bbrNo = null;
		String receiptNo = null;
		String receiptDate = null;

		double totalAmt = 0D;
		double netReceivableAmt = 0D;
		double actualPayment = 0D;
		double outstandingAmt = 0D;

		String receiptType = "";
		String paymentType = "";
		double tdsAmt = 0D;

		String concession = null;
		double concessionAmt = 0D;

		int receiptBy = 0;
		String refReceiptNo = null;
		String outstandingPaidDate = null;
		double outstandingPaidAmt = 0D;

		String chequNo = "";
		String chequDate = "";
		String chequeIssuedBy = "";
		String chequBankName = "";
		String chequeBankBranch = "";
		double chequeAmt = 0D;

		String receiptByName = null;

		String charityCaseNo = "";

		int srNo = 1;

		int multipleIDcheck = 1;

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");

		SimpleDateFormat dateToBeFormatted = new SimpleDateFormat("dd-MM-yyyy");

		SimpleDateFormat dateToBeFormatted1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:SS");

		try {

			connection = getConnection();

			File file = new File(PDFFileName);

			/*
			 * Creating Document for PDF
			 */
			Document document = new Document(PageSize.A4);

			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file));

			Font Font1 = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.BOLD);
			Font Font3 = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
			Font Font2 = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.STRIKETHRU);
			Font mainContent = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.NORMAL);
			Font mainContent1 = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.NORMAL);
			Font mainContent2 = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.NORMAL);
			mainContent1.setColor(BaseColor.GRAY);

			document.open();

			/*
			 * Setting header
			 */
			document.addCreator("Rudhira");
			document.addTitle("CardCheque-Based report");

			// Sorting reportReceiptID array
			Arrays.sort(reportForm.getReportReceiptID());

			/*
			 * Iterating over the generated receipd and patient IDs
			 */
			for (int i = 0; i < reportForm.getReportReceiptID().length; i++) {

				if (reportForm.getReportReceiptID().length > 1) {
					multipleIDcheck++;
				}

				/*
				 * Generate the query to fetch Patient Details
				 */
				String fetchDetailQuery1 = QueryMaker.BILL_PDF_RETRIEVE_PATIENT_DETAILS1;

				preparedStatement1 = connection.prepareStatement(fetchDetailQuery1);
				preparedStatement1.setInt(1, Integer.parseInt(reportForm.getReportPatientID()[i]));
				resultSet1 = preparedStatement1.executeQuery();

				while (resultSet1.next()) {
					patientIDNO = resultSet1.getString("patientIdentifier");

					if (resultSet1.getString("middleName") == null || resultSet1.getString("middleName") == "") {

						patientName = resultSet1.getString("firstName") + " " + resultSet1.getString("lastName");

					} else {

						patientName = resultSet1.getString("firstName") + " " + resultSet1.getString("middleName") + " "
								+ resultSet1.getString("lastName");

					}

					hospital = resultSet1.getString("hospital");
					address = resultSet1.getString("address");
				}

				/*
				 * Generate the query to fetch Receipt Details
				 */
				String fetchDetailQuery3 = QueryMaker.BILL_PDF_RETRIEVE_RECEIPT_DETAILS2;

				preparedStatement3 = connection.prepareStatement(fetchDetailQuery3);
				preparedStatement3.setInt(1, Integer.parseInt(reportForm.getReportReceiptID()[i]));
				resultSet3 = preparedStatement3.executeQuery();

				while (resultSet3.next()) {

					totalAmt = resultSet3.getDouble("totalAmt");
					netReceivableAmt = resultSet3.getDouble("netReceivableAmt");
					actualPayment = resultSet3.getDouble("actualPayment");
					outstandingAmt = resultSet3.getDouble("outstandingAmt");

					concession = resultSet3.getString("concessionType");
					concessionAmt = resultSet3.getDouble("concessionAmt");

					receiptBy = resultSet3.getInt("receiptBy");
					refReceiptNo = resultSet3.getString("refReceiptNumber");
					outstandingPaidDate = resultSet3.getString("outstandingPaidDate");

					if (outstandingPaidDate == null || outstandingPaidDate == "") {

						outstandingPaidDate = "";

					} else {

						outstandingPaidDate = dateToBeFormatted.format(dateFormat.parse(outstandingPaidDate));

					}
					outstandingPaidAmt = resultSet3.getDouble("outstandingPaidAmt");
					paymentType = resultSet3.getString("paymentType");
					tdsAmt = resultSet3.getDouble("tdsAmt");
					bbrNo = resultSet3.getString("bdrNo");

					receiptNo = resultSet3.getString("receiptNo");
					receiptDate = resultSet3.getString("receiptDate");

					if (receiptDate == null || receiptDate == "") {

						receiptDate = "";

					} else {

						receiptDate = dateToBeFormatted1.format(dateFormat1.parse(receiptDate));

					}

					receiptType = resultSet3.getString("receiptType");
					charityCaseNo = resultSet3.getString("charityCaseNo");

				}

				/*
				 * Generate the query to fetch ReceiptItems details
				 */
				String fetchDetailQuery4 = QueryMaker.BILL_PDF_RETRIEVE_RECEIPT_ITEM_DETAILS;

				preparedStatement4 = connection.prepareStatement(fetchDetailQuery4);
				preparedStatement4.setInt(1, Integer.parseInt(reportForm.getReportReceiptID()[i]));
				resultSet4 = preparedStatement4.executeQuery();

				/*
				 * Generate the query to fetch Other Charges details
				 */
				String fetchDetailQuery5 = QueryMaker.BILL_PDF_RETRIEVE_OTHER_CHARGES;

				preparedStatement5 = connection.prepareStatement(fetchDetailQuery5);
				preparedStatement5.setInt(1, Integer.parseInt(reportForm.getReportReceiptID()[i]));
				resultSet5 = preparedStatement5.executeQuery();

				/*
				 * Generate the query to fetch Cheque details
				 */
				String fetchDetailQuery6 = QueryMaker.BILL_PDF_RETRIEVE_CHEQUE_DETAILS;

				preparedStatement6 = connection.prepareStatement(fetchDetailQuery6);
				preparedStatement6.setInt(1, Integer.parseInt(reportForm.getReportReceiptID()[i]));
				resultSet6 = preparedStatement6.executeQuery();

				while (resultSet6.next()) {
					chequeIssuedBy = resultSet6.getString("chequeIssuedBy");
					chequNo = resultSet6.getString("chequeNumber");
					chequDate = resultSet6.getString("chequeDate");

					if (chequDate == null || chequDate == "") {
						chequDate = "";
					} else {
						chequDate = dateToBeFormatted.format(dateFormat.parse(chequDate));
					}
					chequBankName = resultSet6.getString("bankName");
					chequeBankBranch = resultSet6.getString("bankBranch");
					chequeAmt = resultSet6.getDouble("chequeAmt");
				}

				PdfContentByte cb = writer.getDirectContent();

				// Barcode for patientIDNo
				Barcode128 code128 = new Barcode128();
				code128.setCode(String.valueOf(patientIDNO));
				code128.setCodeType(Barcode128.CODE128);
				Image patientIDImage = code128.createImageWithBarcode(cb, null, null);
				patientIDImage.setAbsolutePosition(30, 200);
				patientIDImage.scalePercent(55);

				// Barcode for BDRNo
				Barcode128 code1281 = new Barcode128();
				code1281.setCode(String.valueOf(bbrNo));
				code1281.setCodeType(Barcode128.CODE128);
				Image patientbdrNoImage = code1281.createImageWithBarcode(cb, null, null);
				patientbdrNoImage.setAbsolutePosition(30, 200);
				patientbdrNoImage.scalePercent(55);

				/*
				 * For Receipt type
				 */
				PdfPTable table0 = new PdfPTable(3);
				table0.setWidthPercentage(100);
				Rectangle rect0 = new Rectangle(270, 700);
				table0.setWidthPercentage(new float[] { 90, 90, 90 }, rect0);

				PdfPCell cell0 = new PdfPCell(new Paragraph("Patient Name : " + patientName, mainContent));
				cell0.setPaddingBottom(5);
				cell0.setBorderWidthBottom(1f);
				cell0.setBorderColor(BaseColor.WHITE);

				PdfPCell cell01 = new PdfPCell(new Paragraph("Net Receivable : " + netReceivableAmt, mainContent));
				cell01.setPaddingBottom(5);
				cell01.setBorderWidthBottom(1f);
				cell01.setBorderColor(BaseColor.WHITE);

				PdfPCell cell02 = new PdfPCell(
						new Paragraph("Outstanding Paid Date : " + outstandingPaidDate, mainContent));
				cell02.setPaddingBottom(5);
				cell02.setBorderWidthBottom(1f);
				cell02.setBorderColor(BaseColor.WHITE);

				PdfPCell cell03 = new PdfPCell(new Paragraph("BDR No. : ", mainContent));
				cell03.setPaddingBottom(5);
				cell03.setBorderWidthBottom(1f);
				cell03.setBorderColor(BaseColor.WHITE);

				PdfPCell cell04 = new PdfPCell(new Paragraph("Actual Payment : " + actualPayment, mainContent));
				cell04.setPaddingBottom(5);
				cell04.setBorderWidthBottom(1f);
				cell04.setBorderColor(BaseColor.WHITE);

				PdfPCell cell05 = new PdfPCell(
						new Paragraph("Outstanding Paid Amount : " + outstandingPaidAmt, mainContent));
				cell05.setPaddingBottom(5);
				cell05.setBorderWidthBottom(1f);
				cell05.setBorderColor(BaseColor.WHITE);

				// Creating table for barcode image
				PdfPTable tableOD = new PdfPTable(2);

				PdfPCell imageCellOD = new PdfPCell(patientIDImage, true);
				imageCellOD.setBorderColor(BaseColor.WHITE);
				imageCellOD.setFixedHeight(20f);

				PdfPCell textCellOD = new PdfPCell(new Paragraph("Patient Barcode : ", mainContent));
				textCellOD.setBorderColor(BaseColor.WHITE);

				tableOD.addCell(textCellOD);
				tableOD.addCell(imageCellOD);

				PdfPCell cell06 = new PdfPCell(tableOD);
				cell06.setPaddingBottom(5);
				cell06.setBorderWidthBottom(1f);
				cell06.setBorderColor(BaseColor.WHITE);

				PdfPCell cell07 = new PdfPCell(new Paragraph("Outstanding Amount : " + outstandingAmt, mainContent));
				cell07.setPaddingBottom(5);
				cell07.setBorderWidthBottom(1f);
				cell07.setBorderColor(BaseColor.WHITE);

				PdfPCell cell08 = new PdfPCell(new Paragraph("Receipt By : " + userFullName, mainContent));
				cell08.setPaddingBottom(5);
				cell08.setBorderWidthBottom(1f);
				cell08.setBorderColor(BaseColor.WHITE);

				PdfPCell cell09 = new PdfPCell(new Paragraph("Receipt No. : " + receiptNo, mainContent));
				cell09.setPaddingBottom(5);
				cell09.setPaddingTop(-5);
				cell09.setBorderWidthBottom(1f);
				cell09.setBorderColor(BaseColor.WHITE);

				PdfPCell cell010 = new PdfPCell(new Paragraph("Payment Type : " + paymentType, mainContent));
				cell010.setPaddingBottom(5);
				cell010.setPaddingTop(-5);
				cell010.setBorderWidthBottom(1f);
				cell010.setBorderColor(BaseColor.WHITE);

				PdfPCell cell012 = new PdfPCell(new Paragraph("Date of Receipt : " + receiptDate, mainContent));
				cell012.setPaddingBottom(-5);
				cell012.setBorderWidthBottom(1f);
				cell012.setBorderColor(BaseColor.WHITE);

				PdfPCell cell013 = new PdfPCell(new Paragraph("TDS Amount : " + tdsAmt, mainContent));
				cell013.setPaddingBottom(5);
				cell013.setBorderWidthBottom(1f);
				cell013.setBorderColor(BaseColor.WHITE);

				// Creating table for receipt barcode image
				PdfPTable tableOD1 = new PdfPTable(2);

				PdfPCell imageCellOD1 = new PdfPCell(patientbdrNoImage, true);
				imageCellOD1.setBorderColor(BaseColor.WHITE);
				imageCellOD1.setFixedHeight(20f);

				PdfPCell textCellOD1 = new PdfPCell(new Paragraph("Receipt Barcode : ", mainContent));
				textCellOD1.setBorderColor(BaseColor.WHITE);

				tableOD1.addCell(textCellOD1);
				tableOD1.addCell(imageCellOD1);

				PdfPCell cell015 = new PdfPCell(tableOD1);
				cell015.setPaddingBottom(5);
				cell015.setBorderWidthBottom(1f);
				cell015.setBorderColor(BaseColor.WHITE);

				PdfPCell cell016 = new PdfPCell(new Paragraph("Ref Receipt Number : " + refReceiptNo, mainContent));
				cell016.setPaddingBottom(5);
				cell016.setBorderWidthBottom(1f);
				cell016.setBorderColor(BaseColor.WHITE);

				PdfPCell cell018 = new PdfPCell(new Paragraph("Receipt Details", Font1));
				cell018.setPaddingBottom(15);
				cell018.setBorderWidthBottom(1f);
				cell018.setColspan(3);
				cell018.setBorderColor(BaseColor.WHITE);

				table0.addCell(cell0);
				table0.addCell(cell01);
				table0.addCell(cell02);
				table0.addCell(cell03);
				table0.addCell(cell04);
				table0.addCell(cell05);
				table0.addCell(cell06);
				table0.addCell(cell07);
				table0.addCell(cell08);
				table0.addCell(cell09);
				table0.addCell(cell010);
				table0.addCell(cell012);
				table0.addCell(cell013);
				table0.addCell(cell015);
				table0.addCell(cell016);
				table0.addCell(cell018);

				document.add(table0);

				/*
				 * For Component details table
				 */
				PdfPTable table3 = new PdfPTable(6);
				table3.setWidthPercentage(100);
				Rectangle rect2 = new Rectangle(310, 700);
				table3.setWidthPercentage(new float[] { 20, 125, 40, 30, 35, 35 }, rect2);

				/*
				 * For blank space
				 */
				PdfPCell cell18 = new PdfPCell(new Paragraph("", Font2));
				cell18.setColspan(6);
				cell18.setBorderColor(BaseColor.WHITE);

				/*
				 * Creating header titles for table
				 */
				// For Sr.No.
				PdfPCell cell19 = new PdfPCell(new Paragraph("Sr.No.", Font1));
				cell19.setBorderWidth(0.01f);
				cell19.setPaddingBottom(3);
				cell19.setBorderColor(BaseColor.GRAY);

				// For Product req.
				PdfPCell cell20 = new PdfPCell(new Paragraph("Product Req.", Font1));
				cell20.setBorderWidth(0.01f);
				cell20.setPaddingBottom(3);
				cell20.setBorderColor(BaseColor.GRAY);

				// For Bag No
				PdfPCell cell21 = new PdfPCell(new Paragraph("Bag No.", Font1));
				cell21.setBorderWidth(0.01f);
				cell21.setPaddingBottom(3);
				cell21.setBorderColor(BaseColor.GRAY);

				// For Quantity
				PdfPCell cell22 = new PdfPCell(new Paragraph("Quantity", Font1));
				cell22.setBorderWidth(0.01f);
				cell22.setPaddingBottom(3);
				cell22.setBorderColor(BaseColor.GRAY);

				// For Rate
				PdfPCell cell23 = new PdfPCell(new Paragraph("Rate", Font1));
				cell23.setBorderWidth(0.01f);
				cell23.setPaddingBottom(3);
				cell23.setBorderColor(BaseColor.GRAY);

				// For Amount
				PdfPCell cell24 = new PdfPCell(new Paragraph("Amount", Font1));
				cell24.setBorderWidth(0.01f);
				cell24.setPaddingBottom(3);
				cell24.setBorderColor(BaseColor.GRAY);

				table3.addCell(cell18);
				table3.addCell(cell19);
				table3.addCell(cell20);
				table3.addCell(cell21);
				table3.addCell(cell22);
				table3.addCell(cell23);
				table3.addCell(cell24);

				/*
				 * For Receipt items values
				 */
				while (resultSet4.next()) {

					// For Sr.No.
					PdfPCell cell37 = new PdfPCell(new Paragraph("" + srNo, mainContent));
					cell37.setBorderWidth(0.01f);
					cell37.setPaddingBottom(3);
					cell37.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell37.setBorderColor(BaseColor.GRAY);

					// For Product req.
					PdfPCell cell38 = new PdfPCell(new Paragraph(resultSet4.getString("product"), mainContent));
					cell38.setBorderWidth(0.01f);
					cell38.setPaddingBottom(3);
					cell38.setBorderColor(BaseColor.GRAY);

					int receiptItemID = resultSet4.getInt("id");

					/*
					 * Retrieving BagNo based on receiptItemID from BagLog table
					 */
					String bagNo = billingDAOInf.retrieveBagNoByReceiptItemID(receiptItemID);

					// For Bag No.
					PdfPCell cell39 = new PdfPCell(new Paragraph("" + bagNo, mainContent));
					cell39.setBorderWidth(0.01f);
					cell39.setPaddingBottom(3);
					cell39.setBorderColor(BaseColor.GRAY);

					// For Bag No.
					PdfPCell cell40 = new PdfPCell(new Paragraph("" + resultSet4.getInt("quantity"), mainContent));
					cell40.setBorderWidth(0.01f);
					cell40.setPaddingBottom(3);
					cell40.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell40.setBorderColor(BaseColor.GRAY);

					// For Rate
					PdfPCell cell41 = new PdfPCell(new Paragraph("" + resultSet4.getDouble("rate"), mainContent));
					cell41.setBorderWidth(0.01f);
					cell41.setPaddingBottom(3);
					cell41.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell41.setBorderColor(BaseColor.GRAY);

					// For Amount
					PdfPCell cell42 = new PdfPCell(new Paragraph("" + resultSet4.getDouble("amount"), mainContent));
					cell42.setBorderWidth(0.01f);
					cell42.setPaddingBottom(3);
					cell42.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell42.setBorderColor(BaseColor.GRAY);

					srNo++;

					table3.addCell(cell37);
					table3.addCell(cell38);
					table3.addCell(cell39);
					table3.addCell(cell40);
					table3.addCell(cell41);
					table3.addCell(cell42);

				}

				/*
				 * For Other charges
				 */
				while (resultSet5.next()) {

					PdfPCell cell45 = new PdfPCell(new Paragraph("Other Charges", Font1));
					cell45.setBorderWidth(0.01f);
					cell45.setPaddingBottom(3);
					cell45.setColspan(4);
					cell45.setHorizontalAlignment(Element.ALIGN_RIGHT);
					cell45.setBorderColor(BaseColor.GRAY);

					PdfPCell cell46 = new PdfPCell(new Paragraph(resultSet5.getString("chargeType"), mainContent));
					cell46.setBorderWidth(0.01f);
					cell46.setPaddingBottom(3);
					cell46.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell46.setBorderColor(BaseColor.GRAY);

					PdfPCell cell47 = new PdfPCell(
							new Paragraph("" + resultSet5.getDouble("otherCharges"), mainContent));
					cell47.setBorderWidth(0.01f);
					cell47.setPaddingBottom(3);
					cell47.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell47.setBorderColor(BaseColor.GRAY);

					table3.addCell(cell45);
					table3.addCell(cell46);
					table3.addCell(cell47);

				}

				/*
				 * For Concession
				 */
				PdfPCell cell85 = new PdfPCell(new Paragraph("Concession", Font1));
				cell85.setBorderWidth(0.01f);
				cell85.setPaddingBottom(3);
				cell85.setColspan(4);
				cell85.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell85.setBorderColor(BaseColor.GRAY);

				PdfPCell cell86 = new PdfPCell(new Paragraph(concession, mainContent));
				cell86.setBorderWidth(0.01f);
				cell86.setPaddingBottom(3);
				cell86.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell86.setBorderColor(BaseColor.GRAY);

				PdfPCell cell87 = new PdfPCell(new Paragraph("" + concessionAmt, mainContent));
				cell87.setBorderWidth(0.01f);
				cell87.setPaddingBottom(3);
				cell87.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell87.setBorderColor(BaseColor.GRAY);

				table3.addCell(cell85);
				table3.addCell(cell86);
				table3.addCell(cell87);

				document.add(table3);

				/*
				 * Checking whether paymentType is Card or Cheque, if its cheque
				 * then showing cheque details
				 */
				if (paymentType.equals("Cheque")) {

					/*
					 * For Receipt type
					 */
					PdfPTable table1 = new PdfPTable(3);
					table1.setWidthPercentage(100);
					Rectangle rect1 = new Rectangle(270, 700);
					table1.setWidthPercentage(new float[] { 90, 90, 90 }, rect1);

					PdfPCell cell40 = new PdfPCell(new Paragraph("\n", mainContent));
					cell40.setPaddingBottom(5);
					cell40.setColspan(3);
					cell40.setBorderWidthBottom(1f);
					cell40.setBorderColor(BaseColor.WHITE);

					PdfPCell cell41 = new PdfPCell(new Paragraph("Cheque Details", Font1));
					cell41.setPaddingBottom(5);
					cell41.setBorderWidthBottom(1f);
					cell41.setColspan(3);
					cell41.setBorderColor(BaseColor.WHITE);

					PdfPCell cell42 = new PdfPCell(new Paragraph("Cheque Issued By : " + chequeIssuedBy, mainContent));
					cell42.setPaddingBottom(5);
					cell42.setBorderWidthBottom(1f);
					cell42.setBorderColor(BaseColor.WHITE);

					PdfPCell cell43 = new PdfPCell(new Paragraph("Cheque No. : " + chequNo, mainContent));
					cell43.setPaddingBottom(5);
					cell43.setBorderWidthBottom(1f);
					cell43.setBorderColor(BaseColor.WHITE);

					PdfPCell cell44 = new PdfPCell(new Paragraph("Bank Name : " + chequBankName, mainContent));
					cell44.setPaddingBottom(5);
					cell44.setBorderWidthBottom(1f);
					cell44.setBorderColor(BaseColor.WHITE);

					PdfPCell cell45 = new PdfPCell(new Paragraph("Bank Branch : " + chequeBankBranch, mainContent));
					cell45.setPaddingBottom(5);
					cell45.setBorderWidthBottom(1f);
					cell45.setBorderColor(BaseColor.WHITE);

					PdfPCell cell46 = new PdfPCell(new Paragraph("Cheque Date : " + chequDate, mainContent));
					cell46.setPaddingBottom(5);
					cell46.setBorderWidthBottom(1f);
					cell46.setBorderColor(BaseColor.WHITE);

					PdfPCell cell47 = new PdfPCell(new Paragraph("Cheque Amount : " + chequeAmt, mainContent));
					cell47.setPaddingBottom(5);
					cell47.setBorderWidthBottom(1f);
					cell47.setBorderColor(BaseColor.WHITE);

					table1.addCell(cell40);
					table1.addCell(cell41);
					table1.addCell(cell42);
					table1.addCell(cell43);
					table1.addCell(cell44);
					table1.addCell(cell45);
					table1.addCell(cell46);
					table1.addCell(cell47);

					document.add(table1);

				}

				if (multipleIDcheck > 1) {

					PdfPTable table2 = new PdfPTable(1);
					table2.setWidthPercentage(100);
					Rectangle rect3 = new Rectangle(270, 700);
					table2.setWidthPercentage(new float[] { 270 }, rect3);

					PdfPCell cell88 = new PdfPCell(new Paragraph("\n", mainContent));
					cell88.setPaddingBottom(5);
					cell88.setBorderWidthBottom(1f);
					cell88.setBorderColor(BaseColor.WHITE);

					PdfPCell cell89 = new PdfPCell(new Paragraph("", mainContent));
					cell89.setPaddingBottom(5);
					cell89.setBorderWidthBottom(1f);
					cell89.setBorderColorBottom(BaseColor.BLACK);
					cell89.setBorderColor(BaseColor.WHITE);

					PdfPCell cell90 = new PdfPCell(new Paragraph("\n", mainContent));
					cell90.setPaddingBottom(5);
					cell90.setBorderWidthBottom(1f);
					cell90.setBorderColor(BaseColor.WHITE);

					table2.addCell(cell88);
					table2.addCell(cell89);
					table2.addCell(cell90);

					document.add(table2);

				}

				multipleIDcheck++;

				srNo = 1;

			}

			document.close();

			System.out.println("Successfully written and generated card/Cheque report PDF");

			status = "success";

			/*
			 * Closing resultSet, preparedStatement, connection objects
			 */
			resultSet6.close();
			preparedStatement6.close();

			resultSet5.close();
			preparedStatement5.close();

			resultSet4.close();
			preparedStatement4.close();

			resultSet3.close();
			preparedStatement3.close();

			resultSet1.close();
			preparedStatement1.close();

			connection.close();

		} catch (Exception exception) {
			exception.printStackTrace();

			status = "error";
		}

		return status;
	}

}
