package rudhira.util;

/**
 * 
 * @author Kovid Bionalytics
 * 
 */
public class QueryMaker {

	public static final String VERIFY_USER_CREDENTIALS = "SELECT * FROM AppUser WHERE activityStatus = ?";

	public static final String RETRIVE_LOCKED_USER_STATUS = "SELECT activityStatus FROM AppUser WHERE username = ? ";

	public static final String RETRIEVE_USER_ID_FROM_LOGIN_ATTEMPT = "SELECT userID FROM LoginAttempt WHERE userID = ? AND activityStatus = ?";

	public static final String INSERT_INTO_LOGIN_ATTEMPT = "INSERT INTO LoginAttempt (attemptCounter,userID, dateAndTime, activityStatus) VALUES (?,?,?,?) ";

	public static final String UPDATE_LOGIN_ATTEMPT = "UPDATE LoginAttempt SET attemptCounter = ?  WHERE userID = ? AND dateAndTime = ?";

	public static final String UPDATE_USER_STATUS = "UPDATE AppUser SET activityStatus = ?  WHERE id = ? ";

	public static final String RETRIEVE_USER_ID_BY_USERNAME = "SELECT id FROM AppUser WHERE username = ?";

	public static final String RETRIEVE_USER_DETAILS = "SELECT id, firstName, middleName, lastName, email, username, role, activityStatus, bloodBankID, profilePic FROM AppUser WHERE username = ?";

	public static final String RETRIEVE_LOGIN_ATTEMPT_DATE = "SELECT dateAndTime FROM LoginAttempt WHERE userID = ? AND activityStatus = ?";

	public static final String UPDATE_LOGIN_ATTEMPT_STATUS = "UPDATE LoginAttempt SET activityStatus = ?  WHERE userID = ? ";

	public static final String RETRIEVE_BLOOD_BANK_LIST = "SELECT id, name FROM BloodBank";

	public static final String VERIFY_USERNAME_EXIST = "SELECT username FROM AppUser WHERE username = ?";

	public static final String INSERT_USER_DETAILS = "INSERT INTO AppUser (firstName, middleName, lastName, phone, address, city, state, country, email, username, password, role, activityStatus, bloodBankID, profilePic, pin) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

	public static final String RETRIEVE_EDIT_USER_DETAILS = "SELECT id, firstName, middleName, lastName, username, role, activityStatus FROM AppUser ORDER BY activityStatus ASC";

	public static final String RETRIEVE_EDIT_USER_DETAILS_BY_USER_ID = "SELECT * FROM AppUser WHERE id = ?";

	public static final String UPDATE_USER_DETAILS = "UPDATE AppUser SET firstName = ?, middleName = ?, lastName = ?, phone = ?, address = ?, city = ?, state = ?, country = ?, email = ?, username = ?, password = ?, role = ?, bloodBankID = ?, activityStatus = ?, profilePic = ?, pin = ? WHERE id = ?";

	public static final String SEARCH_USER_BY_USER_NAME = "SELECT id, firstName, middleName, lastName, username, role, activityStatus FROM AppUser WHERE CONCAT(firstName,' ',middleName,' ',lastName) LIKE ? ORDER BY activityStatus ASC";

	public static final String DISABLE_USER_BY_USER_ID = "UPDATE AppUser SET activityStatus = ? WHERE id = ?";

	public static final String INSERT_PASSWORD_HISTORY = "INSERT INTO PasswordHistory (password, userID) VALUES (?,?)";

	public static final String VERIFY_PASSWORD = "SELECT password FROM AppUser WHERE id = ? AND password = ?";

	public static final String VERIFY_PIN_CHANGE = "SELECT pin FROM AppUser WHERE id = ? AND pin = ?";

	public static final String VERIFY_PASSWORD_HISTORY = "SELECT password FROM PasswordHistory WHERE userID = ? AND password = ? ORDER BY id DESC LIMIT 5";

	public static final String RETRIEVE_BLOOD_BANK_NAME = "SELECT name FROM BloodBank WHERE id = ?";

	public static final String INSERT_BLOOD_BANK_DETALS = "INSERT INTO BloodBank (name, registrationNo, logo, address, phone1, phone2, email, website, tagline) VALUES (?,?,?,?,?,?,?,?,?)";

	public static final String UPDATE_BLOOD_BANK_DETALS = "UPDATE BloodBank SET name = ?, registrationNo = ?, logo = ?, address = ?, phone1 = ?, phone2 = ?, email = ?, website = ?, tagline = ? WHERE id = ? ";

	public static final String VERIFY_BLOOD_BANK_NAME_EXIST = "SELECT name FROM BloodBank WHERE name = ?";

	public static final String SEARCH_BLOOD_BANK_BY_NAME = "SELECT id, name, registrationNo, email FROM BloodBank WHERE name LIKE ?";

	public static final String RETRIEVE_EDIT_BLOOD_BANK_LIST = "SELECT id, name, registrationNo, email FROM BloodBank";

	public static final String RETRIEVE_EDIT_BLOOD_BANK_LIST_BY_ID = "SELECT * FROM BloodBank WHERE id = ?";

	public static final String VERIFY_BLOOD_BANK_NAME_EXIST1 = "SELECT name FROM BloodBank WHERE name = ? AND id <> ?";

	public static final String RETRIEVE_BLOOD_BANK_LOGO = "SELECT logo FROM BloodBank WHERE id = ?";

	public static final String VERIFY_USER_PIN = "SELECT pin from AppUser where pin = ? AND id = ?";

	public static final String VERIFY_USER_OLD_PASSWORD = "SELECT password from AppUser where password = ? AND id = ?";

	public static final String UPDATE_PASSWORD = "UPDATE AppUser set password = ? WHERE id = ?";

	public static final String UPDATE_PIN = "UPDATE AppUser set pin = ? WHERE id = ?";

	public static final String UPDATE_PASSWORD_AND_PIN = "UPDATE AppUser set pin = ?, password = ? WHERE id = ?";

	public static final String INSERT_AUDIT_DETAILS = "INSERT INTO Audit (userid, ipaddress, action) VALUES (?,?,?)";

	public static final String SEARCH_CONCESSION_BY_TYPE = "SELECT id, concessionType, concessionAmt FROM PVConcession WHERE concessionType LIKE ? AND bloodBankID = ?";

	public static final String INSERT_CONCESSION_DETAILS = "INSERT INTO PVConcession (concessionType, concessionAmt, bloodBankID) VALUES (?,?,?)";

	public static final String UPDATE_CONCESSION_DETAILS = "UPDATE PVConcession SET concessionType = ?, concessionAmt = ?, bloodBankID = ? WHERE id = ? ";

	public static final String RETRIEVE_CONCESSION_LIST_BY_ID = "SELECT * FROM PVConcession WHERE id = ?";

	public static final String DELETE_CONCESSION = "DELETE FROM PVConcession WHERE id = ?";

	public static final String SEARCH_CHARGE_BY_TYPE = "SELECT id, chargeType, charges FROM PVOtherCharges WHERE chargeType LIKE ? AND bloodBankID = ?";

	public static final String INSERT_CHARGE_TYPE_DETAILS = "INSERT INTO PVOtherCharges (chargeType, charges, bloodBankID) VALUES (?,?,?)";

	public static final String UPDATE_CHARGE_TYPE_DETAILS = "UPDATE PVOtherCharges SET chargeType = ?, charges = ?, bloodBankID = ? WHERE id = ? ";

	public static final String RETRIEVE_CHARGE_TYPE_LIST_BY_ID = "SELECT * FROM PVOtherCharges WHERE id = ?";

	public static final String DELETE_CHARGE_TYPE = "DELETE FROM PVOtherCharges WHERE id = ?";

	public static final String SEARCH_COMPONENTS = "SELECT id, component, category, price FROM PVComponents WHERE component LIKE ? AND bloodBankID = ?";

	public static final String INSERT_COMPONENTS_DETAILS = "INSERT INTO PVComponents (component, category, price, bloodBankID) VALUES (?,?,?,?)";

	public static final String UPDATE_COMPONENTS_DETAILS = "UPDATE PVComponents SET component = ?, category = ?, price = ?, bloodBankID = ? WHERE id = ?";

	public static final String RETRIEVE_COMPONENTS_LIST_BY_ID = "SELECT * FROM PVComponents WHERE id = ?";

	public static final String DELETE_COMPONENTS = "DELETE FROM PVComponents WHERE id = ?";

	public static final String SEARCH_INSTRUCTION = "SELECT id, instructions FROM Instructions WHERE instructions LIKE ? AND bloodBankID = ?";

	public static final String INSERT_INSTRUCTION_DETAILS = "INSERT INTO Instructions (instructions, bloodBankID) VALUES (?,?)";

	public static final String UPDATE_INSTRUCTION_DETAILS = "UPDATE Instructions SET instructions = ? WHERE id = ? ";

	public static final String RETRIEVE_INSTRUCTION_LIST_BY_ID = "SELECT * FROM Instructions WHERE id = ?";

	public static final String DELETE_INSTRUCTION = "DELETE FROM Instructions WHERE id = ?";

	public static final String RETRIEVE_BLOOD_BANK_ADDRESS = "SELECT address FROM BloodBank WHERE id = ?";

	public static final String RETRIEVE_BLOOD_BANK_PHONE = "SELECT phone1, phone2 FROM BloodBank WHERE id = ?";

	public static final String RETRIEVE_BLOOD_BANK_REG_NO = "SELECT registrationNo FROM BloodBank WHERE id = ?";

	public static final String RETRIEVE_COMPONENT_LIST = "SELECT component, category FROM PVComponents WHERE bloodBankID = ?";

	public static final String RETRIEVE_CONCESSION_LIST = "SELECT concessionType FROM PVConcession WHERE bloodBankID = ?";

	public static final String RETRIEVE_OTHER_CHARGES_LIST = "SELECT chargeType FROM PVOtherCharges WHERE bloodBankID = ?";

	public static final String RETRIEVE_RATE_BASED_ON_COMPONENT = "SELECT price FROM PVComponents WHERE component = ? AND category = ? AND bloodBankID = ?";

	public static final String RETRIEVE_RATE_BASED_ON_CONCESSION = "SELECT concessionAmt FROM PVConcession WHERE concessionType LIKE ? AND bloodBankID = ?";

	public static final String RETRIEVE_RATE_BASED_ON_CHARGE_TYPE = "SELECT charges FROM PVOtherCharges WHERE chargeType = ? AND bloodBankID = ?";

	public static final String RETRIEVE_CROSS_MATCHING_RATE = "SELECT charges FROM PVOtherCharges WHERE chargeType = ?";

	public static final String RETRIEVE_ABD_SCREENING_RATE = "SELECT charges FROM PVOtherCharges WHERE chargeType = ?";

	public static final String RETRIEVE_LAST_RECEIPT_NO = "SELECT receiptNo FROM Receipt";

	public static final String INSERT_PATIENT_DETAILS = "INSERT INTO Patient (patientIdentifier, firstName, middleName, lastName, address, barcode, hospital,bloodBankStorage, mobile) VALUES (?,?,?,?,?,?,?,?,?)";

	public static final String RETRIEVE_PATIENT_ID_BASED_ON_INDENTIFIER_NO = "SELECT id FROM Patient WHERE patientIdentifier = ?";

	public static final String INSERT_RECEIPT_DETAILS = "INSERT INTO Receipt (receiptNo, totalAmt, concessionType, concessionAmt, netReceivableAmt, actualPayment, outstandingAmt, receiptBy, refReceiptNumber, outstandingPaidDate, outstandingPaidAmt, patientID, bdrNo, receiptType, paymentType, tdsAmt, charityCaseNo, manualReceiptNo) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

	public static final String RETRIEVE_RECEIPT_ID_BASED_ON_RECEIPT_NO = "SELECT id FROM Receipt WHERE receiptNo = ?";

	public static final String INSERT_RECEIPT_ITEMS = "INSERT INTO ReceiptItems (product, quantity, rate, receiptID, amount) VALUES (?,?,?,?,?)";

	public static final String INSERT_OTHER_CHARGES = "INSERT INTO OtherCharges (chargeType, otherCharges, receiptID) VALUES (?,?,?)";

	public static final String RETRIEVE_LAST_ENTERED_RECEIPT_ITEM_ID = "SELECT id FROM ReceiptItems";

	public static final String INSERT_BAG_DETAILS = "INSERT INTO BagLog (receiptItemID, bagNo, bloodGroup) VALUES (?,?,?)";

	public static final String BILL_PDF_RETRIEVE_BLOOD_BANK_DETAILS = "SELECT * FROM BloodBank WHERE id = ?";

	public static final String BILL_PDF_RETRIEVE_PATIENT_DETAILS = "SELECT * FROM Patient WHERE patientIdentifier = ?";

	public static final String BILL_PDF_RETRIEVE_BLOOD_BANK_STORAGE_CENTER_DETAILS = "SELECT bloodBankStorage FROM Patient WHERE bloodBankStorage = ?";

	public static final String BILL_PDF_RETRIEVE_PATIENT_DETAILS1 = "SELECT * FROM Patient WHERE id = ?";

	public static final String BILL_PDF_RETRIEVE_RECEIPT_DETAILS1 = "SELECT receiptNo, receiptDate, receiptType, manualReceiptNo FROM Receipt WHERE id = ?";

	public static final String BILL_PDF_RETRIEVE_RECEIPT_DETAILS2 = "SELECT * FROM Receipt WHERE id = ?";

	public static final String BILL_PDF_RETRIEVE_RECEIPT_ITEM_DETAILS = "SELECT * FROM ReceiptItems WHERE receiptID = ?";

	public static final String BILL_PDF_RETRIEVE_RECEIPT_ITEM_COUNT = "SELECT count(*) AS count FROM ReceiptItems WHERE receiptID = ?";

	public static final String BILL_PDF_RETRIEVE_OTHER_CHARGES = "SELECT * FROM OtherCharges WHERE receiptID = ?";

	public static final String BILL_PDF_RETRIEVE_OTHER_CHARGES1 = "SELECT otherCharges FROM OtherCharges WHERE receiptID = ?";

	public static final String BILL_PDF_RETRIEVE_CHEQUE_DETAILS = "SELECT * FROM ChequeDetails WHERE receiptID = ?";

	public static final String BILL_PDF_RETRIEVE_INSTRUCTIONS_DETAILS = "SELECT * FROM Instructions WHERE bloodBankID = ?";

	public static final String BILL_PDF_RETRIEVE_BAG_DETAILS = "SELECT bagNo, bloodGroup FROM BagLog WHERE receiptItemID IN (SELECT id FROM ReceiptItems WHERE receiptID = ?)";

	public static final String BILL_PDF_RETRIEVE_BAG_COUNT = "SELECT count(*) AS count FROM BagLog WHERE receiptItemID IN (SELECT id FROM ReceiptItems WHERE receiptID = ?)";

	public static final String BILL_PDF_RETRIEVE_BAG_DETAILS_COUNT = "SELECT  count(1) AS count, bloodGroup FROM BagLog WHERE receiptItemID IN (SELECT id FROM ReceiptItems WHERE receiptID = ?) group by bloodGroup";

	public static final String RETRIEVE_RECEIPT_BY_NAME = "SELECT firstName, middleName, lastName FROM AppUser WHERE id = ?";

	public static final String RETRIEVE_PROFILE_PIC_NAME = "SELECT profilePic FROM AppUser WHERE id = ?";

	public static final String RETRIEVE_COMPONENTS = "SELECT id, component, category, price FROM PVComponents WHERE bloodBankID = ?";

	public static final String RETRIEVE_CONCESSION = "SELECT id, concessionType, concessionAmt FROM PVConcession WHERE bloodBankID = ?";

	public static final String RETRIEVE_INSTRUCTION = "SELECT id, instructions FROM Instructions WHERE bloodBankID = ?";

	public static final String RETRIEVE_CHARGE = "SELECT id, chargeType, charges FROM PVOtherCharges WHERE bloodBankID = ?";

	public static final String RETRIEVE_PATIENT_DETILAILS_BASED_ON_BDR_NO = "SELECT r.bbrno,r.requestdate, r.receipentcontactno,p.surname,p.fname,p.mname,h.hname,r.ward, p.age,p.sex,p.patientcode,p.add1,p.add2,p.add3 FROM tblrequest r JOIN tblpatient p ON p.patientid = r.patientid LEFT JOIN tblhospitals h ON h.hid =r.hospitalid WHERE r.bbrno=? ";
	// +"AND YEAR(DATE_FORMAT(r.requestdate, '%Y-%m-%d')) = YEAR(NOW())";

	public static final String RETRIEVE_PATIENT_ID_ON_VERIFY_BDR_NO = "SELECT patientID FROM Receipt WHERE bdrNo = ?";

	public static final String RETRIEVE_PATIENT_ID_ON_VERIFY_PATIENT_ID_NO = "SELECT id FROM Patient WHERE patientIdentifier = ?";

	public static final String COUNT_TOTAL_ACTIVE_ADMINISTRATOR = "SELECT count(*) AS count FROM AppUser WHERE role = ? AND activityStatus = ? AND bloodBankID = ?";

	public static final String COUNT_TOTAL_ACTIVE_ACCOUNTANTS = "SELECT count(*) AS count FROM AppUser WHERE role = ? AND activityStatus = ? AND bloodBankID = ?";

	public static final String COUNT_TOTAL_PATIENTS = "SELECT count(*) AS count FROM Patient WHERE patientDate BETWEEN ? AND ?";

	public static final String COUNT_TOTAL_RECEIPTS = "SELECT count(*) AS count FROM Receipt WHERE receiptDate BETWEEN ? AND ? AND receiptBy IN (SELECT id FROM AppUser WHERE bloodBankID = ?)";

	public static final String COUNT_TOTAL_COMPONENTS = "SELECT count(*) AS count FROM  PVComponents WHERE bloodBankID = ?";

	public static final String COUNT_TOTAL_CHARGES = "SELECT sum(netReceivableAmt) AS count FROM  Receipt WHERE receiptDate BETWEEN ? AND ? AND receiptBy IN (SELECT id FROM AppUser WHERE bloodBankID = ?)";

	public static final String COUNT_WEEKLY_TOTAL_PATIENTS = "SELECT count(*) AS count FROM Patient WHERE YEAR(patientDate) = YEAR(NOW()) AND YEARWEEK(patientDate) = YEARWEEK(NOW())";

	public static final String COUNT_WEEKLY_TOTAL_RECEIPTS = "SELECT count(*) AS count FROM Receipt WHERE YEAR(receiptDate) = YEAR(NOW()) AND YEARWEEK(receiptDate) = YEARWEEK(NOW()) AND receiptBy IN (SELECT id FROM AppUser WHERE bloodBankID = ?)";

	public static final String COUNT_WEEKLY_RECEIPT_PENDING_PAYMENT = "SELECT count(outstandingAmt) AS count FROM Receipt WHERE YEAR(receiptDate) = YEAR(NOW()) AND YEARWEEK(receiptDate) = YEARWEEK(NOW()) AND outstandingPaidAmt = 0 AND receiptBy IN (SELECT id FROM AppUser WHERE bloodBankID = ?)";

	public static final String COUNT_TOTAL_CROSS_MATCHING = "SELECT count(*) AS count FROM ReceiptItems WHERE product = 'Gr Cross Match' AND receiptID IN (SELECT id FROM Receipt WHERE YEAR(receiptDate) = YEAR(NOW()) AND YEARWEEK(receiptDate) = YEARWEEK(NOW()) AND receiptBy IN (SELECT id FROM AppUser WHERE bloodBankID = ?))";

	public static final String COUNT_TOTAL_ABD_SCREENING = "SELECT count(*) AS count FROM ReceiptItems WHERE product = 'ABD Screening' AND receiptID IN (SELECT id FROM Receipt WHERE YEAR(receiptDate) = YEAR(NOW()) AND YEARWEEK(receiptDate) = YEARWEEK(NOW()) AND receiptBy IN (SELECT id FROM AppUser WHERE bloodBankID = ?))";

	public static final String COUNT_WEEKLY_TOTAL_BILLING = "SELECT sum(netReceivableAmt) AS count FROM  Receipt WHERE YEAR(receiptDate) = YEAR(NOW()) AND YEARWEEK(receiptDate) = YEARWEEK(NOW()) AND receiptBy IN (SELECT id FROM AppUser WHERE bloodBankID = ?)";

	public static final String COUNT_MY_WEEKLY_TOTAL_LOGINS = "SELECT count(*) AS count FROM Audit WHERE action = 'Login' AND YEAR(timeStampLog) = YEAR(NOW()) AND YEARWEEK(timeStampLog) = YEARWEEK(NOW()) AND userid = ?";

	public static final String COUNT_MY_WEEKLY_TOTAL_RECEIPT = "SELECT count(*) AS count FROM Receipt WHERE YEAR(receiptDate) = YEAR(NOW()) AND YEARWEEK(receiptDate) = YEARWEEK(NOW()) AND receiptBy = ?";

	public static final String COUNT_MY_WEEKLY_TOTAL_BILLING = "SELECT sum(netReceivableAmt) AS count FROM  Receipt WHERE YEAR(receiptDate) = YEAR(NOW()) AND YEARWEEK(receiptDate) = YEARWEEK(NOW()) AND receiptBy = ?";

	public static final String RETRIEVE_LAST_LOGIN_DATE_TIME = "SELECT timeStampLog FROM Audit WHERE action = ? AND userid = ? ORDER BY id DESC LIMIT 1, 1";

	public static final String RETRIEVE_LAST_PASSWORD_CHANGE_DATE_TIME = "SELECT timeStampLog FROM Audit WHERE action = ? AND userid = ? ORDER BY id DESC LIMIT 1";

	public static final String RETRIEVE_LAST_PIN_CHANGE_DATE_TIME = "SELECT timeStampLog FROM Audit WHERE action = ? AND userid = ? ORDER BY id DESC LIMIT 1";

	public static final String RETRIEVE_RECEIPT_NO_BY_RECEIPT_TYPE = "SELECT receiptNo, receiptDate FROM Receipt WHERE receiptNo LIKE ? AND receiptBy IN (SELECT id FROM AppUser WHERE bloodBankID = ?)";

	public static final String INSERT_CHEQUE_DETAILS = "INSERT INTO ChequeDetails (receiptID, chequeIssuedBy, chequeNumber, bankName, bankBranch, chequeDate, chequeAmt, mobileNo) VALUES (?,?,?,?,?,?,?,?)";

	public static final String RETRIEVE_BAG_NO_BY_RECEIPT_ITEM_ID = "SELECT bagNo FROM BagLog WHERE receiptItemID = ?";

	public static final String RETRIEVE_HOSPITAL_NAME = "SELECT hname FROM tblhospitals WHERE hid = ?";

	public static final String SEARCH_BILL_BY_PATIENT_NAME_OR_RECEIPT_TYPE = "SELECT a.id, a.receiptType, a.receiptDate, a.receiptNo, a.patientID, b.firstName, b.lastName FROM Receipt as a, Patient as b WHERE a.patientID = b.id AND CONCAT(firstName,' ',lastName) LIKE ? OR a.patientID = b.id AND a.receiptType = ? ORDER BY a.id DESC";

	public static final String SEARCH_BILL_BY_PATIENT_NAME_OR_RECEIPT_TYPE_BY_DATE = "SELECT a.id, a.receiptType, a.receiptDate, a.receiptNo, a.patientID, b.firstName, b.lastName FROM Receipt as a, Patient as b WHERE a.patientID = b.id AND CONCAT(firstName,' ',lastName) LIKE ? AND a.receiptDate BETWEEN ? AND ? OR a.patientID = b.id AND a.receiptType = ? AND a.receiptDate BETWEEN ? AND ? ORDER BY a.id DESC";

	public static final String RETRIEVE_ALL_RECEIPT_DETAILS = "SELECT a.id, a.receiptType, a.receiptDate, a.receiptNo, a.patientID, b.firstName, b.lastName FROM Receipt as a, Patient as b WHERE a.patientID = b.id";

	public static final String RETRIEVE_ALL_RECEIPT_DETAILS_BY_DATE = "SELECT a.id, a.receiptType, a.receiptDate, a.receiptNo, a.patientID, b.firstName, b.lastName FROM Receipt as a, Patient as b WHERE a.patientID = b.id AND a.receiptDate BETWEEN ? AND ?";

	public static final String RETRIEVE_MY_RECEIPT_DETAILS = "SELECT a.id, a.receiptType, a.receiptDate, a.receiptNo, a.patientID, b.firstName, b.lastName FROM Receipt as a, Patient as b WHERE a.patientID = b.id AND a.receiptBy = ?";

	public static final String RETRIEVE_MY_RECEIPT_DETAILS_BY_DATE = "SELECT a.id, a.receiptType, a.receiptDate, a.receiptNo, a.patientID, b.firstName, b.lastName FROM Receipt as a, Patient as b WHERE a.patientID = b.id AND a.receiptBy = ? AND a.receiptDate BETWEEN ? AND ?";

	public static final String RETRIEVE_RECEIPT_BY_ID = "SELECT * FROM Receipt WHERE id = ?";

	public static final String RETRIEVE_CHEQUE_DETAILS = "SELECT * FROM ChequeDetails WHERE receiptID = ?";

	public static final String RETRIEVE_PATIENT_DETAILS = "SELECT * FROM Patient WHERE id = ?";

	public static final String RETRIEVE_COMPONENT_BY_RECEIPT_ID = "SELECT * FROM ReceiptItems WHERE receiptID = ?";

	public static final String RETRIEVE_OTHER_CHARGES_BY_RECEIPT_ID = "SELECT id, chargeType, otherCharges FROM OtherCharges WHERE receiptID = ?";

	public static final String RETRIEVE_PAYMENT_TYPE = "SELECT paymentType FROM Receipt WHERE id = ?";

	public static final String RETRIEVE_RECEIPT_TYPE = "SELECT receiptType FROM Receipt WHERE id = ?";

	public static final String RETRIEVE_CONCESSION_TYPE = "SELECT concessionType FROM Receipt WHERE id = ?";

	public static final String ABD_CSR_CHECK_BY_RECEIPT_ID = "SELECT product FROM ReceiptItems WHERE receiptID = ?";

	public static final String RETRIEVE_CRS_MTCH_LIST_BY_RECEIPT_ID = "SELECT a.id, a.product,a.quantity,a.rate,a.amount FROM ReceiptItems as a WHERE a.receiptID = ? AND a.product =  ?";

	public static final String RETRIEVE_CRS_MTCH_ABD_SCRN_LIST_BY_RECEIPT_ID = "SELECT a.id, a.product,a.quantity,a.rate,a.amount FROM ReceiptItems as a WHERE a.receiptID = ? LIMIT 2";

	public static final String UPDATE_RECEIPT_DETAILS = "UPDATE Receipt SET receiptNo = ?, totalAmt = ?, concessionType = ?, concessionAmt = ?, netReceivableAmt = ?, actualPayment = ?, outstandingAmt = ?, receiptBy = ?, refReceiptNumber = ?, outstandingPaidDate = ?, outstandingPaidAmt = ?, patientID = ?, bdrNo = ?, receiptType = ?, paymentType = ?, tdsAmt = ?, charityCaseNo = ?, receiptDate = ? WHERE id = ?";

	public static final String VERIFY_CHEQUE_EXIST = "SELECT receiptID FROM ChequeDetails WHERE receiptID = ?";

	public static final String UPDATE_CHEQUE_DETAILS = "UPDATE ChequeDetails SET receiptID = ?, chequeIssuedBy = ?, chequeNumber = ?, bankName = ?, bankBranch = ?, chequeDate = ?, chequeAmt = ?, mobileNo = ? WHERE receiptID = ?";

	public static final String SEARCH_PATIENT_RETPORT_ON_PATIENT_NAME = "SELECT a.firstName, a.lastName, b.id, b.patientID, b.receiptNo, b.receiptDate, b.bdrNo FROM Patient as a, Receipt as b WHERE a.id = b.patientID AND CONCAT(firstName,' ',lastName) LIKE ?";

	public static final String SEARCH_PATIENT_RETPORT_ON_PATIENT_NAME_BY_DATE = "SELECT a.firstName, a.lastName, b.id, b.patientID, b.receiptNo, b.receiptDate, b.bdrNo FROM Patient as a, Receipt as b WHERE a.id = b.patientID AND CONCAT(firstName,' ',lastName) LIKE ? AND b.receiptDate BETWEEN ? AND ?";

	public static final String SEARCH_PATIENT_RETPORT_ON_RECEIPT_NO = "SELECT a.firstName, a.lastName, b.id, b.patientID, b.receiptNo, b.receiptDate, b.bdrNo FROM Patient as a, Receipt as b WHERE a.id = b.patientID AND receiptNo LIKE ?";

	public static final String SEARCH_PATIENT_RETPORT_ON_RECEIPT_NO_BY_DATE = "SELECT a.firstName, a.lastName, b.id, b.patientID, b.receiptNo, b.receiptDate, b.bdrNo FROM Patient as a, Receipt as b WHERE a.id = b.patientID AND receiptNo LIKE ? AND b.receiptDate BETWEEN ? AND ?";

	public static final String SEARCH_PATIENT_RETPORT_ON_BDR_NO = "SELECT a.firstName, a.lastName, b.id, b.patientID, b.receiptNo, b.receiptDate, b.bdrNo FROM Patient as a, Receipt as b WHERE a.id = b.patientID AND bdrNo = ?";

	public static final String SEARCH_PATIENT_RETPORT_ON_BDR_NO_BY_DATE = "SELECT a.firstName, a.lastName, b.id, b.patientID, b.receiptNo, b.receiptDate, b.bdrNo FROM Patient as a, Receipt as b WHERE a.id = b.patientID AND bdrNo = ? AND b.receiptDate BETWEEN ? AND ?";

	public static final String SEARCH_PATIENT_REPORT_ON_BAG_NO = "SELECT a.firstName, a.lastName, b.id, b.patientID, b.receiptNo, b.receiptDate, b.bdrNo, c.bagNo FROM Patient as a, Receipt as b, BagLog as c, ReceiptItems as d WHERE a.id = b.patientID AND b.id = d.receiptID AND d.id = c.receiptItemID AND bagNo LIKE ?";

	public static final String SEARCH_PATIENT_REPORT_ON_BAG_NO_BY_DATE = "SELECT a.firstName, a.lastName, b.id, b.patientID, b.receiptNo, b.receiptDate, b.bdrNo, c.bagNo FROM Patient as a, Receipt as b, BagLog as c, ReceiptItems as d WHERE a.id = b.patientID AND b.id = d.receiptID AND d.id = c.receiptItemID AND bagNo LIKE ? AND b.receiptDate BETWEEN ? AND ?";

	public static final String SEARCH_PATIENT_RETPORT_ON_STORAGE_CENTRE = "SELECT a.bloodBankStorage, b.id, b.patientID, b.receiptNo, b.receiptDate, b.bdrNo FROM Patient as a, Receipt as b WHERE a.id = b.patientID AND bloodBankStorage LIKE ?";

	public static final String SEARCH_PATIENT_RETPORT_ON_STORAGE_CENTRE_BY_DATE = "SELECT a.bloodBankStorage, b.id, b.patientID, b.receiptNo, b.receiptDate, b.bdrNo FROM Patient as a, Receipt as b WHERE a.id = b.patientID AND bloodBankStorage LIKE ? AND b.receiptDate BETWEEN ? AND ?";

	public static final String DELETE_PRODUCT_COMPONENT_FROM_RECEIPT_ITEMS = "DELETE FROM ReceiptItems WHERE id = ?";

	public static final String RETRIEVE_BAG_DETAILS = "SELECT bagNo, bloodGroup FROM BagLog WHERE receiptItemID = ?";

	public static final String SEARCH_SHIFTS = "SELECT * FROM PVShifts WHERE name LIKE ? AND bloodBankID = ?";

	public static final String RETRIEVE_ALL_SHIFTS = "SELECT * FROM PVShifts WHERE bloodBankID = ?";

	public static final String INSERT_SHIFT_DETAILS = "INSERT INTO PVShifts (name, starTime, endTime, shiftOrder, bloodBankID) VALUES (?,?,?,?,?)";

	public static final String RETRIEVE_SHIFT_LIST_BY_ID = "SELECT * FROM PVShifts WHERE id = ?";

	public static final String UPDATE_SHIFT_DETAILS = "UPDATE PVShifts SET name = ?, starTime = ?, endTime = ?, bloodBankID = ?, shiftOrder = ? WHERE id = ?";

	public static final String DELETE_SHIFTS = "DELETE FROM PVShifts WHERE id = ?";

	public static final String VERIFY_OPEN_REGISTER = "SELECT id FROM Register WHERE registerDate LIKE ? AND activityStatus = ?";

	public static final String RETRIEVE_SHIFT_TIME = "SELECT starTime, endTime, shiftOrder FROM PVShifts WHERE name = ?";

	public static final String RETRIEVE_SHIFT_NAME = "SELECT name FROM PVShifts WHERE bloodBankID = ?";

	public static final String INSERT_REGISTER = "INSERT INTO Register (registerDate, registerStartTime, handOverCash, bloodBankBalance, otherProductBalance,  expectedCashBalance, actualCashBalance, cashMismatch, cashMismatchCarryOver, activityStatus, userID) VALUES (?,?,?,?,?,?,?,?,?,?,?)";

	public static final String RETRIEVE_CASH_DEPOSITED = "SELECT cashDeposited FROM Register WHERE registerDate LIKE ? AND activityStatus = ?";

	public static final String RETRIEVE_TODAYS_CASH_DEPOSITED = "SELECT cashDeposited FROM Register WHERE registerDate LIKE ? AND activityStatus = ? AND shiftOrder = ?";

	public static final String RETRIEVE_YESTERDAYS_CASH_DEPOSITED = "SELECT cashDeposited FROM Register WHERE registerDate LIKE ? AND activityStatus = ? ORDER BY id DESC LIMIT 1";

	public static final String UPDATE_CASH_DEPOSITED = "UPDATE Register SET cashDeposited = ? WHERE registerDate = ? AND activityStatus = ?";

	public static final String RETRIEVE_ADVANCE_PAYMENT_SUM = "SELECT sum(actualPayment) as SUM FROM Receipt WHERE receiptDate LIKE ? AND receiptDate >= ? AND receiptDate <= ?";

	public static final String RETRIEVE_CASH_HANDOVER = "SELECT cashHandover FROM Register WHERE registerDate LIKE ? AND activityStatus = ?";

	public static final String RETRIEVE_TODAYS_CASH_HANDOVER = "SELECT cashHandover FROM Register WHERE registerDate LIKE ? AND activityStatus = ?";

	public static final String RETRIEVE_YESTERDAYS_CASH_HANDOVER = "SELECT cashHandover FROM Register WHERE registerDate LIKE ? AND activityStatus = ? ORDER BY id DESC LIMIT 1";

	public static final String UPDATE_REGISTER_BALANCE = "UPDATE Register SET registerBalance = ?, activityStatus = ?, cashHandoverTo = ? WHERE registerDate = ? AND activityStatus = ?";

	public static final String UPDATE_TOTAL_BILL_INTO_REGISTER = "UPDATE Register SET shiftCash = ? WHERE registerDate = ? AND activityStatus = ?";

	public static final String RETRIEVE_RECEPTIONIST_USER = "SELECT id, firstName, middleName, lastName FROM AppUser WHERE id != ? AND role = ?";

	public static final String RETRIEVE_OTHER_PRODUCT_CASH = "SELECT otherProduct FROM Register WHERE registerDate LIKE ? AND activityStatus = ?";

	public static final String RETRIEVE_TODAYS_OTHER_PRODUCT_CASH = "SELECT otherProduct FROM Register WHERE registerDate LIKE ? AND activityStatus = ? AND shiftOrder = ?";

	public static final String RETRIEVE_YESTERDAYS_OTHER_PRODUCT_CASH = "SELECT otherProduct FROM Register WHERE registerDate LIKE ? AND activityStatus = ? ORDER BY id DESC LIMIT 1";

	public static final String RETRIEVE_REGISTER_SHIFT_TIME = "SELECT registerStartTime , registerEndTime FROM Register WHERE registerDate LIKE ? AND activityStatus = ?";

	public static final String RETRIEVE_TODAYS_LAST_REGISTER_SHIFT_TIME = "SELECT registerStartTime , registerEndTime FROM Register WHERE registerDate LIKE ? AND activityStatus = ? AND shiftOrder = ?";

	public static final String RETRIEVE_YESTERDAYS_LAST_REGISTER_SHIFT_TIME = "SELECT registerStartTime , registerEndTime FROM Register WHERE registerDate LIKE ? AND activityStatus = ? ORDER BY id DESC LIMIT 1";

	public static final String INSERT_OTHER_PRODUCT = "INSERT INTO OtherProducts (amount, product, userID) VALUES (?,?,?)";

	public static final String RETRIEVE_TODAYS_OTHER_PRODUCT_AMOUNT = "SELECT sum(amount) AS SUM FROM OtherProducts WHERE transactionDate LIKE ?";

	public static final String UPDATE_OTHER_PRODUCT_INTO_REGISTER = "UPDATE Register SET otherProduct = ? WHERE registerDate = ? AND activityStatus = ?";

	public static final String RETRIEVE_BALANCE_DETAILS = "SELECT registerDate, expectedCashBalance, bankDeposit FROM Register WHERE registerDate IN (?,?)";

	public static final String RETRIEVE_CASH_HANDOVER_REPORT_DETAILS = "SELECT registerDate, (SELECT name FROM PVShifts WHERE starTime = registerStartTime AND endTime = registerEndTime) AS shift, registerBalance, cashDeposited, cashHandover, shiftCash, otherProduct FROM Register WHERE registerDate >= ? AND registerDate <= ?";

	public static final String RETRIEVE_BLOOD_GROUP = "SELECT actualbloodgroup, actualrh FROM tbldonors WHERE bloodbagno = ? AND YEAR(DATE_FORMAT(donationdate, '%Y-%m-%d')) = YEAR(NOW())";

	public static final String VERIFY_TODAYS_LAST_REGISTER_OPEN = "SELECT id FROM Register Where registerDate = ? AND activityStatus = ? AND shiftOrder = ?";

	public static final String VERIFY_YESTERDAYS_LAST_REGISTER_OPEN = "SELECT id FROM Register Where registerDate = ? AND activityStatus = ? ORDER BY id DESC LIMIT 1";

	public static final String RETRIEVE_COMPONENT_REPORT_LIST = "SELECT p.firstName, p.lastName, r.id, r.bdrNo, r.patientID, r.receiptNo, r.receiptDate, ri.product, ri.rate FROM Patient as p, Receipt as r, ReceiptItems as ri where ri.receiptID = r.id AND r.patientID = p.id AND ri.product = ?";

	public static final String RETRIEVE_COMPONENT_REPORT_LIST_FOR_ALL = "SELECT p.firstName, p.lastName, r.id, r.bdrNo, r.patientID, r.receiptNo, r.receiptDate, ri.product, ri.rate FROM Patient as p, Receipt as r, ReceiptItems as ri where ri.receiptID = r.id AND r.patientID = p.id";

	public static final String RETRIEVE_COMPONENT_REPORT_LIST_BY_DATE = "SELECT p.firstName, p.lastName, r.id, r.bdrNo, r.patientID, r.receiptNo, r.receiptDate, ri.product, ri.rate FROM Patient as p, Receipt as r, ReceiptItems as ri where ri.receiptID = r.id AND r.patientID = p.id AND ri.product = ? AND r.receiptDate BETWEEN ? AND ?";

	public static final String RETRIEVE_COMPONENT_REPORT_LIST_BY_DATE_FOR_ALL = "SELECT p.firstName, p.lastName, r.id, r.bdrNo, r.patientID, r.receiptNo, r.receiptDate, ri.product, ri.rate FROM Patient as p, Receipt as r, ReceiptItems as ri where ri.receiptID = r.id AND r.patientID = p.id AND r.receiptDate BETWEEN ? AND ?";

	public static final String SEARCH_OTHER_PRODUCT_BY_PRODUCT_NAME = "SELECT * FROM OtherProducts WHERE product LIKE ? AND YEAR(transactionDate) = YEAR(NOW())";

	public static final String RETRIEVE_ALL_OTHER_PRODUCT = "SELECT * FROM OtherProducts WHERE YEAR(transactionDate) = YEAR(NOW())";

	public static final String RETRIEVE_EXPORT_FOR_ACCOUNT_DETAILS_TODAY = "SELECT id, receiptNo, patientID, receiptType, receiptDate, (SELECT bloodBankStorage FROM Patient WHERE id = patientID) AS bloodBankStorage, (SELECT hospital FROM Patient WHERE id = patientID) AS hospital, (SELECT firstName FROM Patient WHERE id = patientID) AS firstName, (SELECT middleName FROM Patient WHERE id = patientID) AS middleName, (SELECT lastName FROM Patient WHERE id = patientID) AS lastName, netReceivableAmt, actualPayment, outstandingAmt, paymentType, concessionType, concessionAmt FROM Receipt WHERE DAY(receiptDate) = DAY(NOW()) AND YEAR(receiptDate) = YEAR(NOW()) AND MONTH(receiptDate) = MONTH(NOW()) ORDER BY receiptNo";

	public static final String RETRIEVE_EXPORT_FOR_ACCOUNT_DETAILS_THIS_WEEK = "SELECT id, receiptNo, patientID, receiptType, receiptDate, (SELECT bloodBankStorage FROM Patient WHERE id = patientID) AS bloodBankStorage, (SELECT hospital FROM Patient WHERE id = patientID) AS hospital, (SELECT firstName FROM Patient WHERE id = patientID) AS firstName, (SELECT middleName FROM Patient WHERE id = patientID) AS middleName, (SELECT lastName FROM Patient WHERE id = patientID) AS lastName, netReceivableAmt, actualPayment, outstandingAmt, paymentType, concessionType, concessionAmt FROM Receipt WHERE WEEKOFYEAR(receiptDate) = WEEKOFYEAR(NOW()) AND YEAR(receiptDate) = YEAR(NOW()) ORDER BY receiptNo";

	public static final String RETRIEVE_EXPORT_FOR_ACCOUNT_DETAILS_THIS_MONTH = "SELECT id, receiptNo, patientID, receiptType, receiptDate, (SELECT bloodBankStorage FROM Patient WHERE id = patientID) AS bloodBankStorage, (SELECT hospital FROM Patient WHERE id = patientID) AS hospital, (SELECT firstName FROM Patient WHERE id = patientID) AS firstName, (SELECT middleName FROM Patient WHERE id = patientID) AS middleName, (SELECT lastName FROM Patient WHERE id = patientID) AS lastName, netReceivableAmt, actualPayment, outstandingAmt, paymentType, concessionType, concessionAmt FROM Receipt WHERE YEAR(receiptDate) = YEAR(NOW()) AND MONTH(receiptDate) = MONTH(NOW()) ORDER BY receiptNo";

	public static final String RETRIEVE_EXPORT_FOR_ACCOUNT_DETAILS_BY_DATE = "SELECT id, receiptNo, patientID, receiptType, receiptDate, (SELECT bloodBankStorage FROM Patient WHERE id = patientID) AS bloodBankStorage, (SELECT hospital FROM Patient WHERE id = patientID) AS hospital, (SELECT firstName FROM Patient WHERE id = patientID) AS firstName, (SELECT middleName FROM Patient WHERE id = patientID) AS middleName, (SELECT lastName FROM Patient WHERE id = patientID) AS lastName, netReceivableAmt, actualPayment, outstandingAmt, paymentType, concessionType, concessionAmt FROM Receipt WHERE receiptDate BETWEEN ? AND ? ORDER BY receiptNo";

	public static final String RETRIEVE_PATIENT_ID_BASED_ON_BB_STORAGE_CENTER = "SELECT id FROM Patient WHERE bloodBankStorage = ?";

	public static final String RETRIEVE_RECEIPT_DETAILS_BY_PATIENT_ID = "SELECT rc.id, rc.receiptNo, rc.receiptDate, rc.netReceivableAmt, rc.outstandingAmt, rc.patientID, ri.product FROM Receipt AS rc, ReceiptItems AS ri WHERE rc.id = ri.receiptID AND ri.product IN ('ABD Screening','Gr Cross Match') AND rc.patientID IN (SELECT id FROM Patient WHERE patientIdentifier = ?) AND YEAR(rc.receiptDate) = YEAR(NOW())";

	public static final String RETRIEVE_EXPECTED_CASH = "SELECT sum(netReceivableAmt) AS sum FROM Receipt WHERE YEAR(receiptDate) = YEAR(NOW())";

	public static final String VERIFY_FIRST_REGISTER_ENTRY = "SELECT id FROM Register";

	public static final String INSERT_FIRST_REGISTER_DETAILS = "INSERT INTO Register (handOverCash, expectedCashBalance) VALUES (?,?)";

	public static final String RETRIEVE_OTHER_PRODUCT_CASH_BALANCE = "SELECT sum(amount) AS sum FROM OtherProducts WHERE YEAR(transactionDate) = YEAR(NOW())";

	public static final String UPDATED_FIRST_REGISTER_DETAILS = "UPDATE Register SET registerDate = ?, registerStartTime = ?, bloodBankBalance = ?, otherProductBalance = ?, expectedCashBalance = ?, actualCashBalance = ?, cashMismatch = ?, cashMismatchCarryOver = ?, activityStatus = ?, userID = ? WHERE id = ?";

	public static final String RETRIEVE_LAST_CASH_MISMATCH_CARRY_OVER = "SELECT cashMismatchCarryOver FROM Register ORDER BY id DESC LIMIT 1,1";

	public static final String RETRIEVE_PREVIOUS_EXPECTED_END_CASH = "SELECT expectedCashBalance FROM Register ORDER BY id DESC LIMIT 1";

	public static final String RETRIEVE_BANK_DEPOSITE_LIST = "SELECT * FROM BankDeposit WHERE DAY(depositDate) = DAY(NOW())";

	public static final String RETRIEVE_CASH_ADJUSTMENT_LIST = "SELECT * FROM CashAdjustment WHERE DAY(adjustmentDate) = DAY(NOW())";

	public static final String RETRIEVE_CASH_DEPOSITED_BY_ID = "SELECT cashDeposited, id, comment FROM BankDeposit WHERE id = ?";

	public static final String DELETE_CASH_DEPOSITED_BY_ID = "DELETE FROM BankDeposit WHERE id = ?";

	public static final String RETRIEVE_CASH_ADJUSTED_BY_ID = "SELECT cashAdjusted, id, reason FROM CashAdjustment WHERE id = ?";

	public static final String DELETE_CASH_ADJUSTED_BY_ID = "DELETE FROM CashAdjustment WHERE id = ?";

	public static final String RETRIEVE_REGISTER_ID = "SELECT id FROM Register ORDER BY id DESC LIMIT 1";

	public static final String INSERT_CASH_ADJUSTMENT = "INSERT INTO CashAdjustment (cashAdjusted, reason, adjustmentDate, adjustedBy, registerID) VALUES (?,?,?,?,?)";

	public static final String UPDATE_CASH_ADJUSTMENT = "UPDATE CashAdjustment SET cashAdjusted = ?, reason = ?, adjustedBy = ? WHERE id = ?";

	public static final String INSERT_CASH_DEPOSIT = "INSERT INTO BankDeposit (cashDeposited, comment, depositDate, depositedBy, registerID) VALUES (?,?,?,?,?)";

	public static final String UPDATE_CASH_DEPOSIT = "UPDATE BankDeposit SET cashDeposited = ?, comment = ?, depositedBy = ? WHERE id = ?";

	public static final String RETRIEVE_REGISTER_START_DATE_AND_TIME = "SELECT registerDate, registerStartTime FROM Register WHERE id = ?";

	public static final String RETRIEVE_BLOOD_BANK_BALANCE = "SELECT sum(netReceivableAmt) AS sum FROM Receipt WHERE receiptDate >= ?  AND paymentType = 'Cash'";

	public static final String RETRIEVE_OTHER_PRODUCT_BALANCE = "SELECT sum(amount) AS sum FROM OtherProducts WHERE transactionDate >= ?";

	public static final String RETRIEVE_TOTAL_CASH_DEPOSITED_REGISTER_ID = "SELECT sum(cashDeposited) AS sum FROM BankDeposit WHERE registerID = ?";

	public static final String RETRIEVE_TOTAL_CASH_ADJUSTED_REGISTER_ID = "SELECT sum(cashAdjusted) AS sum FROM CashAdjustment WHERE registerID = ?";

	public static final String RETRIEVE_CASH_HANDOVER_BY_ID = "SELECT handOverCash FROM Register WHERE id = ?";

	public static final String UPDATE_REGISTER = "UPDATE Register SET registerEndTime = ?, handOverCash = ?, bloodBankBalance = ?, otherProductBalance = ?, expectedCashBalance = ?, actualCashBalance = ?, bankDeposit = ?, cashMismatch = ?, cashMismatchCarryOver = ?, activityStatus = ?, cashAdjustment = ? WHERE id = ?";

	public static final String RETRIEVE_REGISTER_REPORT = "SELECT registerDate, sum(handOverCash) AS handOverCash, sum(bloodBankBalance) AS bloodBankBalance, sum(otherProductBalance) AS otherProductBalance, sum(expectedCashBalance) AS expectedCashBalance, sum(actualCashBalance) AS actualCashBalance, sum(bankDeposit) AS bankDeposit, sum(bankDeposit) AS cashMismatch, sum(cashAdjustment) AS cashAdjustment FROM Register WHERE registerDate BETWEEN ? AND ? GROUP BY registerDate";

	public static final String RETRIEVE_CONCESSION_TYPE_LIST = "SELECT concessionType FROM PVConcession WHERE bloodBankID = ?";

	public static final String RETRIEVE_CONCESSION_REPORT_LIST = "SELECT p.firstName, p.lastName, r.id, r.receiptNo, r.receiptDate, r.patientID, r.concessionType, r.concessionAmt, r.netReceivableAmt FROM Patient AS p, Receipt AS r WHERE r.patientID = p.id AND r.concessionType = ?";

	public static final String RETRIEVE_CONCESSION_REPORT_LIST_FOR_ALL = "SELECT p.firstName, p.lastName, r.id, r.receiptNo, r.receiptDate, r.patientID, r.concessionType, r.concessionAmt, r.netReceivableAmt FROM Patient AS p, Receipt AS r WHERE r.patientID = p.id AND r.concessionType <> '000'";

	public static final String RETRIEVE_CONCESSION_REPORT_LIST_BY_DATE = "SELECT p.firstName, p.lastName, r.id, r.receiptNo, r.receiptDate, r.patientID, r.concessionType, r.concessionAmt, r.netReceivableAmt FROM Patient AS p, Receipt AS r WHERE r.patientID = p.id AND r.concessionType =  ? AND r.receiptDate BETWEEN ? AND ?";

	public static final String RETRIEVE_CONCESSION_REPORT_LIST_BY_DATE_FOR_ALL = "SELECT p.firstName, p.lastName, r.id, r.receiptNo, r.receiptDate, r.patientID, r.concessionType, r.concessionAmt, r.netReceivableAmt FROM Patient AS p, Receipt AS r WHERE r.patientID = p.id AND r.receiptDate BETWEEN ? AND ? AND r.concessionType <> '000'";

	public static final String RETRIEVE_CARD_CHEQUE_REPORT_LIST = "SELECT p.firstName, p.lastName, r.id, r.receiptNo, r.receiptDate, r.patientID, r.paymentType, r.netReceivableAmt FROM Patient AS p, Receipt AS r WHERE r.patientID = p.id AND r.paymentType = ?";

	public static final String RETRIEVE_CARD_CHEQUE_REPORT_LIST_FOR_BOTH = "SELECT p.firstName, p.lastName, r.id, r.receiptNo, r.receiptDate, r.patientID, r.paymentType, r.netReceivableAmt FROM Patient AS p, Receipt AS r WHERE r.patientID = p.id AND r.paymentType IN ('Credit/Debit Card','Cheque')";

	public static final String RETRIEVE_CARD_CHEQUE_REPORT_LIST_BY_DATE = "SELECT p.firstName, p.lastName, r.id, r.receiptNo, r.receiptDate, r.patientID, r.paymentType, r.netReceivableAmt FROM Patient AS p, Receipt AS r WHERE r.patientID = p.id AND r.paymentType = ? AND r.receiptDate BETWEEN ? AND ?";

	public static final String RETRIEVE_CARD_CHEQUE_REPORT_LIST_BY_DATE_FOR_BOTH = "SELECT p.firstName, p.lastName, r.id, r.receiptNo, r.receiptDate, r.patientID, r.paymentType, r.netReceivableAmt FROM Patient AS p, Receipt AS r WHERE r.patientID = p.id AND r.paymentType IN ('Credit/Debit Card','Cheque') AND r.receiptDate BETWEEN ? AND ?";

	public static final String RETRIEVE_TOTAL_OTHER_CHARGE = "SELECT SUM(otherCharges) AS sum FROM OtherCharges WHERE receiptID = ?";

	public static final String RETRIEVE_COMPONENT_BASED_LIST = "SELECT p.firstName, p.lastName, p.middleName,p.bloodBankStorage, r.receiptNo, r.receiptDate, ri.product, ri.quantity,ri.rate, ri.amount, r.id FROM Patient AS p, Receipt AS r, ReceiptItems AS ri WHERE ri.receiptID = r.id AND r.patientID = p.id AND ri.product = ? AND r.id = ?";

	public static final String RETRIEVE_COMPONENT_BASED_LIST_BY_DATE = "SELECT p.firstName, p.lastName, p.middleName,p.bloodBankStorage, r.receiptNo, r.receiptDate, ri.product, ri.quantity,ri.rate, ri.amount, r.id FROM Patient AS p, Receipt AS r, ReceiptItems AS ri WHERE ri.receiptID = r.id AND r.patientID = p.id AND ri.product = ? AND r.receiptDate BETWEEN ? AND ?";

	public static final String RETRIEVE_BLOOD_STORAGE_NAME_LIST = "SELECT name FROM PVBloodStorageCentre ORDER BY name";

	public static final String INSERT_NEW_BLOOD_STORAGE_CENTER_NAME = "INSERT INTO PVBloodStorageCentre (name) VALUES (?)";

	public static final String VERIFY_BSC_EXISTS = "SELECT bloodBankStorage FROM Patient WHERE bloodBankStorage = ?";

	public static final String RETRIEVE_EXPORT_FOR_TALLY_ACCOUNT_DETAILS_TODAY = "SELECT (SELECT bloodBankStorage FROM Patient WHERE id = r.patientID) AS bloodBankStorage, r.id, r.receiptNo, r.patientID, r.receiptType, r.receiptDate, r.netReceivableAmt, r.actualPayment, r.outstandingAmt, r.paymentType, r.concessionType, r.concessionAmt, ri.product FROM Receipt AS r, ReceiptItems AS ri WHERE r.id = ri.receiptID AND DAY(r.receiptDate) = DAY(NOW()) AND YEAR(r.receiptDate) = YEAR(NOW()) AND MONTH(r.receiptDate) = MONTH(NOW()) ORDER BY r.receiptNo";

	public static final String RETRIEVE_EXPORT_FOR_TALLY_ACCOUNT_DETAILS_THIS_WEEK = "SELECT (SELECT bloodBankStorage FROM Patient WHERE id = r.patientID) AS bloodBankStorage,r.id, r.receiptNo, r.patientID, r.receiptType, r.receiptDate, r.netReceivableAmt, r.actualPayment, r.outstandingAmt, r.paymentType, r.concessionType, r.concessionAmt, ri.product FROM Receipt AS r, ReceiptItems AS ri WHERE r.id = ri.receiptID AND WEEKOFYEAR(r.receiptDate) = WEEKOFYEAR(NOW()) AND YEAR(r.receiptDate) = YEAR(NOW()) ORDER BY r.receiptNo";

	public static final String RETRIEVE_EXPORT_FOR_TALLY_ACCOUNT_DETAILS_THIS_MONTH = "SELECT (SELECT bloodBankStorage FROM Patient WHERE id = r.patientID) AS bloodBankStorage,r.id, r.receiptNo, r.patientID, r.receiptType, r.receiptDate, r.netReceivableAmt, r.actualPayment, r.outstandingAmt, r.paymentType, r.concessionType, r.concessionAmt, ri.product FROM Receipt AS r, ReceiptItems AS ri WHERE r.id = ri.receiptID AND YEAR(r.receiptDate) = YEAR(NOW()) AND MONTH(r.receiptDate) = MONTH(NOW()) ORDER BY r.receiptNo";

	public static final String RETRIEVE_EXPORT_FOR_TALLY_ACCOUNT_DETAILS_BY_DATE = "SELECT (SELECT bloodBankStorage FROM Patient WHERE id = r.patientID) AS bloodBankStorage,r.id, r.receiptNo, r.patientID, r.receiptType, r.receiptDate, r.netReceivableAmt, r.actualPayment, r.outstandingAmt, r.paymentType, r.concessionType, r.concessionAmt, ri.product, (SELECT bloodBankStorage FROM Patient WHERE id = r.patientID) AS bloodBankStorage FROM Receipt AS r, ReceiptItems AS ri WHERE r.id = ri.receiptID AND r.receiptDate BETWEEN ? AND ? ORDER BY r.receiptNo";

	public static final String RETRIEVE_BLOOD_STORAGE_CENTER_LIST = "SELECT id, name FROM PVBloodStoragecentre";

}
