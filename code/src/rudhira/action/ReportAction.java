package rudhira.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import rudhira.DAO.BillingDAOImpl;
import rudhira.DAO.BillingDAOInf;
import rudhira.DAO.ReportDAOImpl;
import rudhira.DAO.ReportDAOInf;
import rudhira.DAO.UserDAOImpl;
import rudhira.DAO.UserDAOInf;
import rudhira.form.ReportForm;
import rudhira.form.UserForm;
import rudhira.service.RudhiraServiceInf;
import rudhira.util.ConfigListenerUtil;
import rudhira.util.ConfigXMLUtil;
import rudhira.util.ExcelUtil;
import rudhira.util.PDFUtil;
import rudhira.util.XMLUtil;

/**
 * 
 * @author Kovid Bioanalytics
 * 
 */
public class ReportAction extends ActionSupport
		implements ModelDriven<ReportForm>, SessionAware, ServletRequestAware, ServletResponseAware {

	String message = null;

	private Map<String, Object> sessionAttriute = null;

	HttpServletRequest request;
	HttpServletResponse response;

	UserDAOInf loginDAOInf = null;
	RudhiraServiceInf serviceInf = null;

	ReportDAOInf reportDAOInf = null;

	ReportForm reportForm = new ReportForm();

	PDFUtil pdfUtil = null;

	ExcelUtil excelUtil = null;

	List<ReportForm> patientReportList;

	List<ReportForm> cashHandoverReportList;

	List<String> componentNameList;

	List<ReportForm> componentList;

	BillingDAOInf billingDAOInf = null;

	List<ReportForm> registerList;

	List<String> concessionTypeList;

	List<ReportForm> concessionList;

	List<ReportForm> cardReportList;

	List<ReportForm> storageCenterList;

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String searchPatientReport() throws Exception {

		reportDAOInf = new ReportDAOImpl();

		patientReportList = reportDAOInf.retrievePatientReportList(reportForm.getSearchPatientReportName(),
				reportForm.getPatientReportCriteria(), reportForm.getStartDate(), reportForm.getEndDate());

		// Setting storage center name into request variable
		request.setAttribute("storageCenter", reportForm.getPatientReportCriteria());

		if (patientReportList.size() > 0) {

			request.setAttribute("searchEnable", "searchEnable");

			return SUCCESS;

		} else {

			String errorMsg = "No record found.";

			addActionError(errorMsg);

			return ERROR;

		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String generatePatientBasedReport() throws Exception {

		pdfUtil = new PDFUtil();

		loginDAOInf = new UserDAOImpl();

		excelUtil = new ExcelUtil();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		/*
		 * Checking whether PDF button is clicked or Excel button, and depending upon
		 * that generating report
		 */
		if (reportForm.getSubmitButton().equals("PDF")) {

			/*
			 * Generating PDF Patient based report
			 */
			String realPath = request.getServletContext().getRealPath("/");

			String PDFFileName = realPath + "patient-BasedReport.pdf";

			message = pdfUtil.generatePatientBasedReport(reportForm, realPath, PDFFileName, userForm.getFullname());

			if (message.equalsIgnoreCase("success")) {

				addActionMessage("Patient-based report generated successfully.");

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
						"Patient-based PDF Report");

				// Setting fileinput stream and file name in order to get
				// fileinputStream and file name while downloading
				reportForm.setFileInputStream(new FileInputStream(new File(PDFFileName)));

				reportForm.setFileName("patient-BasedReport.pdf");

				return SUCCESS;

			} else {

				addActionError(
						"Failed to generate patient-based PDF report.Please check server logs for more details.");

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
						"Patient-based PDF Report Exception Occurred");

				return ERROR;

			}

		} else {

			/*
			 * Generating EXCEL Patient based report
			 */
			/*
			 * Generating PDF Patient based report
			 */
			String realPath = request.getServletContext().getRealPath("/");

			String PDFFileName = realPath + "patient-BasedReport.xls";

			message = excelUtil.generatePatientBasedReport(reportForm, realPath, PDFFileName, userForm.getFullname());

			if (message.equalsIgnoreCase("success")) {

				addActionMessage("Patient-based report generated successfully.");

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
						"Patient-based Excel Report");

				// Setting fileinput stream and file name in order to get
				// fileinputStream and file name while downloading
				reportForm.setFileInputStream(new FileInputStream(new File(PDFFileName)));

				reportForm.setFileName("patient-BasedReport.xls");

				return SUCCESS;

			} else {

				addActionError(
						"Failed to generate patient-based XLS report.Please check server logs for more details.");

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
						"Patient-based Excel Report Exception Occurred");

				return ERROR;

			}

		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String searchCashHandoverReport() throws Exception {

		reportDAOInf = new ReportDAOImpl();

		cashHandoverReportList = reportDAOInf.retrieveCashHandoverReportList(reportForm.getStartDate(),
				reportForm.getEndDate());

		if (cashHandoverReportList.size() > 0) {

			request.setAttribute("searchEnable", "searchEnable");

			return SUCCESS;

		} else {

			String errorMsg = "No record(s) found.";

			addActionError(errorMsg);

			return ERROR;

		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String generateCashHandoverReport() throws Exception {

		pdfUtil = new PDFUtil();

		loginDAOInf = new UserDAOImpl();

		excelUtil = new ExcelUtil();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		/*
		 * Checking whether PDF button is clicked or Excel button, and depending upon
		 * that generating report
		 */
		if (reportForm.getSubmitButton().equals("PDF")) {

			/*
			 * Generating PDF Patient based report
			 */
			String realPath = request.getServletContext().getRealPath("/");

			String PDFFileName = realPath + "CashHand-overReport.pdf";

			message = pdfUtil.generateCashHandoverReport(reportForm.getStartDate(), reportForm.getEndDate(), realPath,
					PDFFileName);

			if (message.equalsIgnoreCase("success")) {

				addActionMessage("Cash hand-over report generated successfully.");

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
						"Cash hand-over PDF Report");

				// Setting fileinput stream and file name in order to get
				// fileinputStream and file name while downloading
				reportForm.setFileInputStream(new FileInputStream(new File(PDFFileName)));

				reportForm.setFileName("CashHand-overReport.pdf");

				return SUCCESS;

			} else {

				addActionError("Failed to generate Cash hand-over PDF report.");

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
						"Cash hand-over PDF Report Eception Occurred");

				return ERROR;

			}

		} else {

			/*
			 * Generating EXCEL Patient based report
			 */
			/*
			 * Generating PDF Patient based report
			 */
			String realPath = request.getServletContext().getRealPath("/");

			String excelFileName = realPath + "CashHand-overReport.xls";

			message = excelUtil.generateCashHandoverReport(reportForm.getStartDate(), reportForm.getEndDate(), realPath,
					excelFileName);

			if (message.equalsIgnoreCase("success")) {

				addActionMessage("Cash hand-over report generated successfully.");

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
						"Cash hand-over Excel Report");

				// Setting fileinput stream and file name in order to get
				// fileinputStream and file name while downloading
				reportForm.setFileInputStream(new FileInputStream(new File(excelFileName)));

				reportForm.setFileName("CashHand-overReport.xls");

				return SUCCESS;

			} else {

				addActionError("Failed to generate Cash hand-over Excel report.");

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
						"Cash hand-over Excel Report Eception Occurred");

				return ERROR;

			}

		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String renderComponentReport() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		componentNameList = billingDAOInf.retrieveComponentList(userForm.getBloodBankID());

		componentNameList.add("ABD Screening");
		componentNameList.add("Gr Cross Match");

		Collections.sort(componentNameList);

		return SUCCESS;
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String searchComponentReport() throws Exception {

		reportDAOInf = new ReportDAOImpl();

		billingDAOInf = new BillingDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		componentList = reportDAOInf.retrieveComponentReportList(reportForm.getStartDate(), reportForm.getEndDate(),
				reportForm.getSearchComponentName());

		componentNameList = billingDAOInf.retrieveComponentList(userForm.getBloodBankID());

		componentNameList.add("ABD Screening");
		componentNameList.add("Gr Cross Match");

		Collections.sort(componentNameList);

		request.setAttribute("startDate", reportForm.getStartDate());
		request.setAttribute("endDate", reportForm.getEndDate());

		if (componentList.size() > 0) {

			request.setAttribute("searchEnable", "searchEnable");

			return SUCCESS;

		} else {

			String errorMsg = "No component found.";

			addActionError(errorMsg);

			return ERROR;

		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String generateComponentBasedReport() throws Exception {

		pdfUtil = new PDFUtil();

		loginDAOInf = new UserDAOImpl();

		excelUtil = new ExcelUtil();

		billingDAOInf = new BillingDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		componentNameList = billingDAOInf.retrieveComponentList(userForm.getBloodBankID());

		componentNameList.add("ABD Screening");
		componentNameList.add("Gr Cross Match");

		Collections.sort(componentNameList);

		/*
		 * Checking whether PDF button is clicked or Excel button, and depending upon
		 * that generating report
		 */
		if (reportForm.getSubmitButton().equals("PDF")) {

			/*
			 * Generating PDF Component based report
			 */
			String realPath = request.getServletContext().getRealPath("/");

			String PDFFileName = realPath + "component-BasedReport.pdf";

			message = pdfUtil.generateComponentBasedReport(reportForm, realPath, PDFFileName, userForm.getFullname());

			if (message.equalsIgnoreCase("success")) {

				addActionMessage("Component-based report generated successfully.");

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
						"Component-based PDF Report");

				// Setting fileinput stream and file name in order to get
				// fileinputStream and file name while downloading
				reportForm.setFileInputStream(new FileInputStream(new File(PDFFileName)));

				reportForm.setFileName("component-BasedReport.pdf");

				return SUCCESS;

			} else {

				addActionError(
						"Failed to generate component-based PDF report.Please check server logs for more details.");

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
						"Component-based PDF Report Exception Occurred");

				return ERROR;

			}

		} else {

			/*
			 * Generating EXCEL Component based report
			 */
			String realPath = request.getServletContext().getRealPath("/");

			String PDFFileName = realPath + "component-BasedReport.xls";

			message = excelUtil.generateComponentBasedReport(reportForm, realPath, PDFFileName, userForm.getFullname());

			if (message.equalsIgnoreCase("success")) {

				addActionMessage("Component-based report generated successfully.");

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
						"Component-based Excel Report");

				// Setting fileinput stream and file name in order to get
				// fileinputStream and file name while downloading
				reportForm.setFileInputStream(new FileInputStream(new File(PDFFileName)));

				reportForm.setFileName("component-BasedReport.xls");

				return SUCCESS;

			} else {

				addActionError(
						"Failed to generate component-based XLS report.Please check server logs for more details.");

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
						"Component-based Excel Report Exception Occurred");

				return ERROR;

			}

		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String exportForAccounts() throws Exception {

		loginDAOInf = new UserDAOImpl();

		excelUtil = new ExcelUtil();

		XMLUtil xmlUtil = new XMLUtil();

		ConfigXMLUtil configXMLUtil = new ConfigXMLUtil();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		/*
		 * Checking which button is clicked; either Excel or Tally and depending upon
		 * that downloading report
		 */
		if (reportForm.getSubmitButton().equals("Excel")) {

			String realPath = request.getServletContext().getRealPath("/");

			String excelFileName = realPath + "exportForAccounts.xls";

			message = excelUtil.generateExportForAccountsReport(reportForm, realPath, excelFileName);

			if (message.equalsIgnoreCase("success")) {

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
						"Export For Accounts Report");

				// Setting fileinput stream and file name in order to get
				// fileinputStream and file name while downloading
				reportForm.setFileInputStream(new FileInputStream(new File(excelFileName)));

				reportForm.setFileName("exportForAccounts.xls");

				return SUCCESS;

			} else {

				addActionError("Failed to generate export for accounts XLS report.");

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
						"Export For Accounts Report Exception Occurred");

				return ERROR;

			}

		} else if (reportForm.getSubmitButton().equals("TallySend")) {

			SimpleDateFormat dateTimeFormat = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");

			String tallyFilePath = configXMLUtil.getTallyFilePath();

			String xcelFile = "tally_excel_" + dateTimeFormat.format(new Date()) + ".xls";

			String excelFileName = tallyFilePath + xcelFile;

			message = excelUtil.generateExportForTallyAccountsExcel(reportForm, tallyFilePath, excelFileName);

			if (message.equalsIgnoreCase("success")) {

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
						"Send To Tally Accounts Report");

				addActionMessage("Excel file: " + xcelFile + " has been successfully sent in tally shared folder.");

				return "TallySuccess";

			} else if (message.equalsIgnoreCase("input")) {

				addActionError("No receipts found.");

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
						"Send To Tally Accounts Report Exception Occurred");

				return ERROR;

			} else if (message.equalsIgnoreCase("failed")) {

				addActionError("Failed to import voucher. Please check server logs for more details.");

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
						"Send To Tally Accounts Report Exception Occurred");

				return ERROR;

			} else if (message.equalsIgnoreCase("xmlNotFound")) {

				addActionError("Voucher XML File not found.");

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
						"Send To Tally Accounts Report Exception Occurred");

				return ERROR;

			} else if (message.equalsIgnoreCase("exception")) {

				addActionError(
						"Exception occurred while importing voucher XML file into tally. Please check server logs for more details.");

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
						"Send To Tally Accounts Report Exception Occurred");

				return ERROR;

			} else {

				addActionError("Tally server is shut down. Please check tally server is connected or not.");

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
						"Send To Tally Accounts Report Exception Occurred");

				return ERROR;

			}

		} else {

			String realPath = request.getServletContext().getRealPath("/");

			String excelFileName = realPath + "tally_excel.xls";

			message = excelUtil.generateExportForTallyAccountsExcel(reportForm, realPath, excelFileName);

			if (message.equalsIgnoreCase("success")) {

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
						"Export For Tally Accounts Report");

				// Setting fileinput stream and file name in order to get
				// fileinputStream and file name while downloading
				reportForm.setFileInputStream(new FileInputStream(new File(excelFileName)));

				reportForm.setFileName("tally_excel.xls");

				return SUCCESS;

			} else if (message.equalsIgnoreCase("input")) {

				addActionError("No receipts found.");

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
						"Export For Tally Accounts Report Exception Occurred");

				return ERROR;

			} else {

				addActionError("Failed to generate export for accounts excel report.");

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
						"Export For Tally Accounts Report Exception Occurred");

				return ERROR;

			}

		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String searchRegisterReport() throws Exception {

		reportDAOInf = new ReportDAOImpl();

		registerList = reportDAOInf.retrieveRegiterReportList(reportForm.getStartDate(), reportForm.getEndDate());

		request.setAttribute("startDate", reportForm.getStartDate());
		request.setAttribute("endDate", reportForm.getEndDate());

		if (registerList.size() > 0) {

			request.setAttribute("searchEnable", "searchEnable");

			return SUCCESS;

		} else {

			String errorMsg = "No records found.";

			addActionError(errorMsg);

			return ERROR;

		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String registerReport() throws Exception {

		pdfUtil = new PDFUtil();

		loginDAOInf = new UserDAOImpl();

		excelUtil = new ExcelUtil();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		request.setAttribute("startDate", reportForm.getStartDate());
		request.setAttribute("endDate", reportForm.getEndDate());

		/*
		 * Checking whether PDF button is clicked or Excel button, and depending upon
		 * that generating report
		 */
		if (reportForm.getSubmitButton().equals("PDF")) {

			/*
			 * Generating PDF Component based report
			 */
			String realPath = request.getServletContext().getRealPath("/");

			String PDFFileName = realPath + "registerReport_" + reportForm.getStartDate() + "-"
					+ reportForm.getEndDate() + ".pdf";

			message = pdfUtil.generateRegisterReport(reportForm.getStartDate(), reportForm.getEndDate(), realPath,
					PDFFileName);

			if (message.equalsIgnoreCase("success")) {

				addActionMessage("Register report generated successfully.");

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Register PDF Report");

				// Setting fileinput stream and file name in order to get
				// fileinputStream and file name while downloading
				reportForm.setFileInputStream(new FileInputStream(new File(PDFFileName)));

				reportForm.setFileName(
						"registerReport_" + reportForm.getStartDate() + "-" + reportForm.getEndDate() + ".pdf");

				return SUCCESS;

			} else {

				addActionError("Failed to generate register PDF report.Please check server logs for more details.");

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
						"Register PDF Report Exception Occurred");

				return ERROR;

			}

		} else {

			/*
			 * Generating EXCEL Component based report
			 */
			String realPath = request.getServletContext().getRealPath("/");

			String excelFileName = realPath + "registerReport_" + reportForm.getStartDate() + "-"
					+ reportForm.getEndDate() + ".xls";

			message = excelUtil.generateRegisterReport(reportForm.getStartDate(), reportForm.getEndDate(), realPath,
					excelFileName);

			if (message.equalsIgnoreCase("success")) {

				addActionMessage("Register report generated successfully.");

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Register Excel Report");

				// Setting fileinput stream and file name in order to get
				// fileinputStream and file name while downloading
				reportForm.setFileInputStream(new FileInputStream(new File(excelFileName)));

				reportForm.setFileName(
						"registerReport_" + reportForm.getStartDate() + "-" + reportForm.getEndDate() + ".xls");

				return SUCCESS;

			} else {

				addActionError("Failed to generate register XLS report.Please check server logs for more details.");

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
						"Register Excel Report Exception Occurred");

				return ERROR;

			}

		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String renderConcessionReport() throws Exception {

		reportDAOInf = new ReportDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		concessionTypeList = reportDAOInf.retrieveConcessionTypeList(userForm.getBloodBankID());

		Collections.sort(concessionTypeList);

		return SUCCESS;
	}

	/**
	 * 
	 * @return
	 * @throws Excpetion
	 */
	public String searchConcessionReport() throws Exception {

		reportDAOInf = new ReportDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		concessionTypeList = reportDAOInf.retrieveConcessionTypeList(userForm.getBloodBankID());

		concessionList = reportDAOInf.retrieveConcessionReportList(reportForm.getStartDate(), reportForm.getEndDate(),
				reportForm.getSearchConcessionName());

		Collections.sort(concessionTypeList);

		if (concessionList.size() > 0) {

			request.setAttribute("searchEnable", "searchEnable");

			return SUCCESS;

		} else {

			String errorMsg = "No concession found.";

			addActionError(errorMsg);

			return ERROR;

		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String generateConcessionBasedReport() throws Exception {

		pdfUtil = new PDFUtil();

		loginDAOInf = new UserDAOImpl();

		excelUtil = new ExcelUtil();

		reportDAOInf = new ReportDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		concessionTypeList = reportDAOInf.retrieveConcessionTypeList(userForm.getBloodBankID());

		/*
		 * Generating EXCEL Component based report
		 */
		String realPath = request.getServletContext().getRealPath("/");

		String excelFileName = realPath + "concession-BasedReport.xls";

		message = excelUtil.generateConcessionBasedReport(reportForm, realPath, excelFileName);

		if (message.equalsIgnoreCase("success")) {

			addActionMessage("Concession-based report generated successfully.");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
					"Concession-based Excel Report");

			// Setting fileinput stream and file name in order to get
			// fileinputStream and file name while downloading
			reportForm.setFileInputStream(new FileInputStream(new File(excelFileName)));

			reportForm.setFileName("concession-BasedReport.xls");

			return SUCCESS;

		} else {

			addActionError("Failed to generate concession-based XLS report.Please check server logs for more details.");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
					"Concession-based Excel Report Exception Occurred");

			return ERROR;

		}

	}

	/**
	 * 
	 * @return
	 * @throws Excpetion
	 */
	public String searchCardChequeReport() throws Exception {

		reportDAOInf = new ReportDAOImpl();

		billingDAOInf = new BillingDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		cardReportList = reportDAOInf.retrieveCardChequeReportList(reportForm.getStartDate(), reportForm.getEndDate(),
				reportForm.getSearchComponentName());

		if (cardReportList.size() > 0) {

			request.setAttribute("searchEnable", "searchEnable");

			return SUCCESS;

		} else {

			String errorMsg = "No records found.";

			addActionError(errorMsg);

			return ERROR;

		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String generateCardChequeReport() throws Exception {

		pdfUtil = new PDFUtil();

		loginDAOInf = new UserDAOImpl();

		excelUtil = new ExcelUtil();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		/*
		 * Checking whether PDF button is clicked or Excel button, and depending upon
		 * that generating report
		 */
		if (reportForm.getSubmitButton().equals("PDF")) {

			/*
			 * Generating PDF card/cheque based report
			 */
			String realPath = request.getServletContext().getRealPath("/");

			String PDFFileName = realPath + "cardCheque-BasedReport.pdf";

			message = pdfUtil.generateCardChequeBasedReport(reportForm, realPath, PDFFileName, userForm.getFullname());

			if (message.equalsIgnoreCase("success")) {

				addActionMessage("Card/Cheque report generated successfully.");

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Card/Cheque PDF Report");

				// Setting fileinput stream and file name in order to get
				// fileinputStream and file name while downloading
				reportForm.setFileInputStream(new FileInputStream(new File(PDFFileName)));

				reportForm.setFileName("cardCheque-BasedReport.pdf");

				return SUCCESS;

			} else {

				addActionError("Failed to generate Card/Cheque PDF report.Please check server logs for more details.");

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
						"Component-based PDF Report Exception Occurred");

				return ERROR;

			}

		} else {

			/*
			 * Generating EXCEL Component based report
			 */
			String realPath = request.getServletContext().getRealPath("/");

			String PDFFileName = realPath + "cardCheque-BasedReport.xls";

			message = excelUtil.generateCardChequeBasedReport(reportForm, realPath, PDFFileName,
					userForm.getFullname());

			if (message.equalsIgnoreCase("success")) {

				addActionMessage("Card/Cheque report generated successfully.");

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
						"Card/Cheque Excel Report");

				// Setting fileinput stream and file name in order to get
				// fileinputStream and file name while downloading
				reportForm.setFileInputStream(new FileInputStream(new File(PDFFileName)));

				reportForm.setFileName("cardCheque-BasedReport.xls");

				return SUCCESS;

			} else {

				addActionError("Failed to generate Card/Cheque XLS report.Please check server logs for more details.");

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
						"Component-based Excel Report Exception Occurred");

				return ERROR;

			}

		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String renderExportStorageCenter() throws Exception {

		reportDAOInf = new ReportDAOImpl();

		storageCenterList = reportDAOInf.retrievebloodStorageCenterList();

		if (storageCenterList.size() > 0) {
			return SUCCESS;
		} else {

			addActionError("No storage center found.");

			return ERROR;

		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String exportStorageCenterExcel() throws Exception {

		loginDAOInf = new UserDAOImpl();

		excelUtil = new ExcelUtil();
		
		reportDAOInf = new ReportDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		/*
		 * Generating EXCEL Component based report
		 */
		String realPath = request.getServletContext().getRealPath("/");

		String excelFileName = realPath + "storageCenters.xls";

		storageCenterList = reportDAOInf.retrievebloodStorageCenterList();

		message = excelUtil.generateStorageCenterExcel(storageCenterList, realPath, excelFileName);

		if (message.equals("success")) {

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
					"Storage Center Excel Report");

			// Setting fileinput stream and file name in order to get
			// fileinputStream and file name while downloading
			reportForm.setFileInputStream(new FileInputStream(new File(excelFileName)));

			reportForm.setFileName("storageCenters.xls");

			return SUCCESS;

		} else {

			addActionError("Failed to generate storage center report.Please check server logs for more details.");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
					"Storage Center Excel Report Exception Occurred");

			return ERROR;

		}

	}

	/**
	 * @return the storageCenterList
	 */
	public List<ReportForm> getStorageCenterList() {
		return storageCenterList;
	}

	/**
	 * @param storageCenterList the storageCenterList to set
	 */
	public void setStorageCenterList(List<ReportForm> storageCenterList) {
		this.storageCenterList = storageCenterList;
	}

	/**
	 * @return the cardReportList
	 */
	public List<ReportForm> getCardReportList() {
		return cardReportList;
	}

	/**
	 * @param cardReportList the cardReportList to set
	 */
	public void setCardReportList(List<ReportForm> cardReportList) {
		this.cardReportList = cardReportList;
	}

	/**
	 * @return the concessionTypeList
	 */
	public List<String> getConcessionTypeList() {
		return concessionTypeList;
	}

	/**
	 * @param concessionTypeList the concessionTypeList to set
	 */
	public void setConcessionTypeList(List<String> concessionTypeList) {
		this.concessionTypeList = concessionTypeList;
	}

	/**
	 * @return the concessionList
	 */
	public List<ReportForm> getConcessionList() {
		return concessionList;
	}

	/**
	 * @param concessionList the concessionList to set
	 */
	public void setConcessionList(List<ReportForm> concessionList) {
		this.concessionList = concessionList;
	}

	/**
	 * @return the registerList
	 */
	public List<ReportForm> getRegisterList() {
		return registerList;
	}

	/**
	 * @param registerList the registerList to set
	 */
	public void setRegisterList(List<ReportForm> registerList) {
		this.registerList = registerList;
	}

	/**
	 * @return the componentList
	 */
	public List<ReportForm> getComponentList() {
		return componentList;
	}

	/**
	 * @param componentList the componentList to set
	 */
	public void setComponentList(List<ReportForm> componentList) {
		this.componentList = componentList;
	}

	/**
	 * @return the componentNameList
	 */
	public List<String> getComponentNameList() {
		return componentNameList;
	}

	/**
	 * @param componentNameList the componentNameList to set
	 */
	public void setComponentNameList(List<String> componentNameList) {
		this.componentNameList = componentNameList;
	}

	/**
	 * @return the cashHandoverReportList
	 */
	public List<ReportForm> getCashHandoverReportList() {
		return cashHandoverReportList;
	}

	/**
	 * @param cashHandoverReportList the cashHandoverReportList to set
	 */
	public void setCashHandoverReportList(List<ReportForm> cashHandoverReportList) {
		this.cashHandoverReportList = cashHandoverReportList;
	}

	/**
	 * @return the patientReportList
	 */
	public List<ReportForm> getPatientReportList() {
		return patientReportList;
	}

	/**
	 * @param patientReportList the patientReportList to set
	 */
	public void setPatientReportList(List<ReportForm> patientReportList) {
		this.patientReportList = patientReportList;
	}

	/**
	 * @return the reportForm
	 */
	public ReportForm getReportForm() {
		return reportForm;
	}

	/**
	 * @param reportForm the reportForm to set
	 */
	public void setReportForm(ReportForm reportForm) {
		this.reportForm = reportForm;
	}

	public void setServletResponse(HttpServletResponse response) {

		this.response = response;

	}

	public void setServletRequest(HttpServletRequest request) {

		this.request = request;

	}

	public void setSession(Map<String, Object> sessionAttriute) {

		this.sessionAttriute = sessionAttriute;

	}

	public ReportForm getModel() {

		return reportForm;
	}
}
