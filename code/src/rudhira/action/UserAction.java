package rudhira.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.SessionAware;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import rudhira.DAO.UserDAOImpl;
import rudhira.DAO.UserDAOInf;
import rudhira.form.UserForm;
import rudhira.service.RudhiraServiceImpl;
import rudhira.service.RudhiraServiceInf;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

/**
 * 
 * @author Kovid Bioanalytics
 * 
 */
public class UserAction extends ActionSupport
		implements ModelDriven<UserForm>, SessionAware, ServletRequestAware, ServletResponseAware {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	UserForm loginForm = new UserForm();

	String message = null;

	private Map<String, Object> sessionAttriute = null;

	HttpServletRequest request;

	UserDAOInf loginDAOInf = null;
	RudhiraServiceInf serviceInf = null;

	HashMap<Integer, String> bloodBankList = null;

	List<UserForm> userList = null;

	List<UserForm> bldBnkList = null;

	List<UserForm> concessionList = null;

	List<UserForm> concessionEditList = null;

	List<UserForm> chargeSearchList = null;

	List<UserForm> chargeTypeEditList = null;

	List<UserForm> componentSearchList = null;

	List<UserForm> componentEditList = null;

	List<UserForm> instructionsSearchList = null;

	List<UserForm> instructionsEditList = null;

	List<UserForm> shiftList = null;

	List<UserForm> shiftEditList = null;

	String helpManualFileName = null;

	HttpServletResponse response;

	FileInputStream helpManualFile;

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String verifyUserCredentials() throws Exception {
		loginDAOInf = new UserDAOImpl();

		message = loginDAOInf.validateUserCredentials(loginForm);

		if (message.equals("success")) {
			System.out.println("Succcessfully Logged in...");

			/*
			 * Retrieving user details based on username and setting theem into
			 * session varaible
			 */
			loginForm = loginDAOInf.getUserDetails(loginForm.getUsername());
			sessionAttriute.put("USER", loginForm);

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(loginForm.getUserID(), request.getRemoteAddr(), "Login");

			return SUCCESS;
		} else if (message.equals("error")) {
			System.out.println("User is not authorised to login as LOGIN_FLAG is 0. Contact Admin.");
			addActionError("You are not authorised to login. Please contact adminstrator for further detail.");
			return ERROR;
		} else if (message.equals("input")) {
			System.out.println("Please check the user credetials. Wrong credentials entered.");
			addActionError("Enter valid username and password");
			return INPUT;
		} else if (message.equals("login")) {
			System.out.println("User is locked successfully.");
			addActionError(
					"You have exceeded the maximum limit of login attempts. Your account is now locked. Please contact your administrator to unlock it.");
			return LOGIN;
		} else if (message.equals("exception")) {
			System.out.println("Exception occured while logging in.");
			addActionError("Exception occured while logging in. Please check logs for more details.");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(loginForm.getUserID(), request.getRemoteAddr(), "Login Exception occurred");

			return "exception";
		} else {
			System.out.println("User activityStatus is Locked. Not authorised to login.");
			addActionError("Your account is either locked or inactive. Please contact your administrator.");
			return "locked";
		}
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String logoutUser() throws Exception {
		loginDAOInf = new UserDAOImpl();

		// Inserting into Audit
		loginDAOInf.insertAuditDetails(loginForm.getUserID(), request.getRemoteAddr(), "Logout");

		sessionAttriute.remove("USER");
		addActionMessage("You are successfully logged out");
		if (sessionAttriute.get("USER") == "" || sessionAttriute.get("USER") == null) {
			System.out.println("session removed");

		} else {
			System.out.println("error removing session object");
		}
		return SUCCESS;
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String renderAddUser() throws Exception {
		loginDAOInf = new UserDAOImpl();

		bloodBankList = loginDAOInf.retrieveBloodBankList();

		return SUCCESS;
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String addUser() throws Exception {
		loginDAOInf = new UserDAOImpl();
		serviceInf = new RudhiraServiceImpl();

		/*
		 * Getting real Path from Context
		 */
		String realPath = request.getServletContext().getRealPath("/");

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		message = serviceInf.addUser(loginForm, realPath, request);

		if (message.equalsIgnoreCase("success")) {

			System.out.println("New user added successfully. Record inserted successfully into database.");

			addActionMessage("Registered Successfully. Now Login with your username and password.");

			bloodBankList = loginDAOInf.retrieveBloodBankList();

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Add User");

			return SUCCESS;

		} else if (message.equalsIgnoreCase("input")) {

			System.out.println("the entered username already exists into User table. Try with different username.");

			addActionError("Username already exists. Please use different username.");

			bloodBankList = loginDAOInf.retrieveBloodBankList();

			return INPUT;

		} else if (message.equalsIgnoreCase("bloodBankSelect")) {

			addActionError("No Blood Bank selected. Please select Blood Bank");

			bloodBankList = loginDAOInf.retrieveBloodBankList();

			return "bloodBankSelect";

		} else {

			System.out.println("Error while inserting record into database.");

			addActionError("Failed to add user.Please check server logs for more details.");

			bloodBankList = loginDAOInf.retrieveBloodBankList();

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Add User Exception occured");

			return ERROR;

		}
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String editUserList() throws Exception {
		loginDAOInf = new UserDAOImpl();

		userList = loginDAOInf.retrieveUserList();

		if (userList.size() > 0) {

			request.setAttribute("userListEnable", "userListEnable");

			return SUCCESS;

		} else {

			String errorMsg = "No user found. Please add new user.";

			addActionError(errorMsg);

			return ERROR;
		}
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String renderEditUser() throws Exception {
		loginDAOInf = new UserDAOImpl();

		userList = loginDAOInf.retrieveEditUserListByUserID(loginForm.getUserID());

		bloodBankList = loginDAOInf.retrieveBloodBankList();

		return SUCCESS;
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String searchUser() throws Exception {
		loginDAOInf = new UserDAOImpl();

		userList = loginDAOInf.searchUser(loginForm.getSearchUserName());

		/*
		 * Checking whether userList is empty or not, if empty give error
		 * message saying User with name not found
		 */
		if (userList.size() > 0) {

			request.setAttribute("userListEnable", "userSearchListEnable");

			return SUCCESS;

		} else {

			String errorMsg = "User with name '" + loginForm.getSearchUserName() + "' not found.";

			addActionError(errorMsg);

			return ERROR;
		}
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String editUser() throws Exception {
		loginDAOInf = new UserDAOImpl();
		serviceInf = new RudhiraServiceImpl();

		/*
		 * Getting real Path from Context
		 */
		String realPath = request.getServletContext().getRealPath("/");

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		message = serviceInf.editUser(loginForm, realPath, request);

		if (message.equalsIgnoreCase("success")) {

			addActionMessage("User details updated successfully.");

			userList = new ArrayList<UserForm>();

			userList.add(loginForm);

			bloodBankList = loginDAOInf.retrieveBloodBankList();

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Edit User");

			return SUCCESS;

		} else if (message.equalsIgnoreCase("input")) {

			addActionError("Password cannot repeat any of your previous 5 passwords. Please try different password.");

			userList = new ArrayList<UserForm>();

			userList.add(loginForm);

			bloodBankList = loginDAOInf.retrieveBloodBankList();

			return SUCCESS;

		} else {

			addActionError("Failed to update user details. Please check logs for more details.");

			userList = new ArrayList<UserForm>();

			userList.add(loginForm);

			bloodBankList = loginDAOInf.retrieveBloodBankList();

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
					"Edit User Exception occured");

			return ERROR;

		}
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String editUserProfile() throws Exception {
		loginDAOInf = new UserDAOImpl();
		serviceInf = new RudhiraServiceImpl();

		/*
		 * Getting real Path from Context
		 */
		String realPath = request.getServletContext().getRealPath("/");

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		message = serviceInf.editUser(loginForm, realPath, request);

		if (message.equalsIgnoreCase("success")) {

			addActionMessage("User profile updated successfully.");

			userList = new ArrayList<UserForm>();

			userList.add(loginForm);

			bloodBankList = loginDAOInf.retrieveBloodBankList();

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Edit User Profile");

			return SUCCESS;

		} else if (message.equalsIgnoreCase("input")) {

			addActionError("Password cannot repeat any of your previous 5 passwords. Please try different password.");

			userList = new ArrayList<UserForm>();

			userList.add(loginForm);

			bloodBankList = loginDAOInf.retrieveBloodBankList();

			return SUCCESS;

		} else {

			addActionError("Failed to update user profile. Please check logs for more details.");

			userList = new ArrayList<UserForm>();

			userList.add(loginForm);

			bloodBankList = loginDAOInf.retrieveBloodBankList();

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
					"Edit User Profile Exception occured");

			return ERROR;

		}
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String disableSearchUser() throws Exception {
		loginDAOInf = new UserDAOImpl();

		message = loginDAOInf.disableUser(loginForm.getUserID());

		if (message.equalsIgnoreCase("success")) {

			addActionMessage("User Disabled successfully.");

			userList = loginDAOInf.searchUser(loginForm.getSearchUserName());

			request.setAttribute("userListEnable", "userSearchListEnable");

			return SUCCESS;

		} else {

			addActionError("Failed to disable user. Please check logs for more details.");

			userList = loginDAOInf.searchUser(loginForm.getSearchUserName());

			request.setAttribute("userListEnable", "userSearchListEnable");

			return ERROR;

		}
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String disableUser() throws Exception {
		loginDAOInf = new UserDAOImpl();

		message = loginDAOInf.disableUser(loginForm.getUserID());

		if (message.equalsIgnoreCase("success")) {

			addActionMessage("User Disabled successfully.");

			userList = loginDAOInf.retrieveUserList();

			request.setAttribute("userListEnable", "userListEnable");

			return SUCCESS;

		} else {

			addActionError("Failed to disable user. Please check logs for more details.");

			userList = loginDAOInf.retrieveUserList();

			request.setAttribute("userListEnable", "userListEnable");

			return ERROR;

		}
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String renderEditProfile() throws Exception {
		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		userList = loginDAOInf.retrieveEditUserListByUserID(userForm.getUserID());

		bloodBankList = loginDAOInf.retrieveBloodBankList();

		return SUCCESS;
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String addBloodBank() throws Exception {
		loginDAOInf = new UserDAOImpl();
		serviceInf = new RudhiraServiceImpl();

		/*
		 * Getting real Path from Context
		 */
		String realPath = request.getServletContext().getRealPath("/");

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		message = serviceInf.addBloodBank(loginForm, realPath);

		if (message.equalsIgnoreCase("success")) {

			addActionMessage("Successfully added new blood bank.");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Add Blood Bank");

			return SUCCESS;

		} else if (message.equalsIgnoreCase("input")) {

			addActionError("Blood Bank already exists. Try again with different name.");

			return INPUT;

		} else if (message.equalsIgnoreCase("logoFileSelect")) {

			addActionError("Blood Bank logo file not selected. Please select Blood Bank logo file");

			return "logoFileSelect";

		} else {

			addActionError("Failed to add Blood Bank. Please check server logs for more details.");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
					"Add Blood Bank Exception occurred");

			return ERROR;

		}
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String searchBloodBank() throws Exception {
		loginDAOInf = new UserDAOImpl();

		bldBnkList = loginDAOInf.searchBloodBank(loginForm.getSearchBloodBankName());

		/*
		 * Checking whether bldBnkList is empty or not, if empty give error
		 * message saying Blood Bank with name not found
		 */
		if (bldBnkList.size() > 0) {

			request.setAttribute("bloodBankListEnable", "bbSearchListEnable");

			return SUCCESS;

		} else {

			String errorMsg = "Blood Bank with name '" + loginForm.getSearchBloodBankName() + "' not found.";

			addActionError(errorMsg);

			return ERROR;
		}
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String editBloodBankList() throws Exception {
		loginDAOInf = new UserDAOImpl();

		bldBnkList = loginDAOInf.retrieveEditBloodBankList();

		if (bldBnkList.size() > 0) {

			request.setAttribute("bloodBankListEnable", "bbListEnable");

			return SUCCESS;

		} else {

			String errorMsg = "No blood bank found. Please add new blood bank.";

			addActionError(errorMsg);

			return ERROR;
		}
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String renderEditBloodBank() throws Exception {
		loginDAOInf = new UserDAOImpl();

		bldBnkList = loginDAOInf.retrieveEditBloodBankListByID(loginForm.getBloodBankID());

		return SUCCESS;
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String editBloodBank() throws Exception {
		loginDAOInf = new UserDAOImpl();
		serviceInf = new RudhiraServiceImpl();

		/*
		 * Getting real Path from Context
		 */
		String realPath = request.getServletContext().getRealPath("/");

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		message = serviceInf.editBloodBank(loginForm, realPath);

		if (message.equalsIgnoreCase("success")) {

			addActionMessage("Successfully updated blood bank details.");

			bldBnkList = new ArrayList<UserForm>();

			bldBnkList.add(loginForm);

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Edit Blood Bank");

			return SUCCESS;

		} else if (message.equalsIgnoreCase("input")) {

			addActionError("Blood Bank already exists. Try again with different name.");

			bldBnkList = new ArrayList<UserForm>();

			bldBnkList.add(loginForm);

			return INPUT;

		} else if (message.equalsIgnoreCase("logoFileSelect")) {

			addActionError("Blood Bank logo file not selected. Please select Blood Bank logo file");

			bldBnkList = new ArrayList<UserForm>();

			bldBnkList.add(loginForm);

			return "logoFileSelect";

		} else {

			addActionError("Failed to update Blood Bank. Please check server logs for more details.");

			bldBnkList = new ArrayList<UserForm>();

			bldBnkList.add(loginForm);

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
					"Edit Blood Bank Exception occurred");

			return ERROR;

		}
	}

	/**
	 * 
	 * @throws Exception
	 */
	public void newPassCheck() throws Exception {

		loginDAOInf = new UserDAOImpl();

		JSONObject values = new JSONObject();

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		response.setCharacterEncoding("UTF-8");

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		try {

			String newPass = request.getParameter("newPass");

			/*
			 * Password match check; checking whether entered password matches
			 * last five passwords from Password History, then give error
			 * message else proceed further
			 */
			boolean check = loginDAOInf.verifyPasswordHistory(userForm.getUserID(), newPass);

			if (check) {

				System.out.println("Password already exists into password history");

				object.put("newPassCheck", "false");

				array.add(object);

				values.put("Release", array);

				PrintWriter out = response.getWriter();

				out.print(values);

			} else {

				System.out.println("New password encountered");

				object.put("newPassCheck", "true");

				array.add(object);

				values.put("Release", array);

				PrintWriter out = response.getWriter();

				out.print(values);

			}

		} catch (Exception exception) {
			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while verifying new password with password history.");

			array.add(object);

			values.put("Release", array);

			PrintWriter out = response.getWriter();

			out.print(values);
		}

	}

	/**
	 * 
	 * @throws Exception
	 */
	public void verifyUnlockPIN() throws Exception {

		loginDAOInf = new UserDAOImpl();

		JSONObject values = new JSONObject();

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		response.setCharacterEncoding("UTF-8");

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		try {

			String lockPIN = request.getParameter("lockPIN");

			System.out.println("User entered PIN is ::: " + lockPIN);

			values = loginDAOInf.verifyUserPIN(lockPIN, userForm.getUserID());

			PrintWriter out = response.getWriter();

			out.print(values);

		} catch (Exception exception) {
			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while verifying credentials.");

			array.add(object);

			values.put("Release", array);

			PrintWriter out = response.getWriter();

			out.print(values);
		}

	}

	/**
	 * 
	 * @throws Exception
	 */
	public void verifyOldPass() throws Exception {

		loginDAOInf = new UserDAOImpl();

		JSONObject values = new JSONObject();

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		response.setCharacterEncoding("UTF-8");

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		try {

			String oldPass = request.getParameter("oldPass");

			System.out.println("User entered Old password is ::: " + oldPass);

			values = loginDAOInf.verifyUserOldPassword(oldPass, userForm.getUserID());

			PrintWriter out = response.getWriter();

			out.print(values);

		} catch (Exception exception) {
			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while verifying old password.");

			array.add(object);

			values.put("Release", array);

			PrintWriter out = response.getWriter();

			out.print(values);
		}

	}

	/**
	 * 
	 * @throws Exception
	 */
	public void updateSecurityCredentials() throws Exception {

		loginDAOInf = new UserDAOImpl();

		JSONObject values = new JSONObject();

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		response.setCharacterEncoding("UTF-8");

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		try {

			String newPass = request.getParameter("newPass");
			String newPIN = request.getParameter("newPIN");

			values = loginDAOInf.updateUserSecurityCredentials(newPass, newPIN, userForm.getUserID());

			// Inserting new password into PasswordHistory table
			loginDAOInf.insertPasswordHistory(userForm.getUserID(), newPass);

			if (newPass == null || newPass == "" || newPass.isEmpty()) {

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "PIN changed");

			} else if (newPIN == null || newPIN == "" || newPIN.isEmpty()) {

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Password changed");

			} else {

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Password changed");

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "PIN changed");

			}

			PrintWriter out = response.getWriter();

			out.print(values);

		} catch (Exception exception) {
			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while updating security credentials.");

			array.add(object);

			values.put("Release", array);

			PrintWriter out = response.getWriter();

			out.print(values);
		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String searchConcession() throws Exception {
		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		concessionList = loginDAOInf.searchConcession(loginForm.getSearchConcessionName(), userForm.getBloodBankID());

		/*
		 * Checking whether concession list is empty or not, if empty give error
		 * message saying COncession with type not found
		 */
		if (concessionList.size() > 0) {

			request.setAttribute("concessionMsg", "available");

			return SUCCESS;

		} else {

			String errorMsg = "Concession with name '" + loginForm.getSearchConcessionName() + "' not found.";

			addActionError(errorMsg);

			return ERROR;
		}
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String viewAllConcession() throws Exception {
		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		concessionList = loginDAOInf.retrieveConcession(userForm.getBloodBankID());

		/*
		 * Checking whether concession list is empty or not, if empty give error
		 * message saying COncession with type not found
		 */
		if (concessionList.size() > 0) {

			request.setAttribute("concessionMsg", "available");

			return SUCCESS;

		} else {

			addActionError("No Concession found. Please add new Concession.");

			return ERROR;
		}
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String addConcession() throws Exception {
		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		message = loginDAOInf.insertConcession(loginForm, userForm.getBloodBankID());

		if (message.equalsIgnoreCase("success")) {

			addActionMessage("Concession added successfully.");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Add Concession");

			return SUCCESS;

		} else {

			addActionError("Failed to add concession. Please check server logs for more details.");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
					"Add Concession Exception occurred");

			return ERROR;

		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String renderEditConcession() throws Exception {
		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		concessionEditList = loginDAOInf.retrieveEditConcessionList(loginForm.getConcessionID(),
				loginForm.getSearchConcessionName());

		if (concessionEditList.size() > 0) {

			/*
			 * Retrieving concession search list
			 */
			if (loginForm.getSearchConcessionName() == null || loginForm.getSearchConcessionName() == "") {

				concessionList = loginDAOInf.retrieveConcession(userForm.getBloodBankID());

			} else {

				concessionList = loginDAOInf.searchConcession(loginForm.getSearchConcessionName(),
						userForm.getBloodBankID());

			}

			request.setAttribute("concessionMsg", "available");

			request.setAttribute("concessionEdit", "edit");

			return SUCCESS;

		} else {

			/*
			 * Retrieving concession search list
			 */
			if (loginForm.getSearchConcessionName() == null || loginForm.getSearchConcessionName() == "") {

				concessionList = loginDAOInf.retrieveConcession(userForm.getBloodBankID());

			} else {

				concessionList = loginDAOInf.searchConcession(loginForm.getSearchConcessionName(),
						userForm.getBloodBankID());

			}

			request.setAttribute("concessionMsg", "available");

			addActionError(
					"Failed to retrieve concession list based on concession ID. Please check server logs for more details.");

			return ERROR;
		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String editConcession() throws Exception {
		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		message = loginDAOInf.updateConcession(loginForm, userForm.getBloodBankID());

		if (message.equalsIgnoreCase("success")) {

			addActionMessage("Concession updated successfully.");

			/*
			 * Retrieving concession search list
			 */
			if (loginForm.getSearchConcessionName() == null || loginForm.getSearchConcessionName() == "") {

				concessionList = loginDAOInf.retrieveConcession(userForm.getBloodBankID());

			} else {

				concessionList = loginDAOInf.searchConcession(loginForm.getSearchConcessionName(),
						userForm.getBloodBankID());

			}

			request.setAttribute("concessionMsg", "available");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Edit Concession");

			return SUCCESS;

		} else {

			addActionError("Failed to update concession. Please check server logs for more details.");

			/*
			 * Retrieving concession search list
			 */
			if (loginForm.getSearchConcessionName() == null || loginForm.getSearchConcessionName() == "") {

				concessionList = loginDAOInf.retrieveConcession(userForm.getBloodBankID());

			} else {

				concessionList = loginDAOInf.searchConcession(loginForm.getSearchConcessionName(),
						userForm.getBloodBankID());

			}

			request.setAttribute("concessionEdit", "edit");

			request.setAttribute("concessionMsg", "available");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
					"Edit Concession Exception occurred");

			return ERROR;

		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String deleteConcession() throws Exception {
		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		message = loginDAOInf.deleteConcession(loginForm.getConcessionID());

		if (message.equalsIgnoreCase("success")) {

			addActionMessage("Concession deleted successfully.");

			/*
			 * Retrieving concession search list
			 */
			if (loginForm.getSearchConcessionName() == null || loginForm.getSearchConcessionName() == "") {

				concessionList = loginDAOInf.retrieveConcession(userForm.getBloodBankID());

			} else {

				concessionList = loginDAOInf.searchConcession(loginForm.getSearchConcessionName(),
						userForm.getBloodBankID());

			}

			request.setAttribute("concessionMsg", "available");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Delete Concession");

			return SUCCESS;

		} else {

			addActionError("Failed to delete concession. Please check server logs for more details.");

			/*
			 * Retrieving concession search list
			 */
			if (loginForm.getSearchConcessionName() == null || loginForm.getSearchConcessionName() == "") {

				concessionList = loginDAOInf.retrieveConcession(userForm.getBloodBankID());

			} else {

				concessionList = loginDAOInf.searchConcession(loginForm.getSearchConcessionName(),
						userForm.getBloodBankID());

			}

			request.setAttribute("concessionMsg", "available");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
					"Delete Concession Exception occurred");

			return ERROR;

		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String searchChargeType() throws Exception {
		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		chargeSearchList = loginDAOInf.searchChargeType(loginForm.getSearchChargeName(), userForm.getBloodBankID());

		/*
		 * Checking whether charge list is empty or not, if empty give error
		 * message saying Charge with type not found
		 */
		if (chargeSearchList.size() > 0) {

			request.setAttribute("chargeTypeMsg", "available");

			return SUCCESS;

		} else {

			String errorMsg = "Charge with type '" + loginForm.getSearchChargeName() + "' not found.";

			addActionError(errorMsg);

			return ERROR;
		}
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String viewAllCharges() throws Exception {
		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		chargeSearchList = loginDAOInf.retrieveChargeType(userForm.getBloodBankID());

		/*
		 * Checking whether charge list is empty or not, if empty give error
		 * message saying Charge with type not found
		 */
		if (chargeSearchList.size() > 0) {

			request.setAttribute("chargeTypeMsg", "available");

			return SUCCESS;

		} else {

			addActionError("No charges found. Please add new Charge.");

			return ERROR;
		}
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String addChargeType() throws Exception {
		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		message = loginDAOInf.insertChargeType(loginForm, userForm.getBloodBankID());

		if (message.equalsIgnoreCase("success")) {

			addActionMessage("Change Type added successfully.");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Add Charge Type");

			return SUCCESS;

		} else {

			addActionError("Failed to add charge type. Please check server logs for more details.");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
					"Add Charge Type Exception occurred");

			return ERROR;

		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String renderEditChargeType() throws Exception {
		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		chargeTypeEditList = loginDAOInf.retrieveEditChargeTypeList(loginForm.getChargeID(),
				loginForm.getSearchChargeName());

		if (chargeTypeEditList.size() > 0) {

			/*
			 * Retrieving charge type search list
			 */
			if (loginForm.getSearchChargeName() == null || loginForm.getSearchChargeName() == "") {

				chargeSearchList = loginDAOInf.retrieveChargeType(userForm.getBloodBankID());

			} else {

				chargeSearchList = loginDAOInf.searchChargeType(loginForm.getSearchChargeName(),
						userForm.getBloodBankID());

			}

			request.setAttribute("chargeTypeMsg", "available");

			request.setAttribute("chargeTypeEdit", "edit");

			return SUCCESS;

		} else {

			/*
			 * Retrieving charge type search list
			 */
			if (loginForm.getSearchChargeName() == null || loginForm.getSearchChargeName() == "") {

				chargeSearchList = loginDAOInf.retrieveChargeType(userForm.getBloodBankID());

			} else {

				chargeSearchList = loginDAOInf.searchChargeType(loginForm.getSearchChargeName(),
						userForm.getBloodBankID());

			}

			request.setAttribute("chargeTypeMsg", "available");

			addActionError(
					"Failed to retrieve charge type list based on charge Type ID. Please check server logs for more details.");

			return ERROR;
		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String editChargeType() throws Exception {
		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		message = loginDAOInf.updateChargeType(loginForm, userForm.getBloodBankID());

		if (message.equalsIgnoreCase("success")) {

			addActionMessage("Charge Type updated successfully.");

			/*
			 * Retrieving charge type search list
			 */
			if (loginForm.getSearchChargeName() == null || loginForm.getSearchChargeName() == "") {

				chargeSearchList = loginDAOInf.retrieveChargeType(userForm.getBloodBankID());

			} else {

				chargeSearchList = loginDAOInf.searchChargeType(loginForm.getSearchChargeName(),
						userForm.getBloodBankID());

			}

			request.setAttribute("chargeTypeMsg", "available");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Edit Charge Type");

			return SUCCESS;

		} else {

			addActionError("Failed to update charge type. Please check server logs for more details.");

			/*
			 * Retrieving charge type search list
			 */
			if (loginForm.getSearchChargeName() == null || loginForm.getSearchChargeName() == "") {

				chargeSearchList = loginDAOInf.retrieveChargeType(userForm.getBloodBankID());

			} else {

				chargeSearchList = loginDAOInf.searchChargeType(loginForm.getSearchChargeName(),
						userForm.getBloodBankID());

			}

			request.setAttribute("chargeTypeMsg", "available");

			request.setAttribute("chargeTypeEdit", "edit");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
					"Edit Charge Type Exception occurred");

			return ERROR;

		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String deleteChargeType() throws Exception {
		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		message = loginDAOInf.deleteChargeTypeID(loginForm.getChargeID());

		if (message.equalsIgnoreCase("success")) {

			addActionMessage("Charge Type deleted successfully.");

			/*
			 * Retrieving charge type search list
			 */
			if (loginForm.getSearchChargeName() == null || loginForm.getSearchChargeName() == "") {

				chargeSearchList = loginDAOInf.retrieveChargeType(userForm.getBloodBankID());

			} else {

				chargeSearchList = loginDAOInf.searchChargeType(loginForm.getSearchChargeName(),
						userForm.getBloodBankID());

			}

			request.setAttribute("chargeTypeMsg", "available");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Delete Charge Type");

			return SUCCESS;

		} else {

			addActionError("Failed to delete concession. Please check server logs for more details.");

			/*
			 * Retrieving charge type search list
			 */
			if (loginForm.getSearchChargeName() == null || loginForm.getSearchChargeName() == "") {

				chargeSearchList = loginDAOInf.retrieveChargeType(userForm.getBloodBankID());

			} else {

				chargeSearchList = loginDAOInf.searchChargeType(loginForm.getSearchChargeName(),
						userForm.getBloodBankID());

			}

			request.setAttribute("chargeTypeMsg", "available");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
					"Delete Charge Type Exception occurred");

			return ERROR;

		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String downloadManual() throws Exception {
		
		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		/*
		 * Getting real Path from Context
		 */
		String realPath = request.getServletContext().getRealPath("/");

		/*
		 * Checking whether user role is Administrator or Accountant and
		 * depending on that downloading Help manual file
		 */
		if (userForm.getUserRole().equals("administrator")) {

			FileInputStream inputStream = new FileInputStream(
					new File(realPath + "/userManual/AdministratorManual.pdf"));

			setHelpManualFile(inputStream);

			// Setting file name in order to download the file
			setHelpManualFileName("AdministratorManual.pdf");

		} else {

			FileInputStream inputStream = new FileInputStream(new File(realPath + "/userManual/AccountantManual.pdf"));

			setHelpManualFile(inputStream);

			// Setting file name in order to download the file
			setHelpManualFileName("AccountantManual.pdf");

		}

		// Inserting into Audit
		loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "User Manual downloaded");

		return SUCCESS;
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String searchComponents() throws Exception {
		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		componentSearchList = loginDAOInf.searchComponents(loginForm.getSearchComponentName(),
				userForm.getBloodBankID());

		/*
		 * Checking whether component list is empty or not, if empty give error
		 * message saying Component not found
		 */
		if (componentSearchList.size() > 0) {

			request.setAttribute("componentMsg", "available");

			return SUCCESS;

		} else {

			String errorMsg = "Component '" + loginForm.getSearchComponentName() + "' not found.";

			addActionError(errorMsg);

			return ERROR;
		}
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String viewAllComponents() throws Exception {
		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		componentSearchList = loginDAOInf.retrieveComponents(userForm.getBloodBankID());

		/*
		 * Checking whether component list is empty or not, if empty give error
		 * message saying Component not found
		 */
		if (componentSearchList.size() > 0) {

			request.setAttribute("componentMsg", "available");

			return SUCCESS;

		} else {

			addActionError("No Components found. Please add new component");

			return ERROR;
		}
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String addComponent() throws Exception {
		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		message = loginDAOInf.insertComponents(loginForm, userForm.getBloodBankID());

		if (message.equalsIgnoreCase("success")) {

			addActionMessage("Component added successfully.");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Add Component");

			return SUCCESS;

		} else {

			addActionError("Failed to add component. Please check server logs for more details.");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
					"Add Component Exception occurred");

			return ERROR;

		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String renderEditComponents() throws Exception {
		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		componentEditList = loginDAOInf.retrieveEditComponentsList(loginForm.getComponentID(),
				loginForm.getSearchComponentName());

		if (componentEditList.size() > 0) {

			/*
			 * Retrieving component search list
			 */
			if (loginForm.getSearchComponentName() == null || loginForm.getSearchComponentName() == "") {

				componentSearchList = loginDAOInf.retrieveComponents(userForm.getBloodBankID());

			} else {

				componentSearchList = loginDAOInf.searchComponents(loginForm.getSearchComponentName(),
						userForm.getBloodBankID());

			}

			request.setAttribute("componentMsg", "available");

			request.setAttribute("componentEdit", "edit");

			return SUCCESS;

		} else {

			/*
			 * Retrieving component search list
			 */
			if (loginForm.getSearchComponentName() == null || loginForm.getSearchComponentName() == "") {

				componentSearchList = loginDAOInf.retrieveComponents(userForm.getBloodBankID());

			} else {

				componentSearchList = loginDAOInf.searchComponents(loginForm.getSearchComponentName(),
						userForm.getBloodBankID());

			}

			request.setAttribute("componentMsg", "available");

			addActionError(
					"Failed to retrieve component list based on component ID. Please check server logs for more details.");

			return ERROR;
		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String editComponent() throws Exception {
		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		message = loginDAOInf.updateComponents(loginForm, userForm.getBloodBankID());

		if (message.equalsIgnoreCase("success")) {

			addActionMessage("Component updated successfully.");

			/*
			 * Retrieving component search list
			 */
			if (loginForm.getSearchComponentName() == null || loginForm.getSearchComponentName() == "") {

				componentSearchList = loginDAOInf.retrieveComponents(userForm.getBloodBankID());

			} else {

				componentSearchList = loginDAOInf.searchComponents(loginForm.getSearchComponentName(),
						userForm.getBloodBankID());

			}

			request.setAttribute("componentMsg", "available");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Edit Component");

			return SUCCESS;

		} else {

			addActionError("Failed to update Component. Please check server logs for more details.");

			/*
			 * Retrieving component search list
			 */
			if (loginForm.getSearchComponentName() == null || loginForm.getSearchComponentName() == "") {

				componentSearchList = loginDAOInf.retrieveComponents(userForm.getBloodBankID());

			} else {

				componentSearchList = loginDAOInf.searchComponents(loginForm.getSearchComponentName(),
						userForm.getBloodBankID());

			}

			request.setAttribute("componentMsg", "available");

			request.setAttribute("componentEdit", "edit");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
					"Edit Component Exception occurred");

			return ERROR;

		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String deleteComponent() throws Exception {
		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		message = loginDAOInf.deleteComponents(loginForm.getComponentID());

		if (message.equalsIgnoreCase("success")) {

			addActionMessage("Component deleted successfully.");

			/*
			 * Retrieving component search list
			 */
			if (loginForm.getSearchComponentName() == null || loginForm.getSearchComponentName() == "") {

				componentSearchList = loginDAOInf.retrieveComponents(userForm.getBloodBankID());

			} else {

				componentSearchList = loginDAOInf.searchComponents(loginForm.getSearchComponentName(),
						userForm.getBloodBankID());

			}

			request.setAttribute("componentMsg", "available");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Delete Component");

			return SUCCESS;

		} else {

			addActionError("Failed to delete component. Please check server logs for more details.");

			/*
			 * Retrieving component search list
			 */
			if (loginForm.getSearchComponentName() == null || loginForm.getSearchComponentName() == "") {

				componentSearchList = loginDAOInf.retrieveComponents(userForm.getBloodBankID());

			} else {

				componentSearchList = loginDAOInf.searchComponents(loginForm.getSearchComponentName(),
						userForm.getBloodBankID());

			}

			request.setAttribute("componentMsg", "available");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
					"Delete Component Exception occurred");

			return ERROR;

		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String searchInstruction() throws Exception {
		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		instructionsSearchList = loginDAOInf.searchInstructions(loginForm.getSearchInstruction(),
				userForm.getBloodBankID());

		/*
		 * Checking whether instruction list is empty or not, if empty give
		 * error message saying Instruction not found
		 */
		if (instructionsSearchList.size() > 0) {

			request.setAttribute("instructionMsg", "available");

			return SUCCESS;

		} else {

			String errorMsg = "Instruction '" + loginForm.getSearchInstruction() + "' not found.";

			addActionError(errorMsg);

			return ERROR;
		}
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String viewAllInstruction() throws Exception {
		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		instructionsSearchList = loginDAOInf.retrieveInstructions(userForm.getBloodBankID());

		/*
		 * Checking whether instruction list is empty or not, if empty give
		 * error message saying Instruction not found
		 */
		if (instructionsSearchList.size() > 0) {

			request.setAttribute("instructionMsg", "available");

			return SUCCESS;

		} else {

			addActionError("No Instructions found. Please add new Instruction.");

			return ERROR;
		}
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String addInstruction() throws Exception {
		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		message = loginDAOInf.insertInstructions(loginForm, userForm.getBloodBankID());

		if (message.equalsIgnoreCase("success")) {

			addActionMessage("Instruction added successfully.");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Add Instruction");

			return SUCCESS;

		} else {

			addActionError("Failed to add Instruction. Please check server logs for more details.");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
					"Add Instruction Exception occurred");

			return ERROR;

		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String renderEditInstruction() throws Exception {
		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		instructionsEditList = loginDAOInf.retrieveEditInstructionsList(loginForm.getInstructionID(),
				loginForm.getSearchInstruction());

		if (instructionsEditList.size() > 0) {

			/*
			 * Retrieving instruction search list
			 */
			if (loginForm.getSearchInstruction() == null || loginForm.getSearchInstruction() == "") {

				instructionsSearchList = loginDAOInf.retrieveInstructions(userForm.getBloodBankID());

			} else {

				instructionsSearchList = loginDAOInf.searchInstructions(loginForm.getSearchInstruction(),
						userForm.getBloodBankID());

			}

			request.setAttribute("instructionMsg", "available");

			request.setAttribute("instructionEdit", "edit");

			return SUCCESS;

		} else {

			/*
			 * Retrieving instruction search list
			 */
			if (loginForm.getSearchInstruction() == null || loginForm.getSearchInstruction() == "") {

				instructionsSearchList = loginDAOInf.retrieveInstructions(userForm.getBloodBankID());

			} else {

				instructionsSearchList = loginDAOInf.searchInstructions(loginForm.getSearchInstruction(),
						userForm.getBloodBankID());

			}

			request.setAttribute("instructionMsg", "available");

			addActionError(
					"Failed to retrieve instruction list based on instruction ID. Please check server logs for more details.");

			return ERROR;
		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String editInstruction() throws Exception {
		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		message = loginDAOInf.updateInstructions(loginForm, userForm.getBloodBankID());

		if (message.equalsIgnoreCase("success")) {

			addActionMessage("Instruction updated successfully.");

			/*
			 * Retrieving instruction search list
			 */
			if (loginForm.getSearchInstruction() == null || loginForm.getSearchInstruction() == "") {

				instructionsSearchList = loginDAOInf.retrieveInstructions(userForm.getBloodBankID());

			} else {

				instructionsSearchList = loginDAOInf.searchInstructions(loginForm.getSearchInstruction(),
						userForm.getBloodBankID());

			}

			request.setAttribute("instructionMsg", "available");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Edit Instruction");

			return SUCCESS;

		} else {

			addActionError("Failed to update Instruction. Please check server logs for more details.");

			/*
			 * Retrieving instruction search list
			 */
			if (loginForm.getSearchInstruction() == null || loginForm.getSearchInstruction() == "") {

				instructionsSearchList = loginDAOInf.retrieveInstructions(userForm.getBloodBankID());

			} else {

				instructionsSearchList = loginDAOInf.searchInstructions(loginForm.getSearchInstruction(),
						userForm.getBloodBankID());

			}

			request.setAttribute("instructionMsg", "available");

			request.setAttribute("instructionEdit", "edit");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
					"Edit Instruction Exception occurred");

			return ERROR;

		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String deleteInstruction() throws Exception {
		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		message = loginDAOInf.deleteInstructions(loginForm.getInstructionID());

		if (message.equalsIgnoreCase("success")) {

			addActionMessage("Instruction deleted successfully.");

			/*
			 * Retrieving instruction search list
			 */
			if (loginForm.getSearchInstruction() == null || loginForm.getSearchInstruction() == "") {

				instructionsSearchList = loginDAOInf.retrieveInstructions(userForm.getBloodBankID());

			} else {

				instructionsSearchList = loginDAOInf.searchInstructions(loginForm.getSearchInstruction(),
						userForm.getBloodBankID());

			}

			request.setAttribute("instructionMsg", "available");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Delete Instruction");

			return SUCCESS;

		} else {

			addActionError("Failed to delete Instruction. Please check server logs for more details.");

			/*
			 * Retrieving instruction search list
			 */
			if (loginForm.getSearchInstruction() == null || loginForm.getSearchInstruction() == "") {

				instructionsSearchList = loginDAOInf.retrieveInstructions(userForm.getBloodBankID());

			} else {

				instructionsSearchList = loginDAOInf.searchInstructions(loginForm.getSearchInstruction(),
						userForm.getBloodBankID());

			}

			request.setAttribute("instructionMsg", "available");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
					"Delete Instruction Exception occurred");

			return ERROR;

		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String searchShifts() throws Exception {
		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		shiftList = loginDAOInf.searchShifts(loginForm.getSearchShiftName(), userForm.getBloodBankID());

		/*
		 * Checking whether shift list is empty or not, if empty give error
		 * message saying shift not found
		 */
		if (shiftList.size() > 0) {

			request.setAttribute("componentMsg", "available");

			return SUCCESS;

		} else {

			String errorMsg = "Shift '" + loginForm.getSearchShiftName() + "' not found.";

			addActionError(errorMsg);

			return ERROR;
		}
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String viewAllShifts() throws Exception {
		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		shiftList = loginDAOInf.retrieveShifts(userForm.getBloodBankID());

		/*
		 * Checking whether shift list is empty or not, if empty give error
		 * message saying shift not found
		 */
		if (shiftList.size() > 0) {

			request.setAttribute("componentMsg", "available");

			return SUCCESS;

		} else {

			addActionError("No shifts found. Please add new shifts");

			return ERROR;
		}
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String renderEditShift() throws Exception {
		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		shiftEditList = loginDAOInf.retrieveEditShiftsList(loginForm.getShiftID(), loginForm.getSearchShiftName());

		if (shiftEditList.size() > 0) {

			/*
			 * Retrieving shift search list
			 */
			if (loginForm.getSearchShiftName() == null || loginForm.getSearchShiftName() == "") {

				shiftList = loginDAOInf.retrieveShifts(userForm.getBloodBankID());

			} else {

				shiftList = loginDAOInf.searchShifts(loginForm.getSearchShiftName(), userForm.getBloodBankID());

			}

			request.setAttribute("componentMsg", "available");

			request.setAttribute("componentEdit", "edit");

			return SUCCESS;

		} else {

			/*
			 * Retrieving shift search list
			 */
			if (loginForm.getSearchShiftName() == null || loginForm.getSearchShiftName() == "") {

				shiftList = loginDAOInf.retrieveShifts(userForm.getBloodBankID());

			} else {

				shiftList = loginDAOInf.searchShifts(loginForm.getSearchShiftName(), userForm.getBloodBankID());

			}

			request.setAttribute("componentMsg", "available");

			addActionError(
					"Failed to retrieve shift list based on shift ID. Please check server logs for more details.");

			return ERROR;
		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String addShift() throws Exception {
		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		message = loginDAOInf.insertShift(loginForm, userForm.getBloodBankID());

		if (message.equalsIgnoreCase("success")) {

			addActionMessage("Shift added successfully.");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Add Shift");

			return SUCCESS;

		} else {

			addActionError("Failed to add shift. Please check server logs for more details.");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
					"Add Shift Exception occurred");

			return ERROR;

		}

	}

	public String editShift() throws Exception {
		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		message = loginDAOInf.updateShift(loginForm, userForm.getBloodBankID());

		if (message.equalsIgnoreCase("success")) {

			addActionMessage("Shift updated successfully.");

			/*
			 * Retrieving shift search list
			 */
			if (loginForm.getSearchShiftName() == null || loginForm.getSearchShiftName() == "") {

				shiftList = loginDAOInf.retrieveShifts(userForm.getBloodBankID());

			} else {

				shiftList = loginDAOInf.searchShifts(loginForm.getSearchShiftName(), userForm.getBloodBankID());

			}

			request.setAttribute("componentMsg", "available");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Edit Shift");

			return SUCCESS;

		} else {

			addActionError("Failed to update shift. Please check server logs for more details.");

			/*
			 * Retrieving shift search list
			 */
			if (loginForm.getSearchShiftName() == null || loginForm.getSearchShiftName() == "") {

				shiftList = loginDAOInf.retrieveShifts(userForm.getBloodBankID());

			} else {

				shiftList = loginDAOInf.searchShifts(loginForm.getSearchShiftName(), userForm.getBloodBankID());

			}

			request.setAttribute("componentMsg", "available");

			request.setAttribute("componentEdit", "edit");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
					"Edit Shift Exception occurred");

			return ERROR;

		}

	}

	public String deleteShift() throws Exception {
		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		message = loginDAOInf.deleteShifts(loginForm.getShiftID());

		if (message.equalsIgnoreCase("success")) {

			addActionMessage("Shift deleted successfully.");

			/*
			 * Retrieving shift search list
			 */
			if (loginForm.getSearchShiftName() == null || loginForm.getSearchShiftName() == "") {

				shiftList = loginDAOInf.retrieveShifts(userForm.getBloodBankID());

			} else {

				shiftList = loginDAOInf.searchShifts(loginForm.getSearchShiftName(), userForm.getBloodBankID());

			}

			request.setAttribute("componentMsg", "available");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Shift Component");

			return SUCCESS;

		} else {

			addActionError("Failed to delete shift. Please check server logs for more details.");

			/*
			 * Retrieving shift search list
			 */
			if (loginForm.getSearchShiftName() == null || loginForm.getSearchShiftName() == "") {

				shiftList = loginDAOInf.retrieveShifts(userForm.getBloodBankID());

			} else {

				shiftList = loginDAOInf.searchShifts(loginForm.getSearchShiftName(), userForm.getBloodBankID());

			}

			request.setAttribute("componentMsg", "available");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
					"Delete Shift Exception occurred");

			return ERROR;

		}

	}

	/**
	 * @return the shiftEditList
	 */
	public List<UserForm> getShiftEditList() {
		return shiftEditList;
	}

	/**
	 * @param shiftEditList
	 *            the shiftEditList to set
	 */
	public void setShiftEditList(List<UserForm> shiftEditList) {
		this.shiftEditList = shiftEditList;
	}

	/**
	 * @return the shiftList
	 */
	public List<UserForm> getShiftList() {
		return shiftList;
	}

	/**
	 * @param shiftList
	 *            the shiftList to set
	 */
	public void setShiftList(List<UserForm> shiftList) {
		this.shiftList = shiftList;
	}

	/**
	 * @return the componentSearchList
	 */
	public List<UserForm> getComponentSearchList() {
		return componentSearchList;
	}

	/**
	 * @param componentSearchList
	 *            the componentSearchList to set
	 */
	public void setComponentSearchList(List<UserForm> componentSearchList) {
		this.componentSearchList = componentSearchList;
	}

	/**
	 * @return the componentEditList
	 */
	public List<UserForm> getComponentEditList() {
		return componentEditList;
	}

	/**
	 * @param componentEditList
	 *            the componentEditList to set
	 */
	public void setComponentEditList(List<UserForm> componentEditList) {
		this.componentEditList = componentEditList;
	}

	/**
	 * @return the instructionsSearchList
	 */
	public List<UserForm> getInstructionsSearchList() {
		return instructionsSearchList;
	}

	/**
	 * @param instructionsSearchList
	 *            the instructionsSearchList to set
	 */
	public void setInstructionsSearchList(List<UserForm> instructionsSearchList) {
		this.instructionsSearchList = instructionsSearchList;
	}

	/**
	 * @return the instructionsEditList
	 */
	public List<UserForm> getInstructionsEditList() {
		return instructionsEditList;
	}

	/**
	 * @param instructionsEditList
	 *            the instructionsEditList to set
	 */
	public void setInstructionsEditList(List<UserForm> instructionsEditList) {
		this.instructionsEditList = instructionsEditList;
	}

	/**
	 * @return the helpManualFile
	 */
	public FileInputStream getHelpManualFile() {
		return helpManualFile;
	}

	/**
	 * @param helpManualFile
	 *            the helpManualFile to set
	 */
	public void setHelpManualFile(FileInputStream helpManualFile) {
		this.helpManualFile = helpManualFile;
	}

	/**
	 * @return the helpManualFileName
	 */
	public String getHelpManualFileName() {
		return helpManualFileName;
	}

	/**
	 * @param helpManualFileName
	 *            the helpManualFileName to set
	 */
	public void setHelpManualFileName(String helpManualFileName) {
		this.helpManualFileName = helpManualFileName;
	}

	/**
	 * @return the chargeSearchList
	 */
	public List<UserForm> getChargeSearchList() {
		return chargeSearchList;
	}

	/**
	 * @param chargeSearchList
	 *            the chargeSearchList to set
	 */
	public void setChargeSearchList(List<UserForm> chargeSearchList) {
		this.chargeSearchList = chargeSearchList;
	}

	/**
	 * @return the chargeTypeEditList
	 */
	public List<UserForm> getChargeTypeEditList() {
		return chargeTypeEditList;
	}

	/**
	 * @param chargeTypeEditList
	 *            the chargeTypeEditList to set
	 */
	public void setChargeTypeEditList(List<UserForm> chargeTypeEditList) {
		this.chargeTypeEditList = chargeTypeEditList;
	}

	/**
	 * @return the concessionEditList
	 */
	public List<UserForm> getConcessionEditList() {
		return concessionEditList;
	}

	/**
	 * @param concessionEditList
	 *            the concessionEditList to set
	 */
	public void setConcessionEditList(List<UserForm> concessionEditList) {
		this.concessionEditList = concessionEditList;
	}

	/**
	 * @return the concessionList
	 */
	public List<UserForm> getConcessionList() {
		return concessionList;
	}

	/**
	 * @param concessionList
	 *            the concessionList to set
	 */
	public void setConcessionList(List<UserForm> concessionList) {
		this.concessionList = concessionList;
	}

	/**
	 * @return the bldBnkList
	 */
	public List<UserForm> getBldBnkList() {
		return bldBnkList;
	}

	/**
	 * @param bldBnkList
	 *            the bldBnkList to set
	 */
	public void setBldBnkList(List<UserForm> bldBnkList) {
		this.bldBnkList = bldBnkList;
	}

	/**
	 * @return the userList
	 */
	public List<UserForm> getUserList() {
		return userList;
	}

	/**
	 * @param userList
	 *            the userList to set
	 */
	public void setUserList(List<UserForm> userList) {
		this.userList = userList;
	}

	/**
	 * @return the bloodBankList
	 */
	public HashMap<Integer, String> getBloodBankList() {
		return bloodBankList;
	}

	/**
	 * @param bloodBankList
	 *            the bloodBankList to set
	 */
	public void setBloodBankList(HashMap<Integer, String> bloodBankList) {
		this.bloodBankList = bloodBankList;
	}

	/**
	 * @return the loginForm
	 */
	public UserForm getLoginForm() {
		return loginForm;
	}

	/**
	 * @param loginForm
	 *            the loginForm to set
	 */
	public void setLoginForm(UserForm loginForm) {
		this.loginForm = loginForm;
	}

	public void setSession(Map<String, Object> sessionAttriute) {
		this.sessionAttriute = sessionAttriute;

	}

	public UserForm getModel() {
		// TODO Auto-generated method stub
		return loginForm;
	}

	public void setServletResponse(HttpServletResponse response) {

		this.response = response;

	}

	public void setServletRequest(HttpServletRequest request) {

		this.request = request;

	}

}
