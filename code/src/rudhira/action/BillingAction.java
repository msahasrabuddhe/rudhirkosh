package rudhira.action;

import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import java.io.*;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.SessionAware;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import rudhira.DAO.BillingDAOImpl;
import rudhira.DAO.BillingDAOInf;
import rudhira.DAO.UserDAOImpl;
import rudhira.DAO.UserDAOInf;
import rudhira.form.BillingForm;
import rudhira.form.UserForm;
import rudhira.service.RudhiraServiceImpl;
import rudhira.service.RudhiraServiceInf;
import rudhira.util.PDFUtil;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

/**
 * 
 * @author Kovid Bioanalytics
 * 
 */
public class BillingAction extends ActionSupport
		implements ModelDriven<BillingForm>, SessionAware, ServletRequestAware, ServletResponseAware {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	BillingForm billingForm = new BillingForm();

	String message = null;

	private Map<String, Object> sessionAttriute = null;

	HttpServletRequest request;
	HttpServletResponse response;

	UserDAOInf loginDAOInf = null;
	RudhiraServiceInf serviceInf = null;

	BillingDAOInf billingDAOInf = null;

	PDFUtil pdfUtil = null;

	List<BillingForm> billList;

	List<BillingForm> componentList;

	List<BillingForm> otherChargeList;

	List<BillingForm> abdCrslist;

	List<BillingForm> CrsMatlist;

	List<BillingForm> otherProductList;

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String renderAddNewBill() throws Exception {

		return SUCCESS;
	}

	/**
	 * 
	 * @throws Exception
	 */
	public void getComponentRate() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		JSONObject values = new JSONObject();

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		response.setCharacterEncoding("UTF-8");

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		try {

			String component = request.getParameter("component");

			values = billingDAOInf.retrieveRateByComponent(component, userForm.getBloodBankID());

			PrintWriter out = response.getWriter();

			out.print(values);

		} catch (Exception exception) {

			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while retrieving price based on component");

			array.add(object);

			values.put("Release", array);

			PrintWriter out = response.getWriter();

			out.print(values);
		}

	}

	/**
	 * 
	 * @throws Exception
	 */
	public void getOtherChargesRate() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		JSONObject values = new JSONObject();

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		response.setCharacterEncoding("UTF-8");

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		try {

			String chargeType = request.getParameter("chargeType");

			values = billingDAOInf.retrieveRateByChargeType(chargeType, userForm.getBloodBankID());

			PrintWriter out = response.getWriter();

			out.print(values);

		} catch (Exception exception) {

			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while retrieving price based on charge type");

			array.add(object);

			values.put("Release", array);

			PrintWriter out = response.getWriter();

			out.print(values);
		}

	}

	/**
	 * 
	 * @throws Exception
	 */
	public void getConcessionRate() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		JSONObject values = new JSONObject();

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		response.setCharacterEncoding("UTF-8");

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		try {

			String concession = request.getParameter("concession");

			values = billingDAOInf.retrieveRateByConcession(concession, userForm.getBloodBankID());

			PrintWriter out = response.getWriter();

			out.print(values);

		} catch (Exception exception) {

			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while retrieving price based on concession");

			array.add(object);

			values.put("Release", array);

			PrintWriter out = response.getWriter();

			out.print(values);
		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String addNewBill() throws Exception {

		serviceInf = new RudhiraServiceImpl();
		loginDAOInf = new UserDAOImpl();

		pdfUtil = new PDFUtil();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		message = serviceInf.addNewBill(billingForm, userForm.getUserID());

		if (message.equalsIgnoreCase("success")) {

			/*
			 * Writing and Generating PDF for New Bill
			 */
			ServletContext context = request.getServletContext();

			String realPath = context.getRealPath("/");

			String PDFFileName = "";

			/*
			 * Checking whether firstName contains B/O, if so. replacing / by - else storing
			 * first name as is
			 */
			if (billingForm.getFirstName().contains("B/O")) {

				String firstName = billingForm.getFirstName().replace("B/O", "B-O");

				PDFFileName = realPath + firstName + billingForm.getLastName() + billingForm.getPatientID() + "_"
						+ billingForm.getReceiptID() + "bill.pdf";

			} else {

				PDFFileName = realPath + billingForm.getFirstName() + billingForm.getLastName()
						+ billingForm.getPatientID() + "_" + billingForm.getReceiptID() + "bill.pdf";

			}

			message = pdfUtil.generateBillPDF(userForm.getBloodBankID(), userForm.getFullname(),
					billingForm.getPatientIDNo(), billingForm.getReceiptID(), PDFFileName, realPath,
					billingForm.getBbStorageCenter());

			if (message.equalsIgnoreCase("success")) {

				addActionMessage("New bill added successfully.");

				// Setting pdf file name into request object
				request.setAttribute("PDFOutFileName", PDFFileName);

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Add New Bill");

				return SUCCESS;

			} else {

				addActionError("Failed to create PDF for newly added bill. Please check server logs for more details.");

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
						"Add New Bill PDF generation Exception occurred.");

				return ERROR;

			}

		} else {

			addActionError("Failed to add new bill. Please check server logs for more details.");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
					"Add New Bill Exception occurred.");

			return ERROR;

		}
	}

	/**
	 * 
	 * @throws Exception
	 */
	public void PDFDownload() throws Exception {
		String pdfOutFIleName = request.getParameter("pdfOutPath");
		System.out.println("PDF File path from action ::::: " + pdfOutFIleName);

		response.setContentType("application/pdf");
		InputStream in = new FileInputStream(pdfOutFIleName);
		OutputStream out11 = response.getOutputStream();
		byte[] buf = new byte[1024];
		int len;
		while ((len = in.read(buf)) > 0) {
			out11.write(buf, 0, len);
		}
		in.close();
		out11.close();
	}

	/**
	 * 
	 * @throws Exception
	 */
	public void getPatientDetail() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		JSONObject values = new JSONObject();

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		response.setCharacterEncoding("UTF-8");

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		try {

			String bdrNo = request.getParameter("bdrNo");

			if (bdrNo == null || bdrNo == "") {
				bdrNo = "0";
			}

			int BDRNo = Integer.parseInt(bdrNo);

			values = billingDAOInf.retrievePatientDetailsByBDRNo(BDRNo);

			PrintWriter out = response.getWriter();

			out.print(values);

		} catch (Exception exception) {

			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while retrieving patient details based bdrNo no.");

			array.add(object);

			values.put("Release", array);

			PrintWriter out = response.getWriter();

			out.print(values);
		}

	}

	/**
	 * 
	 * @throws Exception
	 */
	public void getReceiptNo() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		JSONObject values = new JSONObject();

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		response.setCharacterEncoding("UTF-8");

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		try {

			String receiptType = request.getParameter("receiptType");

			values = billingDAOInf.retrieveReceiptNoByReceiptType(receiptType, userForm.getBloodBankID());

			PrintWriter out = response.getWriter();

			out.print(values);

		} catch (Exception exception) {

			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while retrieving receipt no based on receipt type.");

			array.add(object);

			values.put("Release", array);

			PrintWriter out = response.getWriter();

			out.print(values);
		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String searchBill() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		billList = billingDAOInf.searchBillByPatientName(billingForm.getSearchBillName(), billingForm.getStartDate(),
				billingForm.getEndDate());

		/*
		 * Checking whether billList is empty or not, if empty give error message saying
		 * bill not found
		 */
		if (billList.size() > 0) {

			request.setAttribute("userListEnable", "userSearchListEnable");

			return SUCCESS;

		} else {

			String errorMsg = "No bill found for '" + billingForm.getSearchBillName() + "'";

			addActionError(errorMsg);

			return ERROR;
		}
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String editBillList() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		billList = billingDAOInf.retrieveAllReceiptDetails(billingForm.getStartDate(), billingForm.getEndDate());

		/*
		 * Checking whether billList is empty or not, if empty give error message saying
		 * bill not found. please add new bill
		 */
		if (billList.size() > 0) {

			request.setAttribute("userListEnable", "userSearchListEnable");

			return SUCCESS;

		} else {

			String errorMsg = "No bills found. Please add new bill.";

			addActionError(errorMsg);

			return ERROR;
		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String renderMyBillList() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		billList = billingDAOInf.retrieveMyReceiptDetails(billingForm.getStartDate(), billingForm.getEndDate(),
				userForm.getUserID());

		/*
		 * Checking whether billList is empty or not, if empty give error message saying
		 * bill not found. please add new bill
		 */
		if (billList.size() > 0) {

			request.setAttribute("userListEnable", "userSearchListEnable");

			return SUCCESS;

		} else {

			String errorMsg = "No bills found. Please add new bill.";

			addActionError(errorMsg);

			return ERROR;
		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String renderEditBill() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		/*
		 * Checking the user role is administrator or other than administrator and
		 * according to that fetching receipt details
		 */
		if (userForm.getUserRole().equals("administrator") || userForm.getUserRole().equals("receptionist")) {

			// Retrieving receipt detail list from Receipt table
			billList = billingDAOInf.retrieveReceipt(billingForm.getReceiptID(), billingForm.getPatientID());

			// Retrieving component details list from ReceiptItem table
			componentList = billingDAOInf.retrieveComponentForAdminUser(billingForm.getReceiptID());

			// Retrieving other charges details list from OtherCharges table
			otherChargeList = billingDAOInf.retrieveOtherCharges(billingForm.getReceiptID());

			/*
			 * Retrieving the payment type of receipt and setting it into request object in
			 * order to check on viewBill page to display respective division of payment
			 * type
			 */
			String paymentType = billingDAOInf.retrievePaymenyType(billingForm.getReceiptID());

			request.setAttribute("paymentType", paymentType);

			/*
			 * Retrieving receiptType in order to display the corresping division of storage
			 * center for receipt type IBT or BSC
			 */
			String receiptType = billingDAOInf.retrieveReceiptType(billingForm.getReceiptID());

			request.setAttribute("receiptType", receiptType);

			/*
			 * Checking whether receipt item contains either ABD or Cross matching or both
			 */
			int check = billingDAOInf.andScreeningAndCrsMatchingCheck(billingForm.getReceiptID());

			if (check == 1) {

				CrsMatlist = billingDAOInf.retrieveCrossMatchingList(billingForm.getReceiptID());

			} else if (check == 2) {

				abdCrslist = billingDAOInf.retrieveABDScreeningList(billingForm.getReceiptID());

			} else if (check == 3) {

				CrsMatlist = billingDAOInf.retrieveCrossMatchingList(billingForm.getReceiptID());

				abdCrslist = billingDAOInf.retrieveABDScreeningList(billingForm.getReceiptID());

			}

			request.setAttribute("check", String.valueOf(check));

			// Retrieving concession type and setting it into request variable
			String concessionType = billingDAOInf.retrieveConcessionType(billingForm.getReceiptID());

			request.setAttribute("concessionType", concessionType);

			return SUCCESS;

		} else {

			// Retrieving receipt detail list from Receipt table
			billList = billingDAOInf.retrieveReceipt(billingForm.getReceiptID(), billingForm.getPatientID());

			// Retrieving component details list from ReceiptItem table
			componentList = billingDAOInf.retrieveComponent(billingForm.getReceiptID());

			// Retrieving other charges details list from OtherCharges table
			otherChargeList = billingDAOInf.retrieveOtherCharges(billingForm.getReceiptID());

			/*
			 * Retrieving receiptType in order to display the corresping division of storage
			 * center for receipt type IBT or BSC
			 */
			String receiptType = billingDAOInf.retrieveReceiptType(billingForm.getReceiptID());

			request.setAttribute("receiptType", receiptType);

			return SUCCESS;

		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String editBill() throws Exception {

		serviceInf = new RudhiraServiceImpl();

		loginDAOInf = new UserDAOImpl();

		billingDAOInf = new BillingDAOImpl();

		pdfUtil = new PDFUtil();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		if (userForm.getUserRole().equals("administrator") || userForm.getUserRole().equals("receptionist")) {

			message = serviceInf.editBill(billingForm, userForm.getUserID(), "administrator/receptionist");

			String firstName1 = billingForm.getFirstName();

			// Retrieving receipt detail list from Receipt table
			billList = billingDAOInf.retrieveReceipt(billingForm.getReceiptID(), billingForm.getPatientID());

			// Retrieving component details list from ReceiptItem table
			componentList = billingDAOInf.retrieveComponentForAdminUser(billingForm.getReceiptID());

			// Retrieving other charges details list from OtherCharges table
			otherChargeList = billingDAOInf.retrieveOtherCharges(billingForm.getReceiptID());

			/*
			 * Retrieving the payment type of receipt and setting it into request object in
			 * order to check on viewBill page to display respective division of payment
			 * type
			 */
			String paymentType = billingDAOInf.retrievePaymenyType(billingForm.getReceiptID());

			request.setAttribute("paymentType", paymentType);

			/*
			 * Retrieving receiptType in order to display the corresping division of storage
			 * center for receipt type IBT or BSC
			 */
			String receiptType = billingDAOInf.retrieveReceiptType(billingForm.getReceiptID());

			request.setAttribute("receiptType", receiptType);

			/*
			 * Checking whether receipt item contains either ABD or Cross matching or both
			 */
			int check = billingDAOInf.andScreeningAndCrsMatchingCheck(billingForm.getReceiptID());

			if (check == 1) {

				CrsMatlist = billingDAOInf.retrieveCrossMatchingList(billingForm.getReceiptID());

			} else if (check == 2) {

				abdCrslist = billingDAOInf.retrieveABDScreeningList(billingForm.getReceiptID());

			} else if (check == 3) {

				CrsMatlist = billingDAOInf.retrieveCrossMatchingList(billingForm.getReceiptID());

				abdCrslist = billingDAOInf.retrieveABDScreeningList(billingForm.getReceiptID());

			}

			request.setAttribute("check", String.valueOf(check));

			// Retrieving concession type and setting it into request variable
			String concessionType = billingDAOInf.retrieveConcessionType(billingForm.getReceiptID());

			request.setAttribute("concessionType", concessionType);

			if (message.equalsIgnoreCase("success")) {

				/*
				 * Writing and Generating PDF for New Bill
				 */
				ServletContext context = request.getServletContext();

				String realPath = context.getRealPath("/");

				String PDFFileName = "";

				/*
				 * Checking whether firstName contains B/O, if so. replacing / by - else storing
				 * first name as is
				 */
				if (firstName1 == null || firstName1 == "") {
					PDFFileName = realPath + firstName1 + billingForm.getLastName() + billingForm.getPatientID() + "_"
							+ billingForm.getReceiptID() + "bill.pdf";
				} else {

					if (firstName1.contains("B/O")) {

						String firstName = firstName1.replace("B/O", "B-O");

						PDFFileName = realPath + firstName + billingForm.getLastName() + billingForm.getPatientID()
								+ "_" + billingForm.getReceiptID() + "bill.pdf";

					} else {

						PDFFileName = realPath + firstName1 + billingForm.getLastName() + billingForm.getPatientID()
								+ "_" + billingForm.getReceiptID() + "bill.pdf";

					}

				}

				message = pdfUtil.generateBillPDF(userForm.getBloodBankID(), userForm.getFullname(),
						billingForm.getPatientIDNo(), billingForm.getReceiptID(), PDFFileName, realPath,
						billingForm.getBbStorageCenter());

				if (message.equalsIgnoreCase("success")) {

					addActionMessage("Bill updated successfully.");

					// Setting pdf file name into request object
					request.setAttribute("PDFOutFileName", PDFFileName);

					// Inserting into Audit
					loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Edit Bill");

					return SUCCESS;

				} else {

					addActionError(
							"Failed to create PDF for existing bill. Please check server logs for more details.");

					// Inserting into Audit
					loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
							"Edit Bill PDF generation Exception occurred.");

					return ERROR;

				}

			} else {

				addActionError("Failed to update bill. Please check server logs for more details.");

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
						"Edit Bill Exception occurred.");

				return ERROR;

			}

		} else {

			String firstName1 = billingForm.getFirstName();

			message = serviceInf.editBill(billingForm, userForm.getUserID(), "courier");

			// Retrieving receipt detail list from Receipt table
			billList = billingDAOInf.retrieveReceipt(billingForm.getReceiptID(), billingForm.getPatientID());

			// Retrieving component details list from ReceiptItem table
			componentList = billingDAOInf.retrieveComponent(billingForm.getReceiptID());

			// Retrieving other charges details list from OtherCharges table
			otherChargeList = billingDAOInf.retrieveOtherCharges(billingForm.getReceiptID());

			/*
			 * Retrieving receiptType in order to display the corresping division of storage
			 * center for receipt type IBT or BSC
			 */
			String receiptType = billingDAOInf.retrieveReceiptType(billingForm.getReceiptID());

			request.setAttribute("receiptType", receiptType);

			if (message.equalsIgnoreCase("success")) {

				/*
				 * Writing and Generating PDF for New Bill
				 */
				ServletContext context = request.getServletContext();

				String realPath = context.getRealPath("/");

				String PDFFileName = "";

				/*
				 * Checking whether firstName contains B/O, if so. replacing / by - else storing
				 * first name as is
				 */
				if (firstName1 == null || firstName1 == "") {
					PDFFileName = realPath + firstName1 + billingForm.getLastName() + billingForm.getPatientID() + "_"
							+ billingForm.getReceiptID() + "bill.pdf";
				} else {

					if (firstName1.contains("B/O")) {

						String firstName = firstName1.replace("B/O", "B-O");

						PDFFileName = realPath + firstName + billingForm.getLastName() + billingForm.getPatientID()
								+ "_" + billingForm.getReceiptID() + "bill.pdf";

					} else {

						PDFFileName = realPath + firstName1 + billingForm.getLastName() + billingForm.getPatientID()
								+ "_" + billingForm.getReceiptID() + "bill.pdf";

					}

				}

				message = pdfUtil.generateBillPDF(userForm.getBloodBankID(), userForm.getFullname(),
						billingForm.getPatientIDNo(), billingForm.getReceiptID(), PDFFileName, realPath,
						billingForm.getBbStorageCenter());

				if (message.equalsIgnoreCase("success")) {

					addActionMessage("Bill updated successfully.");

					// Setting pdf file name into request object
					request.setAttribute("PDFOutFileName", PDFFileName);

					// Inserting into Audit
					loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Edit Bill");

					return SUCCESS;

				} else {

					addActionError(
							"Failed to create PDF for existing bill. Please check server logs for more details.");

					// Inserting into Audit
					loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
							"Edit Bill PDF generation Exception occurred.");

					return ERROR;

				}

			} else {

				addActionError("Failed to update bill. Please check server logs for more details.");

				// Inserting into Audit
				loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
						"Edit Bill Exception occurred.");

				return ERROR;

			}

		}

	}

	/**
	 * 
	 * @throws Exception
	 */
	public void deleteComponentRow() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		JSONObject values = new JSONObject();

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		response.setCharacterEncoding("UTF-8");

		try {

			values = billingDAOInf.deleteProduct(billingForm.getComponentID());

			PrintWriter out = response.getWriter();

			out.print(values);

		} catch (Exception exception) {

			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while deleting product component from ReceiptItems");

			array.add(object);

			values.put("Release", array);

			PrintWriter out = response.getWriter();

			out.print(values);
		}

	}

	/**
	 * 
	 * @throws Exception
	 */
	public void getBagDetails() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		JSONObject values = new JSONObject();

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		response.setCharacterEncoding("UTF-8");

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		try {

			values = billingDAOInf.retrieveBagDetails(billingForm.getComponentID());

			PrintWriter out = response.getWriter();

			out.print(values);

		} catch (Exception exception) {

			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while retrieving bag details based on receiptItemID");

			array.add(object);

			values.put("Release", array);

			PrintWriter out = response.getWriter();

			out.print(values);
		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String renderViewBill() throws Exception {
		return SUCCESS;
	}

	/**
	 * 
	 * @throws Exception
	 */
	public void retrieveShiftTime() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		JSONObject values = new JSONObject();

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		response.setCharacterEncoding("UTF-8");

		try {

			values = billingDAOInf.retrieveShiftTime(billingForm.getShiftName());

			PrintWriter out = response.getWriter();

			out.print(values);

		} catch (Exception exception) {

			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while retrieving shift time based on Shift ");

			array.add(object);

			values.put("Release", array);

			PrintWriter out = response.getWriter();

			out.print(values);
		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String openRegister() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		message = billingDAOInf.insertRegister(billingForm, userForm.getUserID());

		if (message.equalsIgnoreCase("success")) {

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Open Register");

			return SUCCESS;

		} else {

			addActionError("Failed to open register. Please check server logs for more details.");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
					"Open Register Exception Occurred");

			return ERROR;

		}

	}

	/**
	 * 
	 * @throws Exception
	 */
	public void getBankDepositeDetails() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		JSONObject values = new JSONObject();

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		response.setCharacterEncoding("UTF-8");

		try {

			values = billingDAOInf.retrieveBankDepositeList();

			PrintWriter out = response.getWriter();

			out.print(values);

		} catch (Exception exception) {
			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while retrieving bank depposited list");

			array.add(object);

			values.put("Release", array);

			PrintWriter out = response.getWriter();

			out.print(values);
		}

	}

	/**
	 * 
	 * @throws Exception
	 */
	public void getCashAdjustmentDetails() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		JSONObject values = new JSONObject();

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		response.setCharacterEncoding("UTF-8");

		try {

			values = billingDAOInf.retrieveCashAdjustmentList();

			PrintWriter out = response.getWriter();

			out.print(values);

		} catch (Exception exception) {
			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while retrieving cash adjustment list");

			array.add(object);

			values.put("Release", array);

			PrintWriter out = response.getWriter();

			out.print(values);
		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String editRegister() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		message = billingDAOInf.updateCashDeposited(billingForm.getCashDeposited());

		if (message.equalsIgnoreCase("success")) {

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Edit Register");

			return SUCCESS;

		} else {

			addActionError("Failed to update register. Please check server logs for more details.");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
					"Edit Register Exception Occurred");

			return ERROR;

		}
	}

	/**
	 * 
	 * @throws Exception
	 */
	public void retrieveRegisterBalance() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		JSONObject values = new JSONObject();

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		response.setCharacterEncoding("UTF-8");

		try {

			// Retrieving register start and end time
			String[] registerShiftTime = billingDAOInf.retrieveRegisterShiftTime().split("\\+");

			String finalStartTime = "";

			String finalEndTime = "";

			System.out.println("lenght is " + registerShiftTime.length);

			if (registerShiftTime.length > 1) {

				billingForm.setRegisterStartTime(registerShiftTime[0]);

				billingForm.setRegisterEndTime(registerShiftTime[1]);

				/*
				 * Splitting start and end date in order to convert it into 24hour format
				 */
				String[] array1 = billingForm.getRegisterStartTime().split(" ");

				String startAMPM = array1[1];

				String[] startArray = array1[0].split(":");

				String startHH = startArray[0];
				String startMM = startArray[1];

				if (startHH.length() == 1) {
					startHH = "0" + startHH;
				}

				if (startMM.length() == 1) {
					startMM = "0" + startMM;
				}

				if (startAMPM.equals("pm") && startHH.equals("12")) {
					startHH = startArray[0];

				} else if (startAMPM.equals("pm")) {
					startHH = String.valueOf(Integer.parseInt(startHH) + 12);

				} else if (startAMPM.equals("am") && startHH.equals("12")) {
					startHH = "00";

				}

				finalStartTime = startHH + ":" + startMM;

				System.out.println("final start time: " + finalStartTime);

				String[] array2 = billingForm.getRegisterEndTime().split(" ");

				String endAMPM = array2[1];

				String[] endArray = array2[0].split(":");

				String endHH = endArray[0];
				String endMM = endArray[1];

				if (endHH.length() == 1) {
					endHH = "0" + endHH;
				}

				if (endMM.length() == 1) {
					endMM = "0" + endMM;
				}

				if (endAMPM.equals("pm") && endHH.equals("12")) {
					endHH = endArray[0];

				} else if (endAMPM.equals("pm")) {
					endHH = String.valueOf(Integer.parseInt(endHH) + 12);

				} else if (endAMPM.equals("am") && endHH.equals("12")) {
					endHH = "00";

				}

				finalEndTime = endHH + ":" + endMM;

				System.out.println("final end time: " + finalEndTime);

			}

			/*
			 * Retrieving sum of advance payment from Receipt for the current date with
			 * shift time in order to insert that into totalCash of Register
			 */
			double shiftCash = billingDAOInf.retrieveAdvancePaymentSum(finalStartTime, finalEndTime);

			System.out.println("Shift cash is :: " + shiftCash);

			/*
			 * Retrieving cashHandover and cashDeposited and other product cash from
			 * Register
			 */
			double cashHandove = billingDAOInf.retrieveCashHandover();

			double cashDeposited = billingDAOInf.retrieveCashdeposited();

			double otherProductCash = billingDAOInf.retrieveOtherProductCash();

			/*
			 * Calculating register balance as follows
			 */
			double registerBalance = cashHandove - cashDeposited + shiftCash + otherProductCash;

			object.put("registerBalance", registerBalance);
			object.put("shiftCash", shiftCash);

			array.add(object);

			values.put("Release", array);

			PrintWriter out = response.getWriter();

			out.print(values);

		} catch (Exception exception) {
			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while retrieving cashDeposited");

			array.add(object);

			values.put("Release", array);

			PrintWriter out = response.getWriter();

			out.print(values);
		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String addOtherProduct() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		loginDAOInf = new UserDAOImpl();

		serviceInf = new RudhiraServiceImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		message = serviceInf.addOtherProduct(billingForm);

		if (message.equalsIgnoreCase("success")) {

			addActionMessage("Other product added successfully.");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Add Other Product");

			return SUCCESS;

		} else {

			addActionError("Failed to add other product. Please check server logs for more details.");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
					"Add Other Product Exception Occurred");

			return ERROR;

		}

	}

	/**
	 * 
	 * @throws Exception
	 */
	public void retrieveBalanceDetails() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		JSONObject values = new JSONObject();

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		response.setCharacterEncoding("UTF-8");

		try {

			values = billingDAOInf.retrieveBalanceDetails(billingForm.getStartDate(), billingForm.getEndDate());

			PrintWriter out = response.getWriter();

			out.print(values);

		} catch (Exception exception) {
			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while retrieving balance details");

			array.add(object);

			values.put("Release", array);

			PrintWriter out = response.getWriter();

			out.print(values);
		}

	}

	/**
	 * 
	 * @throws Exception
	 */
	public void retrieveBloodGroup() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		JSONObject values = new JSONObject();

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		response.setCharacterEncoding("UTF-8");

		try {

			values = billingDAOInf.retrieveBloodGroup(billingForm.getBloodBagNo());

			PrintWriter out = response.getWriter();

			out.print(values);

		} catch (Exception exception) {
			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while retrieving blood group based on bag no");

			array.add(object);

			values.put("Release", array);

			PrintWriter out = response.getWriter();

			out.print(values);
		}

	}

	/**
	 * 
	 * @throws Exception
	 */
	public void verifyLastRegisterOpen() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		JSONObject values = new JSONObject();

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		response.setCharacterEncoding("UTF-8");

		try {

			values = billingDAOInf.verifyLastRegisterOpen(billingForm.getShiftOrder());

			PrintWriter out = response.getWriter();

			out.print(values);

		} catch (Exception exception) {
			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while verifying whether last register was opened or not");

			array.add(object);

			values.put("Release", array);

			PrintWriter out = response.getWriter();

			out.print(values);
		}

	}

	/**
	 * 
	 * @throws Exception
	 */
	public void getLastRegisterBalance() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		JSONObject values = new JSONObject();

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		response.setCharacterEncoding("UTF-8");

		try {

			// Retrieving register start and end time
			String[] registerShiftTime = billingDAOInf.retrieveRegisterShiftTime(billingForm.getShiftOrder())
					.split("\\+");

			String finalStartTime = "";

			String finalEndTime = "";

			System.out.println("lenght is " + registerShiftTime.length);

			if (registerShiftTime.length > 1) {

				billingForm.setRegisterStartTime(registerShiftTime[0]);

				billingForm.setRegisterEndTime(registerShiftTime[1]);

				/*
				 * Splitting start and end date in order to convert it into 24hour format
				 */
				String[] array1 = billingForm.getRegisterStartTime().split(" ");

				String startAMPM = array1[1];

				String[] startArray = array1[0].split(":");

				String startHH = startArray[0];
				String startMM = startArray[1];

				if (startHH.length() == 1) {
					startHH = "0" + startHH;
				}

				if (startMM.length() == 1) {
					startMM = "0" + startMM;
				}

				if (startAMPM.equals("pm") && startHH.equals("12")) {
					startHH = startArray[0];

				} else if (startAMPM.equals("pm")) {
					startHH = String.valueOf(Integer.parseInt(startHH) + 12);

				} else if (startAMPM.equals("am") && startHH.equals("12")) {
					startHH = "00";

				}

				finalStartTime = startHH + ":" + startMM;

				System.out.println("final start time: " + finalStartTime);

				String[] array2 = billingForm.getRegisterEndTime().split(" ");

				String endAMPM = array2[1];

				String[] endArray = array2[0].split(":");

				String endHH = endArray[0];
				String endMM = endArray[1];

				if (endHH.length() == 1) {
					endHH = "0" + endHH;
				}

				if (endMM.length() == 1) {
					endMM = "0" + endMM;
				}

				if (endAMPM.equals("pm") && endHH.equals("12")) {
					endHH = endArray[0];

				} else if (endAMPM.equals("pm")) {
					endHH = String.valueOf(Integer.parseInt(endHH) + 12);

				} else if (endAMPM.equals("am") && endHH.equals("12")) {
					endHH = "00";

				}

				finalEndTime = endHH + ":" + endMM;

				System.out.println("final end time: " + finalEndTime);

			}

			/*
			 * Retrieving sum of advance payment from Receipt for the current date with
			 * shift time in order to insert that into totalCash of Register
			 */
			double shiftCash = billingDAOInf.retrieveAdvancePaymentSum(finalStartTime, finalEndTime,
					billingForm.getShiftOrder());

			System.out.println("Shift cash is :: " + shiftCash);

			/*
			 * Retrieving cashHandover and cashDeposited and other product cash from
			 * Register
			 */
			double cashHandove = billingDAOInf.retrieveCashHandover(billingForm.getShiftOrder());

			double cashDeposited = billingDAOInf.retrieveCashdeposited(billingForm.getShiftOrder());

			double otherProductCash = billingDAOInf.retrieveOtherProductCash(billingForm.getShiftOrder());

			/*
			 * Calculating register balance as follows
			 */
			double registerBalance = cashHandove - cashDeposited + shiftCash + otherProductCash;

			object.put("registerBalance", registerBalance);
			object.put("shiftCash", shiftCash);

			array.add(object);

			values.put("Release", array);

			PrintWriter out = response.getWriter();

			out.print(values);

		} catch (Exception exception) {
			exception.printStackTrace();

			object.put("ErrMsg", "Error occured while retrieving cashDeposited");

			array.add(object);

			values.put("Release", array);

			PrintWriter out = response.getWriter();

			out.print(values);
		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String searchOtherProduct() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		otherProductList = billingDAOInf.searchOtherProducts(billingForm.getSearchOtherProductName());

		/*
		 * Checking whether otherProductList is empty or not, if empty give error
		 * message saying other product not found
		 */
		if (otherProductList.size() > 0) {

			request.setAttribute("userListEnable", "userSearchListEnable");

			return SUCCESS;

		} else {

			String errorMsg = "No other product found with product name '" + billingForm.getSearchOtherProductName()
					+ "'";

			addActionError(errorMsg);

			return ERROR;
		}
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String viewAllOtherProducts() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		otherProductList = billingDAOInf.retrieveAllOtherProcucts();

		/*
		 * Checking whether otherProductList is empty or not, if empty give error
		 * message saying other product not found
		 */
		if (otherProductList.size() > 0) {

			request.setAttribute("userListEnable", "userSearchListEnable");

			return SUCCESS;

		} else {

			String errorMsg = "No other product found.";

			addActionError(errorMsg);

			return ERROR;
		}
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public void retrieveRefReceiptNo() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		JSONObject values = new JSONObject();

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		response.setCharacterEncoding("UTF-8");

		try {

			values = billingDAOInf.retireveReceiptDetailsByPatientID(billingForm.getPatientID());

			PrintWriter out = response.getWriter();

			out.print(values);

		} catch (Exception exception) {

			exception.printStackTrace();

			object.put("ErrMsg", "Exception occurred while retrieving receipt details based on patientID");

			array.add(object);

			values.put("Release", array);

			PrintWriter out = response.getWriter();

			out.print(values);
		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public void startFirstRegister() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		JSONObject values = new JSONObject();

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		response.setCharacterEncoding("UTF-8");

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		try {

			values = billingDAOInf.insertFirstRegisterDetails(billingForm.getStartCash());

			PrintWriter out = response.getWriter();

			out.print(values);

		} catch (Exception exception) {

			exception.printStackTrace();

			object.put("ErrMsg", "Exception occurred while starting first register.");

			array.add(object);

			values.put("Release", array);

			PrintWriter out = response.getWriter();

			out.print(values);
		}
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String startRegister() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		loginDAOInf = new UserDAOImpl();

		serviceInf = new RudhiraServiceImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		message = serviceInf.startRegister(billingForm, userForm.getUserID());

		if (message.equals("success")) {

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Start Register");

			return SUCCESS;

		} else {

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
					"Start Register Exception Occurred.");

			return ERROR;

		}
	}

	/**
	 * 
	 */
	public void retrieveExpectedCash() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		JSONObject values = new JSONObject();

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		response.setCharacterEncoding("UTF-8");

		try {

			values = billingDAOInf.retrievePreviousExpectedCash();

			PrintWriter out = response.getWriter();

			out.print(values);

		} catch (Exception exception) {

			exception.printStackTrace();

			object.put("ErrMsg", "Exception occurred while retrieving previous expected cash balance.");

			array.add(object);

			values.put("Release", array);

			PrintWriter out = response.getWriter();

			out.print(values);
		}
	}

	/**
	 * 
	 * @throws Excpetion
	 */
	public void getEditBankDeposit() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		JSONObject values = new JSONObject();

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		response.setCharacterEncoding("UTF-8");

		try {

			values = billingDAOInf.retrieveCashDepositeByID(billingForm.getCashDepID());

			PrintWriter out = response.getWriter();

			out.print(values);

		} catch (Exception exception) {

			exception.printStackTrace();

			object.put("ErrMsg", "Exception occurred while retrieving cash deposit details based on ID.");

			array.add(object);

			values.put("Release", array);

			PrintWriter out = response.getWriter();

			out.print(values);
		}
	}

	/**
	 * 
	 * @throws Exception
	 */
	public void confirmDeleteBankDeposit() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		JSONObject values = new JSONObject();

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		response.setCharacterEncoding("UTF-8");

		try {

			values = billingDAOInf.deleteCashDepositeByID(billingForm.getCashDepID());

			PrintWriter out = response.getWriter();

			out.print(values);

		} catch (Exception exception) {

			exception.printStackTrace();

			object.put("ErrMsg", "Exception occurred while deleting cash deposit details based on ID.");

			array.add(object);

			values.put("Release", array);

			PrintWriter out = response.getWriter();

			out.print(values);
		}
	}

	/**
	 * 
	 * @throws Exception
	 */
	public void getEditCashAdjustment() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		JSONObject values = new JSONObject();

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		response.setCharacterEncoding("UTF-8");

		try {

			values = billingDAOInf.retrieveCashAdjustedByID(billingForm.getCashAdjID());

			PrintWriter out = response.getWriter();

			out.print(values);

		} catch (Exception exception) {

			exception.printStackTrace();

			object.put("ErrMsg", "Exception occurred while retrieving cash adjustment details based on ID.");

			array.add(object);

			values.put("Release", array);

			PrintWriter out = response.getWriter();

			out.print(values);
		}
	}

	/**
	 * 
	 * @throws Exception
	 */
	public void confirmDeleteCashAdjustment() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		JSONObject values = new JSONObject();

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		response.setCharacterEncoding("UTF-8");

		try {

			values = billingDAOInf.deleteCashAdjustedByID(billingForm.getCashAdjID());

			PrintWriter out = response.getWriter();

			out.print(values);

		} catch (Exception exception) {

			exception.printStackTrace();

			object.put("ErrMsg", "Exception occurred while deleting cash adjustment details based on ID.");

			array.add(object);

			values.put("Release", array);

			PrintWriter out = response.getWriter();

			out.print(values);
		}
	}

	/**
	 * 
	 * @throws Exception
	 */
	public void addCashAdjustment() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		JSONObject values = new JSONObject();

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		response.setCharacterEncoding("UTF-8");

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		try {

			// retrieving last registerID from Register table
			int registerID = billingDAOInf.retrieveRegisterID();

			values = billingDAOInf.insertCashAdjustment(billingForm, userForm.getUserID(), registerID);

			PrintWriter out = response.getWriter();

			out.print(values);

		} catch (Exception exception) {

			exception.printStackTrace();

			object.put("ErrMsg", "Exception occurred while adding cash adjustment details.");

			array.add(object);

			values.put("Release", array);

			PrintWriter out = response.getWriter();

			out.print(values);
		}
	}

	/**
	 * 
	 * @throws Exception
	 */
	public void editCashAdjustment() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		JSONObject values = new JSONObject();

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		response.setCharacterEncoding("UTF-8");

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		try {

			values = billingDAOInf.updateCashAdjustment(billingForm, userForm.getUserID());

			PrintWriter out = response.getWriter();

			out.print(values);

		} catch (Exception exception) {

			exception.printStackTrace();

			object.put("ErrMsg", "Exception occurred while updating cash adjustment details.");

			array.add(object);

			values.put("Release", array);

			PrintWriter out = response.getWriter();

			out.print(values);
		}
	}

	/**
	 * 
	 * @throws Exception
	 */
	public void addCashDeposit() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		JSONObject values = new JSONObject();

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		response.setCharacterEncoding("UTF-8");

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		try {

			// retrieving last registerID from Register table
			int registerID = billingDAOInf.retrieveRegisterID();

			values = billingDAOInf.insertCashDeposit(billingForm, userForm.getUserID(), registerID);

			PrintWriter out = response.getWriter();

			out.print(values);

		} catch (Exception exception) {

			exception.printStackTrace();

			object.put("ErrMsg", "Exception occurred while adding cash deposit details.");

			array.add(object);

			values.put("Release", array);

			PrintWriter out = response.getWriter();

			out.print(values);
		}
	}

	/**
	 * 
	 * @throws Exception
	 */
	public void editCashDeposit() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		JSONObject values = new JSONObject();

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		response.setCharacterEncoding("UTF-8");

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		try {

			values = billingDAOInf.updateCashDeposit(billingForm, userForm.getUserID());

			PrintWriter out = response.getWriter();

			out.print(values);

		} catch (Exception exception) {

			exception.printStackTrace();

			object.put("ErrMsg", "Exception occurred while updating cash deposit details.");

			array.add(object);

			values.put("Release", array);

			PrintWriter out = response.getWriter();

			out.print(values);
		}
	}

	/**
	 * 
	 * @throws Exception
	 */
	public void retrieveCloseRegisterDetails() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		serviceInf = new RudhiraServiceImpl();

		JSONObject values = new JSONObject();

		JSONArray array = new JSONArray();

		values = new JSONObject();

		JSONObject object = new JSONObject();

		response.setCharacterEncoding("UTF-8");

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		try {

			// retrieving last registerID from Register table
			int registerID = billingDAOInf.retrieveRegisterID();

			values = serviceInf.retrieveCloseRegisterDetails(registerID);

			PrintWriter out = response.getWriter();

			out.print(values);

		} catch (Exception exception) {

			exception.printStackTrace();

			object.put("ErrMsg", "Exception occurred while retrieving register details for closing.");

			array.add(object);

			values.put("Release", array);

			PrintWriter out = response.getWriter();

			out.print(values);
		}
	}

	/**
	 * 
	 * @return
	 * @throws Excpetion
	 */
	public String closeRegister() throws Exception {

		loginDAOInf = new UserDAOImpl();

		billingDAOInf = new BillingDAOImpl();

		serviceInf = new RudhiraServiceImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		message = serviceInf.closeRegister(billingForm);

		if (message.equals("success")) {

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(), "Close Register");

			return SUCCESS;

		} else {

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
					"Close Register Exception Occurred.");

			return ERROR;

		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String renderViewAddBill() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		/*
		 * Writing and Generating PDF for New Bill
		 */
		ServletContext context = request.getServletContext();

		String realPath = context.getRealPath("/");

		String PDFFileName = "";

		/*
		 * Checking whether firstName contains B/O, if so. replacing / by - else storing
		 * first name as is
		 */
		if (billingForm.getFirstName().contains("B/O")) {

			String firstName = billingForm.getFirstName().replace("B/O", "B-O");

			PDFFileName = realPath + firstName + billingForm.getLastName() + billingForm.getPatientID() + "_"
					+ billingForm.getReceiptID() + "bill.pdf";

		} else {

			PDFFileName = realPath + billingForm.getFirstName() + billingForm.getLastName() + billingForm.getPatientID()
					+ "_" + billingForm.getReceiptID() + "bill.pdf";

		}

		// Setting pdf file name into request object
		request.setAttribute("PDFOutFileName", PDFFileName);

		/*
		 * Checking the user role is administrator or other than administrator and
		 * according to that fetching receipt details
		 */
		if (userForm.getUserRole().equals("administrator")) {

			// Retrieving receipt detail list from Receipt table
			billList = billingDAOInf.retrieveReceipt(billingForm.getReceiptID(), billingForm.getPatientID());

			// Retrieving component details list from ReceiptItem table
			componentList = billingDAOInf.retrieveComponentForAdminUser(billingForm.getReceiptID());

			// Retrieving other charges details list from OtherCharges table
			otherChargeList = billingDAOInf.retrieveOtherCharges(billingForm.getReceiptID());

			/*
			 * Retrieving the payment type of receipt and setting it into request object in
			 * order to check on viewBill page to display respective division of payment
			 * type
			 */
			String paymentType = billingDAOInf.retrievePaymenyType(billingForm.getReceiptID());

			request.setAttribute("paymentType", paymentType);

			/*
			 * Retrieving receiptType in order to display the corresping division of storage
			 * center for receipt type IBT or BSC
			 */
			String receiptType = billingDAOInf.retrieveReceiptType(billingForm.getReceiptID());

			request.setAttribute("receiptType", receiptType);

			/*
			 * Checking whether receipt item contains either ABD or Cross matching or both
			 */
			int check = billingDAOInf.andScreeningAndCrsMatchingCheck(billingForm.getReceiptID());

			if (check == 1) {

				CrsMatlist = billingDAOInf.retrieveCrossMatchingList(billingForm.getReceiptID());

			} else if (check == 2) {

				abdCrslist = billingDAOInf.retrieveABDScreeningList(billingForm.getReceiptID());

			} else if (check == 3) {

				CrsMatlist = billingDAOInf.retrieveCrossMatchingList(billingForm.getReceiptID());

				abdCrslist = billingDAOInf.retrieveABDScreeningList(billingForm.getReceiptID());

			}

			request.setAttribute("check", String.valueOf(check));

			// Retrieving concession type and setting it into request variable
			String concessionType = billingDAOInf.retrieveConcessionType(billingForm.getReceiptID());

			request.setAttribute("concessionType", concessionType);

			return SUCCESS;

		} else {

			// Retrieving receipt detail list from Receipt table
			billList = billingDAOInf.retrieveReceipt(billingForm.getReceiptID(), billingForm.getPatientID());

			// Retrieving component details list from ReceiptItem table
			componentList = billingDAOInf.retrieveComponent(billingForm.getReceiptID());

			// Retrieving other charges details list from OtherCharges table
			otherChargeList = billingDAOInf.retrieveOtherCharges(billingForm.getReceiptID());

			/*
			 * Retrieving receiptType in order to display the corresping division of storage
			 * center for receipt type IBT or BSC
			 */
			String receiptType = billingDAOInf.retrieveReceiptType(billingForm.getReceiptID());

			request.setAttribute("receiptType", receiptType);

			return SUCCESS;

		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String printBillReceipt() throws Exception {

		loginDAOInf = new UserDAOImpl();

		billingDAOInf = new BillingDAOImpl();

		pdfUtil = new PDFUtil();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		String firstName1 = billingForm.getFirstName();

		// Retrieving receipt detail list from Receipt table
		billList = billingDAOInf.retrieveReceipt(billingForm.getReceiptID(), billingForm.getPatientID());

		// Retrieving component details list from ReceiptItem table
		componentList = billingDAOInf.retrieveComponent(billingForm.getReceiptID());

		// Retrieving other charges details list from OtherCharges table
		otherChargeList = billingDAOInf.retrieveOtherCharges(billingForm.getReceiptID());

		/*
		 * Retrieving receiptType in order to display the corresping division of storage
		 * center for receipt type IBT or BSC
		 */
		String receiptType = billingDAOInf.retrieveReceiptType(billingForm.getReceiptID());

		request.setAttribute("receiptType", receiptType);

		/*
		 * Writing and Generating PDF for New Bill
		 */
		ServletContext context = request.getServletContext();

		String realPath = context.getRealPath("/");

		String PDFFileName = "";

		/*
		 * Checking whether firstName contains B/O, if so. replacing / by - else storing
		 * first name as is
		 */
		if (firstName1 == null || firstName1 == "") {
			PDFFileName = realPath + firstName1 + billingForm.getLastName() + billingForm.getPatientID() + "_"
					+ billingForm.getReceiptID() + "bill.pdf";
		} else {

			if (firstName1.contains("B/O")) {

				String firstName = firstName1.replace("B/O", "B-O");

				PDFFileName = realPath + firstName + billingForm.getLastName() + billingForm.getPatientID() + "_"
						+ billingForm.getReceiptID() + "bill.pdf";

			} else {

				PDFFileName = realPath + firstName1 + billingForm.getLastName() + billingForm.getPatientID() + "_"
						+ billingForm.getReceiptID() + "bill.pdf";

			}

		}

		message = pdfUtil.generateBillPDF(userForm.getBloodBankID(), userForm.getFullname(),
				billingForm.getPatientIDNo(), billingForm.getReceiptID(), PDFFileName, realPath,
				billingForm.getBbStorageCenter());

		if (message.equalsIgnoreCase("success")) {

			// Setting pdf file name into request object
			request.setAttribute("PDFOutFileName", PDFFileName);

			return SUCCESS;

		} else {

			addActionError("Failed to print bill receipt. Please check server logs for more details.");

			return ERROR;

		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String addNewBloodStorageCenter() throws Exception {

		billingDAOInf = new BillingDAOImpl();

		loginDAOInf = new UserDAOImpl();

		/*
		 * Getting userID from Session
		 */
		UserForm userForm = (UserForm) sessionAttriute.get("USER");

		message = billingDAOInf.insertBloodStorageName(billingForm.getBbStorageCenter());

		if (message.equals("success")) {

			addActionMessage("New storage center added sucecssfully.");

			System.out.println("New blood storage center name is added successfully into PVBloodStorageCentre  table.");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
					"Add New Blood Storage Center");

			return SUCCESS;
		} else {

			System.out.println("Failed to add new storage center name.");

			addActionError("Failed to add new storage center name. Please check server logs for more details.");

			// Inserting into Audit
			loginDAOInf.insertAuditDetails(userForm.getUserID(), request.getRemoteAddr(),
					"Add New Blood Storage Center Exception Occurred.");

			return ERROR;
		}

	}

	/**
	 * @return the otherProductList
	 */
	public List<BillingForm> getOtherProductList() {
		return otherProductList;
	}

	/**
	 * @param otherProductList the otherProductList to set
	 */
	public void setOtherProductList(List<BillingForm> otherProductList) {
		this.otherProductList = otherProductList;
	}

	/**
	 * @return the crsMatlist
	 */
	public List<BillingForm> getCrsMatlist() {
		return CrsMatlist;
	}

	/**
	 * @param crsMatlist the crsMatlist to set
	 */
	public void setCrsMatlist(List<BillingForm> crsMatlist) {
		CrsMatlist = crsMatlist;
	}

	/**
	 * @return the abdCrslist
	 */
	public List<BillingForm> getAbdCrslist() {
		return abdCrslist;
	}

	/**
	 * @param abdCrslist the abdCrslist to set
	 */
	public void setAbdCrslist(List<BillingForm> abdCrslist) {
		this.abdCrslist = abdCrslist;
	}

	/**
	 * @return the componentList
	 */
	public List<BillingForm> getComponentList() {
		return componentList;
	}

	/**
	 * @param componentList the componentList to set
	 */
	public void setComponentList(List<BillingForm> componentList) {
		this.componentList = componentList;
	}

	/**
	 * @return the otherChargeList
	 */
	public List<BillingForm> getOtherChargeList() {
		return otherChargeList;
	}

	/**
	 * @param otherChargeList the otherChargeList to set
	 */
	public void setOtherChargeList(List<BillingForm> otherChargeList) {
		this.otherChargeList = otherChargeList;
	}

	/**
	 * @return the billList
	 */
	public List<BillingForm> getBillList() {
		return billList;
	}

	/**
	 * @param billList the billList to set
	 */
	public void setBillList(List<BillingForm> billList) {
		this.billList = billList;
	}

	/**
	 * @return the billingForm
	 */
	public BillingForm getBillingForm() {
		return billingForm;
	}

	/**
	 * @param billingForm the billingForm to set
	 */
	public void setBillingForm(BillingForm billingForm) {
		this.billingForm = billingForm;
	}

	public void setSession(Map<String, Object> sessionAttriute) {
		this.sessionAttriute = sessionAttriute;

	}

	public BillingForm getModel() {
		// TODO Auto-generated method stub
		return billingForm;
	}

	public void setServletResponse(HttpServletResponse response) {

		this.response = response;

	}

	public void setServletRequest(HttpServletRequest request) {

		this.request = request;

	}

}
